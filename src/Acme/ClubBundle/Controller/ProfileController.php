<?php

namespace Acme\ClubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

class ProfileController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function profileAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('club_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        $session->set('active_page', 'profile' ); 
        
        if(isset($_POST['email'])){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            
            $model = $em->getRepository('AcmeHeadOfficeBundle:ClubAdmin')->findOneBy(array('club_admin_id'=>$session->get("club_admin_id")));
            $model->setFname($_POST['fname']);
            $model->setLname($_POST['lname']);
            if(strtolower(trim($_POST['password'])) != ''){
                $model->setPassword($mod->passGenerator($_POST['password']));
            }
            $model->setEmail($_POST['email']);
            $em->persist($model);
            $em->flush();
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            if($error_count == 0){
                if($mod->isEmailValid($_POST['email']) == false || ($_POST['password'] != $_POST['confirm_password'])){
                    $errors = array();
                }
                
                if(!$mod->isEmailValid($_POST['email'])){
                    $errors[] = array('message'=>$_POST['email'].' is not a valid email.');
                    $error_count += 1;
                }

                if($_POST['password'] != $_POST['confirm_password']){
                    $errors[] = array('message'=>'Password must be repeated exactly.');
                    $error_count += 1;
                }
            }
            
            if($error_count == 0){
                
                $em->getConnection()->commit(); 
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    'Your profile has been updated successfully.'
                );
                
                // SET ACTIVITY
                $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " updated his/her profile.";
                $this->setActivity($session->get('club_admin_id'), 'club-admin', $details);
                return $this->redirect($this->generateUrl('acme_club_profile'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
               
                return $this->render('AcmeClubBundle:Profile:profile.html.twig',
                        array('errors'=>$errors,
                            'post'=>$_POST
                        ));
            }
        }else{
            return $this->render('AcmeClubBundle:Profile:profile.html.twig',
                    array('post' => $this->getClubAdminUserById($session->get('club_admin_id')))
                    );
        }
    }

}
