<?php

namespace Acme\ClubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

class DashboardController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function dashboardAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        
        if($session->get('club_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        
        
        $keyword_search = (isset($_GET['keyword'])) ? $_GET['keyword'] : '';
        $sort = (isset($_GET['sort'])) ? $_GET['sort'] : NULL;
        //echo date('Y-m-d',strtotime('first day of this month')) ;
        
        $data = array(
            date("M",strtotime("-3 Months"))=> $this->getReports($session->get('club_id'), array('from'=> date('Y-m-d',strtotime('first day of ' . date("F Y",strtotime("-3 Months")) )) . ' 00:00:00', 'to'=> date('Y-m-d',strtotime('last day of ' . date("F Y",strtotime("-3 Months")) )) . ' 23:59:59')), 
            date("M",strtotime("-2 Months")) => $this->getReports($session->get('club_id'), array('from'=> date('Y-m-d',strtotime('first day of ' . date("F Y",strtotime("-2 Months")) )) . ' 00:00:00', 'to'=> date('Y-m-d',strtotime('last day of ' . date("F Y",strtotime("-2 Months")) )) . ' 23:59:59')), 
            date("M",strtotime("-1 Months")) => $this->getReports($session->get('club_id'), array('from'=> date('Y-m-d',strtotime('first day of ' . date("F Y",strtotime("-1 Months")) )) . ' 00:00:00', 'to'=> date('Y-m-d',strtotime('last day of ' . date("F Y",strtotime("-1 Months")) )) . ' 23:59:59')), 
            date("M") => $this->getReports($session->get('club_id'), array('from'=> date('Y-m-d',strtotime('first day of this month')) . ' 00:00:00', 'to'=> date('Y-m-d',strtotime('last day of this month')) . ' 23:59:59'))
            );
        
        $filter_daterange = array( 
                    'from' => date('Y-m-d',strtotime('first day of ' . date("F Y",strtotime("-3 Months")) )) . ' 00:00:00',
                    'to' => $datetime->format("Y-m-d H:i:s"),
                );
        
        return $this->render('AcmeClubBundle:Dashboard:dashboard.html.twig',
                array(
                    'summary' => $this->getReports(
                                $session->get('club_id'),
                                $filter_daterange
                            ),
                    'clients' => $this->getClubClientsWithRecentTestResult(
                        'active', 
                        $session->get('club_id'),
                        0,
                        $this->container->getParameter('ajax_default_load_number'),
                        $sort,
                        $keyword_search
                        ),
                    'data'=> $data
                    )
                );
    }
    
    public function update12weekProgramClientProgressAction(){
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        if($session->get('club_admin_id') == ''){ 
            return new Response("session_expired");
        }
        
        $session->set('show_first_login_msg', 0);
        
        
        $_POST['email'] = trim($_POST['email']);
        if(!$mod->isEmailValid($_POST['email'])){
            return new Response('failed');
        }
        
        $prog_client_details = $this->get12weekProgramClientByEmail( $_POST['email']  );
        $week_total_actions= $this->getProgramClientDayItems( $_POST['email'], $prog_client_details['working_week'], NULL, NULL, 'quote');
        $week_complete_actions = $this->getProgramClientDayItems( $_POST['email'], $prog_client_details['working_week'] , NULL, 1);
        $overall_actions = $this->getProgramClientDayItems( $_POST['email'], NULL, NULL, NULL, 'quote');
        $overall_complete_actions = $this->getProgramClientDayItems( $_POST['email'], NULL , NULL, 1);
        $data = array(
            'week_total_actions'=> count($week_total_actions),
            'week_complete_actions'=> count($week_complete_actions),
            'overall_actions'=> count($overall_actions),
            'overall_complete_actions'=> count($overall_complete_actions)
        );
        
        return new Response(json_encode($data));

    }

    
    public function inviteFriendsAction(){
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        
        if($session->get('club_admin_id') == ''){
            return new Response("session_expired");
        }
        
        $to = $_POST['recipients'];
        $name = $_POST['friend_name'];
        //$to = explode(",", $to);
        $from = $this->container->getParameter('site_email_address');
        $subject = 'Invitation - 12 Week Healthy Life Program';
        $body = $this->renderView('AcmeClubBundle:Dashboard:invite_friends_email.html.twig',array('name'=>$name));
        
        
        $recipients = array();
        for($i=0; $i<count($to); $i++){
            if($mod->isEmailValid(trim($to[$i]))){
                $recipients[] = trim($to[$i]);
            }
        }
        
//        if(count($recipients) > 0){
//            $this->sendEmail($recipients, '', $from, $subject, $body);
//        }
        $this->sendEmail($to, '', $from, $subject, $body);
        
        return new Response("success");
    }
}
