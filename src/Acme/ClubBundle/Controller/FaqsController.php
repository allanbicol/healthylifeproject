<?php

namespace Acme\ClubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FaqsController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function faqsAction()
    {
        $session = $this->getRequest()->getSession();
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        $session->set('active_page', 'faqs' ); 
        return $this->render('AcmeClubBundle:Faqs:faqs.html.twig');
    }

}
