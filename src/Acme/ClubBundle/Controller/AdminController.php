<?php

namespace Acme\ClubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

class AdminController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function adminAction()
    {
        $session = $this->getRequest()->getSession();
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        $session->set('active_page', 'admin' ); 
        
        
        if(isset($_POST['card_expiry_month'])){
            //http://www.templemantwells.com.au/article/tools-tips/integrating-pin-payments-using-php
            //Authentication
            $auth['u'] = $this->container->getParameter('pin_payment_api_private_key_test'); //Private Key from PIN
            $auth['p'] = $this->container->getParameter('pin_payment_api_password'); //API calls use empty password
            $club_details = $this->getClubById($session->get('club_id'));
            if(count($club_details) > 0){
                //Fields to Post                    
                $post = array();
                $post['email'] = $_POST['card_email'];
                if(isset($_POST['card_number'])){
                    $post['card[number]'] = $_POST['card_number'];
                }else{
                    $post['card[number]'] = $club_details['card_number'];
                }
                $post['card[expiry_month]'] = $_POST['card_expiry_month'];
                $post['card[expiry_year]'] = $_POST['card_expiry_year'];
                $post['card[cvc]'] = $_POST['card_cvc'];
                if(isset($_POST['card_name'])){
                    $post['card[name]'] = $_POST['card_name'];
                }else{
                    $post['card[name]'] = $club_details['card_name'];
                }
                $post['card[address_line1]'] = $_POST['card_address_line1'];
                //$post['card[address_line2]'] = '';
                $post['card[address_city]'] = $_POST['card_address_city'];
                $post['card[address_postcode]'] = $_POST['card_address_postcode'];
                $post['card[address_state]'] = $_POST['card_address_state'];
                $post['card[address_country]'] = 'Australia';

                // Create a curl handle
                if(isset($club_details['pin_payment_customer_token']) && trim($club_details['pin_payment_customer_token']) != ''){
                    $ch = curl_init( $this->container->getParameter('pin_payment_api_customer_url_test') . '/' . $club_details['pin_payment_customer_token'] );
                }else{
                    $ch = curl_init( $this->container->getParameter('pin_payment_api_customer_url_test') );
                }

                curl_setopt($ch, CURLOPT_USERPWD, $auth['u'] . ":" . $auth['p']); //authenticate
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //make sure it returns a response
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // allow https verification if true
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // allow https verification if true
                if(isset($club_details['pin_payment_customer_token']) && trim($club_details['pin_payment_customer_token']) != ''){
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT"); //tell it we are posting
                }else{
                    curl_setopt($ch, CURLOPT_POST, 1); //tell it we are posting
                }
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post); //tell it what to post
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json')); //response comes back as json

                // Execute
                $response = curl_exec($ch);

                // Check if any error occurred
                if(!curl_errno($ch)){$info = curl_getinfo($ch);}

                // Close handle
                curl_close($ch);

                //Decode JSON
                $response = json_decode($response, true);
                
                if(isset($response['error'])){

                    if(isset($response['messages'])){
                        for($i=0; $i<count($response['messages']); $i++){
                            $error[] = array('message'=> $response['messages'][$i]['message']);
                        }
                    }else{
                        if(isset($response['error_description'])){
                            $error[] = array('message'=> $response['error_description']);
                        }
                    }



                    $this->get('session')->getFlashBag()->add(
                        'card_error',
                        $error
                    );
                    
                }else{
                    $em = $this->getDoctrine()->getManager();
                    $em->getConnection()->beginTransaction();
                    
                    $club_update = $em->getRepository('AcmeHeadOfficeBundle:Club')->findOneBy(array('club_id'=>$session->get("club_id")));
                    if(isset($club_details['pin_payment_customer_token']) && trim($club_details['pin_payment_customer_token']) != ''){
//                        $club_update->setPinPaymentCustomerToken($response['response']['token']);
                    }else{
                        $club_update->setPinPaymentCustomerToken($response['response']['token']);
                    }
                    if(isset($_POST['card_number'])){
                        $club_update->setCardNumber($_POST['card_number']);
                    }
                    $club_update->setCardExpiryMonth($_POST['card_expiry_month']);
                    $club_update->setCardExpiryYear($_POST['card_expiry_year']);
                    $club_update->setCardCvc($_POST['card_cvc']);
                    if(isset($_POST['card_name'])){
                        $club_update->setCardName($_POST['card_name']);
                    }
                    $club_update->setCardEmail($_POST['card_email']);
                    $club_update->setCardAddressLine1($_POST['card_address_line1']);
                    $club_update->setCardAddressCity($_POST['card_address_city']);
                    $club_update->setCardAddressPostcode($_POST['card_address_postcode']);
                    $club_update->setCardAddressState($_POST['card_address_state']);
                    $em->persist($club_update);
                    $em->flush();
                    $em->getConnection()->commit(); 
                }
            }
        }else{
            if(isset($_GET['note'])){
            
                $_GET['note'] = filter_var($_GET['note'], FILTER_SANITIZE_NUMBER_INT);
                $em = $this->getDoctrine()->getManager();
                $model = $em->getRepository('AcmeHeadOfficeBundle:Notifications')->findOneBy(
                            array(
                                'club_admin_id'=>$session->get('club_admin_id'),
                                'note_id'=> $_GET['note']
                            )
                        );
                if(count($model) > 0){
                    $model->setSRead(1);
                    $em->persist($model);
                    $em->flush();
                }
            }
        }
        //$lat_long = $this->get_lat_long($pro);
        return $this->render('AcmeClubBundle:Admin:admin.html.twig',
                array(
                    'club_admins'=> $this->getClubAdminsByClubId($session->get('club_id')),
                    'events'=> $this->getEvents($session->get('club_id')),
                    'states' => $this->getStates(),
                    'client_category'=>$this->getClientCategory($session->get('club_id')),
                    'club_details'=> $this->getClubById($session->get('club_id'))
                ));
    }

}
