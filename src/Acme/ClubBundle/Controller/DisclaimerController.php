<?php

namespace Acme\ClubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DisclaimerController extends Controller
{
    public function disclaimerAction()
    {
       return $this->render('AcmeClubBundle:Disclaimer:disclaimer.html.twig'); 
    }

}
