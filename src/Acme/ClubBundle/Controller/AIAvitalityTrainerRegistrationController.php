<?php

namespace Acme\ClubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AIAvitalityTrainerRegistrationController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function registrationAction()
    {
        $session = $this->getRequest()->getSession();
        if($session->get('club_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        $session->set('active_page', 'aia-registration' ); 
        
        if(isset($_POST['accept_terms'])){
            $em = $this->getDoctrine()->getManager();
            $model = $em->getRepository('AcmeHeadOfficeBundle:ClubAdmin')->findOneBy(
                        array(
                            'club_admin_id'=>$session->get('club_admin_id')
                        )
                    );
            if(count($model) > 0){
                $model->setSAiaApproved(1);

                $em->persist($model);
                $em->flush();
            }
            
            $subject = 'AIA Vitality Trainer Registration';
            $body = $this->renderView('AcmeClubBundle:AIAvitalityTrainerRegistration:registration_email_template.html.twig',
                            array(
                                'club_admin_name' => $model->getFname() . ' ' . $model->getLname(),
                                'club_admin_email'=> $model->getEmail(),
                                'club_admin_role'=> ($model->getRole() == 'site-admin') ? 'site admin' : 'coach',
                                'club_name' => $session->get('club_name')
                            ));
            
            
            $this->sendEmail( $this->container->getParameter('site_email_address') , '', $this->container->getParameter('sender_email_address'), $subject, $body);
//            $this->sendEmail( 'leokarl2108@gmail.com' , '', 'noreply@healthylifeproject.com.au', $subject, $body);
            
            return $this->redirect($this->generateUrl('acme_club_aia_vitality_trainer_registration_success'));
        }
        return $this->render('AcmeClubBundle:AIAvitalityTrainerRegistration:registration.html.twig');
    }

    public function registrationSuccessfulAction()
    {
        return $this->render('AcmeClubBundle:AIAvitalityTrainerRegistration:success.html.twig');
    }
}
