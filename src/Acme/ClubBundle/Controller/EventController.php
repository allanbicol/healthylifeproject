<?php

namespace Acme\ClubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

use Acme\HeadOfficeBundle\Entity\Event;

class EventController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function eventAction()
    {
        $session = $this->getRequest()->getSession();
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        return $this->render('AcmeClubBundle:Event:event.html.twig',
                array('events'=> $this->getEvents($session->get('club_id')))
                );
    }
    
    public function addEditEventAction($slug)
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        if(isset($_POST['event_name'])){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            
            
            if(strtolower(trim($slug)) == 'new'){
                $model = new Event();
            }else{
                $model = $em->getRepository('AcmeHeadOfficeBundle:Event')->findOneBy(array('event_id'=>$_POST["event_id"], 'club_id'=> $session->get('club_id')));
            }
            $model->setEventName($_POST['event_name']);
            $model->setLocation($_POST['location']);
            $model->setClubId($session->get('club_id'));
            $model->setStatus($_POST['status']);
            $em->persist($model);
            $em->flush();
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            
            if($error_count == 0){
                
                $em->getConnection()->commit(); 
//                if(strtolower(trim($slug)) == 'new'){
//                    $this->get('session')->getFlashBag()->add(
//                        'success',
//                        $_POST['event_name'] . ' has been added successfully.'
//                    );
//                }else{
//                    $this->get('session')->getFlashBag()->add(
//                        'success',
//                        $_POST['event_name'] . ' has been updated successfully.'
//                    );
//                }
                // SET ACTIVITY
                if(strtolower(trim($slug)) == 'new'){
                    $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " added " . $_POST['event_name'] . " as a new event.";
                }else{
                    $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " updated the details of " . $_POST['event_name'] . ", an event.";
                }
                if($session->get('club_admin_id') != ''){ 
                    $this->setActivity($session->get('club_admin_id'), 'club-admin', $details);
                }else{
                    $this->setActivity($session->get('ho_admin_id'), 'head-office-admin', $details);
                }

                //return $this->redirect($this->generateUrl('acme_club_event_add_edit', array('slug' => 'edit')) . "?eid=".$model->getEventId());
                return $this->redirect($this->generateUrl('acme_club_admin_area'));
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                if(strtolower(trim($slug)) == 'new'){
                    $this->get('session')->getFlashBag()->add(
                        'event-error',
                        $errors
                    );
                }else{
                    $this->get('session')->getFlashBag()->add(
                        'event-edit-error',
                        $errors
                    );
                }
                
               return $this->render('AcmeClubBundle:Admin:admin.html.twig',
                        array('errors'=>$errors,
                            'club_admins'=> $this->getClubAdminsByClubId($session->get('club_id')),
                            'events'=> $this->getEvents($session->get('club_id')),
                            'states' => $this->getStates(),
                            'post'=>$_POST,
                            'get'=>$_GET
                        ));
            }
            
        }
        
        if(strtolower(trim($slug)) == 'new'){
            return $this->render('AcmeClubBundle:Event:add_edit_event.html.twig');
        }else{
            $_GET['eid'] = intval($_GET['eid']);
            return $this->render('AcmeClubBundle:Event:add_edit_event.html.twig',
                    array(
                        'post'=> $this->getEventById($_GET['eid']))
                    );
        }
    }
    
    public function deleteAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        
        if(isset($_POST['event_id'])){
            $em = $this->getDoctrine()->getEntityManager();

            $_POST['event_id'] = intval($_POST['event_id']);
            $model = $em->getRepository('AcmeHeadOfficeBundle:Event')->findOneBy(array('event_id'=>$_POST["event_id"], 'club_id'=> $session->get('club_id')));
            $em->remove($model);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'success',
                    'Event has been deleted successfully.'
                );

            // SET ACTIVITY
            $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " deleted " . $model->getEventName() . ", an event.";
            
            if($session->get('club_admin_id') != ''){ 
                $this->setActivity($session->get('club_admin_id'), 'club-admin', $details);
            }else{
                $this->setActivity($session->get('ho_admin_id'), 'head-office-admin', $details);
            }
            
            //return $this->redirect($this->generateUrl('acme_club_event'));
            return $this->redirect($this->generateUrl('acme_club_admin_area'));
        }
        
    }

}
