<?php

namespace Acme\ClubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

use Acme\HeadOfficeBundle\Entity\Payment;

class CreditController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function creditAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_dashboard'));
        }
        
        return $this->render('AcmeClubBundle:Credit:credit.html.twig');
    }

    
}
