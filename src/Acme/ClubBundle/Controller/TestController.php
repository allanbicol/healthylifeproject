<?php

namespace Acme\ClubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

use Acme\HeadOfficeBundle\Entity\ClientTest;
use Acme\HeadOfficeBundle\Entity\LifestyleQuestionnareClientAnswer;
use Acme\HeadOfficeBundle\Entity\EmailQueue;
use Acme\HeadOfficeBundle\Entity\SignUpQuestionsClientAnswer;

class TestController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function testTypeRedirectAction(){
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        if(!isset($_POST['test_option']) || !isset($_POST['club_client_id'])){
            return $this->render('AcmeClubBundle:Test:view_error.html.twig',
                    array(
                        'error'=> array(
                            'title'=> 'Create Test',
                            'message'=> 'Please select an option.'
                            )
                    )
                );
        }
        
        if(!in_array($_POST['test_option'], $mod->testTypes())){
            return $this->render('AcmeClubBundle:Test:view_error.html.twig', array(
                'error'=> array(
                    'title'=> 'Error',
                    'message'=> 'Invalid parameter value.'
                    )
                )
            );
        }
        
        
        if($_POST['test_option'] == 'normal' || $_POST['test_option'] == 'aia'){
            return $this->redirect($this->generateUrl('acme_club_create_test') . '?id=' .$_POST['club_client_id']);
        }else{
            return $this->redirect($this->generateUrl('acme_club_create_lite_test') . '?id=' .$_POST['club_client_id'] . '&type=' . $_POST['test_option']);
        }
        
        return $this->render('AcmeClubBundle:Test:view_error.html.twig',
                    array(
                        'error'=> array(
                            'title'=> 'Create Test',
                            'message'=> 'Please select an option.'
                            )
                    )
                );
        
    }
    // EMAIL TEST
    public function emailTestAction(){
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        require_once($root_dir.'/resources/dompdf/dompdf_config.inc.php');
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
//        $_POST["client_test_id"] = $_GET['client_test_id'];// uncomment this to view pdf in browser
        $_POST["client_test_id"] = intval($_POST["client_test_id"]);
                
        $test_details = $this->getClientTestDetailsById($_POST["client_test_id"]);
        
        $content = '';
        
        
        if(isset($test_details['status'])){
            if($test_details['status'] == 'finalised'){
                $first_test_details = $this->getTestForComparison($test_details['club_client_id'], 'ASC');
                $last_test_details = $this->getTestForComparison($test_details['club_client_id'], 'DESC', $_POST["client_test_id"]);
                
                $content = $this->renderView('AcmeClubBundle:Test:email_test_pdf.html.twig',
                        array(
                            'site_url' => $root_dir,
                            'client_details' => $this->getClubClientById($test_details['club_client_id']),
                            'test_details' => $test_details,
                            'first_test_details' => (count($first_test_details) > 0) ? $first_test_details[0] : array(),
                            'last_test_details' => (count($last_test_details) > 0) ? $last_test_details[0] : array(),
                            'hl_expectancy_male' => $this->getHealthyLifeExpectancies('male'),
                            'hl_expectancy_female' => $this->getHealthyLifeExpectancies('female'),
                            
                            'pa_physical_activity' => $this->getSpecificTestQuestionnaireClientAnswer(1 /*physical activity*/, $test_details['club_client_id'], $_POST["client_test_id"]),
                            'pa_muscle_strengthening' => $this->getSpecificTestQuestionnaireClientAnswer(2 /*muscle strengthening*/, $test_details['club_client_id'], $_POST["client_test_id"]),
                            'alcohol_standard' => $this->getSpecificTestQuestionnaireClientAnswer(4 /*standard drink*/, $test_details['club_client_id'], $_POST["client_test_id"]),
                            'alcohol_one_setting' => $this->getSpecificTestQuestionnaireClientAnswer(5 /*standard drink one setting*/, $test_details['club_client_id'], $_POST["client_test_id"]),
                            'nutrition_questions' => $this->getTestQuestionnaire('nutrition', $test_details['club_client_id'], $_POST["client_test_id"]),
                            'mental_health_questions' => $this->getTestQuestionnaire('mental_health', $test_details['club_client_id'], $_POST["client_test_id"]),
                            'risk_profile_questions' => $this->getTestQuestionnaire('risk_profile', $test_details['club_client_id'], $_POST["client_test_id"]),
                            'smoking_options' => $this->getTestQuestionnaireOptionsByCode('smoking'),
                            'smoking_answer' => $this->getSpecificTestQuestionnaireClientAnswer(3 /*smoking*/, $test_details['club_client_id'], $_POST["client_test_id"]),
                            
                        ));
            }else{
                return new Response("error");
            }
        }else{
            return new Response("error");
        }
        
        
        $stylesheet = file_get_contents($root_dir.'/css/email-test-dompdf.css'); /// here call you external css file 
        $stylesheet = '<style>'.$stylesheet.'</style>';
        
        $test_datetime = date_create($test_details['test_datetime_finalised']);
        $test_datetime = date_format($test_datetime,"d-m-Y");
        
        $file = $root_dir .'/temp/'. $test_details['fname'] . ' ' . $test_details['lname'] .' '.$test_datetime.'.pdf';
        
        $dompdf = new \DOMPDF();
        $dompdf->set_paper( 'A4' );
        $dompdf->load_html( $stylesheet . $content );
        $dompdf->render();
//        $dompdf->stream($test_details['fname'] . ' ' . $test_details['lname'] .' '.$test_datetime.'.pdf',array('Attachment'=>0));
        //save the pdf file on the server
        file_put_contents($file, $dompdf->output()); 
        
        /* uncomment below to view pdf in browser */
//        $response = new Response("success");
//        $response->setContent('success');
//        $response->headers->set('Content-Type', 'application/pdf');
//        return $response;
        
        
        $to = $_POST['recipients'];
        $to = explode(",", $to);
//        $from = $session->get('email');
        $from = $this->container->getParameter('site_email_address');
        $subject = $test_details['fname'] . ' ' . $test_details['lname'] . ' -  Test Results PDF';
        $body = $this->renderView('AcmeClubBundle:Test:email_test_content.html.twig',
                        array(
                            'name' => $test_details['fname'] . ' ' . $test_details['lname'],
                            'test_date' => $test_details['test_datetime_finalised']
                        ));
        
        $attachments = array();
        
        
        $attachments[] = $file;
        
        $recipients = array();
        for($i=0; $i<count($to); $i++){
            if($mod->isEmailValid(trim($to[$i]))){
                $recipients[] = trim($to[$i]);
            }
        }
        
        if(count($recipients) > 0){
            $this->sendEmail($recipients, '', $from, $subject, $body, $attachments);
        }else{
            unlink($file);
        }
        
        return new Response("success");
    }
    
    
    // VIEW TEST
    public function viewTestAction(){
        $session = $this->getRequest()->getSession();
        $formula = new Model\TestFormulas();
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        $session->set('active_page', 'test' ); 
        $test_details = $this->getClientTestDetailsById($_GET['id']);
        
        
        if(isset($test_details['status'])){
            if($test_details['status'] == 'finalised'){
                $first_test_details = $this->getTestForComparison($test_details['club_client_id'], 'ASC');
                $last_test_details = $this->getTestForComparison($test_details['club_client_id'], 'DESC', $_GET['id']);
                
//                echo $first_test_details[0]['client_test_id'];
//                echo '<br>';
//                echo $last_test_details[0]['client_test_id'];
//                echo '<br>';
//                echo $test_details['client_test_id'];
//                exit();
                return $this->render('AcmeClubBundle:Test:view_test.html.twig',
                        array(
                            'client_details' => $this->getClubClientById($test_details['club_client_id']),
                            'test_details' => $test_details,
                            'first_test_details' => (count($first_test_details) > 0) ? $first_test_details[0] : array(),
                            'last_test_details' => (count($last_test_details) > 0) ? $last_test_details[0] : array(),
                            'hl_expectancy_male' => $this->getHealthyLifeExpectancies('male'),
                            'hl_expectancy_female' => $this->getHealthyLifeExpectancies('female'),
                            
                            'pa_physical_activity' => $this->getSpecificTestQuestionnaireClientAnswer(1 /*physical activity*/, $test_details['club_client_id'], $_GET['id']),
                            'pa_muscle_strengthening' => $this->getSpecificTestQuestionnaireClientAnswer(2 /*muscle strengthening*/, $test_details['club_client_id'], $_GET['id']),
                            'alcohol_standard' => $this->getSpecificTestQuestionnaireClientAnswer(4 /*standard drink*/, $test_details['club_client_id'], $_GET['id']),
                            'alcohol_one_setting' => $this->getSpecificTestQuestionnaireClientAnswer(5 /*standard drink one setting*/, $test_details['club_client_id'], $_GET['id']),
                            'nutrition_questions' => $this->getTestQuestionnaire('nutrition', $test_details['club_client_id'], $_GET['id']),
                            'mental_health_questions' => $this->getTestQuestionnaire('mental_health', $test_details['club_client_id'], $_GET['id']),
                            'risk_profile_questions' => $this->getTestQuestionnaire('risk_profile', $test_details['club_client_id'], $_GET['id']),
                            'smoking_options' => $this->getTestQuestionnaireOptionsByCode('smoking'),
                            'smoking_answer' => $this->getSpecificTestQuestionnaireClientAnswer(3 /*smoking*/, $test_details['club_client_id'], $_GET['id']),
                        ));
            }else{
                if(isset($_GET['aia'])){
                    return $this->redirect($this->generateUrl('acme_club_create_test') . '?id=' . $test_details['club_client_id'] .'&test='. $_GET['id'] . '&aia=1');
                }elseif( isset($_GET['type']) ){
                    return $this->redirect($this->generateUrl('acme_club_create_lite_test') . '?id=' . $test_details['club_client_id'] .'&test='. $_GET['id'] . '&type='. $_GET['type']);
                }else{
                    return $this->redirect($this->generateUrl('acme_club_create_test') . '?id=' . $test_details['club_client_id'] .'&test='. $_GET['id']);
                }
                
            }
        }else{
            return $this->render('AcmeClubBundle:Test:view_error.html.twig');
        }
    }
    
    public function getTestForComparison($club_client_id, $order, $except_client_test_id = NULL){
        $details = $this->getClientTestHistory($club_client_id, 'recent', 'finalised', 
                $order, $except_client_test_id, FALSE);
        if(count($details) > 0){
            $details[0]['pa_physical_activity'] = $this->getSpecificTestQuestionnaireClientAnswer(1 /*physical activity*/, $club_client_id, $details[0]['client_test_id']);
            $details[0]['pa_muscle_strengthening'] = $this->getSpecificTestQuestionnaireClientAnswer(2 /*muscle strengthening*/, $club_client_id, $details[0]['client_test_id']);
            $details[0]['alcohol_standard'] = $this->getSpecificTestQuestionnaireClientAnswer(4 /*standard drink*/, $club_client_id, $details[0]['client_test_id']);
            $details[0]['alcohol_one_setting'] = $this->getSpecificTestQuestionnaireClientAnswer(5 /*standard drink one setting*/, $club_client_id, $details[0]['client_test_id']);
            $details[0]['nutrition_questions'] = $this->getTestQuestionnaire('nutrition', $club_client_id, $details[0]['client_test_id']);
            $details[0]['mental_health_questions'] = $this->getTestQuestionnaire('mental_health', $club_client_id, $details[0]['client_test_id']);
            $details[0]['risk_profile_questions'] = $this->getTestQuestionnaire('risk_profile', $club_client_id, $details[0]['client_test_id']);
            $details[0]['smoking_answer'] = $this->getSpecificTestQuestionnaireClientAnswer(3 /*smoking*/, $club_client_id, $details[0]['client_test_id']);
        }
        
        return $details;
    }
    // CREATE TEST
    public function createTestAction()
    {
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $formula = new Model\TestFormulas();
        $mod = new Model\GlobalModel();
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        $session->set('active_page', 'test' );
        if(count($_POST) > 0){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            $error = array();
            
            
            $_POST['club_client_id'] = intval($_POST['club_client_id']);
            $_POST['event_id'] = intval($_POST['event_id']);
            $_POST['weight'] = ($_POST['weight'] != '') ? intval($_POST['weight']) : '';
            $_POST['height'] = ($_POST['height'] != '') ? intval($_POST['height']) : '';
            
            $club_client_details = $this->getClubClientById($_POST['club_client_id']);
            
            $app_type = '';
            $test_status = ($_POST['submit_type'] == 'draft') ? 'draft' : 'finalised';
            
            // VALIDATE INPUTS
            
                    // VO2 Validation
            if($_POST['physiological_vo2_test_type'] == 'bike'){
                if(trim($_POST['physiological_vo2_test_value1']) != '' && !$mod->sFullNumber($_POST['physiological_vo2_test_value1'])){
                    $error[] = array(
                        'message' => "Bike test value must be a full number."
                    );
                }

            }elseif($_POST['physiological_vo2_test_type'] == 'resting-hr'){
                if(trim($_POST['physiological_vo2_test_value1']) != '' && !$mod->sFullNumber($_POST['physiological_vo2_test_value1'])){
                    $error[] = array(
                        'message' => "Resting HR test value must a full number."
                    );
                }
            }elseif($_POST['physiological_vo2_test_type'] == 'treadmill'){

                if(trim($_POST['physiological_vo2_test_value1']) != '' && !$mod->sFullNumber($_POST['physiological_vo2_test_value1'])){
                    $error[] = array(
                        'message' => "Treadmill test 'time' value must be a full number."
                    );
                }
				
				if(trim($_POST['physiological_vo2_test_value2']) != '' && !$mod->sFullNumber($_POST['physiological_vo2_test_value2'])){
                    $error[] = array(
                        'message' => "Treadmill test 'HR' value must a full number."
                    );
                }

            }elseif($_POST['physiological_vo2_test_type'] == 'beep'){

                if(trim($_POST['physiological_vo2_test_value1']) != '' && !$mod->sFullNumber($_POST['physiological_vo2_test_value1'])){
                    $error[] = array(
                        'message' => "Beep test 'level' value must be a full number."
                    );
                }
				
				if(trim($_POST['physiological_vo2_test_value2']) != '' && !$mod->sFullNumber($_POST['physiological_vo2_test_value2'])){
                    $error[] = array(
                        'message' => "Beep test 'shuttle' value must be a full number."
                    );
                }
            }else{

                if(trim($_POST['physiological_vo2_test_value1']) != '' && !$mod->sFullNumber($_POST['physiological_vo2_test_value1'])){
                    $error[] = array(
                        'message' => "VO2 test value must not be a full number."
                    );
                }
            }
            
                // Balance Test Validation

                if(trim($_POST['physiological_balance_type']) != ''){
                        if(trim($_POST['physiological_balance_value']) != '' && !$mod->sFullNumber($_POST['physiological_balance_value'])){
                                $error[] = array(
                                                'message' => "Balance test value must be a full number."
                                        );
                        }
                }


                $client_test_id = (isset($_POST['client_test_id'])) ? $_POST['client_test_id'] : NULL;

        
            // VALIDATE INPUTS
            if($test_status == 'finalised'){
                if(trim($_POST['height']) == '' || trim($_POST['height']) == 0){
                    $error[] = array(
                            'message' => "Please enter valid height (cm)."
                        );
                }
                
                if(trim($_POST['weight']) == '' || trim($_POST['weight']) == 0){
                    $error[] = array(
                            'message' => "Please enter valid weight (kg)."
                        );
                }
                
                
                if(trim($_POST['physiological_vo2_test_type']) == ''){
                    $error[] = array(
                            'message' => "Please select VO2 type."
                        );
                }
                
                
                if(trim($_POST['physiological_vo2_test_type']) != ''){
                    if($_POST['physiological_vo2_test_type'] == 'bike'){
                        if(trim($_POST['physiological_vo2_test_value1']) == ''){
                            $error[] = array(
                                'message' => "Bike test value must not be blank."
                            );
                        }

                    }elseif($_POST['physiological_vo2_test_type'] == 'resting-hr'){
                        if(trim($_POST['physiological_vo2_test_value1']) == ''){
                            $error[] = array(
                                'message' => "Resting HR test value must not be blank."
                            );
                        }
                    }elseif($_POST['physiological_vo2_test_type'] == 'treadmill'){

                        if(trim($_POST['physiological_vo2_test_value1']) == '' || trim($_POST['physiological_vo2_test_value2']) == ''){
                            $error[] = array(
                                'message' => "Treadmill test value must not be blank."
                            );
                        }

                    }elseif($_POST['physiological_vo2_test_type'] == 'beep'){

                        if(trim($_POST['physiological_vo2_test_value1']) == '' || trim($_POST['physiological_vo2_test_value2']) == ''){
                            $error[] = array(
                                'message' => "Beep test value must not be blank."
                            );
                        }
                    }else{

                        if(trim($_POST['physiological_vo2_test_value1']) == ''){
                            $error[] = array(
                                'message' => "VO2 test value must not be blank."
                            );
                        }
                    }
                }
                
                if(trim($_POST['physiological_balance_type']) == ''){
                    $error[] = array(
                            'message' => "Please select Balance test type."
                        );
                }
                
                if(trim($_POST['physiological_balance_type']) != ''){
                    if(trim($_POST['physiological_balance_value']) == ''){
                        $error[] = array(
                                'message' => "Balance test value must not be blank."
                            );
                    }
                }
                
                if(trim($_POST['physiological_squat_value']) == ''){
                    $error[] = array(
                            'message' => "Squat test must not be blank."
                        );
                }
                
                if(trim($_POST['physiological_situp_value']) == ''){
                    $error[] = array(
                            'message' => "7 Stage Situp test must not be blank."
                        );
                }
                
                if(trim($_POST['biometric_waist_value']) == ''){
                    $error[] = array(
                            'message' => "Waist must not be blank."
                        );
                }
                
                if(trim($_POST['biometric_chest']) == ''){
                    $error[] = array(
                            'message' => "Chest must not be blank."
                        );
                }
                
                if(trim($_POST['biometric_abdomen']) == ''){
                    $error[] = array(
                            'message' => "Abdomen must not be blank."
                        );
                }
                
                if(trim($_POST['biometric_hip']) == ''){
                    $error[] = array(
                            'message' => "HIP must not be blank."
                        );
                }
                
                if(trim($_POST['biometric_thigh']) == ''){
                    $error[] = array(
                            'message' => "Thigh must not be blank."
                        );
                }
                
                if(trim($_POST['biometric_arm']) == ''){
                    $error[] = array(
                            'message' => "Arm must not be blank."
                        );
                }
                
                if(trim($_POST['biometric_body_fat_value']) == ''){
                    $error[] = array(
                            'message' => "Body fat must not be blank."
                        );
                }
                
                if(trim($_POST['biometric_blood_pressure_systolic']) == ''){
                    $error[] = array(
                            'message' => "Blood pressure (systolic) must not be blank."
                        );
                }
                
                if(trim($_POST['biometric_blood_pressure_diastolic']) == ''){
                    $error[] = array(
                            'message' => "Blood pressure (diastolic) must not be blank."
                        );
                }
                
                if(trim($_POST['biometric_blood_oxygen_value']) == ''){
                    $error[] = array(
                            'message' => "Blood oxygen must not be blank."
                        );
                }
            }
            
            // DEDUCT CREDIT
            if($_POST['submit_type'] != 'draft'){
                $club = $em->getRepository('AcmeHeadOfficeBundle:Club')
                            ->findOneBy(array(
                                'club_id' => $session->get('club_id'),
                            ));
                
                if( $club->getPaymentOption() != 'monthly' || ($club->getPaymentOption() == 'monthly' && $club->getMonthlyFeeStatus() != 'paid')){
                    $available_credit = $club->getAvailableCredit() - 1;

                    if($available_credit >= 0){
                        $club->setAvailableCredit($available_credit);
                        $em->persist($club);
                        $em->flush();

                        $session->set('available_credit', $available_credit);
                    }else{
                        $test_status = 'draft';

                        $error[] = array(
                                'message' => "Your club doesn't have enough credit! This test has been saved but has not been finalised."
                            );
                    }
                }
            }
            
            if(isset($_POST['client_test_id'])){
                $test = $em->getRepository('AcmeHeadOfficeBundle:ClientTest')
                            ->findOneBy(array(
                                'client_test_id' => $_POST['client_test_id'],
                            ));
                
                $app_type = 'update';
                if(count($test) == 0){
                    $test = new ClientTest();
                    $app_type = 'new';
                }
            }else{
                $test = new ClientTest();
                $app_type = 'new';
            }
            
            $current_test_status = $test->getStatus();
            
            $test->setClubClientId($_POST['club_client_id']);
            if($session->get('club_admin_id') != ''){
                $test->setClubAdminId($session->get('club_admin_id'));
            }
            $test->setEventId($_POST['event_id']);
            $test->setAge($club_client_details['age']);
            
            $healthy_life_expectancy_male = $this->getHealthyLifeExpectancies('male');
            $healthy_life_expectancy_female = $this->getHealthyLifeExpectancies('female');
            $male_max_score = $this->getHealthyLifeMaxWorstScore('male', 'max');
            $female_max_score = $this->getHealthyLifeMaxWorstScore('female', 'max');
            $male_worst_score = $this->getHealthyLifeMaxWorstScore('male', 'worst');
            $female_worst_score = $this->getHealthyLifeMaxWorstScore('female', 'worst');
            $test->setHealthyLifeExpectancyDalysFemale($healthy_life_expectancy_female['dalys']);
            $test->setHealthyLifeExpectancyDalysMale($healthy_life_expectancy_male['dalys']);
            $test->setHealthyLifeExpectancyFemale($healthy_life_expectancy_female['expectancy']);
            $test->setHealthyLifeExpectancyMale($healthy_life_expectancy_male['expectancy']);
            $test->setHealthyLifeExpectancyTotalFemale($healthy_life_expectancy_female['total_expectancy']);
            $test->setHealthyLifeExpectancyTotalMale($healthy_life_expectancy_male['total_expectancy']);
            $test->setHealthyLifeMaxScoreFemale($female_max_score['score']);
            $test->setHealthyLifeMaxScoreMale($male_max_score['score']);
            $test->setHealthyLifeWorstScoreFemale($female_worst_score['score']);
            $test->setHealthyLifeWorstScoreMale($male_worst_score['score']);
            
//            $test->setHeight($club_client_details['height']);
            if($_POST['height'] != ''){
                $test->setHeight($_POST['height']);
            }else{
                $test->setHeight(0);
            }
            if($_POST['weight'] != ''){
                $test->setWeight($_POST['weight']);
            }else{
                $test->setWeight(0);
            }
            
            $test->setPhysiologicalVo2TestType($_POST['physiological_vo2_test_type']);
            if(trim($_POST['physiological_vo2_test_value1']) != ''){
                $test->setPhysiologicalVo2TestValue1($_POST['physiological_vo2_test_value1']);
            }else{
                $test->setPhysiologicalVo2TestValue1(0);
            }
            
            if(trim($_POST['physiological_vo2_test_value2']) != ''){
                $test->setPhysiologicalVo2TestValue2($_POST['physiological_vo2_test_value2']);
            }else{
                $test->setPhysiologicalVo2TestValue2(0);
            }
            $test->setPhysiologicalBalanceType($_POST['physiological_balance_type']);
            if(trim($_POST['physiological_balance_value']) != ''){
                $test->setPhysiologicalBalanceValue($_POST['physiological_balance_value']);
            }else{
                $test->setPhysiologicalBalanceValue(0);
            }
            
            if(trim($_POST['physiological_squat_value']) != ''){
                $test->setPhysiologicalSquatValue($_POST['physiological_squat_value']);
            }else{
                $test->setPhysiologicalSquatValue(0);
            }
            if(trim($_POST['physiological_situp_value']) != ''){
                $test->setPhysiologicalSitupValue($_POST['physiological_situp_value']); 
            }else{
                $test->setPhysiologicalSitupValue(0); 
            }
            $test->setBiometricRestingHeartRate($_POST['biometric_resting_heart_rate']);
            
            if(trim($_POST['biometric_waist_value']) != ''){
                $test->setBiometricWaistValue($_POST['biometric_waist_value']);
            }else{
                $test->setBiometricWaistValue(0);
            }
            if(trim($_POST['biometric_chest']) != ''){
                $test->setBiometricChest($_POST['biometric_chest']);
            }else{
                $test->setBiometricChest(0);
            }
            if(trim($_POST['biometric_abdomen']) != ''){
                $test->setBiometricAbdomen($_POST['biometric_abdomen']);
            }else{
                $test->setBiometricAbdomen(0);
            }
            if(trim($_POST['biometric_hip']) != ''){
                $test->setBiometricHip($_POST['biometric_hip']);
            }else{
                $test->setBiometricHip(0);
            }
            if(trim($_POST['biometric_thigh']) != ''){
                $test->setBiometricThigh($_POST['biometric_thigh']);
            }else{
                $test->setBiometricThigh(0);
            }
            if(trim($_POST['biometric_arm']) != ''){
                $test->setBiometricArm($_POST['biometric_arm']);
            }else{
                $test->setBiometricArm(0);
            }
            if(trim($_POST['biometric_body_fat_value']) != ''){
                $test->setBiometricBodyFatValue($_POST['biometric_body_fat_value']);
            }else{
                $test->setBiometricBodyFatValue(0);
            }
            if(trim($_POST['biometric_blood_pressure_systolic']) != ''){
                $test->setBiometricBloodPressureSystolic($_POST['biometric_blood_pressure_systolic']);
            }else{
                $test->setBiometricBloodPressureSystolic(0);
            }
            if(trim($_POST['biometric_blood_pressure_diastolic']) != ''){
                $test->setBiometricBloodPressureDiastolic($_POST['biometric_blood_pressure_diastolic']);
            }else{
                $test->setBiometricBloodPressureDiastolic(0);
            }
            if(trim($_POST['biometric_blood_oxygen_value']) != ''){
                $test->setBiometricBloodOxygenValue($_POST['biometric_blood_oxygen_value']);
            }else{
                $test->setBiometricBloodOxygenValue(0);
            }
            
            if(trim($_POST['biometric_waist_value']) != '' && trim($_POST['biometric_hip']) != ''){
                $waistHipRatio = $formula->waistToHipRatio($_POST['biometric_waist_value'], $_POST['biometric_hip']);
            }else{
                $waistHipRatio = 0;
            }
            $test->setWaistHipRatio($waistHipRatio);
            $waistHipRatioScore = $formula->waistToHipRatioScore($club_client_details['gender'], $waistHipRatio);
            $test->setWaistHipRatioScore($waistHipRatioScore);
            
            // Results
                $bmi = $formula->bMI($_POST['height'], $_POST['weight']);
            $test->setBmi($bmi);
            
                $_POST['moderate_vigorous_pa'] = intval($_POST['moderate_vigorous_pa']);
                $_POST['muscle_strenthening_pa'] = intval($_POST['muscle_strenthening_pa']);
                $moderate_vigorous_pa = $formula->moderateOrVigorousIntensityPhysicalActivity($_POST['moderate_vigorous_pa']);
                $muscle_strenthening_pa = $formula->muscleStrentheningPhysicalActivity($_POST['muscle_strenthening_pa']);

                $physicalActivity = $formula->physicalActivity($moderate_vigorous_pa, $muscle_strenthening_pa);
            $test->setLifestylePhysicalActivity( $physicalActivity );
            
                $_POST['lifestyle_smoking'] = intval($_POST['lifestyle_smoking']);
                $smokingHabits = $formula->smokingHabits($club_client_details['gender'], $_POST['lifestyle_smoking']);
            $test->setLifestyleSmoking( $smokingHabits );
            
                $_POST['per_week_value'] = intval($_POST['per_week_value']);
                $_POST['one_sitting_value'] = intval($_POST['one_sitting_value']);
                $alcohol_consumption_per_week = $formula->alcoholConsumptionPerWeekResult($club_client_details['gender'], $_POST['per_week_value']);
                $alcohol_consumption_in_one_sitting = $formula->alcoholConsumptionInOneSittingResult($_POST['one_sitting_value']);

                $alcoholConsumptionTotal = $formula->alcoholConsumptionTotal($alcohol_consumption_per_week, $alcohol_consumption_in_one_sitting);
            $test->setLifestyleAlcohol( $alcoholConsumptionTotal );
            
                $number_of_yes = 0;
                if(isset($_POST['nutrition'])){
                    foreach ($_POST['nutrition'] as $lifestyle_question_id => $answer) {
                        $number_of_yes += intval($answer);
                    }
                }
                $nutrition = $formula->nutrition($number_of_yes);
            $test->setLifestyleNutrition( $nutrition );
                $mental_health_value = 0;
                if(isset($_POST['mental_health'])){
                    
                    foreach ($_POST['mental_health'] as $lifestyle_question_id => $answer) {
                        if($answer != ''){
                            $mental_health_value += intval($answer);
                        }
                    }
                    if($mental_health_value > 0){
                        $mentalHealth = $formula->mentalHealth($mental_health_value);

                    }else{
                        $mentalHealth = '';
                    }
                }else{
                    $mentalHealth = '';
                }
            $test->setLifestyleMentalHealth( $mentalHealth );
                $risk_profile_value = 0;
                if(isset($_POST['risk_profile'])){
                    
                    foreach ($_POST['risk_profile'] as $lifestyle_question_id => $answer) {
                        if($answer != ''){
                            $risk_profile_value += intval($answer);
                        }
                    }
                    if($risk_profile_value > 0){
                        //$response = $formula->mentalHealth($mental_health_value);
                        $riskProfile = $formula->riskProfile($risk_profile_value);

                    }else{
                        $riskProfile = '';
                    }
                }else{
                    $riskProfile = '';
                }
            $test->setLifestyleRiskProfile( $riskProfile );
            
                if($_POST['physiological_vo2_test_type'] == 'bike'){
                    $physiologicalVo2 = $formula->vo2BikeTest($club_client_details['gender'], $_POST['weight'], $club_client_details['age'], $_POST['physiological_vo2_test_value1']);

                }elseif($_POST['physiological_vo2_test_type'] == 'resting-hr'){

                    $physiologicalVo2 = $formula->vo2RestingHrTest($club_client_details['age'], $_POST['physiological_vo2_test_value1']);

                }elseif($_POST['physiological_vo2_test_type'] == 'treadmill'){

                    $physiologicalVo2 = $formula->vo2TreadmillTest($club_client_details['gender'], $_POST['weight'], $_POST['physiological_vo2_test_value1'], $_POST['physiological_vo2_test_value2']);

                }elseif($_POST['physiological_vo2_test_type'] == 'beep'){

                    $physiologicalVo2 = $formula->vo2BeepTest($_POST['physiological_vo2_test_value1'], $_POST['physiological_vo2_test_value2']);
                    
                }elseif($_POST['physiological_vo2_test_type'] == 'qc-step'){
                    
                    $physiologicalVo2 = $formula->vo2QcStepTest($club_client_details['gender'], $_POST['physiological_vo2_test_value1']);
                    
                }else{
                    
                    $physiologicalVo2 = $formula->vo2DirectEntry($_POST['physiological_vo2_test_value1']);
                }
            
            $test->setPhysiologicalVo2Score($physiologicalVo2);
                $physiologicalVo2_result = $formula->vo2($club_client_details['gender'], $club_client_details['age'], $physiologicalVo2);
            $test->setPhysiologicalVo2($physiologicalVo2_result);
            
                $_POST['physiological_balance_value'] = intval($_POST['physiological_balance_value']);
                $balance = $formula->balance($_POST['physiological_balance_type'], $club_client_details['gender'], $_POST['physiological_balance_value']);
            $test->setPhysiologicalBalance($balance);
            
                $smoking_habit_value = $this->getSpecificTestQuestionnaireClientAnswer(
                        3 /* smoking*/, 
                        $_POST['club_client_id'], 
                        $test->getClientTestId());

                $physiologicalSquat = $formula->squatTest(
                        $club_client_details['gender'], 
                        $smoking_habit_value, 
                        $_POST['physiological_squat_value']);
            
            $test->setPhysiologicalSquat($physiologicalSquat);
            
                $physiologicalSitup = $formula->sevenStateSitUpTest($_POST['physiological_situp_value']);
                
            $test->setPhysiologicalSitup($physiologicalSitup);
                $biometricWaist = $formula->waist($club_client_details['gender'], $_POST['biometric_waist_value']);
            $test->setBiometricWaist($biometricWaist);
                $biometricBodyFat = $formula->bodyFat($club_client_details['gender'], $_POST['biometric_body_fat_value']);
            $test->setBiometricBodyFat($biometricBodyFat);
                $biometricBloodPressure = $formula->bloodPressure($_POST['biometric_blood_pressure_systolic'], $_POST['biometric_blood_pressure_diastolic']);
            $test->setBiometricBloodPressure($biometricBloodPressure);
                $biometricBloodOxygen = $formula->bloodOxygen($_POST['biometric_blood_oxygen_value']);
            $test->setBiometricBloodOxygen($biometricBloodOxygen);
            
            
            $totalCrf = $formula->totalCRF(
                    $formula->crf( $physicalActivity ), 
                    $formula->crf( $smokingHabits ), 
                    $formula->crf( $alcoholConsumptionTotal ), 
                    $formula->crf( $nutrition ), 
                    $formula->crf( $mentalHealth ), 
                    $formula->crf( $riskProfile ), 
                    $formula->crf($biometricBodyFat), 
                    $formula->crf($biometricBloodPressure), 
                    $formula->crf($biometricBloodOxygen), 
                    $formula->crf($physiologicalVo2_result), 
                    $formula->crf($balance), 
                    $formula->crf($physiologicalSquat), 
                    $formula->crf($physiologicalSitup), 
                    $formula->crf($biometricWaist)
                );
            $test->setTotalCrf($totalCrf);
            
            $healthyLifePoints = $formula->healthyLifeYears(
                    $club_client_details['gender'], 
                    $physicalActivity, 
                    $alcoholConsumptionTotal, 
                    $nutrition, 
                    $smokingHabits, 
                    $mentalHealth, 
                    $riskProfile, 
                    $biometricBodyFat, 
                    $biometricBloodPressure, 
                    $biometricBloodOxygen, 
                    $physiologicalVo2_result, 
                    $balance, 
                    $physiologicalSquat, 
                    $physiologicalSitup, 
                    $biometricWaist, 
                    $totalCrf
                );
            
            
            $test->setHealthyLifePoints($healthyLifePoints);
            
            
            $estimatedHealthyLifeExpectancy = $formula->estimatedHealthyLifeExpectancy(
                    $club_client_details['gender'], 
                    $healthy_life_expectancy_male['expectancy'], 
                    $healthy_life_expectancy_female['expectancy'],
                    $healthyLifePoints);
            
            $test->setEstimatedHealthyLifeExpectancy($estimatedHealthyLifeExpectancy);
            
            $max_hle = $formula->maxHLE(
                    $club_client_details['gender'], 
                    $healthy_life_expectancy_male['expectancy'], 
                    $healthy_life_expectancy_female['expectancy'], 
                    $male_max_score['score'], 
                    $female_max_score['score']);
            
            $test->setMaxHle($max_hle);
            
            $healthyLifeScore = $formula->currentHealthyLifeScore(
                    $club_client_details['gender'], 
                    $estimatedHealthyLifeExpectancy, 
                    $max_hle
                );
            
            $test->setHealthyLifeScore($healthyLifeScore);
            
//                $healthyLifeScoreOutcome = $formula->outcomes($healthyLifeScore);
//            
//            $test->setHealthyLifeScoreOutcome($healthyLifeScoreOutcome);
            
                $estimatedYearsOfHealthyLifeLeft = $formula->estimatedYearsOfHealthyLifeLeft($estimatedHealthyLifeExpectancy, $club_client_details['age']);
            
            $test->setEstimatedYearsOfHealthyLifeLeft($estimatedYearsOfHealthyLifeLeft);
            
                $estimatedYearsYouWillLiveWithDid = $formula->estimatedNumberOfYearsYouWillLive(
                        $club_client_details['gender'], 
                        $estimatedHealthyLifeExpectancy, 
                        $healthy_life_expectancy_male['total_expectancy'], 
                        $healthy_life_expectancy_female['total_expectancy']
                    );
            $test->setEstimatedYearsYouWillLiveWithDid($estimatedYearsYouWillLiveWithDid);
            
                $estimatedYearsYouWillLiveWithDidAsOpposedTo = $formula->daly(
                        $club_client_details['gender'], 
                        $healthy_life_expectancy_male['dalys'],
                        $healthy_life_expectancy_female['dalys']
                    );
            $test->setEstimatedYearsYouWillLiveWithDidAsOpposedTo($estimatedYearsYouWillLiveWithDidAsOpposedTo);
            
            $youCouldAddUpTo = $formula->couldAddUpTo(
                    $club_client_details['gender'], 
                    $club_client_details['age'], 
                    $healthy_life_expectancy_male['total_expectancy'], 
                    $healthy_life_expectancy_female['total_expectancy'], 
//                    $healthy_life_expectancy_male['expectancy'], 
//                    $healthy_life_expectancy_female['expectancy'], 
                    $max_hle, 
                    $estimatedHealthyLifeExpectancy, 
                    $male_max_score['score'], 
                    $female_max_score['score']
                );
            
            $test->setYouCouldAddUpTo($youCouldAddUpTo);
            
                $estimatedCurrentHealthyLifeAge = $formula->estimatedCurrentHealthyLifeAge(
                        $club_client_details['age'], 
                        $healthyLifePoints, 
                        $max_hle
                    );
            $test->setEstimatedCurrentHealthyLifeAge($estimatedCurrentHealthyLifeAge);
            
            $first_test_details = $this->getClientFirstOrRecentTestDetails($_POST['club_client_id'], NULL, $client_test_id);
            
            
            if(isset($first_test_details['estimated_healthy_life_expectancy'])){
//                $yearsAdded = $formula->yearsAdded($first_test_details['estimated_current_healthy_life_age'], 
//                $estimatedCurrentHealthyLifeAge);
                
                $yearsAdded = $formula->yearsAdded($first_test_details['estimated_healthy_life_expectancy'], 
                                $estimatedHealthyLifeExpectancy);
                
            }else{
                $yearsAdded = '';
            }
            
            $test->setYearsAdded($yearsAdded);
            
            if(isset($first_test_details['biometric_waist_value'])){
                $cms_current_test = array(
                    $_POST['biometric_waist_value'],
                    $_POST['biometric_chest'],
                    $_POST['biometric_abdomen'],
                    $_POST['biometric_hip'],
                    $_POST['biometric_thigh'],
                    $_POST['biometric_arm']
                );

                $cms_1st_test = array(
                    $first_test_details['biometric_waist_value'],
                    $first_test_details['biometric_chest'],
                    $first_test_details['biometric_abdomen'],
                    $first_test_details['biometric_hip'],
                    $first_test_details['biometric_thigh'],
                    $first_test_details['biometric_arm']
                );
                $cmLost = $formula->cmLost($cms_1st_test, $cms_current_test);
                
                $biometricWaistValueLost = $formula->cmLost(
                        array($first_test_details['biometric_waist_value']), 
                        array($_POST['biometric_waist_value'])
                        );
                $biometricChestLost = $formula->cmLost(
                        array($first_test_details['biometric_chest']), 
                        array($_POST['biometric_chest'])
                        );
                $biometricThighLost = $formula->cmLost(
                        array($first_test_details['biometric_thigh']), 
                        array($_POST['biometric_thigh'])
                        );
                $biometricArmLost = $formula->cmLost(
                        array($first_test_details['biometric_arm']), 
                        array($_POST['biometric_arm'])
                        );
                
                $biometricAbdomenLost = $formula->cmLost(
                        array($first_test_details['biometric_abdomen']), 
                        array($_POST['biometric_abdomen'])
                        );
                $biometricHipLost = $formula->cmLost(
                        array($first_test_details['biometric_hip']), 
                        array($_POST['biometric_hip'])
                        );
            }else{
                $cmLost = '';
                $biometricWaistValueLost = '';
                $biometricChestLost = '';
                $biometricThighLost = '';
                $biometricArmLost = '';
                $biometricAbdomenLost ='';
                $biometricHipLost = '';
            }
            
            $test->setCmLost($cmLost);
            
            $test->setBiometricWaistValueLost($biometricWaistValueLost);
            $test->setBiometricChestLost($biometricChestLost);
            $test->setBiometricThighLost($biometricThighLost);
            $test->setBiometricArmLost($biometricArmLost);
            $test->setBiometricAbdomenLost($biometricAbdomenLost);
            $test->setBiometricHipLost($biometricHipLost);
            
            if(isset($first_test_details['weight'])){
                $kgLost = $formula->kgLost($first_test_details['weight'], $_POST['weight']);
            }else{
                $kgLost = '';
            }
            
            $test->setKgLost($kgLost);
            
            if($session->get('ho_admin_id') != ''){
                if( $test->getStatus() == 'finalised' && $test_status == 'draft'){
                    $test->setStatus('finalised');
                }else{
                    $test->setStatus($test_status);
                }
            }else{
                $test->setStatus($test_status);
            }
            $test->setTestDatetime($datetime->format('Y-m-d H:i:s'));
            if($test_status != 'draft'){
                $test->setTestDatetimeFinalised($datetime->format('Y-m-d H:i:s'));
            }
            
            if( count($first_test_details) > 0 ){
                    $bmiImprovement = $first_test_details['bmi'] - $bmi;
                $test->setBmiImprovement($bmiImprovement);
                    $bodyFatReduction = $first_test_details['biometric_body_fat_value'] - intval($_POST['biometric_body_fat_value']);
                $test->setBodyFatReduction($bodyFatReduction);
                    $vo2Improvement = $physiologicalVo2 - $first_test_details['physiological_vo2_score'];
                $test->setVo2Improvement($vo2Improvement);
                    $squatTestImprovement = intval($_POST['physiological_squat_value']) - $first_test_details['physiological_squat_value'];
                $test->setSquatTestImprovement($squatTestImprovement);
                    if(trim($_POST['physiological_balance_type']) == 'standing functional reach'){
                        $standingFunctionalReachImprovement = intval($_POST['physiological_balance_value']) - $first_test_details['physiological_balance_value'];
                    }else{
                        $standingFunctionalReachImprovement = 0;
                    }
                $test->setStandingFunctionalReachImprovement($standingFunctionalReachImprovement);
                    if(trim($_POST['physiological_balance_type']) == 'stork test'){
                        $storkTestImprovement = intval($_POST['physiological_balance_value']) - $first_test_details['physiological_balance_value'];
                    }else{
                        $storkTestImprovement = 0;
                    }
                $test->setStorkTestImprovement($storkTestImprovement);

                    $abdominalTestImprovement = $biometricAbdomenLost - $first_test_details['biometric_abdomen_lost'];
                $test->setAbdominalTestImprovement($abdominalTestImprovement);
                
                    $lsqca_current = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                                        ->findOneBy(array(
                                            'lq_id' => 1, 
                                            'club_client_id' => $_POST['club_client_id'],
                                            'client_test_id' => $test->getClientTestId()
                                        ));
                    
                    $lsqca_ftest = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                                        ->findOneBy(array(
                                            'lq_id' => 1, 
                                            'club_client_id' => $_POST['club_client_id'],
                                            'client_test_id' => $first_test_details['client_test_id']
                                        ));
                    
                    if(count($lsqca_current) > 0 && count($lsqca_ftest) > 0){
                        $vigorousPaPerWeek = $lsqca_current->getAnswer() - $lsqca_ftest->getAnswer();
                    }elseif(count($lsqca_current) == 0 && count($lsqca_ftest) > 0){
                        $vigorousPaPerWeek = 0 - $lsqca_ftest->getAnswer();
                    }else{
                        $vigorousPaPerWeek = 0;
                    }
                $test->setVigorousPaPerWeek($vigorousPaPerWeek);

                    $lsqca_current = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                                        ->findOneBy(array(
                                            'lq_id' => 2, 
                                            'club_client_id' => $_POST['club_client_id'],
                                            'client_test_id' => $test->getClientTestId()
                                        ));

                    $lsqca_ftest = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                                        ->findOneBy(array(
                                            'lq_id' => 2, 
                                            'club_client_id' => $_POST['club_client_id'],
                                            'client_test_id' => $first_test_details['client_test_id']
                                        ));
                    if(count($lsqca_current) > 0 && count($lsqca_ftest) > 0){
                        $strengthPaPerWeek = $lsqca_current->getAnswer() - $lsqca_ftest->getAnswer();
                    }elseif(count($lsqca_current) == 0 && count($lsqca_ftest) > 0){
                        $strengthPaPerWeek = 0 - $lsqca_ftest->getAnswer();
                    }else{
                        $strengthPaPerWeek = 0;
                    }
                $test->setStrengthPaPerWeek($strengthPaPerWeek);

                    $smokingImprovement = intval($smokingHabits) - $first_test_details['lifestyle_smoking'];
                $test->setSmokingImprovement($smokingImprovement);

                    $lsqca_current = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                                        ->findOneBy(array(
                                            'lq_id' => 4, 
                                            'club_client_id' => $_POST['club_client_id'],
                                            'client_test_id' => $test->getClientTestId()
                                        ));

                    $lsqca_ftest = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                                        ->findOneBy(array(
                                            'lq_id' => 4, 
                                            'club_client_id' => $_POST['club_client_id'],
                                            'client_test_id' => $first_test_details['client_test_id']
                                        ));
                    if(count($lsqca_current) > 0 && count($lsqca_ftest) > 0){
                        $standardAlcoholPerWeek = $lsqca_ftest->getAnswer() - $lsqca_current->getAnswer();
                    }elseif(count($lsqca_current) == 0 && count($lsqca_ftest) > 0){
                        $standardAlcoholPerWeek = $lsqca_ftest->getAnswer() - 0;
                    }else{
                        $standardAlcoholPerWeek = 0;
                    }
                $test->setStandardAlcoholPerWeek($standardAlcoholPerWeek);

                    $lsqca_current = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                                        ->findOneBy(array(
                                            'lq_id' => 5, 
                                            'club_client_id' => $_POST['club_client_id'],
                                            'client_test_id' => $test->getClientTestId()
                                        ));

                    $lsqca_ftest = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                                        ->findOneBy(array(
                                            'lq_id' => 5, 
                                            'club_client_id' => $_POST['club_client_id'],
                                            'client_test_id' => $first_test_details['client_test_id']
                                        ));
                    if(count($lsqca_current) > 0 && count($lsqca_ftest) > 0){
                        $maximumAlcoholOneSitting = $lsqca_ftest->getAnswer() - $lsqca_current->getAnswer();
                    }elseif(count($lsqca_current) == 0 && count($lsqca_ftest) > 0){
                        $maximumAlcoholOneSitting = $lsqca_ftest->getAnswer() - 0;
                    }else{
                        $maximumAlcoholOneSitting = 0;
                    }
                $test->setMaximumAlcoholOneSitting($maximumAlcoholOneSitting);


                    $nutrition_1st = $this->getLifestyleQuestionnaireClientAnswerByCode('nutrition', $first_test_details['client_test_id']);
                    $nutrition_1st_yeses = 0;
                    for($a=0; $a<count($nutrition_1st); $a++){
                        $nutrition_1st_yeses += ($nutrition_1st[$a]['answer'] == 1) ? 1 : 0;
                    }
                    $nutritionScoreImprovement = $number_of_yes - $nutrition_1st_yeses;
                    $nutritionScoreImprovement = $nutritionScoreImprovement * 10;
//                    $nutritionScoreImprovement = intval($nutrition) - $first_test_details['lifestyle_nutrition'];
                $test->setNutritionScoreImprovement($nutritionScoreImprovement);

                    $mental_health_1st = $this->getLifestyleQuestionnaireClientAnswerByCode('mental_health', $first_test_details['client_test_id']);
                    $mental_health_1st_total = 0;
                    for($a=0; $a<count($mental_health_1st); $a++){
                        $mental_health_1st_total += $mental_health_1st[$a]['answer'];
                    }
                    $mentalHealthImprovement = $mental_health_1st_total - intval($mental_health_value);
                    $mentalHealthImprovement = $mentalHealthImprovement * 2;
                $test->setMentalHealthImprovement($mentalHealthImprovement);
                
                    $risk_profile_1st = $this->getLifestyleQuestionnaireClientAnswerByCode('risk_profile', $first_test_details['client_test_id']);
                    $risk_profile_1st_total = 0;
                    for($a=0; $a<count($risk_profile_1st); $a++){
                        $risk_profile_1st_total += $risk_profile_1st[$a]['answer'];
                    }
                    $riskProfileImprovement = $risk_profile_1st_total - intval($risk_profile_value);
                    $riskProfileImprovement = $riskProfileImprovement * 1.42857143;
//                    $riskProfileImprovement = $first_test_details['lifestyle_risk_profile'] - intval($riskProfile);
                $test->setRiskProfileImprovement($riskProfileImprovement);
            }else{
                $test->setBmiImprovement(0);
                $test->setBodyFatReduction(0);
                $test->setVo2Improvement(0);
                $test->setSquatTestImprovement(0);
                $test->setStandingFunctionalReachImprovement(0);
                $test->setStorkTestImprovement(0);
                $test->setAbdominalTestImprovement(0);
                $test->setVigorousPaPerWeek(0);
                $test->setStrengthPaPerWeek(0);
                $test->setSmokingImprovement(0);
                $test->setStandardAlcoholPerWeek(0);
                $test->setMaximumAlcoholOneSitting(0);
                $test->setNutritionScoreImprovement(0);
                $test->setMentalHealthImprovement(0);
                $test->setRiskProfileImprovement(0);
            }
            
            
            $em->persist($test);
            $em->flush();
            // `lk: 19022016
            if(!isset($first_test_details['height'])){
                if($_POST['height'] != ''){
                    $client = $em->getRepository('AcmeHeadOfficeBundle:ClubClient')
                                ->findOneBy(array(
                                    'club_client_id' => $club_client_details['club_client_id']
                                ));
                    $client->setHeight($_POST['height']);
                    $em->persist($client);
                    $em->flush();
                }
            }
            
            
            if(isset($_POST['confirm_aia_test'])){
                $client = $em->getRepository('AcmeHeadOfficeBundle:ClubClient')
                            ->findOneBy(array(
                                'club_client_id' => $club_client_details['club_client_id']
                            ));
                $client->setSConfirmedAiaTest(1);
                $em->persist($client);
                $em->flush();
            }
            
            // SAVE SIGNUP QUESTIONNAIRE ANSWERS
//            $update_signup_questions = 0;
//            if($test->getTestType() == 'aia'){
//                if($club_client_details['s_aia_health_questions_done'] != 1){
//                    $update_signup_questions = 1;
//                }
//            }else{
//                if($club_client_details['s_signup_health_questions_done'] != 1){
//                    $update_signup_questions = 1;
//                }
//            }
            
            if($club_client_details['s_signup_questions_done'] != 1){
                $cust = $this->getDoctrine()->getEntityManager();
                $connection = $cust->getConnection();
                $statement = $connection->prepare("UPDATE tbl_signup_questions_client_answer a
                        LEFT JOIN tbl_signup_questions q ON q.`sq_id` = a.`sq_id`
                        SET a.`answer` = 0
                        WHERE a.`club_client_id` = :club_client_id 
                        AND (q.`type` = 'goal' OR q.type = 'experience' OR q.type = 'what-matters-most')");
                $statement->bindValue('club_client_id', $_POST['club_client_id']);
                $statement->execute();
                if(isset($_POST['goal'])){
                    foreach ($_POST['goal'] as $key => $value) {

                        $goal = $em->getRepository('AcmeHeadOfficeBundle:SignUpQuestionsClientAnswer')
                                ->findOneBy(array(
                                    'sq_id' => $key, 
                                    'club_client_id' => $_POST['club_client_id']
                                ));

                        if(count($goal) == 0){
                            $goal = new SignUpQuestionsClientAnswer();
                            $goal->setSqId( $key );
                            $goal->setClubClientId($_POST['club_client_id']);
                        }
                        $goal->setAnswer( $value );
                        $em->persist($goal);
                        $em->flush();
                    }
                }

                if(isset($_POST['experience'])){
                    foreach ($_POST['experience'] as $key => $value) {
                        $goal = $em->getRepository('AcmeHeadOfficeBundle:SignUpQuestionsClientAnswer')
                                ->findOneBy(array(
                                    'sq_id' => $key, 
                                    'club_client_id' => $_POST['club_client_id']
                                ));

                        if(count($goal) == 0){
                            $goal = new SignUpQuestionsClientAnswer();
                            $goal->setSqId( $key );
                            $goal->setClubClientId($_POST['club_client_id']);
                        }
                        $goal->setAnswer( $value );
                        $em->persist($goal);
                        $em->flush();
                    }
                }
                if(isset($_POST['wmm'])){
                    foreach ($_POST['wmm'] as $key => $value) {
                        $goal = $em->getRepository('AcmeHeadOfficeBundle:SignUpQuestionsClientAnswer')
                                ->findOneBy(array(
                                    'sq_id' => $key, 
                                    'club_client_id' => $_POST['club_client_id']
                                ));

                        if(count($goal) == 0){
                            $goal = new SignUpQuestionsClientAnswer();
                            $goal->setSqId( $key );
                            $goal->setClubClientId($_POST['club_client_id']);
                        }
                        $goal->setAnswer( $value );
                        $em->persist($goal);
                        $em->flush();
                    }
                }
                
            }
            
            if($club_client_details['s_signup_health_questions_done'] != 1){
                $cust = $this->getDoctrine()->getEntityManager();
                $connection = $cust->getConnection();
                $statement = $connection->prepare("UPDATE tbl_signup_questions_client_answer a
                        LEFT JOIN tbl_signup_questions q ON q.`sq_id` = a.`sq_id`
                        SET a.`answer` = 0
                        WHERE a.`club_client_id` = :club_client_id 
                        AND q.`type` = 'pre-exercise'");
                $statement->bindValue('club_client_id', $_POST['club_client_id']);
                $statement->execute();
                if(isset($_POST['pe'])){
                    foreach ($_POST['pe'] as $key => $value) {
                        $goal = $em->getRepository('AcmeHeadOfficeBundle:SignUpQuestionsClientAnswer')
                                ->findOneBy(array(
                                    'sq_id' => $key, 
                                    'club_client_id' => $_POST['club_client_id']
                                ));

                        if(count($goal) == 0){
                            $goal = new SignUpQuestionsClientAnswer();
                            $goal->setSqId( $key );
                            $goal->setClubClientId($_POST['club_client_id']);
                        }
                        $goal->setAnswer( $value );
                        $em->persist($goal);
                        $em->flush();
                    }
                }
            }
            
            if($club_client_details['s_aia_health_questions_done'] != 1){
                $cust = $this->getDoctrine()->getEntityManager();
                $connection = $cust->getConnection();
                $statement = $connection->prepare("UPDATE tbl_signup_questions_client_answer a
                        LEFT JOIN tbl_signup_questions q ON q.`sq_id` = a.`sq_id`
                        SET a.`answer` = 0
                        WHERE a.`club_client_id` = :club_client_id 
                        AND (q.`type` = 'aia-vitality-med-conditions' OR q.`type` = 'aia-vitality-med-history')");
                $statement->bindValue('club_client_id', $_POST['club_client_id']);
                $statement->execute();
                if(isset($_POST['aia_vitality_med_conditions'])){
                    foreach ($_POST['aia_vitality_med_conditions'] as $key => $value) {
                        $goal = $em->getRepository('AcmeHeadOfficeBundle:SignUpQuestionsClientAnswer')
                                ->findOneBy(array(
                                    'sq_id' => $key, 
                                    'club_client_id' => $_POST['club_client_id']
                                ));

                        if(count($goal) == 0){
                            $goal = new SignUpQuestionsClientAnswer();
                            $goal->setSqId( $key );
                            $goal->setClubClientId($_POST['club_client_id']);
                        }
                        $goal->setAnswer( $value );
                        $em->persist($goal);
                        $em->flush();
                    }
                }
                
                if(isset($_POST['aia_vitality_med_history'])){
                    foreach ($_POST['aia_vitality_med_history'] as $key => $value) {
                        $goal = $em->getRepository('AcmeHeadOfficeBundle:SignUpQuestionsClientAnswer')
                                ->findOneBy(array(
                                    'sq_id' => $key, 
                                    'club_client_id' => $_POST['club_client_id']
                                ));

                        if(count($goal) == 0){
                            $goal = new SignUpQuestionsClientAnswer();
                            $goal->setSqId( $key );
                            $goal->setClubClientId($_POST['club_client_id']);
                        }
                        $goal->setAnswer( $value );
                        $em->persist($goal);
                        $em->flush();
                    }
                }
            }
            
            
            
            // SAVE LIFESTYLE QUESTIONNAIRE ANSWERS
            if(isset($_POST['moderate_vigorous_pa'])){
                $_POST['moderate_vigorous_pa'] = intval($_POST['moderate_vigorous_pa']);
                $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                        ->findOneBy(array(
                            'lq_id' => 1, 
                            'club_client_id' => $_POST['club_client_id'],
                            'client_test_id' => $test->getClientTestId()
                        ));

                if(count($model) == 0){
                    $model = new LifestyleQuestionnareClientAnswer();
                    $model->setLqId(1);
                }

                $model->setStatus('saved');
                $model->setClubClientId($_POST['club_client_id']);
                $model->setClientTestId($test->getClientTestId());
                $model->setAnswer($_POST['moderate_vigorous_pa']);
                $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                $em->persist($model);
                $em->flush();
            }
            
            if(isset($_POST['muscle_strenthening_pa'])){
                $_POST['muscle_strenthening_pa'] = intval($_POST['muscle_strenthening_pa']);
                $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                        ->findOneBy(array(
                            'lq_id' => 2, 
                            'club_client_id' => $_POST['club_client_id'],
                            'client_test_id' => $test->getClientTestId()
                        ));

                if(count($model) == 0){
                    $model = new LifestyleQuestionnareClientAnswer();
                    $model->setLqId(2);
                }

                $model->setStatus('saved');
                $model->setClubClientId($_POST['club_client_id']);
                $model->setClientTestId($test->getClientTestId());
                $model->setAnswer($_POST['muscle_strenthening_pa']);
                $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                $em->persist($model);
                $em->flush();
            }
            
            if(isset($_POST['lifestyle_smoking'])){
                $_POST['lifestyle_smoking'] = intval($_POST['lifestyle_smoking']);
                $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                        ->findOneBy(array(
                            'lq_id' => 3, 
                            'club_client_id' => $_POST['club_client_id'],
                            'client_test_id' => $test->getClientTestId()
                        ));

                if(count($model) == 0){
                    $model = new LifestyleQuestionnareClientAnswer();
                    $model->setLqId(3);
                }

                $model->setStatus('saved');
                $model->setClubClientId($_POST['club_client_id']);
                $model->setClientTestId($test->getClientTestId());
                $model->setAnswer($_POST['lifestyle_smoking']);
                $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                $em->persist($model);
                $em->flush();
            }
            
            if(isset($_POST['per_week_value'])){
                $_POST['per_week_value'] = intval($_POST['per_week_value']);
                $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                        ->findOneBy(array(
                            'lq_id' => 4, 
                            'club_client_id' => $_POST['club_client_id'],
                            'client_test_id' => $test->getClientTestId()
                        ));

                if(count($model) == 0){
                    $model = new LifestyleQuestionnareClientAnswer();
                    $model->setLqId(4);
                }

                $model->setStatus('saved');
                $model->setClubClientId($_POST['club_client_id']);
                $model->setClientTestId($test->getClientTestId());
                $model->setAnswer($_POST['per_week_value']);
                $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                $em->persist($model);
                $em->flush();
            }
            
            if(isset($_POST['one_sitting_value'])){
                $_POST['one_sitting_value'] = intval($_POST['one_sitting_value']);
                $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                        ->findOneBy(array(
                            'lq_id' => 5, 
                            'club_client_id' => $_POST['club_client_id'],
                            'client_test_id' => $test->getClientTestId()
                        ));

                if(count($model) == 0){
                    $model = new LifestyleQuestionnareClientAnswer();
                    $model->setLqId(5);
                }

                $model->setStatus('saved');
                $model->setClubClientId($_POST['club_client_id']);
                $model->setClientTestId($test->getClientTestId());
                $model->setAnswer($_POST['one_sitting_value']);
                $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                $em->persist($model);
                $em->flush();
            }
            
            if(isset($_POST['nutrition'])){
                foreach ($_POST['nutrition'] as $lifestyle_question_id => $answer) {
                    $em = $this->getDoctrine()->getManager();

                    $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                            ->findOneBy(array(
                                'lq_id' => $lifestyle_question_id, 
                                'club_client_id' => $_POST['club_client_id'],
                                'client_test_id' => $test->getClientTestId()
                            ));

                    if(count($model) == 0){
                        $model = new LifestyleQuestionnareClientAnswer();
                        $model->setLqId($lifestyle_question_id);
                    }

                    $model->setStatus('saved');
                    $model->setClubClientId($_POST['club_client_id']);
                    $model->setClientTestId($test->getClientTestId());
                    $model->setAnswer($answer);
                    $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                    $em->persist($model);
                    $em->flush();
                }

            }
            
            if(isset($_POST['mental_health'])){
                foreach ($_POST['mental_health'] as $lifestyle_question_id => $answer) {
                    $em = $this->getDoctrine()->getManager();

                    $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                            ->findOneBy(array(
                                'lq_id' => $lifestyle_question_id, 
                                'club_client_id' => $_POST['club_client_id'],
                                'client_test_id' => $test->getClientTestId()
                            ));

                    if(count($model) == 0){
                        $model = new LifestyleQuestionnareClientAnswer();
                        $model->setLqId($lifestyle_question_id);
                    }

                    $model->setStatus('saved');
                    $model->setClubClientId($_POST['club_client_id']);
                    $model->setClientTestId($test->getClientTestId());
                    $model->setAnswer($answer);
                    $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                    $em->persist($model);
                    $em->flush();
                }

            }
            
            if(isset($_POST['risk_profile'])){
                foreach ($_POST['risk_profile'] as $lifestyle_question_id => $answer) {
                    $em = $this->getDoctrine()->getManager();

                    $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                            ->findOneBy(array(
                                'lq_id' => $lifestyle_question_id, 
                                'club_client_id' => $_POST['club_client_id'],
                                'client_test_id' => $test->getClientTestId()
                            ));

                    if(count($model) == 0){
                        $model = new LifestyleQuestionnareClientAnswer();
                        $model->setLqId($lifestyle_question_id);
                    }

                    $model->setStatus('saved');
                    $model->setClubClientId($_POST['club_client_id']);
                    $model->setClientTestId($test->getClientTestId());
                    $model->setAnswer($answer);
                    $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                    $em->persist($model);
                    $em->flush();
                }

            }
            
            
            
            $s_lifestyle_physical_activity_answer_complete = $this->sLifestyleTestQuestionnaireAnswersDone('physical_activity', $_POST['club_client_id'], $client_test_id);
            $s_lifestyle_smoking_answer_complete = $this->sLifestyleTestQuestionnaireAnswersDone('smoking', $_POST['club_client_id'], $client_test_id);
            $s_lifestyle_alcohol_answer_complete = $this->sLifestyleTestQuestionnaireAnswersDone('alcohol', $_POST['club_client_id'], $client_test_id);
            $s_lifestyle_nutrition_answer_complete = $this->sLifestyleTestQuestionnaireAnswersDone('nutrition', $_POST['club_client_id'], $client_test_id);
            $s_lifestyle_mental_health_answer_complete = $this->sLifestyleTestQuestionnaireAnswersDone('mental_health', $_POST['club_client_id'], $client_test_id);
            $s_lifestyle_risk_profile_answer_complete = $this->sLifestyleTestQuestionnaireAnswersDone('risk_profile', $_POST['club_client_id'], $client_test_id);
   
            if($test_status == 'finalised'){
                if(!$s_lifestyle_physical_activity_answer_complete){
                    $error[] = array(
                            'message' => "Please make sure to complete Physical Activity test."
                        );
                }
                
                if(!$s_lifestyle_smoking_answer_complete){
                    $error[] = array(
                            'message' => "Please make sure to complete Smoking Habit test."
                        );
                }
                
                if(!$s_lifestyle_alcohol_answer_complete){
                    $error[] = array(
                            'message' => "Please make sure to complete Alcohol Consumption test."
                        );
                }
                
                if(!$s_lifestyle_nutrition_answer_complete){
                    $error[] = array(
                            'message' => "Please make sure to complete Nutrition test."
                        );
                }
                
                if(!$s_lifestyle_mental_health_answer_complete){
                    $error[] = array(
                            'message' => "Please make sure to complete Mental Health test."
                        );
                }
                
                if(!$s_lifestyle_risk_profile_answer_complete){
                    $error[] = array(
                            'message' => "Please make sure to complete Risk Profile test."
                        );
                }
            }
            
            
            if(count($error) > 0){ 
                $this->get('session')->getFlashBag()->add(
                        'error',
                        $error
                    );
                
                $_POST['club_client_id'] = intval($_POST['club_client_id']);
                $post = array(
                    'club_client_id' => $_POST['club_client_id'],
                    'height' => $_POST['height'],
                    'weight' => $_POST['weight'],
                    'bmi' => $_POST['bmi'],
                    'lifestyle_physical_activity' => $physicalActivity,
                    'lifestyle_smoking' => $smokingHabits,
                    'lifestyle_alcohol' => $alcoholConsumptionTotal,
                    'lifestyle_nutrition' => $nutrition,
                    'lifestyle_mental_health' => $mentalHealth,
                    'lifestyle_risk_profile' => $riskProfile,
                    'physiological_vo2_test_type' => $_POST['physiological_vo2_test_type'],
                    'physiological_vo2_test_value1' => $_POST['physiological_vo2_test_value1'],
                    'physiological_vo2_test_value2' => $_POST['physiological_vo2_test_value2'],
                    'physiological_vo2' => $_POST['physiological_vo2'],
                    'physiological_balance_type' => $_POST['physiological_balance_type'],
                    'physiological_balance_value' => $_POST['physiological_balance_value'],
                    'physiological_balance' => $balance,
                    'physiological_squat_value' => $_POST['physiological_squat_value'],
                    'physiological_situp_value' => $_POST['physiological_situp_value'],
                    'biometric_resting_heart_rate' => $_POST['biometric_resting_heart_rate'],
                    'biometric_waist_value' => $_POST['biometric_waist_value'],
                    'biometric_chest' => $_POST['biometric_chest'],
                    'biometric_abdomen' => $_POST['biometric_abdomen'],
                    'biometric_hip' => $_POST['biometric_hip'],
                    'biometric_thigh' => $_POST['biometric_thigh'],
                    'biometric_arm' => $_POST['biometric_arm'],
                    'biometric_body_fat_value' => $_POST['biometric_body_fat_value'],
                    'biometric_blood_pressure_systolic' => $_POST['biometric_blood_pressure_systolic'],
                    'biometric_blood_pressure_diastolic' => $_POST['biometric_blood_pressure_diastolic'],
                    'biometric_blood_oxygen_value' => $_POST['biometric_blood_oxygen_value'],
                    'event_id' => $_POST['event_id'],
                    'waist_hip_ratio'=> $_POST['waist_hip_ratio'],
                    
                    'goal'=> (isset($_POST['goal'])) ? $_POST['goal'] : array(),
                    'wmm'=> (isset($_POST['wmm'])) ? $_POST['wmm'] : array(),
                    'experience'=> (isset($_POST['experience'])) ? $_POST['experience'] : array(),
                    'aia_vitality_med_conditions'=> (isset($_POST['aia_vitality_med_conditions'])) ? $_POST['aia_vitality_med_conditions'] : array(),
                    'aia_vitality_med_history'=> (isset($_POST['aia_vitality_med_history'])) ? $_POST['aia_vitality_med_history'] : array(),
                    'pe'=> (isset($_POST['pe'])) ? $_POST['pe'] : array(),
                    
                    'test_type'=>$test->getTestType()
                    
                    
                );
                
                $client_details = $this->getClubClientById($_POST['club_client_id']);

                if(isset($_POST['client_test_id'])){
                    $post['client_test_id'] = $_POST['client_test_id'];
                    
                    $_POST['client_test_id'] = intval($_POST['client_test_id']);
                }

                
                $_POST['client_test_id'] = (isset($_POST['client_test_id'])) ? $_POST['client_test_id'] : NULL;
                if($client_details['club_id'] != $session->get('club_id')){
                    return $this->redirect($this->generateUrl('acme_club_login'));
                }
				
                return $this->render('AcmeClubBundle:Test:create_test.html.twig',
                        array(
                            'client_details' => $client_details,
                            'events' => $this->getEvents($session->get('club_id')),
                            'post' => $post,
                            'physical_activity_questions' => $this->getTestQuestionnaire('physical_activity', $_POST['club_client_id'], $test->getClientTestId()),
                            'alcohol_questions' => $this->getTestQuestionnaire('alcohol', $_POST['club_client_id'], $test->getClientTestId()),
                            'nutrition_questions' => $this->getTestQuestionnaire('nutrition', $_POST['club_client_id'], $test->getClientTestId()),
                            'mental_health_questions' => $this->getTestQuestionnaire('mental_health', $_POST['club_client_id'], $test->getClientTestId()),
                            'mental_health_options' => $this->getTestQuestionnaireOptionsByCode('mental_health'),
                            'risk_profile_questions' => $this->getTestQuestionnaire('risk_profile', $_POST['club_client_id'], $test->getClientTestId()),
                            'smoking_options' => $this->getTestQuestionnaireOptionsByCode('smoking'),
                            'smoking_answer' => $this->getSpecificTestQuestionnaireClientAnswer(3 /*smoking*/, $_POST['club_client_id'], $test->getClientTestId()),
                            
                            'questions_goal'=> $this->getSignUpQuestions('goal', $_POST['club_client_id']),
                            'questions_experience'=> $this->getSignUpQuestions('experience', $_POST['club_client_id']),
                            'questions_wmm'=> $this->getSignUpQuestions('what-matters-most', $_POST['club_client_id']),
                            'questions_pe'=> $this->getSignUpQuestions('pre-exercise', $_POST['club_client_id']),

                            'questions_aia_vitality_med_conditions'=> $this->getSignUpQuestions('aia-vitality-med-conditions', $_POST['club_client_id']),
                            'questions_aia_vitality_med_history'=> $this->getSignUpQuestions('aia-vitality-med-history', $_POST['club_client_id']),
//                            's_lifestyle_physical_activity_answer_complete' => $s_lifestyle_physical_activity_answer_complete,
//                            's_lifestyle_smoking_answer_complete' => $s_lifestyle_smoking_answer_complete,
//                            's_lifestyle_alcohol_answer_complete' => $s_lifestyle_alcohol_answer_complete,
//                            's_lifestyle_nutrition_answer_complete' => $s_lifestyle_nutrition_answer_complete,
//                            's_lifestyle_mental_health_answer_complete' => $s_lifestyle_mental_health_answer_complete,
//                            's_lifestyle_risk_profile_answer_complete' => $s_lifestyle_risk_profile_answer_complete,
                        ));
                
            }else{
                
                if($test_status == 'draft'){
//                    $this->get('session')->getFlashBag()->add(
//                            'success',
//                            'Test has been saved as draft!'
//                        );
                }else{
                    
                        if($current_test_status != 'finalised'){
                            $client = $em->getRepository('AcmeHeadOfficeBundle:ClubClient')
                                        ->findOneBy(array(
                                            'club_client_id' => $club_client_details['club_client_id']
                                        ));
                            $client->setSsignupQuestionsDone(1);
                            if($test->getTestType() == 'aia'){
                                $client->setSAiaHealthQuestionsDone(1);
                            }else{
                                $client->setSSignupHealthQuestionsDone(1);
                            }

                            $em->persist($client);
                            $em->flush();

        //                    $this->get('session')->getFlashBag()->add(
        //                            'success',
        //                            'Test has been finalised!'
        //                        );
                            $from = $this->container->getParameter('site_email_address');
                            $subject = $club_client_details['fname'] . ' ' . $club_client_details['lname'] . ' -  Test Results PDF';
                            $body = $this->renderView('AcmeClubBundle:Test:email_test_personal_content.html.twig',
                                            array(
                                                'name' => $club_client_details['fname'],
                                                'test_date' =>  $test->getTestDatetimeFinalised()
                                            ));


                            $recipients = array();
                            $recipients[ $club_client_details['email'] ] = $club_client_details['fname'] . ' ' . $club_client_details['lname'];


                            if(count($recipients) > 0){
                                //$this->sendEmail($recipients, '', $from, $subject, $body, $attachments);

                                $ref = array(
                                    'client_test_id' => $test->getClientTestId(),
                                    'type'=> 'premium-test'
                                    );
                                $emailQueue = new EmailQueue();
                                $emailQueue->setSender( $from );
                                $emailQueue->setSubject( $subject );
                                $emailQueue->setBody( $body );
                                $emailQueue->setRecipients( json_encode($recipients) );
                                $emailQueue->setReference( json_encode($ref) );
                                $emailQueue->setSSent(0);
                                $emailQueue->setSTest(1);
                                $em->persist($emailQueue);
                                $em->flush();
                            }
                        }
                    }



                    if($current_test_status != 'finalised'){
                        // SET ACTIVITY
                        if($app_type == 'new'){
                            if($test_status == 'draft'){
                                $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " created a test for " . $club_client_details['fname'] . " " . $club_client_details['lname'] . " and saved as draft.";
                            }else{
                                $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " created a test for " . $club_client_details['fname'] . " " . $club_client_details['lname'] . " and finalised.";
                            }
                        }else{
                            if($test_status == 'draft'){
                                $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " updated the test for " . $club_client_details['fname'] . " " . $club_client_details['lname'] . " and saved as draft.";
                            }else{
                                $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " updated the test for " . $club_client_details['fname'] . " " . $club_client_details['lname'] . " and finalised.";
                            }
                        }

                        if($session->get('club_admin_id') != ''){ 
                            $this->setActivity($session->get('club_admin_id'), 'club-admin', $details);
                        }else{
                            $this->setActivity($session->get('ho_admin_id'), 'head-office-admin', $details);
                        }

                        $em->getConnection()->commit(); 

                        $MailChimp = new \Acme\HeadOfficeBundle\Model\MailChimp($this->container->getParameter('MailChimp_API_ID'));

                        $subs = $MailChimp->call('lists/subscribe', array(
                        'id'                => $this->container->getParameter('MailChimp_List_premiumtest'),
                        'email'             => array('email'=>$club_client_details['email']),
                        'merge_vars'        => array('FNAME'=>$club_client_details['fname'], 'LNAME'=>$club_client_details['lname']),
                        'double_optin'      => false,
                        'update_existing'   => true,
                        'replace_interests' => false,
                        'send_welcome'      => false,
                        ));
                    }else{
                        if($session->get('ho_admin_id') != ''){
                            $em->getConnection()->commit(); 
                        }
                    }
                    if($test_status == 'draft' && ($session->get('ho_admin_id') == '' && $test->getStatus() == 'draft')){
                        if(isset($_POST['ajax'])){
                            return new Response("success");
                        }else{
                            if($test->getTestType() == 'aia'){
                                return $this->redirect($this->generateUrl('acme_club_create_test') . '?id=' . $_POST['club_client_id'] . '&test=' . $test->getClientTestId() . '&aia=1');
                            }else{
                                return $this->redirect($this->generateUrl('acme_club_create_test') . '?id=' . $_POST['club_client_id'] . '&test=' . $test->getClientTestId());
                            }
                        }


                    }elseif($test_status == 'draft' && ($session->get('ho_admin_id') != '' && $test->getStatus() == 'draft')){
                        if(isset($_POST['ajax'])){
                            return new Response("success");
                        }else{
                            if($test->getTestType() == 'aia'){
                                return $this->redirect($this->generateUrl('acme_club_create_test') . '?id=' . $_POST['club_client_id'] . '&test=' . $test->getClientTestId() . '&aia=1');
                            }else{
                                return $this->redirect($this->generateUrl('acme_club_create_test') . '?id=' . $_POST['club_client_id'] . '&test=' . $test->getClientTestId());
                            }
                        }

                    }else{
                        
                        // get program client here
                        $program_client = $this->get12weekProgramClientByEmail( $club_client_details['email'] );
                        if( count($program_client) > 0 ){
                            if($_SERVER['SERVER_NAME'] == 'localhost'){
                                $program_host="localhost";
                                $program_uname="root";
                                $program_pass="";
                                $program_database = "healthy_life_project_ohwp";

                            }else{
                                $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
                                $program_uname="hlproot";
                                $program_pass="_jWBqa)67?BJq(j-";
                                $program_database = "hlp12weekProgram";
                            }
                            $program_connection=mysqli_connect($program_host,$program_uname,$program_pass, $program_database) or die("Database Connection Failed");

                            // DELETE EXISTING
                            $program_client_test_id = $this->get12weekProgramClientTestIdByFullTestId($test->getClientTestId());

                            // `lk: 19022016
                            if(!isset($first_test_details['height'])){
    //                            if($program_client['height'] == '' || $program_client['height'] <= 0){
                                    mysqli_query($program_connection,
                                    "UPDATE tbl_client SET height=".$test->getHeight()." WHERE email='".$club_client_details['email']."'");
    //                            }
                            }
                            mysqli_query($program_connection,
                            "DELETE FROM tbl_lifestyle_questionnaire_client_answer WHERE client_test_id=" . $program_client_test_id);

                            mysqli_query($program_connection,
                            "DELETE FROM tbl_client_test WHERE full_test_id=" . $test->getClientTestId());

                            // NEW RECORD
                            mysqli_query($program_connection,
                            "INSERT INTO tbl_client_test 
                                (full_test_id,
                                client_id, 
                                test_datetime, 
                                test_datetime_finalised,
                                age,
                                healthy_life_expectancy_male,
                                healthy_life_expectancy_female,
                                healthy_life_expectancy_total_male,
                                healthy_life_expectancy_total_female,
                                healthy_life_expectancy_dalys_male,
                                healthy_life_expectancy_dalys_female,
                                healthy_life_max_score_male,
                                healthy_life_max_score_female,
                                healthy_life_worst_score_male,
                                healthy_life_worst_score_female,
                                height,
                                weight,
                                bmi,
                                lifestyle_physical_activity_q1,
                                lifestyle_physical_activity_q2,
                                lifestyle_physical_activity,
                                lifestyle_smoking,
                                lifestyle_alcohol_q1,
                                lifestyle_alcohol_q2,
                                lifestyle_alcohol,
                                lifestyle_nutrition,
                                lifestyle_mental_health,
                                lifestyle_risk_profile,
                                physiological_vo2_test_type,
                                physiological_vo2_test_value1,
                                physiological_vo2_test_value2,
                                physiological_vo2_score,
                                physiological_vo2,
                                physiological_balance_type,
                                physiological_balance_value,
                                physiological_balance,
                                physiological_squat_value,
                                physiological_squat,
                                physiological_situp_value,
                                physiological_situp,
                                biometric_waist_value,
                                biometric_waist_value_lost,
                                biometric_waist,
                                biometric_chest_lost,
                                biometric_chest,
                                biometric_abdomen,
                                biometric_abdomen_lost,
                                biometric_hip,
                                biometric_hip_lost,
                                biometric_thigh,
                                biometric_thigh_lost,
                                biometric_arm,
                                biometric_arm_lost,
                                biometric_body_fat_value,
                                biometric_body_fat,
                                biometric_blood_pressure_systolic,
                                biometric_blood_pressure_diastolic,
                                biometric_blood_pressure,
                                biometric_blood_oxygen_value,
                                biometric_blood_oxygen,
                                total_crf,
                                healthy_life_points,
                                estimated_healthy_life_expectancy,
                                max_hle,
                                healthy_life_score,
                                estimated_years_of_healthy_life_left,
                                estimated_years_you_will_live_with_did,
                                estimated_years_you_will_live_with_did_as_opposed_to,
                                you_could_add_up_to,
                                estimated_current_healthy_life_age,
                                years_added,
                                cm_lost,
                                kg_lost,
                                waist_hip_ratio,
                                waist_hip_ratio_score,
                                `status`) 

                                VALUES

                                (". $test->getClientTestId() .",
                                ". $program_client['client_id'] .", 
                                '". $test->getTestDatetime() ."', 
                                '". $test->getTestDatetimeFinalised() ."',
                                '". $test->getAge() ."',
                                '". $test->getHealthyLifeExpectancyMale() ."',
                                '". $test->getHealthyLifeExpectancyFeMale() ."',
                                '". $test->getHealthyLifeExpectancyTotalMale() ."',
                                '". $test->getHealthyLifeExpectancyTotalFemale() ."',
                                '". $test->getHealthyLifeExpectancyDalysMale() ."',
                                '". $test->getHealthyLifeExpectancyDalysFemale() ."',
                                '". $test->getHealthyLifeMaxScoreMale() ."',
                                '". $test->getHealthyLifeMaxScoreFemale() ."',
                                '". $test->getHealthyLifeWorstScoreMale() ."',
                                '". $test->getHealthyLifeWorstScoreFemale() ."',
                                '". $test->getHeight() ."',
                                '". $test->getWeight() ."',
                                '". $test->getBmi() ."',
                                '". $test->getLifestylePhysicalActivityQ1() ."',
                                '". $test->getLifestylePhysicalActivityQ2() ."',
                                '". $test->getLifestylePhysicalActivity() ."',
                                '". $test->getLifestyleSmoking() ."',
                                '". $test->getLifestyleAlcoholQ1() ."',
                                '". $test->getLifestyleAlcoholQ2() ."',
                                '". $test->getLifestyleAlcohol() ."',
                                '". $test->getLifestyleNutrition() ."',
                                '". $test->getLifestyleMentalHealth() ."',
                                '". $test->getLifestyleRiskProfile() ."',
                                '". $test->getPhysiologicalVo2TestType() ."',
                                '". $test->getPhysiologicalVo2TestValue1() ."',
                                '". $test->getPhysiologicalVo2TestValue2() ."',
                                '". $test->getPhysiologicalVo2Score() ."',
                                '". $test->getPhysiologicalVo2() ."',
                                '". $test->getPhysiologicalBalanceType() ."',
                                '". $test->getPhysiologicalBalanceValue() ."',
                                '". $test->getPhysiologicalBalance() ."',
                                '". $test->getPhysiologicalSquatValue() ."',
                                '". $test->getPhysiologicalSquat() ."',
                                '". $test->getPhysiologicalSitupValue() ."',
                                '". $test->getPhysiologicalSitup() ."',
                                '". $test->getBiometricWaistValue() ."',
                                '". $test->getBiometricWaistValueLost() ."',
                                '". $test->getBiometricWaist() ."',
                                '". $test->getBiometricChestLost() ."',
                                '". $test->getBiometricChest() ."',
                                '". $test->getBiometricAbdomen() ."',
                                '". $test->getBiometricAbdomenLost() ."',
                                '". $test->getBiometricHip() ."',
                                '". $test->getBiometricHipLost() ."',
                                '". $test->getBiometricThigh() ."',
                                '". $test->getBiometricThighLost() ."',
                                '". $test->getBiometricArm() ."',
                                '". $test->getBiometricArmLost() ."',
                                '". $test->getBiometricBodyFatValue() ."',
                                '". $test->getBiometricBodyFat() ."',
                                '". $test->getBiometricBloodPressureSystolic() ."',
                                '". $test->getBiometricBloodPressureDiastolic() ."',
                                '". $test->getBiometricBloodPressure() ."',
                                '". $test->getBiometricBloodOxygenValue() ."',
                                '". $test->getBiometricBloodOxygen() ."',
                                '". $test->getTotalCrf() ."',
                                '". $test->getHealthyLifePoints() ."',
                                '". $test->getEstimatedHealthyLifeExpectancy() ."',
                                '". $test->getMaxHle() ."',
                                '". $test->getHealthyLifeScore() ."',
                                '". $test->getEstimatedYearsOfHealthyLifeLeft() ."',
                                '". $test->getEstimatedYearsYouWillLiveWithDid() ."',
                                '". $test->getEstimatedYearsYouWillLiveWithDidAsOpposedTo() ."',
                                '". $test->getYouCouldAddUpTo() ."',
                                '". $test->getEstimatedCurrentHealthyLifeAge() ."',
                                '". $test->getYearsAdded() ."',
                                '". $test->getCmLost() ."',
                                '". $test->getKgLost() ."',
                                '". $test->getWaistHipRatio() ."',
                                '". $test->getWaistHipRatioScore() ."',
                                'finalised')");


                            $lqc_answers = $this->getLifestyleQuestionnaireClientAnswer($test->getClientTestId());
                            $program_client_test_id = $this->get12weekProgramClientTestIdByFullTestId($test->getClientTestId());
                            for($i=0; $i<count($lqc_answers); $i++){
                                mysqli_query($program_connection,"
                                    INSERT INTO tbl_lifestyle_questionnaire_client_answer
                                        (lq_id,
                                        client_test_id,
                                        client_id,
                                        answer,
                                        answer_datetime,
                                        `status`)
                                    VALUES
                                        ('". $lqc_answers[$i]['lq_id'] ."',
                                        '". $program_client_test_id ."',
                                        '". $program_client['client_id'] ."',
                                        '". $lqc_answers[$i]['answer'] ."',
                                        '". $lqc_answers[$i]['answer_datetime'] ."',
                                        'saved'
                                            )
                                    ");
                            }
                            mysqli_query($program_connection,
                                "UPDATE tbl_client SET s_opening_message_done = 1, s_lite_test_complete = 1 WHERE client_id=". $program_client['client_id']);

                        

                        if($test->getTestType() == 'aia'){
                            return $this->redirect($this->generateUrl('acme_club_view_test') . '?id=' .  $test->getClientTestId() . '&aia=1');
                        }else{
                            return $this->redirect($this->generateUrl('acme_club_view_test') . '?id=' .  $test->getClientTestId());
                        }

                    }else{
                        if($test->getTestType() == 'aia'){
                            return $this->redirect($this->generateUrl('acme_club_view_test') . '?id=' .  $test->getClientTestId() . '&aia=1');
                        }else{
                            return $this->redirect($this->generateUrl('acme_club_view_test') . '?id=' .  $test->getClientTestId());
                        }
                    }
                }
                    
            }
            
            
            
            
        }else{ 
        
            if(!isset($_GET['id'])){
                return $this->redirect($this->generateUrl('acme_club_login'));
            }
            
            $_GET['id'] = intval($_GET['id']);
            $post = array();
            
            $client_details = $this->getClubClientById($_GET['id']);
            $club_admin_details = ($session->get('club_admin_id') != '') ? $this->getClubAdminUserById($session->get('club_admin_id')) : array();
            if(isset($_GET['test'])){
                $_GET['test'] = intval($_GET['test']);
                $post = $this->getClientTestDetailsById($_GET['test']);
                
                if(!isset($post['status'])){
                    return $this->redirect($this->generateUrl('acme_club_view_test') . '?id=' . $_GET['test']);
                }
                
                if($post['status'] == 'finalised'){
//                    return $this->redirect($this->generateUrl('acme_club_view_test') . '?id=' . $_GET['test']);
                }
                if($session->get('club_admin_id') != '' && $session->get('user_role') != 'admin'){
                    $em = $this->getDoctrine()->getManager();
                    $drafttest = $em->getRepository('AcmeHeadOfficeBundle:ClientTest')->findOneBy(
                            array(
                                'client_test_id'=>$_GET['test']
                            )
                        );
                    if(count($drafttest) > 0 && $drafttest->getStatus() != 'finalised'){
                        if(isset($_GET['aia']) && (isset($club_admin_details['s_aia_approved']) && $club_admin_details['s_aia_approved'] == 1)){
                            $drafttest->setTestType('aia');
                            $post['test_type'] = 'aia';
                        }else{
                            $drafttest->setTestType('normal');
                            $post['test_type'] = 'normal';
                        }
                        $em->persist($drafttest);
                        $em->flush();
                    }
                }
            }
            
            $_GET['test'] = (isset($_GET['test'])) ? $_GET['test'] : NULL;
            if($client_details['club_id'] != $session->get('club_id')){ 
                return $this->redirect($this->generateUrl('acme_club_login'));
            }
            
            if(!isset($_GET['test'])){
                
                $draft_tests = $this->getClientTestHistory( $_GET['id'], NULL, 'draft', NULL, $_GET['test'] );

                if(count($draft_tests) > 0){
                        return $this->render('AcmeClubBundle:Test:view_error.html.twig',
                                array('error'=> 
                                        array(
                                                'title'=> 'Error',
                                                'message'=> 'Sorry you already have a draft test underway - please complete it, or remove it before starting a new one.',
                                                'type'=> 'continue-draft',
                                                
                                                ),
                                        'draft_test_id' => $draft_tests[0]['client_test_id'],
                                        's_aia' => (isset($_GET['aia'])) ? 1 : 0
                                        )
                                    
                        );
                }else{
                    $em = $this->getDoctrine()->getManager();
                    $drafttest = new ClientTest();
                    $drafttest->setClubClientId($_GET['id']);
                    if($session->get('club_admin_id') != ''){
                        $drafttest->setClubAdminId($session->get('club_admin_id'));
                    }
                    if(isset($client_details['height']) && $client_details['height'] != '' && $client_details['height'] > 0){
                        $drafttest->setHeight($client_details['height']);
                    }
                    $drafttest->setStatus('draft');
                    $drafttest->setTestDatetime($datetime->format('Y-m-d H:i:s'));
                    if($session->get('club_admin_id') != '' && $session->get('user_role') != 'admin'){
                        if($drafttest->getStatus() != 'finalised'){
                            if(isset($_GET['aia']) && (isset($club_admin_details['s_aia_approved']) && $club_admin_details['s_aia_approved'] == 1)){
                                $drafttest->setTestType('aia');
                            }else{
                                $drafttest->setTestType('normal');
                            }
                        }
                    }
                    $em->persist($drafttest);
                    $em->flush();
                    
                    if(isset($_GET['aia'])){
                        return $this->redirect($this->generateUrl('acme_club_create_test') . '?id=' . $_GET['id'] . '&test=' . $drafttest->getClientTestId() . '&aia=1');
                    }else{
                        return $this->redirect($this->generateUrl('acme_club_create_test') . '?id=' . $_GET['id'] . '&test=' . $drafttest->getClientTestId());
                    }
                    
                }
                
                
            }
			
            return $this->render('AcmeClubBundle:Test:create_test.html.twig',
                    array(
                        'client_details' => $client_details,
                        'events' => $this->getEvents($session->get('club_id')),
                        'post' => $post,
                        'physical_activity_questions' => $this->getTestQuestionnaire('physical_activity', $_GET['id'], $_GET['test']),
                        'alcohol_questions' => $this->getTestQuestionnaire('alcohol', $_GET['id'], $_GET['test']),
                        'nutrition_questions' => $this->getTestQuestionnaire('nutrition', $_GET['id'], $_GET['test']),
                        'mental_health_questions' => $this->getTestQuestionnaire('mental_health', $_GET['id'], $_GET['test']),
                        'mental_health_options' => $this->getTestQuestionnaireOptionsByCode('mental_health'),
                        'risk_profile_questions' => $this->getTestQuestionnaire('risk_profile', $_GET['id'], $_GET['test']),
                        'smoking_options' => $this->getTestQuestionnaireOptionsByCode('smoking'),
                        'smoking_answer' => $this->getSpecificTestQuestionnaireClientAnswer(3 /*smoking*/, $_GET['id'], $_GET['test']),
                        
                        'questions_goal'=> $this->getSignUpQuestions('goal', $_GET['id']),
                        'questions_experience'=> $this->getSignUpQuestions('experience', $_GET['id']),
                        'questions_wmm'=> $this->getSignUpQuestions('what-matters-most', $_GET['id']),
                        'questions_pe'=> $this->getSignUpQuestions('pre-exercise', $_GET['id']),
                        'questions_aia_vitality_med_conditions'=> $this->getSignUpQuestions('aia-vitality-med-conditions', $_GET['id']),
                        'questions_aia_vitality_med_history'=> $this->getSignUpQuestions('aia-vitality-med-history', $_GET['id']),
//                        's_lifestyle_physical_activity_answer_complete' => $this->sLifestyleTestQuestionnaireAnswersDone('physical_activity', $_GET['id'], $_GET['test']),
//                        's_lifestyle_smoking_answer_complete' => $this->sLifestyleTestQuestionnaireAnswersDone('smoking', $_GET['id'], $_GET['test']),
//                        's_lifestyle_alcohol_answer_complete' => $this->sLifestyleTestQuestionnaireAnswersDone('alcohol', $_GET['id'], $_GET['test']),
//                        's_lifestyle_nutrition_answer_complete' => $this->sLifestyleTestQuestionnaireAnswersDone('nutrition', $_GET['id'], $_GET['test']),
//                        's_lifestyle_mental_health_answer_complete' => $this->sLifestyleTestQuestionnaireAnswersDone('mental_health', $_GET['id'], $_GET['test']),
//                        's_lifestyle_risk_profile_answer_complete' => $this->sLifestyleTestQuestionnaireAnswersDone('risk_profile', $_GET['id'], $_GET['test']),
                    ));
        }
    }
    
    
    
    // GET LIFESTYLE QUESTIONNAIRE TEMPLATE
    public function testQuestionnaireTemplateAction()
    {
        $session = $this->getRequest()->getSession();
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            //return $this->redirect($this->generateUrl('acme_club_login'));
            return new Response("session expired");
        }
        
        if(!isset($_POST['code'])){
            //return $this->redirect($this->generateUrl('acme_club_login'));
            return new Response("invalid parameter");
        }
        
        if(!isset($_POST['club_client_id'])){
            //return $this->redirect($this->generateUrl('acme_club_login'));
            return new Response("invalid parameter");
        }
        
        $_POST['code'] = trim($_POST['code']);
        $_POST['club_client_id'] = intval($_POST['club_client_id']);
        $_POST['client_test_id'] = (isset($_POST['client_test_id']) && $_POST['client_test_id'] != '') ? intval($_POST['client_test_id']) : NULL; 
        
        $lifestyle_codes = array("physical_activity", "smoking", "alcohol", "nutrition", "mental_health", "risk_profile");

        if (!in_array($_POST['code'], $lifestyle_codes)){
          return new Response("invalid parameter");
        }
        
        $smoking_answer = ($_POST['code'] == 'smoking') ? $this->getSpecificTestQuestionnaireClientAnswer(3 /*smoking*/, $_POST['club_client_id'], $_POST['client_test_id']) : '';
        
        
        
        return new Response($this->renderView(
            'AcmeClubBundle:Test:test_questionnaire_template_'. $_POST['code'] .'.html.twig',
                array(
                    'client_test_id' => $_POST['client_test_id'],
                    'club_client_id' => $_POST['club_client_id'],
                    'gender' => $_POST['gender'],
                    'questions' => $this->getTestQuestionnaire($_POST['code'], $_POST['club_client_id'], $_POST['client_test_id']),
                    'options' => $this->getTestQuestionnaireOptionsByCode($_POST['code']),
                    'smoking_answer' => ($_POST['code'] == 'smoking') ? $this->getSpecificTestQuestionnaireClientAnswer(3 /*smoking*/, $_POST['club_client_id'], $_POST['client_test_id']) : ''
                )
            ));
        
    }
    
    // SUBMIT LIFESTYLE ANSWER
    public function lifeStyleTestAnswerAction(){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $formula = new Model\TestFormulas();
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            //return $this->redirect($this->generateUrl('acme_club_login'));
            return new Response("session expired");
        }
        
        $_POST['club_client_id'] = intval($_POST['club_client_id']);
        $_POST['client_test_id'] = ($_POST['client_test_id'] != '') ? $_POST['client_test_id'] : NULL;
        
        $error = array();
        
        if($_POST['answer_type'] == 'risk-profile'){
            $error_count = 0;
            foreach ($_POST['answer'] as $lifestyle_question_id => $answer) {
                if(trim($answer) != '' && ($answer > 7 || $answer < 1)){
                    $error_count +=1;
                }
            }
            if( $error_count > 0){
                $error[] = 'Make sure that your answer(s) must not exceed to 7 (highly likely) and not below 1  (highly unlikely).';
            }
        }
        
        if(count($error) == 0 ){
            if($_POST['answer_type'] == 'physical-activity' ||
                $_POST['answer_type'] == 'alcohol' || 
                $_POST['answer_type'] == 'mental-health' ||
                $_POST['answer_type'] == 'risk-profile' ||
                $_POST['answer_type'] == 'nutrition' ){

                if(isset($_POST['answer'])){
                    foreach ($_POST['answer'] as $lifestyle_question_id => $answer) {
                        $em = $this->getDoctrine()->getManager();

                        if($answer != ''){
                            $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                                    ->findOneBy(array(
                                        'lq_id' => $lifestyle_question_id, 
                                        'club_client_id' => $_POST['club_client_id'],
                                        'client_test_id' => $_POST['client_test_id']
                                    ));

                            if(count($model) == 0){
                                $model = new LifestyleQuestionnareClientAnswer();
                                $model->setLqId($lifestyle_question_id);
                            }

                            if($_POST['client_test_id'] != NULL){
                                $model->setStatus('saved');
                            }else{
                                $model->setStatus('draft');
                            }
                            $model->setClubClientId($_POST['club_client_id']);
                            $model->setClientTestId($_POST['client_test_id']);
                            $model->setAnswer($answer);
                            $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                            $em->persist($model);
                            $em->flush();
                        }else{
                            $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                                    ->findOneBy(array(
                                        'lq_id' => $lifestyle_question_id, 
                                        'club_client_id' => $_POST['club_client_id'],
                                        'client_test_id' => $_POST['client_test_id']
                                    ));
                            if(count($model) > 0){
                                $model->setAnswer($answer);
                                $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                                $em->persist($model);
                                $em->flush();
                            }
                        }
                    }

                }
            }elseif($_POST['answer_type'] == 'smoking'){

                $em = $this->getDoctrine()->getManager();
                if($_POST['answer'] != ''){
                    $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                            ->findOneBy(array(
                                'lq_id' => 3 , //smoking
                                'club_client_id' => $_POST['club_client_id'],
                                'client_test_id' => $_POST['client_test_id']
                            ));

                    if(count($model) == 0){
                        $model = new LifestyleQuestionnareClientAnswer();
                        $model->setLqId(3);
                    }
                    if($_POST['client_test_id'] != NULL){
                        $model->setStatus('saved');
                    }else{
                        $model->setStatus('draft');
                    }

                    $model->setClubClientId($_POST['club_client_id']);
                    $model->setClientTestId($_POST['client_test_id']);
                    $model->setAnswer($_POST['answer']);
                    $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                    $em->persist($model);
                    $em->flush();

                }else{
                    $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                            ->findOneBy(array(
                                'lq_id' => 3, 
                                'club_client_id' => $_POST['club_client_id'],
                                'client_test_id' => $_POST['client_test_id']
                            ));
                    if(count($model) > 0){
                        $model->setAnswer($answer);
                        $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                        $em->persist($model);
                        $em->flush();
                    }
                }


            }

            $answer_type = str_replace('-', '_', $_POST['answer_type']);
            $s_complete = $this->sLifestyleTestQuestionnaireAnswersDone($answer_type, $_POST['club_client_id'], $_POST['client_test_id']);
            
            $response = array(
                'result' => 'success',
                'code' => $_POST['answer_type'],
                's_complete' => $s_complete
            );
        }else{
            $response = array(
                'result' => 'error',
                'message' => $error[0]
            );
        }
        
        
        return new Response(json_encode($response));
    }
    
    
    
    
    
//==============================================================================
// GET TEST RESULTS (AJAX)
//==============================================================================
    public function getBmiResultAction(){
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['weight'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['height'])){
            return new Response('Invalid parameter input!');
        }
        $_POST['weight'] = intval($_POST['weight']);
        $_POST['height'] = intval($_POST['height']);
        
        $response = $formula->bMI($_POST['height'], $_POST['weight']);
        return new Response($response);
        
    }
    
    public function getPhysicalActivityResultAction(){
        $session = $this->getRequest()->getSession();
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['moderate_vigorous_pa'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['muscle_strenthening_pa'])){
            return new Response('Invalid parameter input!');
        }
        
        
        $_POST['moderate_vigorous_pa'] = intval($_POST['moderate_vigorous_pa']);
        $_POST['muscle_strenthening_pa'] = intval($_POST['muscle_strenthening_pa']);
        
        $moderate_vigorous_pa = $formula->moderateOrVigorousIntensityPhysicalActivity($_POST['moderate_vigorous_pa']);
        $muscle_strenthening_pa = $formula->muscleStrentheningPhysicalActivity($_POST['muscle_strenthening_pa']);
        
        $response = $formula->physicalActivity($moderate_vigorous_pa, $muscle_strenthening_pa);
        
        
        // UPDATE tbl_client_test
        if($_POST['client_test_id'] != ''){
            $em = $this->getDoctrine()->getManager();
            $test = $em->getRepository('AcmeHeadOfficeBundle:ClientTest')
                        ->findOneBy(array(
                            'client_test_id' => $_POST['client_test_id'],
                        ));
            $test->setLifestylePhysicalActivity($response);
            $em->persist($test);
            $em->flush();
        }
        
        $session->set('lifestyle-physical-activity', $response);
        return new Response($response);
        
    }
    
    public function getSmokingResultAction(){
        $session = $this->getRequest()->getSession();
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['gender'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['answer'])){
            return new Response('Invalid parameter input!');
        }
        
        $_POST['answer'] = intval($_POST['answer']);
        
        $response = $formula->smokingHabits($_POST['gender'], $_POST['answer']);
        
        // UPDATE tbl_client_test
        if($_POST['client_test_id'] != ''){
            $em = $this->getDoctrine()->getManager();
            $test = $em->getRepository('AcmeHeadOfficeBundle:ClientTest')
                        ->findOneBy(array(
                            'client_test_id' => $_POST['client_test_id'],
                        ));
            $test->setLifestyleSmoking($response);
            $em->persist($test);
            $em->flush();
        }
        
        $session->set('lifestyle-smoking', $response);
        
        return new Response($response);
    }
    
    public function getAlcoholConsumptionResultAction(){
        $session = $this->getRequest()->getSession();
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['gender'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['per_week_value'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['one_sitting_value'])){
            return new Response('Invalid parameter input!');
        }
        
        $_POST['per_week_value'] = intval($_POST['per_week_value']);
        $_POST['one_sitting_value'] = intval($_POST['one_sitting_value']);
        
        $alcohol_consumption_per_week = $formula->alcoholConsumptionPerWeekResult($_POST['gender'], $_POST['per_week_value']);
        $alcohol_consumption_in_one_sitting = $formula->alcoholConsumptionInOneSittingResult($_POST['one_sitting_value']);
        
        $response = $formula->alcoholConsumptionTotal($alcohol_consumption_per_week, $alcohol_consumption_in_one_sitting);
        
        // UPDATE tbl_client_test
        if($_POST['client_test_id'] != ''){
            $em = $this->getDoctrine()->getManager();
            $test = $em->getRepository('AcmeHeadOfficeBundle:ClientTest')
                        ->findOneBy(array(
                            'client_test_id' => $_POST['client_test_id'],
                        ));
            $test->setLifestyleAlcohol($response);
            $em->persist($test);
            $em->flush();
        }
        
        $session->set('lifestyle-alcohol', $response);
        
        return new Response($response);
    }
    

    public function getNutritionResultAction(){
        $session = $this->getRequest()->getSession();
        $formula = new Model\TestFormulas();
        
        if(isset($_POST['answer'])){
            $number_of_yes = 0;
            foreach ($_POST['answer'] as $lifestyle_question_id => $answer) {
                $number_of_yes += intval($answer);
            }

            $response = $formula->nutrition($number_of_yes);

            // UPDATE tbl_client_test
            if($_POST['client_test_id'] != ''){
                $em = $this->getDoctrine()->getManager();
                $test = $em->getRepository('AcmeHeadOfficeBundle:ClientTest')
                            ->findOneBy(array(
                                'client_test_id' => $_POST['client_test_id'],
                            ));
                $test->setLifestyleNutrition($response);
                $em->persist($test);
                $em->flush();
            }
        }else{
            $response = '';
        }
        $session->set('lifestyle-nutrition', $response);
        
        return new Response($response);
    }
    
    
    public function getMentalHealthResultAction(){
        $session = $this->getRequest()->getSession();
        $formula = new Model\TestFormulas();
        
        if(isset($_POST['answer'])){
            $mental_health_value = 0;
            foreach ($_POST['answer'] as $lifestyle_question_id => $answer) {
                if($answer != ''){
                    $mental_health_value += intval($answer);
                }
            }
            if($mental_health_value > 0){
                $response = $formula->mentalHealth($mental_health_value);

            }else{
                $response = '';
            }
        }else{
            $response = '';
        }
        
        // UPDATE tbl_client_test
        if($_POST['client_test_id'] != ''){
            $em = $this->getDoctrine()->getManager();
            $test = $em->getRepository('AcmeHeadOfficeBundle:ClientTest')
                        ->findOneBy(array(
                            'client_test_id' => $_POST['client_test_id'],
                        ));
            if($response != ''){
                $test->setLifestyleMentalHealth($response);
            }else{
                $test->setLifestyleMentalHealth(NULL);
            }
            $em->persist($test);
            $em->flush();
        }
        
        $session->set('lifestyle-mental-health', $response);
        
        return new Response($response);
    }
    
    
    public function getRiskProfileResultAction(){
        $session = $this->getRequest()->getSession();
        $formula = new Model\TestFormulas();
        
        if(isset($_POST['answer'])){
            $risk_profile_value = 0;
            foreach ($_POST['answer'] as $lifestyle_question_id => $answer) {
                if($answer != ''){
                    $risk_profile_value += intval($answer);
                }
            }
            if($risk_profile_value > 0){
                //$response = $formula->mentalHealth($mental_health_value);
                $response = $formula->riskProfile($risk_profile_value);

            }else{
                $response = '';
            }
        }else{
            $response = '';
        }
        
        // UPDATE tbl_client_test
        if($_POST['client_test_id'] != ''){
            $em = $this->getDoctrine()->getManager();
            $test = $em->getRepository('AcmeHeadOfficeBundle:ClientTest')
                        ->findOneBy(array(
                            'client_test_id' => $_POST['client_test_id'],
                        ));
            if($response != ''){
                $test->setLifestyleRiskProfile($response);
            }else{
                $test->setLifestyleRiskProfile(NULL);
            }
            $em->persist($test);
            $em->flush();
        }
        
        $session->set('lifestyle-risk-profile',$response);
        
        return new Response($response);
    }
    
    public function getVo2TestAction(){
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['gender'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['weight'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['age'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['test_type'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['value1'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['value2'])){
            return new Response('Invalid parameter input!');
        }
        
        $_POST['weight'] = intval($_POST['weight']);
        $_POST['age'] = intval($_POST['age']);
        $_POST['value1'] = intval($_POST['value1']);
        $_POST['value2'] = intval($_POST['value2']);
        
        if($_POST['test_type'] == 'bike'){
            $score = $formula->vo2BikeTest($_POST['gender'], $_POST['weight'], $_POST['age'], $_POST['value1']);
            
        }elseif($_POST['test_type'] == 'resting-hr'){
            
            $score = $formula->vo2RestingHrTest($_POST['age'], $_POST['value1']);
            
        }elseif($_POST['test_type'] == 'treadmill'){
            
            $score = $formula->vo2TreadmillTest($_POST['gender'], $_POST['weight'], $_POST['value1'], $_POST['value2']);
        
        }elseif($_POST['test_type'] == 'beep'){
            $score = $formula->vo2BeepTest($_POST['value1'], $_POST['value2']);
        }else{
            
            $score = $formula->vo2DirectEntry($_POST['value1']);
        }
        
        $result = $formula->vo2($_POST['gender'], $_POST['age'], $score);
        
        $response = array(
            'score'=>$score,
            'result'=>$result
            );
        
        return new Response(json_encode($response));
        
    }
    
    public function getBalanceTestAction(){
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['gender'])){
            return new Response('Invalid parameter input!');
        }
        if(!isset($_POST['value'])){
            return new Response('Invalid parameter input!');
        }
        
        $_POST['value'] = intval($_POST['value']);
        
        $response = $formula->balance($_POST['test_type'], $_POST['gender'], $_POST['value']);
        
        
        return new Response($response);
    }
    
    // removelater
    public function getWaistTestAction(){
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['gender'])){
            return new Response('Invalid parameter input!');
        }
        
        $_POST['value'] = intval($_POST['value']);
        
        $response = $formula->waist($_POST['gender'], $_POST['value']);
        
        
        return new Response($response);
    }
    
    // removelater
    public function getSquatTestAction(){
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['gender'])){
            return new Response('Invalid parameter input!');
        }
        
        $_POST['value'] = intval($_POST['value']);
        
        $response = $formula->waist($_POST['gender'], $_POST['value']);
        
        
        return new Response($response);
    }
    
    public function deleteAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        if(isset($_POST['client_test_id'])){
            $em = $this->getDoctrine()->getEntityManager();

            $_POST['client_test_id'] = filter_var($_POST['client_test_id'], FILTER_SANITIZE_NUMBER_INT);
            
            $query = $em->createQuery('DELETE AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer a WHERE a.client_test_id = :client_test_id');
            $query->setParameter('client_test_id', $_POST["client_test_id"]);
            $query->execute(); 
            
            $model = $em->getRepository('AcmeHeadOfficeBundle:ClientTest')->findOneBy(array('client_test_id'=>$_POST["client_test_id"]));
            $em->remove($model);
            $em->flush();
            
            
            
            // DELETE TEST FROM 12WEEK PROGRAM
            $mod = new Model\GlobalModel();
            if($_SERVER['SERVER_NAME'] == 'localhost'){
                $program_host="localhost";
                $program_uname="root";
                $program_pass="";
                $program_database = "healthy_life_project_ohwp";
            }else{
                $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
                $program_uname="hlproot";
                $program_pass="_jWBqa)67?BJq(j-";
                $program_database = "hlp12weekProgram";
            }

            $program_connection=mysqli_connect($program_host,$program_uname,$program_pass, $program_database) or die("Database Connection Failed");

            $program_client_test_id = $this->get12weekProgramClientTestIdByFullTestId($_POST["client_test_id"]);
            mysqli_query($program_connection,
                "DELETE FROM tbl_lifestyle_questionnaire_client_answer WHERE client_test_id=" . $program_client_test_id);

            mysqli_query($program_connection,
                "DELETE FROM tbl_client_test WHERE full_test_id=" . $_POST["client_test_id"]);

            
            // SET ACTIVITY
            $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " deleted a draft test with id " . $model->getClientTestId() . ".";
            
            if($session->get('club_admin_id') != ''){ 
                $this->setActivity($session->get('club_admin_id'), 'club-admin', $details);
            }else{
                $this->setActivity($session->get('ho_admin_id'), 'head-office-admin', $details);
            }
            
            return $this->redirect($this->generateUrl('acme_club_client_view') . "?id=".$model->getClubClientId());
        }
        
    }
    
    public function updateImprovementResultsForReportingAction(){
        $formula = new Model\TestFormulas();
        $em = $this->getDoctrine()->getEntityManager();
        $cust = $this->getDoctrine()->getEntityManager();
        $connection = $cust->getConnection();
        $statement = $connection->prepare("SELECT t.*, cc.gender 
                FROM tbl_client_test t
                LEFT JOIN tbl_club_client cc ON cc.club_client_id = t.club_client_id
                WHERE t.physiological_vo2_score IS NULL 
                    AND t.status='finalised'");
//        $statement->bindValue('lite_test_id', $lite_test_id);
        
        $statement->execute();
        $data =  $statement->fetchAll();
        
        for($i=0; $i<count($data); $i++){ 
            $testscore = $em->getRepository('AcmeHeadOfficeBundle:ClientTest')
                            ->findOneBy(array(
                                'client_test_id' => $data[$i]['client_test_id'],
                            ));
            //the scores
            if($testscore->getPhysiologicalVo2TestType() == 'bike'){
                $physiologicalVo2 = $formula->vo2BikeTest($data[$i]['gender'], $data[$i]['weight'], $data[$i]['age'], $data[$i]['physiological_vo2_test_value1']);

            }elseif($testscore->getPhysiologicalVo2TestType() == 'resting-hr'){

                $physiologicalVo2 = $formula->vo2RestingHrTest($data[$i]['age'], $data[$i]['physiological_vo2_test_value1']);

            }elseif($testscore->getPhysiologicalVo2TestType() == 'treadmill'){

                $physiologicalVo2 = $formula->vo2TreadmillTest($data[$i]['gender'], $data[$i]['weight'], $data[$i]['physiological_vo2_test_value1'], $data[$i]['physiological_vo2_test_value2']);

            }elseif($testscore->getPhysiologicalVo2TestType() == 'beep'){

                $physiologicalVo2 = $formula->vo2BeepTest($data[$i]['physiological_vo2_test_value1'], $data[$i]['physiological_vo2_test_value2']);
            }else{

                $physiologicalVo2 = $formula->vo2DirectEntry($data[$i]['physiological_vo2_test_value1']);
            }
            $testscore->setPhysiologicalVo2Score($physiologicalVo2);
            $em->persist($testscore);
            $em->flush();
                
            $first_test_details = $this->getClientFirstOrRecentTestDetails($data[$i]['club_client_id']);
            echo $data[$i]['client_test_id'] . ' - ' . $first_test_details['client_test_id']; 
            echo '<br>';
            if( $data[$i]['client_test_id'] != $first_test_details['client_test_id'] ){
                
                if( count($first_test_details) > 0 ){
                    $test = $em->getRepository('AcmeHeadOfficeBundle:ClientTest')
                            ->findOneBy(array(
                                'client_test_id' => $data[$i]['client_test_id'],
                            ));
                    
                    
                    
                    
                    //the improvements
                        $bmi = $data[$i]['bmi'];
                        $bmiImprovement = $first_test_details['bmi'] - $bmi;
                    $test->setBmiImprovement($bmiImprovement);
                        $bodyFatReduction = $first_test_details['biometric_body_fat_value'] - intval($data[$i]['biometric_body_fat_value']);
                    $test->setBodyFatReduction($bodyFatReduction);
                        $vo2Improvement = $data[$i]['physiological_vo2_score'] - $first_test_details['physiological_vo2_score'];
                    $test->setVo2Improvement($vo2Improvement);
                        $squatTestImprovement = intval($data[$i]['physiological_squat_value']) - $first_test_details['physiological_squat_value'];
                    $test->setSquatTestImprovement($squatTestImprovement);
                        if(trim($data[$i]['physiological_balance_type']) == 'standing functional reach'){
                            $standingFunctionalReachImprovement = intval($data[$i]['physiological_balance_value']) - $first_test_details['physiological_balance_value'];
                        }else{
                            $standingFunctionalReachImprovement = 0;
                        }
                    $test->setStandingFunctionalReachImprovement($standingFunctionalReachImprovement);
                        if(trim($data[$i]['physiological_balance_type']) == 'stork test'){
                            $storkTestImprovement = intval($data[$i]['physiological_balance_value']) - $first_test_details['physiological_balance_value'];
                        }else{
                            $storkTestImprovement = 0;
                        }
                    $test->setStorkTestImprovement($storkTestImprovement);

                        $abdominalTestImprovement = $data[$i]['biometric_abdomen_lost'] - $first_test_details['biometric_abdomen_lost'];
                    $test->setAbdominalTestImprovement($abdominalTestImprovement);

                        $lsqca_current = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                                            ->findOneBy(array(
                                                'lq_id' => 1, 
                                                'club_client_id' => $data[$i]['club_client_id'],
                                                'client_test_id' => $data[$i]['client_test_id']
                                            ));

                        $lsqca_ftest = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                                            ->findOneBy(array(
                                                'lq_id' => 1, 
                                                'club_client_id' => $data[$i]['club_client_id'],
                                                'client_test_id' => $first_test_details['client_test_id']
                                            ));

                        if(count($lsqca_current) > 0 && count($lsqca_ftest) > 0){
                            $vigorousPaPerWeek = $lsqca_current->getAnswer() - $lsqca_ftest->getAnswer();
                        }elseif(count($lsqca_current) == 0 && count($lsqca_ftest) > 0){
                            $vigorousPaPerWeek = 0 - $lsqca_ftest->getAnswer();
                        }else{
                            $vigorousPaPerWeek = 0;
                        }
                    $test->setVigorousPaPerWeek($vigorousPaPerWeek);

                        $lsqca_current = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                                            ->findOneBy(array(
                                                'lq_id' => 2, 
                                                'club_client_id' => $data[$i]['club_client_id'],
                                                'client_test_id' => $data[$i]['client_test_id']
                                            ));

                        $lsqca_ftest = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                                            ->findOneBy(array(
                                                'lq_id' => 2, 
                                                'club_client_id' => $data[$i]['club_client_id'],
                                                'client_test_id' => $first_test_details['client_test_id']
                                            ));
                        if(count($lsqca_current) > 0 && count($lsqca_ftest) > 0){
                            $strengthPaPerWeek = $lsqca_current->getAnswer() - $lsqca_ftest->getAnswer();
                        }elseif(count($lsqca_current) == 0 && count($lsqca_ftest) > 0){
                            $strengthPaPerWeek = 0 - $lsqca_ftest->getAnswer();
                        }else{
                            $strengthPaPerWeek = 0;
                        }
                    $test->setStrengthPaPerWeek($strengthPaPerWeek);

                        $smokingImprovement = $data[$i]['lifestyle_smoking'] - $first_test_details['lifestyle_smoking'];
                    $test->setSmokingImprovement($smokingImprovement);

                        $lsqca_current = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                                            ->findOneBy(array(
                                                'lq_id' => 4, 
                                                'club_client_id' => $data[$i]['club_client_id'],
                                                'client_test_id' => $data[$i]['client_test_id']
                                            ));

                        $lsqca_ftest = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                                            ->findOneBy(array(
                                                'lq_id' => 4, 
                                                'club_client_id' => $data[$i]['club_client_id'],
                                                'client_test_id' => $first_test_details['client_test_id']
                                            ));
                        if(count($lsqca_current) > 0 && count($lsqca_ftest) > 0){
                            $standardAlcoholPerWeek = $lsqca_ftest->getAnswer() - $lsqca_current->getAnswer();
                        }elseif(count($lsqca_current) == 0 && count($lsqca_ftest) > 0){
                            $standardAlcoholPerWeek = $lsqca_ftest->getAnswer() - 0;
                        }else{
                            $standardAlcoholPerWeek = 0;
                        }
                    $test->setStandardAlcoholPerWeek($standardAlcoholPerWeek);

                        $lsqca_current = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                                            ->findOneBy(array(
                                                'lq_id' => 5, 
                                                'club_client_id' => $data[$i]['club_client_id'],
                                                'client_test_id' => $data[$i]['client_test_id']
                                            ));

                        $lsqca_ftest = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                                            ->findOneBy(array(
                                                'lq_id' => 5, 
                                                'club_client_id' => $data[$i]['club_client_id'],
                                                'client_test_id' => $first_test_details['client_test_id']
                                            ));
                        if(count($lsqca_current) > 0 && count($lsqca_ftest) > 0){
                            $maximumAlcoholOneSitting = $lsqca_ftest->getAnswer() - $lsqca_current->getAnswer();
                        }elseif(count($lsqca_current) == 0 && count($lsqca_ftest) > 0){
                            $maximumAlcoholOneSitting = $lsqca_ftest->getAnswer() - 0;
                        }else{
                            $maximumAlcoholOneSitting = 0;
                        }
                    $test->setMaximumAlcoholOneSitting($maximumAlcoholOneSitting);
                        
                        $nutrition_current = $this->getLifestyleQuestionnaireClientAnswerByCode('nutrition', $data[$i]['client_test_id']);
                        $nutrition_current_yeses = 0;
                        for($a=0; $a<count($nutrition_current); $a++){
                            $nutrition_current_yeses += ($nutrition_current[$a]['answer'] == 1) ? 1 : 0;
                        }
                        
                        $nutrition_1st = $this->getLifestyleQuestionnaireClientAnswerByCode('nutrition', $first_test_details['client_test_id']);
                        $nutrition_1st_yeses = 0;
                        for($a=0; $a<count($nutrition_1st); $a++){
                            $nutrition_1st_yeses += ($nutrition_1st[$a]['answer'] == 1) ? 1 : 0;
                        }
                        $nutritionScoreImprovement = $nutrition_current_yeses - $nutrition_1st_yeses;
                        $nutritionScoreImprovement = $nutritionScoreImprovement * 10;
                    $test->setNutritionScoreImprovement($nutritionScoreImprovement);

                        $mental_health_current = $this->getLifestyleQuestionnaireClientAnswerByCode('mental_health', $data[$i]['client_test_id']);
                        $mental_health_current_total = 0;
                        for($a=0; $a<count($mental_health_current); $a++){
                            $mental_health_current_total += $mental_health_current[$a]['answer'];
                        }
                        
                        $mental_health_1st = $this->getLifestyleQuestionnaireClientAnswerByCode('mental_health', $first_test_details['client_test_id']);
                        $mental_health_1st_total = 0;
                        for($a=0; $a<count($mental_health_1st); $a++){
                            $mental_health_1st_total += $mental_health_1st[$a]['answer'];
                        }
                        $mentalHealthImprovement = $mental_health_1st_total - $mental_health_current_total;
                        $mentalHealthImprovement = $mentalHealthImprovement * 2;
                    $test->setMentalHealthImprovement($mentalHealthImprovement);
                    
                        $risk_profile_current = $this->getLifestyleQuestionnaireClientAnswerByCode('risk_profile', $data[$i]['client_test_id']);
                        $risk_profile_current_total = 0;
                        for($a=0; $a<count($risk_profile_current); $a++){
                            $risk_profile_current_total += $risk_profile_current[$a]['answer'];
                        }
                        
                        $risk_profile_1st = $this->getLifestyleQuestionnaireClientAnswerByCode('risk_profile', $first_test_details['client_test_id']);
                        $risk_profile_1st_total = 0;
                        for($a=0; $a<count($risk_profile_1st); $a++){
                            $risk_profile_1st_total += $risk_profile_1st[$a]['answer'];
                        }
                        $riskProfileImprovement = $risk_profile_1st_total - intval($risk_profile_current_total);
                        $riskProfileImprovement = $riskProfileImprovement * 1.42857143;
//                        $riskProfileImprovement = $first_test_details['lifestyle_risk_profile'] - intval($data[$i]['lifestyle_risk_profile']);
                    $test->setRiskProfileImprovement($riskProfileImprovement);
                }else{
                    $test->setBmiImprovement(0);
                    $test->setBodyFatReduction(0);
                    $test->setVo2Improvement(0);
                    $test->setSquatTestImprovement(0);
                    $test->setStandingFunctionalReachImprovement(0);
                    $test->setStorkTestImprovement(0);
                    $test->setAbdominalTestImprovement(0);
                    $test->setVigorousPaPerWeek(0);
                    $test->setStrengthPaPerWeek(0);
                    $test->setSmokingImprovement(0);
                    $test->setStandardAlcoholPerWeek(0);
                    $test->setMaximumAlcoholOneSitting(0);
                    $test->setNutritionScoreImprovement(0);
                    $test->setMentalHealthImprovement(0);
                    $test->setRiskProfileImprovement(0);
                }
                
                $em->persist($test);
                $em->flush();
                
            }
        }
        return new Response("success");
    }
    
    public function getWaistHipRatioAction(){
        $formula = new Model\TestFormulas();

        if(!isset($_POST['waist'])){
            return new Response('Invalid parameter input!');
        }

        if(!isset($_POST['hip'])){
            return new Response('Invalid parameter input!');
        }
        $_POST['waist'] = intval($_POST['waist']);
        $_POST['hip'] = intval($_POST['hip']);

        $response = $formula->waistToHipRatio($_POST['waist'], $_POST['hip']);
        return new Response($response);

    }
    
    
    
    public function deleteDraftTestAfter7DaysAction(){
        $cust = $this->getDoctrine()->getEntityManager();
        $connection = $cust->getConnection();
        if($connection){
            $statement = $connection->prepare("DELETE FROM tbl_client_test WHERE `status`='draft' AND IFNULL(DATEDIFF(CURDATE(),test_datetime),0) >=7");
            $statement->execute();
        }
        return new Response('success');
    }
}
