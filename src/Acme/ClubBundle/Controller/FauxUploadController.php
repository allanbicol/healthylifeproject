<?php

namespace Acme\ClubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

class FauxUploadController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function fauxUploadAction()
    {
        $session = $this->getRequest()->getSession();
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return new Response('session_expired');
        }
        
        $mod = new Model\GlobalModel();
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        
        // Undefined | Multiple Files | $_FILES Corruption Attack
        // If this request falls under any of them, treat it invalid.
        if (
            !isset($_FILES['fileToUpload']['error']) ||
            is_array($_FILES['fileToUpload']['error'])
        ) {
            return new Response('Invalid parameters.');
        }
        
        // Check $_FILES['fileToUpload']['error'] value.
        switch ($_FILES['fileToUpload']['error']) {
           case UPLOAD_ERR_OK:
               break;
           case UPLOAD_ERR_NO_FILE:
               return new Response('Warning: No file sent. File upload failed.');
           case UPLOAD_ERR_INI_SIZE:
               return new Response('Warning: Exceeded filesize limit. File upload failed.');
           case UPLOAD_ERR_FORM_SIZE:
               return new Response('Warning: Exceeded filesize limit. File upload failed.');
           default:
               return new Response('Warning: Unknown errors. File upload failed.');
        }
        
        if(!isset($_POST['email'])){
            return new Response('Warning: Invalid Parameter. File upload failed.');
        }
        
        // check filesize  
        if ($_FILES['fileToUpload']['size'] > 2048000 ) {
            return new Response('Warning: Exceeded filesize limit. File upload failed.');
        }
        
        // DO NOT TRUST $_FILES['fileToUpload']['mime'] VALUE !!
        // Check MIME Type by yourself.
        $finfo = new \finfo(FILEINFO_MIME_TYPE);
        
        if (false === $ext = array_search(
            $finfo->file($_FILES['fileToUpload']['tmp_name']),
            array(
                'pdf' => 'application/pdf',
                'doc' => 'application/msword',
                'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
            ),
            true
        )) {
            return new Response('Warning: Invalid file format. File upload failed.');
        }
        $_POST['email'] = $mod->getDirNameFromEmail($_POST['email']);
        $dir = $root_dir . '/uploads/client-files/'.$_POST['email'].'/'; 
                
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        $_FILES['fileToUpload']['name'] = str_replace("'", "_", $_FILES['fileToUpload']['name']);
        $_FILES['fileToUpload']['name'] = str_replace(" ", "_", $_FILES['fileToUpload']['name']);
        
        $file = $dir . $_FILES['fileToUpload']['name']; 
        
        move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $file); 
        
        $pathinfo = pathinfo($file);
        $file_date = array("datecreated"=>date ("m-d-Y", filemtime($dir . "/".$pathinfo['basename'])));
        $pathinfo = array_merge($pathinfo, $file_date);
        
        $site_url = $mod->siteURL();
        $site_url = ($site_url == 'http://localhost') ? $site_url . '/healthylife-new/web/uploads/client-files/' : $site_url . '/web/uploads/client-files/';
        $site_url = $site_url . $_POST['email'];
        
        //for security reason, we force to remove all uploaded file
        @unlink($_FILES['fileToUpload']);
        
        return new Response(
                $this->renderView('AcmeClubBundle:FauxUpload:file_row_template.html.twig',
                            array(
                                'item' => $pathinfo,
                                'site_url' => $site_url
                            ))
                );
        return new Response("Your file has been uploaded successfully.");

    }
    
    public function fauxDeleteFileAction(){
        $session = $this->getRequest()->getSession();
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return new Response('session_expired');
        }
        
        $mod = new Model\GlobalModel();
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        $_POST['email'] = $mod->getDirNameFromEmail($_POST['email']);
        $file = $root_dir . '/uploads/client-files/'. $_POST['email'] . '/' . $_POST['filename'];
        if (!file_exists($file)) {
            return new Response("Warning: File does not exist.");
        }
        
        unlink($file);
        
        return new Response("success");
    }

}
