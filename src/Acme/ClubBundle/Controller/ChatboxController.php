<?php

namespace Acme\ClubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

use Acme\HeadOfficeBundle\Entity\SeenChatMessages;
use Doctrine\DBAL\Driver\Mysqli;
class ChatboxController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function openChatToAction(){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $mod = new Model\GlobalModel();
        
        if($session->get('club_admin_id') == ''){
            return new Response('session_expired');
        }
        if(isset($_POST['box'])){ 
            $_POST['box'] = filter_var($_POST['box'], FILTER_SANITIZE_NUMBER_INT);
            
            $client_details = $this->getClubClientById($_POST['box']);
            $openchats = json_decode($session->get('open_chat'), true);
            if(!$this->isInMultiArray($openchats, 'id', $_POST['box'])){
                $openchats[] = array(
                        'id'=> $_POST['box'],
                        'fname'=> $client_details['fname'],
                        'lname'=> $client_details['lname'],
                        'email'=> $client_details['email']
                    );
                $session->set('open_chat', json_encode($openchats));
            }
        }
        
        return new Response('success');
        
    }
    
    public function closeChatAction(){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $mod = new Model\GlobalModel();
        
        if($session->get('club_admin_id') == ''){
            return new Response('session_expired');
        }
        if(isset($_POST['box'])){ 
            $_POST['box'] = filter_var($_POST['box'], FILTER_SANITIZE_NUMBER_INT);
            
            $client_details = $this->getClubClientById($_POST['box']);
            $openchats = json_decode($session->get('open_chat'), true);
            
            
            $openchats = $this->removeElementWithValue($openchats, 'id', $_POST['box']);
            $session->set('open_chat', json_encode($openchats));
        }
        
        return new Response('success');
        
    }
    
    public function removeElementWithValue($array, $key, $value){
        foreach($array as $subKey => $subArray){
             if($subArray[$key] == $value){
                  unset($array[$subKey]);
             }
        }
        return $array;
   }
    
    public function chatAction()
    {
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $mod = new Model\GlobalModel();
        
        if($session->get('club_admin_id') == ''){
            return new Response('session_expired');
        }
        
        if($_SERVER['SERVER_NAME'] == 'localhost'){
            $program_host="localhost";
            $program_uname="root";
            $program_pass="";
            $program_database = "healthy_life_project_ohwp";

        }else{
            $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
            $program_uname="hlproot";
            $program_pass="_jWBqa)67?BJq(j-";
            $program_database = "hlp12weekProgram";

        }
        
        $program_connection=mysqli_connect($program_host,$program_uname,$program_pass, $program_database) or die("Database Connection Failed");
        
        $_POST['message'] =$_POST['message'];
        $_POST['email'] = trim($_POST['email']);
        if(!$mod->isEmailValid($_POST['email'])){
            return new Response('failed');
        }
        
        $_POST['id'] = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);
        $client_details = $this->getClubClientById($_POST['id']);
//        $_POST['message'] = addslashes($_POST['message']); 
        $_POST['message'] = mysqli_real_escape_string($program_connection,$_POST['message']); 
        $program_insert = mysqli_query($program_connection,
                "INSERT INTO tbl_chatbox(
                    chat_datetime,
                    club_id,
                    club_admin_id,
                    club_admin_name,
                    club_client_id,
                    client_email,
                    client_fname,
                    client_lname,
                    message,
                    sender_type) 
                 VALUES(
                    '". $datetime->format("Y-m-d H:i:s") ."',
                    ". $session->get('club_id') .",    
                    ". $session->get('club_admin_id') .",
                    '". $session->get('fname') ."',
                    ". $_POST['id'] .",
                    '". $_POST['email'] ."',
                    '". $client_details['fname'] ."',
                    '". $client_details['lname'] ."',
                    '". $_POST['message'] ."',
                    'coach'
                 )");
//        mysql_real_escape_string
        
        $program_query = mysqli_query($program_connection,
                "SELECT * FROM tbl_chatbox p WHERE p.chat_datetime = '". $datetime->format("Y-m-d H:i:s") ."'
                    AND p.club_admin_id = ". $session->get('club_admin_id') ." 
                    AND p.club_client_id = ". $_POST['id']);
        
        $data = [];
        while($row = mysqli_fetch_array($program_query)){
            $data[] = $row;
        }
        
        return new Response(json_encode($data));
    }
    
    
    public function getChatMsgsAction()
    {
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $mod = new Model\GlobalModel();
        
        if($session->get('club_admin_id') == ''){
            return new Response('session_expired');
        }
        
        
        if(isset($_POST['box'])){
            $_POST['box'] = filter_var($_POST['box'], FILTER_SANITIZE_NUMBER_INT);
            
            $client_details = $this->getClubClientById($_POST['box']);
            $openchats = json_decode($session->get('open_chat'), true);
            
            if(!$this->isInMultiArray($openchats, 'id', $_POST['box'])){
                $openchats[] = array(
                        'id'=> $_POST['box'],
                        'fname'=> $client_details['fname'],
                        'lname'=> $client_details['lname'],
                        'email'=> $client_details['email']
                    );
                $session->set('open_chat', json_encode($openchats));
            }
        }
        
        if(isset($_POST['email'])){
            $_POST['email'] = trim($_POST['email']);
            if(!$mod->isEmailValid($_POST['email'])){
                return new Response('failed');
            }
            $email = $_POST['email'];
        }else{
            $email = NULL;
        }
        
        $except_admin_id = (isset($_POST['except_me'])) ? $session->get('club_admin_id') : NULL;
        $new_only = (isset($_POST['new_only'])) ? 1 : NULL;
            
        $messages = $this->getChatMessages($session->get('club_id'), $session->get('club_admin_id'), $email, $except_admin_id, $new_only);
        
        
        return new Response(json_encode($messages));
    }
    
    public function getOldChatMsgsAction(){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $mod = new Model\GlobalModel();
        
        if($session->get('club_admin_id') == ''){
            return new Response('session_expired');
        }
        
        if(!isset($_POST['lastno'])){
            return new Response('failed');
        }
        
        if(!isset($_POST['box_name'])){
            return new Response('failed');
        }
        
        $lastno = filter_var($_POST['lastno'], FILTER_SANITIZE_NUMBER_INT);
        $client_id = filter_var($_POST['box_name'], FILTER_SANITIZE_NUMBER_INT);
        $prog_client_details = $this->getClubClientById($client_id);
        
        $messages = $this->getChatMessages($session->get('club_id'),$session->get('club_admin_id'), $prog_client_details['email'], NULL, NULL, 1, $lastno);
        
        
        return new Response(json_encode($messages));
    }
    
    public function isInMultiArray($array, $key, $value){
        $counter = 0;
        if(count($array) > 0){
            foreach($array as $subKey => $subArray){
                 if($subArray[$key] == $value){
                      $counter += 1;
                 }
            }
            return ($counter > 0) ? true : false;
        }else{
            return false;
        }
   }
   
  
   
    public function getChatMessages($club_id = NULL, $club_admin_id = NULL, $email = NULL, $except_me = NULL, $new_only = NULL, $old_chats = NULL, $lastno = NULL){
        $session = $this->getRequest()->getSession();
        if($_SERVER['SERVER_NAME'] == 'localhost'){
            $program_host="localhost";
            $program_uname="root";
            $program_pass="";
            $program_database = "healthy_life_project_ohwp";
        }else{
            $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
            $program_uname="hlproot";
            $program_pass="_jWBqa)67?BJq(j-";
            $program_database = "hlp12weekProgram";
        }
        
        $program_connection=mysqli_connect($program_host,$program_uname,$program_pass, $program_database) or die("Database Connection Failed");
//        $original_connection=mysqli_connect($original_host,$original_uname,$original_pass, $original_database) or die("Database Connection Failed");
        
        $condition = '';
        if($club_id != NULL){
            $club_id = filter_var($club_id, FILTER_SANITIZE_NUMBER_INT);
            if($condition != ''){
                $condition .= " AND p.club_id = " . $club_id ;
            }else{
                $condition .= " WHERE p.club_id = " . $club_id ;
            }
        }
        
        if($club_admin_id != NULL){
            $club_admin_id = filter_var($club_admin_id, FILTER_SANITIZE_NUMBER_INT);
            if($condition != ''){
                $condition .= " AND p.club_admin_id = " . $club_admin_id ;
            }else{
                $condition .= " WHERE p.club_admin_id = " . $club_admin_id ;
            }
        }
        
        if($email != NULL){
            if($condition != ''){
                $condition .= " AND p.client_email = '". $email . "'" ;
            }else{
                $condition .= " WHERE p.client_email = '". $email . "'" ;
            }
        }
        
        if($except_me != NULL){
            $except_me = filter_var($except_me, FILTER_SANITIZE_NUMBER_INT);
            if($condition != ''){
                $condition .= " AND (p.sender_type ='client' OR p.club_admin_id != " . $except_me .")";
            }else{
                $condition .= " WHERE (p.sender_type ='client' OR p.club_admin_id != " . $except_me .")" ;
            }
        }
        
        if($new_only != NULL){
            $myseenmessages = $this->getClubAdminSeenMessages($session->get("club_admin_id"));
            
            if(count($myseenmessages) > 0){
                $myseenmessages = json_decode($myseenmessages, true);
                $ids = join(',',$myseenmessages);  
                if($condition != ''){
                    $condition .= " AND p.chat_id NOT IN (". $ids . ")" ;
                }else{
                    $condition .= " WHERE p.chat_id NOT IN (". $ids . ")" ;
                }
            }
        }
        $limit = '';
        if($old_chats != NULL && $lastno != NULL){
            $limit = 'DESC LIMIT '. $lastno . ',30';
        }else{
            $limit = 'DESC LIMIT 30';
        }
        
        $program_query = mysqli_query($program_connection,
                "SELECT * FROM tbl_chatbox p ". $condition . " ORDER BY p.chat_datetime ". $limit);
        
        
        $data = [];
        while($row = mysqli_fetch_array($program_query)){
            $data[] = $row;
        }
        
        if($old_chats != NULL && $lastno != NULL){
            return $data;
        }else{
            return array_reverse($data);
        }
    }
    
    public function getClubAdminSeenMessages($club_admin_id){
        $club_admin_id = filter_var($club_admin_id, FILTER_SANITIZE_NUMBER_INT);
        
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:SeenChatMessages');
        $query = $cust->createQueryBuilder('p')
            ->where('p.club_admin_id = :club_admin_id')
            ->setParameter('club_admin_id', $club_admin_id)
            ->getQuery();
        $data = $query->getArrayResult();
        return (count($data) > 0) ? $data[0]['message_ids'] : array();
    }

    public function addMessageToSeenMsgsAction(){
        $session = $this->getRequest()->getSession();
        if($session->get('club_admin_id') == ''){
            return new Response('session_expired');
        }
        $em = $this->getDoctrine()->getEntityManager();
        $model = $em->getRepository('AcmeHeadOfficeBundle:SeenChatMessages')->findOneBy(array('club_admin_id'=>$session->get("club_admin_id")));
        if(count($model) <= 0){
            $model  = new SeenChatMessages();
        }
        $model->setClubAdminId($session->get("club_admin_id"));
        $seen = $model->getMessageIds();
        $seen = json_decode($seen, true);
        $seen = (count($seen) > 0) ? $seen : array();
        $messageids = [];
        
        $_POST['chat_id'] = filter_var($_POST['chat_id'], FILTER_SANITIZE_NUMBER_INT);
        
        if(!in_array($_POST['chat_id'], $seen)){
            $messageids[] = $_POST['chat_id'];
        }
        
        $seen = (count($messageids) > 0) ? array_merge($seen, $messageids) : $seen;
        $model->setMessageIds(json_encode($seen));
        $em->persist($model);
        $em->flush();
        
        return new Response("success");
    }
    
    public function emailUnreadMsgsAction(){
        if($_SERVER['SERVER_NAME'] == 'localhost'){
            $program_host="localhost";
            $program_uname="root";
            $program_pass="";
            $program_database = "healthy_life_project_ohwp";
        }else{
            $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
            $program_uname="hlproot";
            $program_pass="_jWBqa)67?BJq(j-";
            $program_database = "hlp12weekProgram";
        }
        $mysqli = new \Mysqli($program_host, $program_uname, $program_pass, $program_database);
        $result = $mysqli->query("SELECT club_id, club_admin_id, club_admin_name, client_email, client_fname
            FROM tbl_chatbox 
            WHERE s_seen = 0 AND s_email_sent = 0 AND sender_type = 'coach' AND
            (TIMESTAMPDIFF(MINUTE, chat_datetime,DATE_SUB(NOW(), INTERVAL -11 HOUR) ) > 5 AND TIMESTAMPDIFF(MINUTE, chat_datetime,DATE_SUB(NOW(), INTERVAL -11 HOUR) ) < 20)
            GROUP BY club_admin_id,client_email
            ORDER BY chat_datetime DESC");
        // 
        while ($row = $result->fetch_assoc()) {
            $data[] = $row;
            $msgs = $this->getClubAdminUnreadChatMsgs($row['club_admin_id'], $row['client_email']);
            $admin = $this->getClubAdminUserById($row['club_admin_id']);
            $club = $this->getClubById($row['club_id']);
            $content = $this->renderView('AcmeClubBundle:Chatbox:email_msgs_body.html.twig',
                            array(
                                'msgs' => $msgs,
                                'details' => $row,
                                'club'=> $club
                            ));
            
            $this->sendEmail($row['client_email'], '', $this->container->getParameter('sender_email_address'), 
                        "Message from " . $row['club_admin_name'] . " (".$club['club_name'].")", $content);
//            $this->sendEmail($row['client_email'], '', $admin['email'], 
//                        "Message from " . $row['club_admin_name'] . " (".$club['club_name'].")", $content);
        }
        
        $result->close();
        $mysqli->close();
        return new Response("success");
    }
    
    public function emailClientUnreadMsgsToCoachAction(){
        if($_SERVER['SERVER_NAME'] == 'localhost'){
            $program_host="localhost";
            $program_uname="root";
            $program_pass="";
            $program_database = "healthy_life_project_ohwp";
        }else{
            $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
            $program_uname="hlproot";
            $program_pass="_jWBqa)67?BJq(j-";
            $program_database = "hlp12weekProgram";
        }
        $mysqli = new \Mysqli($program_host, $program_uname, $program_pass, $program_database);
        
        
        $result = $mysqli->query("SELECT club_id, club_admin_id, club_admin_name, client_email, client_fname
            FROM tbl_chatbox 
            WHERE s_email_sent = 0 AND sender_type = 'client' 
            AND (TIMESTAMPDIFF(MINUTE, chat_datetime,DATE_SUB(NOW(), INTERVAL -11 HOUR) ) > 5 AND TIMESTAMPDIFF(MINUTE, chat_datetime,DATE_SUB(NOW(), INTERVAL -11 HOUR) ) < 20)
            GROUP BY club_admin_id,client_email
            ORDER BY chat_datetime DESC");
        // 
        while ($row = $result->fetch_assoc()) {
            $em = $this->getDoctrine()->getManager();
            $seen_msgs = $em->getRepository('AcmeHeadOfficeBundle:SeenChatMessages')
                        ->findOneBy(array(
                            'club_admin_id' => $row['club_admin_id'],
                        ));
            
            $seen_msgs_ids = $seen_msgs->getMessageIds();
            $seen_msgs_ids = json_decode($seen_msgs_ids, true);
            $msgs = $this->getClientUnreadChatMsgs($row['client_email'], $seen_msgs_ids);
            $admin = $this->getClubAdminUserById($row['club_admin_id']);
            $club = $this->getClubById($row['club_id']);
            $content = $this->renderView('AcmeClubBundle:Chatbox:email_msgs_body.html.twig',
                            array(
                                'msgs' => $msgs,
                                'details' => $row,
                                'club'=> $club,
                                's_to_admin'=> 1
                            ));
            
            $this->sendEmail($admin['email'], '', $this->container->getParameter('sender_email_address'), 
                        "Message from " . $row['client_fname'] . " (".$club['club_name']."'s client)", $content);
//            $this->sendEmail($admin['email'], '', $row['client_email'], 
//                        "Message from " . $row['client_fname'] . " (".$club['club_name']."'s client)", $content);
        }
        
        $result->close();
        $mysqli->close();
        return new Response("success");
    }
    
    function getClubAdminUnreadChatMsgs($club_admin_id, $client_email){
        if($_SERVER['SERVER_NAME'] == 'localhost'){
            $program_host="localhost";
            $program_uname="root";
            $program_pass="";
            $program_database = "healthy_life_project_ohwp";
        }else{
            $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
            $program_uname="hlproot";
            $program_pass="_jWBqa)67?BJq(j-";
            $program_database = "hlp12weekProgram";
        }
        $mysqli = new \Mysqli($program_host, $program_uname, $program_pass, $program_database);
        $result = $mysqli->query("SELECT *
            FROM tbl_chatbox 
            WHERE s_seen = 0 AND s_email_sent = 0 AND club_admin_id = ". $club_admin_id ." AND sender_type = 'coach' AND client_email ='". $client_email ."' AND
            (TIMESTAMPDIFF(MINUTE, chat_datetime,DATE_SUB(NOW(), INTERVAL -11 HOUR) ) > 5 AND TIMESTAMPDIFF(MINUTE, chat_datetime,DATE_SUB(NOW(), INTERVAL -11 HOUR) ) < 20)
            ORDER BY chat_datetime ASC");
        
        $data = array();
        while ($row = $result->fetch_assoc()) {
            $data[] = $row;
            $this->updateEmailedChatMsgs($row['chat_id']);
        }
        
        $result->close();
        $mysqli->close();
        
        return $data;
    }
    
    function getClientUnreadChatMsgs($client_email, $seen_msgs_ids){
        if($_SERVER['SERVER_NAME'] == 'localhost'){
            $program_host="localhost";
            $program_uname="root";
            $program_pass="";
            $program_database = "healthy_life_project_ohwp";
        }else{
            $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
            $program_uname="hlproot";
            $program_pass="_jWBqa)67?BJq(j-";
            $program_database = "hlp12weekProgram";
        }
        $mysqli = new \Mysqli($program_host, $program_uname, $program_pass, $program_database);
        
        
        $seen_msgs_ids = implode("','", $seen_msgs_ids);
        $seen_msgs_ids = "'".$seen_msgs_ids."'";
        
        $result = $mysqli->query("SELECT *
            FROM tbl_chatbox 
            WHERE s_email_sent = 0 AND sender_type = 'client' AND client_email ='". $client_email ."' 
            AND chat_id NOT IN (".$seen_msgs_ids.")
            AND (TIMESTAMPDIFF(MINUTE, chat_datetime,DATE_SUB(NOW(), INTERVAL -11 HOUR) ) > 5 AND TIMESTAMPDIFF(MINUTE, chat_datetime,DATE_SUB(NOW(), INTERVAL -11 HOUR) ) < 20)
            ORDER BY chat_datetime ASC");
        
        $data = array();
        while ($row = $result->fetch_assoc()) {
            $data[] = $row;
            $this->updateEmailedChatMsgs($row['chat_id']);
        }
        
        $result->close();
        $mysqli->close();
        
        return $data;
    }
    
    public function updateEmailedChatMsgs($chat_id){
        if($_SERVER['SERVER_NAME'] == 'localhost'){
            $program_host="localhost";
            $program_uname="root";
            $program_pass="";
            $program_database = "healthy_life_project_ohwp";
        }else{
            $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
            $program_uname="hlproot";
            $program_pass="_jWBqa)67?BJq(j-";
            $program_database = "hlp12weekProgram";
        }
        $chat_id = filter_var($chat_id, FILTER_SANITIZE_NUMBER_INT);
        $mysqli = new \Mysqli($program_host, $program_uname, $program_pass, $program_database);
        $stmt = $mysqli->prepare("UPDATE tbl_chatbox SET s_email_sent = 1 WHERE chat_id = ?");
        $stmt->bind_param("s", $chat_id);
        $stmt->execute();
        $stmt->close();
        $mysqli->close();
    }
    
    
}
