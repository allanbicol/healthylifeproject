<?php

namespace Acme\ClubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;
//use Acme\CLSclientGovBundle\Json;

class LoginController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function loginAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        
        
        if($session->get('ho_admin_id') != '' && isset($_GET['club'])){
            $_GET['club'] = filter_var($_GET['club'], FILTER_SANITIZE_NUMBER_INT);
            $club = $this->getClubById($_GET['club']);
//            $session->set('email', $rows[0]['email']); 
//            $session->set('fname', $rows[0]['fname']); 
//            $session->set('lname', $rows[0]['lname']);
            $session->set('club_name', $club['club_name']);

            $session->set('club_id', $_GET['club']);
//            $session->set('ho_admin_id', $rows[0]['ho_admin_id']); 
//            $session->set('user_type', 'head-office-user');
            $session->set('user_role', 'admin');
            $session->set('show_first_login_msg', 0);

            $session->set('office-address', $club['address']);
            $session->set('office-postcode', $club['postcode']);
            $session->set('office-state', $club['state']);
            $session->set('office-city', $club['city']);


            $session->set('available_credit', $club['available_credit']);

            $session->set('my_clubs', array());
        }
        
        if($session->get('club_admin_id') != '' || $session->get('ho_admin_id') != ''){
            return $this->redirect($this->generateUrl('acme_club_dashboard'));
        }
        
        if(isset($_POST['email'])){
            
            // Check username and password
            if(!isset($_POST['email']) && !isset($_POST['password'])){
                return $this->redirect($this->generateUrl('acme_club_login'));
            }
            $error = 0; 
            if(trim($_POST['email']) == ''){
                $error += 1;
                $message = "Email address should not be blank.";
            }
            
            // check email validity
            if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) && $error == 0){
                $error += 1;
                $message = "Email ".$_POST['email']." is not a valid email address.";
            }
            
            if(trim($_POST['password']) == '' && $error == 0){
                $error += 1;
                $message = "Password should not be blank.";
            }
           
            if( isset($_POST['club']) ){
                $_POST['club'] = filter_var($_POST['club'], FILTER_SANITIZE_NUMBER_INT);
                // check government user
                $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:HeadOfficeAdmin');
                $query = $cust->createQueryBuilder('p')
                    ->where('p.email = :email AND p.password = :password AND p.status = :status')
                    ->setParameter('email', $_POST['email'])
                    ->setParameter('password', $mod->passGenerator($_POST['password']))
                    ->setParameter('status', 'active')
                    ->getQuery();
                $rows = $query->getArrayResult();
                
            }else{
                
                $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:ClubAdmin');
                $query = $cust->createQueryBuilder('p')
                    ->where('p.email = :email AND p.password = :password AND p.status = :status')
                    ->setParameter('email', $_POST['email'])
                    ->setParameter('password', $mod->passGenerator($_POST['password']))
                    ->setParameter('status', 'active')
                    ->getQuery();
                $rows = $query->getArrayResult();
            }
             
            if(count($rows) > 0 ){
                
//                $this->get('session')->clear();
                
                // set session
//                if(isset($_POST['remember_me'])){
//                    $this->container->get('session')->migrate($destroy = false, 2419200);
////                    $this->container->get('session')->getMetadataBag()->stampNew(31536000);
//                }else{
//                    $this->container->get('session')->migrate($destroy = false, 3600);
//                }
                
				
		if( isset($_POST['club']) ){
                    $club = $this->getClubById($_POST['club']);
                    $clubs = array();
                    if (count($club) <= 0){
                        return $this->redirect($this->generateUrl('acme_club_dashboard'));
                    }
                }else{
                    $club = $this->getClubById($rows[0]['club_id']);
                    $clubs = $this->getClubsByAdminEmail($rows[0]['email']);
                }
                
                
                $session->set('email', $rows[0]['email']); 
                $session->set('fname', $rows[0]['fname']); 
                $session->set('lname', $rows[0]['lname']);
                $session->set('club_name', $club['club_name']);
                
                if( isset($_POST['club']) ){ 
                    $session->set('club_id', $_POST['club']);
                    $session->set('ho_admin_id', $rows[0]['ho_admin_id']); 
                    $session->set('user_type', 'head-office-user');
                    $session->set('user_role', 'admin');
                    $session->set('show_first_login_msg', 0);
                }else{
                    $session->set('club_id', $rows[0]['club_id']);
                    $session->set('club_admin_id', $rows[0]['club_admin_id']); 
                    $session->set('user_type', 'club-user');
                    $session->set('user_role', $rows[0]['role']);
                    $session->set('show_first_login_msg', $rows[0]['show_first_login_msg']);
                }
                
                $session->set('office-address', $club['address']);
                $session->set('office-postcode', $club['postcode']);
                $session->set('office-state', $club['state']);
                $session->set('office-city', $club['city']);
                
                
                $session->set('available_credit', $club['available_credit']);
                
                $session->set('my_clubs', $clubs);
                
                if( !isset($_POST['club']) ){
                    // last login
                    $em = $this->getDoctrine()->getManager();
                    $em->getConnection()->beginTransaction(); 
                    $model = $em->getRepository('AcmeHeadOfficeBundle:ClubAdmin')->findOneBy(array('club_admin_id'=>$rows[0]['club_admin_id']));
                    $model->setLastLoginDatetime($datetime->format('Y-m-d H:i:s'));
                    $model->setSLogin(1);
                    $model->setShowFirstLoginMsg(0);
                    $em->persist($model);
                    $em->flush();
                    $em->getConnection()->commit(); 

                    $this->deleteClientTestAnswers($rows[0]['club_admin_id']);
                }
                // SET ACTIVITY
                if( isset($_POST['club']) ){
                    $details = $session->get('fname') . " " . $session->get('lname') . ", head office admin, has logged into " . $session->get('club_name') . ".";
                }else{
                    $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " has logged into the site.";
                }
                $this->setActivity($session->get('club_admin_id'), 'club-admin', $details);
                
                return $this->redirect($this->generateUrl('acme_club_dashboard'));
            }else{
                $error += 1;
                $message = "The login details you've entered <br/>don't match any in our system.";
            }
            
            if($error > 0){
                // set flash message
                $this->get('session')->getFlashBag()->add(
                        'error',
                        $message
                    );
                
                return $this->render('AcmeClubBundle:Login:login.html.twig',
                    array('post'=> $_POST)
                );
            }
        }
        return $this->render('AcmeClubBundle:Login:login.html.twig');
    }

    public function logoutAction(){
        $session = $this->getRequest()->getSession();
        
        if($session->get('club_admin_id') != ''){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            $model = $em->getRepository('AcmeHeadOfficeBundle:ClubAdmin')->findOneBy(array('club_admin_id'=>$session->get('club_admin_id')));
            $model->setSLogin(0);
            $em->persist($model);
            $em->flush();
            $em->getConnection()->commit(); 

            $this->deleteClientTestAnswers($session->get('club_admin_id'));

            // SET ACTIVITY
            $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " has logged out from the site.";
            $this->setActivity($session->get('club_admin_id'), 'club-admin', $details);
        }
        $this->get('session')->clear(); // clear all sessions
        
        return $this->redirect($this->generateUrl('acme_club_login'));
    }
    
    public function deleteClientTestAnswers($club_admin_id){
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        
        $statement = $connection->prepare("DELETE a FROM tbl_lifestyle_questionnaire_client_answer a 
            LEFT JOIN tbl_club_client c ON c.club_client_id = a.club_client_id
            WHERE a.status = 'draft' AND c.club_admin_id = " . $club_admin_id);
        $statement->execute();
    }
}
