<?php

namespace Acme\ClubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

use Acme\HeadOfficeBundle\Entity\Club;
use Acme\HeadOfficeBundle\Entity\ClubAdmin;
use \Acme\HeadOfficeBundle\Entity\ClientCategory;

class ClubAdminController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    
    public function clubAdminAction()
    {
        $session = $this->getRequest()->getSession();
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        $session->set('active_page', 'admin' ); 
        
        return $this->render('AcmeClubBundle:ClubAdmin:club_admin.html.twig',
                array('club_admins'=> $this->getClubAdminsByClubId($session->get('club_id')),
                    'get' => $_GET)
                );
    }
    
    
    public function addEditClubAdminAction($slug)
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        if($session->get('user_role') == 'site-admin' || $session->get('user_role') == 'admin'){
            
        }else{
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        $session->set('active_page', 'admin' ); 
        
        if(isset($_POST['email'])){
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            
            
            if(strtolower(trim($slug)) == 'new'){
                $model = new ClubAdmin();
                $model->setClubId($session->get('club_id'));
            }else{
                $model = $em->getRepository('AcmeHeadOfficeBundle:ClubAdmin')->findOneBy(array('club_admin_id'=>$_POST["club_admin_id"]));
            }
            
            $model->setFname($_POST['fname']);
            $model->setLname($_POST['lname']);
            if(strtolower(trim($slug)) == 'new'){
                $model->setRegisteredDate($datetime->format("Y-m-d H:i:s"));
                $model->setPassword($mod->passGenerator($_POST['password']));
            }elseif(strtolower(trim($slug)) == 'edit' && $_POST['password'] != ''){
                $model->setPassword($mod->passGenerator($_POST['password']));
            }
            $model->setEmail($_POST['email']);
            $model->setStatus($_POST['status']);
            $model->setRole($_POST['role']);
            $em->persist($model);
            $em->flush();
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            if($error_count == 0){
                if($mod->isEmailValid($_POST['email']) == false || ($_POST['password'] != $_POST['confirm_password'])){
                    $errors = array();
                }
                
                if(!$mod->isEmailValid($_POST['email'])){
                    $errors[] = array('message'=>$_POST['email'].' is not a valid email.');
                    $error_count += 1;
                }

                if($_POST['password'] != $_POST['confirm_password']){
                    $errors[] = array('message'=>'Password must be repeated exactly.');
                    $error_count += 1;
                }
            }
            
            if($error_count == 0){
                
                $em->getConnection()->commit(); 
                
//                if(strtolower(trim($slug)) == 'new'){
//                    $this->get('session')->getFlashBag()->add(
//                        'club-admin-success',
//                        $_POST['fname'] . ' ' . $_POST['lname'] . ' has been added successfully.'
//                    );
//                }else{
//                    $this->get('session')->getFlashBag()->add(
//                        'club-admin-success',
//                        $_POST['fname'] . ' ' . $_POST['lname'] . ' has been updated successfully.'
//                    );
//                }
                
                // SET ACTIVITY
                if(strtolower(trim($slug)) == 'new'){
                    $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " added " . $_POST['fname'] . ' ' . $_POST['lname'] . " as a new " . $_POST['role'] .".";
                }else{
                    $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " updated the details of " . $_POST['fname'] . ' ' . $_POST['lname'] . ", a " . $_POST['role'] .".";
                }
                
                if($session->get('club_admin_id') != ''){ 
                    $this->setActivity($session->get('club_admin_id'), 'club-admin', $details);
                }else{
                    $this->setActivity($session->get('ho_admin_id'), 'head-office-admin', $details);
                }

                //return $this->redirect($this->generateUrl('acme_club_admin_add_edit', array('slug' => 'edit')) . "?caid=".$model->getClubAdminId());
                return $this->redirect($this->generateUrl('acme_club_admin_area'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                if(strtolower(trim($slug)) == 'new'){
                    $this->get('session')->getFlashBag()->add(
                        'club-admin-error',
                        $errors
                    );
                }else{
                    $this->get('session')->getFlashBag()->add(
                        'club-admin-edit-error',
                        $errors
                    );
                }
                
                
                return $this->redirect($this->generateUrl('acme_club_admin_area'));
//                return $this->render('AcmeClubBundle:Admin:admin.html.twig',
//                        array('errors'=>$errors,
//                            'club_admins'=> $this->getClubAdminsByClubId($session->get('club_id')),
//                            'events'=> $this->getEvents($session->get('club_id')),
//                            'states' => $this->getStates(),
//                            'post'=>$_POST,
//                            'get'=>$_GET
//                        ));
            }
        }
        
        if(strtolower(trim($slug)) == 'new'){
            return $this->render('AcmeClubBundle:ClubAdmin:add_edit_club_admin.html.twig',
                    array(
                            'get' => $_GET,
                            'clubs' => $this->getClubs('active')
                        ));
        }else{
            $_GET['caid'] = intval($_GET['caid']);
            return $this->render('AcmeClubBundle:ClubAdmin:add_edit_club_admin.html.twig',
                    array(
                        'post'=> $this->getClubAdminUserById($_GET['caid']),
                        'get'=>$_GET,
                        'clubs' => $this->getClubs('active')
                    ));
        }
    }
    
    public function deleteAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        if($session->get('user_role') == 'site-admin' || $session->get('user_role') == 'admin'){
        }else{
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        if(isset($_POST['club_admin_id'])){
            $em = $this->getDoctrine()->getEntityManager();

            $_POST['club_admin_id'] = intval($_POST['club_admin_id']);
            $model = $em->getRepository('AcmeHeadOfficeBundle:ClubAdmin')->findOneBy(array('club_admin_id'=>$_POST["club_admin_id"]));
            $em->remove($model);
            $em->flush();


            $this->get('session')->getFlashBag()->add(
                    'success',
                    'Club admin has been deleted successfully.'
                );
            
            // SET ACTIVITY
            $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " deleted " . $model->getFname() . " " . $model->getLname() . ", a ". $model->getRole() .".";
//            $this->setActivity($session->get('club_admin_id'), 'club-admin', $details);
            if($session->get('club_admin_id') != ''){ 
                $this->setActivity($session->get('club_admin_id'), 'club-admin', $details);
            }else{
                $this->setActivity($session->get('ho_admin_id'), 'head-office-admin', $details);
            }
            
            //return $this->redirect($this->generateUrl('acme_club_admin') . "?cid=" . $_POST['club_admin_id']);
            return $this->redirect($this->generateUrl('acme_club_admin_area'));
            
        }
        
    }
    
    public function switchClubAction(){
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
		
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
		
		
        $_GET['clubs'] = (isset($_GET['clubs'])) ? intval($_GET['clubs']) : '';
        
        if($_GET['clubs'] != ''){
            $_GET['clubs'] = filter_var($_GET['clubs'], FILTER_SANITIZE_NUMBER_INT);
           $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:Club');
            $query = $cust->createQueryBuilder('p')
                    ->select("a.club_admin_id, a.fname, a.lname, 
                            p.club_id, p.club_name, p.address, p.postcode, 
                            p.state, p.city, p.available_credit")
                    ->leftJoin('AcmeHeadOfficeBundle:ClubAdmin', 'a', 'WITH', 'a.club_id = p.club_id')
                    ->where('a.email = :email AND p.club_id = :club_id')
                    ->setParameter('email', $session->get('email'))
                    ->setParameter('club_id', $_GET['clubs'])
                    ->getQuery();
            $data = $query->getArrayResult();

            if(count($data) > 0){
                $session->set('club_admin_id', $data[0]['club_admin_id']); 
                $session->set('fname', $data[0]['fname']); 
                $session->set('lname', $data[0]['lname']);

                $session->set('club_id', $data[0]['club_id']);
                $session->set('club_name', $data[0]['club_name']);

                $session->set('office-address', $data[0]['address']);
                $session->set('office-postcode', $data[0]['postcode']);
                $session->set('office-state', $data[0]['state']);
                $session->set('office-city', $data[0]['city']);

                $session->set('available_credit', $data[0]['available_credit']);
            }
        }

        return $this->redirect($this->generateUrl('acme_club_dashboard'));
    }
	
        
    public function deleteClientCategoryAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        if($session->get('user_role') != 'site-admin'){
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        if(isset($_POST['category_id'])){
            $em = $this->getDoctrine()->getEntityManager();

            $_POST['category_id'] = intval($_POST['category_id']);
            $model = $em->getRepository('AcmeHeadOfficeBundle:ClientCategory')->findOneBy(array('id'=>$_POST["category_id"]));
            $em->remove($model);
            $em->flush();


            $this->get('session')->getFlashBag()->add(
                    'success',
                    'Client category has been deleted successfully.'
                );
            
            // SET ACTIVITY
            $details = $session->get('fname') . " " . $session->get('lname') . " of client category with an id of" . $_POST["category_id"].".";
//            $this->setActivity($session->get('club_admin_id'), 'club-admin', $details);
            if($session->get('club_admin_id') != ''){ 
                $this->setActivity($session->get('club_admin_id'), 'club-admin', $details);
            }else{
                $this->setActivity($session->get('ho_admin_id'), 'head-office-admin', $details);
            }
            
            //return $this->redirect($this->generateUrl('acme_club_admin') . "?cid=" . $_POST['club_admin_id']);
            return $this->redirect($this->generateUrl('acme_club_admin_area'));
            
        }
        
    }
    
    public function addEditClientCategoryAction($slug)
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        if($session->get('user_role') != 'site-admin'){
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        if(isset($_POST['category_name'])){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            
            
            if(strtolower(trim($slug)) == 'new'){
                $model = new ClientCategory;
            }else{
                $model = $em->getRepository('AcmeHeadOfficeBundle:ClientCategory')->findOneBy(array('id'=>$_POST["id"]));
            }
            
            $model->setCategoryName($_POST['category_name']);
            $model->setColor($_POST['color']);
            $model->setClubId($session->get('club_id'));
            $em->persist($model);
            $em->flush();
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            
            
            if($error_count == 0){
                
                $em->getConnection()->commit(); 
                   
//                // SET ACTIVITY
//                if(strtolower(trim($slug)) == 'new'){
//                    $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " added " . $_POST['fname'] . ' ' . $_POST['lname'] . " as a new " . $_POST['role'] .".";
//                }else{
//                    $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " updated the details of " . $_POST['fname'] . ' ' . $_POST['lname'] . ", a " . $_POST['role'] .".";
//                }
//                $this->setActivity($session->get('club_admin_id'), 'club-admin', $details);

                //return $this->redirect($this->generateUrl('acme_club_admin_add_edit', array('slug' => 'edit')) . "?caid=".$model->getClubAdminId());
                return $this->redirect($this->generateUrl('acme_club_admin_area'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                if(strtolower(trim($slug)) == 'new'){
                    $this->get('session')->getFlashBag()->add(
                        'club-admin-error',
                        $errors
                    );
                }else{
                    $this->get('session')->getFlashBag()->add(
                        'club-admin-edit-error',
                        $errors
                    );
                }
                
               
                return $this->render('AcmeClubBundle:Admin:admin.html.twig',
                        array('errors'=>$errors,
                            'club_admins'=> $this->getClubAdminsByClubId($session->get('club_id')),
                            'events'=> $this->getEvents($session->get('club_id')),
                            'states' => $this->getStates(),
                            'post'=>$_POST,
                            'get'=>$_GET
                        ));
            }
        }

    }
    
}
