<?php

namespace Acme\ClubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

use Acme\HeadOfficeBundle\Entity\ClientTest;

class HelpController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function getHelpAction()
    {
        $help_fields = array(
            'weight',
            'height',
            'bmi',
            'physical_activity',
            'smoking',
            'alcohol',
            'nutrition',
            'mental_health',
            'risk_profile',
            'physiological_vo2',
            'physiological_balance',
            'physiological_squat',
            'physiological_situp',
            'biometric_upper',
            'biometric_waist',
            'biometric_body_fat',
            'biometric_blood_pressure',
            'biometric_blood_oxygen',
            'squat_1min',
            'push_ups_1min',
            'sit_ups_1min',
            'row_500m',
            'standing_broad_jump',
            'time_trial',
            'squat_1rm',
            'bench_press_1rm',
            'deadlift_1rm',
            'overhead_press'
        );
        
        if( !in_array($_POST['field'], $help_fields)){
            return new Response ('Invalid request!');
        }
        
        $help = $this->getHelpByField($_POST['field']);
        
        if(isset($help['html'])){
            return new Response($help['html']);
        }else{
            return new Response('');
        }
    }
    
    public function helpAction(){
        $session = $this->getRequest()->getSession();
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        $session->set('active_page', 'help' ); 
        $club_admin_details = ($session->get('club_admin_id') != '') ? $this->getClubAdminUserById($session->get('club_admin_id')) : array();
        return $this->render('AcmeClubBundle:Help:help.html.twig',
                array('club_admin_details'=> $club_admin_details)
                );
    }

}
