<?php

namespace Acme\ClubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;
use Acme\HeadOfficeBundle\Entity\ClientTest;
use Acme\HeadOfficeBundle\Entity\ClubClient;
class DashboardController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function dashboardAction()
    {
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        $session->set('active_page', 'dashboard' ); 
        
        
        
        $keyword_search = (isset($_GET['keyword'])) ? $_GET['keyword'] : '';
        $sort = (isset($_GET['sort'])) ? $_GET['sort'] : NULL;
        
        $data = array(
            date("M",strtotime("-3 Months"))=> 0,
            date("M",strtotime("-2 Months")) => 0,
            date("M",strtotime("-1 Months")) => 0,
            date("M") => 0,
            );
        
        $club_admin_id = ($session->get('user_role') == 'coach') ? $session->get('club_admin_id') : NULL;
        
        if($session->get('club_id') != ''){
            $em = $this->getDoctrine()->getManager();
            $club_update = $em->getRepository('AcmeHeadOfficeBundle:Club')
                        ->findOneBy(array(
                            'club_id' => $session->get('club_id')
                        ));
            $club_update->setLastLoginDatetime( $datetime->format('Y-m-d H:i:s') );
            $em->persist($club_update);
            $em->flush();
        }
        if($session->get('club_admin_id') != ''){
            $club_admin_update = $em->getRepository('AcmeHeadOfficeBundle:ClubAdmin')
                        ->findOneBy(array(
                            'club_admin_id'=>$session->get('club_admin_id')
                        ));
            $club_admin_update->setLastLoginDatetime($datetime->format('Y-m-d H:i:s'));
            $em->persist($club_admin_update);
            $em->flush();
        }
        
        return $this->render('AcmeClubBundle:Dashboard:dashboard.html.twig',
                array(
                    'clients' => $this->getClubClientsWith12weekProgramDetails(
                        'active', 
                        $session->get('club_id'),
                        0,
                        $this->container->getParameter('ajax_default_load_number'),
                        $sort,
                        $keyword_search,
                        $club_admin_id
                        ),
                    'data'=> $data
                    )
                );
    }
    
    public function generateSummaryAction(){
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return new Response("session_expired");
        }
        
        $filter_daterange = array( 
                    'from' => date('Y-m-d',strtotime('first day of ' . date("F Y",strtotime("-3 Months")) )) . ' 00:00:00',
                    'to' => $datetime->format("Y-m-d H:i:s"),
                );
        $club_admin_id = ($session->get('user_role') == 'coach') ? $session->get('club_admin_id') : NULL;
        $summary = $this->getReportSummary(
                $session->get('club_id'),
                $filter_daterange,
                $club_admin_id
            );
        $result = array();
        $years_added = 0;
        $cm_lost = 0;
        $kg_lost = 0;
        $client = 0;
        
        for($i=0, $count = count($summary); $i<$count; $i++){
            if($client != $summary[$i]['client_id']){
                $years_added += $summary[$i]['years_added'];
                $cm_lost += $summary[$i]['cm_lost'];
                $kg_lost += $summary[$i]['kg_lost'];
                $client = $summary[$i]['client_id'];
            }

        }
        $result = array();
        $result[] = array(
            'total_years_added'=> $years_added,
            'total_cm_lost'=> $cm_lost,
            'total_kg_lost'=> $kg_lost,
        );
        
        
        return new Response(json_encode($result));
    }
    
    public function generateSummaryTestAction(){
        $start = microtime(true);
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return new Response("session_expired");
        }
        
        $filter_daterange = array( 
                    'from' => date('Y-m-d',strtotime('first day of ' . date("F Y",strtotime("-3 Months")) )) . ' 00:00:00',
                    'to' => $datetime->format("Y-m-d H:i:s"),
                );
        $club_admin_id = ($session->get('user_role') == 'coach') ? $session->get('club_admin_id') : NULL;
        $summary = $this->getReportSummary(
                $session->get('club_id'),
                $filter_daterange,
                $club_admin_id
            );
        
        $result = array();
        $years_added = 0;
        $cm_lost = 0;
        $kg_lost = 0;
        $client = 0;
        
        for($i=0, $count = count($summary); $i<$count; $i++){
            if($client != $summary[$i]['client_id']){
                $years_added += $summary[$i]['years_added'];
                $cm_lost += $summary[$i]['cm_lost'];
                $kg_lost += $summary[$i]['kg_lost'];
                $client = $summary[$i]['client_id'];
            }
            
        }
        $result = array();
        $result[] = array(
            'total_years_added'=> $years_added,
            'total_cm_lost'=> $cm_lost,
            'total_kg_lost'=> $kg_lost,
        );
        
        echo json_encode($result);
        $time_elapsed_secs = microtime(true) - $start;
        echo '<br> time elapsed = ' . $time_elapsed_secs;
        exit();
    }
    
    public function generateDataAction(){
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return new Response("session_expired");
        }
        
        $club_admin_id = ($session->get('user_role') == 'coach') ? $session->get('club_admin_id') : NULL;
        
        $data = array(
            date("M",strtotime("-3 Months"))=> $this->getReports($session->get('club_id'), array('from'=> date('Y-m-d',strtotime('first day of ' . date("F Y",strtotime("-3 Months")) )) . ' 00:00:00', 'to'=> date('Y-m-d',strtotime('last day of ' . date("F Y",strtotime("-3 Months")) )) . ' 23:59:59'), $club_admin_id), 
            date("M",strtotime("-2 Months")) => $this->getReports($session->get('club_id'), array('from'=> date('Y-m-d',strtotime('first day of ' . date("F Y",strtotime("-2 Months")) )) . ' 00:00:00', 'to'=> date('Y-m-d',strtotime('last day of ' . date("F Y",strtotime("-2 Months")) )) . ' 23:59:59'), $club_admin_id), 
            date("M",strtotime("-1 Months")) => $this->getReports($session->get('club_id'), array('from'=> date('Y-m-d',strtotime('first day of ' . date("F Y",strtotime("-1 Months")) )) . ' 00:00:00', 'to'=> date('Y-m-d',strtotime('last day of ' . date("F Y",strtotime("-1 Months")) )) . ' 23:59:59'), $club_admin_id), 
            date("M") => $this->getReports($session->get('club_id'), array('from'=> date('Y-m-d',strtotime('first day of this month')) . ' 00:00:00', 'to'=> date('Y-m-d',strtotime('last day of this month')) . ' 23:59:59'), $club_admin_id)
            );
        
        $yearsadded = array(
            date("M",strtotime("-3 Months"))=> (isset($data[ date("M",strtotime("-3 Months")) ][0]['total_years_added']) && $data[ date("M",strtotime("-3 Months")) ][0]['total_years_added'] > 0) ? $data[ date("M",strtotime("-3 Months")) ][0]['total_years_added'] : 0, 
            date("M",strtotime("-2 Months")) => (isset($data[ date("M",strtotime("-2 Months")) ][0]['total_years_added']) && $data[ date("M",strtotime("-2 Months")) ][0]['total_years_added'] > 0) ? $data[ date("M",strtotime("-2 Months")) ][0]['total_years_added'] : 0, 
            date("M",strtotime("-1 Months")) => (isset($data[ date("M",strtotime("-1 Months")) ][0]['total_years_added']) && $data[ date("M",strtotime("-1 Months")) ][0]['total_years_added'] > 0) ? $data[ date("M",strtotime("-1 Months")) ][0]['total_years_added'] : 0,
            date("M") => (isset($data[ date("M") ][0]['total_years_added']) && $data[ date("M") ][0]['total_years_added'] > 0) ? $data[ date("M") ][0]['total_years_added'] : 0
        );
        
        $kglost = array(
            date("M",strtotime("-3 Months"))=> (isset($data[ date("M",strtotime("-3 Months")) ][0]['total_kg_lost']) && $data[ date("M",strtotime("-3 Months")) ][0]['total_kg_lost'] > 0) ? $data[ date("M",strtotime("-3 Months")) ][0]['total_kg_lost'] : 0, 
            date("M",strtotime("-2 Months")) => (isset($data[ date("M",strtotime("-2 Months")) ][0]['total_kg_lost']) && $data[ date("M",strtotime("-2 Months")) ][0]['total_kg_lost'] > 0) ? $data[ date("M",strtotime("-2 Months")) ][0]['total_kg_lost'] : 0, 
            date("M",strtotime("-1 Months")) => (isset($data[ date("M",strtotime("-1 Months")) ][0]['total_kg_lost']) && $data[ date("M",strtotime("-1 Months")) ][0]['total_kg_lost'] > 0) ? $data[ date("M",strtotime("-1 Months")) ][0]['total_kg_lost'] : 0,
            date("M") => (isset($data[ date("M") ][0]['total_kg_lost']) && $data[ date("M") ][0]['total_kg_lost'] > 0) ? $data[ date("M") ][0]['total_kg_lost'] : 0
        );
        
        $response = array(
            'yearsadded'=>$yearsadded,
            'kglost'=>$kglost
        );
        return new Response(json_encode($response));
    }
    
    public function update12weekProgramClientProgressAction(){
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return new Response("session_expired");
        }
        
        $session->set('show_first_login_msg', 0);
        
        
        if($_SERVER['SERVER_NAME'] == 'localhost'){
            $program_host="localhost";
            $program_uname="root";
            $program_pass="";
            $program_database = "healthy_life_project_ohwp";
        }else{
            $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
            $program_uname="hlproot";
            $program_pass="_jWBqa)67?BJq(j-";
            $program_database = "hlp12weekProgram";

        }

        $program_connection=mysqli_connect($program_host,$program_uname,$program_pass, $program_database) or die("Database Connection Failed");
        $mysqli = new \Mysqli($program_host, $program_uname, $program_pass, $program_database);
        
        $emails = implode('","', $_GET['emails']);
//        $emails = mysqli_real_escape_string($mysqli,$emails); 
        
        $program_query = mysqli_query($program_connection,
            'SELECT c.email, c.current_week, c.program_type FROM tbl_client c WHERE c.email IN ("'.$emails.'")');
            
        $data = [];
        while($row = mysqli_fetch_array($program_query)){
            $data[] = array(
                    'email'=>$row['email'],
                    'week_total_actions' => $this->getProgramClientSnapshotsDayItems( $row['email'], $row['current_week'], NULL, NULL, 'quote',true, $row['program_type']),
                    'week_complete_actions' => $this->getProgramClientSnapshotsDayItems( $row['email'], $row['current_week'] , NULL, 1, NULL,true, $row['program_type']),
//                    'overall_actions' => $this->getProgramClientDayItems( $row['email'], NULL, NULL, NULL, 'quote',true),
//                    'overall_complete_actions' => $this->getProgramClientDayItems( $row['email'], NULL , NULL, 1, NULL, true)
                );
        }
        
        
        return new Response(json_encode($data));

    }
    
    public function updateClientSnapshotsSummaryResultsAction(){
        $session = $this->getRequest()->getSession();
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return new Response("session_expired");
        }
        
        $cust = $this->getDoctrine()->getEntityManager();
        $connection = $cust->getConnection();
        
        $client_ids = array();
        for($i=0, $count = count($_GET['clients']); $i<$count; $i++){
            $client_ids[] = filter_var($_GET['clients'][$i], FILTER_SANITIZE_NUMBER_INT);
        }
        $club_clients_ids = implode(",", $client_ids);
        
        $statement = $connection->prepare("SELECT 
                    t.club_client_id,
                    t.healthy_life_score,
                    t.years_added,
                    (SELECT COUNT(t2.client_test_id) FROM tbl_client_test t2 WHERE t2.club_client_id = t.`club_client_id`) AS total_tests
                FROM tbl_client_test t 
                WHERE t.status = 'finalised' AND t.club_client_id IN ($club_clients_ids) 
                AND t.`test_datetime_finalised` = (SELECT MAX(t2.test_datetime_finalised)
                                                FROM tbl_client_test t2
                                                WHERE t2.club_client_id = t.club_client_id)");
        
        
        $statement->execute();
        $data = $statement->fetchAll();
        
//        print_r( $clients ); exit();
//        
//        $recent_test = $this->getClientTestHistory($_GET['client_id'], 'recent', 'finalised');
//        $all_test = $this->getClientTestHistory($_GET['client_id'], 'all', 'finalised', NULL, NULL, true);
//        $data = array(
//            'recent_test'=> $recent_test,
//            'all_test'=> $all_test
//        );
        
        return new Response(json_encode($data));

    }
    
    public function updateClientSnapshotsSummaryResultsTestAction(){
        $start = microtime(true);
        $session = $this->getRequest()->getSession();
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return new Response("session_expired");
        }
        $cust = $this->getDoctrine()->getEntityManager();
        $connection = $cust->getConnection();
        
        $client_ids = array();
        for($i=0, $count = count($_GET['clients']); $i<$count; $i++){
            $client_ids[] = filter_var($_GET['clients'][$i], FILTER_SANITIZE_NUMBER_INT);
        }
        $club_clients_ids = implode(",", $client_ids);
        
        $statement = $connection->prepare("SELECT 
                    t.club_client_id,
                    t.healthy_life_score,
                    t.years_added,
                    (SELECT COUNT(t2.client_test_id) FROM tbl_client_test t2 WHERE t2.club_client_id = t.`club_client_id`) AS total_tests
                FROM tbl_client_test t 
                WHERE t.status = 'finalised' AND t.club_client_id IN ($club_clients_ids) 
                AND t.`test_datetime_finalised` = (SELECT MAX(t2.test_datetime_finalised)
                                                FROM tbl_client_test t2
                                                WHERE t2.club_client_id = t.club_client_id)");
        
        
        $statement->execute();
        $data = $statement->fetchAll();
        
        
        echo json_encode($data);
        $time_elapsed_secs = microtime(true) - $start;
        echo '<br> time elapsed = ' . $time_elapsed_secs;
        exit();
        
    }
    

    
    public function inviteFriendsAction(){
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return new Response("session_expired");
        }
        $program_type = '12week';
        $program_type_number = 12;
        
        if($_POST['program_type'] == '4week'){
            $program_type = '4week';
            $program_type_number = '4';
        }elseif($_POST['program_type'] == '8week'){
            $program_type = '8week';
            $program_type_number = '8';
        }else{
            $program_type = '12week';
            $program_type_number = '12';
        }

        
        $to = $_POST['recipients'];
        $name = $_POST['friend_name'];
        //$to = explode(",", $to);
        $from = $this->container->getParameter('site_email_address');
        $subject = 'Invitation - '.$program_type_number.' Week Healthy Life Program';
        $body = $this->renderView('AcmeClubBundle:Dashboard:invite_friends_email.html.twig',
                array(
                    'name'=>$name,
                    'program_type'=>$program_type
                )
            );
        
        
        $recipients = array();
        for($i=0; $i<count($to); $i++){
            if($mod->isEmailValid(trim($to[$i]))){
                $recipients[] = trim($to[$i]);
            }
        }
        
//        if(count($recipients) > 0){
//            $this->sendEmail($recipients, '', $from, $subject, $body);
//        }
        $this->sendEmail($to, '', $from, $subject, $body);
        
        if(isset($_POST['club_client_id'])){ 
            $_POST['club_client_id'] = filter_var($_POST['club_client_id'], FILTER_SANITIZE_NUMBER_INT);
            
            $em = $this->getDoctrine()->getEntityManager();
            $client = $em->getRepository('AcmeHeadOfficeBundle:ClubClient')->findOneBy(array('club_client_id'=> $_POST['club_client_id']));
            if(count($client) > 0){
                $client->setProgramType($program_type);
                $em->persist($client);
                $em->flush();
            }
        }
        
        return new Response("success");
    }
    
    public function inviteToTakeLifestyleQuestionnairesAction(){
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return new Response("session_expired");
        }
        
        $club_client_id = filter_var($_POST['client_id'], FILTER_SANITIZE_NUMBER_INT);
        
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('AcmeHeadOfficeBundle:ClubClient')
                        ->findOneBy(array(
                            'club_client_id' => $club_client_id
                        ));
        $client->setDoLifestyleTest(1);
        $em->persist($client);
        $em->flush();

        $draft_tests = $this->getClientTestHistory( $club_client_id, NULL, 'draft' );
        
        if(count($draft_tests) > 0){
            $draft_test_id = $draft_tests[0]['client_test_id'];
               
        }else{
            $em = $this->getDoctrine()->getManager();
            $drafttest = new ClientTest();
            $drafttest->setClubClientId( $club_client_id );
            $drafttest->setClubAdminId($session->get('club_admin_id'));
            $drafttest->setStatus('draft');
            $drafttest->setTestDatetime($datetime->format('Y-m-d H:i:s'));
            $em->persist($drafttest);
            $em->flush();

            $draft_test_id = $drafttest->getClientTestId();
        }

        $draft_test_id = base64_encode($draft_test_id);
        $to = array($_POST['email'] => $_POST['client_fname'] . ' ' . $_POST['client_lname']);
        
        //$to = explode(",", $to);
        $from = $this->container->getParameter('site_email_address');
        $subject = 'Invitation to Lifestyle Questionnaires';
        $body = $this->renderView('AcmeClubBundle:Dashboard:invite_to_lifestyle_questionnaires_email.html.twig',
                array(
                    'client_fname'=>$_POST['client_fname'],
                    'client_lname'=>$_POST['client_lname'],
                    'draft_test_id' => $draft_test_id
                )
            );
        
        
        $this->sendEmail($to, '', $from, $subject, $body);
        
        return new Response('success');
    }
    
    public function testSymfony2QueryAction(){
        $start = microtime(true);
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:ClubClient');
        $query = $cust->createQueryBuilder('p')
            ->where('p.email = :email')
            ->setParameter('email', 'leokarl2108@gmail.com')
            ->getQuery();
        print_r( $query->getArrayResult());
        
        $time_elapsed_secs = microtime(true) - $start;
        echo '<hr> time elapsed = ' . $time_elapsed_secs;
        exit();
    }
    
    public function testMysqliQueryAction(){
        $start = microtime(true);
        if($_SERVER['SERVER_NAME'] == 'localhost'){
            $program_host="localhost";
            $program_uname="root";
            $program_pass="";
            $program_database = "healthy_life_project_ohwp";

            $original_host="localhost";
            $original_uname="root";
            $original_pass="";
            $original_database = "healthy_life_project";
        }else{
            $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
            $program_uname="hlproot";
            $program_pass="_jWBqa)67?BJq(j-";
            $program_database = "hlp12weekProgram";
            
            $original_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
            $original_uname="hlproot";
            $original_pass="_jWBqa)67?BJq(j-";
            $original_database = "healthy_life_project";

        }
        
//        $program_connection=mysqli_connect($program_host,$program_uname,$program_pass, $program_database) or die("Database Connection Failed");
//        $mysqli = new \Mysqli($program_host, $program_uname, $program_pass, $program_database);
        $mysqli = new \Mysqli($original_host, $original_uname, $original_pass, $original_database);
        
        if ($mysqli->connect_errno) {
            printf("Connect failed: %s\n", $mysqli->connect_error);
            exit();
        }
        $query = $mysqli->query("Select * from tbl_club_client where email='leokarl2108@gmail.com'");
        
        $data = array(); 
        while ($row = $query->fetch_assoc()) {
            $data[] = $row;
        }
        print_r($data);
        
        $time_elapsed_secs = microtime(true) - $start;
        echo '<hr> time elapsed = ' . $time_elapsed_secs;
        exit();
    }
    
    
    
}
