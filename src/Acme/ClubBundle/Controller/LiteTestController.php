<?php

namespace Acme\ClubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

use Acme\HeadOfficeBundle\Entity\ClientTest;
use Acme\HeadOfficeBundle\Entity\LiteTest;
use Acme\HeadOfficeBundle\Entity\LifestyleQuestionnareClientAnswer;
use Acme\HeadOfficeBundle\Entity\EmailQueue;
use Acme\HeadOfficeBundle\Entity\ClubClient;
use Acme\HeadOfficeBundle\Entity\Notifications;

class LiteTestController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function emailLiteTestAction(){
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        require_once($root_dir.'/resources/dompdf/dompdf_config.inc.php');
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
//        $_POST["client_test_id"] = $_GET['client_test_id'];// uncomment this to view pdf in browser
        $_POST["client_test_id"] = intval($_POST["client_test_id"]);
                
        $test_details = $this->getClientTestDetailsById($_POST["client_test_id"]);
        
        $content = '';
        
        
        if(isset($test_details['status'])){
            if($test_details['status'] == 'finalised'){
                $content = $this->renderView('AcmeClubBundle:LiteTest:email_lite_test_pdf.html.twig',
                        array(
                            'site_url' => $root_dir,
                            'client_details' => $this->getClubClientById($test_details['club_client_id']),
                            'test_details' => $test_details,
                            'hl_expectancy_male' => $this->getHealthyLifeExpectancies('male'),
                            'hl_expectancy_female' => $this->getHealthyLifeExpectancies('female')
                        ));
            }else{
                return new Response("error");
            }
        }else{
            return new Response("error");
        }
        
        
        $stylesheet = file_get_contents($root_dir.'/css/email-test-dompdf.css'); /// here call you external css file 
        $stylesheet = '<style>'.$stylesheet.'</style>';
        
        $test_datetime = date_create($test_details['test_datetime_finalised']);
        $test_datetime = date_format($test_datetime,"d-m-Y");
        
        $file = $root_dir .'/temp/'. $test_details['fname'] . ' ' . $test_details['lname'] .' '.$test_datetime.'.pdf';
        
        $dompdf = new \DOMPDF();
        $dompdf->set_paper( 'A4' );
        $dompdf->load_html( $stylesheet . $content );
        $dompdf->render();
        
//        $dompdf->stream($test_details['fname'] . ' ' . $test_details['lname'] .' '.$test_datetime.'.pdf',array('Attachment'=>0));
        //save the pdf file on the server
        file_put_contents($file, $dompdf->output()); 
        
        /* uncomment below to view pdf in browser */
//        $response = new Response("success");
//        $response->setContent('success');
//        $response->headers->set('Content-Type', 'application/pdf');
//        return $response;
        
        
        $to = $_POST['recipients'];
        $to = explode(",", $to);
        $from = $this->container->getParameter('site_email_address');
        $subject = $test_details['fname'] . ' ' . $test_details['lname'] . ' -  Test Results PDF';
        $body = $this->renderView('AcmeClubBundle:Test:email_test_content.html.twig',
                        array(
                            'name' => $test_details['fname'] . ' ' . $test_details['lname'],
                            'test_date' => $test_details['test_datetime_finalised']
                        ));
        
        $attachments = array();
        
        
        $attachments[] = $file;
        
        $recipients = array();
        for($i=0; $i<count($to); $i++){
            if($mod->isEmailValid(trim($to[$i]))){
                $recipients[] = trim($to[$i]);
            }
        }
        
        if(count($recipients) > 0){
            $this->sendEmail($recipients, '', $from, $subject, $body, $attachments);
        }else{
            unlink($file);
        }
        
        return new Response("success");
    }
    
    public function viewLiteTestAction(){
        $session = $this->getRequest()->getSession();
        $formula = new Model\TestFormulas();
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        $session->set('active_page', 'test' ); 
        $test_details = $this->getClientTestDetailsById($_GET['id']);
        
        
        if(isset($test_details['status'])){
            if($test_details['status'] == 'finalised'){
                return $this->render('AcmeClubBundle:LiteTest:view_lite_test.html.twig',
                        array(
                            'client_details' => $this->getClubClientById($test_details['club_client_id']),
                            'test_details' => $test_details,
                            'hl_expectancy_male' => $this->getHealthyLifeExpectancies('male'),
                            'hl_expectancy_female' => $this->getHealthyLifeExpectancies('female')
                        ));
            }else{
                if(isset($_GET['aia'])){
                    return $this->redirect($this->generateUrl('acme_club_create_test') . '?id=' . $test_details['club_client_id'] .'&test='. $_GET['id'] . '&aia=1');
                }elseif( isset($_GET['type']) && $_GET['type'] == 'normal' ){
                    return $this->redirect($this->generateUrl('acme_club_create_test') . '?id=' . $test_details['club_client_id'] .'&test='. $_GET['id'] );
                }else{
                    return $this->redirect($this->generateUrl('acme_club_create_lite_test') . '?id=' . $test_details['club_client_id'] .'&test='. $_GET['id'] . '&type='. $test_details['test_type']);
                }
                
            }
        }else{
            return $this->render('AcmeClubBundle:Test:view_error.html.twig');
        }
    }
    
    // CREATE TEST
    public function createLiteTestAction()
    {
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $formula = new Model\TestFormulas();
        $mod = new Model\GlobalModel();
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        $session->set('active_page', 'lite-test' );
        
        if(count($_POST) > 0){
            // POST
            
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            $error = array();
            
            $club_client_id = intval($_POST['club_client_id']);
            $_POST['event_id'] = intval($_POST['event_id']);
            $_POST['weight'] = ($_POST['weight'] != '') ? intval($_POST['weight']) : '';
            $_POST['height'] = ($_POST['height'] != '') ? intval($_POST['height']) : '';
            $_POST['test_type'] = trim(strtolower($_POST['test_type']));
            $template = str_replace("-", "_", $_POST['test_type']);
            
            if(isset($_POST['client_test_id'])){
                $_POST['client_test_id'] = intval($_POST['client_test_id']);
            }
            $club_client_details = $this->getClubClientById($club_client_id);
            
            if($club_client_details['club_id'] != $session->get('club_id')){
                return $this->redirect($this->generateUrl('acme_club_login'));
            }

            $app_type = '';
            $test_status = ($_POST['submit_type'] == 'draft') ? 'draft' : 'finalised';
            $client_test_id = (isset($_POST['client_test_id'])) ? $_POST['client_test_id'] : NULL;
            
            $error = ($test_status != 'draft') ? $this->testInputValidations($_POST) : array();
            
                $_POST['moderate_vigorous_pa'] = intval($_POST['moderate_vigorous_pa']);
                $_POST['muscle_strenthening_pa'] = intval($_POST['muscle_strenthening_pa']);
                $moderate_vigorous_pa = $formula->moderateOrVigorousIntensityPhysicalActivity($_POST['moderate_vigorous_pa']);
                $muscle_strenthening_pa = $formula->muscleStrentheningPhysicalActivity($_POST['muscle_strenthening_pa']);
                $physicalActivity = $formula->physicalActivity($moderate_vigorous_pa, $muscle_strenthening_pa, true);

                $_POST['lifestyle_smoking'] = intval($_POST['lifestyle_smoking']);
                $smokingHabits = $formula->smokingHabits($club_client_details['gender'], $_POST['lifestyle_smoking'], true);
                    
                $_POST['per_week_value'] = intval($_POST['per_week_value']);
                $_POST['one_sitting_value'] = intval($_POST['one_sitting_value']);
                $alcohol_consumption_per_week = $formula->alcoholConsumptionPerWeekResult($club_client_details['gender'], $_POST['per_week_value']);
                $alcohol_consumption_in_one_sitting = $formula->alcoholConsumptionInOneSittingResult($_POST['one_sitting_value']);

                $alcoholConsumptionTotal = $formula->alcoholConsumptionTotal($alcohol_consumption_per_week, $alcohol_consumption_in_one_sitting, true);
                
                $number_of_yes = 0;
                if(isset($_POST['nutrition'])){
                    foreach ($_POST['nutrition'] as $lifestyle_question_id => $answer) {
                        $number_of_yes += intval($answer);
                    }
                }
                $nutrition = $formula->nutrition($number_of_yes, true);

            if(count($error) <= 0){
                $first_test_details = $this->getClientFirstOrRecentTestDetails($club_client_id, NULL, $client_test_id);
                // DEDUCT CREDIT
                if($_POST['submit_type'] != 'draft'){
                    $club = $em->getRepository('AcmeHeadOfficeBundle:Club')
                                ->findOneBy(array(
                                    'club_id' => $session->get('club_id'),
                                ));

                    if( $club->getPaymentOption() != 'monthly' || ($club->getPaymentOption() == 'monthly' && $club->getMonthlyFeeStatus() != 'paid')){
                        $available_credit = $club->getAvailableCredit() - 1;

                        if($available_credit >= 0){
                            $club->setAvailableCredit($available_credit);
                            $em->persist($club);
                            $em->flush();

                            $session->set('available_credit', $available_credit);
                        }else{
                            $test_status = 'draft';

                            $error[] = array(
                                    'message' => "Your club doesn't have enough credit! This test has been saved but has not been finalised."
                                );
                        }
                    }
                }
            
            
                if(isset($_POST['client_test_id'])){
                    $test = $em->getRepository('AcmeHeadOfficeBundle:ClientTest')
                                ->findOneBy(array(
                                    'client_test_id' => $_POST['client_test_id'],
                                ));

                    $app_type = 'update';
                    if(count($test) == 0){
                        $test = new ClientTest();
                        $app_type = 'new';
                    }
                }else{
                    $test = new ClientTest();
                    $app_type = 'new';
                }

                $current_test_status = $test->getStatus();

                $test->setClubClientId($club_client_id);
                if($session->get('club_admin_id') != ''){
                    $test->setClubAdminId($session->get('club_admin_id'));
                }
                $test->setEventId($_POST['event_id']);
                $test->setAge($club_client_details['age']);

                    $healthy_life_expectancy_male = $this->getHealthyLifeExpectancies('male');
                    $healthy_life_expectancy_female = $this->getHealthyLifeExpectancies('female');
                    $male_max_score = $this->getHealthyLifeMaxWorstScore('male', 'max');
                    $female_max_score = $this->getHealthyLifeMaxWorstScore('female', 'max');
                    $male_worst_score = $this->getHealthyLifeMaxWorstScore('male', 'worst');
                    $female_worst_score = $this->getHealthyLifeMaxWorstScore('female', 'worst');

                $test->setHealthyLifeExpectancyDalysFemale($healthy_life_expectancy_female['dalys']);
                $test->setHealthyLifeExpectancyDalysMale($healthy_life_expectancy_male['dalys']);
                $test->setHealthyLifeExpectancyFemale($healthy_life_expectancy_female['expectancy']);
                $test->setHealthyLifeExpectancyMale($healthy_life_expectancy_male['expectancy']);
                $test->setHealthyLifeExpectancyTotalFemale($healthy_life_expectancy_female['total_expectancy']);
                $test->setHealthyLifeExpectancyTotalMale($healthy_life_expectancy_male['total_expectancy']);
                $test->setHealthyLifeMaxScoreFemale($female_max_score['score']);
                $test->setHealthyLifeMaxScoreMale($male_max_score['score']);
                $test->setHealthyLifeWorstScoreFemale($female_worst_score['score']);
                $test->setHealthyLifeWorstScoreMale($male_worst_score['score']);
                
                $test->setHeight( intval($_POST['height']) );
                $test->setWeight( intval($_POST['weight']));

                $test->setPhysiologicalVo2TestValue1(0);
                $test->setPhysiologicalVo2TestValue2(0);
                $test->setPhysiologicalBalanceValue(0);
                $test->setPhysiologicalSquatValue(0);
                $test->setPhysiologicalSitupValue(0); 
                
                if($_POST['test_type'] == 'strength'){
                    if(isset($_POST['physiological_squat_1rm_s_direct_entry'])){
                        $_POST['physiological_squat_1rm_direct_entry'] = floatval($_POST['physiological_squat_1rm_direct_entry']);
                        $test->setPhysiologicalSquat1rmSDirectEntry( 1 ); 
                        $test->setPhysiologicalSquat1rmDirectEntry( $_POST['physiological_squat_1rm_direct_entry']); 
                        $test->setPhysiologicalSquat1rmWeight( 0); 
                        $test->setPhysiologicalSquat1rmReps( 0);
                        $test->setPhysiologicalSquat1rm( $_POST['physiological_squat_1rm_direct_entry']);
                    }else{
                        $_POST['physiological_squat_1rm_weight'] = floatval($_POST['physiological_squat_1rm_weight']);
                        $_POST['physiological_squat_1rm_reps'] = floatval($_POST['physiological_squat_1rm_reps']);
                        $test->setPhysiologicalSquat1rmSDirectEntry( 0 ); 
                        $test->setPhysiologicalSquat1rmDirectEntry( 0); 
                        $test->setPhysiologicalSquat1rmWeight( $_POST['physiological_squat_1rm_weight'] ); 
                        $test->setPhysiologicalSquat1rmReps( $_POST['physiological_squat_1rm_reps'] );
                        $test->setPhysiologicalSquat1rm( $formula->physiologicalStrength($_POST['physiological_squat_1rm_weight'], $_POST['physiological_squat_1rm_reps']));
                    }

                    if(isset($_POST['physiological_bench_press_1rm_s_direct_entry'])){
                        $_POST['physiological_bench_press_1rm_direct_entry'] = floatval($_POST['physiological_bench_press_1rm_direct_entry']);
                        $test->setPhysiologicalBenchPress1rmSDirectEntry( 1 ); 
                        $test->setPhysiologicalBenchPress1rmDirectEntry( $_POST['physiological_bench_press_1rm_direct_entry']); 
                        $test->setPhysiologicalBenchPress1rmWeight( 0); 
                        $test->setPhysiologicalBenchPress1rmReps( 0);
                        $test->setPhysiologicalBenchPress1rm( $_POST['physiological_bench_press_1rm_direct_entry']);
                    }else{
                        $_POST['physiological_bench_press_1rm_weight'] = floatval($_POST['physiological_bench_press_1rm_weight']);
                        $_POST['physiological_bench_press_1rm_reps'] = floatval($_POST['physiological_bench_press_1rm_reps']);
                        $test->setPhysiologicalBenchPress1rmSDirectEntry( 0 ); 
                        $test->setPhysiologicalBenchPress1rmDirectEntry( 0); 
                        $test->setPhysiologicalBenchPress1rmWeight( $_POST['physiological_bench_press_1rm_weight'] ); 
                        $test->setPhysiologicalBenchPress1rmReps( $_POST['physiological_bench_press_1rm_reps'] );
                        $test->setPhysiologicalBenchPress1rm( $formula->physiologicalStrength($_POST['physiological_bench_press_1rm_weight'], $_POST['physiological_bench_press_1rm_reps']));
                    }

                    if(isset($_POST['physiological_deadlift_1rm_s_direct_entry'])){
                        $_POST['physiological_deadlift_1rm_direct_entry'] = floatval($_POST['physiological_deadlift_1rm_direct_entry']);
                        $test->setPhysiologicalDeadlift1rmSDirectEntry( 1 ); 
                        $test->setPhysiologicalDeadlift1rmDirectEntry( $_POST['physiological_deadlift_1rm_direct_entry']); 
                        $test->setPhysiologicalDeadlift1rmWeight( 0); 
                        $test->setPhysiologicalDeadlift1rmReps( 0);
                        $test->setPhysiologicalDeadlift1rm( $_POST['physiological_deadlift_1rm_direct_entry']);
                    }else{
                        $_POST['physiological_deadlift_1rm_weight'] = floatval($_POST['physiological_deadlift_1rm_weight']);
                        $_POST['physiological_deadlift_1rm_reps'] = floatval($_POST['physiological_deadlift_1rm_reps']);
                        $test->setPhysiologicalDeadlift1rmSDirectEntry( 0 ); 
                        $test->setPhysiologicalDeadlift1rmDirectEntry( 0); 
                        $test->setPhysiologicalDeadlift1rmWeight( $_POST['physiological_deadlift_1rm_weight'] ); 
                        $test->setPhysiologicalDeadlift1rmReps( $_POST['physiological_deadlift_1rm_reps'] );
                        $test->setPhysiologicalDeadlift1rm( $formula->physiologicalStrength($_POST['physiological_deadlift_1rm_weight'], $_POST['physiological_deadlift_1rm_reps']));
                    }

                    if(isset($_POST['physiological_overhead_press_s_direct_entry'])){
                        $_POST['physiological_overhead_press_direct_entry'] = intval($_POST['physiological_overhead_press_direct_entry']);
                        $test->setPhysiologicalOverheadPressSDirectEntry( 1 ); 
                        $test->setPhysiologicalOverheadPressDirectEntry( $_POST['physiological_overhead_press_direct_entry']); 
                        $test->setPhysiologicalOverheadPressWeight( 0); 
                        $test->setPhysiologicalOverheadPressReps( 0);
                        $test->setPhysiologicalOverheadPress( $_POST['physiological_overhead_press_direct_entry']);
                    }else{
                        $_POST['physiological_overhead_press_weight'] = intval($_POST['physiological_overhead_press_weight']);
                        $_POST['physiological_overhead_press_reps'] = intval($_POST['physiological_overhead_press_reps']);
                        $test->setPhysiologicalOverheadPressSDirectEntry( 0 ); 
                        $test->setPhysiologicalOverheadPressDirectEntry( 0); 
                        $test->setPhysiologicalOverheadPressWeight( $_POST['physiological_overhead_press_weight'] ); 
                        $test->setPhysiologicalOverheadPressReps( $_POST['physiological_overhead_press_reps'] );
                        $test->setPhysiologicalOverheadPress( $formula->physiologicalStrength($_POST['physiological_overhead_press_weight'], $_POST['physiological_overhead_press_reps']));
                    }
                    
                }elseif($_POST['test_type'] == 'fitness'){
                    $test->setPhysiologicalSquat1minMin( intval($_POST['physiological_squat_1min_min']) );
//                    $test->setPhysiologicalSquat1minSec( intval($_POST['physiological_squat_1min_sec']) );
                    $test->setPhysiologicalPushUps1minMin( intval($_POST['physiological_push_ups_1min_min']) );
//                    $test->setPhysiologicalPushUps1minSec( intval($_POST['physiological_push_ups_1min_sec']) );
                    $test->setPhysiologicalSitUps1minMin( intval($_POST['physiological_sit_ups_1min_min']) );
//                    $test->setPhysiologicalSitUps1minSec( intval($_POST['physiological_sit_ups_1min_sec']) );
                    $test->setPhysiologicalRow500mMin( intval($_POST['physiological_row_500m_min']) );
                    $test->setPhysiologicalRow500mSec( intval($_POST['physiological_row_500m_sec']) );
                    $test->setPhysiologicalStandingBroadJump( intval($_POST['physiological_standing_broad_jump']) );
                
                }elseif($_POST['test_type'] == 'run'){
                    $test->setPhysiologicalTimeTrialOption( $_POST['physiological_time_trial_option'] );
                    $test->setPhysiologicalTimeTrialMin( intval($_POST['physiological_time_trial_min']) );
                    $test->setPhysiologicalTimeTrialSec( intval($_POST['physiological_time_trial_sec']) );
                }
                
                
                $test->setBiometricWaistValue( intval($_POST['biometric_waist_value']) );
                $test->setBiometricThigh(0);
                $test->setBiometricArm(0);

                $test->setBiometricBloodPressureSystolic(0);
                $test->setBiometricBloodPressureDiastolic(0);
                $test->setBiometricBloodOxygenValue(0);

                if(trim($_POST['biometric_body_fat_value']) != ''){
                    $test->setBiometricBodyFatValue($_POST['biometric_body_fat_value']);
                }else{
                    $test->setBiometricBodyFatValue(0);
                }
                $test->setBiometricChest(intval( $_POST['biometric_chest']) );
                $test->setBiometricAbdomen( intval($_POST['biometric_abdomen']) );
                $test->setBiometricHip( intval($_POST['biometric_hip']) );
                $test->setBiometricArmLeft( intval($_POST['biometric_arm_left']) );
                $test->setBiometricArmRight( intval($_POST['biometric_arm_right']) );
                $test->setBiometricThighLeft( intval($_POST['biometric_thigh_left']) );
                $test->setBiometricThighRight( intval($_POST['biometric_thigh_right']) );

                // Results
                    $bmi = $formula->bMI($_POST['height'], $_POST['weight']);
                $test->setBmi($bmi);
                $test->setLifestylePhysicalActivity($physicalActivity);
                $test->setLifestyleSmoking($smokingHabits);
                $test->setLifestyleAlcohol($alcoholConsumptionTotal);
                $test->setLifestyleNutrition($nutrition);

                    $biometricWaist = $formula->waist($club_client_details['gender'], $_POST['biometric_waist_value'], TRUE);
                $test->setBiometricWaist($biometricWaist);

                    $biometricBodyFat = $formula->bodyFat($club_client_details['gender'], $_POST['biometric_body_fat_value']);
                $test->setBiometricBodyFat($biometricBodyFat);

                    $totalCrf = $formula->totalCRF(
                        $formula->crf( $physicalActivity ), 
                        $formula->crf( $smokingHabits ), 
                        $formula->crf( $alcoholConsumptionTotal ), 
                        $formula->crf( $nutrition ),
                        0, //$formula->crf($_POST['lifestyle_mental_health']), 
                        0, //$formula->crf($_POST['lifestyle_risk_profile']), 
                        0, //$formula->crf($biometricBodyFat), 
                        0, //$formula->crf($biometricBloodPressure), 
                        0, //$formula->crf($biometricBloodOxygen), 
                        0, //$formula->crf($physiologicalVo2_result), 
                        0, //$formula->crf($_POST['physiological_balance']), 
                        0, //$formula->crf($physiologicalSquat), 
                        0, //$formula->crf($physiologicalSitup), 
                        $formula->crf($biometricWaist)
                    );
                $test->setTotalCrf($totalCrf);

                    $healthyLifePoints = $formula->healthyLifeYears(
                        $club_client_details['gender'], 
                        $physicalActivity, 
                        $alcoholConsumptionTotal, 
                        $nutrition,  
                        $smokingHabits, 
                        0, //$_POST['lifestyle_mental_health'], 
                        0, //$_POST['lifestyle_risk_profile'], 
                        0, //$biometricBodyFat, 
                        0, //$biometricBloodPressure, 
                        0, //$biometricBloodOxygen, 
                        0, //$physiologicalVo2_result, 
                        0, //$_POST['physiological_balance'], 
                        0, //$physiologicalSquat, 
                        0, //$physiologicalSitup, 
                        $biometricWaist, 
                        $totalCrf
                    );
                $test->setHealthyLifePoints($healthyLifePoints);

                    $estimatedHealthyLifeExpectancy = $formula->estimatedHealthyLifeExpectancy(
                        $club_client_details['gender'], 
                        $healthy_life_expectancy_male['expectancy'], 
                        $healthy_life_expectancy_female['expectancy'],
                        $healthyLifePoints);
                $test->setEstimatedHealthyLifeExpectancy($estimatedHealthyLifeExpectancy);

                    $max_hle = $formula->maxHLE(
                        $club_client_details['gender'],
                        $healthy_life_expectancy_male['expectancy'], 
                        $healthy_life_expectancy_female['expectancy'], 
                        $male_max_score['score'], 
                        $female_max_score['score']);
                $test->setMaxHle($max_hle);

                    $healthyLifeScore = $formula->currentHealthyLifeScore(
                        $club_client_details['gender'],
                        $estimatedHealthyLifeExpectancy, 
                        $max_hle
                    );
                $test->setHealthyLifeScore($healthyLifeScore);

                    $estimatedYearsOfHealthyLifeLeft = $formula->estimatedYearsOfHealthyLifeLeft($estimatedHealthyLifeExpectancy, $club_client_details['age']);
                $test->setEstimatedYearsOfHealthyLifeLeft($estimatedYearsOfHealthyLifeLeft);

                    $estimatedYearsYouWillLiveWithDid = $formula->estimatedNumberOfYearsYouWillLive(
                            $club_client_details['gender'],
                            $estimatedHealthyLifeExpectancy, 
                            $healthy_life_expectancy_male['total_expectancy'], 
                            $healthy_life_expectancy_female['total_expectancy']
                        );
                $test->setEstimatedYearsYouWillLiveWithDid($estimatedYearsYouWillLiveWithDid);

                    $estimatedYearsYouWillLiveWithDidAsOpposedTo = $formula->daly(
                            $club_client_details['gender'],
                            $healthy_life_expectancy_male['dalys'],
                            $healthy_life_expectancy_female['dalys']
                        );
                $test->setEstimatedYearsYouWillLiveWithDidAsOpposedTo($estimatedYearsYouWillLiveWithDidAsOpposedTo);

                    $youCouldAddUpTo = $formula->couldAddUpTo(
                        $club_client_details['gender'],
                        $club_client_details['age'], 
                        $healthy_life_expectancy_male['total_expectancy'], 
                        $healthy_life_expectancy_female['total_expectancy'], 
                        $max_hle, 
                        $estimatedHealthyLifeExpectancy, 
                        $male_max_score['score'], 
                        $female_max_score['score']
                    );
                $test->setYouCouldAddUpTo($youCouldAddUpTo);

                    $estimatedCurrentHealthyLifeAge = $formula->estimatedCurrentHealthyLifeAge(
                            $club_client_details['age'], 
                            $healthyLifePoints, 
                            $max_hle
                        );
                $test->setEstimatedCurrentHealthyLifeAge($estimatedCurrentHealthyLifeAge);
                
                    $yearsAdded = $this->getYearsAdded($formula, $first_test_details, $estimatedHealthyLifeExpectancy);
                $test->setYearsAdded($yearsAdded);
                
                    $cmLost = $this->getCmLost($formula, $first_test_details, $_POST['biometric_waist_value']);
                $test->setCmLost($cmLost);
                
                    $biometricWaistValueLost = $this->getBiometricWaistValueLost($formula, $first_test_details, $_POST['biometric_waist_value']);
                $test->setBiometricWaistValueLost($biometricWaistValueLost);
                
                    $biometricChestLost = $this->getBiometricChestLost($formula, $first_test_details, 0);
                $test->setBiometricChestLost($biometricChestLost);
                
                    $biometricThighLost = $this->getBiometricThighLost($formula, $first_test_details, 0, 0);
                $test->setBiometricThighLost($biometricThighLost);
                
                    $biometricArmLost = $this->getBiometricArmLost($formula, $first_test_details, 0, 0);
                $test->setBiometricArmLost($biometricArmLost);
                
                    $biometricAbdomenLost = $this->getBiometricAbdomenLost($formula, $first_test_details, 0);
                $test->setBiometricAbdomenLost($biometricAbdomenLost);
                
                    $biometricHipLost = $this->getBiometricHipLost($formula, $first_test_details, 0);
                $test->setBiometricHipLost($biometricHipLost);
                
                    $kgLost = (isset($first_test_details['weight'])) ? $formula->kgLost($first_test_details['weight'], intval($_POST['weight'])) : 0;
                $test->setKgLost( intval($kgLost) );
                
                
                if($session->get('ho_admin_id') != ''){
                    if( $test->getStatus() == 'finalised' && $test_status == 'draft'){
                        $test->setStatus('finalised');
                    }else{
                        $test->setStatus($test_status);
                    }
                }else{
                    $test->setStatus($test_status);
                }
                
                $test->setTestDatetime($datetime->format('Y-m-d H:i:s'));
                if($test_status != 'draft'){
                    $test->setTestDatetimeFinalised($datetime->format('Y-m-d H:i:s'));
                }
                $test->setTestType($_POST['test_type']);
                $em->persist($test);
                $em->flush();
                
                if(!isset($first_test_details['height'])){
                    if(isset($_POST['height']) && $_POST['height'] != ''){
                        $this->setClientHeight($em, $club_client_id, intval($_POST['height']));
                    }
                }
                
                if($club_client_details['s_signup_questions_done'] != 1){
                    $cust = $this->getDoctrine()->getEntityManager();
                    $connection = $cust->getConnection();
                    $statement = $connection->prepare("UPDATE tbl_signup_questions_client_answer a
                            LEFT JOIN tbl_signup_questions q ON q.`sq_id` = a.`sq_id`
                            SET a.`answer` = 0
                            WHERE a.`club_client_id` = :club_client_id 
                            AND (q.`type` = 'goal' OR q.type = 'experience' OR q.type = 'what-matters-most')");
                    $statement->bindValue('club_client_id', $club_client_id);
                    $statement->execute();
                    if(isset($_POST['goal'])){
                        foreach ($_POST['goal'] as $key => $value) {

                            $goal = $em->getRepository('AcmeHeadOfficeBundle:SignUpQuestionsClientAnswer')
                                    ->findOneBy(array(
                                        'sq_id' => $key, 
                                        'club_client_id' => $club_client_id
                                    ));

                            if(count($goal) == 0){
                                $goal = new SignUpQuestionsClientAnswer();
                                $goal->setSqId( $key );
                                $goal->setClubClientId($club_client_id);
                            }
                            $goal->setAnswer( $value );
                            $em->persist($goal);
                        }
                        $em->flush();
                    }

                    if(isset($_POST['experience'])){
                        foreach ($_POST['experience'] as $key => $value) {
                            $goal = $em->getRepository('AcmeHeadOfficeBundle:SignUpQuestionsClientAnswer')
                                    ->findOneBy(array(
                                        'sq_id' => $key, 
                                        'club_client_id' => $club_client_id
                                    ));

                            if(count($goal) == 0){
                                $goal = new SignUpQuestionsClientAnswer();
                                $goal->setSqId( $key );
                                $goal->setClubClientId($club_client_id);
                            }
                            $goal->setAnswer( $value );
                            $em->persist($goal);
                        }
                        $em->flush();
                    }
                    if(isset($_POST['wmm'])){
                        foreach ($_POST['wmm'] as $key => $value) {
                            $goal = $em->getRepository('AcmeHeadOfficeBundle:SignUpQuestionsClientAnswer')
                                    ->findOneBy(array(
                                        'sq_id' => $key, 
                                        'club_client_id' => $club_client_id
                                    ));

                            if(count($goal) == 0){
                                $goal = new SignUpQuestionsClientAnswer();
                                $goal->setSqId( $key );
                                $goal->setClubClientId($club_client_id);
                            }
                            $goal->setAnswer( $value );
                            $em->persist($goal);
                        }
                        $em->flush();
                    }

                }
                
                if($club_client_details['s_signup_health_questions_done'] != 1){
                    $cust = $this->getDoctrine()->getEntityManager();
                    $connection = $cust->getConnection();
                    $statement = $connection->prepare("UPDATE tbl_signup_questions_client_answer a
                            LEFT JOIN tbl_signup_questions q ON q.`sq_id` = a.`sq_id`
                            SET a.`answer` = 0
                            WHERE a.`club_client_id` = :club_client_id 
                            AND q.`type` = 'pre-exercise'");
                    $statement->bindValue('club_client_id', $club_client_id);
                    $statement->execute();
                    if(isset($_POST['pe'])){
                        foreach ($_POST['pe'] as $key => $value) {
                            $goal = $em->getRepository('AcmeHeadOfficeBundle:SignUpQuestionsClientAnswer')
                                    ->findOneBy(array(
                                        'sq_id' => $key, 
                                        'club_client_id' => $club_client_id
                                    ));

                            if(count($goal) == 0){
                                $goal = new SignUpQuestionsClientAnswer();
                                $goal->setSqId( $key );
                                $goal->setClubClientId($club_client_id);
                            }
                            $goal->setAnswer( $value );
                            $em->persist($goal);
                        }
                        $em->flush();
                    }
                }
                
                // SAVE LIFESTYLE QUESTIONNAIRE ANSWERS
                if(isset($_POST['moderate_vigorous_pa'])){
                    $_POST['moderate_vigorous_pa'] = intval($_POST['moderate_vigorous_pa']);
                    $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                            ->findOneBy(array(
                                'lq_id' => 1, 
                                'club_client_id' => $club_client_id,
                                'client_test_id' => $test->getClientTestId()
                            ));

                    if(count($model) == 0){
                        $model = new LifestyleQuestionnareClientAnswer();
                        $model->setLqId(1);
                    }

                    $model->setStatus('saved');
                    $model->setClubClientId($club_client_id);
                    $model->setClientTestId($test->getClientTestId());
                    $model->setAnswer($_POST['moderate_vigorous_pa']);
                    $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                    $em->persist($model);
                    $em->flush();
                }
                
                if(isset($_POST['muscle_strenthening_pa'])){
                    $_POST['muscle_strenthening_pa'] = intval($_POST['muscle_strenthening_pa']);
                    $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                            ->findOneBy(array(
                                'lq_id' => 2, 
                                'club_client_id' => $club_client_id,
                                'client_test_id' => $test->getClientTestId()
                            ));

                    if(count($model) == 0){
                        $model = new LifestyleQuestionnareClientAnswer();
                        $model->setLqId(2);
                    }

                    $model->setStatus('saved');
                    $model->setClubClientId($club_client_id);
                    $model->setClientTestId($test->getClientTestId());
                    $model->setAnswer($_POST['muscle_strenthening_pa']);
                    $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                    $em->persist($model);
                    $em->flush();
                }

                if(isset($_POST['lifestyle_smoking'])){
                    $_POST['lifestyle_smoking'] = intval($_POST['lifestyle_smoking']);
                    $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                            ->findOneBy(array(
                                'lq_id' => 3, 
                                'club_client_id' => $club_client_id,
                                'client_test_id' => $test->getClientTestId()
                            ));

                    if(count($model) == 0){
                        $model = new LifestyleQuestionnareClientAnswer();
                        $model->setLqId(3);
                    }

                    $model->setStatus('saved');
                    $model->setClubClientId($club_client_id);
                    $model->setClientTestId($test->getClientTestId());
                    $model->setAnswer($_POST['lifestyle_smoking']);
                    $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                    $em->persist($model);
                    $em->flush();
                }
                
                if(isset($_POST['per_week_value'])){
                    $_POST['per_week_value'] = intval($_POST['per_week_value']);
                    $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                            ->findOneBy(array(
                                'lq_id' => 4, 
                                'club_client_id' => $club_client_id,
                                'client_test_id' => $test->getClientTestId()
                            ));

                    if(count($model) == 0){
                        $model = new LifestyleQuestionnareClientAnswer();
                        $model->setLqId(4);
                    }

                    $model->setStatus('saved');
                    $model->setClubClientId($club_client_id);
                    $model->setClientTestId($test->getClientTestId());
                    $model->setAnswer($_POST['per_week_value']);
                    $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                    $em->persist($model);
                    $em->flush();
                }
                
                if(isset($_POST['one_sitting_value'])){
                    $_POST['one_sitting_value'] = intval($_POST['one_sitting_value']);
                    $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                            ->findOneBy(array(
                                'lq_id' => 5, 
                                'club_client_id' => $club_client_id,
                                'client_test_id' => $test->getClientTestId()
                            ));

                    if(count($model) == 0){
                        $model = new LifestyleQuestionnareClientAnswer();
                        $model->setLqId(5);
                    }

                    $model->setStatus('saved');
                    $model->setClubClientId($club_client_id);
                    $model->setClientTestId($test->getClientTestId());
                    $model->setAnswer($_POST['one_sitting_value']);
                    $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                    $em->persist($model);
                    $em->flush();
                }

                if(isset($_POST['nutrition'])){
                    foreach ($_POST['nutrition'] as $lifestyle_question_id => $answer) {
                        $em = $this->getDoctrine()->getManager();

                        $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                                ->findOneBy(array(
                                    'lq_id' => $lifestyle_question_id, 
                                    'club_client_id' => $club_client_id,
                                    'client_test_id' => $test->getClientTestId()
                                ));

                        if(count($model) == 0){
                            $model = new LifestyleQuestionnareClientAnswer();
                            $model->setLqId($lifestyle_question_id);
                        }

                        $model->setStatus('saved');
                        $model->setClubClientId($club_client_id);
                        $model->setClientTestId($test->getClientTestId());
                        $model->setAnswer($answer);
                        $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                        $em->persist($model);
                        $em->flush();
                    }

                }
                
                
                
                $s_lifestyle_physical_activity_answer_complete = $this->sLifestyleTestQuestionnaireAnswersDone('physical_activity', $club_client_id, $client_test_id);
                $s_lifestyle_smoking_answer_complete = $this->sLifestyleTestQuestionnaireAnswersDone('smoking', $club_client_id, $client_test_id);
                $s_lifestyle_alcohol_answer_complete = $this->sLifestyleTestQuestionnaireAnswersDone('alcohol', $club_client_id, $client_test_id);
                $s_lifestyle_nutrition_answer_complete = $this->sLifestyleTestQuestionnaireAnswersDone('nutrition', $club_client_id, $client_test_id);
                
                if($test_status == 'finalised'){
                    if(!$s_lifestyle_physical_activity_answer_complete){
                        $error[] = array(
                                'message' => "Please make sure to complete Physical Activity test."
                            );
                    }

                    if(!$s_lifestyle_smoking_answer_complete){
                        $error[] = array(
                                'message' => "Please make sure to complete Smoking Habit test."
                            );
                    }

                    if(!$s_lifestyle_alcohol_answer_complete){
                        $error[] = array(
                                'message' => "Please make sure to complete Alcohol Consumption test."
                            );
                    }

                    if(!$s_lifestyle_nutrition_answer_complete){
                        $error[] = array(
                                'message' => "Please make sure to complete Nutrition test."
                            );
                    }

                }
                
            
            }
            if(count($error) > 0){
                $this->get('session')->getFlashBag()->add(
                        'error',
                        $error
                    );
                
                
                $post = array(
                    'club_client_id' => $club_client_id,
                    'height' => $_POST['height'],
                    'weight' => $_POST['weight'],
                    'bmi' => $_POST['bmi'],
                    'lifestyle_physical_activity' => $physicalActivity,
                    'lifestyle_smoking' => $smokingHabits,
                    'lifestyle_alcohol' => $alcoholConsumptionTotal,
                    'lifestyle_nutrition' => $nutrition,
                    
                    'biometric_waist_value' => $_POST['biometric_waist_value'],
                    'biometric_chest' => $_POST['biometric_chest'],
                    'biometric_abdomen' => $_POST['biometric_abdomen'],
                    'biometric_hip' => $_POST['biometric_hip'],
                    'biometric_thigh_left' => $_POST['biometric_thigh_left'],
                    'biometric_thigh_right' => $_POST['biometric_thigh_right'],
                    'biometric_arm_left' => $_POST['biometric_arm_left'],
                    'biometric_arm_right' => $_POST['biometric_arm_right'],
                    'biometric_body_fat_value' => $_POST['biometric_body_fat_value'],
                    
                    'event_id' => $_POST['event_id'],
                    'waist_hip_ratio'=> $_POST['waist_hip_ratio'],
                    
                    'goal'=> (isset($_POST['goal'])) ? $_POST['goal'] : array(),
                    'wmm'=> (isset($_POST['wmm'])) ? $_POST['wmm'] : array(),
                    'experience'=> (isset($_POST['experience'])) ? $_POST['experience'] : array(),
                    'pe'=> (isset($_POST['pe'])) ? $_POST['pe'] : array(),
                    
                    'test_type'=> $_POST['test_type']
                );
                
                if(isset($_POST['client_test_id'])){
                    $post['client_test_id'] = $_POST['client_test_id'];
                }
                
                
            }else{
                $em->getConnection()->commit(); 
                if($test_status != 'draft'){
                    if($current_test_status != 'finalised'){
                        $client = $em->getRepository('AcmeHeadOfficeBundle:ClubClient')
                                    ->findOneBy(array(
                                        'club_client_id' => $club_client_details['club_client_id']
                                    ));
                        $client->setSsignupQuestionsDone(1);
                        $client->setSSignupHealthQuestionsDone(1);
                        $em->persist($client);
                        $em->flush();

                        $from = $this->container->getParameter('site_email_address');
                        $subject = $club_client_details['fname'] . ' ' . $club_client_details['lname'] . ' -  Test Results PDF';
                        $body = $this->renderView('AcmeClubBundle:Test:email_test_personal_content.html.twig',
                                        array(
                                            'name' => $club_client_details['fname'],
                                            'test_date' =>  $test->getTestDatetimeFinalised()
                                        ));


                        $recipients = array();
                        $recipients[ $club_client_details['email'] ] = $club_client_details['fname'] . ' ' . $club_client_details['lname'];


                        if(count($recipients) > 0){
                            $ref = array(
                                'client_test_id' => $test->getClientTestId(),
                                'type'=> 'premium-test'
                                );
                            $emailQueue = new EmailQueue();
                            $emailQueue->setSender( $from );
                            $emailQueue->setSubject( $subject );
                            $emailQueue->setBody( $body );
                            $emailQueue->setRecipients( json_encode($recipients) );
                            $emailQueue->setReference( json_encode($ref) );
                            $emailQueue->setSSent(0);
                            $emailQueue->setSTest(1);
                            $em->persist($emailQueue);
                            $em->flush();
                        }
                        
                        
                        // SET ACTIVITY
                        if($app_type == 'new'){
                            if($test_status == 'draft'){
                                $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " created a test for " . $club_client_details['fname'] . " " . $club_client_details['lname'] . " and saved as draft.";
                            }else{
                                $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " created a test for " . $club_client_details['fname'] . " " . $club_client_details['lname'] . " and finalised.";
                            }
                        }else{
                            if($test_status == 'draft'){
                                $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " updated the test for " . $club_client_details['fname'] . " " . $club_client_details['lname'] . " and saved as draft.";
                            }else{
                                $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " updated the test for " . $club_client_details['fname'] . " " . $club_client_details['lname'] . " and finalised.";
                            }
                        }

                        if($session->get('club_admin_id') != ''){ 
                            $this->setActivity($session->get('club_admin_id'), 'club-admin', $details);
                        }else{
                            $this->setActivity($session->get('ho_admin_id'), 'head-office-admin', $details);
                        }
                        
                        $MailChimp = new \Acme\HeadOfficeBundle\Model\MailChimp($this->container->getParameter('MailChimp_API_ID'));

                        $subs = $MailChimp->call('lists/subscribe', array(
                                'id'                => $this->container->getParameter('MailChimp_List_premiumtest'),
                                'email'             => array('email'=>$club_client_details['email']),
                                'merge_vars'        => array('FNAME'=>$club_client_details['fname'], 'LNAME'=>$club_client_details['lname']),
                                'double_optin'      => false,
                                'update_existing'   => true,
                                'replace_interests' => false,
                                'send_welcome'      => false,
                                ));
                    }
                    
                    if($test_status == 'draft' && ($session->get('ho_admin_id') == '' && $test->getStatus() == 'draft')){
                        if(isset($_POST['ajax'])){
                            return new Response("success");
                        }else{
                            return $this->redirect($this->generateUrl('acme_club_create_lite_test') . '?id=' . $club_client_id . '&test=' . $test->getClientTestId() . '&type=' . $_POST['test_type']);
                        }


                    }elseif($test_status == 'draft' && ($session->get('ho_admin_id') != '' && $test->getStatus() == 'draft')){
                        if(isset($_POST['ajax'])){
                            return new Response("success");
                        }else{
                            return $this->redirect($this->generateUrl('acme_club_create_lite_test') . '?id=' . $club_client_id . '&test=' . $test->getClientTestId() . '&type=' . $_POST['test_type']);
                        }

                    }else{
                        // get program client here
                        $program_client = $this->get12weekProgramClientByEmail( $club_client_details['email'] );
                        if( count($program_client) > 0 ){
                            $this->copyTestDataToClientsSite($test, $first_test_details, $program_client);
                        }
                        
                        return $this->redirect($this->generateUrl('acme_club_view_lite_test') . '?id=' .  $test->getClientTestId());
                    }
                    
                }
                
            }
            
            
            
        }else{
            
            
            
            
            // VIEW
            
            if(!isset($_GET['id']) || !isset($_GET['type'])){
                return $this->render('AcmeClubBundle:Test:view_error.html.twig', array(
                    'error'=> array(
                        'title'=> 'Error',
                        'message'=> 'Missing parameter value.'
                        )
                    )
                );
            }
            
            if(!in_array($_GET['type'], $mod->testTypes())){
                return $this->render('AcmeClubBundle:Test:view_error.html.twig', array(
                    'error'=> array(
                        'title'=> 'Error',
                        'message'=> 'Invalid parameter value.'
                        )
                    )
                );
            }
            
            
            $club_client_id = intval($_GET['id']);
            $_GET['type'] = strtolower( trim($_GET['type']) );
            $post = array();
            
            $club_client_details = $this->getClubClientById($club_client_id);
            
            if($club_client_details['club_id'] != $session->get('club_id')){ 
                return $this->redirect($this->generateUrl('acme_club_login'));
            }
            
            $club_admin_details = ($session->get('club_admin_id') != '') ? $this->getClubAdminUserById($session->get('club_admin_id')) : array();
            if(isset($_GET['test'])){
                // IF TEST HAS ALREADY BEEN CREATED
                $client_test_id = intval($_GET['test']);
                $post = $this->getClientTestDetailsById($client_test_id);
                
                if(!isset($post['status'])){
                    return $this->redirect($this->generateUrl('acme_club_view_lite_test') . '?id=' . $_GET['test']);
                }
                if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){}else{
//                if($session->get('club_admin_id') != '' && $session->get('user_role') != 'admin'){
                    $this->updateClientTestType($club_client_id, $_GET['type']);
                }
                
                
            }else{
                
                // SAVE NEW TEST IF NOT CREATED
                $draft_tests = $this->getClientTestHistory( $club_client_id, NULL, 'draft', NULL, NULL );
                
                if(count($draft_tests) > 0){
                    // IF THERE IS AN EXISTING DRAFT TEST
                    return $this->render('AcmeClubBundle:Test:view_error.html.twig', array(
                        'error'=> array(
                            'title'=> 'Error',
                            'message'=> 'Sorry you already have a draft test underway - please complete it, or remove it before starting a new one.',
                            'type'=> 'continue-draft'
                            ),
                        'draft_test_id'=> $draft_tests[0]['client_test_id'],
                        'test_type'=> $_GET['type']
                        )
                    );

                }else{
                    // CREATE NEW RECORD TO INITIALISE THE TEST
                    $client_test_id = $this->createNewLiteTest($session, $club_client_details, $club_client_id, $_GET['type']);
                    
                    if($client_test_id != NULL){
                        return $this->redirect($this->generateUrl('acme_club_create_lite_test') . '?id=' . $club_client_id . '&test=' . $client_test_id . '&type='. $_GET['type']);
                    }else{
                        return $this->render('AcmeClubBundle:Test:view_error.html.twig', array(
                            'error'=> array(
                                'title'=> 'Error',
                                'message'=> "The system couldn't process your request, please try again later."
                                )
                            )
                        );
                    }
                }
            }
            
            
            
            $template = str_replace("-", "_", $_GET['type']);
            
        }
        
        if((isset($test_status) && $test_status != 'draft') || !isset($test_status)){
            return new Response($this->renderView('AcmeClubBundle:LiteTest:create_lite_test_'. $template .'.html.twig',
                array(
                    'client_details' => $club_client_details,
                    'events' => $this->getEvents($session->get('club_id')),
                    'post' => (isset($post)) ? $post : array(),
                    'physical_activity_questions' => $this->getTestQuestionnaire('physical_activity', $club_client_id, $client_test_id),
                    'alcohol_questions' => $this->getTestQuestionnaire('alcohol', $club_client_id, $client_test_id),
                    'nutrition_questions' => $this->getTestQuestionnaire('nutrition', $club_client_id, $client_test_id),
                    'mental_health_questions' => $this->getTestQuestionnaire('mental_health', $club_client_id, $client_test_id),
                    'mental_health_options' => $this->getTestQuestionnaireOptionsByCode('mental_health'),
                    'risk_profile_questions' => $this->getTestQuestionnaire('risk_profile', $club_client_id, $client_test_id),
                    'smoking_options' => $this->getTestQuestionnaireOptionsByCode('smoking'),
                    'smoking_answer' => $this->getSpecificTestQuestionnaireClientAnswer(3 /*smoking*/, $club_client_id, $client_test_id),

                    'questions_goal'=> $this->getSignUpQuestions('goal', $club_client_id),
                    'questions_experience'=> $this->getSignUpQuestions('experience', $club_client_id),
                    'questions_wmm'=> $this->getSignUpQuestions('what-matters-most', $club_client_id),
                    'questions_pe'=> $this->getSignUpQuestions('pre-exercise', $club_client_id),
                )));
        }else{
            return new Response("done");
        }
    }
    
    public function copyTestDataToClientsSite($test, $first_test_details, $program_client){
        
        
        if($_SERVER['SERVER_NAME'] == 'localhost'){
            $program_host="localhost";
            $program_uname="root";
            $program_pass="";
            $program_database = "healthy_life_project_ohwp";

        }else{
            $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
            $program_uname="hlproot";
            $program_pass="_jWBqa)67?BJq(j-";
            $program_database = "hlp12weekProgram";
        }
        $program_connection=mysqli_connect($program_host,$program_uname,$program_pass, $program_database) or die("Database Connection Failed");

        // DELETE EXISTING
        $program_client_test_id = $this->get12weekProgramClientTestIdByFullTestId($test->getClientTestId());

        // `lk: 19022016
        if(!isset($first_test_details['height'])){
            mysqli_query($program_connection,
            "UPDATE tbl_client SET height=".$test->getHeight()." WHERE email='".$program_client['email']."'");
        }
        mysqli_query($program_connection,
        "DELETE FROM tbl_lifestyle_questionnaire_client_answer WHERE client_test_id=" . $program_client_test_id);

        mysqli_query($program_connection,
        "DELETE FROM tbl_client_test WHERE full_test_id=" . $test->getClientTestId());

        // NEW RECORD
        mysqli_query($program_connection,
        "INSERT INTO tbl_client_test 
            (full_test_id,
            client_id, 
            
            test_type,
            
            test_datetime, 
            test_datetime_finalised,
            age,
            healthy_life_expectancy_male,
            healthy_life_expectancy_female,
            healthy_life_expectancy_total_male,
            healthy_life_expectancy_total_female,
            healthy_life_expectancy_dalys_male,
            healthy_life_expectancy_dalys_female,
            healthy_life_max_score_male,
            healthy_life_max_score_female,
            healthy_life_worst_score_male,
            healthy_life_worst_score_female,
            height,
            weight,
            bmi,
            lifestyle_physical_activity_q1,
            lifestyle_physical_activity_q2,
            lifestyle_physical_activity,
            lifestyle_smoking,
            lifestyle_alcohol_q1,
            lifestyle_alcohol_q2,
            lifestyle_alcohol,
            lifestyle_nutrition,
            lifestyle_mental_health,
            lifestyle_risk_profile,
            physiological_vo2_test_type,
            physiological_vo2_test_value1,
            physiological_vo2_test_value2,
            physiological_vo2_score,
            physiological_vo2,
            physiological_balance_type,
            physiological_balance_value,
            physiological_balance,
            physiological_squat_value,
            physiological_squat,
            physiological_situp_value,
            physiological_situp,
            
            physiological_squat_1rm_s_direct_entry,
            physiological_squat_1rm_direct_entry,
            physiological_squat_1rm_weight,
            physiological_squat_1rm_reps,
            physiological_squat_1rm,
            physiological_bench_press_1rm_s_direct_entry,
            physiological_bench_press_1rm_direct_entry,
            physiological_bench_press_1rm_weight,
            physiological_bench_press_1rm_reps,
            physiological_bench_press_1rm,
            physiological_deadlift_1rm_s_direct_entry,
            physiological_deadlift_1rm_direct_entry,
            physiological_deadlift_1rm_weight,
            physiological_deadlift_1rm_reps,
            physiological_deadlift_1rm,
            physiological_overhead_press_s_direct_entry,
            physiological_overhead_press_direct_entry,
            physiological_overhead_press_weight,
            physiological_overhead_press_reps,
            physiological_overhead_press,
            physiological_squat_1min_min,
            physiological_push_ups_1min_min,
            physiological_sit_ups_1min_min,
            physiological_row_500m_min,
            physiological_row_500m_sec,
            physiological_standing_broad_jump,
            physiological_time_trial_option,
            physiological_time_trial_min,
            physiological_time_trial_sec,

            biometric_waist_value,
            biometric_waist_value_lost,
            biometric_waist,
            biometric_chest_lost,
            biometric_chest,
            biometric_abdomen,
            biometric_abdomen_lost,
            biometric_hip,
            biometric_hip_lost,
            biometric_thigh,
            biometric_thigh_right,
            biometric_thigh_left,
            biometric_thigh_lost,
            biometric_arm,
            biometric_arm_right,
            biometric_arm_left,
            biometric_arm_lost,
            biometric_body_fat_value,
            biometric_body_fat,
            biometric_blood_pressure_systolic,
            biometric_blood_pressure_diastolic,
            biometric_blood_pressure,
            biometric_blood_oxygen_value,
            biometric_blood_oxygen,
            total_crf,
            healthy_life_points,
            estimated_healthy_life_expectancy,
            max_hle,
            healthy_life_score,
            estimated_years_of_healthy_life_left,
            estimated_years_you_will_live_with_did,
            estimated_years_you_will_live_with_did_as_opposed_to,
            you_could_add_up_to,
            estimated_current_healthy_life_age,
            years_added,
            cm_lost,
            kg_lost,
            waist_hip_ratio,
            waist_hip_ratio_score,
            `status`) 

            VALUES

            (". $test->getClientTestId() .",
            ". $program_client['client_id'] .", 
                
            '". $test->getTestType() ."',
                
            '". $test->getTestDatetime() ."', 
            '". $test->getTestDatetimeFinalised() ."',
            '". $test->getAge() ."',
            '". $test->getHealthyLifeExpectancyMale() ."',
            '". $test->getHealthyLifeExpectancyFeMale() ."',
            '". $test->getHealthyLifeExpectancyTotalMale() ."',
            '". $test->getHealthyLifeExpectancyTotalFemale() ."',
            '". $test->getHealthyLifeExpectancyDalysMale() ."',
            '". $test->getHealthyLifeExpectancyDalysFemale() ."',
            '". $test->getHealthyLifeMaxScoreMale() ."',
            '". $test->getHealthyLifeMaxScoreFemale() ."',
            '". $test->getHealthyLifeWorstScoreMale() ."',
            '". $test->getHealthyLifeWorstScoreFemale() ."',
            '". $test->getHeight() ."',
            '". $test->getWeight() ."',
            '". $test->getBmi() ."',
            '". $test->getLifestylePhysicalActivityQ1() ."',
            '". $test->getLifestylePhysicalActivityQ2() ."',
            '". $test->getLifestylePhysicalActivity() ."',
            '". $test->getLifestyleSmoking() ."',
            '". $test->getLifestyleAlcoholQ1() ."',
            '". $test->getLifestyleAlcoholQ2() ."',
            '". $test->getLifestyleAlcohol() ."',
            '". $test->getLifestyleNutrition() ."',
            '". $test->getLifestyleMentalHealth() ."',
            '". $test->getLifestyleRiskProfile() ."',
            '". $test->getPhysiologicalVo2TestType() ."',
            '". $test->getPhysiologicalVo2TestValue1() ."',
            '". $test->getPhysiologicalVo2TestValue2() ."',
            '". $test->getPhysiologicalVo2Score() ."',
            '". $test->getPhysiologicalVo2() ."',
            '". $test->getPhysiologicalBalanceType() ."',
            '". $test->getPhysiologicalBalanceValue() ."',
            '". $test->getPhysiologicalBalance() ."',
            '". $test->getPhysiologicalSquatValue() ."',
            '". $test->getPhysiologicalSquat() ."',
            '". $test->getPhysiologicalSitupValue() ."',
            '". $test->getPhysiologicalSitup() ."',
            
            '". $test->getPhysiologicalSquat1rmSDirectEntry() ."',
            '". $test->getPhysiologicalSquat1rmDirectEntry() ."',
            '". $test->getPhysiologicalSquat1rmWeight() ."',
            '". $test->getPhysiologicalSquat1rmReps() ."',
            '". $test->getPhysiologicalSquat1rm() ."',
            '". $test->getPhysiologicalBenchPress1rmSDirectEntry() ."',
            '". $test->getPhysiologicalBenchPress1rmDirectEntry() ."',
            '". $test->getPhysiologicalBenchPress1rmWeight() ."',
            '". $test->getPhysiologicalBenchPress1rmReps() ."',
            '". $test->getPhysiologicalBenchPress1rm() ."',
            '". $test->getPhysiologicalDeadlift1rmSDirectEntry() ."',
            '". $test->getPhysiologicalDeadlift1rmDirectEntry() ."',
            '". $test->getPhysiologicalDeadlift1rmWeight() ."',
            '". $test->getPhysiologicalDeadlift1rmReps() ."',
            '". $test->getPhysiologicalDeadlift1rm() ."',
            '". $test->getPhysiologicalOverheadPressSDirectEntry() ."',
            '". $test->getPhysiologicalOverheadPressDirectEntry() ."',
            '". $test->getPhysiologicalOverheadPressWeight() ."',
            '". $test->getPhysiologicalOverheadPressReps() ."',
            '". $test->getPhysiologicalOverheadPress() ."',
            '". $test->getPhysiologicalSquat1minMin() ."',
            '". $test->getPhysiologicalPushUps1minMin() ."',
            '". $test->getPhysiologicalSitUps1minMin() ."',
            '". $test->getPhysiologicalRow500mMin() ."',
            '". $test->getPhysiologicalRow500mSec() ."',
            '". $test->getPhysiologicalStandingBroadJump() ."',
            '". $test->getPhysiologicalTimeTrialOption() ."',
            '". $test->getPhysiologicalTimeTrialMin() ."',
            '". $test->getPhysiologicalTimeTrialSec() ."',

            '". $test->getBiometricWaistValue() ."',
            '". $test->getBiometricWaistValueLost() ."',
            '". $test->getBiometricWaist() ."',
            '". $test->getBiometricChestLost() ."',
            '". $test->getBiometricChest() ."',
            '". $test->getBiometricAbdomen() ."',
            '". $test->getBiometricAbdomenLost() ."',
            '". $test->getBiometricHip() ."',
            '". $test->getBiometricHipLost() ."',
            '". $test->getBiometricThigh() ."',
            '". $test->getBiometricThighRight() ."',
            '". $test->getBiometricThighLeft() ."',
            '". $test->getBiometricThighLost() ."',
            '". $test->getBiometricArm() ."',
            '". $test->getBiometricArmRight() ."',
            '". $test->getBiometricArmLeft() ."',
            '". $test->getBiometricArmLost() ."',
            '". $test->getBiometricBodyFatValue() ."',
            '". $test->getBiometricBodyFat() ."',
            '". $test->getBiometricBloodPressureSystolic() ."',
            '". $test->getBiometricBloodPressureDiastolic() ."',
            '". $test->getBiometricBloodPressure() ."',
            '". $test->getBiometricBloodOxygenValue() ."',
            '". $test->getBiometricBloodOxygen() ."',
            '". $test->getTotalCrf() ."',
            '". $test->getHealthyLifePoints() ."',
            '". $test->getEstimatedHealthyLifeExpectancy() ."',
            '". $test->getMaxHle() ."',
            '". $test->getHealthyLifeScore() ."',
            '". $test->getEstimatedYearsOfHealthyLifeLeft() ."',
            '". $test->getEstimatedYearsYouWillLiveWithDid() ."',
            '". $test->getEstimatedYearsYouWillLiveWithDidAsOpposedTo() ."',
            '". $test->getYouCouldAddUpTo() ."',
            '". $test->getEstimatedCurrentHealthyLifeAge() ."',
            '". $test->getYearsAdded() ."',
            '". $test->getCmLost() ."',
            '". $test->getKgLost() ."',
            '". $test->getWaistHipRatio() ."',
            '". $test->getWaistHipRatioScore() ."',
            'finalised')");


        $lqc_answers = $this->getLifestyleQuestionnaireClientAnswer($test->getClientTestId());
        $program_client_test_id = $this->get12weekProgramClientTestIdByFullTestId($test->getClientTestId());
        for($i=0; $i<count($lqc_answers); $i++){
            mysqli_query($program_connection,"
                INSERT INTO tbl_lifestyle_questionnaire_client_answer
                    (lq_id,
                    client_test_id,
                    client_id,
                    answer,
                    answer_datetime,
                    `status`)
                VALUES
                    ('". $lqc_answers[$i]['lq_id'] ."',
                    '". $program_client_test_id ."',
                    '". $program_client['client_id'] ."',
                    '". $lqc_answers[$i]['answer'] ."',
                    '". $lqc_answers[$i]['answer_datetime'] ."',
                    'saved'
                        )
                ");
        }
        mysqli_query($program_connection,
            "UPDATE tbl_client SET s_opening_message_done = 1, s_lite_test_complete = 1 WHERE client_id=". $program_client['client_id']);

            
            
    }
    
    public function testInputValidations($post){
        $error = array();
        if(trim($post['biometric_waist_value']) == ''){
            $error[] = array(
                    'message' => "Waist must not be blank."
                );
        }

        if(trim($post['biometric_chest']) == ''){
            $error[] = array(
                    'message' => "Chest must not be blank."
                );
        }

        if(trim($post['biometric_abdomen']) == ''){
            $error[] = array(
                    'message' => "Abdomen must not be blank."
                );
        }

        if(trim($post['biometric_hip']) == ''){
            $error[] = array(
                    'message' => "HIP must not be blank."
                );
        }

        if(trim($post['biometric_thigh_right']) == '' || trim($post['biometric_thigh_left']) == ''){
            $error[] = array(
                    'message' => "Thigh must not be blank."
                );
        }

        if(trim($post['biometric_arm_right']) == '' || trim($post['biometric_arm_right']) == ''){
            $error[] = array(
                    'message' => "Arm must not be blank."
                );
        }

        if(trim($post['biometric_body_fat_value']) == ''){
            $error[] = array(
                    'message' => "Body fat must not be blank."
                );
        }
        
        return $error;
    }
    
    public function setClientHeight( $em, $club_client_id, $height){
        $client = $em->getRepository('AcmeHeadOfficeBundle:ClubClient')
                    ->findOneBy(array(
                        'club_client_id' => $club_client_id
                    ));
        $client->setHeight( $height );
        $em->persist($client);
        $em->flush();
    }
    
    public function getYearsAdded($formula, $first_test_details, $estimatedHealthyLifeExpectancy){
        
        if(isset($first_test_details['estimated_healthy_life_expectancy'])){
            $yearsAdded = $formula->yearsAdded($first_test_details['estimated_healthy_life_expectancy'], 
                            $estimatedHealthyLifeExpectancy);
        }else{
            $yearsAdded = '';
        }
        
        return $yearsAdded;
    }
    
    public function getCmLost($formula, $first_test_details, $biometric_waist_value){
        if(isset($first_test_details['biometric_waist_value'])){
            $cms_current_test = array(
                $biometric_waist_value,
                0,
                0,
                0,
                0,
                0
            );

            $cms_1st_test = array(
                $first_test_details['biometric_waist_value'],
                0,
                0,
                0,
                0,
                0
            );
            $cmLost = $formula->cmLost($cms_1st_test, $cms_current_test);
        }else{
            $cmLost = '';
        }
        
        return $cmLost;
    }
    
    public function getBiometricWaistValueLost($formula, $first_test_details, $biometric_waist_value){
        if(isset($first_test_details['biometric_waist_value'])){
            $biometricWaistValueLost = $formula->cmLost(
                    array($first_test_details['biometric_waist_value']), 
                    array($biometric_waist_value)
                    );
        }else{
            $biometricWaistValueLost = '';
        }
        
        return $biometricWaistValueLost;
    }
    
    public function getBiometricChestLost($formula, $first_test_details, $biometric_chest_value){
        $biometricChestLost = $formula->cmLost(
                    array(0), 
                    array(0)
                    );
        
        return $biometricChestLost;
    }
    
    
    public function getBiometricThighLost($formula, $first_test_details, $biometric_thigh_right, $biometric_thigh_left){
        $biometricThighLost = $formula->cmLost(
                    array(0), 
                    array(0)
                    );
        
        return $biometricThighLost;
    }
    
    public function getBiometricArmLost($formula, $first_test_details, $biometric_arm_right, $biometric_arm_left){
        $biometricArmLost = $formula->cmLost(
                    array(0), 
                    array(0)
                    );
        
        return $biometricArmLost;
    }
    
    public function getBiometricAbdomenLost($formula, $first_test_details, $biometric_abdomen){
        $biometricAbdomenLost = $formula->cmLost(
                    array(0), 
                    array(0)
                    );
        
        return $biometricAbdomenLost;
    }
    
    public function getBiometricHipLost($formula, $first_test_details, $biometric_hip){
        $biometricHipLost = $formula->cmLost(
                    array(0), 
                    array(0)
                    );
        
        return $biometricHipLost;
    }
    
    
    
    
    
    public function createNewLiteTest($session, $client_details, $client_id, $test_type){
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){
            return NULL;
        }else{
//        if($session->get('club_admin_id') != '' && $session->get('user_role') != 'admin'){
            $em = $this->getDoctrine()->getManager();
            $drafttest = new ClientTest();
            $drafttest->setClubClientId( $client_id );
            $drafttest->setClubAdminId($session->get('club_admin_id'));
            $drafttest->setStatus('draft');
            $drafttest->setTestDatetime($datetime->format('Y-m-d H:i:s'));
            if(isset($client_details['height']) && $client_details['height'] != '' && $client_details['height'] > 0){
                $drafttest->setHeight($client_details['height']);
            }
            if($drafttest->getStatus() != 'finalised'){
                $drafttest->setTestType($test_type);
            }
            $em->persist($drafttest);
            $em->flush();
            
            return $drafttest->getClientTestId();
        }
    }
    
    public function updateClientTestType($client_test_id, $test_type){
        $test_type = strtolower($test_type);
        
        
        $em = $this->getDoctrine()->getManager();
        $drafttest = $em->getRepository('AcmeHeadOfficeBundle:ClientTest')->findOneBy(
                array(
                    'client_test_id'=>$client_test_id
                )
            );
        if(count($drafttest) > 0 && $drafttest->getStatus() != 'finalised'){
            $drafttest->setTestType($test_type);
            $em->persist($drafttest);
            $em->flush();
        }
    }
    
    
    
    
//    // GET LIFESTYLE QUESTIONNAIRE TEMPLATE
//    public function testQuestionnaireTemplateAction()
//    {
//        $session = $this->getRequest()->getSession();
//        
//        
//        if(!isset($_POST['code'])){
//            //return $this->redirect($this->generateUrl('acme_club_login'));
//            return new Response("invalid parameter");
//        }
//        
//        $_POST['code'] = trim($_POST['code']);
//        
//        $lifestyle_codes = array("physical_activity", "smoking", "alcohol", "nutrition", "mental_health", "risk_profile");
//
//        if (!in_array($_POST['code'], $lifestyle_codes)){
//          return new Response("invalid parameter");
//        }
//        
//        //$smoking_answer = ($_POST['code'] == 'smoking') ? $this->getSpecificTestQuestionnaireClientAnswer(3 /*smoking*/, $club_client_id, $_POST['client_test_id']) : '';
//        
//        return new Response($this->renderView(
//            'AcmeLiteTestBundle:LiteTest:test_questionnaire_template_'. $_POST['code'] .'.html.twig',
//                array(
//                    'gender' => $_POST['gender'],
//                    'questions' => $this->getTestQuestionnaire($_POST['code'], NULL, NULL),
//                    'options' => $this->getTestQuestionnaireOptionsByCode($_POST['code']),
//                    'smoking_answer' => ($_POST['code'] == 'smoking') ? $this->getSpecificTestQuestionnaireClientAnswer(3 /*smoking*/, NULL, NULL) : ''
//                )
//            ));
//        
//    }
//    
//    // SUBMIT LIFESTYLE ANSWER
//    public function lifeStyleTestAnswerAction(){
//        $session = $this->getRequest()->getSession();
//        $datetime = new \DateTime(date("Y-m-d H:i:s"));
//        $formula = new Model\TestFormulas();
//        
//        $error = array();
//        
//        if($_POST['answer_type'] == 'risk-profile'){
//            $error_count = 0;
//            foreach ($_POST['answer'] as $lifestyle_question_id => $answer) {
//                if(trim($answer) != '' && ($answer > 7 || $answer < 1)){
//                    $error_count +=1;
//                }
//            }
//            if( $error_count > 0){
//                $error[] = 'Make sure that your answer(s) must not exceed to 7 (highly likely) and not below 1  (highly unlikely).';
//            }
//        }
//        
//        if(count($error) == 0 ){
//			
//            if($_POST['answer_type'] == 'physical-activity' ||
//                $_POST['answer_type'] == 'alcohol' || 
//                $_POST['answer_type'] == 'mental-health' ||
//                $_POST['answer_type'] == 'risk-profile' ||
//                $_POST['answer_type'] == 'nutrition' ){
//				$lq_ans = array();
//                if(isset($_POST['answer'])){
//                    foreach ($_POST['answer'] as $lifestyle_question_id => $answer) {
//						$lq_ans[$lifestyle_question_id] = array(
//								'answer'=> $answer
//							);
//						
//                    }
//					$session->set('litetest-'.$_POST['answer_type'], $lq_ans);
//					
//                }else{
//				
//					$session->set('litetest-'.$_POST['answer_type'], array() );
//				}
//				
//            }elseif($_POST['answer_type'] == 'smoking'){
//				$lq_ans = array();
//                $lq_ans[3] = array(
//								'answer'=> $_POST['answer']
//							);
//							
//				$session->set('litetest-smoking', $lq_ans);
//
//            }
//
//			
//            $s_complete = $this->sLiteLifestyleTestQuestionnaireAnswersDone($_POST['answer_type']);
//            
//            $response = array(
//                'result' => 'success',
//                'code' => $_POST['answer_type'],
//                's_complete' => $s_complete
//            );
//        }else{
//            $response = array(
//                'result' => 'error',
//                'message' => $error[0]
//            );
//        }
//        
//        
//        return new Response(json_encode($response));
//    }
//	
//    
//	public function sLiteLifestyleTestQuestionnaireAnswersDone($code, $nutrition_anwers = null){
//			$session = $this->getRequest()->getSession();
//            $codesearch = str_replace('-', '_', $code);
//            $questionnaire = $this->getTestQuestionnaire($codesearch);
//
//            $questions = count($questionnaire);    
//            if($code != 'nutrition'){
//                
//                $answers = 0;
//                for($i=0; $i<count( $questionnaire ); $i++){
//                                    //$item = $this->getSpecificTestQuestionnaireClientAnswer($questionnaire[$i]['lq_id'], $club_client_id, $client_test_id);
//                    $code_session = $session->get('litetest-'.$code);
//                    $answers += (isset($code_session[ $questionnaire[$i]['lq_id'] ]['answer']) && $code_session[ $questionnaire[$i]['lq_id'] ]['answer'] != '') ? 1 : 0;
//
//                    //$answers += ($code_session[ $questionnaire[$i]['lq_id'] ]['answer'] != '') ? 1 : 0;
//                }
//            }else{
//                $answers= 0;
//                for($i=0; $i<count( $questionnaire ); $i++){
//                    $answers += (isset($nutrition_anwers[ $questionnaire[$i]['lq_id'] ])) ? 1 : 0;
//                }
//                
//            }
//            
//            return ($questions == $answers) ? true : false;
//    }
//    
//    
//    
//    
////==============================================================================
//// GET TEST RESULTS (AJAX)
////==============================================================================
//    public function getBmiResultAction(){
//        $formula = new Model\TestFormulas();
//        
//        if(!isset($_POST['weight'])){
//            return new Response('Invalid parameter input!');
//        }
//        
//        if(!isset($_POST['height'])){
//            return new Response('Invalid parameter input!');
//        }
//        $_POST['weight'] = intval($_POST['weight']);
//        $_POST['height'] = intval($_POST['height']);
//        
//        $response = $formula->bMI($_POST['height'], $_POST['weight']);
//        return new Response($response);
//        
//    }
//    
//    public function getPhysicalActivityResultAction(){
//        $session = $this->getRequest()->getSession();
//        $formula = new Model\TestFormulas();
//        
//        if(!isset($_POST['moderate_vigorous_pa'])){
//            return new Response('Invalid parameter input!');
//        }
//        
//        if(!isset($_POST['muscle_strenthening_pa'])){
//            return new Response('Invalid parameter input!');
//        }
//        
//        
//        $_POST['moderate_vigorous_pa'] = intval($_POST['moderate_vigorous_pa']);
//        $_POST['muscle_strenthening_pa'] = intval($_POST['muscle_strenthening_pa']);
//        
//        $moderate_vigorous_pa = $formula->moderateOrVigorousIntensityPhysicalActivity($_POST['moderate_vigorous_pa']);
//        $muscle_strenthening_pa = $formula->muscleStrentheningPhysicalActivity($_POST['muscle_strenthening_pa']);
//        
//        $response = $formula->physicalActivity($moderate_vigorous_pa, $muscle_strenthening_pa, TRUE);
//        
//        
//        
//        $session->set('lifestyle-physical-activity', $response);
//        return new Response($response);
//        
//    }
//    
//    public function getSmokingResultAction(){
//        $session = $this->getRequest()->getSession();
//        $formula = new Model\TestFormulas();
//        
//        if(!isset($_POST['gender'])){
//            return new Response('Invalid parameter input!');
//        }
//        
//        if(!isset($_POST['answer'])){
//            return new Response('Invalid parameter input!');
//        }
//        
//        $_POST['answer'] = intval($_POST['answer']);
//        
//        $response = $formula->smokingHabits($_POST['gender'], $_POST['answer'], TRUE);
//        
//        $session->set('lifestyle-smoking', $response);
//        
//        return new Response($response);
//    }
//    
//    public function getAlcoholConsumptionResultAction(){
//        $session = $this->getRequest()->getSession();
//        $formula = new Model\TestFormulas();
//        
//        if(!isset($_POST['gender'])){
//            return new Response('Invalid parameter input!');
//        }
//        
//        if(!isset($_POST['per_week_value'])){
//            return new Response('Invalid parameter input!');
//        }
//        
//        if(!isset($_POST['one_sitting_value'])){
//            return new Response('Invalid parameter input!');
//        }
//        
//        $_POST['per_week_value'] = intval($_POST['per_week_value']);
//        $_POST['one_sitting_value'] = intval($_POST['one_sitting_value']);
//        
//        $alcohol_consumption_per_week = $formula->alcoholConsumptionPerWeekResult($_POST['gender'], $_POST['per_week_value']);
//        $alcohol_consumption_in_one_sitting = $formula->alcoholConsumptionInOneSittingResult($_POST['one_sitting_value']);
//        
//        $response = $formula->alcoholConsumptionTotal($alcohol_consumption_per_week, $alcohol_consumption_in_one_sitting, TRUE);
//        
//        // UPDATE tbl_client_test
//        $session->set('lifestyle-alcohol', $response);
//        
//        return new Response($response);
//    }
//    
//
//    public function getNutritionResultAction(){
//        $session = $this->getRequest()->getSession();
//        $formula = new Model\TestFormulas();
//        
//        if(isset($_POST['answer'])){
//            $number_of_yes = 0;
//            foreach ($_POST['answer'] as $lifestyle_question_id => $answer) {
//                $number_of_yes += intval($answer);
//            }
//
//            $response = $formula->nutrition($number_of_yes, TRUE);
//
//        }else{
//            $response = '';
//        }
//        $session->set('lifestyle-nutrition', $response);
//        
//        return new Response($response);
//    }
//    
//    
//    public function getMentalHealthResultAction(){
//        $session = $this->getRequest()->getSession();
//        $formula = new Model\TestFormulas();
//        
//        if(isset($_POST['answer'])){
//            $mental_health_value = 0;
//            foreach ($_POST['answer'] as $lifestyle_question_id => $answer) {
//                if($answer != ''){
//                    $mental_health_value += intval($answer);
//                }
//            }
//            if($mental_health_value > 0){
//                $response = $formula->mentalHealth($mental_health_value);
//
//            }else{
//                $response = '';
//            }
//        }else{
//            $response = '';
//        }
//        
//        // UPDATE tbl_client_test
//        if($_POST['client_test_id'] != ''){
//            $em = $this->getDoctrine()->getManager();
//            $test = $em->getRepository('AcmeHeadOfficeBundle:ClientTest')
//                        ->findOneBy(array(
//                            'client_test_id' => $_POST['client_test_id'],
//                        ));
//            if($response != ''){
//                $test->setLifestyleMentalHealth($response);
//            }else{
//                $test->setLifestyleMentalHealth(NULL);
//            }
//            $em->persist($test);
//            $em->flush();
//        }
//        
//        $session->set('lifestyle-mental-health', $response);
//        
//        return new Response($response);
//    }
//    
//    
//    public function getRiskProfileResultAction(){
//        $session = $this->getRequest()->getSession();
//        $formula = new Model\TestFormulas();
//        
//        if(isset($_POST['answer'])){
//            $risk_profile_value = 0;
//            foreach ($_POST['answer'] as $lifestyle_question_id => $answer) {
//                if($answer != ''){
//                    $risk_profile_value += intval($answer);
//                }
//            }
//            if($risk_profile_value > 0){
//                //$response = $formula->mentalHealth($mental_health_value);
//                $response = $formula->riskProfile($risk_profile_value);
//
//            }else{
//                $response = '';
//            }
//        }else{
//            $response = '';
//        }
//        
//        // UPDATE tbl_client_test
//        if($_POST['client_test_id'] != ''){
//            $em = $this->getDoctrine()->getManager();
//            $test = $em->getRepository('AcmeHeadOfficeBundle:ClientTest')
//                        ->findOneBy(array(
//                            'client_test_id' => $_POST['client_test_id'],
//                        ));
//            if($response != ''){
//                $test->setLifestyleRiskProfile($response);
//            }else{
//                $test->setLifestyleRiskProfile(NULL);
//            }
//            $em->persist($test);
//            $em->flush();
//        }
//        
//        $session->set('lifestyle-risk-profile',$response);
//        
//        return new Response($response);
//    }
//    
//    public function getVo2TestAction(){
//        $formula = new Model\TestFormulas();
//        
//        if(!isset($_POST['gender'])){
//            return new Response('Invalid parameter input!');
//        }
//        
//        if(!isset($_POST['weight'])){
//            return new Response('Invalid parameter input!');
//        }
//        
//        if(!isset($_POST['age'])){
//            return new Response('Invalid parameter input!');
//        }
//        
//        if(!isset($_POST['test_type'])){
//            return new Response('Invalid parameter input!');
//        }
//        
//        if(!isset($_POST['value1'])){
//            return new Response('Invalid parameter input!');
//        }
//        
//        if(!isset($_POST['value2'])){
//            return new Response('Invalid parameter input!');
//        }
//        
//        $_POST['weight'] = intval($_POST['weight']);
//        $_POST['age'] = intval($_POST['age']);
//        $_POST['value1'] = intval($_POST['value1']);
//        $_POST['value2'] = intval($_POST['value2']);
//        
//        if($_POST['test_type'] == 'bike'){
//            $score = $formula->vo2BikeTest($_POST['gender'], $_POST['weight'], $_POST['age'], $_POST['value1']);
//            
//        }elseif($_POST['test_type'] == 'resting-hr'){
//            
//            $score = $formula->vo2RestingHrTest($_POST['age'], $_POST['value1']);
//            
//        }elseif($_POST['test_type'] == 'treadmill'){
//            
//            $score = $formula->vo2TreadmillTest($_POST['gender'], $_POST['weight'], $_POST['value1'], $_POST['value2']);
//        
//        }elseif($_POST['test_type'] == 'beep'){
//            $score = $formula->vo2BeepTest($_POST['value1'], $_POST['value2']);
//        }else{
//            
//            $score = $formula->vo2DirectEntry($_POST['value1']);
//        }
//        
//        $result = $formula->vo2($_POST['gender'], $_POST['age'], $score);
//        
//        $response = array(
//            'score'=>$score,
//            'result'=>$result
//            );
//        
//        return new Response(json_encode($response));
//        
//    }
//    
//    public function getBalanceTestAction(){
//        $formula = new Model\TestFormulas();
//        
//        if(!isset($_POST['gender'])){
//            return new Response('Invalid parameter input!');
//        }
//        if(!isset($_POST['value'])){
//            return new Response('Invalid parameter input!');
//        }
//        
//        $_POST['value'] = intval($_POST['value']);
//        
//        $response = $formula->balance($_POST['test_type'], $_POST['gender'], $_POST['value']);
//        
//        
//        return new Response($response);
//    }
//    
//    // removelater
//    public function getWaistTestAction(){
//        $formula = new Model\TestFormulas();
//        
//        if(!isset($_POST['gender'])){
//            return new Response('Invalid parameter input!');
//        }
//        
//        $_POST['value'] = intval($_POST['value']);
//        
//        $response = $formula->waist($_POST['gender'], $_POST['value']);
//        
//        
//        return new Response($response);
//    }
//    
//    // removelater
//    public function getSquatTestAction(){
//        $formula = new Model\TestFormulas();
//        
//        if(!isset($_POST['gender'])){
//            return new Response('Invalid parameter input!');
//        }
//        
//        $_POST['value'] = intval($_POST['value']);
//        
//        $response = $formula->waist($_POST['gender'], $_POST['value']);
//        
//        
//        return new Response($response);
//    }
//    
//    
//    public function createShareImageAction($slug){
//        
//        $session = $this->getRequest()->getSession();
//        
//        header('Content-type: image/jpeg');
//        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
//        //$path = ($_SERVER['HTTP_HOST'] == 'localhost') ? $mod->siteURL()."/airport/web/dev/uploads/images" .$session->get("image_library_parent_dir") : $mod->siteURL()."/web/dev/uploads/images" . $session->get("image_library_parent_dir");
//        // Create Image From Existing File
//        $jpg_image = imagecreatefromjpeg($root_dir.'/img/FB-Share_empty.jpg');
//        $test_details = $this->getLiteTestDetailsById($slug);
//        // Allocate A Color For The Text
//        $white = imagecolorallocate($jpg_image, 108, 70, 149);
//
//        // Set Path to Font File
//        $font_path = $root_dir.'/fonts/OpenSans-Bold.ttf';
//
//        // Set Text to Be Printed On Image
//        $text1 = round($test_details['healthy_life_score'] *100,1).'%';
//        $text2 = round($test_details['estimated_current_healthy_life_age']);
//
//        // Print Text On Image
//        imagettftext($jpg_image, 70, 0, 45, 420, $white, $font_path, $text1);
//        imagettftext($jpg_image, 70, 0, 420, 420, $white, $font_path, $text2);
//        // Send Image to Browser
//        imagejpeg($jpg_image);
//
//        // Clear Memory
//        imagedestroy($jpg_image);
//
//
//    }
//    
//    public function shareResultAction($slug){
//       
//        return $this->render('AcmeLiteTestBundle:LiteTest:test_share.html.twig',array('litetest_id'=>$slug)); 
//
//    }
//    
//    public function confirmationAction(){
//       $session = $this->getRequest()->getSession(); 
//       return $this->render('AcmeLiteTestBundle:LiteTest:test_confirmation.html.twig',array('litetest_id'=>$session->get('litetest_id'))); 
//
//    }
//    
//    public function loadNearestClubAction(){
//        if($_POST['postcode']){
//        $lat_long = $this->get_lat_long('Australia '.$_POST['postcode']);
//            
//        $str_lat_long = explode(",",$lat_long);
//        
//        $data = $this->getNearestClub($str_lat_long[0], $str_lat_long[1],$_POST['postcode']);
//        //$data = $this->getNearestClub('-35.2026134', '149.0310104');
//
//        return new Response(json_encode($data, 0));
//        }
//        
//    }
}
