<?php

namespace Acme\ClubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;
use Acme\HeadOfficeBundle\Entity\Club;
use Acme\HeadOfficeBundle\Entity\ClubAdmin;
use Acme\HeadOfficeBundle\Entity\Payment;

class ClubRegistrationController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function registerAction(){
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction(); 
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $error_count = 0;
        $admin_id = '';
        
        
        if(isset($_POST['save'])){
            if(isset($_POST['clubname'])){
            $model = new Club();
            
            $model->setClubName($_POST['clubname']);
            $model->setAddress($_POST['address']);
            $model->setCity($_POST['city']);
            $model->setState($_POST['state']);
            $model->setLam($_POST['lam']);
            $model->setPostcode($_POST['postcode']);
            $model->setPhone($_POST['postcode']);
            $model->setCountry('Australia');
            $model->setStatus('active');
            $model->setRegisteredDate($datetime->format("Y-m-d H:i:s"));
            if(isset($_POST['registration_option']) && trim($_POST['registration_option']) == 'monthly'){
                $model->setAvailableCredit(0);
                $model->setPaymentOption('monthly');
            }else{
                $model->setAvailableCredit(10);
                $model->setPaymentOption('regular');
            }
            
            
            $model->setCardNumber($_POST['card_number']);
            $model->setCardExpiryMonth($_POST['card_month']);
            $model->setCardExpiryYear($_POST['card_year']);
            $model->setCardCvc($_POST['ccv']);
            $model->setCardName($_POST['card_name']);
            $model->setCardAddressLine1($_POST['bill_address']);
            $model->setCardAddressCity($_POST['bill_city']);
            $model->setCardAddressPostcode($_POST['bill_postcode']);
            $model->setCardAddressState($_POST['bill_state']);
            $model->setCardAddressCountry('Australia');
            $em->persist($model);
            $em->flush();
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
                if($error_count == 0){
                    if (isset($_POST['fname']) && isset($_POST['email'])){
                        $model1 = new ClubAdmin();

                        $model1->setClubId($model->getClubId());
                        $model1->setFname($_POST['fname']);
                        $model1->setLname($_POST['lname']);
                        $model1->setPassword($mod->passGenerator($_POST['password']));
                        $model1->setEmail($_POST['email']);
                        $model1->setStatus('active');
                        $model1->setRole('site-admin');
                        $model1->setShowFirstLoginMsg(1);
                        $model1->setRegisteredDate($datetime->format("Y-m-d H:i:s"));
                        $em->persist($model1);
                        $em->flush();

                        $validator = $this->get('validator');
                        $errors = $validator->validate($model1);
                        $error_count = count($errors);

                    }
                    $admin_id = $model1->getClubAdminId();
                 }
                if($error_count == 0){

                    

                    if(isset($_POST['registration_option']) && trim($_POST['registration_option']) == 'monthly'){
                        // Register customer
                        //http://www.templemantwells.com.au/article/tools-tips/integrating-pin-payments-using-php
                        //Authentication
                        $auth['u'] = $this->container->getParameter('pin_payment_api_private_key'); //Private Key from PIN
                        $auth['p'] = $this->container->getParameter('pin_payment_api_password'); //API calls use empty password

                        //Fields to Post                    
                        $post = array();
                        $post['email'] = $_POST['email'];
                        $post['card[number]'] = $_POST['card_number'];
                        $post['card[expiry_month]'] = $_POST['card_month'];
                        $post['card[expiry_year]'] = $_POST['card_year'];
                        $post['card[cvc]'] = $_POST['ccv'];
                        $post['card[name]'] = $_POST['card_name'];
                        $post['card[address_line1]'] = $_POST['bill_address'];
                        //$post['card[address_line2]'] = '';
                        $post['card[address_city]'] = $_POST['bill_city'];
                        $post['card[address_postcode]'] = $_POST['bill_postcode'];
                        $post['card[address_state]'] = $_POST['bill_state'];
                        $post['card[address_country]'] = 'Australia';

                        // Create a curl handle
                        $ch = curl_init( $this->container->getParameter('pin_payment_api_customer_url') );

                        curl_setopt($ch, CURLOPT_USERPWD, $auth['u'] . ":" . $auth['p']); //authenticate
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //make sure it returns a response
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // allow https verification if true
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // allow https verification if true
                        curl_setopt($ch, CURLOPT_POST, 1); //tell it we are posting
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $post); //tell it what to post
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json')); //response comes back as json
                        
                        // Execute
                        $response = curl_exec($ch);

                        // Check if any error occurred
                        if(!curl_errno($ch)){$info = curl_getinfo($ch);}

                        // Close handle
                        curl_close($ch);
                        
                        //Decode JSON
                        $response = json_decode($response, true);
                        
                        // PAYMENT
                        if(!isset($response['error'])){
                            $pin_payment_customer_token = $response['response']['token'];
                            $amount = 200;
                            //Fields to Post
                            $post = array();
                            $post['customer_token'] = $pin_payment_customer_token;
                            $post['amount'] = ($amount * 100);
                            $post['currency'] = "AUD";
                            $post['description'] = urlencode("Payment for HLP Club unlimited tests");
                            $post['email'] = $_POST['email'];
                            $post['ip_address'] = $_SERVER['REMOTE_ADDR'];

                            // Create a curl handle
                            $ch = curl_init( $this->container->getParameter('pin_payment_api_url') );

                            curl_setopt($ch, CURLOPT_USERPWD, $auth['u'] . ":" . $auth['p']); //authenticate
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //make sure it returns a response
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // allow https verification if true
                            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // allow https verification if true
                            curl_setopt($ch, CURLOPT_POST, 1); //tell it we are posting
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $post); //tell it what to post
                            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json')); //response comes back as json

                            // Execute
                            $response = curl_exec($ch);
                            
                            // Check if any error occurred
                            if(!curl_errno($ch)){$info = curl_getinfo($ch);}

                            // Close handle
                            curl_close($ch);

                            //Decode JSON
                            $response = json_decode($response, true);
                        }
                        
                        
                    }else{
                        //http://www.templemantwells.com.au/article/tools-tips/integrating-pin-payments-using-php
                        //Authentication
                        $auth['u'] = $this->container->getParameter('pin_payment_api_private_key'); //Private Key from PIN
                        $auth['p'] = $this->container->getParameter('pin_payment_api_password'); //API calls use empty password

                        $amount = 169;
                        //Fields to Post
                        $post = array();
                        $post['amount'] = ($amount * 100);
                        $post['currency'] = "AUD";
                        $post['description'] = urlencode("Payment for HLP Club registration");
                        $post['email'] = $_POST['email'];
                        $post['ip_address'] = $_SERVER['REMOTE_ADDR'];
                        $post['card[number]'] = $_POST['card_number'];
                        $post['card[expiry_month]'] = $_POST['card_month'];
                        $post['card[expiry_year]'] = $_POST['card_year'];
                        $post['card[cvc]'] = $_POST['ccv'];
                        $post['card[name]'] = $_POST['card_name'];
                        $post['card[address_line1]'] = $_POST['bill_address'];
                        //$post['card[address_line2]'] = '';
                        $post['card[address_city]'] = $_POST['bill_city'];
                        $post['card[address_postcode]'] = $_POST['bill_postcode'];
                        $post['card[address_state]'] = $_POST['bill_state'];
                        $post['card[address_country]'] = 'Australia';

                        // Create a curl handle
                        $ch = curl_init( $this->container->getParameter('pin_payment_api_url') );

                        curl_setopt($ch, CURLOPT_USERPWD, $auth['u'] . ":" . $auth['p']); //authenticate
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //make sure it returns a response
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // allow https verification if true
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // allow https verification if true
                        curl_setopt($ch, CURLOPT_POST, 1); //tell it we are posting
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $post); //tell it what to post
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json')); //response comes back as json
                        
                        // Execute
                        $response = curl_exec($ch);

                        // Check if any error occurred
                        if(!curl_errno($ch)){$info = curl_getinfo($ch);}

                        // Close handle
                        curl_close($ch);
                        
                        //Decode JSON
                        $response = json_decode($response, true);
                    }
                    

                    

                    



                    if(isset($response['error'])){

                        if(isset($response['messages'])){
                            for($i=0; $i<count($response['messages']); $i++){
                                $error[] = array('message'=> $response['messages'][$i]['message']);
                            }
                        }else{
                            if(isset($response['error_description'])){
                                $error[] = array('message'=> $response['error_description']);
                            }
                        }



                        $this->get('session')->getFlashBag()->add(
                            'error',
                            $error
                        );


                        return $this->render('AcmeClubBundle:ClubRegistration:registration.html.twig',
                            array('states'=> $this->getStatesForClub(),
                                  'lams'=> $this->getLams(),
                                  'post'=>$_POST));

                    }else{

                        $payment = new Payment();

                        $payment->setAmount(169);
                        $payment->setPaymentDatetime($datetime->format("Y-m-d H:i:s"));
                        $payment->setPrice(169);
                        $payment->setQty(1);
                        $payment->setStatus('completed');
                        $payment->setUserId($admin_id);
                        $payment->setUserType('site-admin');
                        $em->persist($payment);
                        $em->flush();
                        
                        
                        
                        $em->getConnection()->commit(); 

                        $to = $_POST['email'];
                        $cc = $this->container->getParameter('site_email_address');
                        
                        
                        
                        $invoice_body =  $this->renderView('AcmeClubBundle:ClubRegistration:invoice_template.html.twig',
                                    array(
                                        'invoicedate'=> $datetime->format("Y-m-d H:i:s"),
                                        'invoicenumber'=> (isset( $payment )) ? $payment->getPaymentId() : 1,
                                        'post'=> $_POST
                                        )
                                    );
                        
                        $this->sendEmail($to, $cc, $this->container->getParameter('site_email_address'), 'HLP Club Registration Tax Invoice', $invoice_body);

                        
                        $subject = 'HLP Club Registration';
                        //$site_address = ($session->get('user_type') == 'club-user') ? $this->getRequest()->getUriForPath('/') : $this->getRequest()->getUriForPath('/admin');
                        $body = $this->renderView('AcmeClubBundle:ClubRegistration:email_template.html.twig',
                                    array(
                                        'fname'=> $_POST['fname'],
                                        'club_name'=> $_POST['clubname']
                                        )
                                    );

                        $this->sendEmail($to, $cc, $this->container->getParameter('site_email_address'), $subject, $body);


                        $details = $_POST['fname'] . " " . $_POST['lname'] . " registered " . $_POST['clubname'] . " and paid $169 with free $10 initial credit.";
                        $this->setActivity($admin_id, 'site-admin', $details);

                        $session->set('club_admin_id', $admin_id); 
                        $session->set('email', $_POST['email']); 
                        $session->set('fname', $_POST['fname']); 
                        $session->set('lname', $_POST['lname']);
                        $session->set('club_id', $model->getClubId());
                        $session->set('club_name', $_POST['clubname']);
                        $session->set('user_type', 'club-user');
                        $session->set('user_role', 'site-admin');
                        $session->set('show_first_login_msg', 1);

                        $session->set('office-address', $_POST['address']);
                        $session->set('office-postcode', $_POST['postcode']);
                        $session->set('office-state', $_POST['state']);
                        $session->set('office-city', $_POST['city']);
                        if(isset($_POST['registration_option']) && trim($_POST['registration_option']) == 'monthly'){
                            $session->set('available_credit', 0);
                        }else{
                            $session->set('available_credit', 10);
                        }

                        //$session->set('my_clubs', $clubs);

                        // last login
                        $em = $this->getDoctrine()->getManager();
                        $em->getConnection()->beginTransaction();
                        $model2 = $em->getRepository('AcmeHeadOfficeBundle:ClubAdmin')->findOneBy(array('club_admin_id'=>$admin_id));
                        $model2->setLastLoginDatetime($datetime->format('Y-m-d H:i:s'));
                        $em->persist($model2);
                        $em->flush();
                        
                        if(isset($_POST['registration_option']) && trim($_POST['registration_option']) == 'monthly'){
                            if(isset($pin_payment_customer_token)){
                                $club_update = $em->getRepository('AcmeHeadOfficeBundle:Club')->findOneBy(array('club_id'=>$model->getClubId()));
                                $club_update->setPinPaymentCustomerToken($pin_payment_customer_token);
                                $club_update->setMonthlyFeeStatus("paid");
                                $em->persist($club_update);
                                $em->flush();
                            }
                        }
                        $em->getConnection()->commit(); 


                    }
                }
//                return $this->render('AcmeClubBundle:ClubRegistration:registration_success.html.twig',
//                array('club_name'=> $_POST['clubname']));
                
//                $this->get('session')->getFlashBag()->add(
//                            'club-registration-success',
//                            'Your Club has been successfully registered.'
//                        );
                
                return $this->redirect($this->generateUrl('acme_club_registration_successful'));
            }
        
        }else{
            return $this->render('AcmeClubBundle:ClubRegistration:registration.html.twig',
                array('states'=> $this->getStatesForClub(),
                      'lams'=> $this->getLams()));
        }
    }
    
    public function registrationSuccessAction(){
        return $this->render('AcmeClubBundle:ClubRegistration:registration_success.html.twig');
    }
    
    
    public function registrationSaveAction(){
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction(); 
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $error_count = 0;
        $admin_id = '';
        
        if(isset($_POST['clubname'])){
            $model = new Club();
            
            $model->setClubName($_POST['clubname']);
            $model->setAddress($_POST['address']);
            $model->setCity($_POST['city']);
            $model->setState($_POST['state']);
            $model->setLam($_POST['lam']);
            $model->setPostcode($_POST['postcode']);
            $model->setCountry('Australia');
            $model->setStatus('active');
            $model->setAvailableCredit(10);
            
            $em->persist($model);
            $em->flush();
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            if($error_count == 0){
                if (isset($_POST['fname']) && isset($_POST['email'])){
                    $model1 = new ClubAdmin();

                    $model1->setClubId($model->getClubId());
                    $model1->setFname($_POST['fname']);
                    $model1->setLname($_POST['lname']);
                    $model1->setPassword($mod->passGenerator($_POST['password']));
                    $model1->setEmail($_POST['email']);
                    $model1->setStatus('active');
                    $model1->setRole('site-admin');
                    $em->persist($model1);
                    $em->flush();

                    $validator = $this->get('validator');
                    $errors = $validator->validate($model1);
                    $error_count = count($errors);

                }
                $admin_id = $model1->getClubAdminId();
             }
            if($error_count == 0){
                
                //http://www.templemantwells.com.au/article/tools-tips/integrating-pin-payments-using-php
                //Authentication
                $auth['u'] = $this->container->getParameter('pin_payment_api_private_key'); //Private Key from PIN
                $auth['p'] = $this->container->getParameter('pin_payment_api_password'); //API calls use empty password
                
                $amount = 169;
                //Fields to Post
                $post = array();
                $post['amount'] = ($amount * 100);
                $post['currency'] = "AUD";
                $post['description'] = urlencode("Payment for HLP Club registration");
                $post['email'] = $_POST['email'];
                $post['ip_address'] = $_SERVER['REMOTE_ADDR'];
                $post['card[number]'] = $_POST['card_number'];
                $post['card[expiry_month]'] = $_POST['card_month'];
                $post['card[expiry_year]'] = $_POST['card_year'];
                $post['card[cvc]'] = $_POST['ccv'];
                $post['card[name]'] = $_POST['card_name'];
                $post['card[address_line1]'] = $_POST['bill_address'];
                //$post['card[address_line2]'] = '';
                $post['card[address_city]'] = $_POST['bill_city'];
                $post['card[address_postcode]'] = $_POST['bill_postcode'];
                $post['card[address_state]'] = $_POST['bill_state'];
                $post['card[address_country]'] = 'Australia';

                // Create a curl handle
                $ch = curl_init( $this->container->getParameter('pin_payment_api_url') );

                curl_setopt($ch, CURLOPT_USERPWD, $auth['u'] . ":" . $auth['p']); //authenticate
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //make sure it returns a response
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // allow https verification if true
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // allow https verification if true
                curl_setopt($ch, CURLOPT_POST, 1); //tell it we are posting
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post); //tell it what to post
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json')); //response comes back as json

                // Execute
                $response = curl_exec($ch);

                // Check if any error occurred
                if(!curl_errno($ch)){$info = curl_getinfo($ch);}

                // Close handle
                curl_close($ch);

                //Decode JSON
                $response = json_decode($response, true);



                if(isset($response['error'])){

                    for($i=0; $i<count($response['messages']); $i++){
                        $error[] = array('message'=> $response['messages'][$i]['message']);
                    }


                    $this->get('session')->getFlashBag()->add(
                        'error',
                        $error
                    );
                    
                    

                    return $this->redirect($this->generateUrl('acme_club_registration'));
                    
                }else{


                    $em->getConnection()->commit(); 

//                    $this->get('session')->getFlashBag()->add(
//                        'credit-success',
//                        $_POST['qty'] . ' credit has been added successfully.'
//                    );

                    $to = $_POST['email'];
                    $cc = $this->container->getParameter('site_email_address');
                    $subject = 'HLP Club Registration';
                    //$site_address = ($session->get('user_type') == 'club-user') ? $this->getRequest()->getUriForPath('/') : $this->getRequest()->getUriForPath('/admin');
                    $body = $this->renderView('AcmeClubBundle:ClubRegistration:email_template.html.twig',
                                array(
                                    'fname'=> $_POST['fname'],
                                    'club_name'=> $_POST['clubname']
                                    )
                                );

                    $this->sendEmail($to, $cc, $this->container->getParameter('site_email_address'), $subject, $body);


                    $details = $_POST['fname'] . " " . $_POST['lname'] . " registered " . $_POST['clubname'] . " and paid $169 with free $10 initial credit.";
                    $this->setActivity($admin_id, 'site-admin', $details);

                    $session->set('club_admin_id', $admin_id); 
                    $session->set('email', $_POST['email']); 
                    $session->set('fname', $_POST['fname']); 
                    $session->set('lname', $_POST['lname']);
                    $session->set('club_id', $model->getClubId());
                    $session->set('club_name', $_POST['clubname']);
                    $session->set('user_type', 'club-user');
                    $session->set('user_role', 'site-admin');

                    $session->set('office-address', $_POST['address']);
                    $session->set('office-postcode', $_POST['postcode']);
                    $session->set('office-state', $_POST['state']);
                    $session->set('office-city', $_POST['city']);

                    $session->set('available_credit', 10);

                    //$session->set('my_clubs', $clubs);

                    // last login
                    $em = $this->getDoctrine()->getManager();
                    $em->getConnection()->beginTransaction();
                    $model2 = $em->getRepository('AcmeHeadOfficeBundle:ClubAdmin')->findOneBy(array('club_admin_id'=>$admin_id));
                    $model2->setLastLoginDatetime($datetime->format('Y-m-d H:i:s'));
                    $em->persist($model2);
                    $em->flush();
                    $em->getConnection()->commit(); 

                   
                }
            }
        }
        
        return $this->render('AcmeClubBundle:ClubRegistration:registration_success.html.twig',
                array('club_name'=> $_POST['clubname']));
    }
    
    
    
    public function clubsMonthlyBillingAction(){
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT ca.`club_admin_id`, ca.fname, ca.lname, ca.email AS club_admin_email ,c.club_id, c.`club_name`, c.card_email, c.`address`, c.`city`, c.`postcode`, c.`state`,
            c.`lam`, c.`country`, c.`status`, c.`registered_date`, c.`payment_option`, c.`pin_payment_customer_token`, c.card_number,
            c.`card_expiry_month`, c.`card_expiry_year`, c.`card_cvc`, c.card_name, c.`card_address_line1` AS bill_address, 
            c.`card_address_city` AS bill_city, c.`card_address_postcode` AS bill_postcode, c.`card_address_state`, c.card_address_country, c.`monthly_fee_status`
            FROM tbl_club c 
            LEFT JOIN tbl_club_admin ca ON ca.`club_id` = c.`club_id` AND ca.`role` = 'site-admin'
            WHERE c.payment_option = 'monthly'
            GROUP BY c.`club_id`");
        $statement->execute();
        $data = $statement->fetchAll();
        
        $auth['u'] = $this->container->getParameter('pin_payment_api_private_key'); //Private Key from PIN
        $auth['p'] = $this->container->getParameter('pin_payment_api_password'); //API calls use empty password
                     
        $amount = 200;
        
        for($i=0, $cnt=count($data); $i<$cnt; $i++ ){
            // Create a curl handle
            $ch = curl_init( $this->container->getParameter('pin_payment_api_customer_url') . '/' . $data[$i]['pin_payment_customer_token'] .'/charges');
//            $ch = curl_init( $this->container->getParameter('pin_payment_api_customer_url') . '/' . $data[$i]['pin_payment_customer_token']);

            curl_setopt($ch, CURLOPT_USERPWD, $auth['u'] . ":" . $auth['p']); //authenticate
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //make sure it returns a response
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // allow https verification if true
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // allow https verification if true
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json')); //response comes back as json

            // Execute
            $charges = curl_exec($ch);
            // Check if any error occurred
            if(!curl_errno($ch)){$info = curl_getinfo($ch);}
            // Close handle
            curl_close($ch);
            //Decode JSON
            $charges = json_decode($charges, true);
            
            $do_payment_error = 0;
            if(!isset($charges['error'])){
                
                $do_payment_charge = 0;
                
                if(count($charges['response']) > 0){
                    $chargedate = new \DateTime($charges['response'][0]['created_at']);
                    $chargedate_interval = new \DateInterval('P1M'); // server vs australia time 11hrs
                    $chargedate->add($chargedate_interval);
                    $chargedate_interval = new \DateInterval('PT11H'); // server vs australia time 11hrs
                    $chargedate->add($chargedate_interval);
                    
                    $nowdate = new \DateTime(date("Y-m-d H:i:s"));
//                    $nowdate_interval = new \DateInterval('P1D');
//                    $nowdate->sub($nowdate_interval);
                    
                    if(($chargedate->format("Y-m-d") == $nowdate->format("Y-m-d")) || $data[$i]['monthly_fee_status'] == 'pending' ){
                        $do_payment_charge = 1;
                    }else{
                        $do_payment_charge = 0;
                    }
                }else{
                    $do_payment_charge = 1;
                }
                
                if($do_payment_charge == 1){
                    echo $data[$i]['club_name'];
                    echo '<br>';
                    //Fields to Post
                    $post = array();
                    $post['customer_token'] = $data[$i]['pin_payment_customer_token'];
                    $post['amount'] = ($amount * 100);
                    $post['currency'] = "AUD";
                    $post['description'] = urlencode("Payment for HLP Club unlimited tests");
                    if(isset($charges['response'][0]['email'])){
                        $post['email'] = $charges['response'][0]['email'];
                    }else{
                        if(isset($data[$i]['card_email']) && trim($data[$i]['card_email']) != ''){
                            $post['email'] = $data[$i]['card_email'];
                        }else{
                            $post['email'] = $data[$i]['club_admin_email'];
                        }
                    }
                    $post['ip_address'] = $_SERVER['REMOTE_ADDR'];

                    // Create a curl handle
                    $ch = curl_init( $this->container->getParameter('pin_payment_api_url') );

                    curl_setopt($ch, CURLOPT_USERPWD, $auth['u'] . ":" . $auth['p']); //authenticate
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //make sure it returns a response
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // allow https verification if true
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // allow https verification if true
                    curl_setopt($ch, CURLOPT_POST, 1); //tell it we are posting
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $post); //tell it what to post
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json')); //response comes back as json

                    // Execute
                    $payment = curl_exec($ch);

                    // Check if any error occurred
                    if(!curl_errno($ch)){$info = curl_getinfo($ch);}

                    // Close handle
                    curl_close($ch);

                    //Decode JSON
                    $payment = json_decode($payment, true);

                    if(!isset($payment['error'])){
                        $em = $this->getDoctrine()->getManager();
                        $em->getConnection()->beginTransaction();
                        
                        $club_update = $em->getRepository('AcmeHeadOfficeBundle:Club')->findOneBy(array('club_id'=>$data[$i]['club_id']));
                        $club_update->setMonthlyFeeStatus("paid");
                        $em->persist($club_update);
                        $em->flush();

                        $payment = new Payment();
                        $payment->setAmount(200);
                        $payment->setPaymentDatetime($datetime->format("Y-m-d H:i:s"));
                        $payment->setPrice(200);
                        $payment->setQty(1);
                        $payment->setStatus('completed');
                        $payment->setUserId($data[$i]['club_admin_id']);
                        $payment->setUserType('site-admin');
                        $em->persist($payment);
                        $em->flush();

                        $em->getConnection()->commit();
                        
                        $cc = $this->container->getParameter('site_email_address');
                        
                        $invoice_body =  $this->renderView('AcmeClubBundle:ClubRegistration:invoice_template.html.twig',
                                    array(
                                        'invoicedate'=> $datetime->format("Y-m-d H:i:s"),
                                        'invoicenumber'=> (isset( $payment )) ? $payment->getPaymentId() : 1,
                                        'post'=> $data[$i]
                                        )
                                    );
                        
                        $this->sendEmail($post['email'], $cc, $this->container->getParameter('site_email_address'), 'HLP Club Monthly Fee Tax Invoice', $invoice_body);
                    }else{
                        $do_payment_error = 1;
                    }
                }
                
            }else{
                $do_payment_error = 1;
            }
            
            if($do_payment_error == 1){
                $em = $this->getDoctrine()->getManager();
                $em->getConnection()->beginTransaction();

                $club_update = $em->getRepository('AcmeHeadOfficeBundle:Club')->findOneBy(array('club_id'=>$data[$i]['club_id']));
                if(count($club_update) > 0){
                    if($club_update->getMonthlyFeeStatus() != 'pending'){
                        $club_update->setMonthlyFeeStatus("pending");
                        $em->persist($club_update);
                        $em->flush();

                        $charge_error = array();
                        if(isset($charges['messages'])){
                            $charge_error = $charges['messages'];
                        }else{
                            if(isset($charges['error_description'])){
                                $charge_error = array('message'=> $charges['error_description']);
                            }
                        }
                        $charge_errors = array(
                            'type'=> 'monthly_billing_error',
                            'error' => $charge_error
                        );
                        
                        $this->setNotification($data[$i]['club_id'], $data[$i]['club_admin_id'], "payment-card-details", "Your club's unlimited tests subscription had stopped. Payment card details need updating in the Admin section.");
                        
                        $this->setActivity($data[$i]['club_admin_id'], 'site-admin', json_encode($charge_errors));
                    }
                }
                $em->getConnection()->commit();
            }
        }
        
        return new Response("success");
    }
}
