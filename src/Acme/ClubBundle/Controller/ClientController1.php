<?php

namespace Acme\ClubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

use Acme\HeadOfficeBundle\Entity\ClubClient;

class ClientController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function clientAction()
    {
        
        return $this->redirect($this->generateUrl('acme_club_dashboard'));
        
        $session = $this->getRequest()->getSession();
        
        if($session->get('club_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        return $this->render('AcmeClubBundle:Client:client.html.twig',
                array('clients'=> $this->getClubClients(NULL, $session->get('club_id')))
                );
    }
    
    public function ajaxListClientsAction(){
        $session = $this->getRequest()->getSession();
        
        if($session->get('club_admin_id') == ''){
            //return $this->redirect($this->generateUrl('acme_club_login'));
            return new Response("session expired");
        }
        
        $_POST['limit_start_at'] = intval($_POST['limit_start_at']);
        $keyword_search = (isset($_POST['keyword'])) ? $_POST['keyword'] : '';
        $sort_by = (isset($_POST['sort'])) ? $_POST['sort'] : '';
        
        return new Response($this->renderView(
            'AcmeClubBundle:Client:client_snapshots.html.twig',
                array(
                    'clients'=>$this->getClubClientsWithRecentTestResult(
                        'active', 
                        $session->get('club_id'),
                        $_POST['limit_start_at'],
                        $this->container->getParameter('ajax_load_max_number'),
                        $sort_by,
                        $keyword_search
                        )
                    )
            ));
    }
    
    
    public function viewClientAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        
        if($session->get('club_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        if(!isset($_GET['id'])){
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        $_GET['id'] = intval($_GET['id']);
        
        $client_details = $this->getClubClientById($_GET['id']);
        $prog_client_details = $this->get12weekProgramClientByEmail($client_details['email']);
        return $this->render('AcmeClubBundle:Client:view_client.html.twig',
                array(
                    'client_details' => $client_details,
                    'prog_client_details' => $prog_client_details,
                    //'client_details' => $this->getClientFirstOrRecentTestDetails($_GET['id'], 'recent'),
                    'client_test_history' => $this->getClientTestHistory($_GET['id']),
                    'test_data' => $this->getClientTestHistory($_GET['id'], NULL, 'finalised', 'ASC'),
                    'weekdayitems'=> $this->getProgramWeeksWithDayItemsByEmail( $client_details['email'] , $prog_client_details['working_week'] , 1, 1, 1),
                    'week_items'=> $this->getProgramWeekDays($client_details['email'], $prog_client_details['working_week'], 1, 1, 1)
//                    'week_total_actions'=> $this->getProgramClientDayItems( $client_details['email'], $prog_client_details['working_week'], NULL, NULL, 'quote'),
//                    'week_complete_actions'=> $this->getProgramClientDayItems( $client_details['email'], $prog_client_details['working_week'] , NULL, 1),
//                    'overall_actions'=> $this->getProgramClientDayItems( $client_details['email'], NULL, NULL, NULL, 'quote'),
//                    'overall_complete_actions'=> $this->getProgramClientDayItems( $client_details['email'], NULL , NULL, 1),
                    
                    
            ));
    }
    
    public function addEditClientAction($slug)
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('club_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        
        
        
        if(isset($_POST['email'])){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            
            if(strtolower(trim($slug)) == 'new'){
                $model = new ClubClient();
            }else{
                $model = $em->getRepository('AcmeHeadOfficeBundle:ClubClient')->findOneBy(array('club_client_id'=>$_POST["club_client_id"]));
            }
            $model->setClubId($session->get('club_id'));
            $model->setClubAdminId($_POST['club_admin_id']);
            $_POST['picture'] = str_replace('../../uploads/', '', $_POST['picture']);
            $_POST['picture'] = str_replace('../uploads/', '', $_POST['picture']);
            $model->setPicture($_POST['picture']);
            $model->setOptClientId($_POST['opt_client_id']);
            $model->setFname($_POST['fname']);
            $model->setLname($_POST['lname']);
            $model->setEmail($_POST['email']);
            //$model->setPhone($_POST['phone']);
            
            $model->setBirthdate( $mod->changeFormatToOriginal($_POST['birthdate']) );
            $model->setGender($_POST['gender']);
            $model->setHeight($_POST['height']);
            //$model->setWeight($_POST['weight']);
            //$model->setStatus($_POST['status']);
            $model->setStatus('active');
            $em->persist($model);
            $em->flush();
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            if($error_count == 0){
                if($mod->isEmailValid($_POST['email']) == false || $mod->getAgeByBirthDate( $mod->changeFormatToOriginal($_POST['birthdate']) ) < 16 ){
                    $errors = array();
                }
                
                if(!$mod->isEmailValid($_POST['email'])){
                    $errors[] = array('message'=>$_POST['email'].' is not a valid email.');
                    $error_count += 1;
                }
                
                if( $mod->getAgeByBirthDate( $mod->changeFormatToOriginal($_POST['birthdate']) ) < 16 ){
                    $errors[] = array('message'=>'The client must be 16 years old and above to join the Program.');
                    $error_count += 1;
                }
                // SET ACTIVITY
                if(strtolower(trim($slug)) == 'new'){
                    $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " added " . $_POST['fname'] . " " . $_POST['lname'] . " as new client.";
                }else{
                    $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " updated the details of " . $_POST['fname'] . " " . $_POST['lname'] . ", a client.";
                }
                $this->setActivity($session->get('club_admin_id'), 'club-admin', $details);
            }
            
            if($error_count == 0){
                
                $em->getConnection()->commit(); 
                
                if(strtolower(trim($slug)) == 'new'){
                    
                    if( isset($_POST['addTo12WeekProgram']) ){
                        // get program client here
                        $program_client = $this->get12weekProgramClientByEmail( $_POST['email'] );
                        if( count($program_client) <= 0 ){
                            if($_SERVER['SERVER_NAME'] == 'localhost'){
                                $program_host="localhost";
                                $program_uname="root";
                                $program_pass="";
                                $program_database = "healthy_life_project_ohwp";

                            }else{
                                $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
                                $program_uname="hlproot";
                                $program_pass="_jWBqa)67?BJq(j-";
                                $program_database = "hlp12weekProgram";
                            }
                            $program_connection=mysqli_connect($program_host,$program_uname,$program_pass, $program_database) or die("Database Connection Failed");

                            $pincode = rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);

                            mysqli_query($program_connection,
                            "INSERT INTO tbl_client 
                                (fname,
                                lname,
                                email,
                                birthdate,
                                picture,
                                gender,
                                height,
                                status,
                                registered_datetime,
                                current_week,
                                working_week,
                                s_opening_message_done,
                                s_lite_test_complete,
                                workout_base,
                                show_reg_questions,
                                token,
                                use_main_hlp_picture)

                                VALUES

                                ('". $model->getFname() ."',
                                '". $model->getLname() ."',
                                '". $model->getEmail() ."',
                                '". $model->getBirthdate() ."',
                                '". $model->getPicture() ."',
                                '". $model->getGender() ."',
                                '". $model->getHeight() ."',
                                'active',
                                '". $datetime->format("Y-m-d H:i:s") ."',
                                1,
                                1,
                                0,
                                0,
                                'gym',
                                0,
                                '". $pincode ."',
                                1)
                            ");

                            $new_program_client = $this->get12weekProgramClientByEmail( $model->getEmail() );

                            $default_items = $this->getDefaultDayItems();

                            for($i=0; $i<count($default_items); $i++){

                                mysqli_query($program_connection,
                                    "INSERT INTO tbl_program_client_items 
                                        (client_id,
                                        day_id,
                                        item_id,
                                        sorting,
                                        s_done)

                                    VALUES

                                        ('". $new_program_client['client_id'] ."',
                                        '". $default_items[$i]['day_id'] ."',
                                        '". $default_items[$i]['item_id'] ."',
                                        '". $default_items[$i]['sorting'] ."',
                                        0)
                                    ");
                            }



                            $from = $this->container->getParameter('site_email_address');
                            $subject = 'Welcome to the 12 Week Healthy Life Program!';
                            $body = $this->renderView('AcmeClubBundle:Client:registration_email.html.twig',
                                            array(
                                                'name' => $model->getFname(),
                                                'club_name' => $session->get('club_name'),
                                                'token'=> $pincode,
                                                'email'=> $model->getEmail(),
                                            ));

                            $this->sendEmail( $model->getEmail() , '', $from, $subject, $body);

                        }
                    }
                    
                    
                    
//                    $this->get('session')->getFlashBag()->add(
//                        'success',
//                        $_POST['fname'] . ' ' . $_POST['lname'] . ' has been added successfully.'
//                    );
                    return $this->redirect($this->generateUrl('acme_club_client_view'). "?id=".$model->getClubClientId());
                }else{
                    //====================================
                    if($_SERVER['SERVER_NAME'] == 'localhost'){
                        $program_host="localhost";
                        $program_uname="root";
                        $program_pass="";
                        $program_database = "healthy_life_project_ohwp";

                    }else{
                        $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
                        $program_uname="hlproot";
                        $program_pass="_jWBqa)67?BJq(j-";
                        $program_database = "hlp12weekProgram";
                    }
                    $program_connection=new \mysqli($program_host,$program_uname,$program_pass, $program_database);
                    if(mysqli_connect_errno()){
                        echo mysqli_connect_error();
                    }
                    $result = $program_connection->query("SELECT email FROM tbl_client WHERE email='".$_POST['orig_email']."'");
            
                    if ($result) {
                        $program_connection->query("UPDATE tbl_client SET email ='".$_POST['email']."' WHERE email='".$_POST['orig_email']."'");
                    }
                   
                    //===================================
                    $this->get('session')->getFlashBag()->add(
                        'success',
                        $_POST['fname'] . ' ' . $_POST['lname'] . ' has been updated successfully.'
                    );
                    return $this->redirect($this->generateUrl('acme_club_client_add_edit', array('slug' => 'edit')) . "?id=".$model->getClubClientId());
                }
                
                
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
               
                return $this->render('AcmeClubBundle:Client:add_edit_client.html.twig',
                        array('errors'=>$errors,
                            'post'=>$_POST,
                            'club_admins' => $this->getClubAdminsByClubId($session->get('club_id'))
                        ));
            }
            
        }
        
        
        if(strtolower(trim($slug)) == 'new'){
            return $this->render('AcmeClubBundle:Client:add_edit_client.html.twig',
                    array(
                        'club_admins' => $this->getClubAdminsByClubId($session->get('club_id'))
                    ));
        }else{
            $_GET['id'] = intval($_GET['id']);
            return $this->render('AcmeClubBundle:Client:add_edit_client.html.twig',
                    array(
                        'post'=> $this->getClubClientById($_GET['id']),
                        'club_admins' => $this->getClubAdminsByClubId($session->get('club_id'))
                    ));
        }
    }
    
    public function deleteAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('club_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        if(isset($_POST['club_client_id'])){
            $em = $this->getDoctrine()->getEntityManager();

            $_POST['club_client_id'] = intval($_POST['club_client_id']);
            $model = $em->getRepository('AcmeHeadOfficeBundle:ClubClient')->findOneBy(array('club_client_id'=>$_POST["club_client_id"]));
            $em->remove($model);
            $em->flush();


//            $this->get('session')->getFlashBag()->add(
//                    'success',
//                    'Client has been deleted successfully.'
//                );
            
            // SET ACTIVITY
            $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " deleted " . $model->getFname() . " " . $model->getLname() . ", a client.";
            $this->setActivity($session->get('club_admin_id'), 'club-admin', $details);
            
            return $this->redirect($this->generateUrl('acme_club_client'));
        }
        
    }
    
    public function cropPhotoAction(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        
        $directory = $root_dir . "/uploads/client-pictures/";
		
        if($session->get('club_admin_id') == ''){
            //return $this->redirect($this->generateUrl('acme_club_login'));
			return new Response("session expired");
        }
		
        $mod->cropImage($directory."/".$_POST["image"], $directory, $_POST["width"], $_POST["height"], $_POST["x"], $_POST["y"]);
        
        
        return new Response($_POST["image"]);
    }

    
    
}
