<?php

namespace Acme\ClubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

use Acme\HeadOfficeBundle\Entity\ClubClient;

class ClientController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function clientAction()
    {
        
        return $this->redirect($this->generateUrl('acme_club_dashboard'));
        
        $session = $this->getRequest()->getSession();
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        return $this->render('AcmeClubBundle:Client:client.html.twig',
                array('clients'=> $this->getClubClients(NULL, $session->get('club_id')))
                );
    }
    
    public function ajaxListClientsAction(){
        $start = microtime(true);
        $session = $this->getRequest()->getSession();
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            //return $this->redirect($this->generateUrl('acme_club_login'));
            return new Response("session expired");
        }
        
        $_GET['limit_start_at'] = intval($_GET['limit_start_at']);
        $keyword_search = (isset($_GET['keyword'])) ? $_GET['keyword'] : '';
        $sort_by = (isset($_GET['sort'])) ? $_GET['sort'] : '';
        
        $club_admin_id = ($session->get('user_role') == 'coach') ? $session->get('club_admin_id') : NULL;
        $clients =$this->getClubClientsWith12weekProgramDetails(
                        'active', 
                        $session->get('club_id'),
                        $_GET['limit_start_at'],
                        $this->container->getParameter('ajax_load_max_number'),
                        $sort_by,
                        $keyword_search,
                        $club_admin_id
                        );
        $time_elapsed_secs = microtime(true) - $start;
        return new Response($this->renderView(
            'AcmeClubBundle:Client:client_snapshots.html.twig',
                array(
                        'clients' => $clients,
                        'time_elapsed_secs'=> $time_elapsed_secs
                    )
            ));
        
        
    }
    
    public function ajaxListClientsTestAction(){
        $start = microtime(true);
        $session = $this->getRequest()->getSession();
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return new Response("session expired");
        }
        
        $_GET['limit_start_at'] = rand(1,73);
        $keyword_search = (isset($_GET['keyword'])) ? $_GET['keyword'] : '';
        $sort_by = (isset($_GET['sort'])) ? $_GET['sort'] : '';
        
        $club_admin_id = ($session->get('user_role') == 'coach') ? $session->get('club_admin_id') : NULL;
        $body = $this->renderView(
            'AcmeClubBundle:Client:client_snapshots.html.twig',
                array(
                    'clients'=>$this->getClubClientsWith12weekProgramDetails(
                        'active', 
                        $session->get('club_id'),
                        $_GET['limit_start_at'],
                        $this->container->getParameter('ajax_load_max_number'),
                        $sort_by,
                        $keyword_search,
                        $club_admin_id
                        )
                    )
            );
        $time_elapsed_secs = microtime(true) - $start;
        return new Response($body . $time_elapsed_secs);
    }
    
    public function ajaxListClientsTest1Action(){
        $start = microtime(true);
        $time_elapsed_secs = $start - microtime(true) ;
        echo $time_elapsed_secs;
        exit();
    }
    
    
    public function viewClientAction()
    {
        $session = $this->getRequest()->getSession();
        
        if(isset($_GET['cid'])){
            $_GET['id']=$_GET['cid'];
        }
        if(!isset($_GET['id'])){
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        $mod = new Model\GlobalModel();
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        $_GET['id'] = intval($_GET['id']);
        $client_details = $this->getClubClientById($_GET['id']);
        
        if($session->get('ho_admin_id') != '' && isset($_GET['ho'])){
            
            $club = $this->getClubById($client_details['club_id']);
//            $session->set('email', $rows[0]['email']); 
//            $session->set('fname', $rows[0]['fname']); 
//            $session->set('lname', $rows[0]['lname']);
            $session->set('club_name', $club['club_name']);

            $session->set('club_id', $client_details['club_id']);
//            $session->set('ho_admin_id', $rows[0]['ho_admin_id']); 
//            $session->set('user_type', 'head-office-user');
            $session->set('user_role', 'admin');
            $session->set('show_first_login_msg', 0);

            $session->set('office-address', $club['address']);
            $session->set('office-postcode', $club['postcode']);
            $session->set('office-state', $club['state']);
            $session->set('office-city', $club['city']);


            $session->set('available_credit', $club['available_credit']);

            $session->set('my_clubs', array());
        }
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        $session->set('active_page', 'client' ); 
        
        
        
        if(count($client_details) <= 0){
            return $this->render('AcmeClubBundle:Client:view_error.html.twig');
        }
        
        if($client_details['club_id'] != $session->get('club_id') && !isset($_GET['cid'])){
            return $this->render('AcmeClubBundle:Client:view_error.html.twig');
        }
        
        if( $session->get('user_role') != 'site-admin' && $session->get('user_role') != 'admin' && !isset($_GET['cid']) ){
            if($client_details['club_admin_id'] != $session->get('club_admin_id') && !isset($_GET['cid'])){
                return $this->render('AcmeClubBundle:Client:view_error.html.twig');
            }
        }
        $prog_client_details = $this->get12weekProgramClientByEmail($client_details['email']);
        $client_test_history = $this->getClientTestHistory($_GET['id']);
        

        if(isset($_GET['note'])){
            
            $_GET['note'] = filter_var($_GET['note'], FILTER_SANITIZE_NUMBER_INT);
            $em = $this->getDoctrine()->getManager();
            $model = $em->getRepository('AcmeHeadOfficeBundle:Notifications')->findOneBy(
                        array(
                            'club_admin_id'=>$session->get('club_admin_id'),
                            'club_client_id' => $_GET['id'],
                            'note_id'=> $_GET['note']
                        )
                    );
            if(count($model) > 0){
                $model->setSRead(1);

                $em->persist($model);
                $em->flush();
            }
        }
        
        $dir = $root_dir . '/uploads/client-files/' . $mod->getDirNameFromEmail($client_details['email']);
        $site_url = $mod->siteURL();
        $site_url = ($site_url == 'http://localhost') ? $site_url . '/healthylife-new/web/uploads/client-files/' : $site_url . '/web/uploads/client-files/';
        $site_url = $site_url . $mod->getDirNameFromEmail($client_details['email']);
        if (!file_exists($dir)) {
            $client_files = array();
        }else{
            $client_files = $mod->getFilesInDirectory( $dir );
        }
        
        return $this->render('AcmeClubBundle:Client:view_client.html.twig',
                array(
                    'client_details' => $client_details,
                    'prog_client_details' => $prog_client_details,
                    'client_test_history' => $client_test_history,
                    'test_data'=> array_reverse($client_test_history),
                    'club_admin_details'=> ($session->get('club_admin_id') != '') ? $this->getClubAdminUserById($session->get('club_admin_id')) : array(),
                    'club_admin_tests'=> ($session->get('club_admin_id') != '') ? $this->getClientTestsCountByClubAdminId($session->get('club_admin_id')) : 0,
                    'display_week'=> (isset($_GET['week'])) ? $_GET['week'] : 0,
                    'client_files' => $client_files,
                    'site_url' => $site_url
            ));
    }
    
    
    
    public function addEditClientAction($slug)
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        
        $reset_all = (isset($_POST['resetWeek']))? 1 : 0;
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        
        $session->set('active_page', 'client' ); 
        
        
        
        $checkClient =false;
        $checkFrontClient = false;
        if(isset($_POST['email']) && isset($_POST["club_client_id"])){
            $checkClientExist = $this->getClubClientByEmailAndId($_POST['email'],$_POST["club_client_id"]);
            if (count($checkClientExist)> 0){
                $checkClient =true;
            }
            if($_POST['email'] != $_POST['orig_email']){
                $checkFrontClientExist = $this->getProgramClientByEmail($_POST['email']);
                if ($checkFrontClientExist> 0){
                    $checkFrontClient =true;
                }
            }
        }
        
        if(isset($_POST['email']) && $checkClient==false && $checkFrontClient==false){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            $s_need_password_reset = 0;
            
            if(strtolower(trim($slug)) == 'new'){
                $model = new ClubClient();
                $model->setClubId($session->get('club_id'));
            }else{
                $model = $em->getRepository('AcmeHeadOfficeBundle:ClubClient')->findOneBy(array('club_client_id'=>$_POST["club_client_id"]));
            }
            
            if( isset($_POST['club_admin_id']) ){
                $model->setClubAdminId($_POST['club_admin_id']);
            }else{
                if($session->get('club_admin_id') != ''){
                    $model->setClubAdminId($session->get('club_admin_id'));
                }
            }
            $_POST['picture'] = str_replace('../../uploads/', '', $_POST['picture']);
            $_POST['picture'] = str_replace('../uploads/', '', $_POST['picture']);
            $model->setPicture($_POST['picture']);
            $model->setOptClientId($_POST['opt_client_id']);
            if(isset($_POST['s_non_member_assessment'])){
                $model->setSNonMemberAssessment(1);
            }else{
                $model->setSNonMemberAssessment(0);
            }
            if(isset($_POST['s_aia_vitality_member'])){
                $model->setSAiaVitalityMember(1);
            }else{
                $model->setSAiaVitalityMember(0);
            }
            $model->setAiaVitalityMemberNumber($_POST['aia_vitality_member_number']);
            $model->setFname($_POST['fname']);
            $model->setLname($_POST['lname']);
            $model->setEmail($_POST['email']);
            //$model->setPhone($_POST['phone']);
            
            $model->setBirthdate( $mod->changeFormatToOriginal($_POST['birthdate']) );
            $model->setGender($_POST['gender']);
//            $model->setHeight($_POST['height']);
            //$model->setWeight($_POST['weight']);
            //$model->setStatus($_POST['status']);
            $model->setStatus('active');
            if($_POST['program_type'] == '4week' || $_POST['program_type'] == '8week' || $_POST['program_type'] == '12week'){
                $model->setProgramId(NULL);
                $model->setProgramType($_POST['program_type']);
            }else{
                $model->setProgramId($_POST['program_type']);
            }
             if($_POST['program_type']!=''){
                 if(isset($_POST['program_start_date']) && trim($_POST['program_start_date']) != ''){
                    $model->setProgramStartDate($mod->changeFormatToOriginal($_POST['program_start_date']));
                 }
            }
            $model->setClientCategory($_POST['client_category']);
            
            if(strtolower(trim($slug)) != 'new'){
                if(trim($_POST['password']) != ''){
                    if($_POST['password'] == $_POST['confirm_password']){
                        $s_need_password_reset = 1;
                        $model->setPassword( $mod->passGenerator( $_POST['password'] ));
                    }
                }
            }
            if(strtolower(trim($slug)) == 'new'){
                $model->setRegisteredDate($datetime->format("Y-m-d H:i:s"));
            }
            $em->persist($model);
            $em->flush();
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            if(strtolower(trim($slug)) == 'new'){
                if($error_count == 0){
                    
                    $sclientexist = $this->getClubClientByEmail($_POST['email']);
                    if(count($sclientexist) > 1){
                        $errors = array();
                        $errors[] = array('message'=>'Email is already used.');
                        $error_count += 1;
                    }

                }
            }
            
            if($error_count == 0){
                if($mod->isEmailValid($_POST['email']) == false 
                    || $mod->getAgeByBirthDate( $mod->changeFormatToOriginal($_POST['birthdate']) ) < 16 
                    || (!isset($_POST['s_non_member_assessment']) && trim($_POST['opt_client_id']) == '') 
                    || (isset($_POST['s_aia_vitality_member']) && trim($_POST['aia_vitality_member_number']) == '')
                        ){
                    $errors = array();
                }
                
                if(!$mod->isEmailValid($_POST['email'])){
                    $errors[] = array('message'=>$_POST['email'].' is not a valid email.');
                    $error_count += 1;
                }
                
                if( $mod->getAgeByBirthDate( $mod->changeFormatToOriginal($_POST['birthdate']) ) < 16 ){
                    $errors[] = array('message'=>'The client must be 16 years old and above to join the Program.');
                    $error_count += 1;
                }
                
                if(!isset($_POST['s_non_member_assessment']) && trim($_POST['opt_client_id']) == ''){
                    $errors[] = array('message'=>'FOB number must not be blank.');
                    $error_count += 1;
                }
                 
                if(isset($_POST['s_aia_vitality_member']) && trim($_POST['aia_vitality_member_number']) == ''){
                    $errors[] = array('message'=>'AIA vitality member number must not be blank.');
                    $error_count += 1;
                }
            }
            if($error_count == 0){
                if(strtolower(trim($slug)) != 'new'){
                    if(trim($_POST['password']) != ''){
                        if($_POST['password'] != $_POST['confirm_password']){
                            $errors = array();
                            $errors[] = array('message'=>'Password must be repeated exactly.');
                            $error_count += 1;
                        }
                    }
                    
                }
            }
            
//            $is_premium_test = $this->checkPremiumTestDone($_POST["club_client_id"]); 
            
            if($error_count == 0){
                
                // SET ACTIVITY
                if(strtolower(trim($slug)) == 'new'){
                    $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " added " . $_POST['fname'] . " " . $_POST['lname'] . " as new client.";
                }else{
                    $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " updated the details of " . $_POST['fname'] . " " . $_POST['lname'] . ", a client.";
                }
                
                if($session->get('club_admin_id') != ''){ 
                    $this->setActivity($session->get('club_admin_id'), 'club-admin', $details);
                }else{
                    $this->setActivity($session->get('ho_admin_id'), 'head-office-admin', $details);
                }
                
                $em->getConnection()->commit();
                
                $MailChimp = new \Acme\HeadOfficeBundle\Model\MailChimp($this->container->getParameter('MailChimp_API_ID'));
                if(strtolower(trim($slug)) == 'new'){
                    if (trim($_POST['program_type']) == '4week'){
                        $subs = $MailChimp->call('lists/subscribe', array(
                                                    'id'                => $this->container->getParameter('MailChimp_List_4week'),
                                                    'email'             => array('email'=>$_POST['email']),
                                                    'merge_vars'        => array('FNAME'=>$_POST['fname'], 'LNAME'=>$_POST['lname']),
                                                    'double_optin'      => false,
                                                    'update_existing'   => true,
                                                    'replace_interests' => false,
                                                    'send_welcome'      => false,
                                                    ));
                    }elseif (trim($_POST['program_type']) == '8week'){
                        $subs = $MailChimp->call('lists/subscribe', array(
                                                    'id'                => $this->container->getParameter('MailChimp_List_8week'),
                                                    'email'             => array('email'=>$_POST['email']),
                                                    'merge_vars'        => array('FNAME'=>$_POST['fname'], 'LNAME'=>$_POST['lname']),
                                                    'double_optin'      => false,
                                                    'update_existing'   => true,
                                                    'replace_interests' => false,
                                                    'send_welcome'      => false,
                                                    ));
                    }elseif (trim($_POST['program_type']) == '12week'){
                        $subs = $MailChimp->call('lists/subscribe', array(
                                                    'id'                => $this->container->getParameter('MailChimp_List_12week'),
                                                    'email'             => array('email'=>$_POST['email']),
                                                    'merge_vars'        => array('FNAME'=>$_POST['fname'], 'LNAME'=>$_POST['lname']),
                                                    'double_optin'      => false,
                                                    'update_existing'   => true,
                                                    'replace_interests' => false,
                                                    'send_welcome'      => false,
                                                    ));
                    }
                }
                    
                //if(strtolower(trim($slug)) == 'new'){
                    $pincode = rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
                    if($_SERVER['SERVER_NAME'] == 'localhost'){
                        $program_host="localhost";
                        $program_uname="root";
                        $program_pass="";
                        $program_database = "healthy_life_project_ohwp";

                    }else{
                        $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
                        $program_uname="hlproot";
                        $program_pass="_jWBqa)67?BJq(j-";
                        $program_database = "hlp12weekProgram";
                    }
                    
                    //====================================
                    if($_POST['orig_email']!=$_POST['email']){
                        
                        $program_connection=new \mysqli($program_host,$program_uname,$program_pass, $program_database);
                        if(mysqli_connect_errno()){
                            echo mysqli_connect_error();
                        }
                        $result = $program_connection->query("SELECT email FROM tbl_client WHERE email='".$_POST['orig_email']."'");

                        if ($result) {
                            $program_connection->query("UPDATE tbl_client SET email ='".$_POST['email']."' WHERE email='".$_POST['orig_email']."'");
                        }
                    }
                    //===================================
                    
                    $program_connection=mysqli_connect($program_host,$program_uname,$program_pass, $program_database) or die("Database Connection Failed");
                    $mysqli = new \mysqli($program_host,$program_uname,$program_pass, $program_database);
                            
                    if($_POST['program_type']!=''){
//                        if(trim($_POST['program_type']) == '4week'){
//                            $_POST['program_type'] = '4week';
//                        }elseif(trim($_POST['program_type']) == '8week'){
//                            $_POST['program_type'] = '8week';
//                        }else{
//                            $_POST['program_type'] = '12week';
//                        }
                        
                        // get program client here
                        $program_client = $this->get12weekProgramClientByEmail( $_POST['email'] );
//                        echo $_POST['program_type']; exit();
                        if($_POST['program_type'] == '4week' || $_POST['program_type'] == '8week' || $_POST['program_type'] == '12week'){
                            $insert_program_id = 'NULL';
                            $insert_program_type = $_POST['program_type'];
                        }else{
                            $insert_program_id = intval($_POST['program_type']);
                            $insert_program_type = $model->getProgramType();
                        }
                        if( count($program_client) <= 0 ){
                            
                            $lname = $mysqli->real_escape_string($model->getLname());
                            $fname = $mysqli->real_escape_string($model->getFname());
                            $email = $mysqli->real_escape_string($model->getEmail());
                            $opt_client_id = $mysqli->real_escape_string($model->getOptClientId());
                            
                            
                            mysqli_query($program_connection,
                            "INSERT INTO tbl_client 
                                (fname,
                                opt_client_id,
                                lname,
                                email,
                                birthdate,
                                picture,
                                gender,
                                height,
                                status,
                                registered_datetime,
                                current_week,
                                working_week,
                                s_opening_message_done,
                                s_lite_test_complete,
                                workout_base,
                                show_reg_questions,
                                token,
                                use_main_hlp_picture,
                                program_id,
                                program_type,
                                program_start_date,
                                password,
                                s_need_password_reset)

                                VALUES

                                ('". $fname ."',
                                '". $opt_client_id ."',
                                '". $lname ."',
                                '". $email ."',
                                '". $model->getBirthdate() ."',
                                '". $model->getPicture() ."',
                                '". $model->getGender() ."',
                                '". $model->getHeight() ."',
                                'active',
                                '". $datetime->format("Y-m-d H:i:s") ."',
                                1,
                                1,
                                1,
                                1,
                                'gym',
                                0,
                                '". $pincode ."',
                                1,
                                ". $insert_program_id .",
                                '".$insert_program_type."',
                                '". $model->getProgramStartDate() ."',
                                '". $model->getPassword() ."',
                                0)
                            ");
                            

                            $from = $this->container->getParameter('site_email_address');
                            if ($_POST['program_type']=='12week'){
                                $subject = 'Welcome to the 12 Week Healthy Life Program!';
                                $body = $this->renderView('AcmeClubBundle:Client:registration_email.html.twig',
                                                array(
                                                    'name' => $model->getFname(),
                                                    'club_name' => $session->get('club_name'),
                                                    'token'=> $pincode,
                                                    'email'=> $model->getEmail(),
                                                ));
                            }elseif($_POST['program_type']=='8week'){
                                $subject = 'Welcome to the Healthy Life Project!';
                                $body = $this->renderView('AcmeClubBundle:Client:registration_to_8week_email.html.twig',
                                                array(
                                                    'name' => $model->getFname(),
                                                    'club_name' => $session->get('club_name'),
                                                    'token'=> $pincode,
                                                    'email'=> $model->getEmail(),
                                                ));
                            }else{

                                $subject = 'Welcome to the Healthy Life Project!';
                                $body = $this->renderView('AcmeClubBundle:Client:registration_to_4week_email.html.twig',
                                                array(
                                                    'name' => $model->getFname(),
                                                    'club_name' => $session->get('club_name'),
                                                    'token'=> $pincode,
                                                    'email'=> $model->getEmail(),
                                                ));
                            }
                            $this->sendEmail( $model->getEmail() , '', $from, $subject, $body);
                            
                        }else{
                            // `lk: 060216
                            if($s_need_password_reset == 1){
                                $password_changed = ",password = '". $model->getPassword() ."'
                                        ,s_need_password_reset=1";
                            }else{
                                $password_changed = "";
                            }
                            $program_start_date_changed = '';
                            if(isset($_POST['program_start_date']) && trim($_POST['program_start_date']) != ''){ 
//                              $start = new \DateTime($program_client['program_start_date']);
                                $end = new \DateTime( $model->getProgramStartDate() );
                                $mondays = $mod->countDays('sunday', strtotime($end->format("Y-m-d")), strtotime($datetime->format("Y-m-d")));
                                $mondays = $mondays + 1;
                                $mondays = ($mondays > 0) ? $mondays : 1;
                                $mondays = ($mondays > 12) ? 12 : $mondays;
                                
                                if($reset_all==1){
                                    if($model->getProgramStartDate() != ''){
                                        $program_start_date_changed = ',current_week='. $mondays .',working_week='. $mondays .'';
                                    }else{
                                        $program_start_date_changed = ',current_week=1,working_week=1';
                                    }
                                }else{
                                    if($model->getProgramStartDate() != ''){

                                        $program_start_date_changed = ',current_week='. $mondays .',working_week='. $mondays .'';
                                    }
                                }
                                
                                $program_start_date = "program_start_date='".$mod->changeFormatToOriginal($_POST['program_start_date'])."', ";
                            }else{
                                $program_start_date = "";
                            } //print_r($insert_program_id); exit();
                            mysqli_query($program_connection,
                                "UPDATE tbl_client
                                SET ". $program_start_date ." 
                                    gender='". $model->getGender() ."',
                                    birthdate='". $model->getBirthdate() ."',
                                    opt_client_id='". $model->getOptClientId() ."',
                                    program_id=".$insert_program_id.",
                                    program_type='".$insert_program_type."' 
                                    ". $program_start_date_changed ."
                                    ". $password_changed ."
                                WHERE email='".$_POST['email']."' 
                                ");
                        }
                    
                            $new_program_client = $this->get12weekProgramClientByEmail( $_POST['email'] );
                            
                            
                            
                            if ($_POST['program_type']=='12week'){
                                if($reset_all==1){
                                    mysqli_query($program_connection,"DELETE FROM tbl_program_client_items WHERE client_id='".$new_program_client['client_id']."'");
                                }
                                
                                $check_12week_program_exist = $this->check12weekProgramExist($new_program_client['client_id']);
                                if($check_12week_program_exist['cnt']==0){
                                    $default_items = $this->getDefaultDayItems();

                                    for($i=0; $i<count($default_items); $i++){

                                        mysqli_query($program_connection,
                                            "INSERT INTO tbl_program_client_items 
                                                (client_id,
                                                day_id,
                                                item_id,
                                                sorting,
                                                s_done)

                                            VALUES

                                                ('". $new_program_client['client_id'] ."',
                                                '". $default_items[$i]['day_id'] ."',
                                                '". $default_items[$i]['item_id'] ."',
                                                '". $default_items[$i]['sorting'] ."',
                                                0)
                                            ");
                                    }
                                }
                            }elseif ($_POST['program_type']=='8week'){
                                if($reset_all==1){
                                    mysqli_query($program_connection,"DELETE FROM tbl_program_client_items_8week WHERE client_id='".$new_program_client['client_id']."'");

                                }
                                    
                                $check_8week_program_exist = $this->check8weekProgramExist($new_program_client['client_id']);
                                if($check_8week_program_exist['cnt']==0){
                                    
                                    $default_items = $this->getDefaultDayItems8Week();
                                    
                                    
                                    for($i=0; $i<count($default_items); $i++){
                                        mysqli_query($program_connection,
                                            "INSERT INTO tbl_program_client_items_8week 
                                                (client_id,
                                                day_id,
                                                item_id,
                                                sorting,
                                                s_done)

                                            VALUES

                                                ('". $new_program_client['client_id'] ."',
                                                '". $default_items[$i]['day_id'] ."',
                                                '". $default_items[$i]['item_id'] ."',
                                                '". $default_items[$i]['sorting'] ."',
                                                0)
                                            ");
                                    }
                                }else{
                                    
                                }
                            }elseif ($_POST['program_type']=='4week'){
                                if($reset_all==1){
                                    mysqli_query($program_connection,"DELETE FROM tbl_program_client_items_4week WHERE client_id='".$new_program_client['client_id']."'");

                                }
                                    
                                $check_4week_program_exist = $this->check4weekProgramExist($new_program_client['client_id']);
                                if($check_4week_program_exist['cnt']==0){
                                    
                                    $default_items = $this->getDefaultDayItems4Week();
                                    
                                    
                                    for($i=0; $i<count($default_items); $i++){
                                        mysqli_query($program_connection,
                                            "INSERT INTO tbl_program_client_items_4week 
                                                (client_id,
                                                day_id,
                                                item_id,
                                                sorting,
                                                s_done)

                                            VALUES

                                                ('". $new_program_client['client_id'] ."',
                                                '". $default_items[$i]['day_id'] ."',
                                                '". $default_items[$i]['item_id'] ."',
                                                '". $default_items[$i]['sorting'] ."',
                                                0)
                                            ");
                                    }
                                }else{
                                    
                                }
                            }
                            if(isset($new_program_client['program_type'])){
                                if($new_program_client['program_type'] == '4week'){
                                    $client_current_week = ($new_program_client['current_week'] > 4) ? 4 : $new_program_client['current_week'];
                                }elseif($new_program_client['program_type'] == '8week'){
                                    $client_current_week = ($new_program_client['current_week'] > 8) ? 8 : $new_program_client['current_week'];
                                }elseif($new_program_client['program_type'] == '12week' || $new_program_client['program_type'] == ''){
                                    $client_current_week = ($new_program_client['current_week'] > 12) ? 12 : $new_program_client['current_week'];
//                                    $client_current_week = $new_program_client['current_week'];
                                }
                                $this->curlUpdateClientProgress($_POST['email'], $new_program_client['current_week']);
                            }
                            
                            
                        
                    }else{
                        
                    }
                    
                    
                    
//                    $this->get('session')->getFlashBag()->add(
//                        'success',
//                        $_POST['fname'] . ' ' . $_POST['lname'] . ' has been added successfully.'
//                    );
                 //   return $this->redirect($this->generateUrl('acme_club_client_view'). "?id=".$model->getClubClientId());
                //}else{
                    
                    $this->get('session')->getFlashBag()->add(
                        'success',
                        $_POST['fname'] . ' ' . $_POST['lname'] . ' has been updated successfully.'
                    );
                    return $this->redirect($this->generateUrl('acme_club_client_add_edit', array('slug' => 'edit')) . "?id=".$model->getClubClientId());
                //}
                
                
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
               if($_POST['program_type'] == '4week' || $_POST['program_type'] == '8week' || $_POST['program_type'] == '12week'){
                    $insert_program_id = '';
                    $insert_program_type = $_POST['program_type'];
                }else{
                    $insert_program_id = intval($_POST['program_type']);
                    $insert_program_type = $model->getProgramType();
                }
                return $this->render('AcmeClubBundle:Client:add_edit_client.html.twig',
                        array('errors'=>$errors,
//                            'post'=>$_POST,
                            'post'=>array(
                                    'club_client_id'=> (isset($_POST['club_client_id'])) ? $_POST['club_client_id'] : '',
                                    'picture'=> (isset($_POST['picture'])) ? $_POST['picture'] : '',
                                    'opt_client_id'=> $_POST['opt_client_id'],
                                    's_non_member_assessment'=> (isset($_POST['s_non_member_assessment'])) ? 1 : 0,
                                    's_aia_vitality_member'=> (isset($_POST['s_aia_vitality_member'])) ? 1 : 0,
                                    'aia_vitality_member_number'=> $_POST['aia_vitality_member_number'],
                                    'fname'=> $_POST['fname'],
                                    'lname'=> $_POST['lname'],
                                    'birthdate'=> $_POST['birthdate'],
                                    'program_id'=>$insert_program_id,
                                    'program_type'=> $insert_program_type,
                                    'program_start_date'=> $mod->changeFormatToOriginal($_POST['program_start_date']),
                                    'email'=> $_POST['email'],
                                    'orig_email'=> $_POST['orig_email'],
                                    'gender'=> $_POST['gender'],
                                    'client_category'=> $_POST['client_category'],
                                    'club_admin_id'=> $_POST['club_admin_id'],
                                    'password'=> (isset($_POST['password'])) ? $_POST['password'] : '',
                                    'confirm_password'=> (isset($_POST['confirm_password'])) ? $_POST['confirm_password'] : '',
                                ),
                            'club_admins' => $this->getClubAdminsByClubId($session->get('club_id')),
                            'client_category'=>$this->getClientCategory($session->get('club_id')),
                            'paid_programs'=> $this->getPaidPrograms()
                        ));
            }
            
        }else{
            if(strtolower(trim($slug)) != 'new' && isset($_POST['email'])){
                $errors = array();
                $errors[] = array('message'=>'Email already used.');
                $this->get('session')->getFlashBag()->add(
                        'error',
                        $errors
                    );
            }
            
        }
        
        
        if(strtolower(trim($slug)) == 'new'){
            return $this->render('AcmeClubBundle:Client:add_edit_client.html.twig',
                    array(
                        'club_admins' => $this->getClubAdminsByClubId($session->get('club_id')),
                        'client_category'=>$this->getClientCategory($session->get('club_id')),
                        'paid_programs'=> $this->getPaidPrograms()
                    ));
        }else{
            $_GET['id'] = intval($_GET['id']);
            
            $post = $this->getClubClientById($_GET['id']);
            if($session->get('ho_admin_id')==''){
                if($post['club_id'] != $session->get('club_id')){
                    return $this->render('AcmeClubBundle:Client:view_error.html.twig');
                }
            }
            $program_client_details = $this->get12weekProgramClientByEmail($post['email']);
            $prospect = $this->getClientCategoryById($session->get('club_id'),$post['client_category']);
            $is_prospect = 0;
            if($prospect){
                $is_prospect=1;
            
            }
            
            return $this->render('AcmeClubBundle:Client:add_edit_client.html.twig',
                    array(
                        'post'=> $post,
                        'program_client_details' => $program_client_details,
                        'club_admins' => $this->getClubAdminsByClubId($session->get('club_id')),
                        'client_category'=>$this->getClientCategory($session->get('club_id')),
                        'is_prospect'=>$is_prospect,
                        'paid_programs'=> $this->getPaidPrograms()
                    ));
        }
    }
    
    public function curlUpdateClientProgress($email, $week){
        $mod = new Model\GlobalModel();
        //extract data from the post
        //set POST variables
        $site = $mod->siteURL();
        $url = ($site == 'http://localhost') ? 'http://localhost/healthylife-ohwp-new/web/app_dev.php/update-client-program-progress' : 'http://www.healthylifeproject.com.au/program/web/update-client-program-progress';
        $fields = array(
                'email' => urlencode($email),
                'week' => urlencode($week),
        );
        $fields_string = '';
        //url-ify the data for the POST
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string, '&');

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);

        //execute post
        $result = curl_exec($ch);
//        var_dump(curl_error($ch));
        //close connection
        curl_close($ch);
        
    }
    
    public function deleteAction()
    {
        $session = $this->getRequest()->getSession();
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        if(isset($_POST['club_client_id'])){
            $em = $this->getDoctrine()->getEntityManager();

            $_POST['club_client_id'] = intval($_POST['club_client_id']);
            $model = $em->getRepository('AcmeHeadOfficeBundle:ClubClient')->findOneBy(array('club_client_id'=>$_POST["club_client_id"]));
            $em->remove($model);
            $em->flush();


//            $this->get('session')->getFlashBag()->add(
//                    'success',
//                    'Client has been deleted successfully.'
//                );
            
            // SET ACTIVITY
            $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " deleted " . $model->getFname() . " " . $model->getLname() . ", a client.";
            
            if($session->get('club_admin_id') != ''){ 
                $this->setActivity($session->get('club_admin_id'), 'club-admin', $details);
            }else{
                $this->setActivity($session->get('ho_admin_id'), 'head-office-admin', $details);
            }


            return $this->redirect($this->generateUrl('acme_club_client'));
        }
        
    }
    
    public function cropPhotoAction(){
        $mod = new Model\GlobalModel();
        $session = $this->getRequest()->getSession();
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        
        $directory = $root_dir . "/uploads/client-pictures/";
		
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            //return $this->redirect($this->generateUrl('acme_club_login'));
			return new Response("session expired");
        }
		
        $mod->cropImage($directory."/".$_POST["image"], $directory, $_POST["width"], $_POST["height"], $_POST["x"], $_POST["y"]);
        
        $mod->createImageThumb($_POST["image"], 320, $directory, $directory);
        
        $path_parts = pathinfo($directory."/".$_POST["image"]);
        
        return new Response($path_parts['filename'] . '_thumb.' . $path_parts['extension']);
    }
    
    public function get12weekProgramItems1Action(){
        
        $session = $this->getRequest()->getSession();
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return new Response('session_expired');
        }
        
        $_POST['client_id'] = filter_var($_POST['client_id'], FILTER_SANITIZE_NUMBER_INT);
        $_POST['prog_client_id'] = filter_var($_POST['prog_client_id'], FILTER_SANITIZE_NUMBER_INT);
        $display_week = ($_POST['program_type'] == '8week' && $_POST['current_week'] > 8) ? 8 : $_POST['current_week'];
        
        $content = $this->renderView('AcmeClubBundle:Client:12weekprogram_template.html.twig',
                    array(
//                        'client_details' => $client_details,
                        'client_id' => $_POST['client_id'],
                        'program_type'=> $_POST['program_type'],
                        'current_week'=> $_POST['current_week'],
                        'working_week'=> $_POST['working_week'],
                        'workout_base'=> $_POST['workout_base'],
                        'program_type'=> $_POST['program_type'],
//                        'weekdayitems'=> $this->getProgramWeeksWithDayItemsByEmail( $_POST['client_email'] , $display_week , 1, 1, 1, $_POST['prog_client_id'], $_POST['program_type']),
                        'weekdayitems'=> $this->getProgramWeeksWithDayItemsByEmailAllIn( $_POST['client_email'] , $display_week , 1, 1, 1, $_POST['prog_client_id'], $_POST['program_type'])
                        )
                    );

        return new Response( $content );
        
    }
    
    public function getPaidProgramItemsAction(){
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return new Response('session_expired');
        }
        
        $s_lite_test_complete = filter_var($_POST['s_lite_test_complete'], FILTER_SANITIZE_NUMBER_INT);
        $_POST['client_id'] = filter_var($_POST['client_id'], FILTER_SANITIZE_NUMBER_INT);
        $_POST['prog_client_id'] = filter_var($_POST['prog_client_id'], FILTER_SANITIZE_NUMBER_INT);
        $s_single = (isset($_POST['s_single'])) ? $_POST['s_single'] : 0;
        $s_single = filter_var($s_single, FILTER_SANITIZE_NUMBER_INT);
        $program_id = (isset($_POST['program'])) ? $_POST['program'] : 0;
        $program_id = filter_var($program_id, FILTER_SANITIZE_NUMBER_INT);
        $display_week = filter_var($_POST['display_week'], FILTER_SANITIZE_NUMBER_INT);
        
        $week_items = $this->geProgramDetailsByProgramId( $program_id , $display_week, $_POST['prog_client_id'], 1, 1, 1);
        $total_display_week = $week_items['total_weeks'];
        
        $current_week = filter_var($_POST['current_week'], FILTER_SANITIZE_NUMBER_INT);
        $current_week = ($current_week <= $total_display_week) ? $current_week : $total_display_week;
        $display_week = ($display_week <= $total_display_week) ? $display_week : $total_display_week;
        
        
        $comments = $this->getProgramWeekDayComments($_POST['prog_client_id'], $program_id, $display_week);
        $program_tips = $this->getProgramWeekTips($program_id, $display_week);
        if( !isset($_POST['s_week_item']) ){
            $content = $this->renderView('AcmeClubBundle:Client:paid_program_template.html.twig',
                        array(
                            'site_url' => ($mod->siteURL() == 'http://localhost') ? 'http://localhost/healthylife-ohwp-new/web/app_dev.php' : 'http://healthylifeproject.com.au/program/web',
                            'client_id' => $_POST['client_id'],
                            'prog_client_id' => $_POST['prog_client_id'],
                            'display_week'=> $display_week,
                            'current_week'=> $current_week,
                            'workout_base'=> $_POST['workout_base'],
                            'workout_level'=> $_POST['workout_level'],
                            's_lite_test_complete'=> $s_lite_test_complete,
                            'total_display_week'=> $total_display_week,
                            'post'=> $week_items,
                            'comments'=> $comments,
                            'program_tips'=>$program_tips
                            )
                        );
        }else{
            $content = $this->renderView('AcmeClubBundle:Client:paid_program_week_template.html.twig',
                        array(
                            'site_url' => ($mod->siteURL() == 'http://localhost') ? 'http://localhost/healthylife-ohwp-new/web/app_dev.php' : 'http://healthylifeproject.com.au/program/web',
                            'display_week'=> $display_week,
                            'current_week'=> $current_week,
                            'workout_base'=> $_POST['workout_base'],
                            'workout_level'=> $_POST['workout_level'],
                            's_lite_test_complete'=> $s_lite_test_complete,
                            'post'=> $week_items,
                            'comments'=> $comments,
                            'program_tips'=>$program_tips
                            )
                        );
        }
        return new Response( $content );
    }
    
    public function get12weekProgramItemsAction(){
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel;
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){ 
            return new Response('session_expired');
        }
        $total_display_week = 12;
        if(isset($_POST['program_type'])){
            if($_POST['program_type'] == '4week'){
                $program_type = '4week';
                $total_display_week = 4;
            }elseif($_POST['program_type'] == '8week'){
                $program_type = '8week';
                $total_display_week = 8;
            }else{
                $program_type = '12week';
                $total_display_week = 12;
            }
        }else{
            $program_type = '12week';
            $total_display_week = 12;
        }
//        $program_type = (isset($_POST['program_type']) && $_POST['program_type']== '8week') ? '8week' : '12week';
        $display_week = filter_var($_POST['display_week'], FILTER_SANITIZE_NUMBER_INT);
        $display_week = ($program_type == '4week' && $display_week > 4) ? 4 : $display_week;
        $display_week = ($program_type == '8week' && $display_week > 8) ? 8 : $display_week;
        $s_single = (isset($_POST['s_single'])) ? $_POST['s_single'] : 0;
        $s_single = filter_var($s_single, FILTER_SANITIZE_NUMBER_INT);
        $current_week = filter_var($_POST['current_week'], FILTER_SANITIZE_NUMBER_INT);
        $current_week = ($program_type == '4week' && $current_week > 4) ? 4 : $current_week;
        $current_week = ($program_type == '8week' && $current_week > 8) ? 8 : $current_week;
        $s_lite_test_complete = filter_var($_POST['s_lite_test_complete'], FILTER_SANITIZE_NUMBER_INT);
        $_POST['client_id'] = filter_var($_POST['client_id'], FILTER_SANITIZE_NUMBER_INT);
        $_POST['prog_client_id'] = filter_var($_POST['prog_client_id'], FILTER_SANITIZE_NUMBER_INT);
        
        if( !isset($_POST['s_week_item']) ){
            $content = $this->renderView('AcmeClubBundle:Client:12weekprogram_template.html.twig',
                        array(
                            'host' => $mod->siteURL(),
                            'display_week'=> $display_week,
                            'client_id' => $_POST['client_id'],
                            'prog_client_id' => $_POST['prog_client_id'],
//                            'current_week'=> ($program_type == '8week' && $current_week > 8) ? 8 : $current_week,
                            'current_week'=> $current_week,
                            'workout_base'=> $_POST['workout_base'],
                            's_lite_test_complete'=> $s_lite_test_complete,
//                            'total_display_week'=> ($program_type == '8week') ? 8 : 12,
                            'total_display_week'=> $total_display_week,
                            'weekdayitems'=> $this->getProgramWeeksWithDayItemsByEmailAllIn( $_POST['client_email'] , $display_week , 1, 1, 1, $_POST['prog_client_id'], $program_type),
                            )
                        );
        }else{
            $content = $this->renderView('AcmeClubBundle:Client:12weekprogram_week_template.html.twig',
                        array(
                            'prog_client_id' => $_POST['prog_client_id'],
                            'display_week'=> $display_week,
//                            'current_week'=> ($program_type == '8week' && $current_week > 8) ? 8 : $current_week,
                            'current_week'=> $current_week,
                            'workout_base'=> $_POST['workout_base'],
                            's_lite_test_complete'=> $s_lite_test_complete,
                            'weekdayitems'=> $this->getProgramWeeksWithDayItemsByEmailAllIn( $_POST['client_email'] , $display_week , 1, 1, 1, $_POST['prog_client_id'],$program_type)
                            )
                        );
        }
        return new Response( $content );
        
    }
    
    
    
    /**
     * Scenario:
     * 1. Get all clients with duplicate account (by email)
     * 2. Check in the program dbase and compare their program_type.
     *    The account that has the same program_type in program dbase is probably
     *    the correct account.
     * 3. If the other account did not match the program_type in the program dbase,
     *    check the account's tests, if it returns 0, then it should be deleted.
     * 4. If both accounts match the program_type with the account in the program dbase,
     *    check which one has no tests and delete.
     * 5. What if both has no tests?
     */
    public function removeDuplicatesAction(){
        $this->getClientsWithMoreThanOneAccount();
        
        $cust = $this->getDoctrine()->getEntityManager();
        $connection = $cust->getConnection();
        // 1. Get all clients with duplicate account (by email)
        $statement = $connection->prepare("SELECT email, COUNT(email) AS cnt FROM tbl_club_client GROUP BY email HAVING cnt = 2;");
        $statement->execute();
        $data =  $statement->fetchAll();
        
        for($i=0, $count = count($data); $i < $count; $i++){
            
            // 2. Check in the program dbase and compare their program_type.
            //    The account that has the same program_type in program dbase is probably
            //    the correct account.
            $program_client = $this->get12weekProgramClientByEmail($data[$i]['email'], array('email','program_type'));
            if( trim($program_client['email']) != ''){
                $program_type = (trim($program_client['program_type']) == '8week') ? '8week' : '12week';
                
                $statement = $connection->prepare("SELECT club_client_id,email, program_type FROM tbl_club_client WHERE email='". trim($program_client['email']) ."';");
                $statement->execute();
                $dup_client =  $statement->fetchAll();
                
                $count_no_test = 0;
                $acount_to_delete = 0;
                $count_correct_program_type_account = 0;
                $count_correct_program_type_account_no_test = 0;
                $count_correct_program_type_acount_to_delete = 0;
                for($e=0, $dup_client_cnt=count($dup_client); $e<$dup_client_cnt; $e++){
                    $dup_client_program_type = (trim($dup_client[$e]['program_type']) == '8week') ? '8week' : '12week';
                    if($dup_client_program_type != $program_type){
                        $statement = $connection->prepare("SELECT * FROM tbl_client_test WHERE club_client_id=".$dup_client[$e]['club_client_id'].";");
                        $statement->execute();
                        $dup_client_no_test =  $statement->fetchAll();
                        
                        if(count($dup_client_no_test) <= 0){
                            $count_no_test += 1;
                            $acount_to_delete = $dup_client[$e]['club_client_id'];
                        }
                    }else{
                        $count_correct_program_type_account += 1;
                        
                        $statement = $connection->prepare("SELECT * FROM tbl_client_test WHERE club_client_id=".$dup_client[$e]['club_client_id'].";");
                        $statement->execute();
                        $dup_client_no_test =  $statement->fetchAll();
                        
                        if(count($dup_client_no_test) <= 0){
                            $count_correct_program_type_account_no_test += 1;
                            $count_correct_program_type_acount_to_delete = $dup_client[$e]['club_client_id'];
                        }
                    }
                }
                
                if($count_correct_program_type_account == count($dup_client)){
                    if($count_correct_program_type_account_no_test > 0){
                        // delete account
//                      $statement = $connection->prepare("DELETE FROM tbl_club_client WHERE club_client_id=".$count_correct_program_type_acount_to_delete.";");
//                      $statement->execute();
                        echo $count_correct_program_type_acount_to_delete. ' - '. $data[$i]['email'] . ' correct account';
                        echo '<hr>';
                    }
                }else{
                    if($count_no_test > 0){
                        // delete account
//                      $statement = $connection->prepare("DELETE FROM tbl_club_client WHERE club_client_id=".$acount_to_delete.";");
//                      $statement->execute();
                        echo $acount_to_delete. ' - '. $data[$i]['email'] . ' in program';
                        echo '<hr>';
                    }
                }
            }else{
                // client that is not connected to program
                $statement = $connection->prepare("SELECT club_client_id,email, program_type FROM tbl_club_client WHERE email='". trim($data[$i]['email']) ."';");
                $statement->execute();
                $dup_client =  $statement->fetchAll();
                $count_no_test = 0;
                $acount_to_delete = 0;
                 for($e=0, $dup_client_cnt=count($dup_client); $e<$dup_client_cnt; $e++){
                    
                    $statement = $connection->prepare("SELECT * FROM tbl_client_test WHERE club_client_id=".$dup_client[$e]['club_client_id'].";");
                    $statement->execute();
                    $dup_client_no_test =  $statement->fetchAll();
                    
                    
                    if(count($dup_client_no_test) <= 0){
                        $count_no_test += 1;
                        $acount_to_delete = $dup_client[$e]['club_client_id'];
                    }
                }
                
                if($count_no_test > 0){
                    // delete account
//                  $statement = $connection->prepare("DELETE FROM tbl_club_client WHERE club_client_id=".$acount_to_delete.";");
//                  $statement->execute();
                    echo $acount_to_delete . ' - ' . $data[$i]['email'];
                    echo '<hr>';
                }
                
            }
            
        }
        exit();
    }
    
    public function getClientsWithMoreThanOneAccount(){
        $cust = $this->getDoctrine()->getEntityManager();
        $connection = $cust->getConnection();
        $statement = $connection->prepare("SELECT c.club_client_id, c.email , c.program_type, CONCAT(c.fname,' ', c.lname) AS `client`,club.`club_name` AS club, CONCAT(ca.fname, ' ', ca.lname) AS coach 
                FROM tbl_club_client c
                LEFT JOIN tbl_club_admin ca ON ca.`club_admin_id`=c.`club_admin_id`
                LEFT JOIN tbl_club club ON club.`club_id`=c.`club_id`
                INNER JOIN (SELECT c1.email FROM tbl_club_client c1
                GROUP BY c1.email 
                HAVING COUNT(c1.email) > 1) dup ON c.email = dup.email
                ORDER BY c.email ASC, club.`club_name` ASC");
        $statement->execute();
        $data =  $statement->fetchAll();
        $table = '<table>';
        $table .= '<thead>';
        $table .= '<tr>';
        $table .= '<th>System Client ID</th>';
        $table .= '<th>Email</th>';
        $table .= '<th>Name</th>';
        $table .= '<th>Club</th>';
        $table .= '<th>Coach</th>';
        $table .= '<th>Tests</th>';
//        $table .= '<th>Program Type (club setting)</th>';
        $table .= '<th>Program Type (12/8 week program setting)</th>';
        $table .= '</tr>';
        $table .= '</thead>';
        $table .= '<tbody>';
        $current_email = '';
        for($i=0, $cnt=count($data); $i<$cnt; $i++){
            $client_program_details = $this->get12weekProgramClientByEmail(trim($data[$i]['email']), array('program_type'));
            $program_type = '';
            $orig_program_type = (trim($data[$i]['program_type']) != '') ? $data[$i]['program_type'] : '<i>N/A</i>';
            if(count($client_program_details) > 0){
                if(trim($client_program_details['program_type']) != ''){
                    $program_type = $client_program_details['program_type'];
                }else{
                    $program_type = '12week';
                }
            }else{
                $program_type = '<i>N/A</i>';
            }
            
            if( $program_type != '<i>N/A</i>' && $orig_program_type == '<i>N/A</i>' ){
                $orig_program_type = $program_type;
            }
            $table .= '<tr>';
            $table .= '<td>'. $data[$i]['club_client_id'] .'</td>';
            $table .= '<td>'. $data[$i]['email'] .'</td>';
            $table .= '<td>'. $data[$i]['client'] .'</td>';
            if(trim($data[$i]['club']) != ''){
                $table .= '<td>'. $data[$i]['club'] .'</td>';
            }else{
                $table .= '<td><i>N/A</i></td>';
            }
            if(trim($data[$i]['coach']) != ''){
                $table .= '<td>'. $data[$i]['coach'] .'</td>';
            }else{
                $table .= '<td><i>N/A</i></td>';
            }
            
            $number_of_tests = $this->getClientTestHistory($data[$i]['club_client_id'], 'all', NULL, NULL, NULL, true);
            $table .= '<td>'. $number_of_tests .'</td>';
//            $table .= '<td>'. $orig_program_type .'</td>';
            if($current_email != $data[$i]['email']){
                $table .= '<td>'. $program_type .'</td>';
                $current_email = $data[$i]['email'];
            }else{
                $table .= '<td></td>';
            }
            
            $table .= '</tr>';
        }
        $table .= '</tbody>';
        $table .= '</table>';
        
        echo $table; exit();
    }

    
    
}
