<?php

namespace Acme\ClubBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

class NotificationsController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function newNotificationsAction()
    { 
        $session = $this->getRequest()->getSession();
        if($session->get('club_admin_id') == ''){
            return new Response('session_expired');
        }
        $mod = new Model\GlobalModel();
        $cust = $this->getDoctrine()->getEntityManager();
        $connection = $cust->getConnection();
        $statement = $connection->prepare("SELECT n.note_id, n.club_id, n.club_admin_id, 
                n.club_client_id, n.note_type, n.note_datetime, n.details, n.s_read
            FROM tbl_notifications n 
            WHERE n.club_admin_id = :club_admin_id AND n.`s_read` = 0 AND (n.s_read_onload_only IS NULL OR n.s_read_onload_only = 0)
            ORDER BY n.note_type ASC, n.`club_client_id` ASC
            LIMIT 20");
        $statement->bindValue('club_admin_id', $session->get('club_admin_id'));
        $statement->execute();
        $result =  $statement->fetchAll();
        $notice_cnt = 0;
        $message_cnt = 0;
        $current_ccid = 0;
        for($i=0, $cnt=count($result); $i<$cnt; $i++){
            
            if($result[$i]['club_client_id'] != $current_ccid){
                if($result[$i]['note_type'] == 'chat'){
                    $message_cnt += 1;
                }
                $current_ccid = $result[$i]['club_client_id'];
            }
            
            if($result[$i]['note_type'] != 'chat'){
                $notice_cnt += 1;
            }
            
        }
        
        $data = array(
            'notice_count'=>$notice_cnt,
            'chat_count'=>$message_cnt
        );
        
        return new Response(json_encode($data));
    }
    
    public function getNotificationsAction()
    { 
        $session = $this->getRequest()->getSession();
        if($session->get('club_admin_id') == ''){
            return new Response('session_expired');
        }
        $mod = new Model\GlobalModel();
        $cust = $this->getDoctrine()->getEntityManager();
        $connection = $cust->getConnection();
        $_GET['start_at'] = (isset($_GET['start_at'])) ? $_GET['start_at'] : 0;
        $_GET['start_at'] = filter_var($_GET['start_at'], FILTER_SANITIZE_NUMBER_INT);
        if($_GET['request'] == 'chat-messages'){
            $statement = $connection->prepare("SELECT n.note_id, n.club_id, n.club_admin_id, 
                    n.club_client_id, n.note_type, n.note_datetime, n.details, n.s_read,
                    cc.fname AS client_fname, cc.lname AS client_lname, cc.gender,
                    cc.picture, cc.email, n2.`note_id` as n2_note_id
                FROM tbl_notifications n 
                LEFT JOIN tbl_notifications n2 ON (n.club_client_id = n2.club_client_id AND n.note_type = n2.note_type AND n.note_id < n2.note_id)
                LEFT JOIN tbl_club_client cc ON cc.club_client_id = n.club_client_id
                WHERE n.club_admin_id = :club_admin_id AND n.`note_type`='chat' AND n2.`note_id` IS NULL
                ORDER BY n.note_datetime DESC
                LIMIT ". $_GET['start_at'] .",20;");
            
        }else{
            $statement = $connection->prepare("SELECT n.note_id, n.club_id, n.club_admin_id, 
                    n.club_client_id, n.note_type, n.note_datetime, n.details, n.s_read,
                    cc.fname AS client_fname, cc.lname AS client_lname, cc.gender,
                    cc.picture, cc.email
                FROM tbl_notifications n 
                LEFT JOIN tbl_club_client cc ON cc.club_client_id = n.club_client_id
                WHERE n.club_admin_id = :club_admin_id AND n.`note_type` != 'chat'
                ORDER BY n.note_datetime DESC
                LIMIT ". $_GET['start_at'] .",20;");
            
        }
        $statement->bindValue('club_admin_id', $session->get('club_admin_id'));
        $statement->execute();
        $result =  $statement->fetchAll();
        $data = array();
        for($i=0, $cnt=count($result); $i<$cnt; $i++){
            $data[$i] = $result[$i];
            $data[$i]['time_ago'] = $mod->_ago(strtotime($result[$i]['note_datetime']));
            
            if($_GET['request'] == 'notifications'){
                $em = $this->getDoctrine()->getManager();
                $model = $em->getRepository('AcmeHeadOfficeBundle:Notifications')->findOneBy(
                            array(
                                'club_admin_id'=>$session->get('club_admin_id'),
                                'note_id' => $result[$i]['note_id']
                            )
                        );
                $model->setSReadOnloadOnly(1);

                $em->persist($model);
                $em->flush();
            }else{
                $cust = $this->getDoctrine()->getEntityManager();
                $connection = $cust->getConnection();
                $statement = $connection->prepare("UPDATE tbl_notifications SET s_read_onload_only=1 WHERE club_client_id=:club_client_id AND club_admin_id = :club_admin_id  AND s_read=0 AND note_type='chat'");
                $statement->bindValue('club_client_id', $result[$i]['club_client_id']);
                $statement->bindValue('club_admin_id', $session->get('club_admin_id'));
                $statement->execute();
            }
        }
        
        return new Response($this->renderView(
            'AcmeClubBundle:Notifications:notice_template.html.twig',
                array(
                        'data' => $data,
                        'request' => ($_GET['request'] == 'chat-messages') ? 'chat-messages' : 'notifications'
                    )
            ));
    }
    
    public function readNotificationAction(){
        
        $session = $this->getRequest()->getSession();
        if($session->get('club_admin_id') == ''){
            return new Response('session_expired');
        }
        $_POST['note_id'] = filter_var($_POST['note_id'], FILTER_SANITIZE_NUMBER_INT);
        $_POST['club_client_id'] = filter_var($_POST['club_client_id'], FILTER_SANITIZE_NUMBER_INT);
        
        if($_POST['type'] == 'notifications'){
            $em = $this->getDoctrine()->getManager();
            $model = $em->getRepository('AcmeHeadOfficeBundle:Notifications')->findOneBy(
                        array(
                            'club_admin_id'=>$session->get('club_admin_id'),
                            'note_id' => $_POST['note_id'],
                            's_read' => 0,
                        )
                    );
            $model->setSRead(1);
        
            $em->persist($model);
            $em->flush();
        }else{
            $cust = $this->getDoctrine()->getEntityManager();
            $connection = $cust->getConnection();
            $statement = $connection->prepare("UPDATE tbl_notifications SET s_read=1 WHERE club_client_id=:club_client_id AND club_admin_id = :club_admin_id AND s_read=0 AND note_type='chat'");
            $statement->bindValue('club_client_id', $_POST['club_client_id']);
            $statement->bindValue('club_admin_id', $session->get('club_admin_id'));
            $statement->execute();
        }
        
        
        return new Response("success");
        
    }

}
