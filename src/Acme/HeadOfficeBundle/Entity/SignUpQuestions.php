<?php

namespace Acme\HeadOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SignUpQuestions
 *
 * @ORM\Table(name="tbl_signup_questions")
 * @ORM\Entity
 */
class SignUpQuestions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="sq_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $sq_id;


    /**
     * @var string
     *
     * @ORM\Column(name="question", type="string", length=220)
     */
    private $question;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50)
     */
    private $type;


    /**
     * Get sq_id
     *
     * @return integer 
     */
    public function getSqId()
    {
        return $this->sq_id;
    }

    /**
     * Set question
     *
     * @param string $question
     * @return SignUpQuestions
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    
        return $this;
    }

    /**
     * Get question
     *
     * @return string 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return SignUpQuestions
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }
}