<?php

namespace Acme\HeadOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notifications
 *
 * @ORM\Table(name="tbl_notifications")
 * @ORM\Entity
 */
class Notifications
{
    /**
     * @var integer
     *
     * @ORM\Column(name="note_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $note_id;

    /**
     * @var integer
     *
     * @ORM\Column(name="club_id", type="integer")
     */
    private $club_id;

    /**
     * @var integer
     *
     * @ORM\Column(name="club_admin_id", type="integer")
     */
    private $club_admin_id;

    /**
     * @var integer
     *
     * @ORM\Column(name="club_client_id", type="integer")
     */
    private $club_client_id;

    /**
     * @var string
     *
     * @ORM\Column(name="note_type", type="string", length=100)
     */
    private $note_type;

    /**
     * @var string
     *
     * @ORM\Column(name="note_datetime", type="string", length=50)
     */
    private $note_datetime;

    /**
     * @var string
     *
     * @ORM\Column(name="details", type="string")
     */
    private $details;

    /**
     * @var integer
     *
     * @ORM\Column(name="s_read", type="integer")
     */
    private $s_read;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="s_read_onload_only", type="integer")
     */
    private $s_read_onload_only;


    

    /**
     * Get note_id
     *
     * @return integer 
     */
    public function getNoteId()
    {
        return $this->note_id;
    }

    /**
     * Set club_id
     *
     * @param integer $clubId
     * @return Notifications
     */
    public function setClubId($clubId)
    {
        $this->club_id = $clubId;
    
        return $this;
    }

    /**
     * Get club_id
     *
     * @return integer 
     */
    public function getClubId()
    {
        return $this->club_id;
    }

    /**
     * Set club_admin_id
     *
     * @param integer $clubAdminId
     * @return Notifications
     */
    public function setClubAdminId($clubAdminId)
    {
        $this->club_admin_id = $clubAdminId;
    
        return $this;
    }

    /**
     * Get club_admin_id
     *
     * @return integer 
     */
    public function getClubAdminId()
    {
        return $this->club_admin_id;
    }

    /**
     * Set club_client_id
     *
     * @param integer $clubClientId
     * @return Notifications
     */
    public function setClubClientId($clubClientId)
    {
        $this->club_client_id = $clubClientId;
    
        return $this;
    }

    /**
     * Get club_client_id
     *
     * @return integer 
     */
    public function getClubClientId()
    {
        return $this->club_client_id;
    }

    /**
     * Set note_type
     *
     * @param string $noteType
     * @return Notifications
     */
    public function setNoteType($noteType)
    {
        $this->note_type = $noteType;
    
        return $this;
    }

    /**
     * Get note_type
     *
     * @return string 
     */
    public function getNoteType()
    {
        return $this->note_type;
    }

    /**
     * Set note_datetime
     *
     * @param string $noteDatetime
     * @return Notifications
     */
    public function setNoteDatetime($noteDatetime)
    {
        $this->note_datetime = $noteDatetime;
    
        return $this;
    }

    /**
     * Get note_datetime
     *
     * @return string 
     */
    public function getNoteDatetime()
    {
        return $this->note_datetime;
    }

    /**
     * Set details
     *
     * @param string $details
     * @return Notifications
     */
    public function setDetails($details)
    {
        $this->details = $details;
    
        return $this;
    }

    /**
     * Get details
     *
     * @return string 
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Set s_read
     *
     * @param integer $sRead
     * @return Notifications
     */
    public function setSRead($sRead)
    {
        $this->s_read = $sRead;
    
        return $this;
    }

    /**
     * Get s_read
     *
     * @return integer 
     */
    public function getSRead()
    {
        return $this->s_read;
    }

    /**
     * Set s_read_onload_only
     *
     * @param integer $sReadOnloadOnly
     * @return Notifications
     */
    public function setSReadOnloadOnly($sReadOnloadOnly)
    {
        $this->s_read_onload_only = $sReadOnloadOnly;
    
        return $this;
    }

    /**
     * Get s_read_onload_only
     *
     * @return integer 
     */
    public function getSReadOnloadOnly()
    {
        return $this->s_read_onload_only;
    }
}