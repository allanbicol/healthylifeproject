<?php

namespace Acme\HeadOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClientTest
 *
 * @ORM\Table(name="tbl_client_test")
 * @ORM\Entity
 */
class ClientTest
{
    /**
     * @var integer
     *
     * @ORM\Column(name="client_test_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $client_test_id;

    /**
     * @var integer
     *
     * @ORM\Column(name="club_admin_id", type="integer")
     */
    private $club_admin_id;

    /**
     * @var integer
     *
     * @ORM\Column(name="club_client_id", type="integer")
     */
    private $club_client_id;

    /**
     * @var integer
     *
     * @ORM\Column(name="client_12wk_test_id", type="integer")
     */
    private $client_12wk_test_id;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="event_id", type="integer")
     */
    private $event_id;

    /**
     * @var string
     *
     * @ORM\Column(name="test_type", type="string", length=10)
     */
    private $test_type;
    
    /**
     * @var string
     *
     * @ORM\Column(name="test_datetime", type="string", length=50)
     */
    private $test_datetime;
    
    /**
     * @var string
     *
     * @ORM\Column(name="test_datetime_finalised", type="string", length=50)
     */
    private $test_datetime_finalised;

    /**
     * @var integer
     *
     * @ORM\Column(name="age", type="integer")
     */
    private $age;
    
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_expectancy_male", type="float")
     */
    private $healthy_life_expectancy_male;
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_expectancy_female", type="float")
     */
    private $healthy_life_expectancy_female;
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_expectancy_total_male", type="float")
     */
    private $healthy_life_expectancy_total_male;
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_expectancy_total_female", type="float")
     */
    private $healthy_life_expectancy_total_female;
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_expectancy_dalys_male", type="float")
     */
    private $healthy_life_expectancy_dalys_male;
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_expectancy_dalys_female", type="float")
     */
    private $healthy_life_expectancy_dalys_female;
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_max_score_male", type="float")
     */
    private $healthy_life_max_score_male;
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_max_score_female", type="float")
     */
    private $healthy_life_max_score_female;
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_worst_score_male", type="float")
     */
    private $healthy_life_worst_score_male;
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_worst_score_female", type="float")
     */
    private $healthy_life_worst_score_female;
    

    /**
     * @var float
     *
     * @ORM\Column(name="bmi", type="float")
     */
    private $bmi;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="height", type="integer")
     */
    private $height;

    /**
     * @var integer
     *
     * @ORM\Column(name="weight", type="integer")
     */
    private $weight;

    /**
     * @var integer
     *
     * @ORM\Column(name="lifestyle_physical_activity_q1", type="integer")
     */
    private $lifestyle_physical_activity_q1;

    /**
     * @var integer
     *
     * @ORM\Column(name="lifestyle_physical_activity_q2", type="integer")
     */
    private $lifestyle_physical_activity_q2;
    
    /**
     * @var float
     *
     * @ORM\Column(name="lifestyle_physical_activity", type="float")
     */
    private $lifestyle_physical_activity;

    /**
     * @var float
     *
     * @ORM\Column(name="lifestyle_smoking", type="float")
     */
    private $lifestyle_smoking;

    /**
     * @var integer
     *
     * @ORM\Column(name="lifestyle_alcohol_q1", type="integer")
     */
    private $lifestyle_alcohol_q1;

    /**
     * @var integer
     *
     * @ORM\Column(name="lifestyle_alcohol_q2", type="integer")
     */
    private $lifestyle_alcohol_q2;

    /**
     * @var float
     *
     * @ORM\Column(name="lifestyle_alcohol", type="float")
     */
    private $lifestyle_alcohol;
    
    /**
     * @var float
     *
     * @ORM\Column(name="lifestyle_nutrition", type="float")
     */
    private $lifestyle_nutrition;

    /**
     * @var float
     *
     * @ORM\Column(name="lifestyle_mental_health", type="float")
     */
    private $lifestyle_mental_health;

    /**
     * @var float
     *
     * @ORM\Column(name="lifestyle_risk_profile", type="float")
     */
    private $lifestyle_risk_profile;

    /**
     * @var string
     *
     * @ORM\Column(name="physiological_vo2_test_type", type="string", length=100)
     */
    private $physiological_vo2_test_type;

    /**
     * @var integer
     *
     * @ORM\Column(name="physiological_vo2_test_value1", type="integer")
     */
    private $physiological_vo2_test_value1;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="physiological_vo2_test_value2", type="integer")
     */
    private $physiological_vo2_test_value2;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_vo2_score", type="float")
     */
    private $physiological_vo2_score;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_vo2", type="float")
     */
    private $physiological_vo2;


    /**
     * @var string
     *
     * @ORM\Column(name="physiological_balance_type", type="string", length=100)
     */
    private $physiological_balance_type;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="physiological_balance_value", type="integer")
     */
    private $physiological_balance_value;

    /**
     * @var float
     *
     * @ORM\Column(name="physiological_balance", type="float")
     */
    private $physiological_balance;

    /**
     * @var integer
     *
     * @ORM\Column(name="physiological_squat_value", type="integer")
     */
    private $physiological_squat_value;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_squat", type="float")
     */
    private $physiological_squat;

    /**
     * @var integer
     *
     * @ORM\Column(name="physiological_situp_value", type="integer")
     */
    private $physiological_situp_value;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_situp", type="float")
     */
    private $physiological_situp;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="physiological_squat_1rm_s_direct_entry", type="integer")
     */
    private $physiological_squat_1rm_s_direct_entry;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_squat_1rm_direct_entry", type="float")
     */
    private $physiological_squat_1rm_direct_entry;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_squat_1rm_weight", type="float")
     */
    private $physiological_squat_1rm_weight;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_squat_1rm_reps", type="float")
     */
    private $physiological_squat_1rm_reps;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_squat_1rm", type="float")
     */
    private $physiological_squat_1rm;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="physiological_bench_press_1rm_s_direct_entry", type="integer")
     */
    private $physiological_bench_press_1rm_s_direct_entry;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_bench_press_1rm_direct_entry", type="float")
     */
    private $physiological_bench_press_1rm_direct_entry;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_bench_press_1rm_weight", type="float")
     */
    private $physiological_bench_press_1rm_weight;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_bench_press_1rm_reps", type="float")
     */
    private $physiological_bench_press_1rm_reps;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_bench_press_1rm", type="float")
     */
    private $physiological_bench_press_1rm;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="physiological_deadlift_1rm_s_direct_entry", type="integer")
     */
    private $physiological_deadlift_1rm_s_direct_entry;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_deadlift_1rm_direct_entry", type="float")
     */
    private $physiological_deadlift_1rm_direct_entry;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_deadlift_1rm_weight", type="float")
     */
    private $physiological_deadlift_1rm_weight;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_deadlift_1rm_reps", type="float")
     */
    private $physiological_deadlift_1rm_reps;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_deadlift_1rm", type="float")
     */
    private $physiological_deadlift_1rm;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="physiological_overhead_press_s_direct_entry", type="integer")
     */
    private $physiological_overhead_press_s_direct_entry;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_overhead_press_direct_entry", type="float")
     */
    private $physiological_overhead_press_direct_entry;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_overhead_press_weight", type="float")
     */
    private $physiological_overhead_press_weight;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_overhead_press_reps", type="float")
     */
    private $physiological_overhead_press_reps;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_overhead_press", type="float")
     */
    private $physiological_overhead_press;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="physiological_squat_1min_min", type="integer")
     */
    private $physiological_squat_1min_min;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="physiological_squat_1min_sec", type="integer")
     */
    private $physiological_squat_1min_sec;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="physiological_push_ups_1min_min", type="integer")
     */
    private $physiological_push_ups_1min_min;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="physiological_push_ups_1min_sec", type="integer")
     */
    private $physiological_push_ups_1min_sec;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="physiological_sit_ups_1min_min", type="integer")
     */
    private $physiological_sit_ups_1min_min;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="physiological_sit_ups_1min_sec", type="integer")
     */
    private $physiological_sit_ups_1min_sec;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="physiological_row_500m_min", type="integer")
     */
    private $physiological_row_500m_min;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="physiological_row_500m_sec", type="integer")
     */
    private $physiological_row_500m_sec;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_standing_broad_jump", type="float")
     */
    private $physiological_standing_broad_jump;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_time_trial_option", type="float")
     */
    private $physiological_time_trial_option;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_time_trial_min", type="float")
     */
    private $physiological_time_trial_min;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_time_trial_sec", type="float")
     */
    private $physiological_time_trial_sec;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="biometric_resting_heart_rate", type="string", length=100)
     */
    private $biometric_resting_heart_rate;

    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_waist_value", type="integer")
     */
    private $biometric_waist_value;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_waist_value_lost", type="integer")
     */
    private $biometric_waist_value_lost;
    
    /**
     * @var float
     *
     * @ORM\Column(name="biometric_waist", type="float")
     */
    private $biometric_waist;

    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_chest", type="integer")
     */
    private $biometric_chest;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_chest_lost", type="integer")
     */
    private $biometric_chest_lost;

    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_abdomen", type="integer")
     */
    private $biometric_abdomen;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_abdomen_lost", type="integer")
     */
    private $biometric_abdomen_lost;

    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_hip", type="integer")
     */
    private $biometric_hip;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_hip_lost", type="integer")
     */
    private $biometric_hip_lost;

    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_thigh", type="integer")
     */
    private $biometric_thigh;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_thigh_right", type="integer")
     */
    private $biometric_thigh_right;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_thigh_left", type="integer")
     */
    private $biometric_thigh_left;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_thigh_lost", type="integer")
     */
    private $biometric_thigh_lost;

    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_arm", type="integer")
     */
    private $biometric_arm;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_arm_right", type="integer")
     */
    private $biometric_arm_right;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_arm_left", type="integer")
     */
    private $biometric_arm_left;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_arm_lost", type="integer")
     */
    private $biometric_arm_lost;
    
    /**
     * @var string
     *
     * @ORM\Column(name="biometric_body_fat_value", type="string", length=5)
     */
    private $biometric_body_fat_value;
    
    /**
     * @var float
     *
     * @ORM\Column(name="biometric_body_fat", type="float")
     */
    private $biometric_body_fat;

    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_blood_pressure_systolic", type="integer")
     */
    private $biometric_blood_pressure_systolic;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_blood_pressure_diastolic", type="integer")
     */
    private $biometric_blood_pressure_diastolic;
    
    /**
     * @var float
     *
     * @ORM\Column(name="biometric_blood_pressure", type="float")
     */
    private $biometric_blood_pressure;

    /**
     * @var string
     *
     * @ORM\Column(name="biometric_blood_oxygen_value", type="string", length=5)
     */
    private $biometric_blood_oxygen_value;
    
    /**
     * @var float
     *
     * @ORM\Column(name="biometric_blood_oxygen", type="float")
     */
    private $biometric_blood_oxygen;
    
    
    
    
    
    
    /**
     * @var float
     *
     * @ORM\Column(name="total_crf", type="float")
     */
    private $total_crf;
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_points", type="float")
     */
    private $healthy_life_points;
    
    /**
     * @var float
     *
     * @ORM\Column(name="estimated_healthy_life_expectancy", type="float")
     */
    private $estimated_healthy_life_expectancy;
    
    /**
     * @var float
     *
     * @ORM\Column(name="max_hle", type="float")
     */
    private $max_hle;
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_score", type="float")
     */
    private $healthy_life_score;
    
    
    /**
     * @var float
     *
     * @ORM\Column(name="estimated_years_of_healthy_life_left", type="float")
     */
    private $estimated_years_of_healthy_life_left;
    
    /**
     * @var float
     *
     * @ORM\Column(name="estimated_years_you_will_live_with_did", type="float")
     */
    private $estimated_years_you_will_live_with_did;
    
    /**
     * @var float
     *
     * @ORM\Column(name="estimated_years_you_will_live_with_did_as_opposed_to", type="float")
     */
    private $estimated_years_you_will_live_with_did_as_opposed_to;
    
    /**
     * @var float
     *
     * @ORM\Column(name="you_could_add_up_to", type="float")
     */
    private $you_could_add_up_to;
    
    /**
     * @var float
     *
     * @ORM\Column(name="estimated_current_healthy_life_age", type="float")
     */
    private $estimated_current_healthy_life_age;
    
    /**
     * @var float
     *
     * @ORM\Column(name="years_added", type="float")
     */
    private $years_added;
    
    /**
     * @var float
     *
     * @ORM\Column(name="cm_lost", type="float")
     */
    private $cm_lost;
    
    /**
     * @var float
     *
     * @ORM\Column(name="kg_lost", type="float")
     */
    private $kg_lost;
    
    
    /**
     * @var float
     *
     * @ORM\Column(name="bmi_improvement", type="float")
     */
    private $bmi_improvement;
    
    /**
     * @var float
     *
     * @ORM\Column(name="body_fat_reduction", type="float")
     */
    private $body_fat_reduction;
    
    /**
     * @var float
     *
     * @ORM\Column(name="vo2_improvement", type="float")
     */
    private $vo2_improvement;
    
    /**
     * @var float
     *
     * @ORM\Column(name="squat_test_improvement", type="float")
     */
    private $squat_test_improvement;
    
    /**
     * @var float
     *
     * @ORM\Column(name="standing_functional_reach_improvement", type="float")
     */
    private $standing_functional_reach_improvement;
    
    /**
     * @var float
     *
     * @ORM\Column(name="stork_test_improvement", type="float")
     */
    private $stork_test_improvement;
    
    /**
     * @var float
     *
     * @ORM\Column(name="abdominal_test_improvement", type="float")
     */
    private $abdominal_test_improvement;
    
    /**
     * @var float
     *
     * @ORM\Column(name="vigorous_pa_per_week", type="float")
     */
    private $vigorous_pa_per_week;
    
    /**
     * @var float
     *
     * @ORM\Column(name="strength_pa_per_week", type="float")
     */
    private $strength_pa_per_week;
    
    /**
     * @var float
     *
     * @ORM\Column(name="smoking_improvement", type="float")
     */
    private $smoking_improvement;
    
    /**
     * @var float
     *
     * @ORM\Column(name="standard_alcohol_per_week", type="float")
     */
    private $standard_alcohol_per_week;
    
    /**
     * @var float
     *
     * @ORM\Column(name="maximum_alcohol_one_sitting", type="float")
     */
    private $maximum_alcohol_one_sitting;
    
    /**
     * @var float
     *
     * @ORM\Column(name="nutrition_score_improvement", type="float")
     */
    private $nutrition_score_improvement;
    
    /**
     * @var float
     *
     * @ORM\Column(name="mental_health_improvement", type="float")
     */
    private $mental_health_improvement;
    
    /**
     * @var float
     *
     * @ORM\Column(name="risk_profile_improvement", type="float")
     */
    private $risk_profile_improvement;
    
    
    

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=10)
     */
    private $status;


    /**
     * @var float
     *
     * @ORM\Column(name="waist_hip_ratio", type="float")
     */
    private $waist_hip_ratio;
    
    /**
     * @var float
     *
     * @ORM\Column(name="waist_hip_ratio_score", type="float")
     */
    private $waist_hip_ratio_score;



    /**
     * Get client_test_id
     *
     * @return integer 
     */
    public function getClientTestId()
    {
        return $this->client_test_id;
    }

    /**
     * Set club_admin_id
     *
     * @param integer $clubAdminId
     * @return ClientTest
     */
    public function setClubAdminId($clubAdminId)
    {
        $this->club_admin_id = $clubAdminId;
    
        return $this;
    }

    /**
     * Get club_admin_id
     *
     * @return integer 
     */
    public function getClubAdminId()
    {
        return $this->club_admin_id;
    }

    /**
     * Set club_client_id
     *
     * @param integer $clubClientId
     * @return ClientTest
     */
    public function setClubClientId($clubClientId)
    {
        $this->club_client_id = $clubClientId;
    
        return $this;
    }

    /**
     * Get club_client_id
     *
     * @return integer 
     */
    public function getClubClientId()
    {
        return $this->club_client_id;
    }

    /**
     * Set client_12wk_test_id
     *
     * @param integer $client12wkTestId
     * @return ClientTest
     */
    public function setClient12wkTestId($client12wkTestId)
    {
        $this->client_12wk_test_id = $client12wkTestId;
    
        return $this;
    }

    /**
     * Get client_12wk_test_id
     *
     * @return integer 
     */
    public function getClient12wkTestId()
    {
        return $this->client_12wk_test_id;
    }

    /**
     * Set event_id
     *
     * @param integer $eventId
     * @return ClientTest
     */
    public function setEventId($eventId)
    {
        $this->event_id = $eventId;
    
        return $this;
    }

    /**
     * Get event_id
     *
     * @return integer 
     */
    public function getEventId()
    {
        return $this->event_id;
    }

    /**
     * Set test_datetime
     *
     * @param string $testDatetime
     * @return ClientTest
     */
    public function setTestDatetime($testDatetime)
    {
        $this->test_datetime = $testDatetime;
    
        return $this;
    }

    /**
     * Get test_datetime
     *
     * @return string 
     */
    public function getTestDatetime()
    {
        return $this->test_datetime;
    }

    /**
     * Set test_datetime_finalised
     *
     * @param string $testDatetimeFinalised
     * @return ClientTest
     */
    public function setTestDatetimeFinalised($testDatetimeFinalised)
    {
        $this->test_datetime_finalised = $testDatetimeFinalised;
    
        return $this;
    }

    /**
     * Get test_datetime_finalised
     *
     * @return string 
     */
    public function getTestDatetimeFinalised()
    {
        return $this->test_datetime_finalised;
    }

    /**
     * Set age
     *
     * @param integer $age
     * @return ClientTest
     */
    public function setAge($age)
    {
        $this->age = $age;
    
        return $this;
    }

    /**
     * Get age
     *
     * @return integer 
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set healthy_life_expectancy_male
     *
     * @param float $healthyLifeExpectancyMale
     * @return ClientTest
     */
    public function setHealthyLifeExpectancyMale($healthyLifeExpectancyMale)
    {
        $this->healthy_life_expectancy_male = $healthyLifeExpectancyMale;
    
        return $this;
    }

    /**
     * Get healthy_life_expectancy_male
     *
     * @return float 
     */
    public function getHealthyLifeExpectancyMale()
    {
        return $this->healthy_life_expectancy_male;
    }

    /**
     * Set healthy_life_expectancy_female
     *
     * @param float $healthyLifeExpectancyFemale
     * @return ClientTest
     */
    public function setHealthyLifeExpectancyFemale($healthyLifeExpectancyFemale)
    {
        $this->healthy_life_expectancy_female = $healthyLifeExpectancyFemale;
    
        return $this;
    }

    /**
     * Get healthy_life_expectancy_female
     *
     * @return float 
     */
    public function getHealthyLifeExpectancyFemale()
    {
        return $this->healthy_life_expectancy_female;
    }

    /**
     * Set healthy_life_expectancy_total_male
     *
     * @param float $healthyLifeExpectancyTotalMale
     * @return ClientTest
     */
    public function setHealthyLifeExpectancyTotalMale($healthyLifeExpectancyTotalMale)
    {
        $this->healthy_life_expectancy_total_male = $healthyLifeExpectancyTotalMale;
    
        return $this;
    }

    /**
     * Get healthy_life_expectancy_total_male
     *
     * @return float 
     */
    public function getHealthyLifeExpectancyTotalMale()
    {
        return $this->healthy_life_expectancy_total_male;
    }

    /**
     * Set healthy_life_expectancy_total_female
     *
     * @param float $healthyLifeExpectancyTotalFemale
     * @return ClientTest
     */
    public function setHealthyLifeExpectancyTotalFemale($healthyLifeExpectancyTotalFemale)
    {
        $this->healthy_life_expectancy_total_female = $healthyLifeExpectancyTotalFemale;
    
        return $this;
    }

    /**
     * Get healthy_life_expectancy_total_female
     *
     * @return float 
     */
    public function getHealthyLifeExpectancyTotalFemale()
    {
        return $this->healthy_life_expectancy_total_female;
    }

    /**
     * Set healthy_life_expectancy_dalys_male
     *
     * @param float $healthyLifeExpectancyDalysMale
     * @return ClientTest
     */
    public function setHealthyLifeExpectancyDalysMale($healthyLifeExpectancyDalysMale)
    {
        $this->healthy_life_expectancy_dalys_male = $healthyLifeExpectancyDalysMale;
    
        return $this;
    }

    /**
     * Get healthy_life_expectancy_dalys_male
     *
     * @return float 
     */
    public function getHealthyLifeExpectancyDalysMale()
    {
        return $this->healthy_life_expectancy_dalys_male;
    }

    /**
     * Set healthy_life_expectancy_dalys_female
     *
     * @param float $healthyLifeExpectancyDalysFemale
     * @return ClientTest
     */
    public function setHealthyLifeExpectancyDalysFemale($healthyLifeExpectancyDalysFemale)
    {
        $this->healthy_life_expectancy_dalys_female = $healthyLifeExpectancyDalysFemale;
    
        return $this;
    }

    /**
     * Get healthy_life_expectancy_dalys_female
     *
     * @return float 
     */
    public function getHealthyLifeExpectancyDalysFemale()
    {
        return $this->healthy_life_expectancy_dalys_female;
    }

    /**
     * Set healthy_life_max_score_male
     *
     * @param float $healthyLifeMaxScoreMale
     * @return ClientTest
     */
    public function setHealthyLifeMaxScoreMale($healthyLifeMaxScoreMale)
    {
        $this->healthy_life_max_score_male = $healthyLifeMaxScoreMale;
    
        return $this;
    }

    /**
     * Get healthy_life_max_score_male
     *
     * @return float 
     */
    public function getHealthyLifeMaxScoreMale()
    {
        return $this->healthy_life_max_score_male;
    }

    /**
     * Set healthy_life_max_score_female
     *
     * @param float $healthyLifeMaxScoreFemale
     * @return ClientTest
     */
    public function setHealthyLifeMaxScoreFemale($healthyLifeMaxScoreFemale)
    {
        $this->healthy_life_max_score_female = $healthyLifeMaxScoreFemale;
    
        return $this;
    }

    /**
     * Get healthy_life_max_score_female
     *
     * @return float 
     */
    public function getHealthyLifeMaxScoreFemale()
    {
        return $this->healthy_life_max_score_female;
    }

    /**
     * Set healthy_life_worst_score_male
     *
     * @param float $healthyLifeWorstScoreMale
     * @return ClientTest
     */
    public function setHealthyLifeWorstScoreMale($healthyLifeWorstScoreMale)
    {
        $this->healthy_life_worst_score_male = $healthyLifeWorstScoreMale;
    
        return $this;
    }

    /**
     * Get healthy_life_worst_score_male
     *
     * @return float 
     */
    public function getHealthyLifeWorstScoreMale()
    {
        return $this->healthy_life_worst_score_male;
    }

    /**
     * Set healthy_life_worst_score_female
     *
     * @param float $healthyLifeWorstScoreFemale
     * @return ClientTest
     */
    public function setHealthyLifeWorstScoreFemale($healthyLifeWorstScoreFemale)
    {
        $this->healthy_life_worst_score_female = $healthyLifeWorstScoreFemale;
    
        return $this;
    }

    /**
     * Get healthy_life_worst_score_female
     *
     * @return float 
     */
    public function getHealthyLifeWorstScoreFemale()
    {
        return $this->healthy_life_worst_score_female;
    }

    /**
     * Set bmi
     *
     * @param float $bmi
     * @return ClientTest
     */
    public function setBmi($bmi)
    {
        $this->bmi = $bmi;
    
        return $this;
    }

    /**
     * Get bmi
     *
     * @return float 
     */
    public function getBmi()
    {
        return $this->bmi;
    }

    /**
     * Set height
     *
     * @param integer $height
     * @return ClientTest
     */
    public function setHeight($height)
    {
        $this->height = $height;
    
        return $this;
    }

    /**
     * Get height
     *
     * @return integer 
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     * @return ClientTest
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    
        return $this;
    }

    /**
     * Get weight
     *
     * @return integer 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set lifestyle_physical_activity_q1
     *
     * @param integer $lifestylePhysicalActivityQ1
     * @return ClientTest
     */
    public function setLifestylePhysicalActivityQ1($lifestylePhysicalActivityQ1)
    {
        $this->lifestyle_physical_activity_q1 = $lifestylePhysicalActivityQ1;
    
        return $this;
    }

    /**
     * Get lifestyle_physical_activity_q1
     *
     * @return integer 
     */
    public function getLifestylePhysicalActivityQ1()
    {
        return $this->lifestyle_physical_activity_q1;
    }

    /**
     * Set lifestyle_physical_activity_q2
     *
     * @param integer $lifestylePhysicalActivityQ2
     * @return ClientTest
     */
    public function setLifestylePhysicalActivityQ2($lifestylePhysicalActivityQ2)
    {
        $this->lifestyle_physical_activity_q2 = $lifestylePhysicalActivityQ2;
    
        return $this;
    }

    /**
     * Get lifestyle_physical_activity_q2
     *
     * @return integer 
     */
    public function getLifestylePhysicalActivityQ2()
    {
        return $this->lifestyle_physical_activity_q2;
    }

    /**
     * Set lifestyle_physical_activity
     *
     * @param float $lifestylePhysicalActivity
     * @return ClientTest
     */
    public function setLifestylePhysicalActivity($lifestylePhysicalActivity)
    {
        $this->lifestyle_physical_activity = $lifestylePhysicalActivity;
    
        return $this;
    }

    /**
     * Get lifestyle_physical_activity
     *
     * @return float 
     */
    public function getLifestylePhysicalActivity()
    {
        return $this->lifestyle_physical_activity;
    }

    /**
     * Set lifestyle_smoking
     *
     * @param float $lifestyleSmoking
     * @return ClientTest
     */
    public function setLifestyleSmoking($lifestyleSmoking)
    {
        $this->lifestyle_smoking = $lifestyleSmoking;
    
        return $this;
    }

    /**
     * Get lifestyle_smoking
     *
     * @return float 
     */
    public function getLifestyleSmoking()
    {
        return $this->lifestyle_smoking;
    }

    /**
     * Set lifestyle_alcohol_q1
     *
     * @param integer $lifestyleAlcoholQ1
     * @return ClientTest
     */
    public function setLifestyleAlcoholQ1($lifestyleAlcoholQ1)
    {
        $this->lifestyle_alcohol_q1 = $lifestyleAlcoholQ1;
    
        return $this;
    }

    /**
     * Get lifestyle_alcohol_q1
     *
     * @return integer 
     */
    public function getLifestyleAlcoholQ1()
    {
        return $this->lifestyle_alcohol_q1;
    }

    /**
     * Set lifestyle_alcohol_q2
     *
     * @param integer $lifestyleAlcoholQ2
     * @return ClientTest
     */
    public function setLifestyleAlcoholQ2($lifestyleAlcoholQ2)
    {
        $this->lifestyle_alcohol_q2 = $lifestyleAlcoholQ2;
    
        return $this;
    }

    /**
     * Get lifestyle_alcohol_q2
     *
     * @return integer 
     */
    public function getLifestyleAlcoholQ2()
    {
        return $this->lifestyle_alcohol_q2;
    }

    /**
     * Set lifestyle_alcohol
     *
     * @param float $lifestyleAlcohol
     * @return ClientTest
     */
    public function setLifestyleAlcohol($lifestyleAlcohol)
    {
        $this->lifestyle_alcohol = $lifestyleAlcohol;
    
        return $this;
    }

    /**
     * Get lifestyle_alcohol
     *
     * @return float 
     */
    public function getLifestyleAlcohol()
    {
        return $this->lifestyle_alcohol;
    }

    /**
     * Set lifestyle_nutrition
     *
     * @param float $lifestyleNutrition
     * @return ClientTest
     */
    public function setLifestyleNutrition($lifestyleNutrition)
    {
        $this->lifestyle_nutrition = $lifestyleNutrition;
    
        return $this;
    }

    /**
     * Get lifestyle_nutrition
     *
     * @return float 
     */
    public function getLifestyleNutrition()
    {
        return $this->lifestyle_nutrition;
    }

    /**
     * Set lifestyle_mental_health
     *
     * @param float $lifestyleMentalHealth
     * @return ClientTest
     */
    public function setLifestyleMentalHealth($lifestyleMentalHealth)
    {
        $this->lifestyle_mental_health = $lifestyleMentalHealth;
    
        return $this;
    }

    /**
     * Get lifestyle_mental_health
     *
     * @return float 
     */
    public function getLifestyleMentalHealth()
    {
        return $this->lifestyle_mental_health;
    }

    /**
     * Set lifestyle_risk_profile
     *
     * @param float $lifestyleRiskProfile
     * @return ClientTest
     */
    public function setLifestyleRiskProfile($lifestyleRiskProfile)
    {
        $this->lifestyle_risk_profile = $lifestyleRiskProfile;
    
        return $this;
    }

    /**
     * Get lifestyle_risk_profile
     *
     * @return float 
     */
    public function getLifestyleRiskProfile()
    {
        return $this->lifestyle_risk_profile;
    }

    /**
     * Set physiological_vo2_test_type
     *
     * @param string $physiologicalVo2TestType
     * @return ClientTest
     */
    public function setPhysiologicalVo2TestType($physiologicalVo2TestType)
    {
        $this->physiological_vo2_test_type = $physiologicalVo2TestType;
    
        return $this;
    }

    /**
     * Get physiological_vo2_test_type
     *
     * @return string 
     */
    public function getPhysiologicalVo2TestType()
    {
        return $this->physiological_vo2_test_type;
    }

    /**
     * Set physiological_vo2_test_value1
     *
     * @param integer $physiologicalVo2TestValue1
     * @return ClientTest
     */
    public function setPhysiologicalVo2TestValue1($physiologicalVo2TestValue1)
    {
        $this->physiological_vo2_test_value1 = $physiologicalVo2TestValue1;
    
        return $this;
    }

    /**
     * Get physiological_vo2_test_value1
     *
     * @return integer 
     */
    public function getPhysiologicalVo2TestValue1()
    {
        return $this->physiological_vo2_test_value1;
    }

    /**
     * Set physiological_vo2_test_value2
     *
     * @param integer $physiologicalVo2TestValue2
     * @return ClientTest
     */
    public function setPhysiologicalVo2TestValue2($physiologicalVo2TestValue2)
    {
        $this->physiological_vo2_test_value2 = $physiologicalVo2TestValue2;
    
        return $this;
    }

    /**
     * Get physiological_vo2_test_value2
     *
     * @return integer 
     */
    public function getPhysiologicalVo2TestValue2()
    {
        return $this->physiological_vo2_test_value2;
    }

    /**
     * Set physiological_vo2
     *
     * @param float $physiologicalVo2
     * @return ClientTest
     */
    public function setPhysiologicalVo2($physiologicalVo2)
    {
        $this->physiological_vo2 = $physiologicalVo2;
    
        return $this;
    }

    /**
     * Get physiological_vo2
     *
     * @return float 
     */
    public function getPhysiologicalVo2()
    {
        return $this->physiological_vo2;
    }

    /**
     * Set physiological_balance_type
     *
     * @param string $physiologicalBalanceType
     * @return ClientTest
     */
    public function setPhysiologicalBalanceType($physiologicalBalanceType)
    {
        $this->physiological_balance_type = $physiologicalBalanceType;
    
        return $this;
    }

    /**
     * Get physiological_balance_type
     *
     * @return string 
     */
    public function getPhysiologicalBalanceType()
    {
        return $this->physiological_balance_type;
    }

    /**
     * Set physiological_balance_value
     *
     * @param integer $physiologicalBalanceValue
     * @return ClientTest
     */
    public function setPhysiologicalBalanceValue($physiologicalBalanceValue)
    {
        $this->physiological_balance_value = $physiologicalBalanceValue;
    
        return $this;
    }

    /**
     * Get physiological_balance_value
     *
     * @return integer 
     */
    public function getPhysiologicalBalanceValue()
    {
        return $this->physiological_balance_value;
    }

    /**
     * Set physiological_balance
     *
     * @param float $physiologicalBalance
     * @return ClientTest
     */
    public function setPhysiologicalBalance($physiologicalBalance)
    {
        $this->physiological_balance = $physiologicalBalance;
    
        return $this;
    }

    /**
     * Get physiological_balance
     *
     * @return float 
     */
    public function getPhysiologicalBalance()
    {
        return $this->physiological_balance;
    }

    /**
     * Set physiological_squat_value
     *
     * @param integer $physiologicalSquatValue
     * @return ClientTest
     */
    public function setPhysiologicalSquatValue($physiologicalSquatValue)
    {
        $this->physiological_squat_value = $physiologicalSquatValue;
    
        return $this;
    }

    /**
     * Get physiological_squat_value
     *
     * @return integer 
     */
    public function getPhysiologicalSquatValue()
    {
        return $this->physiological_squat_value;
    }

    /**
     * Set physiological_squat
     *
     * @param float $physiologicalSquat
     * @return ClientTest
     */
    public function setPhysiologicalSquat($physiologicalSquat)
    {
        $this->physiological_squat = $physiologicalSquat;
    
        return $this;
    }

    /**
     * Get physiological_squat
     *
     * @return float 
     */
    public function getPhysiologicalSquat()
    {
        return $this->physiological_squat;
    }

    /**
     * Set physiological_situp_value
     *
     * @param integer $physiologicalSitupValue
     * @return ClientTest
     */
    public function setPhysiologicalSitupValue($physiologicalSitupValue)
    {
        $this->physiological_situp_value = $physiologicalSitupValue;
    
        return $this;
    }

    /**
     * Get physiological_situp_value
     *
     * @return integer 
     */
    public function getPhysiologicalSitupValue()
    {
        return $this->physiological_situp_value;
    }

    /**
     * Set physiological_situp
     *
     * @param float $physiologicalSitup
     * @return ClientTest
     */
    public function setPhysiologicalSitup($physiologicalSitup)
    {
        $this->physiological_situp = $physiologicalSitup;
    
        return $this;
    }

    /**
     * Get physiological_situp
     *
     * @return float 
     */
    public function getPhysiologicalSitup()
    {
        return $this->physiological_situp;
    }

    /**
     * Set biometric_waist_value
     *
     * @param integer $biometricWaistValue
     * @return ClientTest
     */
    public function setBiometricWaistValue($biometricWaistValue)
    {
        $this->biometric_waist_value = $biometricWaistValue;
    
        return $this;
    }

    /**
     * Get biometric_waist_value
     *
     * @return integer 
     */
    public function getBiometricWaistValue()
    {
        return $this->biometric_waist_value;
    }

    /**
     * Set biometric_waist_value_lost
     *
     * @param integer $biometricWaistValueLost
     * @return ClientTest
     */
    public function setBiometricWaistValueLost($biometricWaistValueLost)
    {
        $this->biometric_waist_value_lost = $biometricWaistValueLost;
    
        return $this;
    }

    /**
     * Get biometric_waist_value_lost
     *
     * @return integer 
     */
    public function getBiometricWaistValueLost()
    {
        return $this->biometric_waist_value_lost;
    }

    /**
     * Set biometric_waist
     *
     * @param float $biometricWaist
     * @return ClientTest
     */
    public function setBiometricWaist($biometricWaist)
    {
        $this->biometric_waist = $biometricWaist;
    
        return $this;
    }

    /**
     * Get biometric_waist
     *
     * @return float 
     */
    public function getBiometricWaist()
    {
        return $this->biometric_waist;
    }

    /**
     * Set biometric_chest
     *
     * @param integer $biometricChest
     * @return ClientTest
     */
    public function setBiometricChest($biometricChest)
    {
        $this->biometric_chest = $biometricChest;
    
        return $this;
    }

    /**
     * Get biometric_chest
     *
     * @return integer 
     */
    public function getBiometricChest()
    {
        return $this->biometric_chest;
    }

    /**
     * Set biometric_chest_lost
     *
     * @param integer $biometricChestLost
     * @return ClientTest
     */
    public function setBiometricChestLost($biometricChestLost)
    {
        $this->biometric_chest_lost = $biometricChestLost;
    
        return $this;
    }

    /**
     * Get biometric_chest_lost
     *
     * @return integer 
     */
    public function getBiometricChestLost()
    {
        return $this->biometric_chest_lost;
    }

    /**
     * Set biometric_abdomen
     *
     * @param integer $biometricAbdomen
     * @return ClientTest
     */
    public function setBiometricAbdomen($biometricAbdomen)
    {
        $this->biometric_abdomen = $biometricAbdomen;
    
        return $this;
    }

    /**
     * Get biometric_abdomen
     *
     * @return integer 
     */
    public function getBiometricAbdomen()
    {
        return $this->biometric_abdomen;
    }

    /**
     * Set biometric_abdomen_lost
     *
     * @param integer $biometricAbdomenLost
     * @return ClientTest
     */
    public function setBiometricAbdomenLost($biometricAbdomenLost)
    {
        $this->biometric_abdomen_lost = $biometricAbdomenLost;
    
        return $this;
    }

    /**
     * Get biometric_abdomen_lost
     *
     * @return integer 
     */
    public function getBiometricAbdomenLost()
    {
        return $this->biometric_abdomen_lost;
    }

    /**
     * Set biometric_hip
     *
     * @param integer $biometricHip
     * @return ClientTest
     */
    public function setBiometricHip($biometricHip)
    {
        $this->biometric_hip = $biometricHip;
    
        return $this;
    }

    /**
     * Get biometric_hip
     *
     * @return integer 
     */
    public function getBiometricHip()
    {
        return $this->biometric_hip;
    }

    /**
     * Set biometric_hip_lost
     *
     * @param integer $biometricHipLost
     * @return ClientTest
     */
    public function setBiometricHipLost($biometricHipLost)
    {
        $this->biometric_hip_lost = $biometricHipLost;
    
        return $this;
    }

    /**
     * Get biometric_hip_lost
     *
     * @return integer 
     */
    public function getBiometricHipLost()
    {
        return $this->biometric_hip_lost;
    }

    /**
     * Set biometric_thigh
     *
     * @param integer $biometricThigh
     * @return ClientTest
     */
    public function setBiometricThigh($biometricThigh)
    {
        $this->biometric_thigh = $biometricThigh;
    
        return $this;
    }

    /**
     * Get biometric_thigh
     *
     * @return integer 
     */
    public function getBiometricThigh()
    {
        return $this->biometric_thigh;
    }

    /**
     * Set biometric_thigh_lost
     *
     * @param integer $biometricThighLost
     * @return ClientTest
     */
    public function setBiometricThighLost($biometricThighLost)
    {
        $this->biometric_thigh_lost = $biometricThighLost;
    
        return $this;
    }

    /**
     * Get biometric_thigh_lost
     *
     * @return integer 
     */
    public function getBiometricThighLost()
    {
        return $this->biometric_thigh_lost;
    }

    /**
     * Set biometric_arm
     *
     * @param integer $biometricArm
     * @return ClientTest
     */
    public function setBiometricArm($biometricArm)
    {
        $this->biometric_arm = $biometricArm;
    
        return $this;
    }

    /**
     * Get biometric_arm
     *
     * @return integer 
     */
    public function getBiometricArm()
    {
        return $this->biometric_arm;
    }

    /**
     * Set biometric_arm_lost
     *
     * @param integer $biometricArmLost
     * @return ClientTest
     */
    public function setBiometricArmLost($biometricArmLost)
    {
        $this->biometric_arm_lost = $biometricArmLost;
    
        return $this;
    }

    /**
     * Get biometric_arm_lost
     *
     * @return integer 
     */
    public function getBiometricArmLost()
    {
        return $this->biometric_arm_lost;
    }

    /**
     * Set biometric_body_fat_value
     *
     * @param string $biometricBodyFatValue
     * @return ClientTest
     */
    public function setBiometricBodyFatValue($biometricBodyFatValue)
    {
        $this->biometric_body_fat_value = $biometricBodyFatValue;
    
        return $this;
    }

    /**
     * Get biometric_body_fat_value
     *
     * @return string 
     */
    public function getBiometricBodyFatValue()
    {
        return $this->biometric_body_fat_value;
    }

    /**
     * Set biometric_body_fat
     *
     * @param float $biometricBodyFat
     * @return ClientTest
     */
    public function setBiometricBodyFat($biometricBodyFat)
    {
        $this->biometric_body_fat = $biometricBodyFat;
    
        return $this;
    }

    /**
     * Get biometric_body_fat
     *
     * @return float 
     */
    public function getBiometricBodyFat()
    {
        return $this->biometric_body_fat;
    }

    /**
     * Set biometric_blood_pressure_systolic
     *
     * @param integer $biometricBloodPressureSystolic
     * @return ClientTest
     */
    public function setBiometricBloodPressureSystolic($biometricBloodPressureSystolic)
    {
        $this->biometric_blood_pressure_systolic = $biometricBloodPressureSystolic;
    
        return $this;
    }

    /**
     * Get biometric_blood_pressure_systolic
     *
     * @return integer 
     */
    public function getBiometricBloodPressureSystolic()
    {
        return $this->biometric_blood_pressure_systolic;
    }

    /**
     * Set biometric_blood_pressure_diastolic
     *
     * @param integer $biometricBloodPressureDiastolic
     * @return ClientTest
     */
    public function setBiometricBloodPressureDiastolic($biometricBloodPressureDiastolic)
    {
        $this->biometric_blood_pressure_diastolic = $biometricBloodPressureDiastolic;
    
        return $this;
    }

    /**
     * Get biometric_blood_pressure_diastolic
     *
     * @return integer 
     */
    public function getBiometricBloodPressureDiastolic()
    {
        return $this->biometric_blood_pressure_diastolic;
    }

    /**
     * Set biometric_blood_pressure
     *
     * @param float $biometricBloodPressure
     * @return ClientTest
     */
    public function setBiometricBloodPressure($biometricBloodPressure)
    {
        $this->biometric_blood_pressure = $biometricBloodPressure;
    
        return $this;
    }

    /**
     * Get biometric_blood_pressure
     *
     * @return float 
     */
    public function getBiometricBloodPressure()
    {
        return $this->biometric_blood_pressure;
    }

    /**
     * Set biometric_blood_oxygen_value
     *
     * @param string $biometricBloodOxygenValue
     * @return ClientTest
     */
    public function setBiometricBloodOxygenValue($biometricBloodOxygenValue)
    {
        $this->biometric_blood_oxygen_value = $biometricBloodOxygenValue;
    
        return $this;
    }

    /**
     * Get biometric_blood_oxygen_value
     *
     * @return string 
     */
    public function getBiometricBloodOxygenValue()
    {
        return $this->biometric_blood_oxygen_value;
    }

    /**
     * Set biometric_blood_oxygen
     *
     * @param float $biometricBloodOxygen
     * @return ClientTest
     */
    public function setBiometricBloodOxygen($biometricBloodOxygen)
    {
        $this->biometric_blood_oxygen = $biometricBloodOxygen;
    
        return $this;
    }

    /**
     * Get biometric_blood_oxygen
     *
     * @return float 
     */
    public function getBiometricBloodOxygen()
    {
        return $this->biometric_blood_oxygen;
    }

    /**
     * Set total_crf
     *
     * @param float $totalCrf
     * @return ClientTest
     */
    public function setTotalCrf($totalCrf)
    {
        $this->total_crf = $totalCrf;
    
        return $this;
    }

    /**
     * Get total_crf
     *
     * @return float 
     */
    public function getTotalCrf()
    {
        return $this->total_crf;
    }

    /**
     * Set healthy_life_points
     *
     * @param float $healthyLifePoints
     * @return ClientTest
     */
    public function setHealthyLifePoints($healthyLifePoints)
    {
        $this->healthy_life_points = $healthyLifePoints;
    
        return $this;
    }

    /**
     * Get healthy_life_points
     *
     * @return float 
     */
    public function getHealthyLifePoints()
    {
        return $this->healthy_life_points;
    }

    /**
     * Set estimated_healthy_life_expectancy
     *
     * @param float $estimatedHealthyLifeExpectancy
     * @return ClientTest
     */
    public function setEstimatedHealthyLifeExpectancy($estimatedHealthyLifeExpectancy)
    {
        $this->estimated_healthy_life_expectancy = $estimatedHealthyLifeExpectancy;
    
        return $this;
    }

    /**
     * Get estimated_healthy_life_expectancy
     *
     * @return float 
     */
    public function getEstimatedHealthyLifeExpectancy()
    {
        return $this->estimated_healthy_life_expectancy;
    }

    /**
     * Set max_hle
     *
     * @param float $maxHle
     * @return ClientTest
     */
    public function setMaxHle($maxHle)
    {
        $this->max_hle = $maxHle;
    
        return $this;
    }

    /**
     * Get max_hle
     *
     * @return float 
     */
    public function getMaxHle()
    {
        return $this->max_hle;
    }

    /**
     * Set healthy_life_score
     *
     * @param float $healthyLifeScore
     * @return ClientTest
     */
    public function setHealthyLifeScore($healthyLifeScore)
    {
        $this->healthy_life_score = $healthyLifeScore;
    
        return $this;
    }

    /**
     * Get healthy_life_score
     *
     * @return float 
     */
    public function getHealthyLifeScore()
    {
        return $this->healthy_life_score;
    }

    /**
     * Set estimated_years_of_healthy_life_left
     *
     * @param float $estimatedYearsOfHealthyLifeLeft
     * @return ClientTest
     */
    public function setEstimatedYearsOfHealthyLifeLeft($estimatedYearsOfHealthyLifeLeft)
    {
        $this->estimated_years_of_healthy_life_left = $estimatedYearsOfHealthyLifeLeft;
    
        return $this;
    }

    /**
     * Get estimated_years_of_healthy_life_left
     *
     * @return float 
     */
    public function getEstimatedYearsOfHealthyLifeLeft()
    {
        return $this->estimated_years_of_healthy_life_left;
    }

    /**
     * Set estimated_years_you_will_live_with_did
     *
     * @param float $estimatedYearsYouWillLiveWithDid
     * @return ClientTest
     */
    public function setEstimatedYearsYouWillLiveWithDid($estimatedYearsYouWillLiveWithDid)
    {
        $this->estimated_years_you_will_live_with_did = $estimatedYearsYouWillLiveWithDid;
    
        return $this;
    }

    /**
     * Get estimated_years_you_will_live_with_did
     *
     * @return float 
     */
    public function getEstimatedYearsYouWillLiveWithDid()
    {
        return $this->estimated_years_you_will_live_with_did;
    }

    /**
     * Set estimated_years_you_will_live_with_did_as_opposed_to
     *
     * @param float $estimatedYearsYouWillLiveWithDidAsOpposedTo
     * @return ClientTest
     */
    public function setEstimatedYearsYouWillLiveWithDidAsOpposedTo($estimatedYearsYouWillLiveWithDidAsOpposedTo)
    {
        $this->estimated_years_you_will_live_with_did_as_opposed_to = $estimatedYearsYouWillLiveWithDidAsOpposedTo;
    
        return $this;
    }

    /**
     * Get estimated_years_you_will_live_with_did_as_opposed_to
     *
     * @return float 
     */
    public function getEstimatedYearsYouWillLiveWithDidAsOpposedTo()
    {
        return $this->estimated_years_you_will_live_with_did_as_opposed_to;
    }

    /**
     * Set you_could_add_up_to
     *
     * @param float $youCouldAddUpTo
     * @return ClientTest
     */
    public function setYouCouldAddUpTo($youCouldAddUpTo)
    {
        $this->you_could_add_up_to = $youCouldAddUpTo;
    
        return $this;
    }

    /**
     * Get you_could_add_up_to
     *
     * @return float 
     */
    public function getYouCouldAddUpTo()
    {
        return $this->you_could_add_up_to;
    }

    /**
     * Set estimated_current_healthy_life_age
     *
     * @param float $estimatedCurrentHealthyLifeAge
     * @return ClientTest
     */
    public function setEstimatedCurrentHealthyLifeAge($estimatedCurrentHealthyLifeAge)
    {
        $this->estimated_current_healthy_life_age = $estimatedCurrentHealthyLifeAge;
    
        return $this;
    }

    /**
     * Get estimated_current_healthy_life_age
     *
     * @return float 
     */
    public function getEstimatedCurrentHealthyLifeAge()
    {
        return $this->estimated_current_healthy_life_age;
    }

    /**
     * Set years_added
     *
     * @param float $yearsAdded
     * @return ClientTest
     */
    public function setYearsAdded($yearsAdded)
    {
        $this->years_added = $yearsAdded;
    
        return $this;
    }

    /**
     * Get years_added
     *
     * @return float 
     */
    public function getYearsAdded()
    {
        return $this->years_added;
    }

    /**
     * Set cm_lost
     *
     * @param float $cmLost
     * @return ClientTest
     */
    public function setCmLost($cmLost)
    {
        $this->cm_lost = $cmLost;
    
        return $this;
    }

    /**
     * Get cm_lost
     *
     * @return float 
     */
    public function getCmLost()
    {
        return $this->cm_lost;
    }

    /**
     * Set kg_lost
     *
     * @param float $kgLost
     * @return ClientTest
     */
    public function setKgLost($kgLost)
    {
        $this->kg_lost = $kgLost;
    
        return $this;
    }

    /**
     * Get kg_lost
     *
     * @return float 
     */
    public function getKgLost()
    {
        return $this->kg_lost;
    }

    /**
     * Set bmi_improvement
     *
     * @param float $bmiImprovement
     * @return ClientTest
     */
    public function setBmiImprovement($bmiImprovement)
    {
        $this->bmi_improvement = $bmiImprovement;
    
        return $this;
    }

    /**
     * Get bmi_improvement
     *
     * @return float 
     */
    public function getBmiImprovement()
    {
        return $this->bmi_improvement;
    }

    /**
     * Set body_fat_reduction
     *
     * @param float $bodyFatReduction
     * @return ClientTest
     */
    public function setBodyFatReduction($bodyFatReduction)
    {
        $this->body_fat_reduction = $bodyFatReduction;
    
        return $this;
    }

    /**
     * Get body_fat_reduction
     *
     * @return float 
     */
    public function getBodyFatReduction()
    {
        return $this->body_fat_reduction;
    }

    /**
     * Set vo2_improvement
     *
     * @param float $vo2Improvement
     * @return ClientTest
     */
    public function setVo2Improvement($vo2Improvement)
    {
        $this->vo2_improvement = $vo2Improvement;
    
        return $this;
    }

    /**
     * Get vo2_improvement
     *
     * @return float 
     */
    public function getVo2Improvement()
    {
        return $this->vo2_improvement;
    }

    /**
     * Set squat_test_improvement
     *
     * @param float $squatTestImprovement
     * @return ClientTest
     */
    public function setSquatTestImprovement($squatTestImprovement)
    {
        $this->squat_test_improvement = $squatTestImprovement;
    
        return $this;
    }

    /**
     * Get squat_test_improvement
     *
     * @return float 
     */
    public function getSquatTestImprovement()
    {
        return $this->squat_test_improvement;
    }

    /**
     * Set standing_functional_reach_improvement
     *
     * @param float $standingFunctionalReachImprovement
     * @return ClientTest
     */
    public function setStandingFunctionalReachImprovement($standingFunctionalReachImprovement)
    {
        $this->standing_functional_reach_improvement = $standingFunctionalReachImprovement;
    
        return $this;
    }

    /**
     * Get standing_functional_reach_improvement
     *
     * @return float 
     */
    public function getStandingFunctionalReachImprovement()
    {
        return $this->standing_functional_reach_improvement;
    }

    /**
     * Set stork_test_improvement
     *
     * @param float $storkTestImprovement
     * @return ClientTest
     */
    public function setStorkTestImprovement($storkTestImprovement)
    {
        $this->stork_test_improvement = $storkTestImprovement;
    
        return $this;
    }

    /**
     * Get stork_test_improvement
     *
     * @return float 
     */
    public function getStorkTestImprovement()
    {
        return $this->stork_test_improvement;
    }

    /**
     * Set abdominal_test_improvement
     *
     * @param float $abdominalTestImprovement
     * @return ClientTest
     */
    public function setAbdominalTestImprovement($abdominalTestImprovement)
    {
        $this->abdominal_test_improvement = $abdominalTestImprovement;
    
        return $this;
    }

    /**
     * Get abdominal_test_improvement
     *
     * @return float 
     */
    public function getAbdominalTestImprovement()
    {
        return $this->abdominal_test_improvement;
    }

    /**
     * Set vigorous_pa_per_week
     *
     * @param float $vigorousPaPerWeek
     * @return ClientTest
     */
    public function setVigorousPaPerWeek($vigorousPaPerWeek)
    {
        $this->vigorous_pa_per_week = $vigorousPaPerWeek;
    
        return $this;
    }

    /**
     * Get vigorous_pa_per_week
     *
     * @return float 
     */
    public function getVigorousPaPerWeek()
    {
        return $this->vigorous_pa_per_week;
    }

    /**
     * Set strength_pa_per_week
     *
     * @param float $strengthPaPerWeek
     * @return ClientTest
     */
    public function setStrengthPaPerWeek($strengthPaPerWeek)
    {
        $this->strength_pa_per_week = $strengthPaPerWeek;
    
        return $this;
    }

    /**
     * Get strength_pa_per_week
     *
     * @return float 
     */
    public function getStrengthPaPerWeek()
    {
        return $this->strength_pa_per_week;
    }

    /**
     * Set smoking_improvement
     *
     * @param float $smokingImprovement
     * @return ClientTest
     */
    public function setSmokingImprovement($smokingImprovement)
    {
        $this->smoking_improvement = $smokingImprovement;
    
        return $this;
    }

    /**
     * Get smoking_improvement
     *
     * @return float 
     */
    public function getSmokingImprovement()
    {
        return $this->smoking_improvement;
    }

    /**
     * Set standard_alcohol_per_week
     *
     * @param float $standardAlcoholPerWeek
     * @return ClientTest
     */
    public function setStandardAlcoholPerWeek($standardAlcoholPerWeek)
    {
        $this->standard_alcohol_per_week = $standardAlcoholPerWeek;
    
        return $this;
    }

    /**
     * Get standard_alcohol_per_week
     *
     * @return float 
     */
    public function getStandardAlcoholPerWeek()
    {
        return $this->standard_alcohol_per_week;
    }

    /**
     * Set maximum_alcohol_one_sitting
     *
     * @param float $maximumAlcoholOneSitting
     * @return ClientTest
     */
    public function setMaximumAlcoholOneSitting($maximumAlcoholOneSitting)
    {
        $this->maximum_alcohol_one_sitting = $maximumAlcoholOneSitting;
    
        return $this;
    }

    /**
     * Get maximum_alcohol_one_sitting
     *
     * @return float 
     */
    public function getMaximumAlcoholOneSitting()
    {
        return $this->maximum_alcohol_one_sitting;
    }

    /**
     * Set nutrition_score_improvement
     *
     * @param float $nutritionScoreImprovement
     * @return ClientTest
     */
    public function setNutritionScoreImprovement($nutritionScoreImprovement)
    {
        $this->nutrition_score_improvement = $nutritionScoreImprovement;
    
        return $this;
    }

    /**
     * Get nutrition_score_improvement
     *
     * @return float 
     */
    public function getNutritionScoreImprovement()
    {
        return $this->nutrition_score_improvement;
    }

    /**
     * Set mental_health_improvement
     *
     * @param float $mentalHealthImprovement
     * @return ClientTest
     */
    public function setMentalHealthImprovement($mentalHealthImprovement)
    {
        $this->mental_health_improvement = $mentalHealthImprovement;
    
        return $this;
    }

    /**
     * Get mental_health_improvement
     *
     * @return float 
     */
    public function getMentalHealthImprovement()
    {
        return $this->mental_health_improvement;
    }

    /**
     * Set risk_profile_improvement
     *
     * @param float $riskProfileImprovement
     * @return ClientTest
     */
    public function setRiskProfileImprovement($riskProfileImprovement)
    {
        $this->risk_profile_improvement = $riskProfileImprovement;
    
        return $this;
    }

    /**
     * Get risk_profile_improvement
     *
     * @return float 
     */
    public function getRiskProfileImprovement()
    {
        return $this->risk_profile_improvement;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return ClientTest
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set waist_hip_ratio
     *
     * @param float $waistHipRatio
     * @return ClientTest
     */
    public function setWaistHipRatio($waistHipRatio)
    {
        $this->waist_hip_ratio = $waistHipRatio;
    
        return $this;
    }

    /**
     * Get waist_hip_ratio
     *
     * @return float 
     */
    public function getWaistHipRatio()
    {
        return $this->waist_hip_ratio;
    }

    /**
     * Set waist_hip_ratio_score
     *
     * @param float $waistHipRatioScore
     * @return ClientTest
     */
    public function setWaistHipRatioScore($waistHipRatioScore)
    {
        $this->waist_hip_ratio_score = $waistHipRatioScore;
    
        return $this;
    }

    /**
     * Get waist_hip_ratio_score
     *
     * @return float 
     */
    public function getWaistHipRatioScore()
    {
        return $this->waist_hip_ratio_score;
    }

    /**
     * Set physiological_vo2_score
     *
     * @param float $physiologicalVo2Score
     * @return ClientTest
     */
    public function setPhysiologicalVo2Score($physiologicalVo2Score)
    {
        $this->physiological_vo2_score = $physiologicalVo2Score;
    
        return $this;
    }

    /**
     * Get physiological_vo2_score
     *
     * @return float 
     */
    public function getPhysiologicalVo2Score()
    {
        return $this->physiological_vo2_score;
    }

    /**
     * Set biometric_resting_heart_rate
     *
     * @param string $biometricRestingHeartRate
     * @return ClientTest
     */
    public function setBiometricRestingHeartRate($biometricRestingHeartRate)
    {
        $this->biometric_resting_heart_rate = $biometricRestingHeartRate;
    
        return $this;
    }

    /**
     * Get biometric_resting_heart_rate
     *
     * @return string 
     */
    public function getBiometricRestingHeartRate()
    {
        return $this->biometric_resting_heart_rate;
    }

    /**
     * Set test_type
     *
     * @param string $testType
     * @return ClientTest
     */
    public function setTestType($testType)
    {
        $this->test_type = $testType;
    
        return $this;
    }

    /**
     * Get test_type
     *
     * @return string 
     */
    public function getTestType()
    {
        return $this->test_type;
    }

    /**
     * Set biometric_thigh_right
     *
     * @param integer $biometricThighRight
     * @return ClientTest
     */
    public function setBiometricThighRight($biometricThighRight)
    {
        $this->biometric_thigh_right = $biometricThighRight;
    
        return $this;
    }

    /**
     * Get biometric_thigh_right
     *
     * @return integer 
     */
    public function getBiometricThighRight()
    {
        return $this->biometric_thigh_right;
    }

    /**
     * Set biometric_thigh_left
     *
     * @param integer $biometricThighLeft
     * @return ClientTest
     */
    public function setBiometricThighLeft($biometricThighLeft)
    {
        $this->biometric_thigh_left = $biometricThighLeft;
    
        return $this;
    }

    /**
     * Get biometric_thigh_left
     *
     * @return integer 
     */
    public function getBiometricThighLeft()
    {
        return $this->biometric_thigh_left;
    }

    /**
     * Set biometric_arm_right
     *
     * @param integer $biometricArmRight
     * @return ClientTest
     */
    public function setBiometricArmRight($biometricArmRight)
    {
        $this->biometric_arm_right = $biometricArmRight;
    
        return $this;
    }

    /**
     * Get biometric_arm_right
     *
     * @return integer 
     */
    public function getBiometricArmRight()
    {
        return $this->biometric_arm_right;
    }

    /**
     * Set biometric_arm_left
     *
     * @param integer $biometricArmLeft
     * @return ClientTest
     */
    public function setBiometricArmLeft($biometricArmLeft)
    {
        $this->biometric_arm_left = $biometricArmLeft;
    
        return $this;
    }

    /**
     * Get biometric_arm_left
     *
     * @return integer 
     */
    public function getBiometricArmLeft()
    {
        return $this->biometric_arm_left;
    }

    /**
     * Set physiological_squat_1rm_direct_entry
     *
     * @param float $physiologicalSquat1rmDirectEntry
     * @return ClientTest
     */
    public function setPhysiologicalSquat1rmDirectEntry($physiologicalSquat1rmDirectEntry)
    {
        $this->physiological_squat_1rm_direct_entry = $physiologicalSquat1rmDirectEntry;
    
        return $this;
    }

    /**
     * Get physiological_squat_1rm_direct_entry
     *
     * @return float 
     */
    public function getPhysiologicalSquat1rmDirectEntry()
    {
        return $this->physiological_squat_1rm_direct_entry;
    }

    /**
     * Set physiological_squat_1rm_weight
     *
     * @param float $physiologicalSquat1rmWeight
     * @return ClientTest
     */
    public function setPhysiologicalSquat1rmWeight($physiologicalSquat1rmWeight)
    {
        $this->physiological_squat_1rm_weight = $physiologicalSquat1rmWeight;
    
        return $this;
    }

    /**
     * Get physiological_squat_1rm_weight
     *
     * @return float 
     */
    public function getPhysiologicalSquat1rmWeight()
    {
        return $this->physiological_squat_1rm_weight;
    }

    /**
     * Set physiological_squat_1rm_reps
     *
     * @param float $physiologicalSquat1rmReps
     * @return ClientTest
     */
    public function setPhysiologicalSquat1rmReps($physiologicalSquat1rmReps)
    {
        $this->physiological_squat_1rm_reps = $physiologicalSquat1rmReps;
    
        return $this;
    }

    /**
     * Get physiological_squat_1rm_reps
     *
     * @return float 
     */
    public function getPhysiologicalSquat1rmReps()
    {
        return $this->physiological_squat_1rm_reps;
    }

    /**
     * Set physiological_squat_1rm
     *
     * @param float $physiologicalSquat1rm
     * @return ClientTest
     */
    public function setPhysiologicalSquat1rm($physiologicalSquat1rm)
    {
        $this->physiological_squat_1rm = $physiologicalSquat1rm;
    
        return $this;
    }

    /**
     * Get physiological_squat_1rm
     *
     * @return float 
     */
    public function getPhysiologicalSquat1rm()
    {
        return $this->physiological_squat_1rm;
    }

    /**
     * Set physiological_bench_press_1rm_direct_entry
     *
     * @param float $physiologicalBenchPress1rmDirectEntry
     * @return ClientTest
     */
    public function setPhysiologicalBenchPress1rmDirectEntry($physiologicalBenchPress1rmDirectEntry)
    {
        $this->physiological_bench_press_1rm_direct_entry = $physiologicalBenchPress1rmDirectEntry;
    
        return $this;
    }

    /**
     * Get physiological_bench_press_1rm_direct_entry
     *
     * @return float 
     */
    public function getPhysiologicalBenchPress1rmDirectEntry()
    {
        return $this->physiological_bench_press_1rm_direct_entry;
    }

    /**
     * Set physiological_bench_press_1rm_weight
     *
     * @param float $physiologicalBenchPress1rmWeight
     * @return ClientTest
     */
    public function setPhysiologicalBenchPress1rmWeight($physiologicalBenchPress1rmWeight)
    {
        $this->physiological_bench_press_1rm_weight = $physiologicalBenchPress1rmWeight;
    
        return $this;
    }

    /**
     * Get physiological_bench_press_1rm_weight
     *
     * @return float 
     */
    public function getPhysiologicalBenchPress1rmWeight()
    {
        return $this->physiological_bench_press_1rm_weight;
    }

    /**
     * Set physiological_bench_press_1rm_reps
     *
     * @param float $physiologicalBenchPress1rmReps
     * @return ClientTest
     */
    public function setPhysiologicalBenchPress1rmReps($physiologicalBenchPress1rmReps)
    {
        $this->physiological_bench_press_1rm_reps = $physiologicalBenchPress1rmReps;
    
        return $this;
    }

    /**
     * Get physiological_bench_press_1rm_reps
     *
     * @return float 
     */
    public function getPhysiologicalBenchPress1rmReps()
    {
        return $this->physiological_bench_press_1rm_reps;
    }

    /**
     * Set physiological_bench_press_1rm
     *
     * @param float $physiologicalBenchPress1rm
     * @return ClientTest
     */
    public function setPhysiologicalBenchPress1rm($physiologicalBenchPress1rm)
    {
        $this->physiological_bench_press_1rm = $physiologicalBenchPress1rm;
    
        return $this;
    }

    /**
     * Get physiological_bench_press_1rm
     *
     * @return float 
     */
    public function getPhysiologicalBenchPress1rm()
    {
        return $this->physiological_bench_press_1rm;
    }

    /**
     * Set physiological_deadlift_1rm_direct_entry
     *
     * @param float $physiologicalDeadlift1rmDirectEntry
     * @return ClientTest
     */
    public function setPhysiologicalDeadlift1rmDirectEntry($physiologicalDeadlift1rmDirectEntry)
    {
        $this->physiological_deadlift_1rm_direct_entry = $physiologicalDeadlift1rmDirectEntry;
    
        return $this;
    }

    /**
     * Get physiological_deadlift_1rm_direct_entry
     *
     * @return float 
     */
    public function getPhysiologicalDeadlift1rmDirectEntry()
    {
        return $this->physiological_deadlift_1rm_direct_entry;
    }

    /**
     * Set physiological_deadlift_1rm_weight
     *
     * @param float $physiologicalDeadlift1rmWeight
     * @return ClientTest
     */
    public function setPhysiologicalDeadlift1rmWeight($physiologicalDeadlift1rmWeight)
    {
        $this->physiological_deadlift_1rm_weight = $physiologicalDeadlift1rmWeight;
    
        return $this;
    }

    /**
     * Get physiological_deadlift_1rm_weight
     *
     * @return float 
     */
    public function getPhysiologicalDeadlift1rmWeight()
    {
        return $this->physiological_deadlift_1rm_weight;
    }

    /**
     * Set physiological_deadlift_1rm_reps
     *
     * @param float $physiologicalDeadlift1rmReps
     * @return ClientTest
     */
    public function setPhysiologicalDeadlift1rmReps($physiologicalDeadlift1rmReps)
    {
        $this->physiological_deadlift_1rm_reps = $physiologicalDeadlift1rmReps;
    
        return $this;
    }

    /**
     * Get physiological_deadlift_1rm_reps
     *
     * @return float 
     */
    public function getPhysiologicalDeadlift1rmReps()
    {
        return $this->physiological_deadlift_1rm_reps;
    }

    /**
     * Set physiological_deadlift_1rm
     *
     * @param float $physiologicalDeadlift1rm
     * @return ClientTest
     */
    public function setPhysiologicalDeadlift1rm($physiologicalDeadlift1rm)
    {
        $this->physiological_deadlift_1rm = $physiologicalDeadlift1rm;
    
        return $this;
    }

    /**
     * Get physiological_deadlift_1rm
     *
     * @return float 
     */
    public function getPhysiologicalDeadlift1rm()
    {
        return $this->physiological_deadlift_1rm;
    }

    /**
     * Set physiological_overhead_press_direct_entry
     *
     * @param float $physiologicalOverheadPressDirectEntry
     * @return ClientTest
     */
    public function setPhysiologicalOverheadPressDirectEntry($physiologicalOverheadPressDirectEntry)
    {
        $this->physiological_overhead_press_direct_entry = $physiologicalOverheadPressDirectEntry;
    
        return $this;
    }

    /**
     * Get physiological_overhead_press_direct_entry
     *
     * @return float 
     */
    public function getPhysiologicalOverheadPressDirectEntry()
    {
        return $this->physiological_overhead_press_direct_entry;
    }

    /**
     * Set physiological_overhead_press_weight
     *
     * @param float $physiologicalOverheadPressWeight
     * @return ClientTest
     */
    public function setPhysiologicalOverheadPressWeight($physiologicalOverheadPressWeight)
    {
        $this->physiological_overhead_press_weight = $physiologicalOverheadPressWeight;
    
        return $this;
    }

    /**
     * Get physiological_overhead_press_weight
     *
     * @return float 
     */
    public function getPhysiologicalOverheadPressWeight()
    {
        return $this->physiological_overhead_press_weight;
    }

    /**
     * Set physiological_overhead_press_reps
     *
     * @param float $physiologicalOverheadPressReps
     * @return ClientTest
     */
    public function setPhysiologicalOverheadPressReps($physiologicalOverheadPressReps)
    {
        $this->physiological_overhead_press_reps = $physiologicalOverheadPressReps;
    
        return $this;
    }

    /**
     * Get physiological_overhead_press_reps
     *
     * @return float 
     */
    public function getPhysiologicalOverheadPressReps()
    {
        return $this->physiological_overhead_press_reps;
    }

    /**
     * Set physiological_overhead_press
     *
     * @param float $physiologicalOverheadPress
     * @return ClientTest
     */
    public function setPhysiologicalOverheadPress($physiologicalOverheadPress)
    {
        $this->physiological_overhead_press = $physiologicalOverheadPress;
    
        return $this;
    }

    /**
     * Get physiological_overhead_press
     *
     * @return float 
     */
    public function getPhysiologicalOverheadPress()
    {
        return $this->physiological_overhead_press;
    }

    /**
     * Set physiological_squat_1rm_s_direct_entry
     *
     * @param integer $physiologicalSquat1rmSDirectEntry
     * @return ClientTest
     */
    public function setPhysiologicalSquat1rmSDirectEntry($physiologicalSquat1rmSDirectEntry)
    {
        $this->physiological_squat_1rm_s_direct_entry = $physiologicalSquat1rmSDirectEntry;
    
        return $this;
    }

    /**
     * Get physiological_squat_1rm_s_direct_entry
     *
     * @return integer 
     */
    public function getPhysiologicalSquat1rmSDirectEntry()
    {
        return $this->physiological_squat_1rm_s_direct_entry;
    }

    /**
     * Set physiological_bench_press_1rm_s_direct_entry
     *
     * @param integer $physiologicalBenchPress1rmSDirectEntry
     * @return ClientTest
     */
    public function setPhysiologicalBenchPress1rmSDirectEntry($physiologicalBenchPress1rmSDirectEntry)
    {
        $this->physiological_bench_press_1rm_s_direct_entry = $physiologicalBenchPress1rmSDirectEntry;
    
        return $this;
    }

    /**
     * Get physiological_bench_press_1rm_s_direct_entry
     *
     * @return integer 
     */
    public function getPhysiologicalBenchPress1rmSDirectEntry()
    {
        return $this->physiological_bench_press_1rm_s_direct_entry;
    }

    /**
     * Set physiological_deadlift_1rm_s_direct_entry
     *
     * @param integer $physiologicalDeadlift1rmSDirectEntry
     * @return ClientTest
     */
    public function setPhysiologicalDeadlift1rmSDirectEntry($physiologicalDeadlift1rmSDirectEntry)
    {
        $this->physiological_deadlift_1rm_s_direct_entry = $physiologicalDeadlift1rmSDirectEntry;
    
        return $this;
    }

    /**
     * Get physiological_deadlift_1rm_s_direct_entry
     *
     * @return integer 
     */
    public function getPhysiologicalDeadlift1rmSDirectEntry()
    {
        return $this->physiological_deadlift_1rm_s_direct_entry;
    }

    /**
     * Set physiological_overhead_press_s_direct_entry
     *
     * @param integer $physiologicalOverheadPressSDirectEntry
     * @return ClientTest
     */
    public function setPhysiologicalOverheadPressSDirectEntry($physiologicalOverheadPressSDirectEntry)
    {
        $this->physiological_overhead_press_s_direct_entry = $physiologicalOverheadPressSDirectEntry;
    
        return $this;
    }

    /**
     * Get physiological_overhead_press_s_direct_entry
     *
     * @return integer 
     */
    public function getPhysiologicalOverheadPressSDirectEntry()
    {
        return $this->physiological_overhead_press_s_direct_entry;
    }

    /**
     * Set physiological_squat_1min_min
     *
     * @param integer $physiologicalSquat1minMin
     * @return ClientTest
     */
    public function setPhysiologicalSquat1minMin($physiologicalSquat1minMin)
    {
        $this->physiological_squat_1min_min = $physiologicalSquat1minMin;
    
        return $this;
    }

    /**
     * Get physiological_squat_1min_min
     *
     * @return integer 
     */
    public function getPhysiologicalSquat1minMin()
    {
        return $this->physiological_squat_1min_min;
    }

    /**
     * Set physiological_squat_1min_sec
     *
     * @param integer $physiologicalSquat1minSec
     * @return ClientTest
     */
    public function setPhysiologicalSquat1minSec($physiologicalSquat1minSec)
    {
        $this->physiological_squat_1min_sec = $physiologicalSquat1minSec;
    
        return $this;
    }

    /**
     * Get physiological_squat_1min_sec
     *
     * @return integer 
     */
    public function getPhysiologicalSquat1minSec()
    {
        return $this->physiological_squat_1min_sec;
    }

    /**
     * Set physiological_push_ups_1min_min
     *
     * @param integer $physiologicalPushUps1minMin
     * @return ClientTest
     */
    public function setPhysiologicalPushUps1minMin($physiologicalPushUps1minMin)
    {
        $this->physiological_push_ups_1min_min = $physiologicalPushUps1minMin;
    
        return $this;
    }

    /**
     * Get physiological_push_ups_1min_min
     *
     * @return integer 
     */
    public function getPhysiologicalPushUps1minMin()
    {
        return $this->physiological_push_ups_1min_min;
    }

    /**
     * Set physiological_push_ups_1min_sec
     *
     * @param integer $physiologicalPushUps1minSec
     * @return ClientTest
     */
    public function setPhysiologicalPushUps1minSec($physiologicalPushUps1minSec)
    {
        $this->physiological_push_ups_1min_sec = $physiologicalPushUps1minSec;
    
        return $this;
    }

    /**
     * Get physiological_push_ups_1min_sec
     *
     * @return integer 
     */
    public function getPhysiologicalPushUps1minSec()
    {
        return $this->physiological_push_ups_1min_sec;
    }

    /**
     * Set physiological_sit_ups_1min_min
     *
     * @param integer $physiologicalSitUps1minMin
     * @return ClientTest
     */
    public function setPhysiologicalSitUps1minMin($physiologicalSitUps1minMin)
    {
        $this->physiological_sit_ups_1min_min = $physiologicalSitUps1minMin;
    
        return $this;
    }

    /**
     * Get physiological_sit_ups_1min_min
     *
     * @return integer 
     */
    public function getPhysiologicalSitUps1minMin()
    {
        return $this->physiological_sit_ups_1min_min;
    }

    /**
     * Set physiological_sit_ups_1min_sec
     *
     * @param integer $physiologicalSitUps1minSec
     * @return ClientTest
     */
    public function setPhysiologicalSitUps1minSec($physiologicalSitUps1minSec)
    {
        $this->physiological_sit_ups_1min_sec = $physiologicalSitUps1minSec;
    
        return $this;
    }

    /**
     * Get physiological_sit_ups_1min_sec
     *
     * @return integer 
     */
    public function getPhysiologicalSitUps1minSec()
    {
        return $this->physiological_sit_ups_1min_sec;
    }

    /**
     * Set physiological_row_500m_min
     *
     * @param integer $physiologicalRow500mMin
     * @return ClientTest
     */
    public function setPhysiologicalRow500mMin($physiologicalRow500mMin)
    {
        $this->physiological_row_500m_min = $physiologicalRow500mMin;
    
        return $this;
    }

    /**
     * Get physiological_row_500m_min
     *
     * @return integer 
     */
    public function getPhysiologicalRow500mMin()
    {
        return $this->physiological_row_500m_min;
    }

    /**
     * Set physiological_row_500m_sec
     *
     * @param integer $physiologicalRow500mSec
     * @return ClientTest
     */
    public function setPhysiologicalRow500mSec($physiologicalRow500mSec)
    {
        $this->physiological_row_500m_sec = $physiologicalRow500mSec;
    
        return $this;
    }

    /**
     * Get physiological_row_500m_sec
     *
     * @return integer 
     */
    public function getPhysiologicalRow500mSec()
    {
        return $this->physiological_row_500m_sec;
    }

    /**
     * Set physiological_standing_broad_jump
     *
     * @param float $physiologicalStandingBroadJump
     * @return ClientTest
     */
    public function setPhysiologicalStandingBroadJump($physiologicalStandingBroadJump)
    {
        $this->physiological_standing_broad_jump = $physiologicalStandingBroadJump;
    
        return $this;
    }

    /**
     * Get physiological_standing_broad_jump
     *
     * @return float 
     */
    public function getPhysiologicalStandingBroadJump()
    {
        return $this->physiological_standing_broad_jump;
    }

    /**
     * Set physiological_time_trial_option
     *
     * @param float $physiologicalTimeTrialOption
     * @return ClientTest
     */
    public function setPhysiologicalTimeTrialOption($physiologicalTimeTrialOption)
    {
        $this->physiological_time_trial_option = $physiologicalTimeTrialOption;
    
        return $this;
    }

    /**
     * Get physiological_time_trial_option
     *
     * @return float 
     */
    public function getPhysiologicalTimeTrialOption()
    {
        return $this->physiological_time_trial_option;
    }

    /**
     * Set physiological_time_trial_min
     *
     * @param float $physiologicalTimeTrialMin
     * @return ClientTest
     */
    public function setPhysiologicalTimeTrialMin($physiologicalTimeTrialMin)
    {
        $this->physiological_time_trial_min = $physiologicalTimeTrialMin;
    
        return $this;
    }

    /**
     * Get physiological_time_trial_min
     *
     * @return float 
     */
    public function getPhysiologicalTimeTrialMin()
    {
        return $this->physiological_time_trial_min;
    }

    /**
     * Set physiological_time_trial_sec
     *
     * @param float $physiologicalTimeTrialSec
     * @return ClientTest
     */
    public function setPhysiologicalTimeTrialSec($physiologicalTimeTrialSec)
    {
        $this->physiological_time_trial_sec = $physiologicalTimeTrialSec;
    
        return $this;
    }

    /**
     * Get physiological_time_trial_sec
     *
     * @return float 
     */
    public function getPhysiologicalTimeTrialSec()
    {
        return $this->physiological_time_trial_sec;
    }
}