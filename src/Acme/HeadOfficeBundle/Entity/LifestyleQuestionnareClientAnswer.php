<?php

namespace Acme\HeadOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LifestyleQuestionnareClientAnswer
 *
 * @ORM\Table(name="tbl_lifestyle_questionnaire_client_answer")
 * @ORM\Entity
 */
class LifestyleQuestionnareClientAnswer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="lqca_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $lqca_id;

    /**
     * @var integer
     *
     * @ORM\Column(name="lq_id", type="integer")
     */
    private $lq_id;

    /**
     * @var integer
     *
     * @ORM\Column(name="client_test_id", type="integer")
     */
    private $client_test_id;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="club_client_id", type="integer")
     */
    private $club_client_id;

    /**
     * @var string
     *
     * @ORM\Column(name="answer", type="string", length=11)
     */
    private $answer;

    /**
     * @var string
     *
     * @ORM\Column(name="answer_datetime", type="string", length=20)
     */
    private $answer_datetime;
    
    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=10)
     */
    private $status;



    /**
     * Get lqca_id
     *
     * @return integer 
     */
    public function getLqcaId()
    {
        return $this->lqca_id;
    }

    /**
     * Set lq_id
     *
     * @param integer $lqId
     * @return LifestyleQuestionnareClientAnswer
     */
    public function setLqId($lqId)
    {
        $this->lq_id = $lqId;
    
        return $this;
    }

    /**
     * Get lq_id
     *
     * @return integer 
     */
    public function getLqId()
    {
        return $this->lq_id;
    }

    /**
     * Set client_test_id
     *
     * @param integer $clientTestId
     * @return LifestyleQuestionnareClientAnswer
     */
    public function setClientTestId($clientTestId)
    {
        $this->client_test_id = $clientTestId;
    
        return $this;
    }

    /**
     * Get client_test_id
     *
     * @return integer 
     */
    public function getClientTestId()
    {
        return $this->client_test_id;
    }

    /**
     * Set club_client_id
     *
     * @param integer $clubClientId
     * @return LifestyleQuestionnareClientAnswer
     */
    public function setClubClientId($clubClientId)
    {
        $this->club_client_id = $clubClientId;
    
        return $this;
    }

    /**
     * Get club_client_id
     *
     * @return integer 
     */
    public function getClubClientId()
    {
        return $this->club_client_id;
    }

    /**
     * Set answer
     *
     * @param integer $answer
     * @return LifestyleQuestionnareClientAnswer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    
        return $this;
    }

    /**
     * Get answer
     *
     * @return integer 
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set answer_datetime
     *
     * @param string $answerDatetime
     * @return LifestyleQuestionnareClientAnswer
     */
    public function setAnswerDatetime($answerDatetime)
    {
        $this->answer_datetime = $answerDatetime;
    
        return $this;
    }

    /**
     * Get answer_datetime
     *
     * @return string 
     */
    public function getAnswerDatetime()
    {
        return $this->answer_datetime;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return LifestyleQuestionnareClientAnswer
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }
}