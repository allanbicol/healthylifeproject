<?php

namespace Acme\HeadOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SeenChatMessages
 *
 * @ORM\Table(name="tbl_seen_chat_messages")
 * @ORM\Entity
 */
class SeenChatMessages
{
    /**
     * @var integer
     *
     * @ORM\Column(name="seen_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $seen_id;

    /**
     * @var integer
     *
     * @ORM\Column(name="club_admin_id", type="integer")
     */
    private $club_admin_id;

    /**
     * @var string
     *
     * @ORM\Column(name="message_ids", type="string")
     */
    private $message_ids;



    /**
     * Get seen_id
     *
     * @return integer 
     */
    public function getSeenId()
    {
        return $this->seen_id;
    }

    /**
     * Set club_admin_id
     *
     * @param integer $clubAdminId
     * @return SeenChatMessages
     */
    public function setClubAdminId($clubAdminId)
    {
        $this->club_admin_id = $clubAdminId;
    
        return $this;
    }

    /**
     * Get club_admin_id
     *
     * @return integer 
     */
    public function getClubAdminId()
    {
        return $this->club_admin_id;
    }

    /**
     * Set message_ids
     *
     * @param string $messageIds
     * @return SeenChatMessages
     */
    public function setMessageIds($messageIds)
    {
        $this->message_ids = $messageIds;
    
        return $this;
    }

    /**
     * Get message_ids
     *
     * @return string 
     */
    public function getMessageIds()
    {
        return $this->message_ids;
    }
}