<?php

namespace Acme\HeadOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * ClubAdmin
 *
 * @ORM\Table(name="tbl_club_admin")
 * @ORM\Entity
 * @UniqueEntity(
 *      fields={"email", "club_id"},
 *      message="Email is already used"
 * )
 * @ORM\Entity
 */
class ClubAdmin
{
    /**
     * @var integer
     *
     * @ORM\Column(name="club_admin_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $club_admin_id;

    /**
     * @var integer
     *
     * @ORM\Column(name="club_id", type="integer")
     */
    private $club_id;

    /**
     * @var string
     * @Assert\NotBlank(message="Role must not be blank.")
     * @ORM\Column(name="role", type="string", length=20)
     */
    private $role;
    
    /**
     * @var string
     * @Assert\NotBlank(message="First name must not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "100",
     *      minMessage = "First name must be at least {{ limit }} characters length",
     *      maxMessage = "First name cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="fname", type="string", length=100)
     */
    private $fname;

    /**
     * @var string
     * @Assert\NotBlank(message="Last name must not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "100",
     *      minMessage = "Last name must be at least {{ limit }} characters length",
     *      maxMessage = "Last name cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="lname", type="string", length=100)
     */
    private $lname;

    /**
     * @var string
     * @Assert\NotBlank(message="Email must not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "100",
     *      minMessage = "Email must be at least {{ limit }} characters length",
     *      maxMessage = "Email cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="email", type="string", length=100)
     */
    private $email;

    /**
     * @var string
     * @Assert\NotBlank(message="Password should not be blank.")
     * @ORM\Column(name="password", type="string", length=100)
     */
    private $password;

    /**
     * @var string
     * @Assert\NotBlank(message="Status should not be blank.")
     * @ORM\Column(name="status", type="string", length=10)
     */
    private $status;


    /**
     * @var string
     * 
     * @ORM\Column(name="last_login_datetime", type="string", length=20)
     */
    private $last_login_datetime;
    

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=100)
     */
    private $token;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="s_login", type="integer")
     */
    private $s_login;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="show_first_login_msg", type="integer")
     */
    private $show_first_login_msg;
    
    /**
     * @var string
     *
     * @ORM\Column(name="report_column_filter", type="string")
     */
    private $report_column_filter;

    /**
     * @var string
     *
     * @ORM\Column(name="registered_date", type="string", length=20)
     */
    private $registered_date;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="s_aia_approved", type="integer")
     */
    private $s_aia_approved;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="s_aia_approved_by_admin", type="integer")
     */
    private $s_aia_approved_by_admin;

    /**
     * Get club_admin_id
     *
     * @return integer 
     */
    public function getClubAdminId()
    {
        return $this->club_admin_id;
    }

    /**
     * Set club_id
     *
     * @param integer $clubId
     * @return ClubAdmin
     */
    public function setClubId($clubId)
    {
        $this->club_id = $clubId;
    
        return $this;
    }

    /**
     * Get club_id
     *
     * @return integer 
     */
    public function getClubId()
    {
        return $this->club_id;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return ClubAdmin
     */
    public function setRole($role)
    {
        $this->role = $role;
    
        return $this;
    }

    /**
     * Get role
     *
     * @return string 
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set fname
     *
     * @param string $fname
     * @return ClubAdmin
     */
    public function setFname($fname)
    {
        $this->fname = $fname;
    
        return $this;
    }

    /**
     * Get fname
     *
     * @return string 
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set lname
     *
     * @param string $lname
     * @return ClubAdmin
     */
    public function setLname($lname)
    {
        $this->lname = $lname;
    
        return $this;
    }

    /**
     * Get lname
     *
     * @return string 
     */
    public function getLname()
    {
        return $this->lname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return ClubAdmin
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return ClubAdmin
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return ClubAdmin
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set last_login_datetime
     *
     * @param string $lastLoginDatetime
     * @return ClubAdmin
     */
    public function setLastLoginDatetime($lastLoginDatetime)
    {
        $this->last_login_datetime = $lastLoginDatetime;
    
        return $this;
    }

    /**
     * Get last_login_datetime
     *
     * @return string 
     */
    public function getLastLoginDatetime()
    {
        return $this->last_login_datetime;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return ClubAdmin
     */
    public function setToken($token)
    {
        $this->token = $token;
    
        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set s_login
     *
     * @param integer $sLogin
     * @return ClubAdmin
     */
    public function setSLogin($sLogin)
    {
        $this->s_login = $sLogin;
    
        return $this;
    }

    /**
     * Get s_login
     *
     * @return integer 
     */
    public function getSLogin()
    {
        return $this->s_login;
    }

    /**
     * Set show_first_login_msg
     *
     * @param integer $showFirstLoginMsg
     * @return ClubAdmin
     */
    public function setShowFirstLoginMsg($showFirstLoginMsg)
    {
        $this->show_first_login_msg = $showFirstLoginMsg;
    
        return $this;
    }

    /**
     * Get show_first_login_msg
     *
     * @return integer 
     */
    public function getShowFirstLoginMsg()
    {
        return $this->show_first_login_msg;
    }

    /**
     * Set report_column_filter
     *
     * @param string $reportColumnFilter
     * @return ClubAdmin
     */
    public function setReportColumnFilter($reportColumnFilter)
    {
        $this->report_column_filter = $reportColumnFilter;
    
        return $this;
    }

    /**
     * Get report_column_filter
     *
     * @return string 
     */
    public function getReportColumnFilter()
    {
        return $this->report_column_filter;
    }

    /**
     * Set registered_date
     *
     * @param string $registeredDate
     * @return ClubAdmin
     */
    public function setRegisteredDate($registeredDate)
    {
        $this->registered_date = $registeredDate;
    
        return $this;
    }

    /**
     * Get registered_date
     *
     * @return string 
     */
    public function getRegisteredDate()
    {
        return $this->registered_date;
    }

    /**
     * Set s_aia_approved
     *
     * @param integer $sAiaApproved
     * @return ClubAdmin
     */
    public function setSAiaApproved($sAiaApproved)
    {
        $this->s_aia_approved = $sAiaApproved;
    
        return $this;
    }

    /**
     * Get s_aia_approved
     *
     * @return integer 
     */
    public function getSAiaApproved()
    {
        return $this->s_aia_approved;
    }

    /**
     * Set s_aia_approved_by_admin
     *
     * @param integer $sAiaApprovedByAdmin
     * @return ClubAdmin
     */
    public function setSAiaApprovedByAdmin($sAiaApprovedByAdmin)
    {
        $this->s_aia_approved_by_admin = $sAiaApprovedByAdmin;
    
        return $this;
    }

    /**
     * Get s_aia_approved_by_admin
     *
     * @return integer 
     */
    public function getSAiaApprovedByAdmin()
    {
        return $this->s_aia_approved_by_admin;
    }
}