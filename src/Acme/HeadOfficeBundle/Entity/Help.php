<?php

namespace Acme\HeadOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Help
 *
 * @ORM\Table(name="tbl_help")
 * @ORM\Entity
 */
class Help
{
    /**
     * @var integer
     *
     * @ORM\Column(name="help_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $help_id;

    /**
     * @var string
     *
     * @ORM\Column(name="field", type="string", length=50)
     */
    private $field;

    /**
     * @var string
     *
     * @ORM\Column(name="html", type="string")
     */
    private $html;



    /**
     * Get help_id
     *
     * @return integer 
     */
    public function getHelpId()
    {
        return $this->help_id;
    }

    /**
     * Set field
     *
     * @param string $field
     * @return Help
     */
    public function setField($field)
    {
        $this->field = $field;
    
        return $this;
    }

    /**
     * Get field
     *
     * @return string 
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set html
     *
     * @param string $html
     * @return Help
     */
    public function setHtml($html)
    {
        $this->html = $html;
    
        return $this;
    }

    /**
     * Get html
     *
     * @return string 
     */
    public function getHtml()
    {
        return $this->html;
    }
}