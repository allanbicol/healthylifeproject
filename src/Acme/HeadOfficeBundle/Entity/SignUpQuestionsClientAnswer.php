<?php

namespace Acme\HeadOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SignUpQuestionsClientAnswer
 *
 * @ORM\Table(name="tbl_signup_questions_client_answer")
 * @ORM\Entity
 */
class SignUpQuestionsClientAnswer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="club_client_id", type="integer")
     */
    private $club_client_id;

    /**
     * @var integer
     *
     * @ORM\Column(name="sq_id", type="integer")
     */
    private $sq_id;

    /**
     * @var string
     *
     * @ORM\Column(name="answer", type="string", length=20)
     */
    private $answer;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set club_client_id
     *
     * @param integer $clubClientId
     * @return SignUpQuestionsClientAnswer
     */
    public function setClubClientId($clubClientId)
    {
        $this->club_client_id = $clubClientId;
    
        return $this;
    }

    /**
     * Get club_client_id
     *
     * @return integer 
     */
    public function getClubClientId()
    {
        return $this->club_client_id;
    }

    /**
     * Set sq_id
     *
     * @param integer $sqId
     * @return SignUpQuestionsClientAnswer
     */
    public function setSqId($sqId)
    {
        $this->sq_id = $sqId;
    
        return $this;
    }

    /**
     * Get sq_id
     *
     * @return integer 
     */
    public function getSqId()
    {
        return $this->sq_id;
    }

    /**
     * Set answer
     *
     * @param string $answer
     * @return SignUpQuestionsClientAnswer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    
        return $this;
    }

    /**
     * Get answer
     *
     * @return string 
     */
    public function getAnswer()
    {
        return $this->answer;
    }
}