<?php

namespace Acme\HeadOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * HeadOfficeAdmin
 *
 * @ORM\Table(name="tbl_head_office_admin")
 * @ORM\Entity
 * @UniqueEntity(
 *      fields={"email"},
 *      message="Email is already used"
 * )
 */
class HeadOfficeAdmin
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ho_admin_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $ho_admin_id;

    /**
     * @var string
     * @Assert\NotBlank(message="First name must not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "100",
     *      minMessage = "First name must be at least {{ limit }} characters length",
     *      maxMessage = "First name cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="fname", type="string", length=100)
     */
    private $fname;

    /**
     * @var string
     * @Assert\NotBlank(message="Last name must not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "100",
     *      minMessage = "Last name must be at least {{ limit }} characters length",
     *      maxMessage = "Last name cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="lname", type="string", length=100)
     */
    private $lname;

    /**
     * @var string
     * @Assert\NotBlank(message="Email must not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "100",
     *      minMessage = "Email must be at least {{ limit }} characters length",
     *      maxMessage = "Email cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="email", type="string", length=100)
     */
    private $email;

    /**
     * @var string
     * @Assert\NotBlank(message="Password should not be blank.")
     * @ORM\Column(name="password", type="string", length=100)
     */
    private $password;

    /**
     * @var string
     * @Assert\NotBlank(message="Status should not be blank.")
     * @ORM\Column(name="status", type="string", length=10)
     */
    private $status;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="last_login_datetime", type="string", length=20)
     */
    private $last_login_datetime;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="receive_litetests", type="integer")
     */
    private $receive_litetests;
    
    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=100)
     */
    private $token;

    /**
     * @var string
     *
     * @ORM\Column(name="report_column_filter", type="string")
     */
    private $report_column_filter;

    /**
     * Get ho_admin_id
     *
     * @return integer 
     */
    public function getHoAdminId()
    {
        return $this->ho_admin_id;
    }

    /**
     * Set fname
     *
     * @param string $fname
     * @return HeadOfficeAdmin
     */
    public function setFname($fname)
    {
        $this->fname = $fname;
    
        return $this;
    }

    /**
     * Get fname
     *
     * @return string 
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set lname
     *
     * @param string $lname
     * @return HeadOfficeAdmin
     */
    public function setLname($lname)
    {
        $this->lname = $lname;
    
        return $this;
    }

    /**
     * Get lname
     *
     * @return string 
     */
    public function getLname()
    {
        return $this->lname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return HeadOfficeAdmin
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return HeadOfficeAdmin
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return HeadOfficeAdmin
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set last_login_datetime
     *
     * @param string $lastLoginDatetime
     * @return HeadOfficeAdmin
     */
    public function setLastLoginDatetime($lastLoginDatetime)
    {
        $this->last_login_datetime = $lastLoginDatetime;
    
        return $this;
    }

    /**
     * Get last_login_datetime
     *
     * @return string 
     */
    public function getLastLoginDatetime()
    {
        return $this->last_login_datetime;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return HeadOfficeAdmin
     */
    public function setToken($token)
    {
        $this->token = $token;
    
        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set receive_litetests
     *
     * @param integer $receiveLitetests
     * @return HeadOfficeAdmin
     */
    public function setReceiveLitetests($receiveLitetests)
    {
        $this->receive_litetests = $receiveLitetests;
    
        return $this;
    }

    /**
     * Get receive_litetests
     *
     * @return integer 
     */
    public function getReceiveLitetests()
    {
        return $this->receive_litetests;
    }

    /**
     * Set report_column_filter
     *
     * @param string $reportColumnFilter
     * @return HeadOfficeAdmin
     */
    public function setReportColumnFilter($reportColumnFilter)
    {
        $this->report_column_filter = $reportColumnFilter;
    
        return $this;
    }

    /**
     * Get report_column_filter
     *
     * @return string 
     */
    public function getReportColumnFilter()
    {
        return $this->report_column_filter;
    }
}