<?php

namespace Acme\HeadOfficeBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;

/**
 * Club
 *
 * @ORM\Table(name="tbl_club")
 * @ORM\Entity
 */
class Club
{
    /**
     * @var integer
     *
     * @ORM\Column(name="club_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $club_id;

    /**
     * @var string
     * @Assert\NotBlank(message="Club name must not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "100",
     *      minMessage = "Club name must be at least {{ limit }} characters length",
     *      maxMessage = "Club name cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="club_name", type="string", length=250)
     */
    private $club_name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string")
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=250)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", length=50)
     */
    private $postcode;
    
    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=50)
     */
    private $state;
    
    /**
     * @var string
     *
     * @ORM\Column(name="lam", type="string", length=100)
     */
    private $lam;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=1000)
     */
    private $country;

    /**
     * @var float
     *
     * @ORM\Column(name="available_credit", type="float")
     */
    private $available_credit;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=10)
     */
    private $status;
    
    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="string", length=50)
     */
    private $latitude;
    
    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="string", length=50)
     */
    private $longitude;
    
    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string")
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="registered_date", type="string", length=20)
     */
    private $registered_date;
    
    /**
     * @var string
     *
     * @ORM\Column(name="payment_option", type="string", length=10)
     */
    private $payment_option;

    /**
     * @var string
     *
     * @ORM\Column(name="pin_payment_customer_token", type="string", length=250)
     */
    private $pin_payment_customer_token;

    /**
     * @var string
     *
     * @ORM\Column(name="card_number", type="string", length=100)
     */
    private $card_number;
    
    /**
     * @var string
     *
     * @ORM\Column(name="card_expiry_month", type="string", length=10)
     */
    private $card_expiry_month;
    
    /**
     * @var string
     *
     * @ORM\Column(name="card_expiry_year", type="string", length=10)
     */
    private $card_expiry_year;
    
    /**
     * @var string
     *
     * @ORM\Column(name="card_cvc", type="string", length=10)
     */
    private $card_cvc;
    
    /**
     * @var string
     *
     * @ORM\Column(name="card_name", type="string", length=250)
     */
    private $card_name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="card_email", type="string", length=100)
     */
    private $card_email;
    
    /**
     * @var string
     *
     * @ORM\Column(name="card_address_line1", type="string", length=500)
     */
    private $card_address_line1;
    
    /**
     * @var string
     *
     * @ORM\Column(name="card_address_city", type="string", length=10)
     */
    private $card_address_city;
    
    /**
     * @var string
     *
     * @ORM\Column(name="card_address_postcode", type="string", length=100)
     */
    private $card_address_postcode;
    
    /**
     * @var string
     *
     * @ORM\Column(name="card_address_state", type="string", length=100)
     */
    private $card_address_state;
    
    /**
     * @var string
     *
     * @ORM\Column(name="card_address_country", type="string", length=10)
     */
    private $card_address_country;
    
    /**
     * @var string
     *
     * @ORM\Column(name="monthly_fee_status", type="string", length=10)
     */
    private $monthly_fee_status;
    
    
    /**
     * @var string
     * 
     * @ORM\Column(name="last_login_datetime", type="string", length=20)
     */
    private $last_login_datetime;

    

    /**
     * Get club_id
     *
     * @return integer 
     */
    public function getClubId()
    {
        return $this->club_id;
    }

    /**
     * Set club_name
     *
     * @param string $clubName
     * @return Club
     */
    public function setClubName($clubName)
    {
        $this->club_name = $clubName;
    
        return $this;
    }

    /**
     * Get club_name
     *
     * @return string 
     */
    public function getClubName()
    {
        return $this->club_name;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Club
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Club
     */
    public function setCity($city)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     * @return Club
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    
        return $this;
    }

    /**
     * Get postcode
     *
     * @return string 
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return Club
     */
    public function setState($state)
    {
        $this->state = $state;
    
        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set lam
     *
     * @param string $lam
     * @return Club
     */
    public function setLam($lam)
    {
        $this->lam = $lam;
    
        return $this;
    }

    /**
     * Get lam
     *
     * @return string 
     */
    public function getLam()
    {
        return $this->lam;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Club
     */
    public function setCountry($country)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set available_credit
     *
     * @param float $availableCredit
     * @return Club
     */
    public function setAvailableCredit($availableCredit)
    {
        $this->available_credit = $availableCredit;
    
        return $this;
    }

    /**
     * Get available_credit
     *
     * @return float 
     */
    public function getAvailableCredit()
    {
        return $this->available_credit;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Club
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     * @return Club
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    
        return $this;
    }

    /**
     * Get latitude
     *
     * @return string 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     * @return Club
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    
        return $this;
    }

    /**
     * Get longitude
     *
     * @return string 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Club
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    
        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set registered_date
     *
     * @param string $registeredDate
     * @return Club
     */
    public function setRegisteredDate($registeredDate)
    {
        $this->registered_date = $registeredDate;
    
        return $this;
    }

    /**
     * Get registered_date
     *
     * @return string 
     */
    public function getRegisteredDate()
    {
        return $this->registered_date;
    }

    /**
     * Set payment_option
     *
     * @param string $paymentOption
     * @return Club
     */
    public function setPaymentOption($paymentOption)
    {
        $this->payment_option = $paymentOption;
    
        return $this;
    }

    /**
     * Get payment_option
     *
     * @return string 
     */
    public function getPaymentOption()
    {
        return $this->payment_option;
    }

    /**
     * Set pin_payment_customer_token
     *
     * @param string $pinPaymentCustomerToken
     * @return Club
     */
    public function setPinPaymentCustomerToken($pinPaymentCustomerToken)
    {
        $this->pin_payment_customer_token = $pinPaymentCustomerToken;
    
        return $this;
    }

    /**
     * Get pin_payment_customer_token
     *
     * @return string 
     */
    public function getPinPaymentCustomerToken()
    {
        return $this->pin_payment_customer_token;
    }

    /**
     * Set card_number
     *
     * @param string $cardNumber
     * @return Club
     */
    public function setCardNumber($cardNumber)
    {
        $this->card_number = $cardNumber;
    
        return $this;
    }

    /**
     * Get card_number
     *
     * @return string 
     */
    public function getCardNumber()
    {
        return $this->card_number;
    }

    /**
     * Set card_expiry_month
     *
     * @param string $cardExpiryMonth
     * @return Club
     */
    public function setCardExpiryMonth($cardExpiryMonth)
    {
        $this->card_expiry_month = $cardExpiryMonth;
    
        return $this;
    }

    /**
     * Get card_expiry_month
     *
     * @return string 
     */
    public function getCardExpiryMonth()
    {
        return $this->card_expiry_month;
    }

    /**
     * Set card_expiry_year
     *
     * @param string $cardExpiryYear
     * @return Club
     */
    public function setCardExpiryYear($cardExpiryYear)
    {
        $this->card_expiry_year = $cardExpiryYear;
    
        return $this;
    }

    /**
     * Get card_expiry_year
     *
     * @return string 
     */
    public function getCardExpiryYear()
    {
        return $this->card_expiry_year;
    }

    /**
     * Set card_cvc
     *
     * @param string $cardCvc
     * @return Club
     */
    public function setCardCvc($cardCvc)
    {
        $this->card_cvc = $cardCvc;
    
        return $this;
    }

    /**
     * Get card_cvc
     *
     * @return string 
     */
    public function getCardCvc()
    {
        return $this->card_cvc;
    }

    /**
     * Set card_name
     *
     * @param string $cardName
     * @return Club
     */
    public function setCardName($cardName)
    {
        $this->card_name = $cardName;
    
        return $this;
    }

    /**
     * Get card_name
     *
     * @return string 
     */
    public function getCardName()
    {
        return $this->card_name;
    }

    /**
     * Set card_email
     *
     * @param string $cardEmail
     * @return Club
     */
    public function setCardEmail($cardEmail)
    {
        $this->card_email = $cardEmail;
    
        return $this;
    }

    /**
     * Get card_email
     *
     * @return string 
     */
    public function getCardEmail()
    {
        return $this->card_email;
    }

    /**
     * Set card_address_line1
     *
     * @param string $cardAddressLine1
     * @return Club
     */
    public function setCardAddressLine1($cardAddressLine1)
    {
        $this->card_address_line1 = $cardAddressLine1;
    
        return $this;
    }

    /**
     * Get card_address_line1
     *
     * @return string 
     */
    public function getCardAddressLine1()
    {
        return $this->card_address_line1;
    }

    /**
     * Set card_address_city
     *
     * @param string $cardAddressCity
     * @return Club
     */
    public function setCardAddressCity($cardAddressCity)
    {
        $this->card_address_city = $cardAddressCity;
    
        return $this;
    }

    /**
     * Get card_address_city
     *
     * @return string 
     */
    public function getCardAddressCity()
    {
        return $this->card_address_city;
    }

    /**
     * Set card_address_postcode
     *
     * @param string $cardAddressPostcode
     * @return Club
     */
    public function setCardAddressPostcode($cardAddressPostcode)
    {
        $this->card_address_postcode = $cardAddressPostcode;
    
        return $this;
    }

    /**
     * Get card_address_postcode
     *
     * @return string 
     */
    public function getCardAddressPostcode()
    {
        return $this->card_address_postcode;
    }

    /**
     * Set card_address_state
     *
     * @param string $cardAddressState
     * @return Club
     */
    public function setCardAddressState($cardAddressState)
    {
        $this->card_address_state = $cardAddressState;
    
        return $this;
    }

    /**
     * Get card_address_state
     *
     * @return string 
     */
    public function getCardAddressState()
    {
        return $this->card_address_state;
    }

    /**
     * Set card_address_country
     *
     * @param string $cardAddressCountry
     * @return Club
     */
    public function setCardAddressCountry($cardAddressCountry)
    {
        $this->card_address_country = $cardAddressCountry;
    
        return $this;
    }

    /**
     * Get card_address_country
     *
     * @return string 
     */
    public function getCardAddressCountry()
    {
        return $this->card_address_country;
    }

    /**
     * Set monthly_fee_status
     *
     * @param string $monthlyFeeStatus
     * @return Club
     */
    public function setMonthlyFeeStatus($monthlyFeeStatus)
    {
        $this->monthly_fee_status = $monthlyFeeStatus;
    
        return $this;
    }

    /**
     * Get monthly_fee_status
     *
     * @return string 
     */
    public function getMonthlyFeeStatus()
    {
        return $this->monthly_fee_status;
    }

    /**
     * Set last_login_datetime
     *
     * @param string $lastLoginDatetime
     * @return Club
     */
    public function setLastLoginDatetime($lastLoginDatetime)
    {
        $this->last_login_datetime = $lastLoginDatetime;
    
        return $this;
    }

    /**
     * Get last_login_datetime
     *
     * @return string 
     */
    public function getLastLoginDatetime()
    {
        return $this->last_login_datetime;
    }
}