<?php

namespace Acme\HeadOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * LiteTest
 *
 * @ORM\Table(name="tbl_lite_test")
 * @ORM\Entity
 */
class LiteTest
{
    /**
     * @var integer
     *
     * @ORM\Column(name="lite_test_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $lite_test_id;

    /**
     * @var string
     * @Assert\NotBlank(message="First name must not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "100",
     *      minMessage = "First name must be at least {{ limit }} characters length",
     *      maxMessage = "First name cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="fname", type="string", length=100)
     */
    private $fname;

    /**
     * @var string
     * @Assert\NotBlank(message="Last name must not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "100",
     *      minMessage = "Last name must be at least {{ limit }} characters length",
     *      maxMessage = "Last name cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="lname", type="string", length=100)
     */
    private $lname;

    /**
     * @var string
     * @Assert\NotBlank(message="Email must not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "100",
     *      minMessage = "Email must be at least {{ limit }} characters length",
     *      maxMessage = "Email cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="email", type="string", length=100)
     */
    private $email;
    
    /**
     * @var string
     * @Assert\NotBlank(message="Gender must not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "10",
     *      minMessage = "Gender must be at least {{ limit }} characters length",
     *      maxMessage = "Gender cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="gender", type="string", length=10)
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="test_datetime", type="string", length=50)
     */
    private $test_datetime;
    
    /**
     * @var string
     *
     * @ORM\Column(name="test_datetime_finalised", type="string", length=50)
     */
    private $test_datetime_finalised;

    /**
     * @var integer
     *
     * @ORM\Column(name="age", type="integer")
     */
    private $age;
    
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_expectancy_male", type="float")
     */
    private $healthy_life_expectancy_male;
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_expectancy_female", type="float")
     */
    private $healthy_life_expectancy_female;
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_expectancy_total_male", type="float")
     */
    private $healthy_life_expectancy_total_male;
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_expectancy_total_female", type="float")
     */
    private $healthy_life_expectancy_total_female;
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_expectancy_dalys_male", type="float")
     */
    private $healthy_life_expectancy_dalys_male;
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_expectancy_dalys_female", type="float")
     */
    private $healthy_life_expectancy_dalys_female;
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_max_score_male", type="float")
     */
    private $healthy_life_max_score_male;
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_max_score_female", type="float")
     */
    private $healthy_life_max_score_female;
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_worst_score_male", type="float")
     */
    private $healthy_life_worst_score_male;
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_worst_score_female", type="float")
     */
    private $healthy_life_worst_score_female;
    

    /**
     * @var float
     *
     * @ORM\Column(name="bmi", type="float")
     */
    private $bmi;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="height", type="integer")
     */
    private $height;

    /**
     * @var integer
     *
     * @ORM\Column(name="weight", type="integer")
     */
    private $weight;

    /**
     * @var integer
     *
     * @ORM\Column(name="lifestyle_physical_activity_q1", type="integer")
     */
    private $lifestyle_physical_activity_q1;

    /**
     * @var integer
     *
     * @ORM\Column(name="lifestyle_physical_activity_q2", type="integer")
     */
    private $lifestyle_physical_activity_q2;
    
    /**
     * @var float
     *
     * @ORM\Column(name="lifestyle_physical_activity", type="float")
     */
    private $lifestyle_physical_activity;

    /**
     * @var float
     *
     * @ORM\Column(name="lifestyle_smoking", type="float")
     */
    private $lifestyle_smoking;

    /**
     * @var integer
     *
     * @ORM\Column(name="lifestyle_alcohol_q1", type="integer")
     */
    private $lifestyle_alcohol_q1;

    /**
     * @var integer
     *
     * @ORM\Column(name="lifestyle_alcohol_q2", type="integer")
     */
    private $lifestyle_alcohol_q2;

    /**
     * @var float
     *
     * @ORM\Column(name="lifestyle_alcohol", type="float")
     */
    private $lifestyle_alcohol;
    
    /**
     * @var float
     *
     * @ORM\Column(name="lifestyle_nutrition", type="float")
     */
    private $lifestyle_nutrition;

    /**
     * @var float
     *
     * @ORM\Column(name="lifestyle_mental_health", type="float")
     */
    private $lifestyle_mental_health;

    /**
     * @var float
     *
     * @ORM\Column(name="lifestyle_risk_profile", type="float")
     */
    private $lifestyle_risk_profile;

    /**
     * @var string
     *
     * @ORM\Column(name="physiological_vo2_test_type", type="string", length=100)
     */
    private $physiological_vo2_test_type;

    /**
     * @var integer
     *
     * @ORM\Column(name="physiological_vo2_test_value1", type="integer")
     */
    private $physiological_vo2_test_value1;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="physiological_vo2_test_value2", type="integer")
     */
    private $physiological_vo2_test_value2;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_vo2", type="float")
     */
    private $physiological_vo2;


    /**
     * @var string
     *
     * @ORM\Column(name="physiological_balance_type", type="string", length=100)
     */
    private $physiological_balance_type;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="physiological_balance_value", type="integer")
     */
    private $physiological_balance_value;

    /**
     * @var float
     *
     * @ORM\Column(name="physiological_balance", type="float")
     */
    private $physiological_balance;

    /**
     * @var integer
     *
     * @ORM\Column(name="physiological_squat_value", type="integer")
     */
    private $physiological_squat_value;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_squat", type="float")
     */
    private $physiological_squat;

    /**
     * @var integer
     *
     * @ORM\Column(name="physiological_situp_value", type="integer")
     */
    private $physiological_situp_value;
    
    /**
     * @var float
     *
     * @ORM\Column(name="physiological_situp", type="float")
     */
    private $physiological_situp;

    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_waist_value", type="integer")
     */
    private $biometric_waist_value;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_waist_value_lost", type="integer")
     */
    private $biometric_waist_value_lost;
    
    /**
     * @var float
     *
     * @ORM\Column(name="biometric_waist", type="float")
     */
    private $biometric_waist;

    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_chest", type="integer")
     */
    private $biometric_chest;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_chest_lost", type="integer")
     */
    private $biometric_chest_lost;

    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_abdomen", type="integer")
     */
    private $biometric_abdomen;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_abdomen_lost", type="integer")
     */
    private $biometric_abdomen_lost;

    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_hip", type="integer")
     */
    private $biometric_hip;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_hip_lost", type="integer")
     */
    private $biometric_hip_lost;

    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_thigh", type="integer")
     */
    private $biometric_thigh;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_thigh_lost", type="integer")
     */
    private $biometric_thigh_lost;

    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_arm", type="integer")
     */
    private $biometric_arm;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_arm_lost", type="integer")
     */
    private $biometric_arm_lost;

    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_body_fat_value", type="integer")
     */
    private $biometric_body_fat_value;
    
    /**
     * @var float
     *
     * @ORM\Column(name="biometric_body_fat", type="float")
     */
    private $biometric_body_fat;

    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_blood_pressure_systolic", type="integer")
     */
    private $biometric_blood_pressure_systolic;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_blood_pressure_diastolic", type="integer")
     */
    private $biometric_blood_pressure_diastolic;
    
    /**
     * @var float
     *
     * @ORM\Column(name="biometric_blood_pressure", type="float")
     */
    private $biometric_blood_pressure;

    /**
     * @var integer
     *
     * @ORM\Column(name="biometric_blood_oxygen_value", type="integer")
     */
    private $biometric_blood_oxygen_value;
    
    /**
     * @var float
     *
     * @ORM\Column(name="biometric_blood_oxygen", type="float")
     */
    private $biometric_blood_oxygen;
    
    
    
    
    
    
    /**
     * @var float
     *
     * @ORM\Column(name="total_crf", type="float")
     */
    private $total_crf;
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_points", type="float")
     */
    private $healthy_life_points;
    
    /**
     * @var float
     *
     * @ORM\Column(name="estimated_healthy_life_expectancy", type="float")
     */
    private $estimated_healthy_life_expectancy;
    
    /**
     * @var float
     *
     * @ORM\Column(name="max_hle", type="float")
     */
    private $max_hle;
    
    /**
     * @var float
     *
     * @ORM\Column(name="healthy_life_score", type="float")
     */
    private $healthy_life_score;
    
    
    /**
     * @var float
     *
     * @ORM\Column(name="estimated_years_of_healthy_life_left", type="float")
     */
    private $estimated_years_of_healthy_life_left;
    
    /**
     * @var float
     *
     * @ORM\Column(name="estimated_years_you_will_live_with_did", type="float")
     */
    private $estimated_years_you_will_live_with_did;
    
    /**
     * @var float
     *
     * @ORM\Column(name="estimated_years_you_will_live_with_did_as_opposed_to", type="float")
     */
    private $estimated_years_you_will_live_with_did_as_opposed_to;
    
    /**
     * @var float
     *
     * @ORM\Column(name="you_could_add_up_to", type="float")
     */
    private $you_could_add_up_to;
    
    /**
     * @var float
     *
     * @ORM\Column(name="estimated_current_healthy_life_age", type="float")
     */
    private $estimated_current_healthy_life_age;
    
    /**
     * @var float
     *
     * @ORM\Column(name="years_added", type="float")
     */
    private $years_added;
    
    /**
     * @var float
     *
     * @ORM\Column(name="cm_lost", type="float")
     */
    private $cm_lost;
    
    /**
     * @var float
     *
     * @ORM\Column(name="kg_lost", type="float")
     */
    private $kg_lost;
    

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=10)
     */
    private $status;



    /**
     * Get lite_test_id
     *
     * @return integer 
     */
    public function getLiteTestId()
    {
        return $this->lite_test_id;
    }

    /**
     * Set fname
     *
     * @param string $fname
     * @return LiteTest
     */
    public function setFname($fname)
    {
        $this->fname = $fname;
    
        return $this;
    }

    /**
     * Get fname
     *
     * @return string 
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set lname
     *
     * @param string $lname
     * @return LiteTest
     */
    public function setLname($lname)
    {
        $this->lname = $lname;
    
        return $this;
    }

    /**
     * Get lname
     *
     * @return string 
     */
    public function getLname()
    {
        return $this->lname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return LiteTest
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return LiteTest
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    
        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set test_datetime
     *
     * @param string $testDatetime
     * @return LiteTest
     */
    public function setTestDatetime($testDatetime)
    {
        $this->test_datetime = $testDatetime;
    
        return $this;
    }

    /**
     * Get test_datetime
     *
     * @return string 
     */
    public function getTestDatetime()
    {
        return $this->test_datetime;
    }

    /**
     * Set test_datetime_finalised
     *
     * @param string $testDatetimeFinalised
     * @return LiteTest
     */
    public function setTestDatetimeFinalised($testDatetimeFinalised)
    {
        $this->test_datetime_finalised = $testDatetimeFinalised;
    
        return $this;
    }

    /**
     * Get test_datetime_finalised
     *
     * @return string 
     */
    public function getTestDatetimeFinalised()
    {
        return $this->test_datetime_finalised;
    }

    /**
     * Set age
     *
     * @param integer $age
     * @return LiteTest
     */
    public function setAge($age)
    {
        $this->age = $age;
    
        return $this;
    }

    /**
     * Get age
     *
     * @return integer 
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set healthy_life_expectancy_male
     *
     * @param float $healthyLifeExpectancyMale
     * @return LiteTest
     */
    public function setHealthyLifeExpectancyMale($healthyLifeExpectancyMale)
    {
        $this->healthy_life_expectancy_male = $healthyLifeExpectancyMale;
    
        return $this;
    }

    /**
     * Get healthy_life_expectancy_male
     *
     * @return float 
     */
    public function getHealthyLifeExpectancyMale()
    {
        return $this->healthy_life_expectancy_male;
    }

    /**
     * Set healthy_life_expectancy_female
     *
     * @param float $healthyLifeExpectancyFemale
     * @return LiteTest
     */
    public function setHealthyLifeExpectancyFemale($healthyLifeExpectancyFemale)
    {
        $this->healthy_life_expectancy_female = $healthyLifeExpectancyFemale;
    
        return $this;
    }

    /**
     * Get healthy_life_expectancy_female
     *
     * @return float 
     */
    public function getHealthyLifeExpectancyFemale()
    {
        return $this->healthy_life_expectancy_female;
    }

    /**
     * Set healthy_life_expectancy_total_male
     *
     * @param float $healthyLifeExpectancyTotalMale
     * @return LiteTest
     */
    public function setHealthyLifeExpectancyTotalMale($healthyLifeExpectancyTotalMale)
    {
        $this->healthy_life_expectancy_total_male = $healthyLifeExpectancyTotalMale;
    
        return $this;
    }

    /**
     * Get healthy_life_expectancy_total_male
     *
     * @return float 
     */
    public function getHealthyLifeExpectancyTotalMale()
    {
        return $this->healthy_life_expectancy_total_male;
    }

    /**
     * Set healthy_life_expectancy_total_female
     *
     * @param float $healthyLifeExpectancyTotalFemale
     * @return LiteTest
     */
    public function setHealthyLifeExpectancyTotalFemale($healthyLifeExpectancyTotalFemale)
    {
        $this->healthy_life_expectancy_total_female = $healthyLifeExpectancyTotalFemale;
    
        return $this;
    }

    /**
     * Get healthy_life_expectancy_total_female
     *
     * @return float 
     */
    public function getHealthyLifeExpectancyTotalFemale()
    {
        return $this->healthy_life_expectancy_total_female;
    }

    /**
     * Set healthy_life_expectancy_dalys_male
     *
     * @param float $healthyLifeExpectancyDalysMale
     * @return LiteTest
     */
    public function setHealthyLifeExpectancyDalysMale($healthyLifeExpectancyDalysMale)
    {
        $this->healthy_life_expectancy_dalys_male = $healthyLifeExpectancyDalysMale;
    
        return $this;
    }

    /**
     * Get healthy_life_expectancy_dalys_male
     *
     * @return float 
     */
    public function getHealthyLifeExpectancyDalysMale()
    {
        return $this->healthy_life_expectancy_dalys_male;
    }

    /**
     * Set healthy_life_expectancy_dalys_female
     *
     * @param float $healthyLifeExpectancyDalysFemale
     * @return LiteTest
     */
    public function setHealthyLifeExpectancyDalysFemale($healthyLifeExpectancyDalysFemale)
    {
        $this->healthy_life_expectancy_dalys_female = $healthyLifeExpectancyDalysFemale;
    
        return $this;
    }

    /**
     * Get healthy_life_expectancy_dalys_female
     *
     * @return float 
     */
    public function getHealthyLifeExpectancyDalysFemale()
    {
        return $this->healthy_life_expectancy_dalys_female;
    }

    /**
     * Set healthy_life_max_score_male
     *
     * @param float $healthyLifeMaxScoreMale
     * @return LiteTest
     */
    public function setHealthyLifeMaxScoreMale($healthyLifeMaxScoreMale)
    {
        $this->healthy_life_max_score_male = $healthyLifeMaxScoreMale;
    
        return $this;
    }

    /**
     * Get healthy_life_max_score_male
     *
     * @return float 
     */
    public function getHealthyLifeMaxScoreMale()
    {
        return $this->healthy_life_max_score_male;
    }

    /**
     * Set healthy_life_max_score_female
     *
     * @param float $healthyLifeMaxScoreFemale
     * @return LiteTest
     */
    public function setHealthyLifeMaxScoreFemale($healthyLifeMaxScoreFemale)
    {
        $this->healthy_life_max_score_female = $healthyLifeMaxScoreFemale;
    
        return $this;
    }

    /**
     * Get healthy_life_max_score_female
     *
     * @return float 
     */
    public function getHealthyLifeMaxScoreFemale()
    {
        return $this->healthy_life_max_score_female;
    }

    /**
     * Set healthy_life_worst_score_male
     *
     * @param float $healthyLifeWorstScoreMale
     * @return LiteTest
     */
    public function setHealthyLifeWorstScoreMale($healthyLifeWorstScoreMale)
    {
        $this->healthy_life_worst_score_male = $healthyLifeWorstScoreMale;
    
        return $this;
    }

    /**
     * Get healthy_life_worst_score_male
     *
     * @return float 
     */
    public function getHealthyLifeWorstScoreMale()
    {
        return $this->healthy_life_worst_score_male;
    }

    /**
     * Set healthy_life_worst_score_female
     *
     * @param float $healthyLifeWorstScoreFemale
     * @return LiteTest
     */
    public function setHealthyLifeWorstScoreFemale($healthyLifeWorstScoreFemale)
    {
        $this->healthy_life_worst_score_female = $healthyLifeWorstScoreFemale;
    
        return $this;
    }

    /**
     * Get healthy_life_worst_score_female
     *
     * @return float 
     */
    public function getHealthyLifeWorstScoreFemale()
    {
        return $this->healthy_life_worst_score_female;
    }

    /**
     * Set bmi
     *
     * @param float $bmi
     * @return LiteTest
     */
    public function setBmi($bmi)
    {
        $this->bmi = $bmi;
    
        return $this;
    }

    /**
     * Get bmi
     *
     * @return float 
     */
    public function getBmi()
    {
        return $this->bmi;
    }

    /**
     * Set height
     *
     * @param integer $height
     * @return LiteTest
     */
    public function setHeight($height)
    {
        $this->height = $height;
    
        return $this;
    }

    /**
     * Get height
     *
     * @return integer 
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     * @return LiteTest
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    
        return $this;
    }

    /**
     * Get weight
     *
     * @return integer 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set lifestyle_physical_activity_q1
     *
     * @param integer $lifestylePhysicalActivityQ1
     * @return LiteTest
     */
    public function setLifestylePhysicalActivityQ1($lifestylePhysicalActivityQ1)
    {
        $this->lifestyle_physical_activity_q1 = $lifestylePhysicalActivityQ1;
    
        return $this;
    }

    /**
     * Get lifestyle_physical_activity_q1
     *
     * @return integer 
     */
    public function getLifestylePhysicalActivityQ1()
    {
        return $this->lifestyle_physical_activity_q1;
    }

    /**
     * Set lifestyle_physical_activity_q2
     *
     * @param integer $lifestylePhysicalActivityQ2
     * @return LiteTest
     */
    public function setLifestylePhysicalActivityQ2($lifestylePhysicalActivityQ2)
    {
        $this->lifestyle_physical_activity_q2 = $lifestylePhysicalActivityQ2;
    
        return $this;
    }

    /**
     * Get lifestyle_physical_activity_q2
     *
     * @return integer 
     */
    public function getLifestylePhysicalActivityQ2()
    {
        return $this->lifestyle_physical_activity_q2;
    }

    /**
     * Set lifestyle_physical_activity
     *
     * @param float $lifestylePhysicalActivity
     * @return LiteTest
     */
    public function setLifestylePhysicalActivity($lifestylePhysicalActivity)
    {
        $this->lifestyle_physical_activity = $lifestylePhysicalActivity;
    
        return $this;
    }

    /**
     * Get lifestyle_physical_activity
     *
     * @return float 
     */
    public function getLifestylePhysicalActivity()
    {
        return $this->lifestyle_physical_activity;
    }

    /**
     * Set lifestyle_smoking
     *
     * @param float $lifestyleSmoking
     * @return LiteTest
     */
    public function setLifestyleSmoking($lifestyleSmoking)
    {
        $this->lifestyle_smoking = $lifestyleSmoking;
    
        return $this;
    }

    /**
     * Get lifestyle_smoking
     *
     * @return float 
     */
    public function getLifestyleSmoking()
    {
        return $this->lifestyle_smoking;
    }

    /**
     * Set lifestyle_alcohol_q1
     *
     * @param integer $lifestyleAlcoholQ1
     * @return LiteTest
     */
    public function setLifestyleAlcoholQ1($lifestyleAlcoholQ1)
    {
        $this->lifestyle_alcohol_q1 = $lifestyleAlcoholQ1;
    
        return $this;
    }

    /**
     * Get lifestyle_alcohol_q1
     *
     * @return integer 
     */
    public function getLifestyleAlcoholQ1()
    {
        return $this->lifestyle_alcohol_q1;
    }

    /**
     * Set lifestyle_alcohol_q2
     *
     * @param integer $lifestyleAlcoholQ2
     * @return LiteTest
     */
    public function setLifestyleAlcoholQ2($lifestyleAlcoholQ2)
    {
        $this->lifestyle_alcohol_q2 = $lifestyleAlcoholQ2;
    
        return $this;
    }

    /**
     * Get lifestyle_alcohol_q2
     *
     * @return integer 
     */
    public function getLifestyleAlcoholQ2()
    {
        return $this->lifestyle_alcohol_q2;
    }

    /**
     * Set lifestyle_alcohol
     *
     * @param float $lifestyleAlcohol
     * @return LiteTest
     */
    public function setLifestyleAlcohol($lifestyleAlcohol)
    {
        $this->lifestyle_alcohol = $lifestyleAlcohol;
    
        return $this;
    }

    /**
     * Get lifestyle_alcohol
     *
     * @return float 
     */
    public function getLifestyleAlcohol()
    {
        return $this->lifestyle_alcohol;
    }

    /**
     * Set lifestyle_nutrition
     *
     * @param float $lifestyleNutrition
     * @return LiteTest
     */
    public function setLifestyleNutrition($lifestyleNutrition)
    {
        $this->lifestyle_nutrition = $lifestyleNutrition;
    
        return $this;
    }

    /**
     * Get lifestyle_nutrition
     *
     * @return float 
     */
    public function getLifestyleNutrition()
    {
        return $this->lifestyle_nutrition;
    }

    /**
     * Set lifestyle_mental_health
     *
     * @param float $lifestyleMentalHealth
     * @return LiteTest
     */
    public function setLifestyleMentalHealth($lifestyleMentalHealth)
    {
        $this->lifestyle_mental_health = $lifestyleMentalHealth;
    
        return $this;
    }

    /**
     * Get lifestyle_mental_health
     *
     * @return float 
     */
    public function getLifestyleMentalHealth()
    {
        return $this->lifestyle_mental_health;
    }

    /**
     * Set lifestyle_risk_profile
     *
     * @param float $lifestyleRiskProfile
     * @return LiteTest
     */
    public function setLifestyleRiskProfile($lifestyleRiskProfile)
    {
        $this->lifestyle_risk_profile = $lifestyleRiskProfile;
    
        return $this;
    }

    /**
     * Get lifestyle_risk_profile
     *
     * @return float 
     */
    public function getLifestyleRiskProfile()
    {
        return $this->lifestyle_risk_profile;
    }

    /**
     * Set physiological_vo2_test_type
     *
     * @param string $physiologicalVo2TestType
     * @return LiteTest
     */
    public function setPhysiologicalVo2TestType($physiologicalVo2TestType)
    {
        $this->physiological_vo2_test_type = $physiologicalVo2TestType;
    
        return $this;
    }

    /**
     * Get physiological_vo2_test_type
     *
     * @return string 
     */
    public function getPhysiologicalVo2TestType()
    {
        return $this->physiological_vo2_test_type;
    }

    /**
     * Set physiological_vo2_test_value1
     *
     * @param integer $physiologicalVo2TestValue1
     * @return LiteTest
     */
    public function setPhysiologicalVo2TestValue1($physiologicalVo2TestValue1)
    {
        $this->physiological_vo2_test_value1 = $physiologicalVo2TestValue1;
    
        return $this;
    }

    /**
     * Get physiological_vo2_test_value1
     *
     * @return integer 
     */
    public function getPhysiologicalVo2TestValue1()
    {
        return $this->physiological_vo2_test_value1;
    }

    /**
     * Set physiological_vo2_test_value2
     *
     * @param integer $physiologicalVo2TestValue2
     * @return LiteTest
     */
    public function setPhysiologicalVo2TestValue2($physiologicalVo2TestValue2)
    {
        $this->physiological_vo2_test_value2 = $physiologicalVo2TestValue2;
    
        return $this;
    }

    /**
     * Get physiological_vo2_test_value2
     *
     * @return integer 
     */
    public function getPhysiologicalVo2TestValue2()
    {
        return $this->physiological_vo2_test_value2;
    }

    /**
     * Set physiological_vo2
     *
     * @param float $physiologicalVo2
     * @return LiteTest
     */
    public function setPhysiologicalVo2($physiologicalVo2)
    {
        $this->physiological_vo2 = $physiologicalVo2;
    
        return $this;
    }

    /**
     * Get physiological_vo2
     *
     * @return float 
     */
    public function getPhysiologicalVo2()
    {
        return $this->physiological_vo2;
    }

    /**
     * Set physiological_balance_type
     *
     * @param string $physiologicalBalanceType
     * @return LiteTest
     */
    public function setPhysiologicalBalanceType($physiologicalBalanceType)
    {
        $this->physiological_balance_type = $physiologicalBalanceType;
    
        return $this;
    }

    /**
     * Get physiological_balance_type
     *
     * @return string 
     */
    public function getPhysiologicalBalanceType()
    {
        return $this->physiological_balance_type;
    }

    /**
     * Set physiological_balance_value
     *
     * @param integer $physiologicalBalanceValue
     * @return LiteTest
     */
    public function setPhysiologicalBalanceValue($physiologicalBalanceValue)
    {
        $this->physiological_balance_value = $physiologicalBalanceValue;
    
        return $this;
    }

    /**
     * Get physiological_balance_value
     *
     * @return integer 
     */
    public function getPhysiologicalBalanceValue()
    {
        return $this->physiological_balance_value;
    }

    /**
     * Set physiological_balance
     *
     * @param float $physiologicalBalance
     * @return LiteTest
     */
    public function setPhysiologicalBalance($physiologicalBalance)
    {
        $this->physiological_balance = $physiologicalBalance;
    
        return $this;
    }

    /**
     * Get physiological_balance
     *
     * @return float 
     */
    public function getPhysiologicalBalance()
    {
        return $this->physiological_balance;
    }

    /**
     * Set physiological_squat_value
     *
     * @param integer $physiologicalSquatValue
     * @return LiteTest
     */
    public function setPhysiologicalSquatValue($physiologicalSquatValue)
    {
        $this->physiological_squat_value = $physiologicalSquatValue;
    
        return $this;
    }

    /**
     * Get physiological_squat_value
     *
     * @return integer 
     */
    public function getPhysiologicalSquatValue()
    {
        return $this->physiological_squat_value;
    }

    /**
     * Set physiological_squat
     *
     * @param float $physiologicalSquat
     * @return LiteTest
     */
    public function setPhysiologicalSquat($physiologicalSquat)
    {
        $this->physiological_squat = $physiologicalSquat;
    
        return $this;
    }

    /**
     * Get physiological_squat
     *
     * @return float 
     */
    public function getPhysiologicalSquat()
    {
        return $this->physiological_squat;
    }

    /**
     * Set physiological_situp_value
     *
     * @param integer $physiologicalSitupValue
     * @return LiteTest
     */
    public function setPhysiologicalSitupValue($physiologicalSitupValue)
    {
        $this->physiological_situp_value = $physiologicalSitupValue;
    
        return $this;
    }

    /**
     * Get physiological_situp_value
     *
     * @return integer 
     */
    public function getPhysiologicalSitupValue()
    {
        return $this->physiological_situp_value;
    }

    /**
     * Set physiological_situp
     *
     * @param float $physiologicalSitup
     * @return LiteTest
     */
    public function setPhysiologicalSitup($physiologicalSitup)
    {
        $this->physiological_situp = $physiologicalSitup;
    
        return $this;
    }

    /**
     * Get physiological_situp
     *
     * @return float 
     */
    public function getPhysiologicalSitup()
    {
        return $this->physiological_situp;
    }

    /**
     * Set biometric_waist_value
     *
     * @param integer $biometricWaistValue
     * @return LiteTest
     */
    public function setBiometricWaistValue($biometricWaistValue)
    {
        $this->biometric_waist_value = $biometricWaistValue;
    
        return $this;
    }

    /**
     * Get biometric_waist_value
     *
     * @return integer 
     */
    public function getBiometricWaistValue()
    {
        return $this->biometric_waist_value;
    }

    /**
     * Set biometric_waist_value_lost
     *
     * @param integer $biometricWaistValueLost
     * @return LiteTest
     */
    public function setBiometricWaistValueLost($biometricWaistValueLost)
    {
        $this->biometric_waist_value_lost = $biometricWaistValueLost;
    
        return $this;
    }

    /**
     * Get biometric_waist_value_lost
     *
     * @return integer 
     */
    public function getBiometricWaistValueLost()
    {
        return $this->biometric_waist_value_lost;
    }

    /**
     * Set biometric_waist
     *
     * @param float $biometricWaist
     * @return LiteTest
     */
    public function setBiometricWaist($biometricWaist)
    {
        $this->biometric_waist = $biometricWaist;
    
        return $this;
    }

    /**
     * Get biometric_waist
     *
     * @return float 
     */
    public function getBiometricWaist()
    {
        return $this->biometric_waist;
    }

    /**
     * Set biometric_chest
     *
     * @param integer $biometricChest
     * @return LiteTest
     */
    public function setBiometricChest($biometricChest)
    {
        $this->biometric_chest = $biometricChest;
    
        return $this;
    }

    /**
     * Get biometric_chest
     *
     * @return integer 
     */
    public function getBiometricChest()
    {
        return $this->biometric_chest;
    }

    /**
     * Set biometric_chest_lost
     *
     * @param integer $biometricChestLost
     * @return LiteTest
     */
    public function setBiometricChestLost($biometricChestLost)
    {
        $this->biometric_chest_lost = $biometricChestLost;
    
        return $this;
    }

    /**
     * Get biometric_chest_lost
     *
     * @return integer 
     */
    public function getBiometricChestLost()
    {
        return $this->biometric_chest_lost;
    }

    /**
     * Set biometric_abdomen
     *
     * @param integer $biometricAbdomen
     * @return LiteTest
     */
    public function setBiometricAbdomen($biometricAbdomen)
    {
        $this->biometric_abdomen = $biometricAbdomen;
    
        return $this;
    }

    /**
     * Get biometric_abdomen
     *
     * @return integer 
     */
    public function getBiometricAbdomen()
    {
        return $this->biometric_abdomen;
    }

    /**
     * Set biometric_abdomen_lost
     *
     * @param integer $biometricAbdomenLost
     * @return LiteTest
     */
    public function setBiometricAbdomenLost($biometricAbdomenLost)
    {
        $this->biometric_abdomen_lost = $biometricAbdomenLost;
    
        return $this;
    }

    /**
     * Get biometric_abdomen_lost
     *
     * @return integer 
     */
    public function getBiometricAbdomenLost()
    {
        return $this->biometric_abdomen_lost;
    }

    /**
     * Set biometric_hip
     *
     * @param integer $biometricHip
     * @return LiteTest
     */
    public function setBiometricHip($biometricHip)
    {
        $this->biometric_hip = $biometricHip;
    
        return $this;
    }

    /**
     * Get biometric_hip
     *
     * @return integer 
     */
    public function getBiometricHip()
    {
        return $this->biometric_hip;
    }

    /**
     * Set biometric_hip_lost
     *
     * @param integer $biometricHipLost
     * @return LiteTest
     */
    public function setBiometricHipLost($biometricHipLost)
    {
        $this->biometric_hip_lost = $biometricHipLost;
    
        return $this;
    }

    /**
     * Get biometric_hip_lost
     *
     * @return integer 
     */
    public function getBiometricHipLost()
    {
        return $this->biometric_hip_lost;
    }

    /**
     * Set biometric_thigh
     *
     * @param integer $biometricThigh
     * @return LiteTest
     */
    public function setBiometricThigh($biometricThigh)
    {
        $this->biometric_thigh = $biometricThigh;
    
        return $this;
    }

    /**
     * Get biometric_thigh
     *
     * @return integer 
     */
    public function getBiometricThigh()
    {
        return $this->biometric_thigh;
    }

    /**
     * Set biometric_thigh_lost
     *
     * @param integer $biometricThighLost
     * @return LiteTest
     */
    public function setBiometricThighLost($biometricThighLost)
    {
        $this->biometric_thigh_lost = $biometricThighLost;
    
        return $this;
    }

    /**
     * Get biometric_thigh_lost
     *
     * @return integer 
     */
    public function getBiometricThighLost()
    {
        return $this->biometric_thigh_lost;
    }

    /**
     * Set biometric_arm
     *
     * @param integer $biometricArm
     * @return LiteTest
     */
    public function setBiometricArm($biometricArm)
    {
        $this->biometric_arm = $biometricArm;
    
        return $this;
    }

    /**
     * Get biometric_arm
     *
     * @return integer 
     */
    public function getBiometricArm()
    {
        return $this->biometric_arm;
    }

    /**
     * Set biometric_arm_lost
     *
     * @param integer $biometricArmLost
     * @return LiteTest
     */
    public function setBiometricArmLost($biometricArmLost)
    {
        $this->biometric_arm_lost = $biometricArmLost;
    
        return $this;
    }

    /**
     * Get biometric_arm_lost
     *
     * @return integer 
     */
    public function getBiometricArmLost()
    {
        return $this->biometric_arm_lost;
    }

    /**
     * Set biometric_body_fat_value
     *
     * @param integer $biometricBodyFatValue
     * @return LiteTest
     */
    public function setBiometricBodyFatValue($biometricBodyFatValue)
    {
        $this->biometric_body_fat_value = $biometricBodyFatValue;
    
        return $this;
    }

    /**
     * Get biometric_body_fat_value
     *
     * @return integer 
     */
    public function getBiometricBodyFatValue()
    {
        return $this->biometric_body_fat_value;
    }

    /**
     * Set biometric_body_fat
     *
     * @param float $biometricBodyFat
     * @return LiteTest
     */
    public function setBiometricBodyFat($biometricBodyFat)
    {
        $this->biometric_body_fat = $biometricBodyFat;
    
        return $this;
    }

    /**
     * Get biometric_body_fat
     *
     * @return float 
     */
    public function getBiometricBodyFat()
    {
        return $this->biometric_body_fat;
    }

    /**
     * Set biometric_blood_pressure_systolic
     *
     * @param integer $biometricBloodPressureSystolic
     * @return LiteTest
     */
    public function setBiometricBloodPressureSystolic($biometricBloodPressureSystolic)
    {
        $this->biometric_blood_pressure_systolic = $biometricBloodPressureSystolic;
    
        return $this;
    }

    /**
     * Get biometric_blood_pressure_systolic
     *
     * @return integer 
     */
    public function getBiometricBloodPressureSystolic()
    {
        return $this->biometric_blood_pressure_systolic;
    }

    /**
     * Set biometric_blood_pressure_diastolic
     *
     * @param integer $biometricBloodPressureDiastolic
     * @return LiteTest
     */
    public function setBiometricBloodPressureDiastolic($biometricBloodPressureDiastolic)
    {
        $this->biometric_blood_pressure_diastolic = $biometricBloodPressureDiastolic;
    
        return $this;
    }

    /**
     * Get biometric_blood_pressure_diastolic
     *
     * @return integer 
     */
    public function getBiometricBloodPressureDiastolic()
    {
        return $this->biometric_blood_pressure_diastolic;
    }

    /**
     * Set biometric_blood_pressure
     *
     * @param float $biometricBloodPressure
     * @return LiteTest
     */
    public function setBiometricBloodPressure($biometricBloodPressure)
    {
        $this->biometric_blood_pressure = $biometricBloodPressure;
    
        return $this;
    }

    /**
     * Get biometric_blood_pressure
     *
     * @return float 
     */
    public function getBiometricBloodPressure()
    {
        return $this->biometric_blood_pressure;
    }

    /**
     * Set biometric_blood_oxygen_value
     *
     * @param integer $biometricBloodOxygenValue
     * @return LiteTest
     */
    public function setBiometricBloodOxygenValue($biometricBloodOxygenValue)
    {
        $this->biometric_blood_oxygen_value = $biometricBloodOxygenValue;
    
        return $this;
    }

    /**
     * Get biometric_blood_oxygen_value
     *
     * @return integer 
     */
    public function getBiometricBloodOxygenValue()
    {
        return $this->biometric_blood_oxygen_value;
    }

    /**
     * Set biometric_blood_oxygen
     *
     * @param float $biometricBloodOxygen
     * @return LiteTest
     */
    public function setBiometricBloodOxygen($biometricBloodOxygen)
    {
        $this->biometric_blood_oxygen = $biometricBloodOxygen;
    
        return $this;
    }

    /**
     * Get biometric_blood_oxygen
     *
     * @return float 
     */
    public function getBiometricBloodOxygen()
    {
        return $this->biometric_blood_oxygen;
    }

    /**
     * Set total_crf
     *
     * @param float $totalCrf
     * @return LiteTest
     */
    public function setTotalCrf($totalCrf)
    {
        $this->total_crf = $totalCrf;
    
        return $this;
    }

    /**
     * Get total_crf
     *
     * @return float 
     */
    public function getTotalCrf()
    {
        return $this->total_crf;
    }

    /**
     * Set healthy_life_points
     *
     * @param float $healthyLifePoints
     * @return LiteTest
     */
    public function setHealthyLifePoints($healthyLifePoints)
    {
        $this->healthy_life_points = $healthyLifePoints;
    
        return $this;
    }

    /**
     * Get healthy_life_points
     *
     * @return float 
     */
    public function getHealthyLifePoints()
    {
        return $this->healthy_life_points;
    }

    /**
     * Set estimated_healthy_life_expectancy
     *
     * @param float $estimatedHealthyLifeExpectancy
     * @return LiteTest
     */
    public function setEstimatedHealthyLifeExpectancy($estimatedHealthyLifeExpectancy)
    {
        $this->estimated_healthy_life_expectancy = $estimatedHealthyLifeExpectancy;
    
        return $this;
    }

    /**
     * Get estimated_healthy_life_expectancy
     *
     * @return float 
     */
    public function getEstimatedHealthyLifeExpectancy()
    {
        return $this->estimated_healthy_life_expectancy;
    }

    /**
     * Set max_hle
     *
     * @param float $maxHle
     * @return LiteTest
     */
    public function setMaxHle($maxHle)
    {
        $this->max_hle = $maxHle;
    
        return $this;
    }

    /**
     * Get max_hle
     *
     * @return float 
     */
    public function getMaxHle()
    {
        return $this->max_hle;
    }

    /**
     * Set healthy_life_score
     *
     * @param float $healthyLifeScore
     * @return LiteTest
     */
    public function setHealthyLifeScore($healthyLifeScore)
    {
        $this->healthy_life_score = $healthyLifeScore;
    
        return $this;
    }

    /**
     * Get healthy_life_score
     *
     * @return float 
     */
    public function getHealthyLifeScore()
    {
        return $this->healthy_life_score;
    }

    /**
     * Set estimated_years_of_healthy_life_left
     *
     * @param float $estimatedYearsOfHealthyLifeLeft
     * @return LiteTest
     */
    public function setEstimatedYearsOfHealthyLifeLeft($estimatedYearsOfHealthyLifeLeft)
    {
        $this->estimated_years_of_healthy_life_left = $estimatedYearsOfHealthyLifeLeft;
    
        return $this;
    }

    /**
     * Get estimated_years_of_healthy_life_left
     *
     * @return float 
     */
    public function getEstimatedYearsOfHealthyLifeLeft()
    {
        return $this->estimated_years_of_healthy_life_left;
    }

    /**
     * Set estimated_years_you_will_live_with_did
     *
     * @param float $estimatedYearsYouWillLiveWithDid
     * @return LiteTest
     */
    public function setEstimatedYearsYouWillLiveWithDid($estimatedYearsYouWillLiveWithDid)
    {
        $this->estimated_years_you_will_live_with_did = $estimatedYearsYouWillLiveWithDid;
    
        return $this;
    }

    /**
     * Get estimated_years_you_will_live_with_did
     *
     * @return float 
     */
    public function getEstimatedYearsYouWillLiveWithDid()
    {
        return $this->estimated_years_you_will_live_with_did;
    }

    /**
     * Set estimated_years_you_will_live_with_did_as_opposed_to
     *
     * @param float $estimatedYearsYouWillLiveWithDidAsOpposedTo
     * @return LiteTest
     */
    public function setEstimatedYearsYouWillLiveWithDidAsOpposedTo($estimatedYearsYouWillLiveWithDidAsOpposedTo)
    {
        $this->estimated_years_you_will_live_with_did_as_opposed_to = $estimatedYearsYouWillLiveWithDidAsOpposedTo;
    
        return $this;
    }

    /**
     * Get estimated_years_you_will_live_with_did_as_opposed_to
     *
     * @return float 
     */
    public function getEstimatedYearsYouWillLiveWithDidAsOpposedTo()
    {
        return $this->estimated_years_you_will_live_with_did_as_opposed_to;
    }

    /**
     * Set you_could_add_up_to
     *
     * @param float $youCouldAddUpTo
     * @return LiteTest
     */
    public function setYouCouldAddUpTo($youCouldAddUpTo)
    {
        $this->you_could_add_up_to = $youCouldAddUpTo;
    
        return $this;
    }

    /**
     * Get you_could_add_up_to
     *
     * @return float 
     */
    public function getYouCouldAddUpTo()
    {
        return $this->you_could_add_up_to;
    }

    /**
     * Set estimated_current_healthy_life_age
     *
     * @param float $estimatedCurrentHealthyLifeAge
     * @return LiteTest
     */
    public function setEstimatedCurrentHealthyLifeAge($estimatedCurrentHealthyLifeAge)
    {
        $this->estimated_current_healthy_life_age = $estimatedCurrentHealthyLifeAge;
    
        return $this;
    }

    /**
     * Get estimated_current_healthy_life_age
     *
     * @return float 
     */
    public function getEstimatedCurrentHealthyLifeAge()
    {
        return $this->estimated_current_healthy_life_age;
    }

    /**
     * Set years_added
     *
     * @param float $yearsAdded
     * @return LiteTest
     */
    public function setYearsAdded($yearsAdded)
    {
        $this->years_added = $yearsAdded;
    
        return $this;
    }

    /**
     * Get years_added
     *
     * @return float 
     */
    public function getYearsAdded()
    {
        return $this->years_added;
    }

    /**
     * Set cm_lost
     *
     * @param float $cmLost
     * @return LiteTest
     */
    public function setCmLost($cmLost)
    {
        $this->cm_lost = $cmLost;
    
        return $this;
    }

    /**
     * Get cm_lost
     *
     * @return float 
     */
    public function getCmLost()
    {
        return $this->cm_lost;
    }

    /**
     * Set kg_lost
     *
     * @param float $kgLost
     * @return LiteTest
     */
    public function setKgLost($kgLost)
    {
        $this->kg_lost = $kgLost;
    
        return $this;
    }

    /**
     * Get kg_lost
     *
     * @return float 
     */
    public function getKgLost()
    {
        return $this->kg_lost;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return LiteTest
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }
}