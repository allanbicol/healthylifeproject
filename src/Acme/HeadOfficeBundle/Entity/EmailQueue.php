<?php

namespace Acme\HeadOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EmailQueue
 *
 * @ORM\Table(name="tbl_email_queue")
 * @ORM\Entity
 */
class EmailQueue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="queue_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $queue_id;


    /**
     * @var string
     *
     * @ORM\Column(name="recipients", type="string")
     */
    private $recipients;

    /**
     * @var string
     *
     * @ORM\Column(name="sender", type="string", length=220)
     */
    private $sender;
    
    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string")
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="string")
     */
    private $body;

    /**
     * @var integer
     *
     * @ORM\Column(name="s_test", type="integer")
     */
    private $s_test;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=20)
     */
    private $reference;

    /**
     * @var integer
     *
     * @ORM\Column(name="s_sent", type="integer")
     */
    private $s_sent;



    /**
     * Get queue_id
     *
     * @return integer 
     */
    public function getQueueId()
    {
        return $this->queue_id;
    }

    /**
     * Set recipients
     *
     * @param string $recipients
     * @return EmailQueue
     */
    public function setRecipients($recipients)
    {
        $this->recipients = $recipients;
    
        return $this;
    }

    /**
     * Get recipients
     *
     * @return string 
     */
    public function getRecipients()
    {
        return $this->recipients;
    }

    /**
     * Set sender
     *
     * @param string $sender
     * @return EmailQueue
     */
    public function setSender($sender)
    {
        $this->sender = $sender;
    
        return $this;
    }

    /**
     * Get sender
     *
     * @return string 
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return EmailQueue
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    
        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return EmailQueue
     */
    public function setBody($body)
    {
        $this->body = $body;
    
        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set s_test
     *
     * @param integer $sTest
     * @return EmailQueue
     */
    public function setSTest($sTest)
    {
        $this->s_test = $sTest;
    
        return $this;
    }

    /**
     * Get s_test
     *
     * @return integer 
     */
    public function getSTest()
    {
        return $this->s_test;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return EmailQueue
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    
        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set s_sent
     *
     * @param integer $sSent
     * @return EmailQueue
     */
    public function setSSent($sSent)
    {
        $this->s_sent = $sSent;
    
        return $this;
    }

    /**
     * Get s_sent
     *
     * @return integer 
     */
    public function getSSent()
    {
        return $this->s_sent;
    }
}