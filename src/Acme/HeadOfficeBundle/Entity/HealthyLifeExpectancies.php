<?php

namespace Acme\HeadOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HealthyLifeExpectancies
 *
 * @ORM\Table(name="tbl_healthy_life_expectancies")
 * @ORM\Entity
 */
class HealthyLifeExpectancies
{
    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=10)
     * @ORM\Id
     */
    private $gender;

    /**
     * @var float
     *
     * @ORM\Column(name="expectancy", type="float")
     */
    private $expectancy;

    /**
     * @var float
     *
     * @ORM\Column(name="total_expectancy", type="float")
     */
    private $total_expectancy;

    /**
     * @var float
     *
     * @ORM\Column(name="dalys", type="float")
     */
    private $dalys;



    /**
     * Set gender
     *
     * @param string $gender
     * @return HealthyLifeExpectancies
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    
        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set expectancy
     *
     * @param float $expectancy
     * @return HealthyLifeExpectancies
     */
    public function setExpectancy($expectancy)
    {
        $this->expectancy = $expectancy;
    
        return $this;
    }

    /**
     * Get expectancy
     *
     * @return float 
     */
    public function getExpectancy()
    {
        return $this->expectancy;
    }

    /**
     * Set total_expectancy
     *
     * @param float $totalExpectancy
     * @return HealthyLifeExpectancies
     */
    public function setTotalExpectancy($totalExpectancy)
    {
        $this->total_expectancy = $totalExpectancy;
    
        return $this;
    }

    /**
     * Get total_expectancy
     *
     * @return float 
     */
    public function getTotalExpectancy()
    {
        return $this->total_expectancy;
    }

    /**
     * Set dalys
     *
     * @param float $dalys
     * @return HealthyLifeExpectancies
     */
    public function setDalys($dalys)
    {
        $this->dalys = $dalys;
    
        return $this;
    }

    /**
     * Get dalys
     *
     * @return float 
     */
    public function getDalys()
    {
        return $this->dalys;
    }
}