<?php

namespace Acme\HeadOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lams
 *
 * @ORM\Table(name="tbl_lams")
 * @ORM\Entity
 */
class Lams
{
    /**
     * @var integer
     *
     * @ORM\Column(name="lam_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $lam_id;

    /**
     * @var string
     *
     * @ORM\Column(name="lam", type="string", length=100)
     */
    private $lam;


    /**
     * Get lam_id
     *
     * @return integer 
     */
    public function getLamId()
    {
        return $this->lam_id;
    }

    /**
     * Set lam
     *
     * @param string $lam
     * @return Lams
     */
    public function setLam($lam)
    {
        $this->lam = $lam;
    
        return $this;
    }

    /**
     * Get lam
     *
     * @return string 
     */
    public function getLam()
    {
        return $this->lam;
    }
}