<?php

namespace Acme\HeadOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LifestyleQuestionnare
 *
 * @ORM\Table(name="tbl_lifestyle_questionnaire")
 * @ORM\Entity
 */
class LifestyleQuestionnare
{
    /**
     * @var integer
     *
     * @ORM\Column(name="lq_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $lq_id;

    /**
     * @var string
     *
     * @ORM\Column(name="lq_code", type="string", length=20)
     */
    private $lq_code;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="string")
     */
    private $question;
    
    /**
     * @var string
     *
     * @ORM\Column(name="class", type="string", length=50)
     */
    private $class;



    /**
     * Get lq_id
     *
     * @return integer 
     */
    public function getLqId()
    {
        return $this->lq_id;
    }

    /**
     * Set lq_code
     *
     * @param string $lqCode
     * @return LifestyleQuestionnare
     */
    public function setLqCode($lqCode)
    {
        $this->lq_code = $lqCode;
    
        return $this;
    }

    /**
     * Get lq_code
     *
     * @return string 
     */
    public function getLqCode()
    {
        return $this->lq_code;
    }

    /**
     * Set question
     *
     * @param string $question
     * @return LifestyleQuestionnare
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    
        return $this;
    }

    /**
     * Get question
     *
     * @return string 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set class
     *
     * @param string $class
     * @return LifestyleQuestionnare
     */
    public function setClass($class)
    {
        $this->class = $class;
    
        return $this;
    }

    /**
     * Get class
     *
     * @return string 
     */
    public function getClass()
    {
        return $this->class;
    }
}