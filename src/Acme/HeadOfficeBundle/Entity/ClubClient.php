<?php

namespace Acme\HeadOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
* ClubClient
*
* @ORM\Table(name="tbl_club_client")
* @ORM\Entity
 */
class ClubClient
{
    /**
     * @var integer
     *
     * @ORM\Column(name="club_client_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $club_client_id;

    /**
     * @var integer
     * 
     * @ORM\Column(name="club_id", type="integer")
     */
    private $club_id;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="club_admin_id", type="integer")
     */
    private $club_admin_id;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="opt_client_id", type="string", length=100)
     */
    private $opt_client_id;
    
    /**
     * @var integer
     * 
     * @ORM\Column(name="s_non_member_assessment", type="integer")
     */
    private $s_non_member_assessment;
    
    /**
     * @var integer
     * 
     * @ORM\Column(name="s_aia_vitality_member", type="integer")
     */
    private $s_aia_vitality_member;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="aia_vitality_member_number", type="string", length=100)
     */
    private $aia_vitality_member_number;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="client_category", type="integer")
     */
    private $client_category;

    /**
     * @var string
     * @Assert\NotBlank(message="First name must not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "100",
     *      minMessage = "First name must be at least {{ limit }} characters length",
     *      maxMessage = "First name cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="fname", type="string", length=100)
     */
    private $fname;

    /**
     * @var string
     * @Assert\NotBlank(message="Last name must not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "100",
     *      minMessage = "Last name must be at least {{ limit }} characters length",
     *      maxMessage = "Last name cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="lname", type="string", length=100)
     */
    private $lname;

    /**
     * @var string
     * @Assert\NotBlank(message="Email must not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "100",
     *      minMessage = "Email must be at least {{ limit }} characters length",
     *      maxMessage = "Email cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="email", type="string", length=100)
     */
    private $email;

    /**
     * @var string
     * @Assert\Length(
     *      min = "1",
     *      max = "50",
     *      minMessage = "Phone must be at least {{ limit }} characters length",
     *      maxMessage = "Phone cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="phone", type="string", length=50)
     */
    private $phone;

    /**
     * @var string
     * @Assert\NotBlank(message="Birthdate must not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "20",
     *      minMessage = "Birthdate must be at least {{ limit }} characters length",
     *      maxMessage = "Birthdate cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="birthdate", type="string", length=20)
     */
    private $birthdate;

    /**
     * @var string
     * @Assert\NotBlank(message="Gender must not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "10",
     *      minMessage = "Gender must be at least {{ limit }} characters length",
     *      maxMessage = "Gender cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="gender", type="string", length=10)
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="picture", type="string")
     */
    private $picture;

    /**
     * @var float
     *
     * @ORM\Column(name="height", type="float")
     */
    private $height;

    /**
     * @var float
     *
     * @ORM\Column(name="weight", type="float")
     */
    private $weight;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=10)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=100)
     */
    private $password;
    
    /**
     * @var string
     *
     * @ORM\Column(name="last_login_datetime", type="string", length=20)
     */
    private $last_login_datetime;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=100)
     */
    private $token;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="program_id", type="integer")
     */
    private $program_id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="program_type", type="string", length=200)
     */
    private $program_type;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="do_lifestyle_test", type="integer")
     */
    private $do_lifestyle_test;
   

    /**
     * @var string
     * @ORM\Column(name="program_start_date", type="string", length=20)
     */
    private $program_start_date;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="s_prospect", type="integer")
     */
    private $s_prospect;

    /**
     * @var string
     *
     * @ORM\Column(name="registered_date", type="string", length=20)
     */
    private $registered_date;
  
    /**
     * @var integer
     *
     * @ORM\Column(name="s_signup_questions_done", type="integer")
     */
    private $s_signup_questions_done;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="s_signup_health_questions_done", type="integer")
     */
    private $s_signup_health_questions_done;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="s_aia_health_questions_done", type="integer")
     */
    private $s_aia_health_questions_done;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="s_confirmed_aia_test", type="integer")
     */
    private $s_confirmed_aia_test;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="s_weekly_summary_unsubscribed", type="integer")
     */
    private $s_weekly_summary_unsubscribed;
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="s_weekly_summary_unsubscribed_trainer", type="integer")
     */
    private $s_weekly_summary_unsubscribed_trainer;

    /**
     * @var integer
     *
     * @ORM\Column(name="s_like_to_be_contact", type="integer")
     */
    private $s_like_to_be_contact;

    

    

    /**
     * Get club_client_id
     *
     * @return integer 
     */
    public function getClubClientId()
    {
        return $this->club_client_id;
    }

    /**
     * Set club_id
     *
     * @param integer $clubId
     * @return ClubClient
     */
    public function setClubId($clubId)
    {
        $this->club_id = $clubId;
    
        return $this;
    }

    /**
     * Get club_id
     *
     * @return integer 
     */
    public function getClubId()
    {
        return $this->club_id;
    }

    /**
     * Set club_admin_id
     *
     * @param integer $clubAdminId
     * @return ClubClient
     */
    public function setClubAdminId($clubAdminId)
    {
        $this->club_admin_id = $clubAdminId;
    
        return $this;
    }

    /**
     * Get club_admin_id
     *
     * @return integer 
     */
    public function getClubAdminId()
    {
        return $this->club_admin_id;
    }

    /**
     * Set opt_client_id
     *
     * @param string $optClientId
     * @return ClubClient
     */
    public function setOptClientId($optClientId)
    {
        $this->opt_client_id = $optClientId;
    
        return $this;
    }

    /**
     * Get opt_client_id
     *
     * @return string 
     */
    public function getOptClientId()
    {
        return $this->opt_client_id;
    }

    /**
     * Set s_non_member_assessment
     *
     * @param integer $sNonMemberAssessment
     * @return ClubClient
     */
    public function setSNonMemberAssessment($sNonMemberAssessment)
    {
        $this->s_non_member_assessment = $sNonMemberAssessment;
    
        return $this;
    }

    /**
     * Get s_non_member_assessment
     *
     * @return integer 
     */
    public function getSNonMemberAssessment()
    {
        return $this->s_non_member_assessment;
    }

    /**
     * Set s_aia_vitality_member
     *
     * @param integer $sAiaVitalityMember
     * @return ClubClient
     */
    public function setSAiaVitalityMember($sAiaVitalityMember)
    {
        $this->s_aia_vitality_member = $sAiaVitalityMember;
    
        return $this;
    }

    /**
     * Get s_aia_vitality_member
     *
     * @return integer 
     */
    public function getSAiaVitalityMember()
    {
        return $this->s_aia_vitality_member;
    }

    /**
     * Set aia_vitality_member_number
     *
     * @param string $aiaVitalityMemberNumber
     * @return ClubClient
     */
    public function setAiaVitalityMemberNumber($aiaVitalityMemberNumber)
    {
        $this->aia_vitality_member_number = $aiaVitalityMemberNumber;
    
        return $this;
    }

    /**
     * Get aia_vitality_member_number
     *
     * @return string 
     */
    public function getAiaVitalityMemberNumber()
    {
        return $this->aia_vitality_member_number;
    }

    /**
     * Set client_category
     *
     * @param integer $clientCategory
     * @return ClubClient
     */
    public function setClientCategory($clientCategory)
    {
        $this->client_category = $clientCategory;
    
        return $this;
    }

    /**
     * Get client_category
     *
     * @return integer 
     */
    public function getClientCategory()
    {
        return $this->client_category;
    }

    /**
     * Set fname
     *
     * @param string $fname
     * @return ClubClient
     */
    public function setFname($fname)
    {
        $this->fname = $fname;
    
        return $this;
    }

    /**
     * Get fname
     *
     * @return string 
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set lname
     *
     * @param string $lname
     * @return ClubClient
     */
    public function setLname($lname)
    {
        $this->lname = $lname;
    
        return $this;
    }

    /**
     * Get lname
     *
     * @return string 
     */
    public function getLname()
    {
        return $this->lname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return ClubClient
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return ClubClient
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    
        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set birthdate
     *
     * @param string $birthdate
     * @return ClubClient
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;
    
        return $this;
    }

    /**
     * Get birthdate
     *
     * @return string 
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return ClubClient
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    
        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set picture
     *
     * @param string $picture
     * @return ClubClient
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    
        return $this;
    }

    /**
     * Get picture
     *
     * @return string 
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Set height
     *
     * @param float $height
     * @return ClubClient
     */
    public function setHeight($height)
    {
        $this->height = $height;
    
        return $this;
    }

    /**
     * Get height
     *
     * @return float 
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set weight
     *
     * @param float $weight
     * @return ClubClient
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    
        return $this;
    }

    /**
     * Get weight
     *
     * @return float 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return ClubClient
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return ClubClient
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set last_login_datetime
     *
     * @param string $lastLoginDatetime
     * @return ClubClient
     */
    public function setLastLoginDatetime($lastLoginDatetime)
    {
        $this->last_login_datetime = $lastLoginDatetime;
    
        return $this;
    }

    /**
     * Get last_login_datetime
     *
     * @return string 
     */
    public function getLastLoginDatetime()
    {
        return $this->last_login_datetime;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return ClubClient
     */
    public function setToken($token)
    {
        $this->token = $token;
    
        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set program_id
     *
     * @param integer $programId
     * @return ClubClient
     */
    public function setProgramId($programId)
    {
        $this->program_id = $programId;
    
        return $this;
    }

    /**
     * Get program_id
     *
     * @return integer 
     */
    public function getProgramId()
    {
        return $this->program_id;
    }

    /**
     * Set program_type
     *
     * @param string $programType
     * @return ClubClient
     */
    public function setProgramType($programType)
    {
        $this->program_type = $programType;
    
        return $this;
    }

    /**
     * Get program_type
     *
     * @return string 
     */
    public function getProgramType()
    {
        return $this->program_type;
    }

    /**
     * Set do_lifestyle_test
     *
     * @param integer $doLifestyleTest
     * @return ClubClient
     */
    public function setDoLifestyleTest($doLifestyleTest)
    {
        $this->do_lifestyle_test = $doLifestyleTest;
    
        return $this;
    }

    /**
     * Get do_lifestyle_test
     *
     * @return integer 
     */
    public function getDoLifestyleTest()
    {
        return $this->do_lifestyle_test;
    }

    /**
     * Set program_start_date
     *
     * @param string $programStartDate
     * @return ClubClient
     */
    public function setProgramStartDate($programStartDate)
    {
        $this->program_start_date = $programStartDate;
    
        return $this;
    }

    /**
     * Get program_start_date
     *
     * @return string 
     */
    public function getProgramStartDate()
    {
        return $this->program_start_date;
    }

    /**
     * Set s_prospect
     *
     * @param integer $sProspect
     * @return ClubClient
     */
    public function setSProspect($sProspect)
    {
        $this->s_prospect = $sProspect;
    
        return $this;
    }

    /**
     * Get s_prospect
     *
     * @return integer 
     */
    public function getSProspect()
    {
        return $this->s_prospect;
    }

    /**
     * Set registered_date
     *
     * @param string $registeredDate
     * @return ClubClient
     */
    public function setRegisteredDate($registeredDate)
    {
        $this->registered_date = $registeredDate;
    
        return $this;
    }

    /**
     * Get registered_date
     *
     * @return string 
     */
    public function getRegisteredDate()
    {
        return $this->registered_date;
    }

    /**
     * Set s_signup_questions_done
     *
     * @param integer $sSignupQuestionsDone
     * @return ClubClient
     */
    public function setSSignupQuestionsDone($sSignupQuestionsDone)
    {
        $this->s_signup_questions_done = $sSignupQuestionsDone;
    
        return $this;
    }

    /**
     * Get s_signup_questions_done
     *
     * @return integer 
     */
    public function getSSignupQuestionsDone()
    {
        return $this->s_signup_questions_done;
    }

    /**
     * Set s_signup_health_questions_done
     *
     * @param integer $sSignupHealthQuestionsDone
     * @return ClubClient
     */
    public function setSSignupHealthQuestionsDone($sSignupHealthQuestionsDone)
    {
        $this->s_signup_health_questions_done = $sSignupHealthQuestionsDone;
    
        return $this;
    }

    /**
     * Get s_signup_health_questions_done
     *
     * @return integer 
     */
    public function getSSignupHealthQuestionsDone()
    {
        return $this->s_signup_health_questions_done;
    }

    /**
     * Set s_aia_health_questions_done
     *
     * @param integer $sAiaHealthQuestionsDone
     * @return ClubClient
     */
    public function setSAiaHealthQuestionsDone($sAiaHealthQuestionsDone)
    {
        $this->s_aia_health_questions_done = $sAiaHealthQuestionsDone;
    
        return $this;
    }

    /**
     * Get s_aia_health_questions_done
     *
     * @return integer 
     */
    public function getSAiaHealthQuestionsDone()
    {
        return $this->s_aia_health_questions_done;
    }

    /**
     * Set s_confirmed_aia_test
     *
     * @param integer $sConfirmedAiaTest
     * @return ClubClient
     */
    public function setSConfirmedAiaTest($sConfirmedAiaTest)
    {
        $this->s_confirmed_aia_test = $sConfirmedAiaTest;
    
        return $this;
    }

    /**
     * Get s_confirmed_aia_test
     *
     * @return integer 
     */
    public function getSConfirmedAiaTest()
    {
        return $this->s_confirmed_aia_test;
    }

    /**
     * Set s_weekly_summary_unsubscribed
     *
     * @param integer $sWeeklySummaryUnsubscribed
     * @return ClubClient
     */
    public function setSWeeklySummaryUnsubscribed($sWeeklySummaryUnsubscribed)
    {
        $this->s_weekly_summary_unsubscribed = $sWeeklySummaryUnsubscribed;
    
        return $this;
    }

    /**
     * Get s_weekly_summary_unsubscribed
     *
     * @return integer 
     */
    public function getSWeeklySummaryUnsubscribed()
    {
        return $this->s_weekly_summary_unsubscribed;
    }

    /**
     * Set s_weekly_summary_unsubscribed_trainer
     *
     * @param integer $sWeeklySummaryUnsubscribedTrainer
     * @return ClubClient
     */
    public function setSWeeklySummaryUnsubscribedTrainer($sWeeklySummaryUnsubscribedTrainer)
    {
        $this->s_weekly_summary_unsubscribed_trainer = $sWeeklySummaryUnsubscribedTrainer;
    
        return $this;
    }

    /**
     * Get s_weekly_summary_unsubscribed_trainer
     *
     * @return integer 
     */
    public function getSWeeklySummaryUnsubscribedTrainer()
    {
        return $this->s_weekly_summary_unsubscribed_trainer;
    }

    /**
     * Set s_like_to_be_contact
     *
     * @param integer $sLikeToBeContact
     * @return ClubClient
     */
    public function setSLikeToBeContact($sLikeToBeContact)
    {
        $this->s_like_to_be_contact = $sLikeToBeContact;
    
        return $this;
    }

    /**
     * Get s_like_to_be_contact
     *
     * @return integer 
     */
    public function getSLikeToBeContact()
    {
        return $this->s_like_to_be_contact;
    }
}