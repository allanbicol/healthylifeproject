<?php

namespace Acme\HeadOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Event
 *
 * @ORM\Table(name="tbl_event")
 * @ORM\Entity
 */
class Event
{
    /**
     * @var integer
     *
     * @ORM\Column(name="event_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $event_id;

    /**
     * @var integer
     *
     * @ORM\Column(name="club_id", type="integer")
     */
    private $club_id;

    /**
     * @var string
     * @Assert\NotBlank(message="Event name must not be blank.")
     * @Assert\Length(
     *      min = "1",
     *      max = "100",
     *      minMessage = "Event name must be at least {{ limit }} characters length",
     *      maxMessage = "Event name cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="event_name", type="string", length=250)
     */
    private $event_name;
    
    /**
     * @var string
     * @Assert\Length(
     *      min = "1",
     *      max = "500",
     *      minMessage = "Location must be at least {{ limit }} characters length",
     *      maxMessage = "Location cannot be longer than {{ limit }} characters length"
     * )
     * @ORM\Column(name="location", type="string", length=500)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=10)
     */
    private $status;




    /**
     * Get event_id
     *
     * @return integer 
     */
    public function getEventId()
    {
        return $this->event_id;
    }

    /**
     * Set club_id
     *
     * @param integer $clubId
     * @return Event
     */
    public function setClubId($clubId)
    {
        $this->club_id = $clubId;
    
        return $this;
    }

    /**
     * Get club_id
     *
     * @return integer 
     */
    public function getClubId()
    {
        return $this->club_id;
    }

    /**
     * Set event_name
     *
     * @param string $eventName
     * @return Event
     */
    public function setEventName($eventName)
    {
        $this->event_name = $eventName;
    
        return $this;
    }

    /**
     * Get event_name
     *
     * @return string 
     */
    public function getEventName()
    {
        return $this->event_name;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return Event
     */
    public function setLocation($location)
    {
        $this->location = $location;
    
        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Event
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }
}