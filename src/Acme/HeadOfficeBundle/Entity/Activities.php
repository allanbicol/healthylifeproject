<?php

namespace Acme\HeadOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Activity
 *
 * @ORM\Table(name="tbl_activities")
 * @ORM\Entity
 */
class Activities
{
    /**
     * @var integer
     *
     * @ORM\Column(name="activity_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $activity_id;

    /**
     * @var string
     *
     * @ORM\Column(name="log_datetime", type="string", length=20)
     */
    private $log_datetime;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $user_id;

    /**
     * @var string
     *
     * @ORM\Column(name="user_type", type="string", length=10)
     */
    private $user_type;

    /**
     * @var string
     *
     * @ORM\Column(name="details", type="string")
     */
    private $details;



    /**
     * Get activity_id
     *
     * @return integer 
     */
    public function getActivityId()
    {
        return $this->activity_id;
    }

    /**
     * Set user_id
     *
     * @param integer $userId
     * @return Activity
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;
    
        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set user_type
     *
     * @param string $userType
     * @return Activity
     */
    public function setUserType($userType)
    {
        $this->user_type = $userType;
    
        return $this;
    }

    /**
     * Get user_type
     *
     * @return string 
     */
    public function getUserType()
    {
        return $this->user_type;
    }

    /**
     * Set details
     *
     * @param string $details
     * @return Activity
     */
    public function setDetails($details)
    {
        $this->details = $details;
    
        return $this;
    }

    /**
     * Get details
     *
     * @return string 
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Set log_datetime
     *
     * @param string $logDatetime
     * @return Activities
     */
    public function setLogDatetime($logDatetime)
    {
        $this->log_datetime = $logDatetime;
    
        return $this;
    }

    /**
     * Get log_datetime
     *
     * @return string 
     */
    public function getLogDatetime()
    {
        return $this->log_datetime;
    }
}