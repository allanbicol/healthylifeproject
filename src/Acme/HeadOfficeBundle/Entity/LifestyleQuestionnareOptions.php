<?php

namespace Acme\HeadOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LifestyleQuestionnareOptions
 *
 * @ORM\Table(name="tbl_lifestyle_questionnaire_options")
 * @ORM\Entity
 */
class LifestyleQuestionnareOptions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="lqo_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $lqo_id;

    /**
     * @var string
     *
     * @ORM\Column(name="lq_code", type="string", length=20)
     */
    private $lq_code;

    /**
     * @var string
     *
     * @ORM\Column(name="option_description", type="string")
     */
    private $option_description;

    /**
     * @var string
     *
     * @ORM\Column(name="option_value", type="integer", length=100)
     */
    private $option_value;


    /**
     * Get lqo_id
     *
     * @return integer 
     */
    public function getLqoId()
    {
        return $this->lqo_id;
    }

    /**
     * Set lq_code
     *
     * @param string $lqCode
     * @return LifestyleQuestionnareOptions
     */
    public function setLqCode($lqCode)
    {
        $this->lq_code = $lqCode;
    
        return $this;
    }

    /**
     * Get lq_code
     *
     * @return string 
     */
    public function getLqCode()
    {
        return $this->lq_code;
    }

    /**
     * Set option_description
     *
     * @param string $optionDescription
     * @return LifestyleQuestionnareOptions
     */
    public function setOptionDescription($optionDescription)
    {
        $this->option_description = $optionDescription;
    
        return $this;
    }

    /**
     * Get option_description
     *
     * @return string 
     */
    public function getOptionDescription()
    {
        return $this->option_description;
    }

    /**
     * Set option_value
     *
     * @param integer $optionValue
     * @return LifestyleQuestionnareOptions
     */
    public function setOptionValue($optionValue)
    {
        $this->option_value = $optionValue;
    
        return $this;
    }

    /**
     * Get option_value
     *
     * @return integer 
     */
    public function getOptionValue()
    {
        return $this->option_value;
    }
}