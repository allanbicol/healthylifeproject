<?php
/**
 * Description of TestFormulas
 *
 * @author leokarl
 */
namespace Acme\HeadOfficeBundle\Model;


class TestFormulas {
    
    public function physiologicalStrength($weight, $reps){
        // $physiologicalSquat1rm =SUM(C17*(1+(C18/30)))
        $weight = floatval($weight);
        $reps = floatval($reps);
        
        $physiologicalSquat1rm = $weight * ( 1 + ($reps/30) );
        return $physiologicalSquat1rm;
    }
    // =========================================================================
    // BMI
    // =========================================================================
    public function bMI($height, $weight){
        // BMI = SUM(weight/(height/100 * height/100))
        $height = intval($height);
        $weight = intval($weight);
        
        if($height > 0 && $weight > 0){
            $bmi = $weight / (($height/100) * ($height/100));

            return number_format((float)$bmi, 2, '.', '');
        }else{
            return 0;
        }
    }
     

    // =========================================================================
    // Physical Activity
    // =========================================================================
    public function moderateOrVigorousIntensityPhysicalActivity($days){
        // IF(days>4,0.5,IF(days=4,0.25,IF(days=3,0,IF(days=2,-0.5,IF(days=1,-0.75,IF(days=0,-1))))))
        
        $days = intval($days);
        if($days >= 5){
            $result = 0.5;
        }elseif($days == 4){
            $result = 0.25;
        }elseif($days == 3){
            $result = 0;
        }elseif($days == 2){
            $result = -0.5;
        }elseif($days == 1){
            $result = -0.75;
        }elseif($days == 0){
            $result = -1;
        }else{
            $result = -1;
        }
        
        return isset($result) ? $result : false;
        
    }
    
    public function muscleStrentheningPhysicalActivity($days){
        // IF(days>1,0,(IF(days=1,-0.25,(IF(days=0,-0.5,0)))))
        
        $days = intval($days);
        if($days >= 2){
            $result = 0;
        }elseif($days == 1){
            $result = -0.25;
        }else{
            $result = -0.5;
        }
        
        return isset($result) ? $result : false;
        
    }
    
    public function physicalActivity($moderate_vigorous_pa, $muscle_strenthening_pa, $slitetest = FALSE){
        // IF(
        //  AND($muscle_strenthening_pa = -0.25, $moderate_vigorous_pa > -1),
        //      $moderate_vigorous_pa+$muscle_strenthening_pa,
        //          IF(AND($muscle_strenthening_pa=-0.5,$moderate_vigorous_pa>-0.51),
        //              $moderate_vigorous_pa+$muscle_strenthening_pa,
        //                  IF(AND($muscle_strenthening_pa=-0.5,$moderate_vigorous_pa=-0.75),
        //                      -1,$moderate_vigorous_pa)))
        
        if($muscle_strenthening_pa == -0.25 && $moderate_vigorous_pa > -1){
            $result = $moderate_vigorous_pa+$muscle_strenthening_pa;
        }elseif($muscle_strenthening_pa == -0.5 && $moderate_vigorous_pa > -0.51){
            $result = $moderate_vigorous_pa+$muscle_strenthening_pa;
        }elseif($muscle_strenthening_pa == -0.5 && $moderate_vigorous_pa == -0.75){
            $result = -1;
        }else{
            $result = $moderate_vigorous_pa;
        }
        
        $result = (!$slitetest) ? $result : ($result * 2);
        
        return isset($result) ? $result : false;
    }
    
   public function physicalActivityResult($physicalActivity){
        $result = array();
        $color = '';
        $color_val = 0;
        if($physicalActivity > 0){
            if($physicalActivity >= 0.5 ){
                $color = 'android-green';
                $color_val=100;
            }elseif($physicalActivity >= 0.3 ){
                $color_val=80;
            }elseif($physicalActivity > 0 ){
                $color = 'light-blue';
                $color_val=70;
            }
        }elseif($physicalActivity == 0){
            $color = 'buff';
            $color_val=50;
        }elseif($physicalActivity < 0){
            if($physicalActivity <= -1 ){
                $color = 'red';
                $color_val=0;
            }elseif($physicalActivity <= -0.75 ){
                $color = 'apricot';
                $color_val=20;
            }elseif($physicalActivity <= -0.5 ){
                $color = 'big-foot-feet';
                $color_val=40;
            }else{
                $color = 'big-foot-feet';
                $color_val=40;
            }
        }
        
        if($physicalActivity > 0){
            $result = array(
                'level' => 'Low',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Congratulations - Your current physical activity levels indicate that you are physically active most days.  Being physically active and limiting your sedentary behaviour everyday is essential for your health and well-being."
                );
        }elseif($physicalActivity == 0){
            $result = array(
                'level' => 'Neutral',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Your current physical activity levels indicate that you are not meeting the Australian physical activity and sedentary behaviour guidelines for healthy living.  Being physically active and limiting your sedentary behaviour every day is essential for your health and well-being.  No matter how low your current levels of physical activity, doing any physical activities is better than none.  You can start slowly and gradually increase the amount you do, building up to be active on most, preferably all, days every week."
                );
        }elseif($physicalActivity < 0){
            $result = array(
                'level' => 'High',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Your current physical activity levels indicate that you are not meeting the Australian physical activity and sedentary behaviour guidelines for healthy living.  Being physically active and limiting your sedentary behaviour every day is essential for your health and well-being.  No matter how low your current levels of physical activity, doing any physical activities is better than none.  You can start slowly and gradually increase the amount you do, building up to be active on most, preferably all, days every week."
                );
        }
        
        return $result;
    }
    
    
    
    // =========================================================================
    // Smoking Habits
    // =========================================================================
    public function smokingHabits($gender, $smoking_habit_value, $slitetest = FALSE){
        // IF(AND($gender="Male",$smoking_habit_value=0),2,
        // IF(AND($gender="Male",$smoking_habit_value=1),1,
        // IF(AND($gender="Male",$smoking_habit_value=2),0,
        // IF(AND($gender="Male",$smoking_habit_value=3),-1,
        // IF(AND($gender="Male",$smoking_habit_value=4),-2,
        // IF(AND($gender="Male",$smoking_habit_value=5),-3,
        // IF(AND($gender="Male",$smoking_habit_value=6),-4,
        // IF(AND($gender="Female",$smoking_habit_value=0),2,
        // IF(AND($gender="Female",$smoking_habit_value=1),1,
        // IF(AND($gender="Female",$smoking_habit_value=2),0,
        // IF(AND($gender="Female",$smoking_habit_value=3),-2,
        // IF(AND($gender="Female",$smoking_habit_value=4),-4,-4))))))))))))
        
        if($gender == 'male'){
            if($smoking_habit_value == 0){
                $result = 2;
            }elseif($smoking_habit_value == 1){
                $result = 1;
            }elseif($smoking_habit_value == 2){
                $result = 0;
            }elseif($smoking_habit_value == 3){
                $result = -1;
            }elseif($smoking_habit_value == 4){
                $result = -2;
            }elseif($smoking_habit_value == 5){
                $result = -3;
            }elseif($smoking_habit_value == 6){
                $result = -4;
            }else{
                $result = -4;
            }
        }else{
            if($smoking_habit_value == 0){
                $result = 2;
            }elseif($smoking_habit_value == 1){
                $result = 1;
            }elseif($smoking_habit_value == 2){
                $result = 0;
            }elseif($smoking_habit_value == 3){
                $result = -2;
            }elseif($smoking_habit_value == 4){
                $result = -4;
            }else{
                $result = -4;
            }
        }
        
        $result = (!$slitetest) ? $result : ($result / 2);
        
        return isset($result) ? $result : false;
    }
    
    public function smokingHabitsResult($smokingHabits){
        $result = array();
        $color = '';
        $color_val = 0;
        
        if($smokingHabits > 0){
            if($smokingHabits >= 2 ){
                $color = 'android-green';
                $color_val = 100;
            }elseif($smokingHabits > 0 ){
                $color = 'light-blue';
                $color_val = 70;
            }
        }elseif($smokingHabits == 0){
            $color = 'buff';
            $color_val = 50;
        }elseif($smokingHabits < 0){
            if($smokingHabits <= -4 ){
                $color = 'red';
                $color_val = 0;
            }elseif($smokingHabits <= -3 ){
                $color = 'candy-pink';
                $color_val = 20;
            }elseif($smokingHabits <= -2 ){
                $color = 'apricot';
                $color_val = 20;
            }elseif($smokingHabits <= -1 ){
                $color = 'big-foot-feet';
                $color_val = 40;
            }else{
                $color = 'big-foot-feet';
                $color_val = 40;
            }
        }
        if($smokingHabits < 0){
            $result = array(
                'level' => 'High',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Your current smoking habits place you at high risk of developing many chronic diseases and ill health related to tobacco smoking.  Quitting can be hard - To quit smoking, repeated attempts are typically required.  Successfully quitting smoking can result in an increase in life expectancy of up to 10 years, depending on when you successfully give it up."
                );
        }elseif($smokingHabits == 0){
            $result = array(
                'level' => 'Neutral',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Congratulations, your risk of health problems related to smoking are significantly reduced as a result of quitting - Successfully quitting smoking can result in an increase in life expectancy of up to 10 years, depending on when you successfully give it up."
                );
        }elseif($smokingHabits > 0){
            $result = array(
                'level' => 'Low',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Congratualtions, your risk of health problems related to smoking tobacco are very low."
                );
        }
        
        return $result;
    }
    
    // =========================================================================
    // Alcohol Consumption
    // =========================================================================
    public function alcoholConsumptionPerWeekResult($gender, $per_week_value){
        
        //IF(C22=0,2,
        //IF(AND(C11="Male",C22<8),1,
        //IF(AND(C11="Female",C22<8),1,
        //IF(AND(C11="Male",C22<15),0,
        //IF(AND(C11="Female",C22<15),0,
        //IF(AND(C11="Male",C22<22),-1,
        //IF(AND(C11="Female",C22<22),-2,
        //IF(AND(C11="Male",C22<29),-2,
        //IF(AND(C11="Female",C22<28),-3,
        //IF(AND(C11="Male",C22<36),-3,
        //IF(AND(C11="Female",C22>27),-4,
        //IF(AND(C11="Male",C22>35),-4,
        //0))))))))))))
        
        if($per_week_value == 0){$result = 2;}
        elseif($gender == "male" && $per_week_value < 8){ $result = 1 ;}
        elseif($gender == "female" && $per_week_value < 8){ $result = 1 ;}
        elseif($gender == "male" && $per_week_value <15){ $result = 0 ;}
        elseif($gender == "female" && $per_week_value <15){ $result = 0 ;}
        elseif($gender == "male" && $per_week_value < 22){ $result = -1 ;}
        elseif($gender == "female" && $per_week_value < 22){ $result = -2 ;}
        elseif($gender == "male" && $per_week_value < 29){ $result = -2 ;}
        elseif($gender == "female" && $per_week_value < 28){ $result = -3 ;}
        elseif($gender == "male" && $per_week_value < 36){ $result = -3 ;}
        elseif($gender == "female" && $per_week_value > 27){ $result = -4 ;}
        elseif($gender == "male" && $per_week_value > 35){ $result = -4 ;}
        else{ $result = 0 ;}
        
        
        
        return isset($result) ? $result : false;
    }
    
    public function alcoholConsumptionInOneSittingResult($one_sitting_value){
        // IF($one_sitting_value<5,0,(IF($one_sitting_value<11,-1,(IF($one_sitting_value>10,-2,0)))))
        
        if($one_sitting_value < 5){
            $result = 0;
        }elseif($one_sitting_value > 4 && $one_sitting_value < 11){
            $result = -1;
        }elseif($one_sitting_value > 10){
            $result = -2;
        }else{
            $result = 0;
        }
        
        return isset($result) ? $result : false;
    }
    
    public function alcoholConsumptionTotal($alcohol_consumption_per_week, $alcohol_consumption_in_one_sitting, $slitetest = FALSE){
        //IF(AND($alcohol_consumption_in_one_sitting=-2,$alcohol_consumption_per_week=-4),-4,
        //IF(AND($alcohol_consumption_in_one_sitting=-2,$alcohol_consumption_per_week=-3),-4,
        //IF(AND($alcohol_consumption_in_one_sitting=-2,$alcohol_consumption_per_week>=-2),$alcohol_consumption_in_one_sitting+$alcohol_consumption_per_week,
        //IF(AND($alcohol_consumption_in_one_sitting=-1,$alcohol_consumption_per_week=-4),-4,
        //IF(AND($alcohol_consumption_in_one_sitting=-1,$alcohol_consumption_per_week>=-3),$alcohol_consumption_in_one_sitting+$alcohol_consumption_per_week,
        //$alcohol_consumption_per_week)))))
        
        if($alcohol_consumption_in_one_sitting == -2 && $alcohol_consumption_per_week == -4){
            $result = -4;
            
        }elseif($alcohol_consumption_in_one_sitting == -2 && $alcohol_consumption_per_week == -4){
            $result = -4;
            
        }elseif($alcohol_consumption_in_one_sitting == -2 && $alcohol_consumption_per_week >= -2){
            
            $result = $alcohol_consumption_in_one_sitting + $alcohol_consumption_per_week;
            
        }elseif($alcohol_consumption_in_one_sitting == -1 && $alcohol_consumption_per_week == -4){
            $result = -4;
            
        }elseif($alcohol_consumption_in_one_sitting == -1 && $alcohol_consumption_per_week >= -3){
            
            $result = $alcohol_consumption_in_one_sitting + $alcohol_consumption_per_week;
            
        }else{
            $result = $alcohol_consumption_per_week;
        }
        $result = (!$slitetest) ? $result : ($result / 2);
        
        return isset($result) ? $result : false;
        
    }
    
    public function alcoholConsumptionResult($alcoholConsumptionTotal){
        $result = array();
        $color = '';
        $color_val =0;
        
        if($alcoholConsumptionTotal > 0){
            if($alcoholConsumptionTotal >= 2 ){
                $color = 'android-green';
                $color_val=100;
            }elseif($alcoholConsumptionTotal > 0 ){
                $color = 'light-blue';
                $color_val=70;
            }
        }elseif($alcoholConsumptionTotal == 0){
            $color = 'buff';
            $color_val=50;
        }elseif($alcoholConsumptionTotal < 0){
            if($alcoholConsumptionTotal <= -4 ){
                $color = 'red';
                $color_val=0;
            }elseif($alcoholConsumptionTotal <= -3 ){
                $color = 'candy-pink';
                $color_val=20;
            }elseif($alcoholConsumptionTotal <= -2 ){
                $color = 'apricot';
                $color_val=20;
            }elseif($alcoholConsumptionTotal <= -1 ){
                $color = 'big-foot-feet';
                $color_val=40;
            }else{
                $color = 'big-foot-feet';
                $color_val=40;
            }
        }
        
        if($alcoholConsumptionTotal < 0){
            $result = array(
                'level' => 'High',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Your current Alcohol consumption places you at risk of developing health problems related to risky alcohol consumption.  Drinking no more than 2 standard drinks on any day reduces the lifetime risk of harm from alcohol-related disease or injury.  Drinking no more than 4 standard drinks on a single occasion reduces the risk of alcohol-related injury arising from that occasion."
                );
        }elseif($alcoholConsumptionTotal == 0){
            $result = array(
                'level' => 'Neutral',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Your current alcohol consumption is within the Australian guidelines to reduce health risks from drinking alcohol.  Drinking no more than 2 standard drinks on any day reduces the lifetime risk of harm from alcohol-related disease or injury.  Drinking no more than 4 standard drinks on a single occasion reduces the risk of alcohol-related injury arising from that occasion."
                );
        }elseif($alcoholConsumptionTotal > 0){
            $result = array(
                'level' => 'Low',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Congratulations, you are at low risk of developing health problems related to risky Alcohol consumption."
                );
        }
        
        return $result;
    }
    
    // =========================================================================
    // Nutrition
    // =========================================================================
    public function nutrition($nutrition_value, $slitetest = FALSE){
        //IF($nutrition_value=10,2,
        //IF($nutrition_value=9,1.5,
        //IF($nutrition_value=8,1,
        //IF($nutrition_value=7,0.5,
        //IF($nutrition_value=6,0,
        //IF($nutrition_value=5,-1,
        //IF($nutrition_value=4,-2,
        //IF($nutrition_value=3,-3,
        //-4))))))))
        
        if($nutrition_value == 10){
            $result = 2;
        }elseif($nutrition_value == 9){
            $result = 1.5;
        }elseif($nutrition_value == 8){
            $result = 1;
        }elseif($nutrition_value == 7){
            $result = 0.5;
        }elseif($nutrition_value == 6){
            $result = 0;
        }elseif($nutrition_value == 5){
            $result = -1;
        }elseif($nutrition_value == 4){
            $result = -2;
        }elseif($nutrition_value == 3){
            $result = -3;
        }else{
            $result = -4;
        }
        
        $result = (!$slitetest) ? $result : ($result / 2);
        
        return isset($result) ? $result : false;
    }
    
    
    public function nutritionResult($nutrition){
        $result = array();
        $color = '';
        $color_val=0;
        
        if($nutrition > 0){
            if($nutrition >= 2 ){
                $color = 'android-green';
                $color_val=100;
            }elseif($nutrition >= 1.5 ){
                $color = 'light-blue';
                $color_val=80;
            }elseif($nutrition >= 1 ){
                $color = 'light-android-green';
                $color_val=80;
            }elseif($nutrition >= 0.5 ){
                $color = 'transparent-android-green';
                $color_val=70;
            }elseif($nutrition > 0 ){
                $color = 'transparent-android-green';
                $color_val=70;
            }
        }elseif($nutrition == 0){
            $color = 'buff';
            $color_val=50;
        }elseif($nutrition < 0){
            if($nutrition <= -4 ){
                $color = 'red';
                $color_val=0;
            }elseif($nutrition <= -3 ){
                $color = 'candy-pink';
                $color_val=20;
            }elseif($nutrition <= -2 ){
                $color = 'apricot';
                $color_val=20;
            }elseif($nutrition <= -1 ){
                $color = 'big-foot-feet';
                $color_val=40;
            }else{
                $color = 'big-foot-feet';
                $color_val=40;
            }
        }
        
        if($nutrition < 0){
            $result = array(
                'level' => 'High',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Your current diet suggests you are not eating for health and are at risk of developing health problems related to poor nutrition.  The Australian Dietary guidelines advise us to - 
                    <ol>
                        <li>Choose amounts of nutritious food and drinks to meet your energy needs.</li>
                        <li>
                            Enjoy a wide variety of nutritious foods from these five food groups every day:
                            <ul>
                                <li>Plenty of vegetables of different types and colours, and legumes/beans</li>
                                <li>Fruit</li>
                                <li>Grain (cereal) foods, mostly wholegrain and/or high cereal fibre varieties, such as breads, cereals, rice, pasta, noodles, polenta, couscous, oats, quinoa and barley</li>
                                <li>Lean meats and poultry, fish, eggs, tofu, nuts and seeds, and legumes/beans</li>
                                <li>Milk, yoghurt, cheese and/or their alternatives, mostly reduced fat And drink plenty of water.</li>
                            </ul> 
                        </li>
                        <li>Limit intake of foods containing saturated fat, added salt, added sugars and alcohol.</li>
                    </ol>" 
                );
        }elseif($nutrition == 0){
            $result = array(
                'level' => 'Neutral',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "You are almost there, but your current diet suggests you are not eating for health and are at risk of developing health problems related to poor nutrition.  The Australian Dietary guidelines advise us to - 
                    <ol>
                        <li>Choose amounts of nutritious food and drinks to meet your energy needs.</li>
                        <li>
                            Enjoy a wide variety of nutritious foods from these five food groups every day:
                            <ul>
                                <li>Plenty of vegetables of different types and colours, and legumes/beans</li>
                                <li>Fruit</li>
                                <li>Grain (cereal) foods, mostly wholegrain and/or high cereal fibre varieties, such as breads, cereals, rice, pasta, noodles, polenta, couscous, oats, quinoa and barley</li>
                                <li>Lean meats and poultry, fish, eggs, tofu, nuts and seeds, and legumes/beans</li>
                                <li>Milk, yoghurt, cheese and/or their alternatives, mostly reduced fat And drink plenty of water.</li>
                            </ul> 
                        </li>
                        <li>Limit intake of foods containing saturated fat, added salt, added sugars and alcohol.</li>
                    </ol>" 
                );
        }elseif($nutrition > 0){
            $result = array(
                'level' => 'Low',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Congratulations, your current diet suggests that you are a pretty healthy eater and are at low risk of developing health problems related to poor diet."
                );
        }
        
        return $result;
    }
    
    // =========================================================================
    // Mental Health
    // =========================================================================
    public function mentalHealth($mental_health_value){
        //IF($mental_health_value<=10,1,
        //IF($mental_health_value<=14,0.5,
        //IF($mental_health_value<=19,0,
        //IF($mental_health_value<=24,-0.5,
        //IF($mental_health_value<=29,-1,
        //IF($mental_health_value<=34,-1.5,
        //IF($mental_health_value>=35,-2,0)))))))
        
        if($mental_health_value <= 10){
            $result = 1;
        }elseif($mental_health_value <= 14){
            $result = 0.5;
        }elseif($mental_health_value <= 19){
            $result = 0;
        }elseif($mental_health_value <= 24){
            $result = -0.5;
        }elseif($mental_health_value <= 29){
            $result = -1;
        }elseif($mental_health_value <= 34){
            $result = -1.5;
        }elseif($mental_health_value >= 35){
            $result = -2;
        }else{
            $result = 0;
        }
        
        return isset($result) ? $result : false;
    }
    
    public function mentalHealthResult($mentalHealth){
        $result = array();
        $color = '';
        $color_val = 0;
        
        if($mentalHealth > 0){
            if($mentalHealth >= 1 ){
                $color = 'android-green';
                $color_val = 100;
            }elseif($mentalHealth >= 0.5 ){
                $color = 'light-blue';
                $color_val = 80;
            }elseif($mentalHealth > 0 ){
                $color = 'light-blue';
                $color_val = 70;
            }
        }elseif($mentalHealth == 0){
            $color = 'buff';
            $color_val = 50;
        }elseif($mentalHealth < 0){
            if($mentalHealth <= -2 ){
                $color = 'red';
                $color_val = 0;
            }elseif($mentalHealth <= -1.5 ){
                $color = 'apricot';
                $color_val = 20;
            }elseif($mentalHealth <= -1 ){
                $color = 'big-foot-feet';
                $color_val = 20;
            }elseif($mentalHealth <= -0.5 ){
                $color = 'light-big-foot-feet';
                $color_val = 40;
            }else{
                $color = 'light-big-foot-feet';
                $color_val = 40;
            }
        }
        if($mentalHealth < 0){
            $result = array(
                'level' => 'High',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Based on your assessment you may be at risk of health problems relating to depression or anxiety - It can be difficult for people with depression or anxiety to take that first step in getting help. These conditions can reduce people’s motivation or confidence to take action, and some may feel embarrassed. However, effective treatments are available so while you might be hesitant, it's worth seeking support. For more information visit <a href='www.beyondblue.org.au' target='_blank'>www.beyondblue.org.au</a>."
                );
        }elseif($mentalHealth == 0){
            $result = array(
                'level' => 'Neutral',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Based on your assessment you are not at increased risk of health problems relating to your mental health."
                );
        }elseif($mentalHealth > 0){
            $result = array(
                'level' => 'Low',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Good news - based on your assessment you are at low risk of health problems relating to your mental health."
                );
        }
        
        return $result;
    }
    
    // =========================================================================
    // Risk Profile
    // =========================================================================
    public function riskProfile($risk_profile_value){
        //IF($risk_profile_value<30,1,
        //IF($risk_profile_value<40,0.5,
        //IF($risk_profile_value<50,0,
        //IF($risk_profile_value<55,-0.5,
        //IF($risk_profile_value<60,-1,
        //IF($risk_profile_value<65,-1.5,
        //IF($risk_profile_value>64,-2)))))))
        
        if($risk_profile_value < 30){
            $result = 1;
        }elseif($risk_profile_value < 40){
            $result = 0.5;
        }elseif($risk_profile_value < 50){
            $result = 0;
        }elseif($risk_profile_value < 55){
            $result = -0.5;
        }elseif($risk_profile_value < 60){
            $result = -1;
        }elseif($risk_profile_value < 65){
            $result = -1.5;
        }elseif($risk_profile_value > 64){
            $result = -2;
        }
        
        return isset($result) ? $result : false;
    }
    
    public function riskProfileResult($riskProfile){
        $result = array();
        $color = '';
        $color_val = 0;
        if($riskProfile > 0){
            if($riskProfile >= 1 ){
                $color = 'android-green';
                $color_val =100;
            }elseif($riskProfile >= 0.5 ){
                $color = 'light-blue';
                $color_val =80;
            }elseif($riskProfile > 0 ){
                $color = 'light-blue';
                $color_val =70;
            }
        }elseif($riskProfile == 0){
            $color = 'buff';
            $color_val =50;
        }elseif($riskProfile < 0){
            if($riskProfile <= -2 ){
                $color = 'red';
                $color_val =0;
            }elseif($riskProfile <= -1.5 ){
                $color = 'candy-pink';
                $color_val =20;
            }elseif($riskProfile <= -1 ){
                $color = 'apricot';
                $color_val =20;
            }elseif($riskProfile <= -0.5 ){
                $color = 'big-foot-feet';
                $color_val =40;
            }else{
                $color = 'big-foot-feet';
                $color_val =40;
            }
        }
        if($riskProfile < 0){
            $result = array(
                'level' => 'High',
                'color' => $color,
                'color_val' => $color_val,
                'text' => " Your answers indicate that you are at increased risk of Injury or Illness as a result of your 'Risk taking' profile."
                );
        }elseif($riskProfile == 0){
            $result = array(
                'level' => 'Neutral',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Your answers indicate that you are at moderate risk of Injury or Illness as a result of your 'Risk taking' profile."
                );
        }elseif($riskProfile > 0){
            $result = array(
                'level' => 'Low',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Your answers indicate that you are at low risk of Injury or Illness as a result of your 'Risk taking' profile."
                );
        }
        
        return $result;
    }
    
    // =========================================================================
    // Body Fat
    // =========================================================================
    public function bodyFat($gender, $body_fat_value){
        //IF(AND($gender="Male",$body_fat_value<12),1,
        //IF(AND($gender="Male",$body_fat_value<15.5),0.5,
        //IF(AND($gender="Male",$body_fat_value<19),0,
        //IF(AND($gender="Male",$body_fat_value<22),-0.5,
        //IF(AND($gender="Male",$body_fat_value<25),-1,
        //IF(AND($gender="Male",$body_fat_value<28),-1.5,
        //IF(AND($gender="Male",$body_fat_value>27.99),-2,
        //IF(AND($gender="Female",$body_fat_value<18),1,
        //IF(AND($gender="Female",$body_fat_value<23),0.5,
        //IF(AND($gender="Female",$body_fat_value<28),0,
        //IF(AND($gender="Female",$body_fat_value<31),-0.5,
        //IF(AND($gender="Female",$body_fat_value<34),-1,
        //IF(AND($gender="Female",$body_fat_value<37),-1.5,
        //IF(AND($gender="Female",$body_fat_value>36.99),-2,
        //0))))))))))))))
        $gender = trim($gender);
        $gender = strtolower($gender);
        $body_fat_value = intval($body_fat_value);
        
        
        if($gender == "male" && $body_fat_value < 12){
            $result = 1;
        }elseif($gender == "male" && $body_fat_value < 15.5){
            $result = 0.5;
        }elseif($gender == "male" && $body_fat_value < 19){
            $result = 0;
        }elseif($gender == "male" && $body_fat_value < 22){
            $result = -0.5;
        }elseif($gender == "male" && $body_fat_value < 25){
            $result = -1;
        }elseif($gender == "male" && $body_fat_value < 28){
            $result = -1.5;
        }elseif($gender == "male" && $body_fat_value > 27.99){
            $result = -2;
        }elseif($gender == "female" && $body_fat_value < 18){
            $result = 1;
        }elseif($gender == "female" && $body_fat_value < 23){
            $result = 0.5;
        }elseif($gender == "female" && $body_fat_value < 28){
            $result = 0;
        }elseif($gender == "female" && $body_fat_value < 31){
            $result = -0.5;
        }elseif($gender == "female" && $body_fat_value < 34){
            $result = -1;
        }elseif($gender == "female" && $body_fat_value < 37){
            $result = -1.5;
        }elseif($gender == "female" && $body_fat_value > 36.99){
            $result = -2;
        }else{
            $result = 0;
        }
        
        return isset($result) ? $result : false;
    }
    
    public function bodyFatResult($bodyFat){
        $result = array();
        $color = '';
        $color_val = 0;
        
        if($bodyFat > 0){
            if($bodyFat >= 1 ){
                $color = 'android-green';
                $color_val=100;
            }elseif($bodyFat >= 0.5 ){
                $color = 'light-blue';
                $color_val = 80;
            }elseif($bodyFat > 0 ){
                $color = 'light-blue';
                $color_val = 70;
            }
        }elseif($bodyFat == 0){
            $color = 'buff';
            $color_val = 50;
        }elseif($bodyFat < 0){
            if($bodyFat <= -2 ){
                $color = 'red';
                $color_val = 0;
            }elseif($bodyFat <= -1.5 ){
                $color = 'candy-pink';
                $color_val = 20;
            }elseif($bodyFat <= -1 ){
                $color = 'apricot';
                $color_val = 20;
            }elseif($bodyFat <= -0.5 ){
                $color = 'big-foot-feet';
                $color_val = 40;
            }else{
                $color = 'big-foot-feet';
                $color_val = 40;
            }
        }
        
        if($bodyFat < 0){
            $result = array(
                'level' => 'High',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Your current body fat % places you at increased risk of health problems related to a high body fat %.  Acceptable body fat % is considered to be: below 19% for males and below 28% for females."
                );
        }elseif($bodyFat == 0){
            $result = array(
                'level' => 'Neutral',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Your body fat % is within the 'normal' range, you are not at increased risk of health problems related to high body fat %."
                );
        }elseif($bodyFat > 0){
            $result = array(
                'level' => 'Low',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Congratulations, you are at low risk of health problems related to a high body fat %."
                );
        }
        
        return $result;
    }
    
    // =========================================================================
    // Blood Pressure
    // =========================================================================
    public function bloodPressure($systolic, $diastolic){
        //IF(AND($systolic<90,$diastolic<60),0,
        //IF(AND($systolic<110,$diastolic<70),1,
        //IF(AND($systolic<120,$diastolic<80),0.5,
        //IF(AND($systolic<120,$diastolic<85),0,
        //IF(AND($systolic<120,$diastolic<90),-0.5,
        //IF(AND($systolic<120,$diastolic<100),-1,
        //IF(AND($systolic<120,$diastolic<110),-1.5,
        //IF(AND($systolic<120,$diastolic>109),-2,
        //IF(AND($systolic<130,$diastolic<85),0,
        //IF(AND($systolic<130,$diastolic<90),-0.5,
        //IF(AND($systolic<130,$diastolic<100),-1,
        //IF(AND($systolic<130,$diastolic<110),-1.5,
        //IF(AND($systolic<130,$diastolic>109),2,
        //IF(AND($systolic<140,$diastolic<90),-0.5,
        //IF(AND($systolic<140,$diastolic<100),-1,
        //IF(AND($systolic<140,$diastolic<110),-1.5,
        //IF(AND($systolic<140,$diastolic>109),-2,
        //IF(AND($systolic<160,$diastolic<100),-1,
        //IF(AND($systolic<160,$diastolic<110),-1.5,
        //IF(AND($systolic<160,$diastolic>109),-2,
        //IF(AND($systolic<180,$diastolic<110),-1.5,
        //IF(AND($systolic<180,$diastolic>109),-2,
        //IF(AND($systolic>179,$diastolic<100),-2,
        //-2)))))))))))))))))))))))
        
        $systolic = intval($systolic);
        $diastolic = intval($diastolic);
        
        if($systolic < 80 || $diastolic < 50){
            $result = 0;
        }else{
            
            if($systolic<90 && $diastolic<60){ $result = 0;
            }elseif($systolic<110 && $diastolic<70){ $result = 1;
            }elseif($systolic<120 && $diastolic<80){ $result = 0.5;
            }elseif($systolic<120 && $diastolic<85){ $result = 0;
            }elseif($systolic<120 && $diastolic<90){ $result = -0.5;
            }elseif($systolic<120 && $diastolic<100){ $result = -1;
            }elseif($systolic<120 && $diastolic<110){ $result = -1.5;
            }elseif($systolic<120 && $diastolic>109){ $result = -2;
            }elseif($systolic<130 && $diastolic<85){ $result = 0;
            }elseif($systolic<130 && $diastolic<90){ $result = -0.5;
            }elseif($systolic<130 && $diastolic<100){ $result = -1;
            }elseif($systolic<130 && $diastolic<110){ $result = -1.5;
            }elseif($systolic<130 && $diastolic>109){ $result = 2;
            }elseif($systolic<140 && $diastolic<90){ $result = -0.5;
            }elseif($systolic<140 && $diastolic<100){ $result = -1;
            }elseif($systolic<140 && $diastolic<110){ $result = -1.5;
            }elseif($systolic<140 && $diastolic>109){ $result = -2;
            }elseif($systolic<160 && $diastolic<100){ $result = -1;
            }elseif($systolic<160 && $diastolic<110){ $result = -1.5;
            }elseif($systolic<160 && $diastolic>109){ $result = -2;
            }elseif($systolic<180 && $diastolic<110){ $result = -1.5;
            }elseif($systolic<180 && $diastolic>109){ $result = -2;
            }elseif($systolic>179 && $diastolic<100){ $result = -2;
            }else{
                $result = -2;
            }
            
        }
        
        return isset($result) ? $result : false;
    }
    
    
    public function bloodPressureResult($bloodPressure, $systolic = NULL, $diastolic = NULL){
        $result = array();
        $color = '';
        $color_val = 0;
        
        if($bloodPressure > 0){
            if($bloodPressure >= 1 ){
                $color = 'android-green';
                $color_val = 100;
            }elseif($bloodPressure >= 0.5 ){
                $color = 'light-blue';
                $color_val = 80;
            }elseif($bloodPressure > 0 ){
                $color = 'light-blue';
                $color_val = 70;
            }
        }elseif($bloodPressure == 0){
            $color = 'buff';
            $color_val = 50;
        }elseif($bloodPressure < 0){
            if($bloodPressure <= -2 ){
                $color = 'red';
                $color_val = 0;
            }elseif($bloodPressure <= -1.5 ){
                $color = 'candy-pink';
                $color_val = 20;
            }elseif($bloodPressure <= -1 ){
                $color = 'apricot';
                $color_val = 20;
            }elseif($bloodPressure <= -0.5 ){
                $color = 'big-foot-feet';
                $color_val = 40;
            }else{
                $color = 'big-foot-feet';
                $color_val = 40;
            }
        }
        
        if($bloodPressure < 0){
            $result = array(
                'level' => 'High',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Your blood pressure is considered 'high', you are at increased risk of health problems related to high blood pressure.  If your blood pressure remains high, it can lead to serious problems such as a heart attack, stroke, heart failure or kidney disease. High blood pressure usually does not give warning signs. You can have high blood pressure and feel perfectly well. The only way to find out if your blood pressure is high is by having it checked regularly by your doctor."
                );
        }elseif($bloodPressure == 0){
            if($systolic != NULL && $diastolic != NULL){
                if($systolic<90 && $diastolic<60){
                    $result = array(
                        'level' => 'Neutral',
                        'color' => $color,
                        'color_val' => $color_val,
                        'text' => "Your blood pressure is within the 'low' range, as a result you are at low risk of health problems related to high blood pressure but could be affected by other health conditions associated with Low Blood Pressure."
                        );
                }else{
                    $result = array(
                        'level' => 'Neutral',
                        'color' => $color,
                        'color_val' => $color_val,
                        'text' => "Your blood pressure is within the 'normal range', it is worth keeping an eye on this over time to ensure it does not move into the 'high' category.   High blood pressure usually does not give warning signs. You can have high blood pressure and feel perfectly well. The only way to find out if your blood pressure is high is by having it checked regularly by your doctor."
                        );
                }
            }else{
                $result = array(
                    'level' => 'Neutral',
                    'color' => $color,
                    'color_val' => $color_val,
                    'text' => "Your blood pressure is within the 'normal range', it is worth keeping an eye on this over time to ensure it does not move into the 'high' category.   High blood pressure usually does not give warning signs. You can have high blood pressure and feel perfectly well. The only way to find out if your blood pressure is high is by having it checked regularly by your doctor."
                    );
            }
        }elseif($bloodPressure > 0){
            $result = array(
                'level' => 'Low',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Congratulations, your blood pressure is within the 'normal healthy' range, you are at low risk of health problems related to high blood pressure."
                );
        }
        
        return $result;
    }
    // =========================================================================
    // Blood Oxygen
    // =========================================================================
    public function bloodOxygen($blood_oxygen_value){
        
        
//        if($blood_oxygen_value>98.99){ $result = 0.5; }
//        elseif($blood_oxygen_value>96.99){ $result = 0.25; }
//        elseif($blood_oxygen_value>94.99){ $result = 0; }
//        elseif($blood_oxygen_value>92.99){ $result = -0.25; }
//        elseif($blood_oxygen_value>91.49){ $result = -0.5; }
//        elseif($blood_oxygen_value>89.99){ $result = -0.75; }
//        elseif($blood_oxygen_value<90){ $result = -1; }
        
        //new
        if( strtolower(trim($blood_oxygen_value)) != 'na' ){
            if($blood_oxygen_value>98.99){ $result = 0.5; }
            elseif($blood_oxygen_value>96.99){ $result = 0.25; }
            elseif($blood_oxygen_value>94.99){ $result = 0; }
            elseif($blood_oxygen_value>92.99){ $result = -0.25; }
            elseif($blood_oxygen_value>91.49){ $result = -0.5; }
            elseif($blood_oxygen_value>89.99){ $result = -0.75; }
            elseif($blood_oxygen_value<90){ $result = -1; }
        }else{
            $result = 0;
        }
        
        return isset($result) ? $result : false;
    }
    
    public function bloodOxygenResult($bloodOxygen){
        $result = array();
        $color = '';
        $color_val = 0;
        
        if($bloodOxygen > 0){
            if($bloodOxygen >= 0.5 ){
                $color = 'android-green';
                $color_val = 100;
            }elseif($bloodOxygen >= 0.25 ){
                $color = 'light-blue';
                $color_val = 80;
            }elseif($bloodOxygen > 0 ){
                $color = 'light-blue';
                $color_val = 70;
            }
        }elseif($bloodOxygen == 0){
            $color = 'buff';
            $color_val = 50;
        }elseif($bloodOxygen < 0){
            if($bloodOxygen <= -1 ){
                $color = 'red';
                $color_val = 0;
            }elseif($bloodOxygen <= -0.75 ){
                $color = 'candy-pink';
                $color_val = 20;
            }elseif($bloodOxygen <= -0.5 ){
                $color = 'apricot';
                $color_val = 20;
            }elseif($bloodOxygen <= -0.25 ){
                $color = 'big-foot-feet';
                $color_val = 40;
            }else{
                $color = 'big-foot-feet';
                $color_val = 40;
            }
        }
        
        if($bloodOxygen < 0){
            $result = array(
                'level' => 'High',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Your Blood oxygen % is below the normal range, you may be at risk of health problems related to low blood oxygen levels. In order to function properly, your body needs a certain level of oxygen circulating in the blood to cells and tissues. When this level of oxygen falls below a certain amount, hypoxemia occurs and you may experience shortness of breath."
                );
        }elseif($bloodOxygen == 0){
            $result = array(
                'level' => 'Neutral',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Your Blood oxygen % is within the normal range, this indicates you are not at increased risk of health problems related to low Blood oxygen levels."
                );
        }elseif($bloodOxygen > 0){
            $result = array(
                'level' => 'Low',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Your Blood oxygen % is within the normal range, this indicates you are at low risk of health problems related to low Blood oxygen levels."
                );
        }
        
        return $result;
    }
    
    // =========================================================================
    // VO2
    // =========================================================================
    
    public function vo2BikeTest($gender, $weight, $age, $watts){
        //IF($gender="Male", 10.51*$watts+6.35*$weight-10.49*$age+519.3,
        //9.39*$watts+7.7*$weight-5.88*$age+136)/100
        
        if($gender == "male"){
            $result = 10.51 * $watts + 6.35 * $weight - 10.49 * $age + 519.3;
        }else{
            $result = 9.39 * $watts + 7.7 * $weight - 5.88 * $age + 136;
        }
        
        $result = $result/100;
        
        return $result;
    }
    
    public function vo2RestingHrTest($age, $rhr){
        //15.3*(208-0.7*$age)/M32
        $result = 15.3*(208-0.7*$age)/$rhr;
        
        return $result;
    }
    
    
    public function vo2TreadmillTest($gender, $weight, $time, $beat_per_second){
        //IF($gender="male",108.844-0.1636*$weight-1.438*$time/60-0.1928*$beat_per_second,
        //100.5-0.1636*$weight-1.438*$time/60-0.1928*$beat_per_second)
        
        //IF($gender="male",108.844-0.1636*($weight*2.2)-1.438*$time/60-0.1928*$beat_per_second,
        //100.5-0.1636*($weight*2.2)-1.438*$time/60-0.1928*$beat_per_second)
        
        if($gender == "male"){
            $result = 108.844-0.1636*($weight*2.2)-1.438*$time/60-0.1928*$beat_per_second;
        }else{
            $result = 100.5-0.1636*($weight*2.2)-1.438*$time/60-0.1928*$beat_per_second;
        }
//        if($gender == "male"){
//            $result = 108.844-0.1636*$weight-1.438*$time/60-0.1928*$beat_per_second;
//        }else{
//            $result = 100.5-0.1636*$weight-1.438*$time/60-0.1928*$beat_per_second;
//        }
        
        return $result;
    }
    
    public function vo2QcStepTest($gender, $heart_rate){
        //=IF(C11="male",(111.33-(0.42*M37)),(65.81-(0.1847*M37)))
        
        if($gender == "male"){
            $result = 111.33 - (0.42 * $heart_rate);
        }else{
            $result = 65.81 - (0.1847 * $heart_rate);
        }
        
        return $result;
    }
    
    public function vo2DirectEntry($vo2){
        return $vo2;
    }
    public function vo2BeepTest($level, $shuttle){
        if($level == 1 && $shuttle == 1){ $result = 17.4; }
        elseif($level == 1 && $shuttle == 2){ $result = 17.8; }
        elseif($level == 1 && $shuttle == 3){ $result = 18.2; }
        elseif($level == 1 && $shuttle == 4){ $result = 18.6; }
        elseif($level == 1 && $shuttle == 5){ $result = 19; }
        elseif($level == 1 && $shuttle == 6){ $result = 19.4; }
        elseif($level == 1 && $shuttle == 7){ $result = 19.8; }
        elseif($level == 2 && $shuttle == 1){ $result = 20.2; }
        elseif($level == 2 && $shuttle == 2){ $result = 20.6; }
        elseif($level == 2 && $shuttle == 3){ $result = 21; }
        elseif($level == 2 && $shuttle == 4){ $result = 21.4; }
        elseif($level == 2 && $shuttle == 5){ $result = 21.8; }
        elseif($level == 2 && $shuttle == 6){ $result = 22.2; }
        elseif($level == 2 && $shuttle == 7){ $result = 22.6; }
        elseif($level == 2 && $shuttle == 8){ $result = 23; }
        elseif($level == 3 && $shuttle == 1){ $result = 23.4; }
        elseif($level == 3 && $shuttle == 2){ $result = 23.8; }
        elseif($level == 3 && $shuttle == 3){ $result = 24.2; }
        elseif($level == 3 && $shuttle == 4){ $result = 24.6; }
        elseif($level == 3 && $shuttle == 5){ $result = 25; }
        elseif($level == 3 && $shuttle == 6){ $result = 25.4; }
        elseif($level == 3 && $shuttle == 7){ $result = 25.8; }
        elseif($level == 3 && $shuttle == 8){ $result = 26.2; }
        elseif($level == 4 && $shuttle == 2){ $result = 26.8; }
        elseif($level == 4 && $shuttle == 3){ $result = 27.2; }
        elseif($level == 4 && $shuttle == 4){ $result = 27.6; }
        elseif($level == 4 && $shuttle == 5){ $result = 28; }
        elseif($level == 4 && $shuttle == 6){ $result = 28.3; }
        elseif($level == 4 && $shuttle == 7){ $result = 28.7; }
        elseif($level == 4 && $shuttle == 8){ $result = 29.1; }
        elseif($level == 4 && $shuttle == 9){ $result = 29.5; }
        elseif($level == 5 && $shuttle == 1){ $result = 29.8; }
        elseif($level == 5 && $shuttle == 2){ $result = 30.2; }
        elseif($level == 5 && $shuttle == 3){ $result = 30.6; }
        elseif($level == 5 && $shuttle == 4){ $result = 31; }
        elseif($level == 5 && $shuttle == 5){ $result = 31.4; }
        elseif($level == 5 && $shuttle == 6){ $result = 31.8; }
        elseif($level == 5 && $shuttle == 7){ $result = 32.2; }
        elseif($level == 5 && $shuttle == 8){ $result = 32.6; }
        elseif($level == 5 && $shuttle == 9){ $result = 32.9; }
        elseif($level == 6 && $shuttle == 1){ $result = 33.2; }
        elseif($level == 6 && $shuttle == 2){ $result = 33.6; }
        elseif($level == 6 && $shuttle == 3){ $result = 34; }
        elseif($level == 6 && $shuttle == 4){ $result = 34.3; }
        elseif($level == 6 && $shuttle == 5){ $result = 34.7; }
        elseif($level == 6 &&  $shuttle == 6){ $result = 35; }
        elseif($level == 6 &&  $shuttle == 7){ $result = 35.3; }
        elseif($level == 6 &&  $shuttle == 8){ $result = 35.7; }
        elseif($level == 6 &&  $shuttle == 9){ $result = 36.1; }
        elseif($level == 6 &&  $shuttle == 10){ $result = 36.4; }
        elseif($level == 7 &&  $shuttle == 1){ $result = 36.7; }
        elseif($level == 7 &&  $shuttle == 2){ $result = 37.1; }
        elseif($level == 7 &&  $shuttle == 3){ $result = 37.4; }
        elseif($level == 7 &&  $shuttle == 4){ $result = 37.8; }
        elseif($level == 7 &&  $shuttle == 5){ $result = 38.1; }
        elseif($level == 7 &&  $shuttle == 6){ $result = 38.5; }
        elseif($level == 7 &&  $shuttle == 7){ $result = 38.8; }
        elseif($level == 7 &&  $shuttle == 8){ $result = 39.2; }
        elseif($level == 7 &&  $shuttle == 9){ $result = 39.5; }
        elseif($level == 7 &&  $shuttle == 10){ $result = 39.9; }
        elseif($level == 8 &&  $shuttle == 1){ $result = 40.2; }
        elseif($level == 8 &&  $shuttle == 2){ $result = 40.5; }
        elseif($level == 8 &&  $shuttle == 3){ $result = 40.8; }
        elseif($level == 8 &&  $shuttle == 4){ $result = 41.1; }
        elseif($level == 8 &&  $shuttle == 5){ $result = 41.4; }
        elseif($level == 8 &&  $shuttle == 6){ $result = 41.8; }
        elseif($level == 8 &&  $shuttle == 7){ $result = 42.1; }
        elseif($level == 8 &&  $shuttle == 8){ $result = 42.4; }
        elseif($level == 8 &&  $shuttle == 9){ $result = 42.7; }
        elseif($level == 8 &&  $shuttle == 10){ $result = 43; }
        elseif($level == 8 &&  $shuttle == 11){ $result = 43.3; }
        elseif($level == 9 &&  $shuttle == 1){ $result = 43.6; }
        elseif($level == 9 &&  $shuttle == 2){ $result = 43.9; }
        elseif($level == 9 &&  $shuttle == 3){ $result = 44.2; }
        elseif($level == 9 &&  $shuttle == 4){ $result = 44.5; }
        elseif($level == 9 &&  $shuttle == 5){ $result = 44.9; }
        elseif($level == 9 &&  $shuttle == 6){ $result = 45.2; }
        elseif($level == 9 &&  $shuttle == 7){ $result = 45.5; }
        elseif($level == 9 &&  $shuttle == 8){ $result = 45.8; }
        elseif($level == 9 &&  $shuttle == 9){ $result = 46.1; }
        elseif($level == 9 &&  $shuttle == 10){ $result = 46.4; }
        elseif($level == 9 &&  $shuttle == 11){ $result = 46.8; }
        elseif($level == 10 &&  $shuttle == 1){ $result = 47.1; }
        elseif($level == 10 &&  $shuttle == 2){ $result = 47.4; }
        elseif($level == 10 &&  $shuttle == 3){ $result = 47.7; }
        elseif($level == 10 &&  $shuttle == 4){ $result = 48; }
        elseif($level == 10 &&  $shuttle == 5){ $result = 48.4; }
        elseif($level == 10 &&  $shuttle == 6){ $result = 48.7; }
        elseif($level == 10 &&  $shuttle == 7){ $result = 49; }
        elseif($level == 10 &&  $shuttle == 8){ $result = 49.3; }
        elseif($level == 10 &&  $shuttle == 9){ $result = 49.6; }
        elseif($level == 10 &&  $shuttle == 10){ $result = 49.9; }
        elseif($level == 10 &&  $shuttle == 11){ $result = 50.2; }
        elseif($level == 11 &&  $shuttle == 1){ $result = 50.5; }
        elseif($level == 11 &&  $shuttle == 2){ $result = 50.8; }
        elseif($level == 11 &&  $shuttle == 3){ $result = 51.1; }
        elseif($level == 11 &&  $shuttle == 4){ $result = 51.4; }
        elseif($level == 11 &&  $shuttle == 5){ $result = 51.6; }
        elseif($level == 11 &&  $shuttle == 6){ $result = 51.9; }
        elseif($level == 11 &&  $shuttle == 7){ $result = 52.2; }
        elseif($level == 11 &&  $shuttle == 8){ $result = 52.5; }
        elseif($level == 11 &&  $shuttle == 9){ $result = 52.8; }
        elseif($level == 11 &&  $shuttle == 10){ $result = 53.1; }
        elseif($level == 11 &&  $shuttle == 11){ $result = 53.4; }
        elseif($level == 11 &&  $shuttle == 12){ $result = 53.7; }
        elseif($level == 12 &&  $shuttle == 1){ $result = 54; }
        elseif($level == 12 &&  $shuttle == 2){ $result = 54.3; }
        elseif($level == 12 &&  $shuttle == 3){ $result = 54.5; }
        elseif($level == 12 &&  $shuttle == 4){ $result = 54.8; }
        elseif($level == 12 &&  $shuttle == 5){ $result = 55.1; }
        elseif($level == 12 &&  $shuttle == 6){ $result = 55.4; }
        elseif($level == 12 &&  $shuttle == 7){ $result = 55.7; }
        elseif($level == 12 &&  $shuttle == 8){ $result = 56; }
        elseif($level == 12 &&  $shuttle == 9){ $result = 56.3; }
        elseif($level == 12 &&  $shuttle == 10){ $result = 56.5; }
        elseif($level == 12 &&  $shuttle == 11){ $result = 56.8; }
        elseif($level == 12 &&  $shuttle == 12){ $result = 57.1; }
        else{ $result = 57.1; }
        
        return isset($result) ? $result : 'error';
    }
    
    public function vo2($gender, $current_age, $test_result){
        if($gender == "male"){
            if($current_age<30 && $test_result>51){ $result = 1;}
            elseif($current_age<30 && $test_result>44){ $result = 0.5;}
            elseif($current_age<30 && $test_result>41){ $result = 0;}
            elseif($current_age<30 && $test_result>38){ $result = -0.5;}
            elseif($current_age<30 && $test_result>34.5){ $result = -1;}
            elseif($current_age<30 && $test_result>31){ $result = -1.5;}
            elseif($current_age<30 && $test_result<31.01){ $result = -2;}
            elseif($current_age<40 && $test_result>47){ $result = 1;}
            elseif($current_age<40 && $test_result>41){ $result = 0.5;}
            elseif($current_age<40 && $test_result>38){ $result = 0;}
            elseif($current_age<40 && $test_result>35){ $result = -0.5;}
            elseif($current_age<40 && $test_result>31){ $result = -1;}
            elseif($current_age<40 && $test_result>27.5){ $result = -1.5;}
            elseif($current_age<40 && $test_result<27.51){ $result = -2;}
            elseif($current_age<50 && $test_result>42.5){ $result = 1;}
            elseif($current_age<50 && $test_result>37.5){ $result = 0.5;}
            elseif($current_age<50 && $test_result>35){ $result = 0;}
            elseif($current_age<50 && $test_result>32){ $result = -0.5;}
            elseif($current_age<50 && $test_result>28){ $result = -1;}
            elseif($current_age<50 && $test_result>24){ $result = -1.5;}
            elseif($current_age<50 && $test_result<24.01){ $result = -2;}
            elseif($current_age<60 && $test_result>38){ $result = 1;}
            elseif($current_age<60 && $test_result>34){ $result = 0.5;}
            elseif($current_age<60 && $test_result>31.5){ $result = 0;}
            elseif($current_age<60 && $test_result>29){ $result = -0.5;}
            elseif($current_age<60 && $test_result>25){ $result = -1;}
            elseif($current_age<60 && $test_result>21){ $result = -1.5;}
            elseif($current_age<60 && $test_result<21){ $result = -2;}
            elseif($current_age>59 && $test_result>34){ $result = 1;}
            elseif($current_age>59 && $test_result>30){ $result = 0.5;}
            elseif($current_age>59 && $test_result>28.5){ $result = 0;}
            elseif($current_age>59 && $test_result>27){ $result = -0.5;}
            elseif($current_age>59 && $test_result>22){ $result = -1;}
            elseif($current_age>59 && $test_result>17){ $result = -1.5;}
            elseif($current_age>59 && $test_result<17.01){ $result = -2;}
            else{ $result = 0;}
        }else{
            if($current_age<30 && $test_result>41){ $result = 1;}
            elseif($current_age<30 && $test_result>37){ $result = 0.5;}
            elseif($current_age<30 && $test_result>33){ $result = 0;}
            elseif($current_age<30 && $test_result>31){ $result = -0.5;}
            elseif($current_age<30 && $test_result>29){ $result = -1;}
            elseif($current_age<30 && $test_result>27){ $result = -1.5;}
            elseif($current_age<30 && $test_result<27.01){ $result = -2;}
            elseif($current_age<40 && $test_result>38){ $result = 1;}
            elseif($current_age<40 && $test_result>33){ $result = 0.5;}
            elseif($current_age<40 && $test_result>30.5){ $result = 0;}
            elseif($current_age<40 && $test_result>28){ $result = -0.5;}
            elseif($current_age<40 && $test_result>26){ $result = -1;}
            elseif($current_age<40 && $test_result>24){ $result = -1.5;}
            elseif($current_age<40 && $test_result<24.01){ $result = -2;}
            elseif($current_age<50 && $test_result>36){ $result = 1;}
            elseif($current_age<50 && $test_result>31){ $result = 0.5;}
            elseif($current_age<50 && $test_result>29){ $result = 0;}
            elseif($current_age<50 && $test_result>27){ $result = -0.5;}
            elseif($current_age<50 && $test_result>24.5){ $result = -1;}
            elseif($current_age<50 && $test_result>22){ $result = -1.5;}
            elseif($current_age<50 && $test_result<22.01){ $result = -2;}
            elseif($current_age<60 && $test_result>34){ $result = 1;}
            elseif($current_age<60 && $test_result>28){ $result = 0.5;}
            elseif($current_age<60 && $test_result>26){ $result = 0;}
            elseif($current_age<60 && $test_result>24){ $result = -0.5;}
            elseif($current_age<60 && $test_result>22){ $result = -1;}
            elseif($current_age<60 && $test_result>19.5){ $result = -1.5;}
            elseif($current_age<60 && $test_result<19.51){ $result = -2;}
            elseif($current_age>59 && $test_result>32){ $result = 1;}
            elseif($current_age>59 && $test_result>26){ $result = 0.5;}
            elseif($current_age>59 && $test_result>24){ $result = 0;}
            elseif($current_age>59 && $test_result>22){ $result = -0.5;}
            elseif($current_age>59 && $test_result>19.5){ $result = -1;}
            elseif($current_age>59 && $test_result>17){ $result = -1.5;}
            elseif($current_age>59 && $test_result<17.01){ $result = -2;}
            else{ $result = 0;}
        }
        
        return isset($result) ? $result : false;
    }
    
    public function vo2_old($gender, $current_age, $test_result){
        //IF($gender="Male",
        //IF(AND($current_age<30,$test_result>51),1,
        //IF(AND($current_age<30,$test_result>44),0.5,
        //IF(AND($current_age<30,$test_result>41),0,
        //IF(AND($current_age<30,$test_result>38),-0.5,
        //IF(AND($current_age<30,$test_result>34.5),-1,
        //IF(AND($current_age<30,$test_result>31),-1.5,
        //IF(AND($current_age<30,$test_result<31.01),-2,
        //IF(AND($current_age<40,$test_result>47),1,
        //IF(AND($current_age<40,$test_result>41),0.5,
        //IF(AND($current_age<40,$test_result>38),0,
        //IF(AND($current_age<40,$test_result>35),-0.5,
        //IF(AND($current_age<40,$test_result>31),-1,
        //IF(AND($current_age<40,$test_result>27.5),-1.5,
        //IF(AND($current_age<40,$test_result<27.51),-2,
        //IF(AND($current_age<50,$test_result>42.5),1,
        //IF(AND($current_age<50,$test_result>37.5),0.5,
        //IF(AND($current_age<50,$test_result>35),0,
        //IF(AND($current_age<50,$test_result>32),-0.5,
        //IF(AND($current_age<50,$test_result>28),-1,
        //IF(AND($current_age<50,$test_result>24),-1.5,
        //IF(AND($current_age<50,$test_result<24.01),-2,
        //IF(AND($current_age<60,$test_result>38),1,
        //IF(AND($current_age<60,$test_result>34),0.5,
        //IF(AND($current_age<60,$test_result>31.5),0,
        //IF(AND($current_age<60,$test_result>29),-0.5,
        //IF(AND($current_age<60,$test_result>25),-1,
        //IF(AND($current_age<60,$test_result>21),-1.5,
        //IF(AND($current_age<60,$test_result<21),-2,
        //IF(AND($current_age>59,$test_result>34),1,
        //IF(AND($current_age>59,$test_result>30),0.5,
        //IF(AND($current_age>59,$test_result>28.5),0,
        //IF(AND($current_age>59,$test_result>27),-0.5,
        //IF(AND($current_age>59,$test_result>22),-1,
        //IF(AND($current_age>59,$test_result>17),-1.5,
        //IF(AND($current_age>59,$test_result<17.01),-2,
        //0))))))))))))))))))))))))))))))))))),
        //
        //IF(AND($current_age<30,$test_result>41),1,
        //IF(AND($current_age<30,$test_result>37),0.5,
        //IF(AND($current_age<30,$test_result>33),0,
        //IF(AND($current_age<30,$test_result>31),-0.5,
        //IF(AND($current_age<30,$test_result>29),-1,
        //IF(AND($current_age<30,$test_result>27),-1.5,
        //IF(AND($current_age<30,$test_result<27.01),-2,
        //IF(AND($current_age<40,$test_result>38),-1,
        //IF(AND($current_age<40,$test_result>33),-0.5,
        //IF(AND($current_age<40,$test_result>30.5),0,
        //IF(AND($current_age<40,$test_result>28),-0.5,
        //IF(AND($current_age<40,$test_result>26),-1,
        //IF(AND($current_age<40,$test_result>24),-1.5,
        //IF(AND($current_age<40,$test_result<24.01),-2,
        //IF(AND($current_age<50,$test_result>36),1,
        //IF(AND($current_age<50,$test_result>31),0.5,
        //IF(AND($current_age<50,$test_result>29),0,
        //IF(AND($current_age<50,$test_result>27),-0.5,
        //IF(AND($current_age<50,$test_result>24.5),-1,
        //IF(AND($current_age<50,$test_result>22),-1.5,
        //IF(AND($current_age<50,$test_result<22.01),-2,
        //IF(AND($current_age<60,$test_result>34),1,
        //IF(AND($current_age<60,$test_result>28),0.5,
        //IF(AND($current_age<60,$test_result>26),0,
        //IF(AND($current_age<60,$test_result>24),-0.5,
        //IF(AND($current_age<60,$test_result>22),-1,
        //IF(AND($current_age<60,$test_result>19.5),-1.5,
        //IF(AND($current_age<60,$test_result<19.51),-2,
        //IF(AND($current_age>59,$test_result>32),1,
        //IF(AND($current_age>59,$test_result>26),0.5,
        //IF(AND($current_age>59,$test_result>24),0,
        //IF(AND($current_age>59,$test_result>22),-0.5,
        //IF(AND($current_age>59,$test_result>19.5),-1,
        //IF(AND($current_age>59,$test_result>17),-1.5,
        //IF(AND($current_age>59,$test_result<17.01),-2,0))))))))))))))))))))))))))))))))))))
        
        
        if($gender == "male"){
            if($current_age<30 && $test_result>51){ $result = 1;
            }elseif($current_age<30 && $test_result>44){ $result = 0.5;
            }elseif($current_age<30 && $test_result>41){ $result = 0;
            }elseif($current_age<30 && $test_result>38){ $result = -0.5;
            }elseif($current_age<30 && $test_result>34.5){ $result = -1;
            }elseif($current_age<30 && $test_result>31){ $result = -1.5;
            }elseif($current_age<30 && $test_result<31.01){ $result = -2;
            }elseif($current_age<40 && $test_result>47){ $result = 1;
            }elseif($current_age<40 && $test_result>41){ $result = 0.5;
            }elseif($current_age<40 && $test_result>38){ $result = 0;
            }elseif($current_age<40 && $test_result>35){ $result = -0.5;
            }elseif($current_age<40 && $test_result>31){ $result = -1;
            }elseif($current_age<40 && $test_result>27.5){ $result = -1.5;
            }elseif($current_age<40 && $test_result<27.51){ $result = -2;
            }elseif($current_age<50 && $test_result>42.5){ $result = 1;
            }elseif($current_age<50 && $test_result>37.5){ $result = 0.5;
            }elseif($current_age<50 && $test_result>35){ $result = 0;
            }elseif($current_age<50 && $test_result>32){ $result = -0.5;
            }elseif($current_age<50 && $test_result>28){ $result = -1;
            }elseif($current_age<50 && $test_result>24){ $result = -1.5;
            }elseif($current_age<50 && $test_result<24.01){ $result = -2;
            }elseif($current_age<60 && $test_result>38){ $result = 1;
            }elseif($current_age<60 && $test_result>34){ $result = 0.5;
            }elseif($current_age<60 && $test_result>31.5){ $result = 0;
            }elseif($current_age<60 && $test_result>29){ $result = -0.5;
            }elseif($current_age<60 && $test_result>25){ $result = -1;
            }elseif($current_age<60 && $test_result>21){ $result = -1.5;
            }elseif($current_age<60 && $test_result<21){ $result = -2;
            }elseif($current_age>59 && $test_result>34){ $result = 1;
            }elseif($current_age>59 && $test_result>30){ $result = 0.5;
            }elseif($current_age>59 && $test_result>28.5){ $result = 0;
            }elseif($current_age>59 && $test_result>27){ $result = -0.5;
            }elseif($current_age>59 && $test_result>22){ $result = -1;
            }elseif($current_age>59 && $test_result>17){ $result = -1.5;
            }elseif($current_age>59 && $test_result<17.01){ $result = -2;
            }else{ $result = 0;}
        }else{
            if($current_age<30 && $test_result>41){ $result = 1;
            }elseif($current_age<30 && $test_result>37){ $result = 0.5;
            }elseif($current_age<30 && $test_result>33){ $result = 0;
            }elseif($current_age<30 && $test_result>31){ $result = -0.5;
            }elseif($current_age<30 && $test_result>29){ $result = -1;
            }elseif($current_age<30 && $test_result>27){ $result = -1.5;
            }elseif($current_age<30 && $test_result<27.01){ $result = -2;
            }elseif($current_age<40 && $test_result>38){ $result = -1;
            }elseif($current_age<40 && $test_result>33){ $result = -0.5;
            }elseif($current_age<40 && $test_result>30.5){ $result = 0;
            }elseif($current_age<40 && $test_result>28){ $result = -0.5;
            }elseif($current_age<40 && $test_result>26){ $result = -1;
            }elseif($current_age<40 && $test_result>24){ $result = -1.5;
            }elseif($current_age<40 && $test_result<24.01){ $result = -2;
            }elseif($current_age<50 && $test_result>36){ $result = 1;
            }elseif($current_age<50 && $test_result>31){ $result = 0.5;
            }elseif($current_age<50 && $test_result>29){ $result = 0;
            }elseif($current_age<50 && $test_result>27){ $result = -0.5;
            }elseif($current_age<50 && $test_result>24.5){ $result = -1;
            }elseif($current_age<50 && $test_result>22){ $result = -1.5;
            }elseif($current_age<50 && $test_result<22.01){ $result = -2;
            }elseif($current_age<60 && $test_result>34){ $result = 1;
            }elseif($current_age<60 && $test_result>28){ $result = 0.5;
            }elseif($current_age<60 && $test_result>26){ $result = 0;
            }elseif($current_age<60 && $test_result>24){ $result = -0.5;
            }elseif($current_age<60 && $test_result>22){ $result = -1;
            }elseif($current_age<60 && $test_result>19.5){ $result = -1.5;
            }elseif($current_age<60 && $test_result<19.51){ $result = -2;
            }elseif($current_age>59 && $test_result>32){ $result = 1;
            }elseif($current_age>59 && $test_result>26){ $result = 0.5;
            }elseif($current_age>59 && $test_result>24){ $result = 0;
            }elseif($current_age>59 && $test_result>22){ $result = -0.5;
            }elseif($current_age>59 && $test_result>19.5){ $result = -1;
            }elseif($current_age>59 && $test_result>17){ $result = -1.5;
            }elseif($current_age>59 && $test_result<17.01){ $result = -2;
            }else{ $result = 0;}
        }
        
        return isset($result) ? $result : false;
    }
    
    
    public function vo2Result($vo2){
        $result = array();
        $color = '';
        $color_val = 0;
        
        if($vo2 > 0){
            if($vo2 >= 1 ){
                $color = 'android-green';
                $color_val = 100;
            }elseif($vo2 >= 0.5 ){
                $color = 'light-blue';
                $color_val = 80;
            }elseif($vo2 > 0 ){
                $color = 'light-blue';
                $color_val = 70;
            }
        }elseif($vo2 == 0){
            $color = 'buff';
            $color_val = 50;
        }elseif($vo2 < 0){
            if($vo2 <= -2 ){
                $color = 'red';
                $color_val = 0;
            }elseif($vo2 <= -1.5 ){
                $color = 'candy-pink';
                $color_val = 20;
            }elseif($vo2 <= -1 ){
                $color = 'apricot';
                $color_val = 20;
            }elseif($vo2 <= -0.5 ){
                $color = 'big-foot-feet';
                $color_val = 40;
            }else{
                $color = 'big-foot-feet';
                $color_val = 40;
            }
        }
        if($vo2 < 0){
            $result = array(
                'level' => 'High',
                'color' => $color,
                'color_val'=>$color_val,
                'text' => "You have room for improvement, your VO2 max result indicates that you may be at increased risk of developing health issues related to your Cardio vascular efficiency.  There are many ways to improve your cardio vascular efficiency, discuss these results with your health coach for options that will be most suitable for you."
                );
        }elseif($vo2 == 0){
            $result = array(
                'level' => 'Neutral',
                'color' => $color,
                'color_val'=>$color_val,
                'text' => "Your VO2 max result indicates that you may be at risk of developing health issues related to your Cardio vascular efficiency.  There are many ways to improve your cardio vascular efficiency, speak with your health coach about options that will be most suitable for you."
                );
        }elseif($vo2 > 0){
            $result = array(
                'level' => 'Low',
                'color' => $color,
                'color_val'=>$color_val,
                'text' => "Congratulations,  your VO2 max result indicates that you are at low risk of health issues related to your Cardiovascular efficiency."
                );
        }
        
        return $result;
    }
    
    // =========================================================================
    // Balance Test standingFunctionalReach
    // =========================================================================
    public function balance($test_type, $gender, $value){ 
        if($test_type == 'standing-functional-reach' || $test_type == 'standing functional reach'){
            //IF($value<17.5,-1,
            //IF($value<20,-0.75,
            //IF($value<22.5,-0.5,
            //IF($value<25,-0.25,
            //IF($value<27.5,0,
            //IF($value<30,0.25,
            //IF($value>29.5,0.5)))))))
            
            
            if($value<17.5){ $result = -1;
            }elseif($value<20){ $result = -0.75;
            }elseif($value<22.5){ $result = -0.5;
            }elseif($value<25){ $result = -0.25;
            }elseif($value<27.5){ $result = 0;
            }elseif($value<30){ $result = 0.25;
            }elseif($value>29.5){ $result = 0.5;}
            
        }else{ 
            //IF(AND($gender="male",$value>49),0.5,
            //IF(AND($gender="male",$value>35),0.25,
            //IF(AND($gender="male",$value>30),0,
            //IF(AND($gender="male",$value>25),-0.25,
            //IF(AND($gender="male",$value>20),-0.5,
            //IF(AND($gender="male",$value>15),-0.75,
            //IF(AND($gender="male",$value<16),-1,
            //IF(AND($gender="female",$value>39),0.5,
            //IF(AND($gender="female",$value>25),0.25,
            //IF(AND($gender="female",$value>20),0,
            //IF(AND($gender="female",$value>16),-0.25,
            //IF(AND($gender="female",$value>12),-0.05,
            //IF(AND($gender="female",$value>8),-0.75,
            //IF(AND($gender="female",$value<9),-1,0))))))))))))))
            
            //IF(AND($gender="male",$value>32),0.5,
            //IF(AND($gender="male",$value>25),0.25,
            //IF(AND($gender="male",$value>14),0,
            //IF(AND($gender="male",$value>11),-0.25,
            //IF(AND($gender="male",$value>8),-0.5,
            //IF(AND($gender="male",$value>5),-0.75,
            //IF(AND($gender="male",$value<6),-1,
            //IF(AND($gender="female",$value>26),0.5,
            //IF(AND($gender="female",$value>20),0.25,
            //IF(AND($gender="female",$value>9),0,
            //IF(AND($gender="female",$value>7),-0.25,
            //IF(AND($gender="female",$value>5),-0.05,
            //IF(AND($gender="female",$value>3),-0.75,
            //IF(AND($gender="female",$value<4),-1,0))))))))))))))
            
            if($gender == "male" && $value >32){ $result =0.5; }
            elseif($gender == "male" && $value >25){ $result =0.25; }
            elseif($gender == "male" && $value >14){ $result =0; }
            elseif($gender == "male" && $value >11){ $result =-0.25; }
            elseif($gender == "male" && $value >8){ $result =-0.5; }
            elseif($gender == "male" && $value >5){ $result =-0.75; }
            elseif($gender == "male" && $value <6){ $result =-1; }
            elseif($gender == "female" && $value >26){ $result =0.5; }
            elseif($gender == "female" && $value >20){ $result =0.25; }
            elseif($gender == "female" && $value >9){ $result =0; }
            elseif($gender == "female" && $value >7){ $result =-0.25; }
            elseif($gender == "female" && $value >5){ $result =-0.05; }
            elseif($gender == "female" && $value >3){ $result =-0.75; }
            elseif($gender == "female" && $value <4){ $result =-1; }
            else{ $result = 0; }
            
//            if($gender == "male" && $value >49){ $result = 0.5; }
//            elseif($gender == "male" && $value >35){ $result = 0.25; }
//            elseif($gender == "male" && $value >30){ $result = 0; }
//            elseif($gender == "male" && $value >25){ $result = -0.25; }
//            elseif($gender == "male" && $value >20){ $result = -0.5; }
//            elseif($gender == "male" && $value >15){ $result = -0.75; }
//            elseif($gender == "male" && $value <16){ $result = -1; }
//            elseif($gender == "female" && $value >39){ $result = 0.5; }
//            elseif($gender == "female" && $value >25){ $result = 0.25; }
//            elseif($gender == "female" && $value >20){ $result = 0; }
//            elseif($gender == "female" && $value >16){ $result = -0.25; }
//            elseif($gender == "female" && $value >12){ $result = -0.05; }
//            elseif($gender == "female" && $value >8){ $result = -0.75; }
//            elseif($gender == "female" && $value <9){ $result = -1; }
//            else{ $result = 0;}

        }
        return isset($result) ? $result : false;
    }
    
    public function balanceResult($balance){
        $result = array();
        $color = '';
        $color_val = 0;
        
        if($balance > 0){
            if($balance >= 0.5 ){
                $color = 'android-green';
                $color_val = 100;
            }elseif($balance >= 0.25 ){
                $color = 'light-blue';
                $color_val = 80;
            }elseif($balance > 0 ){
                $color = 'light-blue';
                $color_val = 70;
            }
        }elseif($balance == 0){
            $color = 'buff';
            $color_val = 50;
        }elseif($balance < 0){
            if($balance <= -1 ){
                $color = 'red';
                $color_val = 0;
            }elseif($balance <= -0.75 ){
                $color = 'candy-pink';
                $color_val = 20;
            }elseif($balance <= -0.5 ){
                $color = 'apricot';
                $color_val = 20;
            }elseif($balance <= -0.25 ){
                $color = 'big-foot-feet';
                $color_val = 40;
            }else{
                $color = 'big-foot-feet';
                $color_val = 40;
            }
        }
        
        if($balance < 0){
            $result = array(
                'level' => 'High',
                'color' => $color,
                'color_val'=>$color_val,
                'text' => "You are at increased risk of an Injury related to your ability to balance.  Improving your balance takes time and practice and it doesn't just happen overnight. However, with consistent practice, you should be able to improve your balance over time."
                );
        }elseif($balance == 0){
            $result = array(
                'level' => 'Neutral',
                'color' => $color,
                'color_val'=>$color_val,
                'text' => "You are at moderate risk of an Injury related to your ability to balance.  Improving your balance takes time and practice and it doesn't just happen overnight. However, with consistent practice, you should be able to improve your balance over time."
                );
        }elseif($balance > 0){
            $result = array(
                'level' => 'Low',
                'color' => $color,
                'color_val'=>$color_val,
                'text' => "Congratulations, you are at low risk of Injury related to your ability to balance."
                );
        }
        
        return $result;
    }
    
    // =========================================================================
    // Squat Test
    // =========================================================================
    public function squatTest($gender, $smoking_habit_value, $squat_test_value){
        //IF($gender="male",
        //IF($value>34,0.5,
        //IF($value>32,0.25,
        //IF($value>29,0,
        //IF($value>26,-0.25,
        //IF($value>23,-0.5,
        //IF($value>20,-0.75,
        //IF($smoking_habit_value<21,-1,
        //0))))))),
        //
        //IF($value>29,0.5,
        //IF($value>26,0.25,
        //IF($value>23,0,
        //IF($value>20,-0.25,
        //IF($value>17,-0.5,
        //IF($value>14,-0.75,
        //IF($smoking_habit_value<15,-1,
        //0))))))))
        
        if($gender == "male"){
            if($squat_test_value>34){ $result = 0.5;}
            elseif($squat_test_value>32){ $result = 0.25;}
            elseif($squat_test_value>29){ $result = 0;}
            elseif($squat_test_value>26){ $result = -0.25;}
            elseif($squat_test_value>23){ $result = -0.5;}
            elseif($squat_test_value>20){ $result = -0.75;}
            elseif($smoking_habit_value<21){ $result = -1;}
            else{ $result = 0;}
        }else{
            if($squat_test_value>29){ $result = 0.5;}
            elseif($squat_test_value>26){ $result = 0.25;}
            elseif($squat_test_value>23){ $result = 0;}
            elseif($squat_test_value>20){ $result = -0.25;}
            elseif($squat_test_value>17){ $result = -0.5;}
            elseif($squat_test_value>14){ $result = -0.75;}
            elseif($smoking_habit_value<15){ $result = -1;}
            else{ $result = 0;}
        }
        
        return isset($result) ? $result : false;
    }
    
    public function squatTestResult($squatTest){
        $result = array();
        $color = '';
        $color_val = 0;
        
        if($squatTest > 0){
            if($squatTest >= 0.5 ){
                $color = 'android-green';
                $color_val = 100;
            }elseif($squatTest >= 0.25 ){
                $color = 'light-blue';
                $color_val = 80;
            }elseif($squatTest > 0 ){
                $color = 'light-blue';
                $color_val = 70;
            }
        }elseif($squatTest == 0){
            $color = 'buff';
            $color_val = 50;
        }elseif($squatTest < 0){
            if($squatTest <= -1 ){
                $color = 'red';
                $color_val = 0;
            }elseif($squatTest <= -0.75 ){
                $color = 'candy-pink';
                $color_val = 20;
            }elseif($squatTest <= -0.5 ){
                $color = 'apricot';
                $color_val = 20;
            }elseif($squatTest <= -0.25 ){
                $color = 'big-foot-feet';
                $color_val = 40;
            }else{
                $color = 'big-foot-feet';
                $color_val = 40;
            }
        }
        
        if($squatTest < 0){
            $result = array(
                'level' => 'High',
                'color' => $color,
                'color_val'=>$color_val,
                'text' => "Your squat test result indicates you are at increased risk of injuries related to lower body strength and mobility.   There are numerous ways to improve your lower body strength, rectify imbalances and increase mobility, discuss these results with your health coach in order to find the most suitable options for you."
                );
        }elseif($squatTest == 0){
            $result = array(
                'level' => 'Neutral',
                'color' => $color,
                'color_val'=>$color_val,
                'text' => "Your squat test result indicates you are at risk of injuries related to lower body strength and mobility.   There are numerous ways to improve your lower body strength, rectify imbalances and increase mobility, discuss these results with your health coach in order to find the most suitable options for you."
                );
        }elseif($squatTest > 0){
            $result = array(
                'level' => 'Low',
                'color' => $color,
                'color_val'=>$color_val,
                'text' => "Congratulations,  your squat test result indicates you are at low risk of injuries related to lower body strength and mobility."
                );
        }
        
        return $result;
    }
    
    // =========================================================================
    // 7 Stage Sit Up Test
    // =========================================================================
    public function sevenStateSitUpTest($situp_test_value){
        //IF($situp_test_value=1,-1,
        //IF($situp_test_value=2,-0.75,
        //IF($situp_test_value=3,-0.5,
        //IF($situp_test_value=4,-0.25,
        //IF($situp_test_value=5,0,
        //IF($situp_test_value=6,0.25,
        //IF($situp_test_value=7,0.5,0)))))))
        
        //new
        //IF(C46<1,-1,
        //IF(C46=1,-0.75,
        //IF(C46=2,-0.5,
        //IF(C46=3,0,
        //IF(C46=4,0.25,
        //IF(C46=5,0.25,
        //IF(C46=6,0.5,
        //0.5)))))))
        
        if($situp_test_value < 1){ $result = -1;}
        elseif($situp_test_value==1){ $result = -0.75;}
        elseif($situp_test_value==2){ $result = -0.5;}
        elseif($situp_test_value==3){ $result = 0;}
        elseif($situp_test_value==4){ $result = 0.25;}
        elseif($situp_test_value==5){ $result = 0.25;}
        elseif($situp_test_value==6){ $result = 0.5;}
        else{ $result = 0.5; }
        
        return isset($result) ? $result : false;
    }
    
    public function sevenStateSitUpTestResult($sevenStateSitUpTest){
        $result = array();
        $color = '';
        $color_val = 0;
        
        if($sevenStateSitUpTest > 0){
            if($sevenStateSitUpTest >= 0.5 ){
                $color = 'android-green';
                $color_val = 100;
            }elseif($sevenStateSitUpTest >= 0.25 ){
                $color = 'light-blue';
                $color_val = 80;
            }elseif($sevenStateSitUpTest > 0 ){
                $color = 'light-blue';
                $color_val = 70;
            }
        }elseif($sevenStateSitUpTest == 0){
            $color = 'buff';
            $color_val = 50;
        }elseif($sevenStateSitUpTest < 0){
            if($sevenStateSitUpTest <= -1 ){
                $color = 'red';
                $color_val = 0;
            }elseif($sevenStateSitUpTest <= -0.75 ){
                $color = 'candy-pink';
                $color_val = 20;
            }elseif($sevenStateSitUpTest <= -0.5 ){
                $color = 'apricot';
                $color_val = 20;
            }elseif($sevenStateSitUpTest <= -0.25 ){
                $color = 'big-foot-feet';
                $color_val = 40;
            }else{
                $color = 'big-foot-feet';
                $color_val = 40;
            }
        }
        
        if($sevenStateSitUpTest <= -0.75){
            $result = array(
                'level' => 'High',
                'color' => $color,
                'color_val'=>$color_val,
                'text' => "Your 7 stage sit-up test result indicates you are at risk of injuries related to core strength and stability. There are numerous ways to improve your abdominal strength and rectify imbalances, discuss these results with your trainer in order to find the most suitable options for you."
                );
        }elseif($sevenStateSitUpTest == -0.5){
            $result = array(
                'level' => 'Moderate',
                'color' => $color,
                'color_val'=>$color_val,
                'text' => "Your 7 stage sit-up test result indicates you are at moderate risk of injuries related to core strength and stability. There are numerous ways to improve your abdominal strength and rectify imbalances, discuss these results with your trainer in order to find the most suitable options for you."
                );
        }elseif($sevenStateSitUpTest == 0){
            $result = array(
                'level' => 'Neutral',
                'color' => $color,
                'color_val'=>$color_val,
                'text' => "Your 7 stage sit-up test result indicates you are not at increased risk of injuries related to core strength and stability. There are numerous ways to improve your abdominal strength and rectify imbalances, discuss these results with your trainer in order to find the most suitable options for you."
                );
        }elseif($sevenStateSitUpTest > 0){
            $result = array(
                'level' => 'Low',
                'color' => $color,
                'color_val'=>$color_val,
                'text' => "Congratualtions! Your 7 stage sit-up test result indicates you are at low risk of injuries related to core strength and stability."
                );
        }
        
        return $result;
    }
    
    // =========================================================================
    // Waist
    // =========================================================================
    public function waist($gender, $waist_value, $slitetest = FALSE){
        //IF($gender="male",
        //IF($waist_value<90,1,
        //IF($waist_value<92,0.5,
        //IF($waist_value<94,0,
        //IF($waist_value<97,-0.5,
        //IF($waist_value<100,-1,
        //IF($waist_value<102,-1.5,
        //IF($waist_value>101.99,-2,0))))))),
        //
        //IF($waist_value<76,1,
        //IF($waist_value<78,0.5,
        //IF($waist_value<80,0,
        //IF($waist_value<83,-0.5,
        //IF($waist_value<86,-1,
        //IF($waist_value<88,-1.5,
        //IF($waist_value>87.99,-2,0))))))))
        
        if( $waist_value < 51) { 
            $result = 0; 
        }else{
            if($gender == "male"){
                if($waist_value<90){ $result = 1;}
                elseif($waist_value<92){ $result = 0.5;}
                elseif($waist_value<94){ $result = 0;}
                elseif($waist_value<97){ $result = -0.5;}
                elseif($waist_value<100){ $result = -1;}
                elseif($waist_value<102){ $result = -1.5;}
                elseif($waist_value>101.99){ $result = -2;}
                else{ $result = 0;}
            }else{
                if($waist_value<76){ $result =1;}
                elseif($waist_value<78){ $result =0.5;}
                elseif($waist_value<80){ $result =0;}
                elseif($waist_value<83){ $result =-0.5;}
                elseif($waist_value<86){ $result =-1;}
                elseif($waist_value<88){ $result =-1.5;}
                elseif($waist_value>87.99){ $result = -2; }
                else{ $result = 0;}
            }
        }
        
        
        
        $result = (!$slitetest) ? $result : ($result * 3);
        
        return isset($result) ? $result : false;
    }
    
    
    public function waistResult($waist, $waist_value = NULL){
        $result = array();
        $color = '';
        $color_val = 0;
        
        if($waist > 0){
            if($waist >= 1 ){
                $color = 'android-green';
                $color_val = 100;
            }elseif($waist >= 0.5 ){
                $color = 'light-blue';
                $color_val = 80;
            }elseif($waist > 0 ){
                $color = 'light-blue';
                $color_val = 70;
            }
        }elseif($waist == 0){
            $color = 'buff';
            $color_val = 50;
        }elseif($waist < 0){
            if($waist <= -2 ){
                $color = 'red';
                $color_val = 0;
            }elseif($waist <= -1.5 ){
                $color = 'candy-pink';
                $color_val = 20;
            }elseif($waist <= -1 ){
                $color = 'apricot';
                $color_val = 20;
            }elseif($waist <= -0.5 ){
                $color = 'big-foot-feet';
                $color_val = 40;
            }else{
                $color = 'big-foot-feet';
                $color_val = 40;
            }
        }
        
        if($waist < 0){
            $result = array(
                'level' => 'High',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "You are at increased risk of health problems related to a high waist circumference. Regardless of your height or build, for most adults a waist measurement of greater than 94cm for men and 80cm for women is an indicator of the level of internal fat deposits which coat the heart, kidneys, liver and pancreas, and increase the risk of chronic disease."
                );
        }elseif($waist == 0){
            if($waist_value != NULL){
                if($waist_value < 51){ 
                    $result = array(
                        'level' => 'Neutral',
                        'color' => $color,
                        'color_val' => $color_val,
                        'text' => "You are not at increased risk of health problems related to a high waist circumference, however based on your results you may be at risk of health problems related to a low waist circumference. Regardless of your height or build, for most adults a waist measurement of greater than 94cm for men and 80cm for women is an indicator of the level of internal fat deposits which coat the heart, kidneys, liver and pancreas, and increase the risk of chronic disease."
                        );
                }else{
                    $result = array(
                        'level' => 'Neutral',
                        'color' => $color,
                        'color_val' => $color_val,
                        'text' => "You are not at increased risk of health problems related to a high waist circumference, regardless of your height or build, for most adults a waist measurement of greater than 94cm for men and 80cm for women is an indicator of the level of internal fat deposits which coat the heart, kidneys, liver and pancreas, and increase the risk of chronic disease.",
                        );
                }
            }else{
                $result = array(
                    'level' => 'Neutral',
                    'color' => $color,
                    'color_val' => $color_val,
                    'text' => "You are not at increased risk of health problems related to a high waist circumference, regardless of your height or build, for most adults a waist measurement of greater than 94cm for men and 80cm for women is an indicator of the level of internal fat deposits which coat the heart, kidneys, liver and pancreas, and increase the risk of chronic disease.",
                    );
            }
        }elseif($waist > 0){
            $result = array(
                'level' => 'Low',
                'color' => $color,
                'color_val' => $color_val,
                'text' => "Congratulations, you are at low risk of health problems related to a high waist circumference."
                );
        }
        
        return $result;
    }
    
    // =========================================================================
    // CRF
    // =========================================================================
    public function crf($value){
        //IF($value<0,-1)
        
        $result = ($value < 0) ? -1 : false;
        return $result;
    }
    
    public function totalCRF($physical_activity_crf,
            $smoking_crf,
            $alcohol_crf,
            $nutrition_crf,
            $mental_health_crf,
            $risk_profile_crf,
            $body_fat_crf,
            $blood_pressure_crf,
            $blood_oxygen_crf,
            $vo2_crf,
            $balance_crf,
            $squat_test_crf,
            $seven_stage_situp_crf,
            $waist_crf
    ){
        
        $total = $physical_activity_crf + 
                $smoking_crf + 
                $alcohol_crf + 
                $nutrition_crf + 
                $mental_health_crf + 
                $risk_profile_crf + 
//                $body_fat_crf + 
                $blood_pressure_crf + 
                $blood_oxygen_crf + 
                $vo2_crf + 
                $balance_crf + 
                $squat_test_crf + 
                $seven_stage_situp_crf + 
                $waist_crf;
        
        //($total=-1,0,IF($total=-2,-2,IF($total=-3,-3,IF($total=-4,-4.5,IF($total<-4,-6)))))
        if($total == -1){ $result = 0;}
        elseif($total == -2){ $result =-2;}
        elseif($total == -3){ $result =-3;}
        elseif($total == -4){ $result =-4.5;}
        elseif($total < -4){ $result =-6;}
        
        return isset($result) ? $result : false;
    }
    
    // =========================================================================
    // Healthy Life Years
    // =========================================================================
    public function healthyLifeYears($gender, 
            $physical_activity_result, 
            $alcohol_consumption_result,
            $nutrition_result,
            $smoking_result,
            $mental_health_result,
            $risk_profile_result,
            $body_fat_result,
            $blood_pressure_result,
            $blood_oxygen_result,
            $vo2_result,
            $balance_result,
            $squat_test_result,
            $seven_stage_situp_result,
            $waist_result,
            $total_crf
            ){
//        IF($gender="Male",$mental_health_result+$nutrition_result+$alcohol_consumption_result+$smoking_result+$physical_activity_result+$blood_oxygen_result+$risk_profile_result+$waist_result+$squat_test_result+$balance_result+$blood_pressure_result+$vo2_result+$seven_stage_situp_result+$body_fat_result,
//        (($mental_health_result+$nutrition_result+$alcohol_consumption_result+$smoking_result+$physical_activity_result+$risk_profile_result+$waist_result+$squat_test_result+$balance_result+$blood_pressure_result+$vo2_result+$blood_oxygen_result+$seven_stage_situp_result+$body_fat_result)*IF(($mental_health_result+$nutrition_result+$alcohol_consumption_result+$smoking_result+$physical_activity_result+$risk_profile_result+$waist_result+$squat_test_result+$balance_result+$blood_pressure_result+$vo2_result+$blood_oxygen_result+$seven_stage_situp_result+$body_fat_result)>0,1.1,1)))+$total_crf
//        
//        
//        if($gender == "male"){ 
//            $total = $mental_health_result+$nutrition_result+$alcohol_consumption_result+$smoking_result+$physical_activity_result+$blood_oxygen_result+$risk_profile_result+$waist_result+$squat_test_result+$balance_result+$blood_pressure_result+$vo2_result+$seven_stage_situp_result+$body_fat_result;
//        }else{
//            $total = $mental_health_result+$nutrition_result+$alcohol_consumption_result+$smoking_result+$physical_activity_result+$risk_profile_result+$waist_result+$squat_test_result+$balance_result+$blood_pressure_result+$vo2_result+$blood_oxygen_result+$seven_stage_situp_result+$body_fat_result;
//            
//            IF($total>0){
//                $value = 1.1;
//            }else{
//                $value = 1; 
//            (($total)* IF(($total)>0,1.1,1)))+$total_crf;
//        }
//        
//        +$total_crf''
        
        
        $total  = $mental_health_result +
                    $nutrition_result +
                    $alcohol_consumption_result +
                    $smoking_result +
                    $physical_activity_result +
                    $blood_oxygen_result +
                    $risk_profile_result +
                    $waist_result +
                    $squat_test_result +
                    $balance_result +
                    $blood_pressure_result +
                    $vo2_result +
                    $seven_stage_situp_result;
//                    $body_fat_result;
        
        if($gender == "male"){
            $result = $total;
        }else{
            
            if(($total) > 0){ 
                $value = 1.1;   
            }else{ 
                $value = 1;
            } 
            $result = ($total * $value);
        }
        $result = $result + $total_crf;
        
        return isset($result) ? $result : false;
        
    }
    
    // =========================================================================
    // Max HLE
    // =========================================================================
    public function maxHLE($gender, 
            $healthy_life_expectancy_male, 
            $healthy_life_expectancy_female,
            $male_max_score, 
            $female_max_score){
        //C73+C76
        if($gender == 'male'){
            $result = $healthy_life_expectancy_male + $male_max_score;
        }else{
            $result = $healthy_life_expectancy_female + $female_max_score;
        }
        
        return isset($result) ? $result : false;
    }
    
    // =========================================================================
    // Current Healthy Life Score
    // =========================================================================
    public function currentHealthyLifeScore($gender, $estimated_healthy_life_expectancy, $max_hle){
        //IF(C11="Male",C62/D76,C62/D77)
        
        if($gender == 'male'){
            $result = $estimated_healthy_life_expectancy / $max_hle;
        }else{
            $result = $estimated_healthy_life_expectancy / $max_hle;
        }
        
        return isset($result) ? $result : false;
    }
    
    // =========================================================================
    // Outcomes
    // =========================================================================
    public function outcomes($currentHealthyLifeScore){
        //IF(AND($currentHealthyLifeScore<0.9,$currentHealthyLifeScore>0.825),C89,
        //IF(AND($currentHealthyLifeScore<0.8251,$currentHealthyLifeScore>0.7749),C90,
        //IF(AND($currentHealthyLifeScore<0.775,$currentHealthyLifeScore>0.6999),C91,
        //IF(AND($currentHealthyLifeScore<0.7,$currentHealthyLifeScore>0.6249),C92,
        //IF(AND($currentHealthyLifeScore<0.625,$currentHealthyLifeScore>0.5499),C93,
        //IF(AND($currentHealthyLifeScore<0.55,$currentHealthyLifeScore>0),C94,C88))))))
        
        if($currentHealthyLifeScore<0.9 && $currentHealthyLifeScore>0.825){ $result = array("result"=> "Above Average", "color"=> "#86c03f" );}// green
        elseif($currentHealthyLifeScore<0.8251 && $currentHealthyLifeScore>0.7749){ $result = array("result"=> "Average", "color"=> "#c6c6c6" );}// gray
        elseif($currentHealthyLifeScore<0.775 && $currentHealthyLifeScore>0.6999){ $result = array("result"=>"Below Average", "color"=> "#f1fe1f");} // yellow
        elseif($currentHealthyLifeScore<0.7 && $currentHealthyLifeScore>0.6249){ $result = array("result"=>"Poor", "color"=> "#fa4a05" );}// orange
        elseif($currentHealthyLifeScore<0.625 && $currentHealthyLifeScore>0.5499){ $result  = array("result"=> "Very Poor", "color"=> "#ff3333" );}// red
        elseif($currentHealthyLifeScore<0.55 && $currentHealthyLifeScore>0){ $result = array("result"=> "Extreme", "color"=> "#ff3333" );}// red
        else{ $result = array("result"=>"Excellent", "color"=> "#86c03f");} // green
        
        return $result;
    }
    
    // =========================================================================
    // Estimated Healthy Life
    // =========================================================================
    
    public function estimatedHealthyLifeExpectancy($gender, $healthy_life_expectancy_male, $healthy_life_expectancy_female, $healthy_life_years){
        //IF($gender="Male",$healthy_life_expectancy+$healthy_life_years,$healthy_life_expectancy_female+$healthy_life_years)
        //=IF($gender="Male",$healthy_life_expectancy_male+$healthy_life_years,
        //$healthy_life_expectancy_female+$healthy_life_years)
        if($gender == "male"){ 
            $result = $healthy_life_expectancy_male + $healthy_life_years;
        }else{
            $result = $healthy_life_expectancy_female + $healthy_life_years;
        }
        
        return isset($result) ? $result : false;
    }
    
    // =========================================================================
    // Estimated Years of Healthy Life left
    // =========================================================================
    public function estimatedYearsOfHealthyLifeLeft($estimated_healthy_life_expectancy, $current_age){
        //IF(($estimated_healthy_life_expectancy-C54)<0,0,($estimated_healthy_life_expectancy-C54))
        if(($estimated_healthy_life_expectancy-$current_age) < 0){
            $result = 0;
        }else{
            $result = $estimated_healthy_life_expectancy-$current_age;
        }
        
        return isset($result) ? $result : false;
    }
    
    // =========================================================================
    // Estimated number of years you will live with a disability, injury or disease
    // =========================================================================
    public function estimatedNumberOfYearsYouWillLive($gender, $estimated_healthy_life_expectancy, $total_life_expectancy_male, $total_life_expectancy_female){
        //IF(C11="Male",$total_life_expectancy_male-$estimated_healthy_life_expectancy,$total_life_expectancy_female-$estimated_healthy_life_expectancy)
        if($gender == "male"){
            $result = $total_life_expectancy_male - $estimated_healthy_life_expectancy;
        }else{
            $result = $total_life_expectancy_female-$estimated_healthy_life_expectancy;
        }
        
        return isset($result) ? $result : false;
    }
    
    // =========================================================================
    // You could ADD up to 
    // =========================================================================
    public function couldAddUpTo($gender, 
            $current_age, 
            $healthy_life_expectancy_male, 
            $healthy_life_expectancy_female,
            $max_hle,
            $estimated_healthy_life_expectancy,
            $male_max_score,
            $female_max_score
            ){
        //=IF(AND($gender="Male",$current_age>$healthy_life_expectancy_male,$max_hle-$estimated_healthy_life_expectancy>$male_max_score),$max_hle-$current_age,
        //IF(AND($gender="Female",$current_age>$healthy_life_expectancy_female,$max_hle-$estimated_healthy_life_expectancy>$female_max_score),$max_hle-$current_age,
        //IF($gender="Male",$max_hle-$estimated_healthy_life_expectancy,$max_hle-$estimated_healthy_life_expectancy)))
        
        //=IF(AND(C11="Male",C55>C74,D77-C63>C77),D77-C55,
        //IF(AND(C11="Female",C55>C75,D78-C63>C78),D78-C55,
        //IF(C11="Male",D77-C63,D78-C63)))
        
        if($gender == "male" && 
                ($current_age > $healthy_life_expectancy_male) && 
                ($max_hle - $estimated_healthy_life_expectancy > $male_max_score)){
            $result = $max_hle - $current_age;
            
        }elseif($gender == "female" &&
                ($current_age > $healthy_life_expectancy_female) && 
                ($max_hle - $estimated_healthy_life_expectancy > $female_max_score)){
            
            $result = $max_hle - $current_age;
//            
//        }elseif($gender == "male"){
//            $result = $max_hle - $estimated_healthy_life_expectancy;
        }else{
            $result = $max_hle - $estimated_healthy_life_expectancy; 
        }
        
        return isset($result) ? $result : false;
    }
    
    
    // =========================================================================
    // Your estimated current Healthy Life Age is
    // =========================================================================
    public function estimatedCurrentHealthyLifeAge($current_age, $healthy_life_years, $max_hle){
        //IF($current_age-$healthy_life_years<18,18,IF($current_age-$healthy_life_years>$max_hle,$max_hle,$current_age-$healthy_life_years))
        if(($current_age - $healthy_life_years) < 18){
            $result = 18;
        }elseif(($current_age - $healthy_life_years) > $max_hle){
            $result = $max_hle;
        }else{
            $result = $current_age - $healthy_life_years;
        }
        
        return isset($result) ? $result : false;
        
    }
    
    
    public function daly($gender, $healthy_life_expectancy_daly_male, $healthy_life_expectancy_daly_female){
        //=IF($gender=="male",$healthy_life_expectancy_daly_male,$healthy_life_expectancy_daly_female)
        if($gender=="male"){ $result = $healthy_life_expectancy_daly_male;}
        else{ $result = $healthy_life_expectancy_daly_female; }
        
        return $result;
    }
    
    // =========================================================================
    // Years Added
    // =========================================================================
    public function yearsAdded($estimated_healthy_life_expectancy_1st_test, $estimated_healthy_life_expectancy_current_test){
        $result = $estimated_healthy_life_expectancy_current_test - $estimated_healthy_life_expectancy_1st_test;
        
        return $result;
    }
    
//    public function yearsAdded($healthy_life_age_1st_test, $healthy_life_age_current_test){
//        $result = $healthy_life_age_current_test - $healthy_life_age_1st_test;
//        
//        return $result;
//    }
    
    // =========================================================================
    // CM Lost
    // =========================================================================
    public function cmLost($cms_1st_test  = array(), $cms_current_test = array()){
        $first_test = array_sum($cms_1st_test);
        $current_test = array_sum($cms_current_test);
        
        return $first_test - $current_test;
    }
    
    // =========================================================================
    // KG Lost
    // =========================================================================
    public function kgLost($kg_1st_test , $kg_current_test){
        
        return $kg_1st_test - $kg_current_test;
    }
    
    // =========================================================================
    // Waist to Hip Ratio
    // =========================================================================
    public function waistToHipRatio($waist , $hip){
        
        return ($waist > 0 && $hip > 0) ? $waist/$hip : 0;
    }
    
    public function waistToHipRatioScore($gender, $waist_hip_ratio){
        //IF(C11="male",
        //IF(J29<0.85,1,
        //IF(J29<0.9,0.5,
        //IF(J29<0.95,0,
        //IF(J29<0.9667,-0.5,
        //IF(J29<0.9834,-1,
        //IF(J29<1,-1.5,
        //IF(J29>0.999,-2,
        //0))))))),
        //IF(J29<0.75,1,
        //IF(J29<0.8,0.5,
        //IF(J29<0.85,0,
        //IF(J29<0.8667,-0.5,
        //IF(J29<0.8834,-1,
        //IF(J29<0.9,-1.5,
        //IF(J29>0.899,-2,
        //0))))))))
        
        if($gender == "male"){
            if($waist_hip_ratio < 0.85){ $result = 1;}
            elseif($waist_hip_ratio < 0.9){ $result = 0.5;}
            elseif($waist_hip_ratio < 0.95){ $result = 0;}
            elseif($waist_hip_ratio < 0.9667){ $result =-0.5;}
            elseif($waist_hip_ratio < 0.9834){ $result = -1;}
            elseif($waist_hip_ratio < 1){ $result = -1.5;}
            elseif($waist_hip_ratio > 0.999){ $result =-2;}
            else{ $result = 0;}
        }else{
            if($waist_hip_ratio < 0.75){ $result = 1;}
            elseif($waist_hip_ratio < 0.8){ $result = 0.5;}
            elseif($waist_hip_ratio < 0.85){ $result = 0;}
            elseif($waist_hip_ratio < 0.8667){ $result = -0.5;}
            elseif($waist_hip_ratio < 0.8834){ $result = -1;}
            elseif($waist_hip_ratio < 0.9){ $result = -1.5;}
            elseif($waist_hip_ratio > 0.899){ $result = -2;}
            else{ $result = 0;}
        }
        
        return $result;
    }
    
    public function waistToHipRatioResult($waist_hip_ratio_score){
        $result = array();
        $color = '';
        $color_val = 0;
        
        if($waist_hip_ratio_score > 0){
            if($waist_hip_ratio_score >= 1 ){
                $color = 'android-green';
                $color_val=100;
            }elseif($waist_hip_ratio_score >= 0.5 ){
                $color = 'light-blue';
                $color_val=80;
            }elseif($waist_hip_ratio_score > 0 ){
                $color = 'light-blue';
                $color_val=70;
            }
        }elseif($waist_hip_ratio_score == 0){
            $color = 'buff';
            $color_val=50;
        }elseif($waist_hip_ratio_score < 0){
            if($waist_hip_ratio_score <= -2 ){
                $color = 'red';
                $color_val=0;
            }elseif($waist_hip_ratio_score <= -1.5 ){
                $color = 'candy-pink';
                $color_val=20;
            }elseif($waist_hip_ratio_score <= -1 ){
                $color = 'apricot';
                $color_val=20;
            }elseif($waist_hip_ratio_score <= -0.5 ){
                $color = 'big-foot-feet';
                $color_val=40;
            }else{
                $color = 'big-foot-feet';
                $color_val=40;
            }
        }
        
        if($waist_hip_ratio_score < 0){
            $result = array(
                'level' => 'High',
                'color' => $color,
                'color_val'=>$color_val,
                'text' => "Your results indicate you are at increased risk of health issues related to a high waist to hip ratio. "
                );
        }elseif($waist_hip_ratio_score == 0){
            $result = array(
                'level' => 'Neutral',
                'color' => $color,
                'color_val'=>$color_val,
                'text' => "Your results indicate you are not at increased risk of health concerns related to a high waist to hip ratio.",
                );
            
        }elseif($waist_hip_ratio_score > 0){
            $result = array(
                'level' => 'Low',
                'color' => $color,
                'color_val'=>$color_val,
                'text' => " Congratulations! Your results indicate you are at low risk of health concerns related to a high waist to hip ratio."
                );
        }
        
        return $result;
    }
}

?>
