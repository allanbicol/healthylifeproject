<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GlobalModel
 *
 * @author leokarl
 */
namespace Acme\HeadOfficeBundle\Model;


class GlobalModel {
    //put your code here
    public function setActivity($message, $supplier_name, $quote_packet_name, $price, $date_time){
        // replace [supplier_name]
        $message = str_replace('[supplier_name]', $supplier_name, $message);
        // replace [quote_packet_name]
        $message = str_replace('[quote_packet_name]', $quote_packet_name, $message);
        // replace [item_price]
        $message = str_replace('[item_price]', '$'.$price, $message);
        // replace [item_price]
        $date_time = new \DateTime($date_time);
        $message = str_replace('[activity_date_time]', date_format($date_time, 'g:ia \-\- F m, Y'), $message);
        return $message;
    }
    
    /**
     * @todo send email via php mail
     * @author leokarl
     * @param type $recipient
     * @param type $subject
     * @param type $body
     */
    public function sendEmail($recipients, $from, $subject, $body){
        $yourWebsite = "APHA"; // the name of your website
        
        if (strstr($_SERVER['SERVER_SOFTWARE'], "Win")) {
            $headers = "From: $from";
            $headers.= "Reply-To: <$recipients>";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        } else {
            $headers = "From: $yourWebsite $from\n";
            $headers.= "Reply-To: <$recipients>";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        }


        mail($recipients,$subject,$body,$headers);
    }
    
    /**
     * @todo check request number
     * @author leokarl
     * @param type $request_no
     * @return boolean
     */
    public function isRequestNoValid($request_no){
        if (strpos($request_no,' ') !== false) {
            return FALSE;
        }else{
            return TRUE;
        }
    }
    
    /**
     * @todo create directory
     * @author leokarl
     * @param type $container_path
     * @param type $propose_dir_name
     */
    public function createDirectory($container_path, $propose_dir_name){
        $dir = $container_path.$propose_dir_name;
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
    }
    
    /**
     * @todo generateJsonFile
     * @author leokarl
     * @param type $file
     * @param type $data
     */
    public function generateJsonFile($file, $data){
        $fp = fopen($file, 'w');
        fwrite($fp, $data);
        fclose($fp);
    }
    /**
    * @todo password generator
    * @author leokarl
    * @param string $password
    */
    
   public function passGenerator($password){
           return md5(md5($password));
   }
   
   /**
    * @todo disable special characters
    * @param unknown_type $mystring
    */
   public function isSpecialCharPresent($mystring){
           if(strpos($mystring, '~') === false && strpos($mystring, '!') === false && strpos($mystring, '#') === false
                           && strpos($mystring, '$') === false && strpos($mystring, '%') === false && strpos($mystring, '^') === false
                           && strpos($mystring, '*') === false && strpos($mystring, '_') === false
                           && strpos($mystring, '=') === false && strpos($mystring, '`') === false
                           && strpos($mystring, '"') === false
                           && strpos($mystring, ',') === false && strpos($mystring, '<') === false && strpos($mystring, '>') === false
                           && strpos($mystring, '/') === false && strpos($mystring, '?') === false && strpos($mystring, '|') === false){

                   return FALSE; // special character not present
           }else{
                   return TRUE; // special character present
           }
   }
   
   public function isPhoneNumber($number){
       if( !preg_match('/^(NA|[ 0-9+-]+)$/', $number) ) { 
            return FALSE;
        }else{
            return TRUE;
        }
   }
   /**
    * @todo email validator
    * @author leokarl
    * @param string $email
    * @return boolean
    */
   public function isEmailValid($email){
//           if(!preg_match('/^([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*\@([a-z0-9])(([a-z0-9-])*([a-z0-9]))+' . '(\.([a-z0-9])([-a-z0-9_-])?([a-z0-9])+)+$/i', strtolower($email))){
//                   return FALSE;
//           }else{
//                   return TRUE;
//           }
       if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            return true;
       }else{
            return false;
       }
//       return true;
   }
   public function checkDate($mydate) {
        if(substr_count($mydate, '-') == 2){
                list($yy,$mm,$dd)=explode("-",$mydate);
                if (is_numeric($yy) && is_numeric($mm) && is_numeric($dd)){
                        return checkdate($mm,$dd,$yy);
                }else{
                        return false;
                }
        }else{
                return false;
        }
    }
   public function checkDateTime($mydatetime) {
        if(substr_count($mydatetime, '-') == 2){
                list($date, $time) = explode(" ",$mydatetime);
                list($yy,$mm,$dd)=explode("-",$date);
                list($hh,$mn,$sc)=explode(":",$time);

                if (is_numeric($yy) && is_numeric($mm) && is_numeric($dd)){
                    return checkdate($mm,$dd,$yy);
                }else{
                    return false;
                }

                if (!is_numeric($hh) && !is_numeric($mn) && !is_numeric($sc)){
                    return false;
                }
        }else{
            return false;
        }
    }
    
    public function isPetTypeValid($type){
        if($type == 'dog' || $type == 'cat' || $type== 'other'){
            return true;
        }else{
            return false;
        }
    }
    
    public function isReportTypeValid($type){
        if(is_numeric($type)){
            if($type == 1 || $type== 0){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    public function isPetGenderValid($gender){
        if($gender == 'male' || $gender == 'female' || $gender == 'unknown'){
            return true;
        }else{
            return false;
        }
    }
    
    public function createImageThumb($filename,$modheight,$orig_dir,$copy_dir){
        $file= $orig_dir."/".$filename;
        $path_parts = pathinfo($file);
        
        
        // This sets it to a .jpg, but you can change this to png or gif 
        if(strtolower($path_parts['extension'])== 'jpeg' || strtolower($path_parts['extension'])== 'jpg'){
            header('Content-type: image/jpeg'); 
        }else if(strtolower($path_parts['extension'])== 'png'){
            header('Content-type: image/png'); 
        }else if(strtolower($path_parts['extension'])== 'gif'){
            header('Content-type: image/gif'); 
        }
        // Setting the resize parameters
        list($width, $height) = getimagesize($file); 
        //$modwidth = 146; 
        $modwidth = $modheight / ($height / $width);
        
        // Creating the Canvas 
        $tn= imagecreatetruecolor($modwidth, $modheight); 
        if(strtolower($path_parts['extension'])== 'jpeg' || strtolower($path_parts['extension'])== 'jpg'){
            $source = imagecreatefromjpeg($file); 
        }else if(strtolower($path_parts['extension'])== 'png'){
            $source = imagecreatefrompng($file); 
        }else if(strtolower($path_parts['extension'])== 'gif'){
            $source = imagecreatefromgif($file);
        }

        // Resizing our image to fit the canvas 
        //imagecopyresized($tn, $source, 0, 0, 0, 0, $modwidth, $modheight, $width, $height); 
        imagecopyresampled($tn, $source, 0, 0, 0, 0, $modwidth, $modheight, $width, $height); 


        // Outputs a jpg image, you could change this to gif or png if needed 
        if($path_parts['extension']== 'jpeg'){
            imagejpeg($tn,$copy_dir."/".$path_parts['filename']."_thumb.jpeg");
        }else if($path_parts['extension']== 'JPEG'){
            imagejpeg($tn,$copy_dir."/".$path_parts['filename']."_thumb.JPEG");
        }else if($path_parts['extension']== 'jpg'){
            imagejpeg($tn,$copy_dir."/".$path_parts['filename']."_thumb.jpg");
        }else if($path_parts['extension']== 'JPG'){
            imagejpeg($tn,$copy_dir."/".$path_parts['filename']."_thumb.JPG");
        }else if($path_parts['extension']== 'png'){
            imagepng($tn,$copy_dir."/".$path_parts['filename']."_thumb.png");
        }else if($path_parts['extension']== 'PNG'){
            imagepng($tn,$copy_dir."/".$path_parts['filename']."_thumb.PNG");
        }else if($path_parts['extension']== 'gif'){
            imagegif($tn,$copy_dir."/".$path_parts['filename']."_thumb.gif");
        }else if($path_parts['extension']== 'GIF'){
            imagegif($tn,$copy_dir."/".$path_parts['filename']."_thumb.GIF");
        }
    }
    
    public function deleteDir($dirPath) {
        if (! is_dir($dirPath)) {
            return false;
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }
    
    public function siteURL()
    {
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $domainName = $_SERVER['HTTP_HOST'];
        return $protocol.$domainName;
    }

    public function getCurrentFullURL(){
        $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : ""; 
        $protocol = $this->strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s; 
        $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]); 
        return $protocol."://".$_SERVER['SERVER_NAME'].$port.$_SERVER['REQUEST_URI']; 
    }
    
    public function strleft($s1, $s2) { return substr($s1, 0, strpos($s1, $s2)); }
    /**
     * uses curl to execute external php file
     * @param type $url
     */
    public function executeUrl($url){
        // create a new cURL resource
        $ch = curl_init();

        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // grab URL and pass it to the browser
        curl_exec($ch);

        // close cURL resource, and free up system resources
        curl_close($ch);
    }
    
    
    /**
     * get integer from string
     * @param type $string
     * @return type
     */
    public function getNumberFromString($string){
        preg_match_all('!\d+!', $string, $matches);
        return $matches[0][0];
    }
    
    function rot13($string, $mode) {
        $s = fopen("php://memory", "rwb");
        stream_filter_append($s, "string.rot13", STREAM_FILTER_WRITE);
        fwrite($s, $string);
        rewind($s);
        return stream_get_contents($s);
    }
    
    
    public function changeFormatToOriginal($stringdate){
        $arrdate = explode(" ",$stringdate);
        if(isset($arrdate[1])){
            if(trim($arrdate[0]) != '' && substr_count($arrdate[0], "/") > 1){
                list($d, $m , $y) = explode("/", $arrdate[0]);
                $time = (isset($arrdate[1])) ? $arrdate[1] : '';
                return $y.'-'.$m.'-'.$d . ' ' . $time;
            }
        }else{
            if(trim($stringdate) != '' && substr_count($stringdate, "/") > 1){
                list($d, $m , $y) = explode("/", $stringdate);
                return $y.'-'.$m.'-'.$d;
            }
        }
    }
    
    public function acceptTagsExceptScript() {
        return '<p><a><hr><br><table><thead><tbody><tr><td><th><tfoot><span><div><ul><ol><li><img>' .
            '<canvas><video><object><embed><audio><frame><iframe><label><option><select><option>' .
            '<input><textarea><button><form><param><pre><code><small><em><b><u><i><strong><article>' .
            '<aside><bdi><details><summary><figure><figcaption><footer><header><hgroup><mark><meter>' .
            '<nav><progress><ruby><rt><rp><section><time><wbr><track><source><datalist><output><keygen>' .
            '<h1><h2><h3><h4><h5><h6><h7><h8><h9>';
    }
    
    public function getFilesInDirectory($dir){
        
        $directories = array();
        $files_list  = array();
        $files = scandir($dir);
        foreach($files as $file){
           if(($file != '.') && ($file != '..') && (strpos($file, '~$') === false)){
               $pathinfo = pathinfo($file);
                $file_date = array("datecreated"=>date ("m-d-Y", filemtime($dir . "/".$pathinfo['basename'])));
                $file_size = array("filesize"=>$this->human_filesize(filesize($dir . "/".$pathinfo['basename'])));
                $pathinfo = array_merge($pathinfo, $file_date, $file_size);
                
                
              if(is_dir($dir.'/'.$file)){
                 $directories[]  = $pathinfo;
              }else{
                  
                 $files_list[]    = $pathinfo;

              }
           }
        }
        
        return array_merge($directories, $files_list);
        
    }
    
    function human_filesize($bytes, $decimals = 2) {
        $sz = 'BKMGTP';
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
    }
    
    public function passwordHardening($post){
        $isLetter=0;
        $isNumber=0;
        $isSpecial=0;
        $isLen=0;
        if (preg_match('/[A-Z]/', $post)) {
            $isLetter=1;
        }
        if(preg_match('/[0-9]/', $post)){
            $isNumber=1;
        }
        if(preg_match('/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/',$post)) {
            $isSpecial=1;
        }
        if(strlen($post)> 6){
            $isLen=1;
        }
        
        return $isLetter.'#'.$isNumber.'#'.$isSpecial.'#'.$isLen;
        
    }
    
    public function getAgeByBirthDate($birthdate){
         //date in mm/dd/yyyy format; or it can be in other formats as well
        //date in yyyy-mm-dd format; or it can be in other formats as well
        //explode the date to get month, day and year
        $birthdate = explode("-", $birthdate);
        //get age from date or birthdate
        
        if(isset($birthdate[2])){
            $age = (date("md", date("U", mktime(0, 0, 0, $birthdate[1], $birthdate[2], $birthdate[0]))) > date("md")
              ? ((date("Y") - $birthdate[0]) - 1)
              : (date("Y") - $birthdate[0]));
        }else{
            $age = 0;
        }
        return $age;
    }
    
    function isLocalhost() {
        $whitelist = array( '127.0.0.1', '::1' );
        if( in_array( $_SERVER['REMOTE_ADDR'], $whitelist) )
            return true;
    }
    
    public function sFullNumber($number){
        if (preg_match('/^[0-9]+$/', $number)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function cropImage($orig, $to_dir,$width,$height,$x, $y){
        $path_parts = pathinfo($orig);
        
        
        // This sets it to a .jpg, but you can change this to png or gif 
        if(strtolower($path_parts['extension'])== 'jpeg' || strtolower($path_parts['extension'])== 'jpg'){
            header('Content-type: image/jpeg'); 
        }else if(strtolower($path_parts['extension'])== 'png'){
            header('Content-type: image/png'); 
        }else if(strtolower($path_parts['extension'])== 'gif'){
            header('Content-type: image/gif'); 
        }
        
        $img = imagecreatetruecolor($width, $height);
        
        if(strtolower($path_parts['extension'])== 'jpeg' || strtolower($path_parts['extension'])== 'jpg'){
            $org_img = imagecreatefromjpeg($orig); 
        }else if(strtolower($path_parts['extension'])== 'png'){
            $org_img = imagecreatefrompng($orig); 
        }else if(strtolower($path_parts['extension'])== 'gif'){
            $org_img = imagecreatefromgif($orig);
        }
        
        $ims = getimagesize($orig);
        imagecopy($img,$org_img, 0, 0, $x, $y, $ims[0], $ims[1]);
        
        if($path_parts['extension']== 'jpeg'){
            imagejpeg($img, $to_dir."/".$path_parts['filename'].".jpeg", 90);
        }else if($path_parts['extension']== 'JPEG'){
            imagejpeg($img, $to_dir."/".$path_parts['filename'].".JPEG", 90);
        }else if($path_parts['extension']== 'jpg'){
            imagejpeg($img, $to_dir."/".$path_parts['filename'].".jpg", 90);
        }else if($path_parts['extension']== 'JPG'){
            imagejpeg($img, $to_dir."/".$path_parts['filename'].".JPG", 90);
        }else if($path_parts['extension']== 'png'){
            imagepng($img, $to_dir."/".$path_parts['filename'].".png", 90);
        }else if($path_parts['extension']== 'PNG'){
            imagepng($img, $to_dir."/".$path_parts['filename'].".PNG", 90);
        }else if($path_parts['extension']== 'gif'){
            imagegif($img, $to_dir."/".$path_parts['filename'].".gif", 90);
        }else if($path_parts['extension']== 'GIF'){
            imagegif($img, $to_dir."/".$path_parts['filename'].".GIF", 90);
        }
        
        imagedestroy($img);
        
    }
    
    public function countDays($day, $start, $end)
    {        
        //get the day of the week for start and end dates (0-6)
        $w = array(date('w', $start), date('w', $end));

        //get partial week day count
        if ($w[0] < $w[1])
        {            
            $partialWeekCount = ($day >= $w[0] && $day <= $w[1]);
        }else if ($w[0] == $w[1])
        {
            $partialWeekCount = $w[0] == $day;
        }else
        {
            $partialWeekCount = ($day >= $w[0] || $day <= $w[1]);
        }

        //first count the number of complete weeks, then add 1 if $day falls in a partial week.
        return floor( ( $end-$start )/60/60/24/7) + $partialWeekCount;
    }
    
    public function isUrlExist($url){
        $ch = curl_init($url);    
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if($code == 200){
           $status = true;
        }else{
          $status = false;
        }
        curl_close($ch);
       return $status;
    }
    
    public function _ago($time) {
        $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
        $lengths = array("60","60","24","7","4.35","12","10");

        $now = time();

            $difference     = $now - $time;
            $tense         = "ago";

        for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
            $difference /= $lengths[$j];
        }

        $difference = round($difference);

        if($difference != 1) {
            $periods[$j].= "s";
        }

        return "$difference $periods[$j] ago ";
    }
    
    public function getDirNameFromEmail($email){
        $email = strtolower($email);
        $email = str_replace(' ', '-', $email); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $email); // Removes special chars.
    }
    
    public function testTypes(){
        return array('normal', 'aia','weight-loss', 'strength', 'fitness', 'run');
    }
    

}

?>
