<?php

namespace Acme\HeadOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DisclaimerController extends Controller
{
    public function disclaimerAction()
    {
       return $this->render('AcmeHeadOfficeBundle:Disclaimer:disclaimer.html.twig'); 
    }

}
