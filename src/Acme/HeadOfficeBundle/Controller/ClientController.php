<?php

namespace Acme\HeadOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

class ClientController extends GlobalController
{
    public function clientAction()
    {
        $session = $this->getRequest()->getSession();
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        if(isset($_POST['search'])){
            $session->set('search_param', $_POST['search']); 
            $_POST['search'] = filter_var($_POST['search'],FILTER_SANITIZE_STRING);
            $data = $this->getClubClientBySearchParam($_POST['search']);
            return $this->render('AcmeHeadOfficeBundle:Client:client.html.twig',
                array('club_clients'=> $data,
                    'post' => $_POST,
                    'data_count'=>count($data))
                );
        }else{
            return $this->redirect($this->generateUrl('acme_head_office_dashboard'));
        }
    }
    
    public function deleteAction()
    {
        $session = $this->getRequest()->getSession();
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        if(isset($_POST['club_client_id'])){
            $em = $this->getDoctrine()->getEntityManager();
            
            $_POST['club_client_id'] = intval($_POST['club_client_id']);
            if(isset($_POST['type'])){
                if($_POST['type']=='çlub'){
                    $model = $em->getRepository('AcmeHeadOfficeBundle:ClubClient')->findOneBy(array('club_client_id'=>$_POST["club_client_id"]));
                    $em->remove($model);
                    $em->flush();
                }
            }
            if($_SERVER['SERVER_NAME'] == 'localhost'){
                $program_host="localhost";
                $program_uname="root";
                $program_pass="";
                $program_database = "healthy_life_project_ohwp";
            }else{
                $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
                $program_uname="hlproot";
                $program_pass="_jWBqa)67?BJq(j-";
                $program_database = "hlp12weekProgram";
            }

            $program_connection=mysqli_connect($program_host,$program_uname,$program_pass, $program_database) or die("Database Connection Failed");

            $mysqli = new \Mysqli($program_host, $program_uname, $program_pass, $program_database);
            $email = mysqli_real_escape_string($mysqli,$_POST['club_client_email']); 

            mysqli_query($program_connection,"DELETE FROM tbl_client WHERE email='".$email."'");


            return $this->redirect($this->generateUrl('acme_head_office_club_admin') . "?cid=" . $model->getClubId() );
        }
        
    }
    
    
    public function editClubClientAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        
        if(isset($_POST['club_client_id']) && isset($_POST['club_client_email'])){
            if($_POST['slug']=='assign'){
                if($_POST['password'] != ''){
                
                    $em = $this->getDoctrine()->getManager();
                    $em->getConnection()->beginTransaction(); 


                    $model = $em->getRepository('AcmeHeadOfficeBundle:ClubClient')->findOneBy(array('club_client_id'=>$_POST["club_client_id"]));
                    $password = $mod->passGenerator($_POST['password']);
                    $model->setPassword($password);
                    $em->persist($model);
                    $em->flush();

                    $validator = $this->get('validator');
                    $errors = $validator->validate($model);
                    $error_count = count($errors);

                    if($error_count == 0){

                        if($_POST['password'] == ''){
                            $errors[] = array('message'=>'Password must not be blank.');
                            $error_count += 1;
                        }
                    }

                    if($error_count == 0){

                        $em->getConnection()->commit(); 

                        if($_SERVER['SERVER_NAME'] == 'localhost'){
                            $program_host="localhost";
                            $program_uname="root";
                            $program_pass="";
                            $program_database = "healthy_life_project_ohwp";
                        }else{
                            $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
                            $program_uname="hlproot";
                            $program_pass="_jWBqa)67?BJq(j-";
                            $program_database = "hlp12weekProgram";
                        }

                        $program_connection=mysqli_connect($program_host,$program_uname,$program_pass, $program_database) or die("Database Connection Failed");

                        mysqli_query($program_connection,"UPDATE tbl_client SET password='".$password."' WHERE email ='".$_POST["club_client_email"]."'");


                        $this->get('session')->getFlashBag()->add(
                            'success',
                            'Password of '.$_POST['club_client_email'] . ' has been assigned successfully.'
                        );

                        return $this->redirect($this->generateUrl('acme_head_office_club_client_edit') . "?cid=".$_POST['club_client_id']."&email=".$_POST['club_client_email']);

                    }else{
                        $em->getConnection()->rollback();
                        $em->close();

                        $this->get('session')->getFlashBag()->add(
                            'error',
                            $errors
                        );


                        $data =$this->getClubClientByIdAndEmail($_POST["club_client_id"],$_POST["club_client_email"]);
                        return $this->render('AcmeHeadOfficeBundle:Client:edit_client.html.twig',
                                array(
                                'post'=> $data
                            ));
                    }
                
                }else{
                     $errors[] = array('message'=>'Password must not be blank to update.');
                     $this->get('session')->getFlashBag()->add(
                            'error',
                            $errors
                        );
                     return $this->redirect($this->generateUrl('acme_head_office_club_client_edit') . "?cid=".$_POST['club_client_id']."&email=".$_POST['club_client_email']);
                }
            }else{
                
                if($_SERVER['SERVER_NAME'] == 'localhost'){
                    $program_host="localhost";
                    $program_uname="root";
                    $program_pass="";
                    $program_database = "healthy_life_project_ohwp";
                }else{
                    $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
                    $program_uname="hlproot";
                    $program_pass="_jWBqa)67?BJq(j-";
                    $program_database = "hlp12weekProgram";
                }

                $program_connection=mysqli_connect($program_host,$program_uname,$program_pass, $program_database) or die("Database Connection Failed");
                $pincode = rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
                $program_query = mysqli_query($program_connection,"UPDATE tbl_client SET token='".$pincode."' WHERE email ='".$_POST['club_client_email']."'");
                 
                if($program_query){
                    $from = $this->container->getParameter('site_email_address');
                    $subject = 'Healthy Life Project Password Reset';
                    $body = $this->renderView('AcmeHeadOfficeBundle:ResetPasswordClient:email_reset_link.html.twig',
                            array(
                                'token'=> $pincode,
                                'email'=> $_POST['club_client_email'],
                                'fname'=> $_POST['club_client_fname'],
                                'request_form'=>$this->getRequest()->getUriForPath('/admin/reset-form'),
                                'request_undo'=>$this->getRequest()->getUriForPath('/admin/reset-undo-request')
                                )
                            );

                    $this->sendEmail($_POST['club_client_email'], '', $from, $subject, $body);

                    $this->get('session')->getFlashBag()->add(
                                'success',
                                'Password reset link successfully sent to '.$_POST['club_client_email']
                            );
                    return $this->redirect($this->generateUrl('acme_head_office_club_client_edit') . "?cid=".$_POST['club_client_id']."&email=".$_POST['club_client_email']);
                }
            }
        }else{
            $data =$this->getClubClientByIdAndEmail($_GET['cid'],$_GET['email']);
            $_GET['cid'] = intval($_GET['cid']);
            return $this->render('AcmeHeadOfficeBundle:Client:edit_client.html.twig',
                    array(
                    'post'=> $data,
                    'get'=>$_GET
                ));
        }
    }
    
    public function editClub12WeekClientAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        
        if(isset($_POST['client_id']) && isset($_POST['client_email'])){
            if($_POST['slug']=='assign'){
                if($_POST['password'] != ''){

                    if($_SERVER['SERVER_NAME'] == 'localhost'){
                        $program_host="localhost";
                        $program_uname="root";
                        $program_pass="";
                        $program_database = "healthy_life_project_ohwp";
                    }else{
                        $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
                        $program_uname="hlproot";
                        $program_pass="_jWBqa)67?BJq(j-";
                        $program_database = "hlp12weekProgram";
                    }

                    $program_connection=mysqli_connect($program_host,$program_uname,$program_pass, $program_database) or die("Database Connection Failed");
                    $password = $mod->passGenerator($_POST['password']);
                    mysqli_query($program_connection,"UPDATE tbl_client SET password='".$password."' WHERE email ='".$_POST["client_email"]."'");


                    $this->get('session')->getFlashBag()->add(
                        'success',
                        'Password of '.$_POST['client_email'] . ' has been assigned successfully.'
                    );

                    return $this->redirect($this->generateUrl('acme_head_office_club_12week_client_edit') . "?cid=".$_POST['client_id']."&email=".$_POST['client_email']);

                    
                
                }else{
                     $errors[] = array('message'=>'Password must not be blank to update.');
                     $this->get('session')->getFlashBag()->add(
                            'error',
                            $errors
                        );
                     return $this->redirect($this->generateUrl('acme_head_office_club_12week_client_edit') . "?cid=".$_POST['client_id']."&email=".$_POST['client_email']);
                }
            }else{
                
                if($_SERVER['SERVER_NAME'] == 'localhost'){
                    $program_host="localhost";
                    $program_uname="root";
                    $program_pass="";
                    $program_database = "healthy_life_project_ohwp";
                }else{
                    $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
                    $program_uname="hlproot";
                    $program_pass="_jWBqa)67?BJq(j-";
                    $program_database = "hlp12weekProgram";
                }

                $program_connection=mysqli_connect($program_host,$program_uname,$program_pass, $program_database) or die("Database Connection Failed");
                $pincode = rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
                $program_query = mysqli_query($program_connection,"UPDATE tbl_client SET token='".$pincode."' WHERE email ='".$_POST['client_email']."'");
                 
                if($program_query){
                    $from = $this->container->getParameter('site_email_address');
                    $subject = 'Healthy Life Project Password Reset';
                    $body = $this->renderView('AcmeHeadOfficeBundle:ResetPasswordClient:email_reset_link.html.twig',
                            array(
                                'token'=> $pincode,
                                'email'=> $_POST['client_email'],
                                'fname'=> $_POST['client_fname'],
                                'request_form'=>$this->getRequest()->getUriForPath('/admin/reset-form'),
                                'request_undo'=>$this->getRequest()->getUriForPath('/admin/reset-undo-request')
                                )
                            );

                    $this->sendEmail($_POST['client_email'], '', $from, $subject, $body);

                    $this->get('session')->getFlashBag()->add(
                                'success',
                                'Password reset link successfully sent to '.$_POST['client_email']
                            );
                    return $this->redirect($this->generateUrl('acme_head_office_club_12week_client_edit') . "?cid=".$_POST['client_id']."&email=".$_POST['client_email']);
                }
            }
        }else{
            $data =$this->get12weekProgramClientById($_GET['cid']);
            $_GET['cid'] = intval($_GET['cid']);
            return $this->render('AcmeHeadOfficeBundle:Client:edit_12week_client.html.twig',
                    array(
                    'post'=> $data,
                    'get'=>$_GET
                ));
        }
    }
    
}
