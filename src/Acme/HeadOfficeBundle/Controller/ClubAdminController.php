<?php

namespace Acme\HeadOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

use Acme\HeadOfficeBundle\Entity\Club;
use Acme\HeadOfficeBundle\Entity\ClubAdmin;

class ClubAdminController extends GlobalController
{
    
    public function clubAdminAction()
    {
        $session = $this->getRequest()->getSession();
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        
        if(!isset($_GET['cid'])){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        
        $_GET['cid'] = intval($_GET['cid']);
        
        return $this->render('AcmeHeadOfficeBundle:ClubAdmin:club_admin.html.twig',
                array('club_admins'=> $this->getClubAdminsByClubId($_GET['cid'], true),
                    'club' => $this->getClubById($_GET['cid']),
                    'get' => $_GET)
                );
    }
    
    
    public function addEditClubAdminAction($slug)
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        
        if(isset($_POST['email'])){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            
            
            if(strtolower(trim($slug)) == 'new'){
                $_POST['club_id'] = intval($_POST['club_id']);
                $model = new ClubAdmin();
                $model->setClubId($_POST['club_id']);
            }else{
                $model = $em->getRepository('AcmeHeadOfficeBundle:ClubAdmin')->findOneBy(array('club_admin_id'=>$_POST["club_admin_id"]));
            }
            
            $model->setFname($_POST['fname']);
            $model->setLname($_POST['lname']);
            if(strtolower(trim($slug)) == 'new'){
                $model->setPassword($mod->passGenerator($_POST['password']));
            }elseif(strtolower(trim($slug)) == 'edit' && $_POST['password'] != ''){
                $model->setPassword($mod->passGenerator($_POST['password']));
            }
            $model->setEmail($_POST['email']);
            $model->setStatus($_POST['status']);
            $model->setRole($_POST['role']);
            if(isset($_POST['s_aia_approved_by_admin'])){
                $model->setSAiaApproved(1);
                $model->setSAiaApprovedByAdmin(1);
            }else{
                $model->setSAiaApproved(0);
                $model->setSAiaApprovedByAdmin(0);
            }
            $em->persist($model);
            $em->flush();
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            if($error_count == 0){
                if($mod->isEmailValid($_POST['email']) == false || ($_POST['password'] != $_POST['confirm_password'])){
                    $errors = array();
                }
                
                if(!$mod->isEmailValid($_POST['email'])){
                    $errors[] = array('message'=>$_POST['email'].' is not a valid email.');
                    $error_count += 1;
                }

                if($_POST['password'] != $_POST['confirm_password']){
                    $errors[] = array('message'=>'Password must be repeated exactly.');
                    $error_count += 1;
                }
            }
            
            if($error_count == 0){
                
                $em->getConnection()->commit(); 
                
                if(strtolower(trim($slug)) == 'new'){
                    $this->get('session')->getFlashBag()->add(
                        'success',
                        $_POST['fname'] . ' ' . $_POST['lname'] . ' has been added successfully.'
                    );
                }else{
                    $this->get('session')->getFlashBag()->add(
                        'success',
                        $_POST['fname'] . ' ' . $_POST['lname'] . ' has been updated successfully.'
                    );
                }
                return $this->redirect($this->generateUrl('acme_head_office_club_admin_add_edit', array('slug' => 'edit')) . "?caid=".$model->getClubAdminId());
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
               
                return $this->render('AcmeHeadOfficeBundle:ClubAdmin:add_edit_club_admin.html.twig',
                        array('errors'=>$errors,
                            'post'=>$_POST,
                            'get'=>$_GET,
                            'clubs' => $this->getClubs('active')
                        ));
            }
        }
        
        if(strtolower(trim($slug)) == 'new'){
            return $this->render('AcmeHeadOfficeBundle:ClubAdmin:add_edit_club_admin.html.twig',
                    array(
                            'get' => $_GET,
                            'clubs' => $this->getClubs('active')
                        ));
        }else{
            $_GET['caid'] = intval($_GET['caid']);
            return $this->render('AcmeHeadOfficeBundle:ClubAdmin:add_edit_club_admin.html.twig',
                    array(
                        'post'=> $this->getClubAdminUserById($_GET['caid']),
                        'get'=>$_GET,
                        'clubs' => $this->getClubs('active')
                    ));
        }
    }
    
    public function deleteAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        
        if(isset($_POST['club_admin_id'])){
            $em = $this->getDoctrine()->getEntityManager();

            $_POST['club_admin_id'] = intval($_POST['club_admin_id']);
            $model = $em->getRepository('AcmeHeadOfficeBundle:ClubAdmin')->findOneBy(array('club_admin_id'=>$_POST["club_admin_id"]));
            $em->remove($model);
            $em->flush();


//            $this->get('session')->getFlashBag()->add(
//                    'success',
//                    'Club admin has been deleted successfully.'
//                );
            
            return $this->redirect($this->generateUrl('acme_head_office_club_admin') . "?cid=" . $model->getClubId() );
        }
        
    }
    
}
