<?php

namespace Acme\HeadOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

class ProspectsController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function prospectsAction(){
        $session = $this->getRequest()->getSession();
        
        $session->set('active_page', 'prospects' ); 
         if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
//        $data = $this->prospectClientByClub($session->get('club_id'));

        return $this->render('AcmeHeadOfficeBundle:Prospects:prospects.html.twig');
    }
    
    public function prospectsListAction(){
        $session = $this->getRequest()->getSession();
        
        $sort_key = (isset($_REQUEST['order'][0]['column'])) ? $_REQUEST['order'][0]['column'] : 0; 
        $sort_key = filter_var($sort_key, FILTER_SANITIZE_NUMBER_INT);
        $sort_value = (isset($_REQUEST['order'][0]['dir'])) ? $_REQUEST['order'][0]['dir'] : 'asc'; 
        $sort_value = ($sort_value == 'asc') ? 'asc' : 'desc';
        $sort_field = (isset($_REQUEST['columns'][$sort_key]['data'])) ? $_REQUEST['columns'][$sort_key]['data'] : 'name';
        $start = (isset($_REQUEST['start'])) ? $_REQUEST['start'] : 0;
        $start = filter_var($start, FILTER_SANITIZE_NUMBER_INT);
        $length = (isset($_REQUEST['length'])) ? $_REQUEST['length'] : 0;
        $length = filter_var($length, FILTER_SANITIZE_NUMBER_INT);
        
        
        $datatables_settings = array(
                    'sort_field'=>$sort_field,
                    'sort_order'=>$sort_value,
                    'start'=> $start,
                    'length'=> $length
                );
        
        $data = $this->prospectClient($datatables_settings);
         $response = '{ 
                "recordsTotal": '. $data['data_count'] .',
                "recordsFiltered": '. $data['data_count'] .',
                "data":'. json_encode($data['data'],0).
            '}';
        
        
        return new response($response);
    }
}
