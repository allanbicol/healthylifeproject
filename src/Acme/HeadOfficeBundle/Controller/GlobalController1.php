<?php

namespace Acme\HeadOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

use Acme\HeadOfficeBundle\Entity\Activities;
use Acme\HeadOfficeBundle\Entity\Payment;
use Doctrine\DBAL\Driver\Mysqli;

class GlobalController extends Controller
{
    public function getLiteTests($status){
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:LiteTest');
        if($status != ''){
            $query = $cust->createQueryBuilder('p')
                ->where('p.status = :status')
                ->setParameter('status', $status)
                ->getQuery();
        }else{
            $query = $cust->createQueryBuilder('p')
                ->getQuery();
        }
        return $query->getArrayResult();
    }
    
    public function getLiteTestDetailsById($lite_test_id){
        $formula = new Model\TestFormulas();
        $mod = new Model\GlobalModel();
        $cust = $this->getDoctrine()->getEntityManager();
        $connection = $cust->getConnection();
        $lite_test_id = intval($lite_test_id);
        $statement = $connection->prepare("SELECT 
                    t.*
                FROM tbl_lite_test t 
                WHERE t.lite_test_id = :lite_test_id ");
        $statement->bindValue('lite_test_id', $lite_test_id);
        
        $statement->execute();
        $data =  $statement->fetchAll();
        
        if(count($data) > 0){
            $data[0]['lifestyle_physical_activity_result'] = $formula->physicalActivityResult($data[0]['lifestyle_physical_activity']);
            $data[0]['lifestyle_smoking_result'] = $formula->smokingHabitsResult($data[0]['lifestyle_smoking']);
            $data[0]['lifestyle_alcohol_result'] = $formula->alcoholConsumptionResult($data[0]['lifestyle_alcohol']);
            $data[0]['lifestyle_nutrition_result'] = $formula->nutritionResult($data[0]['lifestyle_nutrition']);
            $data[0]['lifestyle_mental_health_result'] = $formula->mentalHealthResult($data[0]['lifestyle_mental_health']);
            $data[0]['lifestyle_risk_profile_result'] = $formula->riskProfileResult($data[0]['lifestyle_risk_profile']);
            $data[0]['physiological_vo2_result'] = $formula->vo2Result($data[0]['physiological_vo2']);
            $data[0]['physiological_balance_result'] = $formula->balanceResult($data[0]['physiological_balance']);
            $data[0]['physiological_squat_result'] = $formula->squatTestResult($data[0]['physiological_squat']);
            $data[0]['physiological_situp_result'] = $formula->sevenStateSitUpTestResult($data[0]['physiological_situp']);
            
            $data[0]['biometric_body_fat_result'] = $formula->bodyFatResult($data[0]['biometric_body_fat']);
            $data[0]['biometric_blood_pressure_result'] = $formula->bloodPressureResult($data[0]['biometric_blood_pressure'], $data[0]['biometric_blood_pressure_systolic'] , $data[0]['biometric_blood_pressure_diastolic']);
            $data[0]['biometric_blood_oxygen_result'] = $formula->bloodOxygenResult($data[0]['biometric_blood_oxygen']);
            $data[0]['biometric_waist_result'] = $formula->waistResult($data[0]['biometric_waist'], $data[0]['biometric_waist_value']);
            
            $data[0]['healthy_life_score_outcome'] = $formula->outcomes($data[0]['healthy_life_score']);
            
            
            $data[0]['physical_activity_crf'] = $formula->crf($data[0]['lifestyle_physical_activity']);
            $data[0]['smoking_crf'] = $formula->crf($data[0]['lifestyle_smoking']);
            $data[0]['alcohol_crf'] = $formula->crf($data[0]['lifestyle_alcohol']);
            $data[0]['nutrition_crf'] = $formula->crf($data[0]['lifestyle_nutrition']);
            $data[0]['mental_health_crf'] = $formula->crf($data[0]['lifestyle_mental_health']);
            $data[0]['risk_profile_crf'] = $formula->crf($data[0]['lifestyle_risk_profile']);
            $data[0]['body_fat_crf'] = $formula->crf($data[0]['biometric_body_fat']);
            $data[0]['blood_pressure_crf'] = $formula->crf($data[0]['biometric_blood_pressure']);
            $data[0]['blood_oxygen_crf'] = $formula->crf($data[0]['biometric_blood_oxygen']);
            $data[0]['vo2_crf'] = $formula->crf($data[0]['physiological_vo2']);
            $data[0]['balance_crf'] = $formula->crf($data[0]['physiological_balance']);
            $data[0]['squat_test_crf'] = $formula->crf($data[0]['physiological_squat']);
            $data[0]['seven_stage_situp_crf'] = $formula->crf($data[0]['physiological_situp']);
            $data[0]['waist_crf'] = $formula->crf($data[0]['biometric_waist']);
        }
        
        return (count($data) > 0) ? $data[0] : '';
    }
    
    public function getHeadOfficeAdminUsers($status = ''){
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:HeadOfficeAdmin');
        if($status != ''){
            $query = $cust->createQueryBuilder('p')
                ->where('p.status = :status')
                ->setParameter('status', $status)
                ->getQuery();
        }else{
            $query = $cust->createQueryBuilder('p')
                ->getQuery();
        }
        return $query->getArrayResult();
    }
    
    public function getHeadOfficeAdminUserById($id){
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:HeadOfficeAdmin');
        $query = $cust->createQueryBuilder('p')
            ->where('p.ho_admin_id = :ho_admin_id')
            ->setParameter('ho_admin_id', $id)
            ->getQuery();
        $data = $query->getArrayResult();
        return (count($data) > 0) ? $data[0] : array();
    }

    public function getClubs($status = ''){
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:Club');
        if($status != ''){
            $query = $cust->createQueryBuilder('p')
                ->where('p.status = :status')
                ->setParameter('status', $status)
                ->getQuery();
        }else{
            $query = $cust->createQueryBuilder('p')
                ->getQuery();
        }
        $result = $query->getArrayResult();
        
        $data = array();
        for($i=0; $i<count($result); $i++){
            $data[] = array(
                'club_id' => $result[$i]['club_id'],
                'club_name' => $result[$i]['club_name'],
                'address' => $result[$i]['address'],
                'city' => $result[$i]['city'],
                'state' => $result[$i]['state'],
                'country' => $result[$i]['country'],
                'available_credit' => $result[$i]['available_credit'],
                'status' => $result[$i]['status'],
                'club_admins' => $this->getClubAdminsByClubId($result[$i]['club_id'])
                );
        }
        
        return $data;
    }
    
    
    public function getClubById($id){
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:Club');
        $query = $cust->createQueryBuilder('p')
            ->where('p.club_id = :club_id')
            ->setParameter('club_id', $id)
            ->getQuery();
        $data = $query->getArrayResult();
        return (count($data) > 0) ? $data[0] : array();
    }
    
    public function getClubAdminsByClubId($id){
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:ClubAdmin');
        $query = $cust->createQueryBuilder('p')
            ->where('p.club_id = :club_id')
            ->setParameter('club_id', $id)
            ->getQuery();
        return $query->getArrayResult();
    }
	
	public function getClubsByAdminEmail($email){
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:Club');
        $query = $cust->createQueryBuilder('p')
			->select("p.club_id, p.club_name")
			->leftJoin('AcmeHeadOfficeBundle:ClubAdmin', 'a', 'WITH', 'a.club_id = p.club_id')
            ->where('a.email = :email')
            ->setParameter('email', $email)
            ->getQuery();
        return $query->getArrayResult();
    }
    
    
    
    public function getClubAdminUserById($id){
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:ClubAdmin');
        $query = $cust->createQueryBuilder('p')
            ->where('p.club_admin_id = :club_admin_id')
            ->setParameter('club_admin_id', $id)
            ->getQuery();
        $data = $query->getArrayResult();
        return (count($data) > 0) ? $data[0] : array();
    }
    
    
    
    public function getStates(){
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:States');
        $query = $cust->createQueryBuilder('p')
            ->getQuery();
        return $query->getArrayResult();
    }
    
    public function getStatesForClub(){
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:States');
         $query = $cust->createQueryBuilder('p')
            ->where('p.is_club_registration = :is_club_registration')
            ->setParameter('is_club_registration', 1)
            ->getQuery();
        return $query->getArrayResult();
    }
    
    public function getLams(){
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:Lams');
        $query = $cust->createQueryBuilder('p')
            ->getQuery();
        return $query->getArrayResult();
    }
    
    public function getEvents($club_id = ''){
        if($club_id == ''){
            $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:Event');
            $query = $cust->createQueryBuilder('p')
                ->getQuery();
        }else{
            $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:Event');
            $query = $cust->createQueryBuilder('p')
                ->where("p.club_id = :club_id OR p.club_id = '' OR p.club_id IS NULL")
                ->setParameter('club_id', $club_id)
                ->getQuery();
        }
        return $query->getArrayResult();
    }
    
    public function getEventById($id){
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:Event');
        $query = $cust->createQueryBuilder('p')
            ->where('p.event_id = :event_id')
            ->setParameter('event_id', $id)
            ->getQuery();
        $data = $query->getArrayResult();
        return (count($data) > 0) ? $data[0] : array();
    }
    
    public function getClubClients($status = NULL, $club_id = NULL, $limit_start_at = NULL, $max_list = NULL, $sort_by = NULL, $keyword_search = NULL){
        $mod = new Model\GlobalModel();
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:ClubClient');
        $sort = array();
        if($sort_by != NULL){
            if($sort_by == 'coach'){
                $sort = array( 'sort_by'=>'coach', 'order'=> 'ASC');
            }elseif($sort_by == 'results'){
                $sort = array( 'sort_by'=>'max_healthy_life_score', 'order'=> 'DESC');
                
            }elseif($sort_by == 'age'){
                $sort = array( 'sort_by'=>'birthdate', 'order'=> 'ASC');
                
            }elseif($sort_by == 'gender'){
                $sort = array( 'sort_by'=>'gender', 'order'=> 'ASC');
            }else{
                $sort = array( 'sort_by'=>'coach', 'order'=> 'ASC');
            }
        }else{
            $sort = array( 'sort_by'=>'coach', 'order'=> 'ASC');
        }
        
        if($status != NULL && $club_id != NULL){
            $sqlin = explode(" ",$keyword_search);
            $keywords = implode("','",$sqlin);  
            $query = $cust->createQueryBuilder('p')
                ->select("p.club_client_id, p.club_id, p.club_admin_id, p.email, 
                p.phone, p.birthdate as birthdate, p.gender as gender, p.picture, p.height, p.status, 
                p.fname, p.lname, a.fname AS coach_fname, 
                a.lname AS coach_lname, CONCAT(a.fname, a.lname) as coach,
                MAX(t.healthy_life_score) as max_healthy_life_score")
                ->leftJoin('AcmeHeadOfficeBundle:ClubAdmin', 'a', 'WITH', 'a.club_admin_id = p.club_admin_id')
                ->leftJoin('AcmeHeadOfficeBundle:ClientTest', 't', 'WITH', 't.club_client_id = p.club_client_id')
                ->where("p.status = :status 
                    AND p.club_id = :club_id
                    AND (p.fname LIKE :keyword
                            OR p.lname LIKE :keyword
                            OR a.fname LIKE :keyword
                            OR a.lname LIKE :keyword
                            
                            OR p.fname IN (:keyword_array)
                            OR p.lname IN (:keyword_array)
                            OR a.fname IN (:keyword_array)
                            OR a.lname IN (:keyword_array))")
                ->setParameter('keyword', '%'.$keyword_search.'%')
                    ->setParameter('keyword_array', $sqlin)
                ->setParameter('status', $status)
                ->setParameter('club_id', $club_id)
                ->orderBy($sort['sort_by'], $sort['order'])
                ->groupBy('p.club_client_id')
                ->setMaxResults($max_list)
                ->setFirstResult($limit_start_at)
                ->getQuery();
            
        }elseif($status != NULL && $club_id == NULL){
            $query = $cust->createQueryBuilder('p')
                ->select("p.club_client_id, p.club_id, p.club_admin_id, p.email, 
                p.phone, p.birthdate, p.gender, p.picture, p.height, p.status, 
                p.fname, p.lname, a.fname AS coach_fname, 
                a.lname AS coach_lname")
                ->leftJoin('AcmeHeadOfficeBundle:ClubAdmin', 'a', 'WITH', 'a.club_admin_id = p.club_admin_id')
                ->where('p.status = :status
                    AND (p.fname LIKE :keyword 
                            OR p.lname LIKE :keyword
                            OR a.fname LIKE :keyword
                            OR a.lname LIKE :keyword)')
                ->setParameter('status', $status)
                ->setParameter('keyword', '%'.$keyword_search.'%')
                ->setMaxResults($max_list)
                ->setFirstResult($limit_start_at)
                ->getQuery();
            
        }elseif($status == NULL && $club_id != NULL){
            $query = $cust->createQueryBuilder('p')
                ->select("p.club_client_id, p.club_id, p.club_admin_id, p.email, 
                p.phone, p.birthdate, p.gender, p.picture, p.height, p.status, 
                p.fname, p.lname, a.fname AS coach_fname, 
                a.lname AS coach_lname")
                ->leftJoin('AcmeHeadOfficeBundle:ClubAdmin', 'a', 'WITH', 'a.club_admin_id = p.club_admin_id')
                ->where('p.club_id = :club_id
                    AND (p.fname LIKE :keyword 
                            OR p.lname LIKE :keyword
                            OR a.fname LIKE :keyword
                            OR a.lname LIKE :keyword)')
                ->setParameter('keyword', '%'.$keyword_search.'%')
                ->setParameter('club_id', $club_id)
                ->setMaxResults($max_list)
                ->setFirstResult($limit_start_at)
                ->getQuery();
            
        }else{
            $query = $cust->createQueryBuilder('p')
                ->select("p.club_client_id, p.club_id, p.club_admin_id, p.email, 
                p.phone, p.birthdate, p.gender, p.picture, p.height, p.status, 
                p.fname, p.lname, a.fname AS coach_fname, 
                a.lname AS coach_lname")
                ->leftJoin('AcmeHeadOfficeBundle:ClubAdmin', 'a', 'WITH', 'a.club_admin_id = p.club_admin_id')
                ->setMaxResults($max_list)
                ->setFirstResult($limit_start_at)
                ->getQuery();
        }
        $data = $query->getArrayResult();
        
        for($i=0; $i<count($data); $i++){
            $data[$i]['age'] = $mod->getAgeByBirthDate($data[$i]['birthdate']);
        }
        
        return $data;
    }
    
    public function getClubClientsWithRecentTestResult($status, $club_id, $limit_start_at = NULL, $max_list = NULL, $sort_by = NULL, $keyword_search = NULL){
        
        $clients = $this->getClubClients($status, $club_id, $limit_start_at, $max_list, $sort_by, $keyword_search);
        
        for($i=0; $i<count($clients); $i++){
            $prog_client_details = $this->get12weekProgramClientByEmail($clients[$i]['email']);
            $clients[$i]['recent_test'] = $this->getClientTestHistory($clients[$i]['club_client_id'], 'recent', 'finalised');
            $clients[$i]['all_test'] = $this->getClientTestHistory($clients[$i]['club_client_id'], 'all', 'finalised');
            $clients[$i]['details_from_12weekProgram'] = $prog_client_details;
//            $clients[$i]['week_total_actions'] = $this->getProgramClientDayItems( $clients[$i]['email'], $prog_client_details['working_week'], NULL, NULL, 'quote');
//            $clients[$i]['week_complete_actions']= $this->getProgramClientDayItems( $clients[$i]['email'], $prog_client_details['working_week'] , NULL, 1);
//            $clients[$i]['overall_actions']= $this->getProgramClientDayItems( $clients[$i]['email'], NULL, NULL, NULL, 'quote');
//            $clients[$i]['overall_complete_actions']= $this->getProgramClientDayItems( $clients[$i]['email'], NULL , NULL, 1);
        }
        
        return $clients;
        
    }    
    public function getClubClientById($id){
        $mod = new Model\GlobalModel();
        
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:ClubClient');
        $query = $cust->createQueryBuilder('p')
            ->select("p.club_client_id, p.club_id, p.club_admin_id, p.opt_client_id, p.email, 
                p.phone, p.birthdate, p.gender, p.picture, p.height, p.status, 
                p.fname, p.lname, a.fname AS coach_fname, 
                a.lname AS coach_lname")
            ->leftJoin('AcmeHeadOfficeBundle:ClubAdmin', 'a', 'WITH', 'a.club_admin_id = p.club_admin_id')
            ->where('p.club_client_id = :club_client_id')
            ->setParameter('club_client_id', $id)
            ->getQuery();
        $data = $query->getArrayResult();
        
        if((count($data) > 0)){
            $data[0]['age'] = $mod->getAgeByBirthDate($data[0]['birthdate']);
            $data[0]['recent_test'] = $this->getClientFirstOrRecentTestDetails($id, 'recent');
        }
        
        return (count($data) > 0) ? $data[0] : array();
    }
    
    public function getTestQuestionnaire($code, $club_client_id = NULL,  $client_test_id = NULL){
        
        if($club_client_id != NULL && $client_test_id == NULL){
            
            $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnare');
            $query = $cust->createQueryBuilder('p')
                ->select('p.lq_id, 
                    p.lq_code, 
                    p.question,
                    p.class,
                    a.lqca_id, 
                    a.client_test_id, 
                    a.club_client_id, 
                    a.answer,
                    a.answer_datetime,
                    a.status')
                ->leftJoin('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer', 'a', 'WITH', 'a.lq_id = p.lq_id AND a.club_client_id = :club_client_id AND a.status = :status')
                ->where('p.lq_code = :lq_code')
                ->setParameter('lq_code', $code)
                ->setParameter('club_client_id', $club_client_id)
                ->setParameter('status', 'draft')
                ->getQuery();
            
        }elseif($club_client_id != NULL && $client_test_id != NULL){
            
            $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnare');
            $query = $cust->createQueryBuilder('p')
                ->select('p.lq_id, 
                    p.lq_code, 
                    p.question,
                    p.class,
                    a.lqca_id, 
                    a.client_test_id, 
                    a.club_client_id, 
                    a.answer,
                    a.answer_datetime,
                    a.status')
                ->leftJoin('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer', 'a', 'WITH', 'a.lq_id = p.lq_id  AND a.club_client_id = :club_client_id AND a.client_test_id = :client_test_id')
                ->where('p.lq_code = :lq_code')
                ->setParameter('lq_code', $code)
                ->setParameter('club_client_id', $club_client_id)
                ->setParameter('client_test_id', $client_test_id)
                ->getQuery();
        }else{
            $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnare');
            $query = $cust->createQueryBuilder('p')
                ->where('p.lq_code = :lq_code')
                ->setParameter('lq_code', $code)
                ->getQuery();
        }
        
        return $query->getArrayResult();
    }
    
    public function sLifestyleTestQuestionnaireAnswersDone($code, $club_client_id, $client_test_id = NULL){
        
            $questionnaire = $this->getTestQuestionnaire($code, $club_client_id, $client_test_id);
            //print_r($questions);
            $questions = count($questionnaire);
            $answers = 0;
            for($i=0; $i<count($questionnaire); $i++){
                //$item = $this->getSpecificTestQuestionnaireClientAnswer($questionnaire[$i]['lq_id'], $club_client_id, $client_test_id);
                $answers += (isset($questionnaire[$i]['answer']) && $questionnaire[$i]['answer'] != '') ? 1 : 0;
                //echo $questions .' == '. $answers . '<br>';
            }
            return ($questions == $answers) ? true : false;
    }
    
    public function sLifestyleTestQuestionnaireWithAnswers($code, $club_client_id, $client_test_id = NULL){
        
            $questionnaire = $this->getTestQuestionnaire($code, $club_client_id, $client_test_id);
            //print_r($questions);
            $questions = count($questionnaire);
            $answers = 0;
            for($i=0; $i<count($questionnaire); $i++){
                //$item = $this->getSpecificTestQuestionnaireClientAnswer($questionnaire[$i]['lq_id'], $club_client_id, $client_test_id);
                $answers += (isset($questionnaire[$i]['answer']) && $questionnaire[$i]['answer'] != '') ? 1 : 0;
                //echo $questions .' == '. $answers . '<br>';
            }
            return (count($answers) > 0) ? true : false;
    }
    
    public function getTestQuestionnaireOptionsByCode($code){
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareOptions');
        $query = $cust->createQueryBuilder('p')
            ->where('p.lq_code = :lq_code')
            ->setParameter('lq_code', $code)
            ->getQuery();
        return $query->getArrayResult();
    }
    
    public function getSpecificTestQuestionnaireClientAnswer($lq_id, $club_client_id, $client_test_id = NULL){
        
        if($client_test_id == NULL){ 
            $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer');
            $query = $cust->createQueryBuilder('a')
                ->where('a.lq_id = :lq_id AND a.club_client_id = :club_client_id AND a.client_test_id IS NULL')
                ->setParameter('lq_id', $lq_id)
                ->setParameter('club_client_id', $club_client_id)
                ->getQuery();
        }else{
            $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer');
            $query = $cust->createQueryBuilder('a')
                ->where('a.lq_id = :lq_id AND a.club_client_id = :club_client_id AND a.client_test_id = :client_test_id')
                ->setParameter('lq_id', $lq_id)
                ->setParameter('club_client_id', $club_client_id)
                ->setParameter('client_test_id', $client_test_id)
                ->getQuery();
        }
        $data = $query->getArrayResult();
        
        
        return (count($data) > 0) ? $data[0]['answer'] : '';
    }
    
    public function getClientFirstOrRecentTestDetails($club_client_id, $level = NULL){
        $formula = new Model\TestFormulas();
        $cust = $this->getDoctrine()->getEntityManager();
        $connection = $cust->getConnection();
        $order = ($level == NULL || $level == 'first') ? 'ASC' : 'DESC';
        
        $statement = $connection->prepare("SELECT
                    t.*, 
                    c.club_admin_id,
                    c.club_client_id,
                    c.club_id,
                    c.gender,
                    c.birthdate,
                    c.fname,
                    c.lname,
                    c.email,
                    c.phone,
                    c.picture,
                    c.height,
                    c.status,
                    a.fname as coach_fname,
                    a.lname as coach_lname,
                    CONCAT(a.fname, ' ', a.lname) as coach,
                    e.event_name,
                    (CASE WHEN (t.`biometric_chest_lost` < 0) THEN 0 ELSE t.`biometric_chest_lost` END) AS `biometric_chest_lost`,
                    (CASE WHEN (t.`biometric_thigh_lost` < 0) THEN 0 ELSE t.`biometric_thigh_lost` END) AS `biometric_thigh_lost`,
                    (CASE WHEN (t.`biometric_waist_value_lost` < 0) THEN 0 ELSE t.`biometric_waist_value_lost` END) AS `biometric_waist_value_lost`,
                    (CASE WHEN (t.`biometric_arm_lost` < 0) THEN 0 ELSE t.`biometric_arm_lost` END) AS `biometric_arm_lost`
                FROM tbl_client_test t 
                LEFT JOIN tbl_club_client c ON c.club_client_id = t.club_client_id
                LEFT JOIN tbl_club_admin a ON a.club_admin_id = t.club_admin_id
                LEFT JOIN tbl_event e ON e.event_id = t.event_id
                WHERE t.club_client_id = :club_client_id
                    AND t.status = :status
                ORDER BY t.test_datetime_finalised $order
                LIMIT 1");
        $statement->bindValue('club_client_id', $club_client_id);
        $statement->bindValue('status', 'finalised');
        $statement->execute();
        $data =  $statement->fetchAll();
        
//        if(count($data) > 0){
//            $data[0]['physical_activity_crf'] = $formula->crf($data[0]['lifestyle_physical_activity']);
//            $data[0]['smoking_crf'] = $formula->crf($data[0]['lifestyle_smoking']);
//            $data[0]['alcohol_crf'] = $formula->crf($data[0]['lifestyle_alcohol']);
//            $data[0]['nutrition_crf'] = $formula->crf($data[0]['lifestyle_nutrition']);
//            $data[0]['mental_health_crf'] = $formula->crf($data[0]['lifestyle_mental_health']);
//            $data[0]['risk_profile_crf'] = $formula->crf($data[0]['lifestyle_risk_profile']);
//            $data[0]['body_fat_crf'] = $formula->crf($data[0]['biometric_body_fat']);
//            $data[0]['blood_pressure_crf'] = $formula->crf($data[0]['biometric_blood_pressure']);
//            $data[0]['blood_oxygen_crf'] = $formula->crf($data[0]['biometric_blood_oxygen']);
//            $data[0]['vo2_crf'] = $formula->crf($data[0]['physiological_vo2']);
//            $data[0]['balance_crf'] = $formula->crf($data[0]['physiological_balance']);
//            $data[0]['squat_test_crf'] = $formula->crf($data[0]['physiological_squat']);
//            $data[0]['seven_stage_situp_crf'] = $formula->crf($data[0]['physiological_situp']);
//            $data[0]['waist_crf'] = $formula->crf($data[0]['biometric_waist']);
//            
//            $data[0]['total_crf'] = $formula->totalCRF(
//                    $data[0]['physical_activity_crf'], 
//                    $data[0]['smoking_crf'], 
//                    $data[0]['alcohol_crf'], 
//                    $data[0]['nutrition_crf'], 
//                    $data[0]['mental_health_crf'], 
//                    $data[0]['risk_profile_crf'], 
//                    $data[0]['body_fat_crf'], 
//                    $data[0]['blood_pressure_crf'], 
//                    $data[0]['blood_oxygen_crf'], 
//                    $data[0]['vo2_crf'], 
//                    $data[0]['balance_crf'], 
//                    $data[0]['squat_test_crf'], 
//                    $data[0]['seven_stage_situp_crf'], 
//                    $data[0]['waist_crf']
//                );
//            
//            $data[0]['healthy_life_points'] = $formula->healthyLifeYears(
//                    $data[0]['gender'], 
//                    $data[0]['lifestyle_physical_activity'], 
//                    $data[0]['lifestyle_alcohol'], 
//                    $data[0]['lifestyle_nutrition'], 
//                    $data[0]['lifestyle_smoking'], 
//                    $data[0]['lifestyle_mental_health'], 
//                    $data[0]['lifestyle_risk_profile'], 
//                    $data[0]['biometric_body_fat'], 
//                    $data[0]['biometric_blood_pressure'], 
//                    $data[0]['biometric_blood_oxygen'], 
//                    $data[0]['physiological_vo2'], 
//                    $data[0]['physiological_balance'], 
//                    $data[0]['physiological_squat'], 
//                    $data[0]['physiological_situp'], 
//                    $data[0]['biometric_waist'], 
//                    $data[0]['total_crf']
//                );
//            
//            
//            $max_hle = $formula->maxHLE(
//                    $data[0]['gender'], 
//                    $data[0]['healthy_life_expectancy_male'], 
//                    $data[0]['healthy_life_expectancy_female'], 
//                    $data[0]['healthy_life_max_score_male'], 
//                    $data[0]['healthy_life_max_score_female']);
//            
//            $data[0]['estimated_current_healthy_life_age'] = $formula->estimatedCurrentHealthyLifeAge(
//                    $data[0]['age'], 
//                    $data[0]['healthy_life_points'], 
//                    $max_hle
//                );
//        }
        return (count($data) > 0) ? $data[0] : array();
    }
    
    public function getClientTestDetailsById($client_test_id, $club_client_id = NULL){
        $formula = new Model\TestFormulas();
        $mod = new Model\GlobalModel();
        $cust = $this->getDoctrine()->getEntityManager();
        $connection = $cust->getConnection();
        
        $condition = '';
        if($club_client_id != NULL){
            $condition .= " AND (t.club_client_id = :club_client_id)";
        }
        $statement = $connection->prepare("SELECT 
                    t.*, 
                    c.gender,
                    c.birthdate,
                    CONCAT(a.fname, ' ', a.lname) as coach,
                    e.event_name,
                    c.fname,
                    c.lname
                FROM tbl_client_test t 
                LEFT JOIN tbl_club_client c ON c.club_client_id = t.club_client_id
                LEFT JOIN tbl_club_admin a ON a.club_admin_id = t.club_admin_id
                LEFT JOIN tbl_event e ON e.event_id = t.event_id
                WHERE t.client_test_id = :client_test_id " . $condition);
        $statement->bindValue('client_test_id', $client_test_id);
        
        if($club_client_id != NULL){
            $statement->bindValue('club_client_id', $club_client_id);
        }
        $statement->execute();
        $data =  $statement->fetchAll();
        
        if(count($data) > 0){
            $data[0]['lifestyle_physical_activity_result'] = $formula->physicalActivityResult($data[0]['lifestyle_physical_activity']);
            $data[0]['lifestyle_smoking_result'] = $formula->smokingHabitsResult($data[0]['lifestyle_smoking']);
            $data[0]['lifestyle_alcohol_result'] = $formula->alcoholConsumptionResult($data[0]['lifestyle_alcohol']);
            $data[0]['lifestyle_nutrition_result'] = $formula->nutritionResult($data[0]['lifestyle_nutrition']);
            $data[0]['lifestyle_mental_health_result'] = $formula->mentalHealthResult($data[0]['lifestyle_mental_health']);
            $data[0]['lifestyle_risk_profile_result'] = $formula->riskProfileResult($data[0]['lifestyle_risk_profile']);
            $data[0]['physiological_vo2_result'] = $formula->vo2Result($data[0]['physiological_vo2']);
            $data[0]['physiological_balance_result'] = $formula->balanceResult($data[0]['physiological_balance']);
            $data[0]['physiological_squat_result'] = $formula->squatTestResult($data[0]['physiological_squat']);
            $data[0]['physiological_situp_result'] = $formula->sevenStateSitUpTestResult($data[0]['physiological_situp']);
            
            $data[0]['biometric_body_fat_result'] = $formula->bodyFatResult($data[0]['biometric_body_fat']);
            $data[0]['biometric_blood_pressure_result'] = $formula->bloodPressureResult($data[0]['biometric_blood_pressure'], $data[0]['biometric_blood_pressure_systolic'] , $data[0]['biometric_blood_pressure_diastolic']);
            $data[0]['biometric_blood_oxygen_result'] = $formula->bloodOxygenResult($data[0]['biometric_blood_oxygen']);
            $data[0]['biometric_waist_result'] = $formula->waistResult($data[0]['biometric_waist'], $data[0]['biometric_waist_value']);
            
            $data[0]['healthy_life_score_outcome'] = $formula->outcomes($data[0]['healthy_life_score']);
            
            
            $data[0]['physical_activity_crf'] = $formula->crf($data[0]['lifestyle_physical_activity']);
            $data[0]['smoking_crf'] = $formula->crf($data[0]['lifestyle_smoking']);
            $data[0]['alcohol_crf'] = $formula->crf($data[0]['lifestyle_alcohol']);
            $data[0]['nutrition_crf'] = $formula->crf($data[0]['lifestyle_nutrition']);
            $data[0]['mental_health_crf'] = $formula->crf($data[0]['lifestyle_mental_health']);
            $data[0]['risk_profile_crf'] = $formula->crf($data[0]['lifestyle_risk_profile']);
            $data[0]['body_fat_crf'] = $formula->crf($data[0]['biometric_body_fat']);
            $data[0]['blood_pressure_crf'] = $formula->crf($data[0]['biometric_blood_pressure']);
            $data[0]['blood_oxygen_crf'] = $formula->crf($data[0]['biometric_blood_oxygen']);
            $data[0]['vo2_crf'] = $formula->crf($data[0]['physiological_vo2']);
            $data[0]['balance_crf'] = $formula->crf($data[0]['physiological_balance']);
            $data[0]['squat_test_crf'] = $formula->crf($data[0]['physiological_squat']);
            $data[0]['seven_stage_situp_crf'] = $formula->crf($data[0]['physiological_situp']);
            $data[0]['waist_crf'] = $formula->crf($data[0]['biometric_waist']);
        }
        
        return (count($data) > 0) ? $data[0] : '';
    }
    
    
    public function getClientTestHistory($club_client_id, $level = NULL, $status = NULL, $order = NULL, $except_client_test_id = NULL){
        $formula = new Model\TestFormulas();
        $cust = $this->getDoctrine()->getEntityManager();
        $connection = $cust->getConnection();
        
        $limit = ($level == 'recent') ? ' LIMIT 1' : '';
        $order = ($order != NULL) ? $order : 'DESC';
		$except_test = ($except_client_test_id != NULL) ?  ' AND t.client_test_id != :client_test_id ' : '';
        if($status == NULL){
            $statement = $connection->prepare("SELECT 
                        t.*, 
                        c.gender,
                        c.birthdate,
                        CONCAT(a.fname, ' ', a.lname) as coach,
                        e.event_name
                    FROM tbl_client_test t 
                    LEFT JOIN tbl_club_client c ON c.club_client_id = t.club_client_id
                    LEFT JOIN tbl_club_admin a ON a.club_admin_id = t.club_admin_id
                    LEFT JOIN tbl_event e ON e.event_id = t.event_id
                    WHERE t.club_client_id = :club_client_id ". $except_test ."
                    ORDER BY t.test_datetime ". $order ." " .
                    $limit);
            $statement->bindValue('club_client_id', $club_client_id);
        }else{
            $statement = $connection->prepare("SELECT 
                        t.*, 
                        c.gender,
                        c.birthdate,
                        CONCAT(a.fname, ' ', a.lname) as coach,
                        e.event_name
                    FROM tbl_client_test t 
                    LEFT JOIN tbl_club_client c ON c.club_client_id = t.club_client_id
                    LEFT JOIN tbl_club_admin a ON a.club_admin_id = t.club_admin_id
                    LEFT JOIN tbl_event e ON e.event_id = t.event_id
                    WHERE t.club_client_id = :club_client_id
                        AND t.status = :status
						". $except_test ."
                    ORDER BY t.test_datetime ". $order ." " .
                    $limit);
            $statement->bindValue('club_client_id', $club_client_id);
            $statement->bindValue('status', $status);
        }
		if($except_test != ''){
			$statement->bindValue('client_test_id', $except_client_test_id);
		}
        $statement->execute();
        $data = $statement->fetchAll();
        
        for($i=0; $i<count($data); $i++){
            if(count($data) > 0){
                $data[$i]['lifestyle_physical_activity_result'] = $formula->physicalActivityResult($data[$i]['lifestyle_physical_activity']);
                $data[$i]['lifestyle_smoking_result'] = $formula->smokingHabitsResult($data[$i]['lifestyle_smoking']);
                $data[$i]['lifestyle_alcohol_result'] = $formula->alcoholConsumptionResult($data[$i]['lifestyle_alcohol']);
                $data[$i]['lifestyle_nutrition_result'] = $formula->nutritionResult($data[$i]['lifestyle_nutrition']);
                $data[$i]['lifestyle_mental_health_result'] = $formula->mentalHealthResult($data[$i]['lifestyle_mental_health']);
                $data[$i]['lifestyle_risk_profile_result'] = $formula->riskProfileResult($data[$i]['lifestyle_risk_profile']);
                $data[$i]['physiological_vo2_result'] = $formula->vo2Result($data[$i]['physiological_vo2']);
                $data[$i]['physiological_balance_result'] = $formula->balanceResult($data[$i]['physiological_balance']);
                $data[$i]['physiological_squat_result'] = $formula->squatTestResult($data[$i]['physiological_squat']);
                $data[$i]['physiological_situp_result'] = $formula->sevenStateSitUpTestResult($data[$i]['physiological_situp']);

                $data[$i]['biometric_body_fat_result'] = $formula->bodyFatResult($data[$i]['biometric_body_fat']);
                $data[$i]['biometric_blood_pressure_result'] = $formula->bloodPressureResult($data[$i]['biometric_blood_pressure'], $data[$i]['biometric_blood_pressure_systolic'] , $data[$i]['biometric_blood_pressure_diastolic']);
                $data[$i]['biometric_blood_oxygen_result'] = $formula->bloodOxygenResult($data[$i]['biometric_blood_oxygen']);
                $data[$i]['biometric_waist_result'] = $formula->waistResult($data[$i]['biometric_waist'], $data[$i]['biometric_waist_value']);

                $data[$i]['healthy_life_score_outcome'] = $formula->outcomes($data[$i]['healthy_life_score']);


                $data[$i]['physical_activity_crf'] = $formula->crf($data[$i]['lifestyle_physical_activity']);
                $data[$i]['smoking_crf'] = $formula->crf($data[$i]['lifestyle_smoking']);
                $data[$i]['alcohol_crf'] = $formula->crf($data[$i]['lifestyle_alcohol']);
                $data[$i]['nutrition_crf'] = $formula->crf($data[$i]['lifestyle_nutrition']);
                $data[$i]['mental_health_crf'] = $formula->crf($data[$i]['lifestyle_mental_health']);
                $data[$i]['risk_profile_crf'] = $formula->crf($data[$i]['lifestyle_risk_profile']);
                $data[$i]['body_fat_crf'] = $formula->crf($data[$i]['biometric_body_fat']);
                $data[$i]['blood_pressure_crf'] = $formula->crf($data[$i]['biometric_blood_pressure']);
                $data[$i]['blood_oxygen_crf'] = $formula->crf($data[$i]['biometric_blood_oxygen']);
                $data[$i]['vo2_crf'] = $formula->crf($data[$i]['physiological_vo2']);
                $data[$i]['balance_crf'] = $formula->crf($data[$i]['physiological_balance']);
                $data[$i]['squat_test_crf'] = $formula->crf($data[$i]['physiological_squat']);
                $data[$i]['seven_stage_situp_crf'] = $formula->crf($data[$i]['physiological_situp']);
                $data[$i]['waist_crf'] = $formula->crf($data[$i]['biometric_waist']);

            }
        }
        
        return $data;
        
    }
    
    public function getHelpByField($field){
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:Help');
        $query = $cust->createQueryBuilder('h')
            ->where('h.field = :field')
            ->setParameter('field', $field)
            ->getQuery();
        
        $data = $query->getArrayResult();
        
        return (count($data) > 0) ? $data[0] : '';
    }
    
    public function getHealthyLifeExpectancies($gender){
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:HealthyLifeExpectancies');
        $query = $cust->createQueryBuilder('h')
            ->where('h.gender = :gender')
            ->setParameter('gender', $gender)
            ->getQuery();
        
        $data = $query->getArrayResult();
        
        return (count($data) > 0) ? $data[0] : '';
    }
    
    public function getHealthyLifeMaxWorstScore($gender, $type){
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:HealthyLifeMaxWorstScore');
        $query = $cust->createQueryBuilder('h')
            ->where('h.gender = :gender AND h.type = :type')
            ->setParameter('gender', $gender)
            ->setParameter('type', $type)
            ->getQuery();
        
        $data = $query->getArrayResult();
        
        return (count($data) > 0) ? $data[0] : '';
    }
    
    public function getHeadOfficeSettings(){
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:HeadOffice');
        $query = $cust->createQueryBuilder('h')
            ->where('h.id = :id')
            ->setParameter('id', 0)
            ->getQuery();
        
        $data = $query->getArrayResult();
        
        return (count($data) > 0) ? $data[0] : '';
    }
    
    public function setActivity($userId, $userType, $details){
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $em = $this->getDoctrine()->getManager();
        $activity = new Activities();
        $activity->setUserId($userId);
        $activity->setUserType($userType);
        $activity->setDetails($details);
        $activity->setLogDatetime($datetime->format("Y-m-d H:i:s"));
        $em->persist($activity);
        $em->flush();
    }
    
    
    public function paymentAction(){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $error = array();
        
        if($session->get('club_admin_id') == '' && $session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_club_dashboard'));
        }
        
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction(); 
        
        $payment = new Payment();
        $payment->setPaymentDatetime($datetime->format("Y-m-d H:i:s"));
        if($session->get('user_type') == 'club-user'){
            $payment->setUserId($session->get('club_admin_id'));
            $payment->setUserType('club-admin');
        }else{
            $payment->setUserId($session->get('ho_admin_id'));
            $payment->setUserType('ho-admin');
        }
        $payment->setQty($_POST['qty']);
        $payment->setPrice(3.30);
        $amount = (int)$_POST['qty'] * 3.30;
        $payment->setAmount($amount);
        $payment->setStatus('completed');
        $em->persist($payment);
        $em->flush();
        
        

        if($session->get('user_type') == 'club-user'){
            $club = $em->getRepository('AcmeHeadOfficeBundle:Club')
                        ->findOneBy(array(
                            'club_id' => $session->get('club_id'),
                        ));
        }else{
            $club = $em->getRepository('AcmeHeadOfficeBundle:HeadOffice')
                        ->findOneBy(array(
                            'id' => 0,
                        ));
        }
        $available_credit = $club->getAvailableCredit() + (int)$_POST['qty'];
        
        $club->setAvailableCredit($available_credit);
        $em->persist($club);
        $em->flush();
        
        
        //http://www.templemantwells.com.au/article/tools-tips/integrating-pin-payments-using-php
        //Authentication
        $auth['u'] = $this->container->getParameter('pin_payment_api_private_key'); //Private Key from PIN
        $auth['p'] = $this->container->getParameter('pin_payment_api_password'); //API calls use empty password
        
        //Fields to Post
        $post = array();
        $post['amount'] = ($amount * 100);
        $post['currency'] = "AUD";
        $post['description'] = urlencode("Payment for HLP Credits");
        $post['email'] = $session->get('email');
        $post['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $post['card[number]'] = $_POST['card_number'];
        $post['card[expiry_month]'] = $_POST['card_month'];
        $post['card[expiry_year]'] = $_POST['card_year'];
        $post['card[cvc]'] = $_POST['card_ccv'];
        $post['card[name]'] = $_POST['card_name'];
        $post['card[address_line1]'] = $_POST['address'];
        //$post['card[address_line2]'] = '';
        $post['card[address_city]'] = $_POST['city'];
        $post['card[address_postcode]'] = $_POST['postcode'];
        $post['card[address_state]'] = $_POST['state'];
        $post['card[address_country]'] = 'Australia';
        
        // Create a curl handle
        $ch = curl_init( $this->container->getParameter('pin_payment_api_url') );

        curl_setopt($ch, CURLOPT_USERPWD, $auth['u'] . ":" . $auth['p']); //authenticate
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //make sure it returns a response
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // allow https verification if true
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // allow https verification if true
        curl_setopt($ch, CURLOPT_POST, 1); //tell it we are posting
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post); //tell it what to post
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json')); //response comes back as json

        // Execute
        $response = curl_exec($ch);

        // Check if any error occurred
        if(!curl_errno($ch)){$info = curl_getinfo($ch);}

        // Close handle
        curl_close($ch);
        
        //Decode JSON
        $response = json_decode($response, true);
        
        
        
        if(isset($response['error'])){
            
            for($i=0; $i<count($response['messages']); $i++){
                $error[] = array('message'=> $response['messages'][$i]['message']);
            }
            
            
            $this->get('session')->getFlashBag()->add(
                'credit-error',
                $error
            );
            
            if($session->get('user_type') == 'club-user'){
                return $this->redirect($this->generateUrl('acme_club_admin_area'));
            }else{
                return $this->redirect($this->generateUrl('acme_head_office_admin_area'));
            }
        }else{
            
            $session->set('available_credit', $available_credit);
            
            $em->getConnection()->commit(); 

            $this->get('session')->getFlashBag()->add(
                'credit-success',
                $_POST['qty'] . ' credit has been added successfully.'
            );

            $to = $session->get('email');
            $cc = $this->container->getParameter('site_email_address');
            $subject = 'Thank you for your payment';
            $site_address = ($session->get('user_type') == 'club-user') ? $this->getRequest()->getUriForPath('/') : $this->getRequest()->getUriForPath('/admin');
            $body = $this->renderView('AcmeHeadOfficeBundle:Global:email_payment.html.twig',
                        array(
                            'fname'=> $session->get('fname'),
                            'site_address'=> $site_address,
                            'qty'=> $_POST['qty']
                            )
                        );
            
            $this->sendEmail($to, $cc, $this->container->getParameter('site_email_address'), $subject, $body);
            
            // SET ACTIVITY
            if($session->get('user_type') == 'club-user'){
                $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " bought " . $payment->getQty() . " credit(s) for $" . $payment->getAmount() . ".";
                $this->setActivity($session->get('club_admin_id'), 'club-admin', $details);
            }else{
                $details = $session->get('fname') . " " . $session->get('lname') . " of Head Office bought " . $payment->getQty() . " credit(s) for $" . $payment->getAmount() . ".";
                $this->setActivity($session->get('ho_admin_id'), 'ho-admin', $details);
            }

            if($session->get('user_type') == 'club-user'){
                return $this->redirect($this->generateUrl('acme_club_admin_area'));
            }else{
                return $this->redirect($this->generateUrl('acme_head_office_admin_area'));
            }
        }
        
    }
    
    public function phpSendEmail($recipients, $from, $subject, $body){
        $yourWebsite = "Healthy Life Project"; // the name of your website
        
        if (strstr($_SERVER['SERVER_SOFTWARE'], "Win")) {
            $headers = "From: $from";
            $headers.= "Reply-To: <$recipients>";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        } else {
            $headers = "From: $yourWebsite $from\n";
            $headers.= "Reply-To: <$recipients>";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        }


        mail($recipients,$subject,$body,$headers);
    }
    
//    public function sendEmail($recipients, $from, $subject, $body){
//        $message = \Swift_Message::newInstance()
//            ->setEncoder(\Swift_Encoding::get8BitEncoding())
//            ->setSubject($subject)
//            ->setFrom($from,'Healthy Life Project')
//            ->setTo($recipients)
//            ->setBody($body)
//            ->setContentType("text/html")
//        ;
//        $this->get('mailer')->send($message);
//    }
//    
    public function sendEmail($to, $cc, $from, $subject, $body, $attachment = NULL){
            
        if($cc != ''){
                $message = \Swift_Message::newInstance()
                ->setEncoder(\Swift_Encoding::get8BitEncoding())
                ->setSubject($subject)
                ->setFrom($from,'Healthy Life Project')
                ->setTo($to)
                ->setCc($cc)
                ->setBody($body)
                ->setContentType("text/html")
            ;
        }else{

            $message = \Swift_Message::newInstance()
                ->setEncoder(\Swift_Encoding::get8BitEncoding())
                ->setSubject($subject)
                ->setFrom($from,'Healthy Life Project')
                ->setTo($to)
                ->setBody($body)
                ->setContentType("text/html")
            ;
        }
        
        if($attachment != NULL){
            for($i=0; $i<count($attachment); $i++){
                if(!is_dir($attachment[$i])){
                    if(file_exists($attachment[$i])){
                        $message->attach(\Swift_Attachment::fromPath($attachment[$i]));
                    }
                }
            }
        }
        $this->get('mailer')->send($message);
    }
    
    public function errorAction(){
        $session = $this->getRequest()->getSession();
        
        return $this->render('AcmeHeadOfficeBundle:Global:error.html.twig');
    }
    
    public function getHeadOfficeAdminResetToken($email, $token){
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:HeadOfficeAdmin');
        $query = $cust->createQueryBuilder('p')
            ->where('p.email = :email AND p.token = :token')
            ->setParameter('email', $email)
            ->setParameter('token', $token)
            ->getQuery();
        $data = $query->getArrayResult();
        return (count($data) > 0) ? $data[0] : array();
    }
    
    public function getClubAdminResetToken($email, $token){
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:ClubAdmin');
        $query = $cust->createQueryBuilder('p')
            ->where('p.email = :email AND p.token = :token')
            ->setParameter('email', $email)
            ->setParameter('token', $token)
            ->getQuery();
        $data = $query->getArrayResult();
        return (count($data) > 0) ? $data[0] : array();
    }
    
    public function getClubClientResetToken($email, $token){
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:ClubClient');
        $query = $cust->createQueryBuilder('p')
            ->where('p.email = :email AND p.token = :token')
            ->setParameter('email', $email)
            ->setParameter('token', $token)
            ->getQuery();
        $data = $query->getArrayResult();
        return (count($data) > 0) ? $data[0] : array();
    }
    
    public function getClubActivities($club_id = NULL){
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:Activities');
        $query = $cust->createQueryBuilder('p')
            ->where('p.user_type = :user_type')
            ->setParameter('user_type', 'club-admin')
            ->getQuery();
        return $query->getArrayResult();
    }
    
    public function getReports($club_id = NULL, $datrange = NULL, 
            $coach = NULL, $gender = NULL, 
            $event = NULL, $agerange = NULL, $results = NULL){
        
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $cust = $this->getDoctrine()->getEntityManager();
        $connection = $cust->getConnection();
        
        $condition = '';
        $parent_condition = '';
        
        
        
        if(count($datrange) > 0){
            $condition .= " AND (ct.test_datetime_finalised BETWEEN '" . $datrange['from'] . "' AND '" . $datrange['to'] . "')";
        }
        
        if($coach != NULL){
            $condition .= " AND (cc.club_admin_id = :coach)";
        }
        
        if($gender != NULL){
            $condition .= " AND (cc.gender = :gender)";
        }
        
        if($event != NULL){
            $condition .= " AND (ct.event_id = :event_id)";
        }
        
        if($agerange != NULL){
            list($agerange_from, $agerange_to) = explode("-",$agerange);
            $condition .= " AND (ct.age >= :age_from AND ct.age <= :age_to)";
        }
       
        if($results != NULL){
            $condition .= " AND (ROUND(ct.years_added) = :years_added)";
        }
        
        if($club_id != NULL){
            $parent_condition .= " WHERE r.club_id = :club_id GROUP BY r.club_id";
        }
        
        $dateTimeParam = (isset($datrange['to'])) ? $datrange['to'] : $datetime->format("Y-m-d H:i:s");
        $statement = $connection->prepare("SELECT 
                    COUNT(r.club_client_id) as number_of_clients,
                    SUM(r.years_added) AS total_years_added,
                    SUM(r.biometric_chest_lost) AS total_biometric_chest_lost,
                    SUM(r.biometric_thigh_lost) AS total_biometric_thigh_lost,
                    SUM(r.biometric_waist_value_lost) AS total_biometric_waist_value_lost,
                    SUM(r.biometric_arm_lost) AS total_biometric_arm_lost,
                    
                    SUM(r.cm_lost) AS total_cm_lost,
                    SUM(r.kg_lost) AS total_kg_lost,
                    AVG(r.years_added) AS avg_years_added,
                    AVG(r.cm_lost) AS avg_cm_lost,
                    AVG(r.kg_lost) AS avg_kg_lost,
                    AVG(r.healthy_life_score) AS avg_healthy_life_score,
                    AVG(r.estimated_current_healthy_life_age) AS avg_estimated_current_healthy_life_age,
                    AVG(r.you_could_add_up_to) AS avg_you_could_add_up_to
            FROM (
            
                SELECT
                    `cc`.`club_id`                 AS `club_id`,
                    `cc`.`club_admin_id`           AS `coach_id`,
                    `cc`.`club_client_id`          AS `club_client_id`,
                    `cc`.`gender`                  AS `gender`,
                    `ct`.`event_id`                AS `event_id`,
                    `ct`.`age`                     AS `age`,
                    `ct`.`test_datetime_finalised` AS `test_datetime_finalised`,
                    (CASE WHEN (`ct`.`biometric_chest_lost` < 0) THEN 0 ELSE `ct`.`biometric_chest_lost` END) AS `biometric_chest_lost`,
                    (CASE WHEN (`ct`.`biometric_thigh_lost` < 0) THEN 0 ELSE `ct`.`biometric_thigh_lost` END) AS `biometric_thigh_lost`,
                    (CASE WHEN (`ct`.`biometric_waist_value_lost` < 0) THEN 0 ELSE `ct`.`biometric_waist_value_lost` END) AS `biometric_waist_value_lost`,
                    (CASE WHEN (`ct`.`biometric_arm_lost` < 0) THEN 0 ELSE `ct`.`biometric_arm_lost` END) AS `biometric_arm_lost`,

                    (CASE WHEN (`ct`.`years_added` < 0) THEN 0 ELSE `ct`.`years_added` END) AS `years_added`,
                    (CASE WHEN (`ct`.`cm_lost` < 0) THEN 0 ELSE `ct`.`cm_lost` END) AS `cm_lost`,
                    (CASE WHEN (`ct`.`kg_lost` < 0) THEN 0 ELSE `ct`.`kg_lost` END) AS `kg_lost`,
                    (CASE WHEN (`ct`.`healthy_life_score` < 0) THEN 0 ELSE `ct`.`healthy_life_score` END) AS `healthy_life_score`,
                    (CASE WHEN (`ct`.`estimated_current_healthy_life_age` < 0) THEN 0 ELSE `ct`.`estimated_current_healthy_life_age` END) AS `estimated_current_healthy_life_age`,
                    (CASE WHEN (`ct`.`you_could_add_up_to` < 0) THEN 0 ELSE `ct`.`you_could_add_up_to` END) AS `you_could_add_up_to`
                  FROM `tbl_client_test` `ct`
                     LEFT JOIN `tbl_club_client` `cc` ON `cc`.`club_client_id` = `ct`.`club_client_id`
                     
                  WHERE `ct`.`status` = 'finalised'
                         AND `ct`.`test_datetime_finalised` = (SELECT
                                                                    MAX(`ct1`.`test_datetime_finalised`)
                                                                  FROM `tbl_client_test` `ct1`
                                                                  WHERE `ct1`.`club_client_id` = `ct`.`club_client_id`
                                                                         AND `ct1`.`status` = 'finalised'
                                                                         AND `ct1`.`test_datetime_finalised` <= :dateTimeParam)
                        ". $condition ."   
                  GROUP BY `cc`.`club_client_id`
                  ORDER BY `ct`.`test_datetime_finalised` DESC

                  
            ) r 
            " . $parent_condition);
        
        
        $statement->bindValue('dateTimeParam', $dateTimeParam);
        
        if($club_id != NULL){
            $statement->bindValue('club_id', $club_id);
        }
        if($coach != NULL){
            $statement->bindValue('coach', $coach);
        }
        if($gender != NULL){
            $statement->bindValue('gender', $gender);
        }
        if($event != NULL){
            $statement->bindValue('event_id', $event);
        }
        if($agerange != NULL){
            $statement->bindValue('age_from', $agerange_from); 
            $statement->bindValue('age_to', $agerange_to); 
        }
        if($results != NULL){
            $statement->bindValue('years_added', $results); 
        }
        $statement->execute();
        $data = $statement->fetchAll();
        
        return $data;
    }
    
    public function getDetailedReports($club_id = NULL, $datrange = NULL, 
            $coach = NULL, $gender = NULL, 
            $event = NULL, $agerange = NULL, $results = NULL){
        
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $cust = $this->getDoctrine()->getEntityManager();
        $connection = $cust->getConnection();
        
        $condition = '';
        $parent_condition = '';
        
        
        
        if(count($datrange) > 0){
            $condition .= " AND (ct.test_datetime_finalised BETWEEN '" . $datrange['from'] . "' AND '" . $datrange['to'] . "')";
        }
        
        if($coach != NULL){
            $condition .= " AND (cc.club_admin_id = :coach)";
        }
        
        if($gender != NULL){
            $condition .= " AND (cc.gender = :gender)";
        }
        
        if($event != NULL){
            $condition .= " AND (ct.event_id = :event_id)";
        }
        
        if($agerange != NULL){
            list($agerange_from, $agerange_to) = explode("-",$agerange);
            $condition .= " AND (ct.age >= :age_from AND ct.age <= :age_to)";
        }
       
        if($results != NULL){
            $condition .= " AND (ROUND(ct.years_added) = :years_added)";
        }
        
        if($club_id != NULL){
            $condition .= " AND `cc`.`club_id` = :club_id";
        }
        
        $dateTimeParam = (isset($datrange['to'])) ? $datrange['to'] : $datetime->format("Y-m-d H:i:s");
        $statement = $connection->prepare("
                SELECT
                    `cc`.`club_id`                 AS `club_id`,
                    `cc`.`club_admin_id`           AS `coach_id`,
                    `cc`.`club_client_id`          AS `club_client_id`,
                    `cc`.`fname`                   AS `fname`,
                    `cc`.`lname`                   AS `lname`,
                    `cc`.`gender`                  AS `gender`,
                    `ct`.`event_id`                AS `event_id`,
                    `ct`.`age`                     AS `age`,
                    `ct`.`test_datetime_finalised` AS `test_datetime_finalised`,
                    ((SELECT sub_ct.bmi FROM tbl_client_test sub_ct WHERE sub_ct.club_client_id= `cc`.`club_client_id`  AND sub_ct.test_datetime_finalised = (SELECT MIN(sub_ct1.test_datetime_finalised) FROM tbl_client_test sub_ct1 WHERE sub_ct1.club_client_id=`cc`.`club_client_id` )) - `ct`.`bmi` ) AS `bmi_improvement`,
                    ((SELECT sub_ct.biometric_body_fat_value FROM tbl_client_test sub_ct WHERE sub_ct.club_client_id= `cc`.`club_client_id`  AND sub_ct.test_datetime_finalised = (SELECT MIN(sub_ct1.test_datetime_finalised) FROM tbl_client_test sub_ct1 WHERE sub_ct1.club_client_id=`cc`.`club_client_id` )) - `ct`.`biometric_body_fat_value` ) AS `body_fat_reduction`,
                    (`ct`.`physiological_vo2` - (SELECT sub_ct.physiological_vo2 FROM tbl_client_test sub_ct WHERE sub_ct.club_client_id= `cc`.`club_client_id`  AND sub_ct.test_datetime_finalised = (SELECT MIN(sub_ct1.test_datetime_finalised) FROM tbl_client_test sub_ct1 WHERE sub_ct1.club_client_id=`cc`.`club_client_id` ))) AS `vo2_improvement`,
                    (`ct`.`physiological_squat_value` - (SELECT sub_ct.physiological_squat_value FROM tbl_client_test sub_ct WHERE sub_ct.club_client_id= `cc`.`club_client_id`  AND sub_ct.test_datetime_finalised = (SELECT MIN(sub_ct1.test_datetime_finalised) FROM tbl_client_test sub_ct1 WHERE sub_ct1.club_client_id=`cc`.`club_client_id` ))) AS `squat_test_improvement`,
                    (CASE WHEN `ct`.`physiological_balance_type` = 'standing functional reach' THEN (`ct`.`physiological_balance_value` - (SELECT sub_ct.physiological_balance_value FROM tbl_client_test sub_ct WHERE sub_ct.club_client_id= `cc`.`club_client_id`  AND sub_ct.test_datetime_finalised = (SELECT MIN(sub_ct1.test_datetime_finalised) FROM tbl_client_test sub_ct1 WHERE sub_ct1.club_client_id=`cc`.`club_client_id` ))) ELSE 0 END) AS `standing_functional_reach_improvement`,
                    (CASE WHEN `ct`.`physiological_balance_type` = 'stork test' THEN (`ct`.`physiological_balance_value` - (SELECT sub_ct.physiological_balance_value FROM tbl_client_test sub_ct WHERE sub_ct.club_client_id= `cc`.`club_client_id`  AND sub_ct.test_datetime_finalised = (SELECT MIN(sub_ct1.test_datetime_finalised) FROM tbl_client_test sub_ct1 WHERE sub_ct1.club_client_id=`cc`.`club_client_id` ))) ELSE 0 END) AS `stork_test_improvement`,
                    (`ct`.`biometric_abdomen_lost` * -1) AS `abdominal_test_improvement`,
                    ((SELECT sub_lqca.answer FROM tbl_lifestyle_questionnaire_client_answer sub_lqca WHERE sub_lqca.club_client_id=`cc`.`club_client_id` AND sub_lqca.client_test_id=`ct`.`client_test_id` AND sub_lqca.lq_id=1) - (SELECT sub_lqca.answer FROM tbl_lifestyle_questionnaire_client_answer sub_lqca WHERE sub_lqca.club_client_id=`cc`.`club_client_id` AND sub_lqca.client_test_id=(SELECT sub_ct1.client_test_id FROM tbl_client_test sub_ct1 WHERE sub_ct1.test_datetime_finalised = (SELECT MIN(sub_ct2.test_datetime_finalised) FROM tbl_client_test sub_ct2 WHERE sub_ct2.club_client_id=`cc`.`club_client_id` )) AND sub_lqca.lq_id=1)) as vigorous_pa_per_week,
                    ((SELECT sub_lqca.answer FROM tbl_lifestyle_questionnaire_client_answer sub_lqca WHERE sub_lqca.club_client_id=`cc`.`club_client_id` AND sub_lqca.client_test_id=`ct`.`client_test_id` AND sub_lqca.lq_id=2) - (SELECT sub_lqca.answer FROM tbl_lifestyle_questionnaire_client_answer sub_lqca WHERE sub_lqca.club_client_id=`cc`.`club_client_id` AND sub_lqca.client_test_id=(SELECT sub_ct1.client_test_id FROM tbl_client_test sub_ct1 WHERE sub_ct1.test_datetime_finalised = (SELECT MIN(sub_ct2.test_datetime_finalised) FROM tbl_client_test sub_ct2 WHERE sub_ct2.club_client_id=`cc`.`club_client_id` )) AND sub_lqca.lq_id=2)) as strength_pa_per_week,
                    ((SELECT sub_ct.lifestyle_smoking FROM tbl_client_test sub_ct WHERE sub_ct.club_client_id= `cc`.`club_client_id`  AND sub_ct.test_datetime_finalised = (SELECT MIN(sub_ct1.test_datetime_finalised) FROM tbl_client_test sub_ct1 WHERE sub_ct1.club_client_id=`cc`.`club_client_id` )) - `ct`.`lifestyle_smoking`) AS `smoking_improvement`,
                    ((SELECT sub_lqca.answer FROM tbl_lifestyle_questionnaire_client_answer sub_lqca WHERE sub_lqca.club_client_id=`cc`.`club_client_id` AND sub_lqca.client_test_id=(SELECT sub_ct1.client_test_id FROM tbl_client_test sub_ct1 WHERE sub_ct1.test_datetime_finalised = (SELECT MIN(sub_ct2.test_datetime_finalised) FROM tbl_client_test sub_ct2 WHERE sub_ct2.club_client_id=`cc`.`club_client_id` )) AND sub_lqca.lq_id=4) - (SELECT sub_lqca.answer FROM tbl_lifestyle_questionnaire_client_answer sub_lqca WHERE sub_lqca.club_client_id=`cc`.`club_client_id` AND sub_lqca.client_test_id=`ct`.`client_test_id` AND sub_lqca.lq_id=4)) as standard_alcohol_per_week,
                    ((SELECT sub_lqca.answer FROM tbl_lifestyle_questionnaire_client_answer sub_lqca WHERE sub_lqca.club_client_id=`cc`.`club_client_id` AND sub_lqca.client_test_id=(SELECT sub_ct1.client_test_id FROM tbl_client_test sub_ct1 WHERE sub_ct1.test_datetime_finalised = (SELECT MIN(sub_ct2.test_datetime_finalised) FROM tbl_client_test sub_ct2 WHERE sub_ct2.club_client_id=`cc`.`club_client_id` )) AND sub_lqca.lq_id=5) - (SELECT sub_lqca.answer FROM tbl_lifestyle_questionnaire_client_answer sub_lqca WHERE sub_lqca.club_client_id=`cc`.`club_client_id` AND sub_lqca.client_test_id=`ct`.`client_test_id` AND sub_lqca.lq_id=5)) as maximum_alcohol_one_sitting,
                    (`ct`.`lifestyle_nutrition` - (SELECT sub_ct.lifestyle_nutrition FROM tbl_client_test sub_ct WHERE sub_ct.club_client_id= `cc`.`club_client_id`  AND sub_ct.test_datetime_finalised = (SELECT MIN(sub_ct1.test_datetime_finalised) FROM tbl_client_test sub_ct1 WHERE sub_ct1.club_client_id=`cc`.`club_client_id` ))) AS `nutrition_score_improvement`,
                    ((SELECT sub_ct.lifestyle_mental_health FROM tbl_client_test sub_ct WHERE sub_ct.club_client_id= `cc`.`club_client_id`  AND sub_ct.test_datetime_finalised = (SELECT MIN(sub_ct1.test_datetime_finalised) FROM tbl_client_test sub_ct1 WHERE sub_ct1.club_client_id=`cc`.`club_client_id` )) - `ct`.`lifestyle_mental_health` ) AS `mental_health_improvement`,
                    ((SELECT sub_ct.lifestyle_risk_profile FROM tbl_client_test sub_ct WHERE sub_ct.club_client_id= `cc`.`club_client_id`  AND sub_ct.test_datetime_finalised = (SELECT MIN(sub_ct1.test_datetime_finalised) FROM tbl_client_test sub_ct1 WHERE sub_ct1.club_client_id=`cc`.`club_client_id` )) - `ct`.`lifestyle_risk_profile` ) AS `risk_profile_improvement`,
                    
                    (CASE WHEN (`ct`.`biometric_chest_lost` < 0) THEN 0 ELSE `ct`.`biometric_chest_lost` END) AS `biometric_chest_lost`,
                    (CASE WHEN (`ct`.`biometric_thigh_lost` < 0) THEN 0 ELSE `ct`.`biometric_thigh_lost` END) AS `biometric_thigh_lost`,
                    (CASE WHEN (`ct`.`biometric_waist_value_lost` < 0) THEN 0 ELSE `ct`.`biometric_waist_value_lost` END) AS `biometric_waist_value_lost`,
                    (CASE WHEN (`ct`.`biometric_arm_lost` < 0) THEN 0 ELSE `ct`.`biometric_arm_lost` END) AS `biometric_arm_lost`,

                    (CASE WHEN (`ct`.`years_added` < 0) THEN 0 ELSE `ct`.`years_added` END) AS `years_added`,
                    (CASE WHEN (`ct`.`cm_lost` < 0) THEN 0 ELSE `ct`.`cm_lost` END) AS `cm_lost`,
                    (CASE WHEN (`ct`.`kg_lost` < 0) THEN 0 ELSE `ct`.`kg_lost` END) AS `kg_lost`,
                    (CASE WHEN (`ct`.`healthy_life_score` < 0) THEN 0 ELSE `ct`.`healthy_life_score` END) AS `healthy_life_score`,
                    (CASE WHEN (`ct`.`estimated_current_healthy_life_age` < 0) THEN 0 ELSE `ct`.`estimated_current_healthy_life_age` END) AS `estimated_current_healthy_life_age`,
                    (CASE WHEN (`ct`.`you_could_add_up_to` < 0) THEN 0 ELSE `ct`.`you_could_add_up_to` END) AS `you_could_add_up_to`
                  FROM `tbl_client_test` `ct`
                     LEFT JOIN `tbl_club_client` `cc` ON `cc`.`club_client_id` = `ct`.`club_client_id`
                     
                  WHERE `ct`.`status` = 'finalised' 
                         AND `ct`.`test_datetime_finalised` = (SELECT
                                                                    MAX(`ct1`.`test_datetime_finalised`)
                                                                  FROM `tbl_client_test` `ct1`
                                                                  WHERE `ct1`.`club_client_id` = `ct`.`club_client_id`
                                                                         AND `ct1`.`status` = 'finalised'
                                                                         AND `ct1`.`test_datetime_finalised` <= :dateTimeParam)
                        ". $condition ."   
                  GROUP BY `cc`.`club_client_id`
                  ORDER BY `ct`.`test_datetime_finalised` DESC"
        );
        
        
        $statement->bindValue('dateTimeParam', $dateTimeParam);
        
        if($club_id != NULL){
            $statement->bindValue('club_id', $club_id);
        }
        if($coach != NULL){
            $statement->bindValue('coach', $coach);
        }
        if($gender != NULL){
            $statement->bindValue('gender', $gender);
        }
        if($event != NULL){
            $statement->bindValue('event_id', $event);
        }
        if($agerange != NULL){
            $statement->bindValue('age_from', $agerange_from); 
            $statement->bindValue('age_to', $agerange_to); 
        }
        if($results != NULL){
            $statement->bindValue('years_added', $results); 
        }
        $statement->execute();
        $data = $statement->fetchAll();
        
        return $data;
        
    }
    
    public function getEmailQueue(){
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:EmailQueue');
        $query = $cust->createQueryBuilder('p')
            ->where('p.s_sent= :s_sent')
            ->setParameter('s_sent', 0)
            ->setMaxResults(3)
            ->getQuery();
        return $query->getArrayResult();
    }
    
    
    public function get12weekProgramClientByEmail($email){
        $mod = new Model\GlobalModel();
        if($_SERVER['SERVER_NAME'] == 'localhost'){
            $program_host="localhost";
            $program_uname="root";
            $program_pass="";
            $program_database = "healthy_life_project_ohwp";

            $original_host="localhost";
            $original_uname="root";
            $original_pass="";
            $original_database = "healthy_life_project";
        }else{
            $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
            $program_uname="hlproot";
            $program_pass="_jWBqa)67?BJq(j-";
            $program_database = "hlp12weekProgram";

            $original_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
            $original_uname="hlproot";
            $original_pass="_jWBqa)67?BJq(j-";
            $original_database = "healthy_life_project";
        }

        $program_connection=mysqli_connect($program_host,$program_uname,$program_pass, $program_database) or die("Database Connection Failed");
        $original_connection=mysqli_connect($original_host,$original_uname,$original_pass, $original_database) or die("Database Connection Failed");

        if( $mod->isEmailValid($email) ){
            $program_query = mysqli_query($program_connection,
                "SELECT * FROM tbl_client WHERE email='". $email ."'");
            
            return mysqli_fetch_array($program_query, MYSQLI_ASSOC);
            
        }else{
            return array();
        }
        
        
    }
    
    public function getDefaultDayItems(){
        
        $mod = new Model\GlobalModel();
        if($_SERVER['SERVER_NAME'] == 'localhost'){
            $program_host="localhost";
            $program_uname="root";
            $program_pass="";
            $program_database = "healthy_life_project_ohwp";

            $original_host="localhost";
            $original_uname="root";
            $original_pass="";
            $original_database = "healthy_life_project";
        }else{
            $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
            $program_uname="hlproot";
            $program_pass="_jWBqa)67?BJq(j-";
            $program_database = "hlp12weekProgram";

            $original_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
            $original_uname="hlproot";
            $original_pass="_jWBqa)67?BJq(j-";
            $original_database = "healthy_life_project";
        }

        $program_connection=mysqli_connect($program_host,$program_uname,$program_pass, $program_database) or die("Database Connection Failed");
        $original_connection=mysqli_connect($original_host,$original_uname,$original_pass, $original_database) or die("Database Connection Failed");

        
        $program_query = mysqli_query($program_connection,
                "SELECT * FROM tbl_program_default_day_items");
        
        //return mysqli_fetch_all($program_query, MYSQLI_ASSOC);
        $data = [];
        while($row = mysqli_fetch_array($program_query)){
            $data[] = $row;
        }
        
        return $data;
    }
    
    public function getProgramClientDayItems($email, $week_id = NULL, $day_id = NULL, $s_done = NULL, $except_item_type = NULL){
        if($_SERVER['SERVER_NAME'] == 'localhost'){
            $program_host="localhost";
            $program_uname="root";
            $program_pass="";
            $program_database = "healthy_life_project_ohwp";

            $original_host="localhost";
            $original_uname="root";
            $original_pass="";
            $original_database = "healthy_life_project";
        }else{
            $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
            $program_uname="hlproot";
            $program_pass="_jWBqa)67?BJq(j-";
            $program_database = "hlp12weekProgram";

            $original_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
            $original_uname="hlproot";
            $original_pass="_jWBqa)67?BJq(j-";
            $original_database = "healthy_life_project";
        }
        
        $program_connection=mysqli_connect($program_host,$program_uname,$program_pass, $program_database) or die("Database Connection Failed");
        $original_connection=mysqli_connect($original_host,$original_uname,$original_pass, $original_database) or die("Database Connection Failed");
        
        $condition = '';
        if($week_id != NULL){
            $condition .= " AND pw.week_id = ". $week_id;
        }
        if($day_id != NULL){
            $condition .= " AND pd.day_id = ". $day_id;
        }
        if($s_done != NULL){
            $condition .= " AND p.s_done = ". $s_done;
        }
        if($except_item_type != NULL){
                $condition .= " AND pi.type != '". $except_item_type . "'";
        }
        
        $program_query = mysqli_query($program_connection,
                "SELECT * 
            FROM tbl_program_client_items p
            LEFT JOIN tbl_client cc ON cc.client_id = p.client_id
            LEFT JOIN tbl_program_day pd ON pd.day_id = p.day_id
            LEFT JOIN tbl_program_week pw ON pw.`week_id`= pd.`week_id`
			LEFT JOIN tbl_program_item pi ON pi.item_id = p.item_id
            WHERE cc.email = '". $email ."' " . $condition);
        
        $data = [];
        while($row = mysqli_fetch_array($program_query)){
            $data[] = $row;
        }
        
        return $data;
        
    }
    
    public function getProgramWeeksWithDayItemsByEmail($client_email, $current_week = NULL, $inc_nutrition = NULL, $inc_exercise = NULL, $inc_mindfullness= NULL){
        header('Content-Type: text/html; charset=iso-8859-1');
        //http://php.net/manual/en/mysqli.query.php
        if($_SERVER['SERVER_NAME'] == 'localhost'){
            $program_host="localhost";
            $program_uname="root";
            $program_pass="";
            $program_database = "healthy_life_project_ohwp";
        }else{
            $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
            $program_uname="hlproot";
            $program_pass="_jWBqa)67?BJq(j-";
            $program_database = "hlp12weekProgram";
        }
        
        $mysqli = new \Mysqli($program_host, $program_uname, $program_pass, $program_database);

        /* check connection */
        if ($mysqli->connect_errno) {
            printf("Connect failed: %s\n", $mysqli->connect_error);
            exit();
        }

        if($current_week == NULL && $current_week != 0){
            $stmt = $mysqli->prepare("SELECT p.week_id, p.week_name FROM tbl_program_week p");
        }else{ 
//            $previous_week = $current_week - 1;
//            $next_week = $current_week + 1;
//            if($current_week == 1){ 
                $stmt = $mysqli->prepare("SELECT p.week_id, p.week_name FROM tbl_program_week p WHERE p.week_id = ?");
                $stmt->bind_param("s", $current_week);
//            }elseif($current_week > 1){ 
//                $stmt = $mysqli->prepare("SELECT p.week_id, p.week_name FROM tbl_program_week p WHERE p.week_id <= ? OR p.week_id = ?");
//                $stmt->bind_param("ss", $current_week, $next_week);
//            }elseif($current_week == 0){ 
//                $stmt = $mysqli->prepare("SELECT p.week_id, p.week_name FROM tbl_program_week p WHERE p.week_id = ? OR p.week_id = ?");
//                $current_week = 1;
//                $next_week = 2;
//                $stmt->bind_param("ss", $current_week, $next_week);
//            }else{
//                $stmt = $mysqli->prepare("SELECT p.week_id, p.week_name FROM tbl_program_week p");
//
//            }
        }
        
        /* execute query */
        $stmt->execute();
        $stmt->bind_result($week_id, $week_name);
        $stmt->store_result();
        
        $data = array();
        if ($stmt->num_rows >= "1") { //Uses the stored result and counts the rows.
            
          while($stmt->fetch()){ 
              $data[] = array(
                'week_id'=> $week_id,
                'week_name'=> $week_name,
                'days'=> $this->getProgramWeekDays($client_email, $week_id, $inc_nutrition, $inc_exercise, $inc_mindfullness)
                );
          }

        }
        $mysqli->close();
        
//        print_r($data); exit();
        
        return $data;
    }
    
    public function getProgramWeekDays($client_email, $week_id, $inc_nutrition = NULL, $inc_exercise = NULL, $inc_mindfullness= NULL){
        //http://php.net/manual/en/mysqli.query.php
        if($_SERVER['SERVER_NAME'] == 'localhost'){
            $program_host="localhost";
            $program_uname="root";
            $program_pass="";
            $program_database = "healthy_life_project_ohwp";
        }else{
            $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
            $program_uname="hlproot";
            $program_pass="_jWBqa)67?BJq(j-";
            $program_database = "hlp12weekProgram";
        }
        
        $mysqli = new \Mysqli($program_host, $program_uname, $program_pass, $program_database);

        /* check connection */
        if ($mysqli->connect_errno) {
            printf("Connect failed: %s\n", $mysqli->connect_error);
            exit();
        }
        
        $stmt = $mysqli->prepare("SELECT p.day_id, p.week_id, p.day_name FROM tbl_program_day p WHERE p.week_id = ?");
        $stmt->bind_param("s", $week_id);
        
        /* execute query */
        $stmt->execute();
        $stmt->bind_result($day_id, $week_id, $day_name);
        $stmt->store_result();
        
        $data = array();
        if ($stmt->num_rows >= "1") { //Uses the stored result and counts the rows.
            
          while($stmt->fetch()){ 
              $data[] = array(
                'day_id'=> $day_id,
                'week_id'=> $week_id,
                'day_name'=> $day_name,
                'items'=> $this->getProgramWeekDayClientItems($client_email, $day_id, $inc_nutrition, $inc_exercise, $inc_mindfullness)
                );
          }

        }
        $mysqli->close();
        return $data;
    }
    
    public function getProgramWeekDayClientItems($client_email, $day_id, $inc_nutrition = NULL, $inc_exercise = NULL, $inc_mindfullness = NULL){
        
        //http://php.net/manual/en/mysqli.query.php
        if($_SERVER['SERVER_NAME'] == 'localhost'){
            $program_host="localhost";
            $program_uname="root";
            $program_pass="";
            $program_database = "healthy_life_project_ohwp";
        }else{
            $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
            $program_uname="hlproot";
            $program_pass="_jWBqa)67?BJq(j-";
            $program_database = "hlp12weekProgram";
        }
        
        $mysqli = new \Mysqli($program_host, $program_uname, $program_pass, $program_database);
        mysqli_set_charset($mysqli, "utf8");
        /* check connection */
        if ($mysqli->connect_errno) {
            printf("Connect failed: %s\n", $mysqli->connect_error);
            exit();
        }
        
        $add_conditions = '';
        $category = '';
        if($inc_nutrition!= NULL){
            $category.= ' OR pit.category = "nutrition"';
        }
        if($inc_exercise!= NULL){
            $category.= ' OR pit.category = "exercise"';
        }
        if($inc_mindfullness!= NULL){
            $category.= ' OR pit.category = "mindfulness"';
        }
        
        $add_conditions .= ' AND (pit.category = "quote" '. $category .')';
        
        
        $client_email = mysqli_real_escape_string($mysqli,$client_email); 
        $day_id = intval($day_id);
        
        $result = $mysqli->query("SELECT 
                p.client_item_id, p.client_id, p.day_id, p.item_id, p.sorting, p.s_done,
                pi.type, pi.body, pi.link, pi.quote_author, pit.category, pit.bgcolor, pi.is_expand,
                pi.body_home_based, pi.home_based_content
            FROM tbl_program_client_items p
            LEFT JOIN tbl_client c ON c.client_id = p.client_id
            LEFT JOIN tbl_program_item pi ON pi.item_id = p.item_id
            LEFT JOIN tbl_program_item_type pit ON pit.`item_type_code`= pi.`type`
            WHERE c.email = '".$client_email."' AND p.day_id = ".$day_id." " . $add_conditions . "
            ORDER BY p.sorting ASC
            ");
        
        $data = array(); 
        while ($row = $result->fetch_assoc()) {
            $data[] = $row;
        }

        $result->close();
        $mysqli->close();
        
        return $data;
        
    }
}
