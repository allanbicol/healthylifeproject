<?php

namespace Acme\HeadOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Style_Fill;
use PHPExcel_Style_Alignment;

class AIAreportsController extends GlobalController
{
    public function scheduledReportsAction(){
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        $state = $this->getDoctrine()->getEntityManager();
        $connection = $state->getConnection();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Leo karl")
                    ->setLastModifiedBy("Leo karl")
                    ->setTitle("Office 2007 XLSX Test Document")
                    ->setSubject("Office 2007 XLSX Test Document")
                    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Test result file");
        
        $previous_week = strtotime("-1 week +1 day");

        $start_week = strtotime("last sunday midnight",$previous_week);
        $end_week = strtotime("next saturday",$start_week);

        $start_week = date("Y-m-d",$start_week);
        $end_week = date("Y-m-d",$end_week);
        $start_week_datetime = $start_week . " 00:00:00";
        $end_week_datetime = $end_week . " 23:59:59";
        
        // to admins
        $statement = $connection->prepare("SELECT
        `cb`.`club_name`               AS `club_name`,
        CONCAT(`cc`.`fname`, ' ', `cc`.`lname`) AS `client_name`,
        `cc`.`aia_vitality_member_number`       AS `AIA Vitality ID`,
        `ct`.`client_test_id`          AS `client_test_id`,
        `ct`.`test_datetime_finalised` AS `test_datetime_finalised`,
        `ct`.`height`                  AS `height`,
        `ct`.`weight`                  AS `weight`,
        `ct`.`bmi`                     AS `bmi`,
        `ct`.`biometric_waist_value`   as `biometric_waist_value`,
        `ct`.`biometric_body_fat_value`   AS `biometric_body_fat_value`,
        `ct`.`biometric_blood_pressure_systolic` as `biometric_blood_pressure_systolic`,
        `ct`.`biometric_blood_pressure_diastolic` as `biometric_blood_pressure_diastolic`,
        `ct`.`physiological_vo2_score` AS `vo2_result`

        FROM `tbl_client_test` `ct`
         LEFT JOIN `tbl_club_client` `cc` ON `cc`.`club_client_id` = `ct`.`club_client_id`
         LEFT JOIN `tbl_club` `cb` ON `cb`.`club_id` = `cc`.`club_id`

        WHERE `ct`.`status` = 'finalised' 
            AND `ct`.`test_type` = 'aia' 
            AND (`cc`.`s_aia_vitality_member` IS NOT NULL AND `cc`.`s_aia_vitality_member` = 1)
            AND (`cc`.`aia_vitality_member_number` IS NOT NULL AND `cc`.`aia_vitality_member_number` != '')
            AND (`ct`.test_datetime_finalised BETWEEN '".$start_week_datetime."' AND '". $end_week_datetime ."')");
        $statement->execute();
        $rows =  $statement->fetchAll();
        $rows_count = count($rows);
        
        
        // PRINT HEADERS
//        if($rows_count > 0){
//            $col_count = 0;
//            while ($col = current($rows[0])) {
//                $alphabet = range('A', 'Z');
//                // Add some data
//                $objPHPExcel->setActiveSheetIndex(0)
//                            ->setCellValue($alphabet[ $col_count ].'1', key($rows[0]));
//                $col_count += 1;
//                next($rows[0]);
//            }
//        }
        
        
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'club_name')
                    ->setCellValue('B1', 'client_name')
                    ->setCellValue('C1', 'AIA Vitality ID')
                    ->setCellValue('D1', 'client_test_id')
                    ->setCellValue('E1', 'test_datetime_finalised')
                    ->setCellValue('F1', 'height')
                    ->setCellValue('G1', 'weight')
                    ->setCellValue('H1', 'bmi')
                    ->setCellValue('I1', 'biometric_waist_value')
                    ->setCellValue('J1', 'biometric_body_fat_value')
                    ->setCellValue('K1', 'biometric_blood_pressure_systolic')
                    ->setCellValue('L1', 'biometric_blood_pressure_diastolic')
                    ->setCellValue('M1', 'vo2_result');
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')
                ->setWidth(25);
        
        foreach(range('B','M') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        
        $objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                     'rgb' => '70ad47'
                )
            ));
        
        $objPHPExcel->getActiveSheet()->getStyle('C1')->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                     'rgb' => 'ffff00'
                )
            ));
        $objPHPExcel->getActiveSheet()->getStyle('M1')->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                     'rgb' => 'ffff00'
                )
            ));
        
        $objPHPExcel->getActiveSheet()->getStyle('A1:M1')->applyFromArray(
                array(
                'font'  => array(
                    'bold'  => true,
                    'color' => array('rgb' => '0c12e9'),
//                  'size'  => 15,
//                  'name'  => 'Verdana'
                    )
                )
            );
        // PRINT RECORDS
        $row_no = 2;
        for($i=0; $i<$rows_count; $i++){
            $col_count = 0;
            
            while ($col = current($rows[$i])) {
                    $alphabet = range('A', 'Z');
                    $col_name = key($rows[$i]);
                    // Add some data
                    $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue($alphabet[ $col_count ]. $row_no, $rows[$i][$col_name]);
                $col_count += 1;
                next($rows[$i]);
            }
            $row_no += 1;
        }
        
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $row_no, $rows_count)
                    ->setCellValue('C' . $row_no, 'NEW FIELD')
                    ->setCellValue('D' . $row_no, 'For tracking only')
                    ->setCellValue('E' . $row_no, 'Date of assessment');
        
        $objPHPExcel->getActiveSheet()->getStyle('A' . $row_no)->applyFromArray(
            array(
                    'font'  => array(
                        'bold'=> true,
                    ),
                    'alignment'=>array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    )
                )
            );
        
        $row_no += 1;
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $row_no, 'All fields are Mandatory');
        
        $objPHPExcel->getActiveSheet()->getStyle('A' . $row_no)->applyFromArray(
            array(
                    'font'  => array(
                        'italic'=> true,
                    )
                )
            );
        
        $row_no += 1;
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $row_no, 'VO2 will be provided as the fitness test result regardless of type of test conducted (e.g. Step test, Treadmill test etc)');
        
        
        $objPHPExcel->getActiveSheet()->getStyle('A' . $row_no)->applyFromArray(
            array(
                    'font'  => array(
                        'italic'=> true,
                    )
                )
            );
        

        $objPHPExcel->getActiveSheet()->setTitle('Simple');


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        
        
        $filename = $root_dir  . '/temp/ATF_FACapture_'.$datetime->format('dmY');
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

//        header('Content-type: application/vnd.ms-excel');
//        header('Content-Disposition: attachment; filename="file.xls"');
//        $objWriter->save('php://output');
        $objWriter->save($filename.'.xlsx');
      
        $attachments = array();
        $attachments[] = $filename.'.xlsx';
        
        $subject = 'Healthy Life - AIA Vitality Report';
        $body = $this->renderView('AcmeHeadOfficeBundle:AIAreports:email_template.html.twig');
        $receipients = array(
            'rhys.cutifani@gmail.com', 'support@aiavitality.com.au', 'leo@voodoocreative.com.au','john@voodoocreative.com.au'
//            'leokarl2108@gmail.com'
        );
        $this->sendEmail( $receipients 
                , '', 
                $this->container->getParameter('site_email_address'), 
                $subject, 
                $body,
                $attachments);
        
        
        
        return new Response("success");
    }


    public function aiaCongratulationEmailAction(){
        $data = $this->getAIAApprovedMember();
        $aiaApprovedMember[]='allan@voodoocreative.com.au';
        for($i=0;$i < count($data); $i++){
            $aiaApprovedMember[]= $data[$i]['email'];
        }
//        print_r($aiaApprovedMember);
//        exit();
        $subject = 'Congratulations!';
        $body = "<p>If you have received this email it is because your club has qualified to gain accreditation to conduct AIA Vitality Healthy Life Assessments.</p>
                 <p>As someone in your club has completed at least 5 Healthy Life Assessments the AIA Vitality test has now been switched on in their Healthy Life dashboard within your club.</p>
                 <p>Your club is now eligible to conduct AIA Vitality member health assessments on AIA Vitality members when they come into your club. AIA is in the process of a 'soft launch' at the moment & will be heavily promoting Anytime Fitness as a fitness testing location for their 10,000 members as of June!</p>
                 <p>To finalise your accreditation, just follow up with your trainers to log in to their Healthy Life Dashboard at www.healthylifeproject.com.au/web and click on the AIA registration button to submit the form. Simple!</p>
                 <p>To watch the AIA Vitality Test tutorial showing the slight variations to the normal Healthy Life test - Click the link below.</p>
                 <br/>
                 <p><a href='https://youtu.be/TzOKYQMvQwM'>https://youtu.be/TzOKYQMvQwM</a></p>
                 <p><iframe width='500' height='315' src='https://www.youtube.com/embed/eYUIE0-fIu8' frameborder='0' allowfullscreen></iframe></p>";
        $receipients = $aiaApprovedMember;
//            'leo@voodoocreative.com.au'
        //);
        print_r($receipients);
        echo count($receipients);
        exit();
        $this->sendEmail( $receipients 
                , '', 
                $this->container->getParameter('site_email_address'), 
                $subject, 
                $body);
        
        return new Response('Success');
    }
}
