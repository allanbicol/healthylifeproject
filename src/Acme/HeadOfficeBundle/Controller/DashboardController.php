<?php

namespace Acme\HeadOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

class DashboardController extends GlobalController
{
    public function dashboardAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        
        $data = array(
            date("M",strtotime("-3 Months"))=> $this->getReports($session->get('club_id'), array('from'=> date('Y-m-d',strtotime('first day of ' . date("F Y",strtotime("-3 Months")) )) . ' 00:00:00', 'to'=> date('Y-m-d',strtotime('last day of ' . date("F Y",strtotime("-3 Months")) )) . ' 23:59:59')), 
            date("M",strtotime("-2 Months")) => $this->getReports($session->get('club_id'), array('from'=> date('Y-m-d',strtotime('first day of ' . date("F Y",strtotime("-2 Months")) )) . ' 00:00:00', 'to'=> date('Y-m-d',strtotime('last day of ' . date("F Y",strtotime("-2 Months")) )) . ' 23:59:59')), 
            date("M",strtotime("-1 Months")) => $this->getReports($session->get('club_id'), array('from'=> date('Y-m-d',strtotime('first day of ' . date("F Y",strtotime("-1 Months")) )) . ' 00:00:00', 'to'=> date('Y-m-d',strtotime('last day of ' . date("F Y",strtotime("-1 Months")) )) . ' 23:59:59')), 
            date("M") => $this->getReports($session->get('club_id'), array('from'=> date('Y-m-d',strtotime('first day of this month')) . ' 00:00:00', 'to'=> date('Y-m-d',strtotime('last day of this month')) . ' 23:59:59'))
            );
        
        $filter_daterange = array( 
                    'from' => date('Y-m-d',strtotime('first day of ' . date("F Y",strtotime("-3 Months")) )) . ' 00:00:00',
                    'to' => $datetime->format("Y-m-d H:i:s"),
                );
        
        return $this->render('AcmeHeadOfficeBundle:Dashboard:dashboard.html.twig',
                    array(
                        'summary' => $this->getReports(
                                NULL,
                                $filter_daterange
                            ),
                        'data'=> $data
                    )
                );
    }

}
