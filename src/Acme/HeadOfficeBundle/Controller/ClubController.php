<?php

namespace Acme\HeadOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

use Acme\HeadOfficeBundle\Entity\Club;
use Acme\HeadOfficeBundle\Entity\ClubAdmin;

class ClubController extends GlobalController
{
    public function clubAction()
    {
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        
        if(isset($_GET['dtFrDay']) && isset($_GET['dtToDay']) &&
                isset($_GET['dtFrMonth']) && isset($_GET['dtToMonth']) &&
                isset($_GET['dtFrYear']) && isset($_GET['dtToYear'])
                ){
            $filter_daterange = array( 
                    'from' => intval($_GET['dtFrYear']) ."-". sprintf("%02d", intval($_GET['dtFrMonth'])) ."-". sprintf("%02d", intval($_GET['dtFrDay'])) . " 00:00:00",
                    'to' => intval($_GET['dtToYear']) ."-". sprintf("%02d", intval($_GET['dtToMonth'])) ."-". sprintf("%02d", intval($_GET['dtToDay'])) . " 23:59:59",
                );
        }else{
            //$filter_daterange = NULL;
            $filter_daterange = array( 
                    //'from' => date('Y-m-d',strtotime('first day of ' . date("F Y",strtotime("-1 Year")) )) . ' 00:00:00',
                    'from' => date('Y-m-d',strtotime('1 January 2015')) . ' 00:00:00',
                    'to' => $datetime->format("Y-m-d H:i:s"),
                );
        }
        
        $dtRangeFrom = new \DateTime($filter_daterange['from']);
        $dtRangeTo = new \DateTime($filter_daterange['to']);
        
        return $this->render('AcmeHeadOfficeBundle:Club:club.html.twig',
                array('clubs'=> $this->getClubs('',$filter_daterange, 'yes'),
                    'filter_daterange' => array('from'=> 
                            array(
                                'year'=> $dtRangeFrom->format("Y"), 
                                'month'=> $dtRangeFrom->format("m"),
                                'day'=> $dtRangeFrom->format("d")
                                ),
                        'to'=> 
                            array(
                                'year'=> $dtRangeTo->format("Y"), 
                                'month'=> $dtRangeTo->format("m"),
                                'day'=> $dtRangeTo->format("d")
                                )
                        )
                    )
                );
    }

    public function addEditClubAction($slug)
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        
        if(isset($_POST['club_name'])){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            
            $headoffice = $em->getRepository('AcmeHeadOfficeBundle:HeadOffice')
                            ->findOneBy(array(
                                'id' => 0,
                            ));
            
            $headoffice_credits = $headoffice->getAvailableCredit();
            
            //Get Latitude and longitude
            $address1 = $_POST['address'].", ".$_POST['city'].", ". $_POST['state']." Australia ".$_POST['postcode'];
            $address2 = $_POST['city'].", ". $_POST['state']." Australia ".$_POST['postcode'];
            $lat_long = $this->get_lat_long($address1);
            if($lat_long=='error'){
               $lat_long = $this->get_lat_long($address2); 
            }
            $str_lat_long = explode(",",$lat_long);
            //====================
            if(strtolower(trim($slug)) == 'new'){
                $model = new Club();
            }else{
                $model = $em->getRepository('AcmeHeadOfficeBundle:Club')->findOneBy(array('club_id'=>$_POST["club_id"]));
            }
            $model->setClubName($_POST['club_name']);
            $model->setAddress($_POST['address']);
            $model->setCity($_POST['city']);
            $model->setState($_POST['state']);
            $model->setLam($_POST['lam']);
            $model->setPostcode($_POST['postcode']);
            $model->setPhone($_POST['phone']);
            $model->setCountry('Australia');
            $model->setStatus($_POST['status']);
            $model->setLatitude($str_lat_long[0]);
            $model->setLongitude($str_lat_long[1]);
            if(strtolower(trim($slug)) == 'new'){
                $model->setRegisteredDate($datetime->format("Y-m-d H:i:s"));
            }
            
            if(((int)$_POST['available_credit'] > 0) && (($headoffice_credits - (int)$_POST['available_credit']) >= 0)){
                $available_credit = $model->getAvailableCredit() + (int)$_POST['available_credit'];
            }else{
                $available_credit = $model->getAvailableCredit();
            }
            $model->setAvailableCredit($available_credit);
            $em->persist($model);
            $em->flush();
            
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            
            if($error_count == 0){
                
                if(((int)$_POST['available_credit'] > 0) && (($headoffice_credits - (int)$_POST['available_credit']) >= 0)){
                    $headoffice = $em->getRepository('AcmeHeadOfficeBundle:HeadOffice')
                                ->findOneBy(array(
                                    'id' => 0,
                                ));
                    
                    $headoffice_available_credit = $headoffice_credits - (int)$_POST['available_credit'];
        
                    $headoffice->setAvailableCredit($headoffice_available_credit);
                    $em->persist($headoffice);
                    $em->flush();
                    
                    $session->set('available_credit', $headoffice_available_credit);
                    
                    // SET ACTIVITY
                    $details = $session->get('fname') . " " . $session->get('lname') . " of Head Office added " . (int)$_POST['available_credit'] . " credit(s) to " . $model->getClubName() . ".";
                    $this->setActivity($session->get('ho_admin_id'), 'ho-admin', $details);
                }else{
                    $errors = array();
                    
                    if((int)$_POST['available_credit'] < 0){
                        $errors[] = array('message'=> 'Credit must be greater than 0!');
                    }
                    
                    if(($headoffice_credits - (int)$_POST['available_credit']) < 0){
                        $errors[] = array('message'=> 'Not enough credit!');
                    }
                    
                    if(((int)$_POST['available_credit'] < 0) || (($headoffice_credits - (int)$_POST['available_credit']) < 0)){
                        $this->get('session')->getFlashBag()->add(
                                'error',
                                $errors
                            );
                    }
                }
                
                $em->getConnection()->commit(); 
                
                if(strtolower(trim($slug)) == 'new'){
                    $this->get('session')->getFlashBag()->add(
                        'success',
                        $_POST['club_name'] . ' has been added successfully.'
                    );
                    
                    // SET ACTIVITY
                    $details = $session->get('fname') . " " . $session->get('lname') . " of Head Office created " . $_POST['club_name'] . " as a new Club.";
                    $this->setActivity($session->get('ho_admin_id'), 'ho-admin', $details);
                }else{
                    $this->get('session')->getFlashBag()->add(
                        'success',
                        $_POST['club_name'] . ' has been updated successfully.'
                    );
                    
                     // SET ACTIVITY
                    $details = $session->get('fname') . " " . $session->get('lname') . " of Head Office updated the details of " . $_POST['club_name'] . ", a Club.";
                    $this->setActivity($session->get('ho_admin_id'), 'ho-admin', $details);
                }
                
                return $this->redirect($this->generateUrl('acme_head_office_club_add_edit', array('slug' => 'edit')) . "?cid=".$model->getClubId());
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
               
                return $this->render('AcmeHeadOfficeBundle:Club:add_edit_club.html.twig',
                        array('errors'=>$errors,
                            'post'=>$_POST,
                            'states'=> $this->getStates(),
                            'lams'=> $this->getLams()
                        ));
            }
            
        }
        
        if(strtolower(trim($slug)) == 'new'){
            return $this->render('AcmeHeadOfficeBundle:Club:add_edit_club.html.twig',
                    array(
                            'states'=> $this->getStates(),
                            'lams'=> $this->getLams()
                        )
                    );
        }else{
            $_GET['cid'] = intval($_GET['cid']);
            return $this->render('AcmeHeadOfficeBundle:Club:add_edit_club.html.twig',
                    array(
                            'states'=> $this->getStates(),
                            'lams'=> $this->getLams(),
                            'post'=> $this->getClubById($_GET['cid'])
                        )
                    );
        }
    }
    
    
    public function clubAdminAction()
    {
        $session = $this->getRequest()->getSession();
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        
        if(!isset($_GET['cid'])){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        
        $_GET['cid'] = intval($_GET['cid']);
        return $this->render('AcmeHeadOfficeBundle:Club:club_admin.html.twig',
                array('club_admins'=> $this->getClubAdminsByClubId($_GET['cid']),
                    'get' => $_GET)
                );
    }
    
    
    public function addEditClubAdminAction($slug)
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        
        if(isset($_POST['email'])){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            
            
            if(strtolower(trim($slug)) == 'new'){
                $_POST['club_id'] = intval($_POST['club_id']);
                $model = new ClubAdmin();
                $model->setClubId($_POST['club_id']);
            }else{
                $model = $em->getRepository('AcmeHeadOfficeBundle:ClubAdmin')->findOneBy(array('club_admin_id'=>$_POST["club_admin_id"]));
            }
            
            $model->setFname($_POST['fname']);
            $model->setLname($_POST['lname']);
            if(strtolower(trim($slug)) == 'new'){
                $model->setRegisteredDate($datetime->format("Y-m-d H:i:s"));
                $model->setPassword($mod->passGenerator($_POST['password']));
            }elseif(strtolower(trim($slug)) == 'edit' && $_POST['password'] != ''){
                $model->setPassword($mod->passGenerator($_POST['password']));
            }
            $model->setEmail($_POST['email']);
            $model->setStatus($_POST['status']);
            $model->setRole($_POST['role']);
            $em->persist($model);
            $em->flush();
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            if($error_count == 0){
                if($mod->isEmailValid($_POST['email']) == false || ($_POST['password'] != $_POST['confirm_password'])){
                    $errors = array();
                }
                
                if(!$mod->isEmailValid($_POST['email'])){
                    $errors[] = array('message'=>$_POST['email'].' is not a valid email.');
                    $error_count += 1;
                }

                if($_POST['password'] != $_POST['confirm_password']){
                    $errors[] = array('message'=>'Password must be repeated exactly.');
                    $error_count += 1;
                }
            }
            
            if($error_count == 0){
                
                $em->getConnection()->commit(); 
                
                if(strtolower(trim($slug)) == 'new'){
                    $this->get('session')->getFlashBag()->add(
                        'success',
                        $_POST['fname'] . ' ' . $_POST['lname'] . ' has been added successfully.'
                    );
                }else{
                    $this->get('session')->getFlashBag()->add(
                        'success',
                        $_POST['fname'] . ' ' . $_POST['lname'] . ' has been updated successfully.'
                    );
                }
                return $this->redirect($this->generateUrl('acme_head_office_club_admin_add_edit', array('slug' => 'edit')) . "?caid=".$model->getClubAdminId());
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
               
                return $this->render('AcmeHeadOfficeBundle:Club:add_edit_club_admin.html.twig',
                        array('errors'=>$errors,
                            'post'=>$_POST,
                            'get'=>$_GET
                        ));
            }
        }
        
        if(strtolower(trim($slug)) == 'new'){
            return $this->render('AcmeHeadOfficeBundle:Club:add_edit_club_admin.html.twig',
                    array('get'=>$_GET)
                    );
        }else{
            $_GET['caid'] = intval($_GET['caid']);
            return $this->render('AcmeHeadOfficeBundle:Club:add_edit_club_admin.html.twig',
                    array(
                        'post'=> $this->getClubAdminUserById($_GET['caid']),
                        'get'=>$_GET
                    ));
        }
    }
    
    public function updateClubLatLongAction(){
        
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:Club');
            $query = $cust->createQueryBuilder('p')
//                ->where('p.status = :status')
//                ->setParameter('status', 'active')
                ->getQuery();
        $result = $query->getArrayResult();
        
       
        for($i=0; $i<count($result); $i++){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            //Get Latitude and longitude
            $address1 = $result[$i]['address'].", ".$result[$i]['city'].", ". $result[$i]['state']." Australia ".$result[$i]['postcode'];
            $address2 = $result[$i]['postcode'];
            $lat_long = $this->get_lat_long($address1);
            if($lat_long=='error'){
               $lat_long = $this->get_lat_long($address2); 
            }
            $str_lat_long = explode(",",$lat_long);
            echo $result[$i]["club_id"].'-'.$str_lat_long[0].''.$str_lat_long[1].'<br/>';
            //====================
            if($str_lat_long){
            $model = $em->getRepository('AcmeHeadOfficeBundle:Club')->findOneBy(array('club_id'=>$result[$i]["club_id"]));

            $model->setLatitude($str_lat_long[0]);
            $model->setLongitude($str_lat_long[1]);
            $em->persist($model);
            $em->flush();
            $em->getConnection()->commit(); 
            }
        }
        
        return new response('success');
    }
    
}
