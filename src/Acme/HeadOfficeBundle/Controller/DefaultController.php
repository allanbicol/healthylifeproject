<?php

namespace Acme\HeadOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('AcmeHeadOfficeBundle:Default:index.html.twig', array('name' => $name));
    }
}
