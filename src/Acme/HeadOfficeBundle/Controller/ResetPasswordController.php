<?php

namespace Acme\HeadOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

class ResetPasswordController extends GlobalController
{
    public function resetAction()
    {   
        
        
        $mod = new Model\GlobalModel();
        if(isset($_POST['email'])){
            $error = array();
            $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:HeadOfficeAdmin');
            $query = $cust->createQueryBuilder('p')
                ->where('p.email = :email')
                ->setParameter('email', $_POST['email'])
                ->getQuery();
            $data = $query->getArrayResult();
            
            if(count($data) > 0){
                
                $pincode = rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
                
                $em = $this->getDoctrine()->getManager();
                $model = $em->getRepository('AcmeHeadOfficeBundle:HeadOfficeAdmin')->findOneBy(array('email'=>$_POST['email']));
                $model->setToken($pincode);
                $em->persist($model);
                $em->flush();
                
                $from = $this->container->getParameter('site_email_address');
                $subject = 'Healthy Life Project Password Reset';
                $body = $this->renderView('AcmeHeadOfficeBundle:ResetPassword:email_reset_link.html.twig',
                        array(
                            'token'=> $pincode,
                            'email'=> $_POST['email'],
                            'data'=> $data[0],
                            'request_form'=>$this->getRequest()->getUriForPath('/admin/reset-form'),
                            'request_undo'=>$this->getRequest()->getUriForPath('/admin/reset-undo-request')
                            )
                        );
                
                $this->sendEmail($_POST['email'], '', $from, $subject, $body);
                
                $this->get('session')->getFlashBag()->add(
                            'success',
                            'Help is on the way.'
                        );
                return $this->redirect($this->generateUrl('acme_reset_password'));
//                return $this->render('AcmeHeadOfficeBundle:ResetPassword:reset.html.twig',
//                        array('result'=> 'success')
//                        );
            }else{
                $error[] = array( 'message' =>"There wasn't an account for that email");
                $this->get('session')->getFlashBag()->add(
                            'error',
                            $error
                        );
                return $this->render('AcmeHeadOfficeBundle:ResetPassword:reset.html.twig');
            }
        }else{
            
            return $this->render('AcmeHeadOfficeBundle:ResetPassword:reset.html.twig');
        }
        
    }
    
    public function resetFormAction(){
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        if($session->get('email') != ''){
            return $this->redirect($this->generateUrl('acme_head_office_dashboard'));
        }
        
        if(isset($_POST['email']) && 
                isset($_POST['token']) && 
                isset($_POST['password']) && 
                isset($_POST['confirm_password']) && 
                trim($_POST['password']) != ''){
            
            if($_POST['password'] == $_POST['confirm_password']){
                
                $em = $this->getDoctrine()->getManager();
                $model = $em->getRepository('AcmeHeadOfficeBundle:HeadOfficeAdmin')->findOneBy(array('email'=>$_POST['email'], 'token'=> $_POST['token']));
                $model->setToken('');
                $model->setPassword( $mod->passGenerator( $_POST['password']) );
                $em->persist($model);
                $em->flush();
                
                return $this->redirect($this->generateUrl('acme_reset_password_success'));
                
            }else{
                
                $error[] = array( 'message' =>"Passwords do not match.");
                $this->get('session')->getFlashBag()->add(
                            'error',
                            $error
                        );
                
                return $this->render('AcmeHeadOfficeBundle:ResetPassword:reset_form.html.twig',
                        array('email'=>$_POST['email'],'token'=>$_POST['token'])
                        );
            }
            
        }else{
            
            
            $counterrors = 0;
        
            $counterrors += (!isset($_GET['em'])) ? 1 : 0;
            $counterrors += (!isset($_GET['rp'])) ? 1 : 0;

            $admin = ($counterrors == 0) ? $this->getHeadOfficeAdminResetToken($_GET['em'], $_GET['rp']) : array();

            if(count($admin) > 0 && $counterrors == 0){
                return $this->render('AcmeHeadOfficeBundle:ResetPassword:reset_form.html.twig',
                        array('email'=>$_GET['em'],'token'=>$_GET['rp'])
                        );
            }else{
                return $this->render('AcmeHeadOfficeBundle:ResetPassword:reset_error.html.twig');
            }
        }
        
    }
    
    public function resetSuccessAction(){
        return $this->render('AcmeHeadOfficeBundle:ResetPassword:reset_success.html.twig');
    }
    
    
    public function undoResetPasswordRequestAction(){
        if(isset($_GET['em']) && isset($_GET['rp'])){
            $em = $this->getDoctrine()->getManager();
            $model = $em->getRepository('AcmeHeadOfficeBundle:HeadOfficeAdmin')->findOneBy(array('email'=>$_GET['em'], 'token'=> $_GET['rp']));
            if(count($model) > 0){
                $model->setToken('');
                $em->persist($model);
                $em->flush();
            }
        }
        
        return $this->redirect($this->generateUrl('acme_head_office_dashboard'));
            
    }

}
