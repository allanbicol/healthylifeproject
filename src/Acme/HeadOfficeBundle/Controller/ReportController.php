<?php

namespace Acme\HeadOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

class ReportController extends GlobalController
{
    
    public function reportAction()
    {
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $mod = new Model\GlobalModel();
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        $host = $mod->siteURL();
        require_once($root_dir.'/resources/pdfc/html2pdf.class.php');
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        
        
        if(isset($_GET['dtFrDay']) && isset($_GET['dtToDay']) &&
                isset($_GET['dtFrMonth']) && isset($_GET['dtToMonth']) &&
                isset($_GET['dtFrYear']) && isset($_GET['dtToYear'])
                ){
            $filter_daterange = array( 
                    'from' => intval($_GET['dtFrYear']) ."-". sprintf("%02d", intval($_GET['dtFrMonth'])) ."-". sprintf("%02d", intval($_GET['dtFrDay'])) . " 00:00:00",
                    'to' => intval($_GET['dtToYear']) ."-". sprintf("%02d", intval($_GET['dtToMonth'])) ."-". sprintf("%02d", intval($_GET['dtToDay'])) . " 23:59:59",
                );
        }else{
            //$filter_daterange = NULL;
            $filter_daterange = array( 
                    'from' => date('Y-m-d',strtotime('first day of ' . date("F Y",strtotime("-3 Months")) )) . ' 00:00:00',
                    'to' => $datetime->format("Y-m-d H:i:s"),
                );
        }
        
        $filter_club = (isset($_GET['sAddClub']) && isset($_GET['fClub']) && $_GET['fClub'] != '') ? intval($_GET['fClub']) : NULL;
        $filter_coach = (isset($_GET['sAddCoach']) && isset($_GET['fCoach']) && $_GET['fCoach'] != '') ? intval($_GET['fCoach']) : NULL;
        $filter_gender = (isset($_GET['sAddGender']) && isset($_GET['fGender']) && $_GET['fGender'] != '') ? $_GET['fGender'] : NULL;
        $filter_event = (isset($_GET['sAddEvent']) && isset($_GET['fEvent']) && $_GET['fEvent'] != '') ? intval($_GET['fEvent']) : NULL;
//        $filter_agerange = (isset($_GET['sAddAgeRange']) && isset($_GET['fAgeRange']) && $_GET['fAgeRange'] != '') ? $_GET['fAgeRange'] : NULL;
        
        $filter_age_fr = (isset($_GET['fAgeRangeFr']) && $_GET['fAgeRangeFr'] != '') ? $_GET['fAgeRangeFr'] : NULL;
        $filter_age_to = (isset($_GET['fAgeRangeTo']) && $_GET['fAgeRangeTo'] != '') ? $_GET['fAgeRangeTo'] : NULL;
        if(isset($_GET['sAddAgeRange'])){
            if( $filter_age_fr != NULL && $filter_age_to != NULL){
                $filter_age_fr = filter_var($filter_age_fr, FILTER_SANITIZE_NUMBER_INT);
                $filter_age_to = filter_var($filter_age_to, FILTER_SANITIZE_NUMBER_INT);
                $filter_agerange = $filter_age_fr . '-' . $filter_age_to;
            }elseif( $filter_age_fr != NULL && $filter_age_to == NULL){
                $filter_age_fr = filter_var($filter_age_fr, FILTER_SANITIZE_NUMBER_INT);
                $filter_agerange = $filter_age_fr . '-100';
            }elseif( $filter_age_fr == NULL && $filter_age_to != NULL){
                $filter_age_to = filter_var($filter_age_to, FILTER_SANITIZE_NUMBER_INT);
                $filter_agerange = '16-'.$filter_age_to;
            }else{
                $filter_agerange = NULL;
            }
        }else{
            $filter_agerange = NULL;
        }
        
        
        $filter_results = (isset($_GET['sAddResults']) && isset($_GET['fResults']) && $_GET['fResults'] != '') ? intval($_GET['fResults']) : NULL;
        
        $data = array();
        $dtRangeFrom = new \DateTime($filter_daterange['from']);
        $dtRangeTo = new \DateTime($filter_daterange['to']);

        // if the same year
        if($dtRangeFrom->format("Y") == $dtRangeTo->format("Y")){
            // if the same month
            if($dtRangeFrom->format("m") == $dtRangeTo->format("m")){

                $start = intval($dtRangeFrom->format("d"));
                $end = intval($dtRangeTo->format("d"));

                for($i=$start; $i<=$end; $i++){
                    $day= sprintf("%02d", $i);
                    $dt = \DateTime::createFromFormat('!m', $dtRangeFrom->format("m"));
                    $data[$dt->format('M') . " " . $i] = $this->getReports(
                            $filter_club, 
                            array('from'=> $dtRangeFrom->format("Y") .'-'.$dtRangeFrom->format("m").'-'.$day. ' 00:00:00', 
                                    'to'=> $dtRangeTo->format("Y") .'-'.$dtRangeFrom->format("m").'-'.$day. ' 23:59:59'
                                ),
                            $filter_coach,
                            $filter_gender,
                            $filter_event,
                            $filter_agerange,
                            $filter_results
                            );
                }


            // if month differs
            }else{

                // only if month (from) < month (to)
                if(intval($dtRangeFrom->format("m")) < intval($dtRangeTo->format("m"))){
                    $start = intval($dtRangeFrom->format("m"));
                    $end = intval($dtRangeTo->format("m"));

                    for($i=$start; $i<=$end; $i++){
                        $month= sprintf("%02d", $i);
                        $dt = \DateTime::createFromFormat('!m', $i);

                        $day_fr = ($i == $start) ? intval($dtRangeFrom->format("d")) : 1;
                        $day_fr= sprintf("%02d", $day_fr);
                        $day_to = ($i == $end) ? intval($dtRangeTo->format("d")) : 31;
                        $day_to= sprintf("%02d", $day_to);

                        $data[$dt->format('M')] = $this->getReports(
                                $filter_club, 
                                array('from'=> $dtRangeFrom->format("Y") .'-'.$month.'-'.$day_fr. ' 00:00:00', 
                                    'to'=> $dtRangeTo->format("Y") .'-'.$month.'-'.$day_to. ' 23:59:59'
                                ),
                                $filter_coach,
                                $filter_gender,
                                $filter_event,
                                $filter_agerange,
                                $filter_results
                                );
                    }
                }
            }

        // if year differs
        }else{
            // only if year (from) < year (to)
            if(intval($dtRangeFrom->format("Y")) < intval($dtRangeTo->format("Y"))){
                $start = intval($dtRangeFrom->format("Y"));
                $end = intval($dtRangeTo->format("Y"));

                for($i=$start; $i<=$end; $i++){
                    $month_fr = ($i == $start) ? intval($dtRangeFrom->format("m")) : 1;
                    $month_fr= sprintf("%02d", $month_fr);
                    $month_to = ($i == $end) ? intval($dtRangeTo->format("m")) : 12;
                    $month_to= sprintf("%02d", $month_to);

                    $day_fr = ($i == $start) ? intval($dtRangeFrom->format("d")) : 1;
                    $day_fr= sprintf("%02d", $day_fr);
                    $day_to = ($i == $end) ? intval($dtRangeTo->format("d")) : 31;
                    $day_to= sprintf("%02d", $day_to);

                    $data[$i] = $this->getReports(
                            $filter_club, 
                            array('from'=> $i .'-'.$month_fr.'-'.$day_fr. ' 00:00:00', 
                                'to'=> $i .'-'.$month_to.'-'.$day_to. ' 23:59:59'
                            ),
                            $filter_coach,
                            $filter_gender,
                            $filter_event,
                            $filter_agerange,
                            $filter_results
                            );
                }
            }
        }
        
        if(!isset($_GET['download']) && !isset($_GET['print'])){
            $request_params = implode('&', array_map(function ($v, $k) { return sprintf("%s=%s", $k, $v); }, $_GET, array_keys($_GET)));
            return $this->render('AcmeHeadOfficeBundle:Report:report.html.twig',
                    array(
                        'clubs'=> $this->getClubs('active'),
                        'events'=> $this->getEvents(),
                        'request_params' => $request_params,
                        'summary'=> $this->getReports(
                                $filter_club, 
                                $filter_daterange,
                                $filter_coach,
                                $filter_gender,
                                $filter_event,
                                $filter_agerange,
                                $filter_results
                            ),
                        'report_data' => $data,
                        'filter_daterange' => array('from'=> 
                                                    array(
                                                        'year'=> $dtRangeFrom->format("Y"), 
                                                        'month'=> $dtRangeFrom->format("m"),
                                                        'day'=> $dtRangeFrom->format("d")
                                                        ),
                                                'to'=> 
                                                    array(
                                                        'year'=> $dtRangeTo->format("Y"), 
                                                        'month'=> $dtRangeTo->format("m"),
                                                        'day'=> $dtRangeTo->format("d")
                                                        )
                        )
                    )
                );
        }else{
            // PDF
            
            
            $content =  $this->renderView('AcmeHeadOfficeBundle:Report:report_pdf.html.twig',
                    array(
                        'site_url' => $root_dir,
                        'host'=> $host,
                        'filter_daterange'=>$filter_daterange,
                        'filter_club'=> $this->getClubById($filter_club),
                        'filter_gender'=> $filter_gender,
                        'filter_event'=> $this->getEventById($filter_event),
                        'filter_agerange'=> $filter_agerange,
                        'filter_results'=> $filter_results,
                        'summary'=> $this->getReports(
                                $filter_club, 
                                $filter_daterange,
                                $filter_coach,
                                $filter_gender,
                                $filter_event,
                                $filter_agerange,
                                $filter_results
                            )
                    )
                );
            
            $html = '<page>';
            $html .= $content . '';
            $html .='</page>';
            
            $html2pdf = new \HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(20, 20, 20, 20));
            $stylesheet = file_get_contents($root_dir.'/css/report-pdf.css'); /// here call you external css file 
            $stylesheet = '<style>'.$stylesheet.'</style>';
            $html2pdf->WriteHTML($stylesheet . $html);
            //$html2pdf->WriteHTML($html);

            $type = (isset($_GET['download'])) ? 'D' : '';

            $html2pdf->Output("reports ". $datetime->format("d-m-Y") .".pdf", $type);

            $response = new Response("success");
            $response->setContent('success');
            
            // the headers public attribute is a ResponseHeaderBag
            $response->headers->set('Content-Type', 'application/pdf');
            return $response;
        }
    }
    
    
    public function detailedReportAction(){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $mod = new Model\GlobalModel();
        $host = $mod->siteURL();
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        require_once($root_dir.'/resources/dompdf/dompdf_config.inc.php');
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        if(isset($_GET['dtFrDay']) && isset($_GET['dtToDay']) &&
                isset($_GET['dtFrMonth']) && isset($_GET['dtToMonth']) &&
                isset($_GET['dtFrYear']) && isset($_GET['dtToYear'])
                ){
            $filter_daterange = array( 
                    'from' => intval($_GET['dtFrYear']) ."-". sprintf("%02d", intval($_GET['dtFrMonth'])) ."-". sprintf("%02d", intval($_GET['dtFrDay'])) . " 00:00:00",
                    'to' => intval($_GET['dtToYear']) ."-". sprintf("%02d", intval($_GET['dtToMonth'])) ."-". sprintf("%02d", intval($_GET['dtToDay'])) . " 23:59:59",
                );
        }else{
            //$filter_daterange = NULL;
            $filter_daterange = array( 
                    'from' => date('Y-m-d',strtotime('first day of ' . date("F Y",strtotime("-3 Months")) )) . ' 00:00:00',
                    'to' => $datetime->format("Y-m-d H:i:s"),
                );
        }
        
        $filter_club = (isset($_GET['sAddClub']) && isset($_GET['fClub']) && $_GET['fClub'] != '') ? intval($_GET['fClub']) : NULL;
        $filter_coach = (isset($_GET['sAddCoach']) && isset($_GET['fCoach']) && $_GET['fCoach'] != '') ? intval($_GET['fCoach']) : NULL;
        $filter_gender = (isset($_GET['sAddGender']) && isset($_GET['fGender']) && $_GET['fGender'] != '') ? $_GET['fGender'] : NULL;
        $filter_event = (isset($_GET['sAddEvent']) && isset($_GET['fEvent']) && $_GET['fEvent'] != '') ? intval($_GET['fEvent']) : NULL;
//        $filter_agerange = (isset($_GET['sAddAgeRange']) && isset($_GET['fAgeRange']) && $_GET['fAgeRange'] != '') ? $_GET['fAgeRange'] : NULL;
        $filter_age_fr = (isset($_GET['fAgeRangeFr']) && $_GET['fAgeRangeFr'] != '') ? $_GET['fAgeRangeFr'] : NULL;
        $filter_age_to = (isset($_GET['fAgeRangeTo']) && $_GET['fAgeRangeTo'] != '') ? $_GET['fAgeRangeTo'] : NULL;
        if(isset($_GET['sAddAgeRange'])){
            if( $filter_age_fr != NULL && $filter_age_to != NULL){
                $filter_age_fr = filter_var($filter_age_fr, FILTER_SANITIZE_NUMBER_INT);
                $filter_age_to = filter_var($filter_age_to, FILTER_SANITIZE_NUMBER_INT);
                $filter_agerange = $filter_age_fr . '-' . $filter_age_to;
            }elseif( $filter_age_fr != NULL && $filter_age_to == NULL){
                $filter_age_fr = filter_var($filter_age_fr, FILTER_SANITIZE_NUMBER_INT);
                $filter_agerange = $filter_age_fr . '-100';
            }elseif( $filter_age_fr == NULL && $filter_age_to != NULL){
                $filter_age_to = filter_var($filter_age_to, FILTER_SANITIZE_NUMBER_INT);
                $filter_agerange = '16-'.$filter_age_to;
            }else{
                $filter_agerange = NULL;
            }
        }else{
            $filter_agerange = NULL;
        }
        
        $filter_results = (isset($_GET['sAddResults']) && isset($_GET['fResults']) && $_GET['fResults'] != '') ? intval($_GET['fResults']) : NULL;
        
        $data = array();
        $dtRangeFrom = new \DateTime($filter_daterange['from']);
        $dtRangeTo = new \DateTime($filter_daterange['to']);
        
        // if the same year
        if($dtRangeFrom->format("Y") == $dtRangeTo->format("Y")){
            // if the same month
            if($dtRangeFrom->format("m") == $dtRangeTo->format("m")){

                $start = intval($dtRangeFrom->format("d"));
                $end = intval($dtRangeTo->format("d"));

                for($i=$start; $i<=$end; $i++){
                    $day= sprintf("%02d", $i);
                    $dt = \DateTime::createFromFormat('!m', $dtRangeFrom->format("m"));
//                    $data[$dt->format('M') . " " . $i] = $this->getReports(
//                            $filter_club, 
//                            array('from'=> $dtRangeFrom->format("Y") .'-'.$dtRangeFrom->format("m").'-'.$day. ' 00:00:00', 
//                                    'to'=> $dtRangeTo->format("Y") .'-'.$dtRangeFrom->format("m").'-'.$day. ' 23:59:59'
//                                ),
//                            $filter_coach,
//                            $filter_gender,
//                            $filter_event,
//                            $filter_agerange,
//                            $filter_results
//                            );
                }


            // if month differs
            }else{

                // only if month (from) < month (to)
                if(intval($dtRangeFrom->format("m")) < intval($dtRangeTo->format("m"))){
                    $start = intval($dtRangeFrom->format("m"));
                    $end = intval($dtRangeTo->format("m"));

                    for($i=$start; $i<=$end; $i++){
                        $month= sprintf("%02d", $i);
                        $dt = \DateTime::createFromFormat('!m', $i);

                        $day_fr = ($i == $start) ? intval($dtRangeFrom->format("d")) : 1;
                        $day_fr= sprintf("%02d", $day_fr);
                        $day_to = ($i == $end) ? intval($dtRangeTo->format("d")) : 31;
                        $day_to= sprintf("%02d", $day_to);

//                        $data[$dt->format('M')] = $this->getReports(
//                                $filter_club, 
//                                array('from'=> $dtRangeFrom->format("Y") .'-'.$month.'-'.$day_fr. ' 00:00:00', 
//                                    'to'=> $dtRangeTo->format("Y") .'-'.$month.'-'.$day_to. ' 23:59:59'
//                                ),
//                                $filter_coach,
//                                $filter_gender,
//                                $filter_event,
//                                $filter_agerange,
//                                $filter_results
//                                );
                    }
                }
            }

        // if year differs
        }else{
            // only if year (from) < year (to)
            if(intval($dtRangeFrom->format("Y")) < intval($dtRangeTo->format("Y"))){
                $start = intval($dtRangeFrom->format("Y"));
                $end = intval($dtRangeTo->format("Y"));

                for($i=$start; $i<=$end; $i++){
                    $month_fr = ($i == $start) ? intval($dtRangeFrom->format("m")) : 1;
                    $month_fr= sprintf("%02d", $month_fr);
                    $month_to = ($i == $end) ? intval($dtRangeTo->format("m")) : 12;
                    $month_to= sprintf("%02d", $month_to);

                    $day_fr = ($i == $start) ? intval($dtRangeFrom->format("d")) : 1;
                    $day_fr= sprintf("%02d", $day_fr);
                    $day_to = ($i == $end) ? intval($dtRangeTo->format("d")) : 31;
                    $day_to= sprintf("%02d", $day_to);

//                    $data[$i] = $this->getReports(
//                            $filter_club, 
//                            array('from'=> $i .'-'.$month_fr.'-'.$day_fr. ' 00:00:00', 
//                                'to'=> $i .'-'.$month_to.'-'.$day_to. ' 23:59:59'
//                            ),
//                            $filter_coach,
//                            $filter_gender,
//                            $filter_event,
//                            $filter_agerange,
//                            $filter_results
//                            );
                }
            }
        }
        
        if(!isset($_GET['download']) && !isset($_GET['print'])){
            $request_params = implode('&', array_map(function ($v, $k) { return sprintf("%s=%s", $k, $v); }, $_GET, array_keys($_GET)));
            return $this->render('AcmeHeadOfficeBundle:Report:detailed_report.html.twig',
                    array(
                        'clubs'=> $this->getClubs('active'),
                        'events'=> $this->getEvents(),
                        'request_params' => $request_params,
//                        'summary'=> $this->getDetailedReports(
//                                $filter_club, 
//                                $filter_daterange,
//                                $filter_coach,
//                                $filter_gender,
//                                $filter_event,
//                                $filter_agerange,
//                                $filter_results
//                            ),
                        'filter_daterange' => array('from'=> 
                                                    array(
                                                        'year'=> $dtRangeFrom->format("Y"), 
                                                        'month'=> $dtRangeFrom->format("m"),
                                                        'day'=> $dtRangeFrom->format("d")
                                                        ),
                                                'to'=> 
                                                    array(
                                                        'year'=> $dtRangeTo->format("Y"), 
                                                        'month'=> $dtRangeTo->format("m"),
                                                        'day'=> $dtRangeTo->format("d")
                                                        )
                        ),
                        'ho_admin_details'=> $this->getHeadOfficeAdminUserById( $session->get('ho_admin_id') )
                    )
                );
            
            
        }else{
            // PDF
            $content =  $this->renderView('AcmeHeadOfficeBundle:Report:detailed_report_pdf.html.twig',
                    array(
                        'site_url' => $root_dir,
                        'host'=> $host,
                        'filter_daterange'=>$filter_daterange,
                        'filter_club'=> $this->getClubById($filter_club),
                        'filter_gender'=> $filter_gender,
                        'filter_event'=> $this->getEventById($filter_event),
                        'filter_agerange'=> $filter_agerange,
                        'filter_results'=> $filter_results,
                        'summary'=> $this->getDetailedReports(
                                $filter_club, 
                                $filter_daterange,
                                $filter_coach,
                                $filter_gender,
                                $filter_event,
                                $filter_agerange,
                                $filter_results
                            ),
                        'ho_admin_details'=> $this->getHeadOfficeAdminUserById( $session->get('ho_admin_id') )
                    )
                );
            
            
            $stylesheet = file_get_contents($root_dir.'/css/detailed-report-pdf.css'); /// here call you external css file 
            $stylesheet = '<style>'.$stylesheet.'</style>';

            $output = (isset($_GET['download'])) ? 1 : 0;

            $dompdf = new \DOMPDF();
            $dompdf->set_paper( 'Legal', 'landscape' );
            $dompdf->load_html( $stylesheet . $content );
            $dompdf->render();
            $dompdf->stream("detailed reports ". $datetime->format("d-m-Y") .".pdf",array('Attachment'=>$output));
            
            $response = new Response("success");
            $response->setContent('success');
            $response->headers->set('Content-Type', 'application/pdf');
            return $response;
            
            
            
        }
        
    }
    
    public function ajaxDetailedReportAction(){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $mod = new Model\GlobalModel();
        
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_club_login'));
        }
        
        if(isset($_GET['dtFrDay']) && isset($_GET['dtToDay']) &&
                isset($_GET['dtFrMonth']) && isset($_GET['dtToMonth']) &&
                isset($_GET['dtFrYear']) && isset($_GET['dtToYear'])
                ){
            $filter_daterange = array( 
                    'from' => intval($_GET['dtFrYear']) ."-". sprintf("%02d", intval($_GET['dtFrMonth'])) ."-". sprintf("%02d", intval($_GET['dtFrDay'])) . " 00:00:00",
                    'to' => intval($_GET['dtToYear']) ."-". sprintf("%02d", intval($_GET['dtToMonth'])) ."-". sprintf("%02d", intval($_GET['dtToDay'])) . " 23:59:59",
                );
        }else{
            //$filter_daterange = NULL;
            $filter_daterange = array( 
                    'from' => date('Y-m-d',strtotime('first day of ' . date("F Y",strtotime("-3 Months")) )) . ' 00:00:00',
                    'to' => $datetime->format("Y-m-d H:i:s"),
                );
        }
        
        $filter_club = (isset($_GET['sAddClub']) && isset($_GET['fClub']) && $_GET['fClub'] != '') ? intval($_GET['fClub']) : NULL;
        $filter_coach = (isset($_GET['sAddCoach']) && isset($_GET['fCoach']) && $_GET['fCoach'] != '') ? intval($_GET['fCoach']) : NULL;
        $filter_gender = (isset($_GET['sAddGender']) && isset($_GET['fGender']) && $_GET['fGender'] != '') ? $_GET['fGender'] : NULL;
        $filter_event = (isset($_GET['sAddEvent']) && isset($_GET['fEvent']) && $_GET['fEvent'] != '') ? intval($_GET['fEvent']) : NULL;
//        $filter_agerange = (isset($_GET['sAddAgeRange']) && isset($_GET['fAgeRange']) && $_GET['fAgeRange'] != '') ? $_GET['fAgeRange'] : NULL;
        $filter_age_fr = (isset($_GET['fAgeRangeFr']) && $_GET['fAgeRangeFr'] != '') ? $_GET['fAgeRangeFr'] : NULL;
        $filter_age_to = (isset($_GET['fAgeRangeTo']) && $_GET['fAgeRangeTo'] != '') ? $_GET['fAgeRangeTo'] : NULL;
        if(isset($_GET['sAddAgeRange'])){
            if( $filter_age_fr != NULL && $filter_age_to != NULL){
                $filter_age_fr = filter_var($filter_age_fr, FILTER_SANITIZE_NUMBER_INT);
                $filter_age_to = filter_var($filter_age_to, FILTER_SANITIZE_NUMBER_INT);
                $filter_agerange = $filter_age_fr . '-' . $filter_age_to;
            }elseif( $filter_age_fr != NULL && $filter_age_to == NULL){
                $filter_age_fr = filter_var($filter_age_fr, FILTER_SANITIZE_NUMBER_INT);
                $filter_agerange = $filter_age_fr . '-100';
            }elseif( $filter_age_fr == NULL && $filter_age_to != NULL){
                $filter_age_to = filter_var($filter_age_to, FILTER_SANITIZE_NUMBER_INT);
                $filter_agerange = '16-'.$filter_age_to;
            }else{
                $filter_agerange = NULL;
            }
        }else{
            $filter_agerange = NULL;
        }
        $filter_results = (isset($_GET['sAddResults']) && isset($_GET['fResults']) && $_GET['fResults'] != '') ? intval($_GET['fResults']) : NULL;
        
        $data = array();
        $dtRangeFrom = new \DateTime($filter_daterange['from']);
        $dtRangeTo = new \DateTime($filter_daterange['to']);
        
        // if the same year
        if($dtRangeFrom->format("Y") == $dtRangeTo->format("Y")){
            // if the same month
            if($dtRangeFrom->format("m") == $dtRangeTo->format("m")){

                $start = intval($dtRangeFrom->format("d"));
                $end = intval($dtRangeTo->format("d"));

                for($i=$start; $i<=$end; $i++){
                    $day= sprintf("%02d", $i);
                    $dt = \DateTime::createFromFormat('!m', $dtRangeFrom->format("m"));
//                    $data[$dt->format('M') . " " . $i] = $this->getReports(
//                            $filter_club, 
//                            array('from'=> $dtRangeFrom->format("Y") .'-'.$dtRangeFrom->format("m").'-'.$day. ' 00:00:00', 
//                                    'to'=> $dtRangeTo->format("Y") .'-'.$dtRangeFrom->format("m").'-'.$day. ' 23:59:59'
//                                ),
//                            $filter_coach,
//                            $filter_gender,
//                            $filter_event,
//                            $filter_agerange,
//                            $filter_results
//                            );
                }


            // if month differs
            }else{

                // only if month (from) < month (to)
                if(intval($dtRangeFrom->format("m")) < intval($dtRangeTo->format("m"))){
                    $start = intval($dtRangeFrom->format("m"));
                    $end = intval($dtRangeTo->format("m"));

                    for($i=$start; $i<=$end; $i++){
                        $month= sprintf("%02d", $i);
                        $dt = \DateTime::createFromFormat('!m', $i);

                        $day_fr = ($i == $start) ? intval($dtRangeFrom->format("d")) : 1;
                        $day_fr= sprintf("%02d", $day_fr);
                        $day_to = ($i == $end) ? intval($dtRangeTo->format("d")) : 31;
                        $day_to= sprintf("%02d", $day_to);

//                        $data[$dt->format('M')] = $this->getReports(
//                                $filter_club, 
//                                array('from'=> $dtRangeFrom->format("Y") .'-'.$month.'-'.$day_fr. ' 00:00:00', 
//                                    'to'=> $dtRangeTo->format("Y") .'-'.$month.'-'.$day_to. ' 23:59:59'
//                                ),
//                                $filter_coach,
//                                $filter_gender,
//                                $filter_event,
//                                $filter_agerange,
//                                $filter_results
//                                );
                    }
                }
            }

        // if year differs
        }else{
            // only if year (from) < year (to)
            if(intval($dtRangeFrom->format("Y")) < intval($dtRangeTo->format("Y"))){
                $start = intval($dtRangeFrom->format("Y"));
                $end = intval($dtRangeTo->format("Y"));

                for($i=$start; $i<=$end; $i++){
                    $month_fr = ($i == $start) ? intval($dtRangeFrom->format("m")) : 1;
                    $month_fr= sprintf("%02d", $month_fr);
                    $month_to = ($i == $end) ? intval($dtRangeTo->format("m")) : 12;
                    $month_to= sprintf("%02d", $month_to);

                    $day_fr = ($i == $start) ? intval($dtRangeFrom->format("d")) : 1;
                    $day_fr= sprintf("%02d", $day_fr);
                    $day_to = ($i == $end) ? intval($dtRangeTo->format("d")) : 31;
                    $day_to= sprintf("%02d", $day_to);

//                    $data[$i] = $this->getReports(
//                            $filter_club, 
//                            array('from'=> $i .'-'.$month_fr.'-'.$day_fr. ' 00:00:00', 
//                                'to'=> $i .'-'.$month_to.'-'.$day_to. ' 23:59:59'
//                            ),
//                            $filter_coach,
//                            $filter_gender,
//                            $filter_event,
//                            $filter_agerange,
//                            $filter_results
//                            );
                }
            }
        }
        
        $sort_key = (isset($_REQUEST['order'][0]['column'])) ? $_REQUEST['order'][0]['column'] : 0; 
        $sort_key = filter_var($sort_key, FILTER_SANITIZE_NUMBER_INT);
        $sort_value = (isset($_REQUEST['order'][0]['dir'])) ? $_REQUEST['order'][0]['dir'] : 'asc'; 
        $sort_value = ($sort_value == 'asc') ? 'asc' : 'desc';
        $sort_field = (isset($_REQUEST['columns'][$sort_key]['data'])) ? $_REQUEST['columns'][$sort_key]['data'] : 'name';
        $start = (isset($_REQUEST['start'])) ? $_REQUEST['start'] : 0;
        $start = filter_var($start, FILTER_SANITIZE_NUMBER_INT);
        $length = (isset($_REQUEST['length'])) ? $_REQUEST['length'] : 0;
        $length = filter_var($length, FILTER_SANITIZE_NUMBER_INT);
        
        $datatables_settings = array(
                    'sort_field'=>$sort_field,
                    'sort_order'=>$sort_value,
                    'start'=> $start,
                    'length'=> $length
                );
        
        $records = $this->getDetailedReports(
                            $filter_club, 
                            $filter_daterange,
                            $filter_coach,
                            $filter_gender,
                            $filter_event,
                            $filter_agerange,
                            $filter_results,
                            $datatables_settings 
                        );
        
        $data = [];
        $records_data = $records['data']; 
        for($i=0; $i<count($records_data); $i++){
            $data[] = array(
                'club_name'=> $records_data[$i]['club_name'],
                'name'=> $records_data[$i]['fname'] . ' ' . $records_data[$i]['lname'],
                'bmi_improvement'=> ($records_data[$i]['bmi_improvement'] > 0 ) ? str_replace(".0","", number_format($records_data[$i]['bmi_improvement'], 1)) : 0,
                'years_added'=> ($records_data[$i]['years_added'] > 0 ) ? str_replace(".0","", number_format($records_data[$i]['years_added'], 1)) : 0,
                'cm_lost'=> ($records_data[$i]['cm_lost'] > 0 ) ? str_replace(".0","", number_format($records_data[$i]['cm_lost'], 1)) : 0,
                'kg_lost'=> ($records_data[$i]['kg_lost'] > 0 ) ? str_replace(".0","", number_format($records_data[$i]['kg_lost'], 1)) : 0,
                'body_fat_reduction'=> ($records_data[$i]['body_fat_reduction'] > 0 ) ? str_replace(".0","", number_format($records_data[$i]['body_fat_reduction'], 1)) : 0,
                'vo2_improvement'=> ($records_data[$i]['vo2_improvement'] > 0 ) ? str_replace(".0","", number_format($records_data[$i]['vo2_improvement'], 1)) : 0,
                'squat_test_improvement'=> ($records_data[$i]['squat_test_improvement'] > 0 ) ? str_replace(".0","", number_format($records_data[$i]['squat_test_improvement'], 1)) : 0,
                'standing_functional_reach_improvement'=> ($records_data[$i]['standing_functional_reach_improvement'] > 0 ) ? str_replace(".0","", number_format($records_data[$i]['standing_functional_reach_improvement'], 1)) : 0,
                'stork_test_improvement'=> ($records_data[$i]['stork_test_improvement'] > 0 ) ? str_replace(".0","", number_format($records_data[$i]['stork_test_improvement'], 1)) : 0,
                'abdominal_test_improvement'=> ($records_data[$i]['abdominal_test_improvement'] > 0 ) ? str_replace(".0","", number_format($records_data[$i]['abdominal_test_improvement'], 1)) : 0,
                'vigorous_pa_per_week'=> ($records_data[$i]['vigorous_pa_per_week'] > 0 ) ? str_replace(".0","", number_format($records_data[$i]['vigorous_pa_per_week'], 1)) : 0,
                'strength_pa_per_week'=> ($records_data[$i]['strength_pa_per_week'] > 0 ) ? str_replace(".0","", number_format($records_data[$i]['strength_pa_per_week'], 1)) : 0,
                'smoking_improvement'=> ($records_data[$i]['smoking_improvement'] > 0 ) ? str_replace(".0","", number_format($records_data[$i]['smoking_improvement'], 1)) : 0,
                'standard_alcohol_per_week'=> ($records_data[$i]['standard_alcohol_per_week'] > 0 ) ? str_replace(".0","", number_format($records_data[$i]['standard_alcohol_per_week'], 1)) : 0,
                'maximum_alcohol_one_sitting'=> ($records_data[$i]['maximum_alcohol_one_sitting'] > 0 ) ? str_replace(".0","", number_format($records_data[$i]['maximum_alcohol_one_sitting'], 1)) : 0,
                'nutrition_score_improvement'=> ($records_data[$i]['nutrition_score_improvement'] > 0 ) ? str_replace(".0","", number_format($records_data[$i]['nutrition_score_improvement'], 1)) : 0,
                'mental_health_improvement'=> ($records_data[$i]['mental_health_improvement'] > 0 ) ? str_replace(".0","", number_format($records_data[$i]['mental_health_improvement'], 1)) : 0,
                'risk_profile_improvement'=> ($records_data[$i]['risk_profile_improvement'] > 0 ) ? str_replace(".0","", number_format($records_data[$i]['risk_profile_improvement'], 1)) : 0,
                    
            );
        }
        $response = '{ 
                "recordsTotal": '. $records['total_record'] .',
                "recordsFiltered": '. $records['total_record'] .',
                "data":'. json_encode($data).
            '}';
        return new Response($response );
        
//        return new Response( $this->renderView('AcmeHeadOfficeBundle:Report:detailed_report_records_template.html.twig',
//                array(
//                    'summary'=> $this->getDetailedReports(
//                            $filter_club, 
//                            $filter_daterange,
//                            $filter_coach,
//                            $filter_gender,
//                            $filter_event,
//                            $filter_agerange,
//                            $filter_results
//                        )
//                )
//            )
//        );
            
        
    }
    
    public function detailedReportColumnFilterAction(){
        $session = $this->getRequest()->getSession();
        
        if($session->get('ho_admin_id') == ''){ 
            return new Response("session_expired");
        }
        
        $em = $this->getDoctrine()->getManager();
        $model = $em->getRepository('AcmeHeadOfficeBundle:HeadOfficeAdmin')->findOneBy(array('ho_admin_id'=>$session->get("ho_admin_id") ));
        $model->setReportColumnFilter( json_encode($_POST['column_filter']) );
        $em->persist($model);
        $em->flush();
        return new Response("success");
    }

}
