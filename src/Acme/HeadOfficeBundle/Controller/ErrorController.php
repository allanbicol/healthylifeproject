<?php

namespace Acme\HeadOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

class ErrorController extends GlobalController
{
    
    
    public function errorAction(){
        $session = $this->getRequest()->getSession();
        
        return $this->render('AcmeHeadOfficeBundle:Error:error.html.twig');
    }
    
}
