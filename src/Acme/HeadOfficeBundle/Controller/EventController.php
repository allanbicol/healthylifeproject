<?php

namespace Acme\HeadOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

use Acme\HeadOfficeBundle\Entity\Event;

class EventController extends GlobalController
{
    public function eventAction()
    {
        $session = $this->getRequest()->getSession();
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        
        return $this->render('AcmeHeadOfficeBundle:Event:event.html.twig',
                array('events'=> $this->getEvents())
                );
    }
    
    public function addEditEventAction($slug)
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        
        if(isset($_POST['event_name'])){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            
            
            if(strtolower(trim($slug)) == 'new'){
                $model = new Event();
            }else{
                $model = $em->getRepository('AcmeHeadOfficeBundle:Event')->findOneBy(array('event_id'=>$_POST["event_id"]));
            }
            $model->setEventName($_POST['event_name']);
            $model->setLocation($_POST['location']);
            if(trim($_POST['club_id']) != ''){
                $_POST['club_id'] = intval($_POST['club_id']);
                $model->setClubId($_POST['club_id']);
            }
            $model->setStatus($_POST['status']);
            $em->persist($model);
            $em->flush();
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            
            if($error_count == 0){
                
                $em->getConnection()->commit(); 
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $_POST['event_name'] . ' has been added successfully.'
                );
                
                //return $this->redirect($this->generateUrl('acme_head_office_event_add_edit', array('slug' => 'edit')) . "?eid=".$model->getEventId());
                return $this->redirect($this->generateUrl('acme_head_office_admin_area'));
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
               
                return $this->render('AcmeHeadOfficeBundle:Club:add_edit_club.html.twig',
                        array('errors'=>$errors,
                            'post'=>$_POST,
                            'states'=> $this->getStates()
                        ));
            }
            
        }
        
        if(strtolower(trim($slug)) == 'new'){
            return $this->render('AcmeHeadOfficeBundle:Event:add_edit_event.html.twig',
                    array('clubs' => $this->getClubs('active'))
                    );
        }else{
            $_GET['eid'] = intval($_GET['eid']);
            return $this->render('AcmeHeadOfficeBundle:Event:add_edit_event.html.twig',
                    array('clubs' => $this->getClubs('active'),
                        'post'=> $this->getEventById($_GET['eid']))
                    );
        }
    }
    
    public function deleteAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        
        if(isset($_POST['event_id'])){
            $em = $this->getDoctrine()->getEntityManager();

            $_POST['event_id'] = intval($_POST['event_id']);
            $model = $em->getRepository('AcmeHeadOfficeBundle:Event')->findOneBy(array('event_id'=>$_POST["event_id"]));
            $em->remove($model);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'success',
                    'Event has been deleted successfully.'
                );

            //return $this->redirect($this->generateUrl('acme_head_office_event'));
            return $this->redirect($this->generateUrl('acme_head_office_admin_area'));
        }
        
    }

}
