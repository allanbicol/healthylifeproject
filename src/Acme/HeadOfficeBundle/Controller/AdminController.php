<?php

namespace Acme\HeadOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

class AdminController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function adminAction()
    {
        $session = $this->getRequest()->getSession();
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        
        return $this->render('AcmeHeadOfficeBundle:Admin:admin.html.twig',
                array(
                    'admin_users'=> $this->getHeadOfficeAdminUsers(),
                    'events'=> $this->getEvents(),
                    'states' => $this->getStates()
                ));
    }

}
