<?php

namespace Acme\HeadOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

use Acme\HeadOfficeBundle\Entity\HeadOfficeAdmin;

class HeadOfficeAdminController extends GlobalController
{
    public function headOfficeAdminAction()
    {
        $session = $this->getRequest()->getSession();
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        
        
        return $this->render('AcmeHeadOfficeBundle:HeadOfficeAdmin:head_office_admin.html.twig',
                array('admin_users'=> $this->getHeadOfficeAdminUsers())
                );
    }
    
    public function addEditHeadOfficeAdminAction($slug)
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        
        if(isset($_POST['email'])){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            
            if(strtolower(trim($slug)) == 'new'){
                $model = new HeadOfficeAdmin();
            }else{
                $model = $em->getRepository('AcmeHeadOfficeBundle:HeadOfficeAdmin')->findOneBy(array('ho_admin_id'=>$_POST["ho_admin_id"]));
            }
            $model->setFname($_POST['fname']);
            $model->setLname($_POST['lname']);
            if(strtolower(trim($slug)) == 'new'){
                $model->setPassword($mod->passGenerator($_POST['password']));
            }elseif(strtolower(trim($slug)) == 'edit' && $_POST['password'] != ''){
                $model->setPassword($mod->passGenerator($_POST['password']));
            }
            $model->setEmail($_POST['email']);
            $model->setStatus($_POST['status']);
            $receiveLitetests = (isset($_POST['receive_litetests'])) ? 1 : 0;
            $model->setReceiveLitetests($receiveLitetests);
            $em->persist($model);
            $em->flush();
            
            $validator = $this->get('validator');
            $errors = $validator->validate($model);
            $error_count = count($errors);
            
            if($error_count == 0){
                if($mod->isEmailValid($_POST['email']) == false || ($_POST['password'] != $_POST['confirm_password'])){
                    $errors = array();
                }
                
                if(!$mod->isEmailValid($_POST['email'])){
                    $errors[] = array('message'=>$_POST['email'].' is not a valid email.');
                    $error_count += 1;
                }

                if($_POST['password'] != $_POST['confirm_password']){
                    $errors[] = array('message'=>'Password must be repeated exactly.');
                    $error_count += 1;
                }
            }
            
            if($error_count == 0){
                
                $em->getConnection()->commit(); 
                
                $this->get('session')->getFlashBag()->add(
                    'success',
                    $_POST['fname'] . ' ' . $_POST['lname'] . ' has been added successfully.'
                );
                
                //return $this->redirect($this->generateUrl('acme_head_office_admin_add_edit', array('slug' => 'edit')) . "?id=".$model->getHoAdminId());
                return $this->redirect($this->generateUrl('acme_head_office_admin_area'));
                
                
            }else{
                $em->getConnection()->rollback();
                $em->close();
                
                $this->get('session')->getFlashBag()->add(
                    'error',
                    $errors
                );
                
               
                return $this->render('AcmeHeadOfficeBundle:HeadOfficeAdmin:add_edit_head_office_admin.html.twig',
                        array('errors'=>$errors,
                            'post'=>$_POST,
                        ));
            }
            
        }
        
        
        if(strtolower(trim($slug)) == 'new'){
            return $this->render('AcmeHeadOfficeBundle:HeadOfficeAdmin:add_edit_head_office_admin.html.twig');
        }else{
            $_GET['id'] = intval($_GET['id']);
            return $this->render('AcmeHeadOfficeBundle:HeadOfficeAdmin:add_edit_head_office_admin.html.twig',
                    array('post'=> $this->getHeadOfficeAdminUserById($_GET['id']))
                    );
        }
    }
    
    public function deleteAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        
        if(isset($_POST['ho_admin_id'])){
            $em = $this->getDoctrine()->getEntityManager();

            $_POST['ho_admin_id'] = intval($_POST['ho_admin_id']);
            $model = $em->getRepository('AcmeHeadOfficeBundle:HeadOfficeAdmin')->findOneBy(array('ho_admin_id'=>$_POST["ho_admin_id"]));
            $em->remove($model);
            $em->flush();


            $this->get('session')->getFlashBag()->add(
                    'success',
                    'Admin has been deleted successfully.'
                );
            
            return $this->redirect($this->generateUrl('acme_head_office_admin_area'));
            //return $this->redirect($this->generateUrl('acme_head_office_admin'));
        }
        
    }

}
