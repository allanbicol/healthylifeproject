<?php

namespace Acme\HeadOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

use Acme\HeadOfficeBundle\Entity\EmailQueue;

class EmailQueueController extends GlobalController
{
    public function emailQueueSenderAction()
    {
        $mod = new Model\GlobalModel();
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        $em = $this->getDoctrine()->getManager();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        $email_queue = $this->getEmailQueue();
        
        
        for($i=0; $i<count($email_queue); $i++){
            $to = json_decode($email_queue[$i]['recipients']);
            
            if($email_queue[$i]['s_test'] == 1){
                require_once($root_dir.'/resources/dompdf/dompdf_config.inc.php');
//                require_once($root_dir.'/resources/pdfc/html2pdf.class.php');
                $ref = json_decode($email_queue[$i]['reference'], true);
                
                if($ref['type'] == 'test'){
                    $test_details = $this->getLiteTestDetailsById( $ref['lite_test_id'] );

                    $content = $this->renderView('AcmeLiteTestBundle:LiteTest:email_test_pdf.html.twig',
                            array(
                                'site_url' => $root_dir,
                                'client_details' => array(
                                    'fname' => $test_details['fname'],
                                    'lname' => $test_details['lname'],
                                    'weight' => $test_details['weight'],
                                    'height' => $test_details['height'],
                                    'gender' => $test_details['gender'],
                                    'age' => $test_details['age']),
                                'test_details' => $test_details,
                                'hl_expectancy_male' => $this->getHealthyLifeExpectancies('male'),
                                'hl_expectancy_female' => $this->getHealthyLifeExpectancies('female')
                            ));

//                    $html2pdf = new \HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(10, 10, 10, 10));
                    $stylesheet = file_get_contents($root_dir.'/css/email-test-dompdf.css'); /// here call you external css file 
                    $stylesheet = '<style>'.$stylesheet.'</style>';
//                    $html2pdf->WriteHTML($stylesheet . $content);

                    $test_datetime = date_create($test_details['test_datetime_finalised']);
                    $test_datetime = date_format($test_datetime,"d-m-Y");

                    $file = $root_dir .'/temp/'. $test_details['fname'] . ' ' . $test_details['lname'] .' '.$test_datetime.'.pdf';
//                    $html2pdf->Output( $file ,"F"); //output to file

                    $dompdf = new \DOMPDF();
                    $dompdf->set_paper( 'A4' );
                    $dompdf->load_html( $stylesheet . $content );
                    $dompdf->render();
//                    $dompdf->stream($test_details['fname'] . ' ' . $test_details['lname'] .' '.$test_datetime.'.pdf',array('Attachment'=>0));
                    file_put_contents($file, $dompdf->output()); 
                    
                    $attachments = array();
                    $attachments[] = $file;


                    $this->sendEmail($to, '', $email_queue[$i]['sender'], 
                        $email_queue[$i]['subject'], $email_queue[$i]['body'], 
                        $attachments);

                    
                    
                }elseif($ref['type'] == 'premium-test'){
                    
                    require_once($root_dir.'/resources/dompdf/dompdf_config.inc.php');
                    
                    $test_details = $this->getClientTestDetailsById( $ref['client_test_id'] );
        
                    if($test_details['test_type'] == 'aia' || $test_details['test_type'] == 'normal'){
                        $first_test_details = $this->getTestForComparison($test_details['club_client_id'], 'ASC');
                        $last_test_details = $this->getTestForComparison($test_details['club_client_id'], 'DESC', $ref["client_test_id"]);
                        
                        $content = $this->renderView('AcmeClubBundle:Test:email_test_pdf.html.twig',
                                array(
                                    'site_url' => $root_dir,
                                    'client_details' => $this->getClubClientById($test_details['club_client_id']),
                                    'test_details' => $test_details,
                                    'first_test_details' => (count($first_test_details) > 0) ? $first_test_details[0] : array(),
                                    'last_test_details' => (count($last_test_details) > 0) ? $last_test_details[0] : array(),
                                    'hl_expectancy_male' => $this->getHealthyLifeExpectancies('male'),
                                    'hl_expectancy_female' => $this->getHealthyLifeExpectancies('female'),
                                    
                                    'pa_physical_activity' => $this->getSpecificTestQuestionnaireClientAnswer(1 /*physical activity*/, $test_details['club_client_id'], $ref["client_test_id"]),
                                    'pa_muscle_strengthening' => $this->getSpecificTestQuestionnaireClientAnswer(2 /*muscle strengthening*/, $test_details['club_client_id'], $ref["client_test_id"]),
                                    'alcohol_standard' => $this->getSpecificTestQuestionnaireClientAnswer(4 /*standard drink*/, $test_details['club_client_id'], $ref["client_test_id"]),
                                    'alcohol_one_setting' => $this->getSpecificTestQuestionnaireClientAnswer(5 /*standard drink one setting*/, $test_details['club_client_id'], $ref["client_test_id"]),
                                    'nutrition_questions' => $this->getTestQuestionnaire('nutrition', $test_details['club_client_id'], $ref["client_test_id"]),
                                    'mental_health_questions' => $this->getTestQuestionnaire('mental_health', $test_details['club_client_id'], $ref["client_test_id"]),
                                    'risk_profile_questions' => $this->getTestQuestionnaire('risk_profile', $test_details['club_client_id'], $ref["client_test_id"]),
                                    'smoking_options' => $this->getTestQuestionnaireOptionsByCode('smoking'),
                                    'smoking_answer' => $this->getSpecificTestQuestionnaireClientAnswer(3 /*smoking*/, $test_details['club_client_id'], $ref["client_test_id"]),
                                ));

                    }else{
                        $content = $this->renderView('AcmeClubBundle:LiteTest:email_lite_test_pdf.html.twig',
                                array(
                                    'site_url' => $root_dir,
                                    'client_details' => $this->getClubClientById($test_details['club_client_id']),
                                    'test_details' => $test_details,
                                    'hl_expectancy_male' => $this->getHealthyLifeExpectancies('male'),
                                    'hl_expectancy_female' => $this->getHealthyLifeExpectancies('female')
                                ));
                    }
//                    $html2pdf = new \HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(10, 10, 10, 10));
                    $stylesheet = file_get_contents($root_dir.'/css/email-test-dompdf.css'); /// here call you external css file 
                    $stylesheet = '<style>'.$stylesheet.'</style>';
//                    $html2pdf->WriteHTML($stylesheet . $content);

                    $test_datetime = date_create($test_details['test_datetime_finalised']);
                    $test_datetime = date_format($test_datetime,"d-m-Y");

                    $file = $root_dir .'/temp/'. $test_details['fname'] . ' ' . $test_details['lname'] .' '.$test_datetime.'.pdf';
//                    $html2pdf->Output( $file ,"F"); //output to file
                    $dompdf = new \DOMPDF();
                    $dompdf->set_paper( 'A4' );
                    $dompdf->load_html( $stylesheet . $content );
                    $dompdf->render();
                    //$dompdf->stream($test_details['fname'] . ' ' . $test_details['lname'] .' '.$test_datetime.'.pdf',array('Attachment'=>1));
                    //save the pdf file on the server
                    file_put_contents($file, $dompdf->output()); 

                    
                    $attachments = array();
                    $attachments[] = $file;


                    $this->sendEmail($to, '', $email_queue[$i]['sender'], 
                        $email_queue[$i]['subject'], $email_queue[$i]['body'], 
                        $attachments);
                    
                }
                
            }else{
                $this->sendEmail($to, '', $email_queue[$i]['from'], 
                    $email_queue[$i]['subject'], $email_queue[$i]['body']);
            }
            
            
            $model = $em->getRepository('AcmeHeadOfficeBundle:EmailQueue')
                        ->findOneBy(array(
                            'queue_id' => $email_queue[$i]['queue_id'],
                        ));
            $model->setSSent(1);

            $em->persist($model);
            $em->flush();
            
        }
        
        return new Response('success');
        
    }
    
    public function getTestForComparison($club_client_id, $order, $except_client_test_id = NULL){
        $details = $this->getClientTestHistory($club_client_id, 'recent', 'finalised', 
                $order, $except_client_test_id, FALSE);
        if(count($details) > 0){
            $details[0]['pa_physical_activity'] = $this->getSpecificTestQuestionnaireClientAnswer(1 /*physical activity*/, $club_client_id, $details[0]['client_test_id']);
            $details[0]['pa_muscle_strengthening'] = $this->getSpecificTestQuestionnaireClientAnswer(2 /*muscle strengthening*/, $club_client_id, $details[0]['client_test_id']);
            $details[0]['alcohol_standard'] = $this->getSpecificTestQuestionnaireClientAnswer(4 /*standard drink*/, $club_client_id, $details[0]['client_test_id']);
            $details[0]['alcohol_one_setting'] = $this->getSpecificTestQuestionnaireClientAnswer(5 /*standard drink one setting*/, $club_client_id, $details[0]['client_test_id']);
            $details[0]['nutrition_questions'] = $this->getTestQuestionnaire('nutrition', $club_client_id, $details[0]['client_test_id']);
            $details[0]['mental_health_questions'] = $this->getTestQuestionnaire('mental_health', $club_client_id, $details[0]['client_test_id']);
            $details[0]['risk_profile_questions'] = $this->getTestQuestionnaire('risk_profile', $club_client_id, $details[0]['client_test_id']);
            $details[0]['smoking_answer'] = $this->getSpecificTestQuestionnaireClientAnswer(3 /*smoking*/, $club_client_id, $details[0]['client_test_id']);
        }
        
        return $details;
    }

}
