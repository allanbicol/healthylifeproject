<?php

namespace Acme\HeadOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;
//use Acme\CLSclientGovBundle\Json;
use Acme\HeadOfficeBundle\Entity\HeadOfficeAdmin;

class LoginController extends GlobalController
{
    public function loginAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('ho_admin_id') != ''){
            return $this->redirect($this->generateUrl('acme_head_office_dashboard'));
        }
        
        if(isset($_POST['email'])){
            $error = 0;
            // Check username and password
            if(!isset($_POST['email']) && !isset($_POST['password'])){
                return $this->redirect($this->generateUrl('acme_head_office_login'));
            }
            
            if(trim($_POST['email']) == ''){
                $error += 1;
                $message = "Email address should not be blank.";
            }
            
            // check email validity
            if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) && $error == 0){
                $error += 1;
                $message = "Email ".$_POST['email']." is not a valid email address.";
            }
            
            if(trim($_POST['password']) == '' && $error == 0){
                $error += 1;
                $message = "Password should not be blank.";
            }
            
            // check government user
            $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:HeadOfficeAdmin');
            $query = $cust->createQueryBuilder('p')
                ->where('p.email = :email AND p.password = :password AND p.status = :status')
                ->setParameter('email', $_POST['email'])
                ->setParameter('password', $mod->passGenerator($_POST['password']))
                ->setParameter('status', 'active')
                ->getQuery();
            $rows = $query->getArrayResult();
            
            
            if(count($rows) > 0 ){
                
//                $this->get('session')->clear();
                
                // set session
//                $this->container->get('session')->migrate($destroy = false, 3600);
                
                $head_office = $this->getHeadOfficeSettings();
                
                $session->set('ho_admin_id', $rows[0]['ho_admin_id']); 
                $session->set('email', $rows[0]['email']); 
                $session->set('fname', $rows[0]['fname']); 
                $session->set('lname', $rows[0]['lname']);
                $session->set('user_type', 'head-office-user');
                $session->set('office-address', $head_office['address']);
                $session->set('office-postcode', $head_office['postcode']);
                $session->set('office-state', $head_office['state']);
                $session->set('office-city', $head_office['city']);
                $session->set('available_credit', $head_office['available_credit']);
                
                // last login
                $em = $this->getDoctrine()->getManager();
                $em->getConnection()->beginTransaction(); 
                $model = $em->getRepository('AcmeHeadOfficeBundle:HeadOfficeAdmin')->findOneBy(array('ho_admin_id'=>$rows[0]['ho_admin_id']));
                $model->setLastLoginDatetime($datetime->format('Y-m-d H:i:s'));
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                return $this->redirect($this->generateUrl('acme_head_office_dashboard'));
            }else{
                $error += 1;
                $message = "The login details you've entered <br/>don't match any in our system.";
            }
            
            if($error > 0){
                // set flash message
                $this->get('session')->getFlashBag()->add(
                        'error',
                        $message
                    );
                
                return $this->render('AcmeHeadOfficeBundle:Login:login.html.twig',
                    array('post'=> $_POST)
                );
            }
        }
        return $this->render('AcmeHeadOfficeBundle:Login:login.html.twig');
    }

    
    
     public function logoutAction(){
        $session = $this->getRequest()->getSession();
        
        $this->get('session')->clear(); // clear all sessions
        return $this->redirect($this->generateUrl('acme_head_office_login'));
    }
}
