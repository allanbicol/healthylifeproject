<?php

namespace Acme\HeadOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model\IPToLocation;
use Acme\HeadOfficeBundle\Entity\Notifications;

class AIAMapController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function aiaMapAction(){

        $search='';
        $lat = '';
        $long = '';
        if(isset($_POST['search'])){
            $search=$_POST['search'];
            $lat_long = $this->get_lat_long($search .' Australia ');
            $str_lat_long = explode(",",$lat_long);
            $lat = $str_lat_long[0];
            $long = $str_lat_long[1];
        }
         return $this->render('AcmeHeadOfficeBundle:AIAMap:map.html.twig',array('loc'=>$search,'lat'=>$lat,'long'=>$long));
    }
    
    public function nearbyAIAAction($loc){
        if($loc){
            if(strlen($loc)=='Australia'){
                $lat_long = $this->get_lat_long($loc);
            }else{
                if(strlen($loc)==4){
                $lat_long = $this->get_lat_long('Australia '.$loc);
                }else{
                    $lat_long = $this->get_lat_long($loc .' Australia ');
                }
            }
            $str_lat_long = explode(",",$lat_long);
            
            $nearby = $this->getNearbyAIAMap($str_lat_long[0], $str_lat_long[1],$loc);
            
        }
        //exit($nearby);
        return new Response($nearby);  
        
    }
    
    public function aiaDetailsAction(){
        if(isset($_GET['cid'])){
            $club_details = $this->getClubById($_GET['cid']);
//            $admin_id= $this->getClubAdminsByClubId($_GET['cid']);
//            $admin_details = $this->getClubAdminUserById($admin_id[0]['club_admin_id']);
            return $this->render('AcmeHeadOfficeBundle:AIAMap:details.html.twig',array('data'=>$club_details));
        }
        
    }
    
   
    public function requestCallAction(){
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction(); 
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if(isset($_POST['name'])){
            
            if(isset($_POST['club_id'])){
                $_POST['club_id'] = intval($_POST['club_id']);
                $club_details = $this->getClubById($_POST['club_id']);
                $club_admins = $this->getClubAdminsByClubId($_POST['club_id']);
                
                for($i=0, $cnt=count($club_admins); $i<$cnt; $i++){
                    $model = new Notifications();
                    $model->setClubId($_POST['club_id']);
                    $model->setClubAdminId($club_admins[$i]['club_admin_id']);
                    $model->setNoteType('request-call');
                    $model->setNoteDatetime($datetime->format("Y-m-d H:i:s"));
                    $model->setDetails('Request call from '.$_POST['name'].' - Phone No: '.$_POST['phone']);
                    $model->setSRead(0);
                    $model->setSReadOnloadOnly(0);
                    $em->persist($model);
                }
                $em->flush();

                
                $em->getConnection()->commit(); 

                $from = $this->container->getParameter('site_email_address');
                $subject = 'Request to Call';
                $body = $this->renderView('AcmeHeadOfficeBundle:AIAMap:email_template.html.twig',
                        array(
                            'post'=>$_POST,
                            'club_details'=>$club_details
                            )
                        );


                $this->sendEmail($_POST['club_email'], '', $from, $subject, $body); 
//                $this->sendEmail('leo@voodoocreative.com.au', '', $from, $subject, $body);
            }
            $this->get('session')->getFlashBag()->add(
                        'success',
                        'Request successfully submitted.'
                    );
//            return $this->redirect($this->generateUrl('acme_ho_aia_map_details').'?cid='.$_POST['club_id']);
            return $this->redirect("http://www.healthylifeproject.com.au/aia-details/?cid=".$_POST['club_id']);
        }
        
    }
}
