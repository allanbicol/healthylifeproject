<?php

namespace Acme\HeadOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

class CoachesController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function coachesAction(){
        $session = $this->getRequest()->getSession();
        
        $session->set('active_page', 'coaches' ); 
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }

        return $this->render('AcmeHeadOfficeBundle:Coaches:coaches.html.twig',
                    array(
                        'coaches'=> $this->getClubAdmins(),
                        'clubs'=> $this->getClubs('active')
                    )
                );
    }
    
    
    public function bulkEmailToCoachesAction(){
        
        $session = $this->getRequest()->getSession();
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        
        
        $coaches = (isset($_POST['clients'])) ? json_decode($_POST['clients']) : array();
        $subject = (isset($_POST['subject'])) ? $_POST['subject'] : '';
        $message = (isset($_POST['message'])) ? $_POST['message'] : '';
        $body = $this->renderView('AcmeHeadOfficeBundle:Coaches:bulk_email_template.html.twig',
                array('message'=> $message)
                );
        
        $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:ClubAdmin');
        $query = $cust->createQueryBuilder('p')
                ->select("p.club_admin_id, p.email, p.fname")
                ->where("p.club_admin_id IN(:coaches_ids)")
                ->setParameter("coaches_ids", $coaches)
                ->getQuery();
        $coach_details =  $query->getArrayResult();
        
        $receipients = array();
        for($i=0, $cnt=count($coach_details); $i<$cnt; $i++){
            $receipients[ $coach_details[$i]['email'] ] = $coach_details[$i]['fname']; 
        }
        
        $this->sendEmail(
                $receipients, 
                '', 
                $this->container->getParameter('site_email_address'), 
                $subject, 
                $body
            );
        
        $this->get('session')->getFlashBag()->add(
                        'success',
                        'Bulk email has been sent.'
                    );
        
        return $this->redirect($this->generateUrl('acme_head_office_coaches'));
    }
    
}
