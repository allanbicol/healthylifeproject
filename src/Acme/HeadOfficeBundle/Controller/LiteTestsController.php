<?php

namespace Acme\HeadOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

use Acme\HeadOfficeBundle\Entity\Club;
use Acme\HeadOfficeBundle\Entity\ClubAdmin;

class LiteTestsController extends GlobalController
{
    public function liteTestsAction()
    {
        $session = $this->getRequest()->getSession();
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        
        return $this->render('AcmeHeadOfficeBundle:LiteTests:litetests.html.twig',
                array('litetests'=> $this->getLiteTests('finalised') )
                );
    }

    public function viewTestAction()
    {
        $session = $this->getRequest()->getSession();
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        
        if(!isset($_GET['test'])){
            return $this->render('AcmeHeadOfficeBundle:LiteTests:view_error.html.twig');
        }
        
        $test_details = $this->getLiteTestDetailsById($_GET['test']);
        
        
        if(isset($test_details['status'])){
            if($test_details['status'] == 'finalised'){
                return $this->render('AcmeHeadOfficeBundle:LiteTests:view_test.html.twig',
                        array(
                            'test_details' => $test_details,
                            'hl_expectancy_male' => $this->getHealthyLifeExpectancies('male'),
                            'hl_expectancy_female' => $this->getHealthyLifeExpectancies('female')
                        ));
            }else{
                return $this->render('AcmeHeadOfficeBundle:LiteTests:view_error.html.twig');
            }
        }else{
            return $this->render('AcmeHeadOfficeBundle:LiteTests:view_error.html.twig');
        }
        
    }
    
    public function litetestListAction(){
        $session = $this->getRequest()->getSession();
        
        $sort_key = (isset($_REQUEST['order'][0]['column'])) ? $_REQUEST['order'][0]['column'] : 0; 
        $sort_key = filter_var($sort_key, FILTER_SANITIZE_NUMBER_INT);
        $sort_value = (isset($_REQUEST['order'][0]['dir'])) ? $_REQUEST['order'][0]['dir'] : 'asc'; 
        $sort_value = ($sort_value == 'asc') ? 'asc' : 'desc';
        $sort_field = (isset($_REQUEST['columns'][$sort_key]['data'])) ? $_REQUEST['columns'][$sort_key]['data'] : 'name';
        $start = (isset($_REQUEST['start'])) ? $_REQUEST['start'] : 0;
        $start = filter_var($start, FILTER_SANITIZE_NUMBER_INT);
        $length = (isset($_REQUEST['length'])) ? $_REQUEST['length'] : 0;
        $length = filter_var($length, FILTER_SANITIZE_NUMBER_INT);
        
        
        $datatables_settings = array(
                    'sort_field'=>$sort_field,
                    'sort_order'=>$sort_value,
                    'start'=> $start,
                    'length'=> $length
                );
        $data = $this->getHeadOfficeLiteTests($datatables_settings);
        //$data = $this->prospectClient($datatables_settings);
         $response = '{ 
                "recordsTotal": '. $data['data_count'] .',
                "recordsFiltered": '. $data['data_count'] .',
                "data":'. json_encode($data['data'],0).
            '}';
        
        
        return new response($response);
    }
    // EMAIL TEST
    public function emailTestAction(){
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        require_once($root_dir.'/resources/pdfc/html2pdf.class.php');
        
        if($session->get('ho_admin_id') == ''){
            return $this->redirect($this->generateUrl('acme_head_office_login'));
        }
        //$_POST["client_test_id"] = $_GET['client_test_id'];// uncomment this to view pdf in browser
        $_POST["client_test_id"] = intval($_POST["client_test_id"]);
                
        $test_details = $this->getLiteTestDetailsById($_POST["client_test_id"]);
        
        $content = '';
        
        
        if(isset($test_details['status'])){
            if($test_details['status'] == 'finalised'){
                $content = $this->renderView('AcmeHeadOfficeBundle:LiteTests:email_test_pdf.html.twig',
                        array(
                            'site_url' => $root_dir,
                            'test_details' => $test_details,
                            'hl_expectancy_male' => $this->getHealthyLifeExpectancies('male'),
                            'hl_expectancy_female' => $this->getHealthyLifeExpectancies('female')
                        ));
            }else{
                return new Response("error");
            }
        }else{
            return new Response("error");
        }
        
        
        $html2pdf = new \HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(10, 10, 10, 10));
        //$html2pdf->setEncoding("ISO-8859-1");
        $stylesheet = file_get_contents($root_dir.'/css/email-test.css'); /// here call you external css file 
        $stylesheet = '<style>'.$stylesheet.'</style>';
        $html2pdf->WriteHTML($stylesheet . $content);
        
        $test_datetime = date_create($test_details['test_datetime_finalised']);
        $test_datetime = date_format($test_datetime,"d-m-Y");
        
        $file = $root_dir .'/temp/'. $test_details['fname'] . ' ' . $test_details['lname'] .' '.$test_datetime.'.pdf';
        $html2pdf->Output( $file ,"F"); //output to file
        
        /* uncomment below to view pdf in browser */
//        $html2pdf->Output( $file);
//        $response = new Response("success");
//        $response->setContent('success');
//        $response->headers->set('Content-Type', 'application/pdf');
//        return $response;
        
        
        
        
        $to = $_POST['recipients'];
        $to = explode(",", $to);
//        $from = $session->get('email');
        $from = $this->container->getParameter('site_email_address');
        $subject = $test_details['fname'] . ' ' . $test_details['lname'] . ' -  Test Results PDF';
        $body = $this->renderView('AcmeHeadOfficeBundle:LiteTests:email_test_content.html.twig',
                        array(
                            'name' => $test_details['fname'],
                            'test_date' => $test_details['test_datetime_finalised']
                        ));
        
        $attachments = array();
        
        
        $attachments[] = $file;
        
        $recipients = array();
        for($i=0; $i<count($to); $i++){
            if($mod->isEmailValid(trim($to[$i]))){
                $recipients[] = trim($to[$i]);
            }
        }
        
        if(count($recipients) > 0){
            $this->sendEmail($recipients, '', $from, $subject, $body, $attachments);
        }else{
            unlink($file);
        }
        
        return new Response("success");
    }
    
}
