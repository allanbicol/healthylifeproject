<?php

namespace Acme\ClubClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

use Acme\HeadOfficeBundle\Entity\ClientTest;
use Acme\HeadOfficeBundle\Entity\LifestyleQuestionnareClientAnswer;

class LifeStyleQuestionnairesController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $formula = new Model\TestFormulas();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $em = $this->getDoctrine()->getManager();
        
        
        if(count($_POST) > 0){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            $error = array();
            
            $_POST['test'] = filter_var($_POST['test'], FILTER_SANITIZE_NUMBER_INT);
            
            $test = $em->getRepository('AcmeHeadOfficeBundle:ClientTest')
                        ->findOneBy(array(
                            'client_test_id' => $_POST['test'],
                            'status' => 'draft'
                        ));
            
            $club_client_details = $this->getClubClientById( $test->getClubClientId() );
            
            
                $_POST['moderate_vigorous_pa'] = intval($_POST['moderate_vigorous_pa']);
                $_POST['muscle_strenthening_pa'] = intval($_POST['muscle_strenthening_pa']);
                $moderate_vigorous_pa = $formula->moderateOrVigorousIntensityPhysicalActivity($_POST['moderate_vigorous_pa']);
                $muscle_strenthening_pa = $formula->muscleStrentheningPhysicalActivity($_POST['muscle_strenthening_pa']);

                $physicalActivity = $formula->physicalActivity($moderate_vigorous_pa, $muscle_strenthening_pa);
            $test->setLifestylePhysicalActivity( $physicalActivity );
            
                $_POST['lifestyle_smoking'] = intval($_POST['lifestyle_smoking']);
                $smokingHabits = $formula->smokingHabits($club_client_details['gender'], $_POST['lifestyle_smoking']);
            $test->setLifestyleSmoking( $smokingHabits );
            
                $_POST['per_week_value'] = intval($_POST['per_week_value']);
                $_POST['one_sitting_value'] = intval($_POST['one_sitting_value']);
                $alcohol_consumption_per_week = $formula->alcoholConsumptionPerWeekResult($club_client_details['gender'], $_POST['per_week_value']);
                $alcohol_consumption_in_one_sitting = $formula->alcoholConsumptionInOneSittingResult($_POST['one_sitting_value']);

                $alcoholConsumptionTotal = $formula->alcoholConsumptionTotal($alcohol_consumption_per_week, $alcohol_consumption_in_one_sitting);
            $test->setLifestyleAlcohol( $alcoholConsumptionTotal );
            
                $number_of_yes = 0;
                if(isset($_POST['nutrition'])){
                    foreach ($_POST['nutrition'] as $lifestyle_question_id => $answer) {
                        $number_of_yes += intval($answer);
                    }
                }
                $nutrition = $formula->nutrition($number_of_yes);
            $test->setLifestyleNutrition( $nutrition );
            
                if(isset($_POST['mental_health'])){
                    $mental_health_value = 0;
                    foreach ($_POST['mental_health'] as $lifestyle_question_id => $answer) {
                        if($answer != ''){
                            $mental_health_value += intval($answer);
                        }
                    }
                    if($mental_health_value > 0){
                        $mentalHealth = $formula->mentalHealth($mental_health_value);

                    }else{
                        $mentalHealth = '';
                    }
                }else{
                    $mentalHealth = '';
                }
            $test->setLifestyleMentalHealth( $mentalHealth );
            
                if(isset($_POST['risk_profile'])){
                    $risk_profile_value = 0;
                    foreach ($_POST['risk_profile'] as $lifestyle_question_id => $answer) {
                        if($answer != ''){
                            $risk_profile_value += intval($answer);
                        }
                    }
                    if($risk_profile_value > 0){
                        //$response = $formula->mentalHealth($mental_health_value);
                        $riskProfile = $formula->riskProfile($risk_profile_value);

                    }else{
                        $riskProfile = '';
                    }
                }else{
                    $riskProfile = '';
                }
            $test->setLifestyleRiskProfile( $riskProfile );
            $em->persist($test);
            $em->flush();
            
            
            
            // SAVE LIFESTYLE QUESTIONNAIRE ANSWERS
            if(isset($_POST['moderate_vigorous_pa'])){
                $_POST['moderate_vigorous_pa'] = intval($_POST['moderate_vigorous_pa']);
                $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                        ->findOneBy(array(
                            'lq_id' => 1, 
                            'club_client_id' => $test->getClubClientId(),
                            'client_test_id' => $test->getClientTestId()
                        ));

                if(count($model) == 0){
                    $model = new LifestyleQuestionnareClientAnswer();
                    $model->setLqId(1);
                }

                $model->setStatus('saved');
                $model->setClubClientId($test->getClubClientId());
                $model->setClientTestId($test->getClientTestId());
                $model->setAnswer($_POST['moderate_vigorous_pa']);
                $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                $em->persist($model);
                $em->flush();
            }
            
            if(isset($_POST['muscle_strenthening_pa'])){
                $_POST['muscle_strenthening_pa'] = intval($_POST['muscle_strenthening_pa']);
                $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                        ->findOneBy(array(
                            'lq_id' => 2, 
                            'club_client_id' => $test->getClubClientId(),
                            'client_test_id' => $test->getClientTestId()
                        ));

                if(count($model) == 0){
                    $model = new LifestyleQuestionnareClientAnswer();
                    $model->setLqId(2);
                }

                $model->setStatus('saved');
                $model->setClubClientId($test->getClubClientId());
                $model->setClientTestId($test->getClientTestId());
                $model->setAnswer($_POST['muscle_strenthening_pa']);
                $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                $em->persist($model);
                $em->flush();
            }
            
            if(isset($_POST['lifestyle_smoking'])){
                $_POST['lifestyle_smoking'] = intval($_POST['lifestyle_smoking']);
                $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                        ->findOneBy(array(
                            'lq_id' => 3, 
                            'club_client_id' => $test->getClubClientId(),
                            'client_test_id' => $test->getClientTestId()
                        ));

                if(count($model) == 0){
                    $model = new LifestyleQuestionnareClientAnswer();
                    $model->setLqId(3);
                }

                $model->setStatus('saved');
                $model->setClubClientId($test->getClubClientId());
                $model->setClientTestId($test->getClientTestId());
                $model->setAnswer($_POST['lifestyle_smoking']);
                $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                $em->persist($model);
                $em->flush();
            }
            
            if(isset($_POST['per_week_value'])){
                $_POST['per_week_value'] = intval($_POST['per_week_value']);
                $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                        ->findOneBy(array(
                            'lq_id' => 4, 
                            'club_client_id' => $test->getClubClientId(),
                            'client_test_id' => $test->getClientTestId()
                        ));

                if(count($model) == 0){
                    $model = new LifestyleQuestionnareClientAnswer();
                    $model->setLqId(4);
                }

                $model->setStatus('saved');
                $model->setClubClientId($test->getClubClientId());
                $model->setClientTestId($test->getClientTestId());
                $model->setAnswer($_POST['per_week_value']);
                $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                $em->persist($model);
                $em->flush();
            }
            
            if(isset($_POST['one_sitting_value'])){
                $_POST['one_sitting_value'] = intval($_POST['one_sitting_value']);
                $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                        ->findOneBy(array(
                            'lq_id' => 5, 
                            'club_client_id' => $test->getClubClientId(),
                            'client_test_id' => $test->getClientTestId()
                        ));

                if(count($model) == 0){
                    $model = new LifestyleQuestionnareClientAnswer();
                    $model->setLqId(5);
                }

                $model->setStatus('saved');
                $model->setClubClientId($test->getClubClientId());
                $model->setClientTestId($test->getClientTestId());
                $model->setAnswer($_POST['one_sitting_value']);
                $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                $em->persist($model);
                $em->flush();
            }
            
            if(isset($_POST['nutrition'])){
                foreach ($_POST['nutrition'] as $lifestyle_question_id => $answer) {
                    $em = $this->getDoctrine()->getManager();

                    $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                            ->findOneBy(array(
                                'lq_id' => $lifestyle_question_id, 
                                'club_client_id' => $test->getClubClientId(),
                                'client_test_id' => $test->getClientTestId()
                            ));

                    if(count($model) == 0){
                        $model = new LifestyleQuestionnareClientAnswer();
                        $model->setLqId($lifestyle_question_id);
                    }

                    $model->setStatus('saved');
                    $model->setClubClientId($test->getClubClientId());
                    $model->setClientTestId($test->getClientTestId());
                    $model->setAnswer($answer);
                    $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                    $em->persist($model);
                    $em->flush();
                }

            }
            
            if(isset($_POST['mental_health'])){
                foreach ($_POST['mental_health'] as $lifestyle_question_id => $answer) {
                    $em = $this->getDoctrine()->getManager();

                    $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                            ->findOneBy(array(
                                'lq_id' => $lifestyle_question_id, 
                                'club_client_id' => $test->getClubClientId(),
                                'client_test_id' => $test->getClientTestId()
                            ));

                    if(count($model) == 0){
                        $model = new LifestyleQuestionnareClientAnswer();
                        $model->setLqId($lifestyle_question_id);
                    }

                    $model->setStatus('saved');
                    $model->setClubClientId($test->getClubClientId());
                    $model->setClientTestId($test->getClientTestId());
                    $model->setAnswer($answer);
                    $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                    $em->persist($model);
                    $em->flush();
                }

            }
            
            if(isset($_POST['risk_profile'])){
                foreach ($_POST['risk_profile'] as $lifestyle_question_id => $answer) {
                    $em = $this->getDoctrine()->getManager();

                    $model = $em->getRepository('AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer')
                            ->findOneBy(array(
                                'lq_id' => $lifestyle_question_id, 
                                'club_client_id' => $test->getClubClientId(),
                                'client_test_id' => $test->getClientTestId()
                            ));

                    if(count($model) == 0){
                        $model = new LifestyleQuestionnareClientAnswer();
                        $model->setLqId($lifestyle_question_id);
                    }

                    $model->setStatus('saved');
                    $model->setClubClientId($test->getClubClientId());
                    $model->setClientTestId($test->getClientTestId());
                    $model->setAnswer($answer);
                    $model->setAnswerDatetime($datetime->format('Y-m-d H:i:s'));
                    $em->persist($model);
                    $em->flush();
                }

            }
            
            
            
            $s_lifestyle_physical_activity_answer_complete = $this->sLifestyleTestQuestionnaireAnswersDone('physical_activity', $test->getClubClientId(), $_POST['test']);
            $s_lifestyle_smoking_answer_complete = $this->sLifestyleTestQuestionnaireAnswersDone('smoking', $test->getClubClientId(), $_POST['test']);
            $s_lifestyle_alcohol_answer_complete = $this->sLifestyleTestQuestionnaireAnswersDone('alcohol', $test->getClubClientId(), $_POST['test']);
            $s_lifestyle_nutrition_answer_complete = $this->sLifestyleTestQuestionnaireAnswersDone('nutrition', $test->getClubClientId(), $_POST['test']);
            $s_lifestyle_mental_health_answer_complete = $this->sLifestyleTestQuestionnaireAnswersDone('mental_health', $test->getClubClientId(), $_POST['test']);
            $s_lifestyle_risk_profile_answer_complete = $this->sLifestyleTestQuestionnaireAnswersDone('risk_profile', $test->getClubClientId(), $_POST['test']);
   
            if( $_POST['submit_type'] == 'finalise' ){
                if(!$s_lifestyle_physical_activity_answer_complete){
                    $error[] = array(
                            'message' => "Please make sure to complete Physical Activity test."
                        );
                }

                if(!$s_lifestyle_smoking_answer_complete){
                    $error[] = array(
                            'message' => "Please make sure to complete Smoking Habit test."
                        );
                }

                if(!$s_lifestyle_alcohol_answer_complete){
                    $error[] = array(
                            'message' => "Please make sure to complete Alcohol Consumption test."
                        );
                }

                if(!$s_lifestyle_nutrition_answer_complete){
                    $error[] = array(
                            'message' => "Please make sure to complete Nutrition test."
                        );
                }

                if(!$s_lifestyle_mental_health_answer_complete){
                    $error[] = array(
                            'message' => "Please make sure to complete Mental Health test."
                        );
                }

                if(!$s_lifestyle_risk_profile_answer_complete){
                    $error[] = array(
                            'message' => "Please make sure to complete Risk Profile test."
                        );
                }
            }
            if(count($error) > 0){
                $this->get('session')->getFlashBag()->add(
                        'error',
                        $error
                    );
                
                $post = array(
                    'test' => $_POST['test'],
                    'lifestyle_physical_activity' => $physicalActivity,
                    'lifestyle_smoking' => $smokingHabits,
                    'lifestyle_alcohol' => $alcoholConsumptionTotal,
                    'lifestyle_nutrition' => $nutrition,
                    'lifestyle_mental_health' => $mentalHealth,
                    'lifestyle_risk_profile' => $riskProfile,
                );
                
                $client_details = $this->getClubClientById( $test->getClubClientId() );
                
                return $this->render('AcmeClubClientBundle:LifeStyleQuestionnaires:create_test.html.twig',
                        array(
                            'test_id' => $_POST['test'],
                            'client_details' => $client_details,
                            'club_details' => $this->getClubById( $client_details['club_id'] ),
                            'physical_activity_questions' => $this->getTestQuestionnaire('physical_activity', $test->getClubClientId(), $test->getClientTestId()),
                            'alcohol_questions' => $this->getTestQuestionnaire('alcohol', $test->getClubClientId(), $test->getClientTestId()),
                            'nutrition_questions' => $this->getTestQuestionnaire('nutrition', $test->getClubClientId(), $test->getClientTestId()),
                            'mental_health_questions' => $this->getTestQuestionnaire('mental_health', $test->getClubClientId(), $test->getClientTestId()),
                            'mental_health_options' => $this->getTestQuestionnaireOptionsByCode('mental_health'),
                            'risk_profile_questions' => $this->getTestQuestionnaire('risk_profile', $test->getClubClientId(), $test->getClientTestId()),
                            'smoking_options' => $this->getTestQuestionnaireOptionsByCode('smoking'),
                            'smoking_answer' => $this->getSpecificTestQuestionnaireClientAnswer(3 /*smoking*/, $test->getClubClientId(), $test->getClientTestId()),
                        )
                    );
                
                
            }else{
                
                $em->getConnection()->commit(); 
                // Notify coach
                
                if( $_POST['submit_type'] == 'finalise' ){
                    
                    $em = $this->getDoctrine()->getManager();
                    $client = $em->getRepository('AcmeHeadOfficeBundle:ClubClient')
                                    ->findOneBy(array(
                                        'club_client_id' => $test->getClubClientId()
                                    ));
                    $client->setDoLifestyleTest(0);
                    $em->persist($client);
                    $em->flush();

                    $coach_details = $this->getClubAdminUserById( $test->getClubAdminId() );
                    if(count($coach_details) > 0){
                        $this->setNotification($coach_details['club_id'], 
                                $test->getClubAdminId(), 
                                'test', 
                                'has submitted his/her lifestyle test.',
                                $test->getClubClientId()
                                );
                        
                        $from = $this->container->getParameter('site_email_address');
                        $subject = $club_client_details['fname'] . ' ' . $club_client_details['lname'] . ' - 6 Lifestyle Questionnaires Complete';
                        $body = $this->renderView('AcmeClubClientBundle:LifeStyleQuestionnaires:coach_notification_email_template.html.twig',
                                        array(
                                            'club_client_details' => $club_client_details,
                                            'coach_name' => $coach_details['fname'],
                                            'client_test_id' => $_POST['test'],
                                            'site_url' => $mod->siteURL()
                                        ));
                        
                        $this->sendEmail(array($coach_details['email']=> $coach_details['fname'] . ' ' . $coach_details['lname']), '', $from, $subject, $body);
                        
                        
                    }
//                    $this->get('session')->getFlashBag()->add(
//                            'success',
//                            'Thank you! Your lifestyle questionnaires answers have been submitted.'
//                        );

                    return $this->redirect($this->generateUrl('acme_club_client_lifestyle_questionnaires_submit_success'));
                }else{
                    return new Response("success");
                }
                
            }
            
        }else{
            if(!isset($_GET['test'])){
                return $this->render('AcmeClubClientBundle:LifeStyleQuestionnaires:view_error.html.twig');
            }
            $_GET['test'] = base64_decode($_GET['test']);
            $_GET['test'] = filter_var($_GET['test'], FILTER_SANITIZE_NUMBER_INT);
            
            
            $test = $em->getRepository('AcmeHeadOfficeBundle:ClientTest')
                                ->findOneBy(array(
                                    'client_test_id' => $_GET['test'],
                                    'status'=> 'draft'
                                ));

            if( count($test) <= 0 ){
                return $this->render('AcmeClubClientBundle:LifeStyleQuestionnaires:view_error.html.twig');
            }
            
            $client_details = $this->getClubClientById( $test->getClubClientId() );
            
            if( count($client_details) <= 0 ){
                return $this->render('AcmeClubClientBundle:LifeStyleQuestionnaires:view_error.html.twig');
            }
            
            if( $client_details['do_lifestyle_test'] == 0 ){
                return $this->render('AcmeClubClientBundle:LifeStyleQuestionnaires:view_error.html.twig');
            }
            
            return $this->render('AcmeClubClientBundle:LifeStyleQuestionnaires:create_test.html.twig',
                        array(
                            'test_id' => $_GET['test'],
                            'client_details' => $client_details,
                            'club_details' => $this->getClubById( $client_details['club_id'] ),
                            'physical_activity_questions' => $this->getTestQuestionnaire('physical_activity', $test->getClubClientId(), $test->getClientTestId()),
                            'alcohol_questions' => $this->getTestQuestionnaire('alcohol', $test->getClubClientId(), $test->getClientTestId()),
                            'nutrition_questions' => $this->getTestQuestionnaire('nutrition', $test->getClubClientId(), $test->getClientTestId()),
                            'mental_health_questions' => $this->getTestQuestionnaire('mental_health', $test->getClubClientId(), $test->getClientTestId()),
                            'mental_health_options' => $this->getTestQuestionnaireOptionsByCode('mental_health'),
                            'risk_profile_questions' => $this->getTestQuestionnaire('risk_profile', $test->getClubClientId(), $test->getClientTestId()),
                            'smoking_options' => $this->getTestQuestionnaireOptionsByCode('smoking'),
                            'smoking_answer' => $this->getSpecificTestQuestionnaireClientAnswer(3 /*smoking*/, $test->getClubClientId(), $test->getClientTestId()),
                        )
                    );
        }
    }
    
    public function successPageAction(){
        return $this->render('AcmeClubClientBundle:LifeStyleQuestionnaires:success_page.html.twig');
    }
    

}
