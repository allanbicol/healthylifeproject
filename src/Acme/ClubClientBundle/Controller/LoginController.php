<?php

namespace Acme\ClubClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;
//use Acme\CLSclientGovBundle\Json;

class LoginController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function loginAction()
    {
        $session = $this->getRequest()->getSession();
        $mod = new Model\GlobalModel();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        
        if($session->get('club_client_id') != ''){
            return $this->redirect($this->generateUrl('acme_club_client_dashboard'));
        }
        
       
        if(isset($_POST['email'])){
            
            // Check username and password
            if(!isset($_POST['email']) && !isset($_POST['password'])){
                return $this->redirect($this->generateUrl('acme_club_client_login'));
            }
            $error = 0; 
            if(trim($_POST['email']) == ''){
                $error += 1;
                $message = "Email address should not be blank.";
            }
            
            // check email validity
            if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) && $error == 0){
                $error += 1;
                $message = "Email ".$_POST['email']." is not a valid email address.";
            }
            
            if(trim($_POST['password']) == '' && $error == 0){
                $error += 1;
                $message = "Password should not be blank.";
            }
           
            // check government user
            $cust = $this->getDoctrine()->getRepository('AcmeHeadOfficeBundle:ClubClient');
            $query = $cust->createQueryBuilder('p')
                ->where('p.email = :email AND p.password = :password AND p.status = :status')
                ->setParameter('email', $_POST['email'])
                ->setParameter('password', $mod->passGenerator($_POST['password']))
                ->setParameter('status', 'active')
                ->getQuery();
            $rows = $query->getArrayResult();
            
             
            if(count($rows) > 0 ){
                $this->get('session')->clear();
                
                // set session
                $this->container->get('session')->migrate($destroy = false, 3600);
                
                $club = $this->getClubById($rows[0]['club_id']);
                
                $session->set('club_client_id', $rows[0]['club_client_id']); 
                $session->set('email', $rows[0]['email']); 
                $session->set('fname', $rows[0]['fname']); 
                $session->set('lname', $rows[0]['lname']);
                $session->set('club_id', $rows[0]['club_id']);
                $session->set('club_name', $club['club_name']);
                $session->set('user_type', 'club-client');
                
                
                // last login
                $em = $this->getDoctrine()->getManager();
                $em->getConnection()->beginTransaction(); 
                $model = $em->getRepository('AcmeHeadOfficeBundle:ClubClient')->findOneBy(array('club_client_id'=>$rows[0]['club_client_id']));
                $model->setLastLoginDatetime($datetime->format('Y-m-d H:i:s'));
                $em->persist($model);
                $em->flush();
                $em->getConnection()->commit(); 
                
                
                // SET ACTIVITY
                $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " has logged into the site.";
                $this->setActivity($session->get('club_client_id'), 'club-client', $details);
                
                return $this->redirect($this->generateUrl('acme_club_client_dashboard'));
            }else{
                $error += 1;
                $message = "The login details you've entered <br/>don't match any in our system.";
            }
            
            if($error > 0){
                // set flash message
                $this->get('session')->getFlashBag()->add(
                        'error',
                        $message
                    );
                
                return $this->render('AcmeClubClientBundle:Login:login.html.twig',
                    array('post'=> $_POST)
                );
            }
        }
        return $this->render('AcmeClubClientBundle:Login:login.html.twig');
    }

    public function logoutAction(){
        $session = $this->getRequest()->getSession();
        
        
        // SET ACTIVITY
        $details = $session->get('fname') . " " . $session->get('lname') . " of " . $session->get('club_name') . " has logged out from the site.";
        $this->setActivity($session->get('club_client_id'), 'club-client', $details);

        $this->get('session')->clear(); // clear all sessions
        
        return $this->redirect($this->generateUrl('acme_club_client_login'));
    }
    
}
