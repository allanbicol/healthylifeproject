<?php

namespace Acme\ClubClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('AcmeClubClientBundle:Default:index.html.twig', array('name' => $name));
    }
}
