<?php

namespace Acme\ClubClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ViewTestController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function viewTestAction()
    {
        $session = $this->getRequest()->getSession();
        if($session->get('club_client_id') == ''){
            return $this->redirect($this->generateUrl('acme_club_client_login'));
        }
        
        
        $test_details = $this->getClientTestDetailsById($_GET['id'], $session->get('club_client_id'));
        
        
        if(isset($test_details['status'])){
            if($test_details['status'] == 'finalised'){
                return $this->render('AcmeClubClientBundle:ViewTest:view_test.html.twig',
                        array(
                            'client_details' => $this->getClubClientById($session->get('club_client_id')),
                            'test_details' => $test_details,
                            'hl_expectancy_male' => $this->getHealthyLifeExpectancies('male'),
                            'hl_expectancy_female' => $this->getHealthyLifeExpectancies('female')
                        ));
            }else{
                return $this->render('AcmeClubClientBundle:ViewTest:view_error.html.twig');
            }
        }else{
            return $this->render('AcmeClubClientBundle:ViewTest:view_error.html.twig');
        }
        
    }

}
