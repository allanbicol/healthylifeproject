<?php

namespace Acme\ClubClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DashboardController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    public function dashboardAction()
    {
        $session = $this->getRequest()->getSession();
        if($session->get('club_client_id') == ''){
            return $this->redirect($this->generateUrl('acme_club_client_login'));
        }
        
        return $this->render('AcmeClubClientBundle:Dashboard:dashboard.html.twig',
                array(
                    'client_details' => $this->getClubClientById($session->get('club_client_id')),
                    'client_test_history' => $this->getClientTestHistory($session->get('club_client_id'), NULL, 'finalised'),
                    'test_data' => $this->getClientTestHistory($session->get('club_client_id'), NULL, 'finalised', 'ASC')
                    )
                );
    }

}
