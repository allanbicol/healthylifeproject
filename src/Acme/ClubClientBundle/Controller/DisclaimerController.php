<?php

namespace Acme\ClubClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DisclaimerController extends Controller
{
    public function disclaimerAction()
    {
       return $this->render('AcmeClubClientBundle:Disclaimer:disclaimer.html.twig'); 
    }

}
