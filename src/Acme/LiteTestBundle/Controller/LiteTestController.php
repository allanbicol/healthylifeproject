<?php

namespace Acme\LiteTestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

use Acme\HeadOfficeBundle\Entity\ClientTest;
use Acme\HeadOfficeBundle\Entity\LiteTest;
use Acme\HeadOfficeBundle\Entity\LifestyleQuestionnareClientAnswer;
use Acme\HeadOfficeBundle\Entity\EmailQueue;

class LiteTestController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    
    
    // CREATE TEST
    public function createTestAction()
    {
        
        
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $formula = new Model\TestFormulas();
        $mod = new Model\GlobalModel();
        
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        require_once($root_dir.'/resources/pdfc/html2pdf.class.php');
        
        
        
        if(count($_POST) > 0){
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction(); 
            $error = array();
            
            // VALIDATE INPUTS

            $s_lifestyle_physical_activity_answer_complete = $this->sLiteLifestyleTestQuestionnaireAnswersDone('physical-activity');
            $s_lifestyle_smoking_answer_complete = $this->sLiteLifestyleTestQuestionnaireAnswersDone('smoking');
            $s_lifestyle_alcohol_answer_complete = $this->sLiteLifestyleTestQuestionnaireAnswersDone('alcohol');
            $s_lifestyle_nutrition_answer_complete = $this->sLiteLifestyleTestQuestionnaireAnswersDone('nutrition');
            
            if(trim($_POST['fname']) == ''){
                $error[] = array(
                                'message' => "Please enter your first name."
                        );
            }

            if(trim($_POST['lname']) == ''){
                $error[] = array(
                                'message' => "Please enter your last name."
                        );
            }

            if(trim($_POST['age']) == '' || trim($_POST['age']) == 0){
                $error[] = array(
                                'message' => "Please enter valid age (year)."
                        );
            }

            if(trim($_POST['height']) == '' || trim($_POST['height']) == 0){
                $error[] = array(
                                'message' => "Please enter valid height (cm)."
                        );
            }

            if(trim($_POST['weight']) == '' || trim($_POST['weight']) == 0){
                $error[] = array(
                                'message' => "Please enter valid weight (kg)."
                        );
            }

            if(!$s_lifestyle_physical_activity_answer_complete){
                $error[] = array(
                                'message' => "Please make sure to complete Physical Activity test."
                        );
            }

            if(!$s_lifestyle_smoking_answer_complete){
                $error[] = array(
                                'message' => "Please make sure to complete Smoking Habit test."
                        );
            }

            if(!$s_lifestyle_alcohol_answer_complete){
                $error[] = array(
                                'message' => "Please make sure to complete Alcohol Consumption test."
                        );
            }

            if(!$s_lifestyle_nutrition_answer_complete){
                $error[] = array(
                                'message' => "Please make sure to complete Nutrition test."
                        );
            }

            if(trim($_POST['biometric_waist_value']) == ''){
                $error[] = array(
                                'message' => "Waist must not be blank."
                        );
            }
            
            if( !$mod->isEmailValid($_POST['email']) ){
                $error[] = array(
                                'message' => "Email address is invalid."
                        );
            }
            
            $_POST['weight'] = intval($_POST['weight']);
            $_POST['height'] = intval($_POST['height']);
            $_POST['age'] = intval($_POST['age']);
            $_POST['biometric_waist_value'] = intval($_POST['biometric_waist_value']);
            
            
            $test = new LiteTest();
            $session->set('litetest_id',$test->getLiteTestId());
            $test->setFname( trim($_POST['fname']) );
            $test->setLname( trim($_POST['lname']) );
            $test->setEmail( $_POST['email'] );
            $test->setGender( $_POST['gender'] );
            $test->setAge($_POST['age']);
            if($_POST['weight'] != ''){
                $test->setWeight($_POST['weight']);
            }else{
                $test->setWeight(0);
            }
            if($_POST['height'] != ''){
                $test->setHeight($_POST['height']);
            }else{
                $test->setHeight(0);
            }
            
            $healthy_life_expectancy_male = $this->getHealthyLifeExpectancies('male');
            $healthy_life_expectancy_female = $this->getHealthyLifeExpectancies('female');
            $male_max_score = $this->getHealthyLifeMaxWorstScore('male', 'max');
            $female_max_score = $this->getHealthyLifeMaxWorstScore('female', 'max');
            $male_worst_score = $this->getHealthyLifeMaxWorstScore('male', 'worst');
            $female_worst_score = $this->getHealthyLifeMaxWorstScore('female', 'worst');
            
            $test->setHealthyLifeExpectancyDalysFemale($healthy_life_expectancy_female['dalys']);
            $test->setHealthyLifeExpectancyDalysMale($healthy_life_expectancy_male['dalys']);
            $test->setHealthyLifeExpectancyFemale($healthy_life_expectancy_female['expectancy']);
            $test->setHealthyLifeExpectancyMale($healthy_life_expectancy_male['expectancy']);
            $test->setHealthyLifeExpectancyTotalFemale($healthy_life_expectancy_female['total_expectancy']);
            $test->setHealthyLifeExpectancyTotalMale($healthy_life_expectancy_male['total_expectancy']);
            $test->setHealthyLifeMaxScoreFemale($female_max_score['score']);
            $test->setHealthyLifeMaxScoreMale($male_max_score['score']);
            $test->setHealthyLifeWorstScoreFemale($female_worst_score['score']);
            $test->setHealthyLifeWorstScoreMale($male_worst_score['score']);
            
            
            
            $test->setPhysiologicalVo2TestValue1(0);
            $test->setPhysiologicalVo2TestValue2(0);
            $test->setPhysiologicalBalanceValue(0);
            $test->setPhysiologicalSquatValue(0);
            $test->setPhysiologicalSitupValue(0); 
            
            if(trim($_POST['biometric_waist_value']) != ''){
                $test->setBiometricWaistValue($_POST['biometric_waist_value']);
            }else{
                $test->setBiometricWaistValue(0);
            }
            $test->setBiometricChest(0);
            $test->setBiometricAbdomen(0);
            $test->setBiometricHip(0);
            $test->setBiometricThigh(0);
            $test->setBiometricArm(0);
            $test->setBiometricBodyFatValue(0);
            $test->setBiometricBloodPressureSystolic(0);
            $test->setBiometricBloodPressureDiastolic(0);
            $test->setBiometricBloodOxygenValue(0);
            
            
            
            // Results
                $bmi = $formula->bMI($_POST['height'], $_POST['weight']);
            $test->setBmi($bmi);
            $test->setLifestylePhysicalActivity($_POST['lifestyle_physical_activity']);
            $test->setLifestyleSmoking($_POST['lifestyle_smoking']);
            $test->setLifestyleAlcohol($_POST['lifestyle_alcohol']);
            $test->setLifestyleNutrition($_POST['lifestyle_nutrition']);
                $biometricWaist = $formula->waist($_POST['gender'], $_POST['biometric_waist_value'], TRUE);
            $test->setBiometricWaist($biometricWaist);
            
            
            
            $totalCrf = $formula->totalCRF(
                    $formula->crf($_POST['lifestyle_physical_activity']), 
                    $formula->crf($_POST['lifestyle_smoking']), 
                    $formula->crf($_POST['lifestyle_alcohol']), 
                    $formula->crf($_POST['lifestyle_nutrition']), 
                    0, //$formula->crf($_POST['lifestyle_mental_health']), 
                    0, //$formula->crf($_POST['lifestyle_risk_profile']), 
                    0, //$formula->crf($biometricBodyFat), 
                    0, //$formula->crf($biometricBloodPressure), 
                    0, //$formula->crf($biometricBloodOxygen), 
                    0, //$formula->crf($physiologicalVo2_result), 
                    0, //$formula->crf($_POST['physiological_balance']), 
                    0, //$formula->crf($physiologicalSquat), 
                    0, //$formula->crf($physiologicalSitup), 
                    $formula->crf($biometricWaist)
                );
            $test->setTotalCrf($totalCrf);
            
            $healthyLifePoints = $formula->healthyLifeYears(
                    $_POST['gender'], 
                    $_POST['lifestyle_physical_activity'], 
                    $_POST['lifestyle_alcohol'], 
                    $_POST['lifestyle_nutrition'], 
                    $_POST['lifestyle_smoking'], 
                    0, //$_POST['lifestyle_mental_health'], 
                    0, //$_POST['lifestyle_risk_profile'], 
                    0, //$biometricBodyFat, 
                    0, //$biometricBloodPressure, 
                    0, //$biometricBloodOxygen, 
                    0, //$physiologicalVo2_result, 
                    0, //$_POST['physiological_balance'], 
                    0, //$physiologicalSquat, 
                    0, //$physiologicalSitup, 
                    $biometricWaist, 
                    $totalCrf
                );
            
            
            $test->setHealthyLifePoints($healthyLifePoints);
            
            
            $estimatedHealthyLifeExpectancy = $formula->estimatedHealthyLifeExpectancy(
                    $_POST['gender'], 
                    $healthy_life_expectancy_male['expectancy'], 
                    $healthy_life_expectancy_female['expectancy'],
                    $healthyLifePoints);
            
            $test->setEstimatedHealthyLifeExpectancy($estimatedHealthyLifeExpectancy);
            
            $max_hle = $formula->maxHLE(
                    $_POST['gender'], 
                    $healthy_life_expectancy_male['expectancy'], 
                    $healthy_life_expectancy_female['expectancy'], 
                    $male_max_score['score'], 
                    $female_max_score['score']);
            
            $test->setMaxHle($max_hle);
            
            $healthyLifeScore = $formula->currentHealthyLifeScore(
                    $_POST['gender'], 
                    $estimatedHealthyLifeExpectancy, 
                    $max_hle
                );
            
            $test->setHealthyLifeScore($healthyLifeScore);
            
            
                $estimatedYearsOfHealthyLifeLeft = $formula->estimatedYearsOfHealthyLifeLeft($estimatedHealthyLifeExpectancy, $_POST['age']);
            
            $test->setEstimatedYearsOfHealthyLifeLeft($estimatedYearsOfHealthyLifeLeft);
            
                $estimatedYearsYouWillLiveWithDid = $formula->estimatedNumberOfYearsYouWillLive(
                        $_POST['gender'], 
                        $estimatedHealthyLifeExpectancy, 
                        $healthy_life_expectancy_male['total_expectancy'], 
                        $healthy_life_expectancy_female['total_expectancy']
                    );
            $test->setEstimatedYearsYouWillLiveWithDid($estimatedYearsYouWillLiveWithDid);
            
                $estimatedYearsYouWillLiveWithDidAsOpposedTo = $formula->daly(
                        $_POST['gender'], 
                        $healthy_life_expectancy_male['dalys'],
                        $healthy_life_expectancy_female['dalys']
                    );
            $test->setEstimatedYearsYouWillLiveWithDidAsOpposedTo($estimatedYearsYouWillLiveWithDidAsOpposedTo);
            
            $youCouldAddUpTo = $formula->couldAddUpTo(
                    $_POST['gender'], 
                    $_POST['age'], 
                    $healthy_life_expectancy_male['total_expectancy'], 
                    $healthy_life_expectancy_female['total_expectancy'], 
                    $max_hle, 
                    $estimatedHealthyLifeExpectancy, 
                    $male_max_score['score'], 
                    $female_max_score['score']
                );
            
            $test->setYouCouldAddUpTo($youCouldAddUpTo);
            
                $estimatedCurrentHealthyLifeAge = $formula->estimatedCurrentHealthyLifeAge(
                        $_POST['age'], 
                        $healthyLifePoints, 
                        $max_hle
                    );
            $test->setEstimatedCurrentHealthyLifeAge($estimatedCurrentHealthyLifeAge);
            
            $first_test_details = $this->getClientFirstOrRecentTestDetails($session->get('client_id'));
            
            
            if(isset($first_test_details['estimated_healthy_life_expectancy'])){
//                $yearsAdded = $formula->yearsAdded($first_test_details['estimated_current_healthy_life_age'], 
//                $estimatedCurrentHealthyLifeAge);
                $yearsAdded = $formula->yearsAdded($first_test_details['estimated_healthy_life_expectancy'], 
                                $estimatedHealthyLifeExpectancy);
            }else{
                $yearsAdded = '';
            }
            
            $test->setYearsAdded($yearsAdded);
            
            if(isset($first_test_details['biometric_waist_value'])){
                $cms_current_test = array(
                    $_POST['biometric_waist_value'],
                    0,
                    0,
                    0,
                    0,
                    0
                );

                $cms_1st_test = array(
                    $first_test_details['biometric_waist_value'],
                    0,
                    0,
                    0,
                    0,
                    0
                );
                $cmLost = $formula->cmLost($cms_1st_test, $cms_current_test);
                
                $biometricWaistValueLost = $formula->cmLost(
                        array($first_test_details['biometric_waist_value']), 
                        array($_POST['biometric_waist_value'])
                        );
                $biometricChestLost = $formula->cmLost(
                        array(0), 
                        array(0)
                        );
                $biometricThighLost = $formula->cmLost(
                        array(0), 
                        array(0)
                        );
                $biometricArmLost = $formula->cmLost(
                        array(0), 
                        array(0)
                        );
                
                $biometricAbdomenLost = $formula->cmLost(
                        array(0), 
                        array(0)
                        );
                $biometricHipLost = $formula->cmLost(
                        array(0), 
                        array(0)
                        );
            }else{
                $cmLost = '';
                $biometricWaistValueLost = '';
                $biometricChestLost = '';
                $biometricThighLost = '';
                $biometricArmLost = '';
                $biometricAbdomenLost ='';
                $biometricHipLost = '';
            }
            
            $test->setCmLost($cmLost);
            
            $test->setBiometricWaistValueLost($biometricWaistValueLost);
            $test->setBiometricChestLost($biometricChestLost);
            $test->setBiometricThighLost($biometricThighLost);
            $test->setBiometricArmLost($biometricArmLost);
            $test->setBiometricAbdomenLost($biometricAbdomenLost);
            $test->setBiometricHipLost($biometricHipLost);
            
            if(isset($first_test_details['weight'])){
                $kgLost = $formula->kgLost($first_test_details['weight'], $_POST['weight']);
            }else{
                $kgLost = '';
            }
            
            $test->setKgLost($kgLost);
            
            
            $test->setStatus('finalised');
            $test->setTestDatetime($datetime->format('Y-m-d H:i:s'));
            $test->setTestDatetimeFinalised($datetime->format('Y-m-d H:i:s'));
            $em->persist($test);
            $em->flush();
            
//            if(!isset($_POST['client_test_id'])){
//                $query = $em->createQuery("UPDATE AcmeHeadOfficeBundle:LifestyleQuestionnareClientAnswer a 
//                    SET a.client_test_id = ".$test->getLiteTestId().", 
//                        a.status='saved'  
//                    WHERE a.client_id = ".$_POST["client_id"] . "
//                        AND a.status = 'draft'");
//                $query->execute();
//            }
            
            
            
            
            $post = array(
                'fname' => $_POST['fname'],
                'lname' => $_POST['lname'],
                'email' => $_POST['email'],
                'weight' => $_POST['weight'],
                'height' => $_POST['height'],
                'age' => $_POST['age'],
                'bmi' => $_POST['bmi'],
                'lifestyle_physical_activity' => $_POST['lifestyle_physical_activity'],
                'lifestyle_smoking' => $_POST['lifestyle_smoking'],
                'lifestyle_alcohol' => $_POST['lifestyle_alcohol'],
                'lifestyle_nutrition' => $_POST['lifestyle_nutrition'],
                'biometric_waist_value' => $_POST['biometric_waist_value']

            );
            
            if(count($error) > 0){
                $this->get('session')->getFlashBag()->add(
                        'error',
                        $error
                    );
                
                
                
                if(isset($_POST['redirect'])){
                    
                    $session->set('post', $post);
                    $session->set('s_lifestyle_physical_activity_answer_complete', $s_lifestyle_physical_activity_answer_complete);
                    $session->set('s_lifestyle_smoking_answer_complete', $s_lifestyle_smoking_answer_complete);
                    $session->set('s_lifestyle_alcohol_answer_complete', $s_lifestyle_alcohol_answer_complete);
                    $session->set('s_lifestyle_nutrition_answer_complete', $s_lifestyle_nutrition_answer_complete);
                    
                    $_POST['redirect'] = trim($_POST['redirect'], '#');
                    return $this->redirect($_POST['redirect'] );
                    
                }else{
                    

                    return new Response($this->renderView('AcmeLiteTestBundle:LiteTest:create_test.html.twig',
                            array(
                                'post' => $post,
                                's_lifestyle_physical_activity_answer_complete' => $s_lifestyle_physical_activity_answer_complete,
                                's_lifestyle_smoking_answer_complete' => $s_lifestyle_smoking_answer_complete,
                                's_lifestyle_alcohol_answer_complete' => $s_lifestyle_alcohol_answer_complete,
                                's_lifestyle_nutrition_answer_complete' => $s_lifestyle_nutrition_answer_complete
                            )));
                }
                
                
                
                
            }else{
                
                
                // get program client here
                $program_client = $this->get12weekProgramClientByEmail( $_POST['email'] );
                if( count($program_client) > 0 ){
                    if($_SERVER['SERVER_NAME'] == 'localhost'){
                        $program_host="localhost";
                        $program_uname="root";
                        $program_pass="";
                        $program_database = "healthy_life_project_ohwp";

                    }else{
                        $program_host="8d06703af5d309209a093227e9976588a6710d47.rackspaceclouddb.com";
                        $program_uname="hlproot";
                        $program_pass="_jWBqa)67?BJq(j-";
                        $program_database = "hlp12weekProgram";
                    }
                    $program_connection=mysqli_connect($program_host,$program_uname,$program_pass, $program_database) or die("Database Connection Failed");
                    
                    mysqli_query($program_connection,
                    "INSERT INTO tbl_client_test 
                        (client_id, 
                        test_datetime, 
                        test_datetime_finalised,
                        age,
                        healthy_life_expectancy_male,
                        healthy_life_expectancy_female,
                        healthy_life_expectancy_total_male,
                        healthy_life_expectancy_total_female,
                        healthy_life_expectancy_dalys_male,
                        healthy_life_expectancy_dalys_female,
                        healthy_life_max_score_male,
                        healthy_life_max_score_female,
                        healthy_life_worst_score_male,
                        healthy_life_worst_score_female,
                        height,
                        weight,
                        bmi,
                        lifestyle_physical_activity_q1,
                        lifestyle_physical_activity_q2,
                        lifestyle_physical_activity,
                        lifestyle_smoking,
                        lifestyle_alcohol_q1,
                        lifestyle_alcohol_q2,
                        lifestyle_alcohol,
                        lifestyle_nutrition,
                        lifestyle_mental_health,
                        lifestyle_risk_profile,
                        physiological_vo2_test_type,
                        physiological_vo2_test_value1,
                        physiological_vo2_test_value2,
                        physiological_vo2,
                        physiological_balance_type,
                        physiological_balance_value,
                        physiological_balance,
                        physiological_squat_value,
                        physiological_squat,
                        physiological_situp_value,
                        physiological_situp,
                        biometric_waist_value,
                        biometric_waist_value_lost,
                        biometric_waist,
                        biometric_chest_lost,
                        biometric_chest,
                        biometric_abdomen,
                        biometric_abdomen_lost,
                        biometric_hip,
                        biometric_hip_lost,
                        biometric_thigh,
                        biometric_thigh_lost,
                        biometric_arm,
                        biometric_arm_lost,
                        biometric_body_fat_value,
                        biometric_body_fat,
                        biometric_blood_pressure_systolic,
                        biometric_blood_pressure_diastolic,
                        biometric_blood_pressure,
                        biometric_blood_oxygen_value,
                        biometric_blood_oxygen,
                        total_crf,
                        healthy_life_points,
                        estimated_healthy_life_expectancy,
                        max_hle,
                        healthy_life_score,
                        estimated_years_of_healthy_life_left,
                        estimated_years_you_will_live_with_did,
                        estimated_years_you_will_live_with_did_as_opposed_to,
                        you_could_add_up_to,
                        estimated_current_healthy_life_age,
                        years_added,
                        cm_lost,
                        kg_lost,
                        `status`) 
                        
                        VALUES
                        
                        (". $program_client['client_id'] .", 
                        '". $test->getTestDatetime() ."', 
                        '". $test->getTestDatetimeFinalised() ."',
                        '". $test->getAge() ."',
                        '". $test->getHealthyLifeExpectancyMale() ."',
                        '". $test->getHealthyLifeExpectancyFeMale() ."',
                        '". $test->getHealthyLifeExpectancyTotalMale() ."',
                        '". $test->getHealthyLifeExpectancyTotalFemale() ."',
                        '". $test->getHealthyLifeExpectancyDalysMale() ."',
                        '". $test->getHealthyLifeExpectancyDalysFemale() ."',
                        '". $test->getHealthyLifeMaxScoreMale() ."',
                        '". $test->getHealthyLifeMaxScoreFemale() ."',
                        '". $test->getHealthyLifeWorstScoreMale() ."',
                        '". $test->getHealthyLifeWorstScoreFemale() ."',
                        '". $test->getHeight() ."',
                        '". $test->getWeight() ."',
                        '". $test->getBmi() ."',
                        '". $test->getLifestylePhysicalActivityQ1() ."',
                        '". $test->getLifestylePhysicalActivityQ2() ."',
                        '". $test->getLifestylePhysicalActivity() ."',
                        '". $test->getLifestyleSmoking() ."',
                        '". $test->getLifestyleAlcoholQ1() ."',
                        '". $test->getLifestyleAlcoholQ2() ."',
                        '". $test->getLifestyleAlcohol() ."',
                        '". $test->getLifestyleNutrition() ."',
                        '". $test->getLifestyleMentalHealth() ."',
                        '". $test->getLifestyleRiskProfile() ."',
                        '". $test->getPhysiologicalVo2TestType() ."',
                        '". $test->getPhysiologicalVo2TestValue1() ."',
                        '". $test->getPhysiologicalVo2TestValue2() ."',
                        '". $test->getPhysiologicalVo2() ."',
                        '". $test->getPhysiologicalBalanceType() ."',
                        '". $test->getPhysiologicalBalanceValue() ."',
                        '". $test->getPhysiologicalBalance() ."',
                        '". $test->getPhysiologicalSquatValue() ."',
                        '". $test->getPhysiologicalSquat() ."',
                        '". $test->getPhysiologicalSitupValue() ."',
                        '". $test->getPhysiologicalSitup() ."',
                        '". $test->getBiometricWaistValue() ."',
                        '". $test->getBiometricWaistValueLost() ."',
                        '". $test->getBiometricWaist() ."',
                        '". $test->getBiometricChestLost() ."',
                        '". $test->getBiometricChest() ."',
                        '". $test->getBiometricAbdomen() ."',
                        '". $test->getBiometricAbdomenLost() ."',
                        '". $test->getBiometricHip() ."',
                        '". $test->getBiometricHipLost() ."',
                        '". $test->getBiometricThigh() ."',
                        '". $test->getBiometricThighLost() ."',
                        '". $test->getBiometricArm() ."',
                        '". $test->getBiometricArmLost() ."',
                        '". $test->getBiometricBodyFatValue() ."',
                        '". $test->getBiometricBodyFat() ."',
                        '". $test->getBiometricBloodPressureSystolic() ."',
                        '". $test->getBiometricBloodPressureDiastolic() ."',
                        '". $test->getBiometricBloodPressure() ."',
                        '". $test->getBiometricBloodOxygenValue() ."',
                        '". $test->getBiometricBloodOxygen() ."',
                        '". $test->getTotalCrf() ."',
                        '". $test->getHealthyLifePoints() ."',
                        '". $test->getEstimatedHealthyLifeExpectancy() ."',
                        '". $test->getMaxHle() ."',
                        '". $test->getHealthyLifeScore() ."',
                        '". $test->getEstimatedYearsOfHealthyLifeLeft() ."',
                        '". $test->getEstimatedYearsYouWillLiveWithDid() ."',
                        '". $test->getEstimatedYearsYouWillLiveWithDidAsOpposedTo() ."',
                        '". $test->getYouCouldAddUpTo() ."',
                        '". $test->getEstimatedCurrentHealthyLifeAge() ."',
                        '". $test->getYearsAdded() ."',
                        '". $test->getCmLost() ."',
                        '". $test->getKgLost() ."',
                        'finalised')");
                    
                    mysqli_query($program_connection,
                        "UPDATE tbl_client SET s_opening_message_done = 1, s_lite_test_complete = 1 WHERE client_id=". $program_client['client_id']);
                }
                
                
                
                
                $from = $this->container->getParameter('site_email_address');
                $subject = $_POST['fname'] . ' ' . $_POST['lname'] . ' -  Lite Test Results PDF';
                $body = $this->renderView('AcmeLiteTestBundle:LiteTest:email_test_content_to_person.html.twig',
                                array(
                                    'site_url' => $root_dir,
                                    'name' => $_POST['fname'],
                                    'test_date' => $datetime->format('Y-m-d H:i:s')
                                ));
                
                $body_to_admin = $this->renderView('AcmeLiteTestBundle:LiteTest:email_test_content_to_admin.html.twig',
                                array(
                                    'name' => $_POST['fname'] . ' ' . $_POST['lname'],
                                    'fname' => $_POST['fname'],
                                    'lname' => $_POST['lname'],
                                    'weight' => $_POST['weight'],
                                    'height' => $_POST['height'],
                                    'age' => $_POST['age'],
                                    'biometric_waist_value' => $_POST['biometric_waist_value'],
                                    'gender' => $_POST['gender'],
                                    'test_date' => $datetime->format('Y-m-d H:i:s')
                                ));

//                $attachments = array();
//                $attachments[] = $file;

                $recipients = array();
                
                if($mod->isEmailValid(trim($_POST['email']))){
                    $recipients[ $_POST['email'] ] = $_POST['fname'] . ' ' . $_POST['lname'];
                }

                if(count($recipients) > 0){
                    //$this->sendEmail($recipients, '', $from, $subject, $body, $attachments);
                    
                    $ref = array(
                        'lite_test_id' => $test->getLiteTestId(),
                        'type'=> 'test'
                        );
                    $emailQueue = new EmailQueue();
                    $emailQueue->setSender( $from );
                    $emailQueue->setSubject( $subject );
                    $emailQueue->setBody( $body );
                    $emailQueue->setRecipients( json_encode($recipients) );
                    $emailQueue->setReference( json_encode($ref) );
                    $emailQueue->setSSent(0);
                    $emailQueue->setSTest(1);
                    $em->persist($emailQueue);
                    $em->flush();
                    
                    
                    // copy to admin
                    $admins = $this->getHeadOfficeAdminUsers('active');
                    $recipients_admin = array();
                    for($i=0; $i<count($admins); $i++){
                        if($admins[$i]['receive_litetests'] == 1){
                            $recipients_admin[ $admins[$i]['email'] ] =  $admins[$i]['fname'] . ' ' . $admins[$i]['lname'];
                        }
                    }
                    if(count($recipients_admin) > 0){
                        $emailQueue = new EmailQueue();
                        $emailQueue->setSender( $from );
                        $emailQueue->setSubject( $subject );
                        $emailQueue->setBody( $body_to_admin );
                        $emailQueue->setRecipients( json_encode($recipients_admin) );
                        $emailQueue->setReference( json_encode($ref) );
                        $emailQueue->setSSent(0);
                        $emailQueue->setSTest(1);
                        $em->persist($emailQueue);
                        $em->flush();
                    }
//                    

//                    
//                    for($i=0; $i<count($admins); $i++){
//                        if($admins[$i]['receive_litetests'] == 1){
//                            $recipients_admin[ $admins[$i]['email'] ] =  $admins[$i]['fname'] . ' ' . $admins[$i]['lname'];
//                        }
//                    }
//                    if(count($recipients_admin) > 0){
//                        $this->sendEmail($recipients_admin, '', $from, $subject, $body_to_admin, $attachments);
//                    }
                }else{
                    unlink($file);
                }

                
                
                $session->set('litetest-success', '1');
                $session->set('litetest_id',$test->getLiteTestId()); 
                setcookie('litetest_id', $test->getLiteTestId(), time() + (86400 * 30), "/"); 
                $this->get('session')->getFlashBag()->add(
                            'success',
                            'Your Lite Test has been submitted. Thank you!'
                        );
                
                $session->set('litetest-physical-activity', '');
                $session->set('litetest-alcohol', '');
                $session->set('litetest-nutrition', '');
                $session->set('litetest-smoking', '');
                
                $session->set('post', '');
                $session->set('s_lifestyle_physical_activity_answer_complete', '');
                $session->set('s_lifestyle_smoking_answer_complete', '');
                $session->set('s_lifestyle_alcohol_answer_complete', '');
                $session->set('s_lifestyle_nutrition_answer_complete', '');
				
                
                $em->getConnection()->commit(); 
                
                if(isset($_POST['redirect'])){
                    $_POST['redirect'] = trim($_POST['redirect'], '#');
                    return $this->redirect($_POST['redirect'] );
                }else{
                    return $this->redirect($this->generateUrl('acme_lite_test_create_test') );
                }
                
            }
            
            
            
        }else{
        
            
            if($session->get('litetest-success') == 1){
                
                $session->set('litetest-success', 0);
                return new Response($this->renderView('AcmeLiteTestBundle:LiteTest:test_confirmation.html.twig',array('litetest_id'=>$session->get('litetest_id'))));
                
                    
                
            }else{
                $post = array();
            
                return new Response($this->renderView('AcmeLiteTestBundle:LiteTest:create_test.html.twig',
                    array(
                        'post' => $post,
                        's_lifestyle_physical_activity_answer_complete' => $this->sLiteLifestyleTestQuestionnaireAnswersDone('physical-activity'),
                        's_lifestyle_smoking_answer_complete' => $this->sLiteLifestyleTestQuestionnaireAnswersDone('smoking'),
                        's_lifestyle_alcohol_answer_complete' => $this->sLiteLifestyleTestQuestionnaireAnswersDone('alcohol'),
                        's_lifestyle_nutrition_answer_complete' => $this->sLiteLifestyleTestQuestionnaireAnswersDone('nutrition')
                    )));
            }
        }
    }
    
    
    
    // GET LIFESTYLE QUESTIONNAIRE TEMPLATE
    public function testQuestionnaireTemplateAction()
    {
        $session = $this->getRequest()->getSession();
        
        
        if(!isset($_POST['code'])){
            //return $this->redirect($this->generateUrl('acme_club_login'));
            return new Response("invalid parameter");
        }
        
        $_POST['code'] = trim($_POST['code']);
        
        $lifestyle_codes = array("physical_activity", "smoking", "alcohol", "nutrition", "mental_health", "risk_profile");

        if (!in_array($_POST['code'], $lifestyle_codes)){
          return new Response("invalid parameter");
        }
        
        //$smoking_answer = ($_POST['code'] == 'smoking') ? $this->getSpecificTestQuestionnaireClientAnswer(3 /*smoking*/, $_POST['club_client_id'], $_POST['client_test_id']) : '';
        
        return new Response($this->renderView(
            'AcmeLiteTestBundle:LiteTest:test_questionnaire_template_'. $_POST['code'] .'.html.twig',
                array(
                    'gender' => $_POST['gender'],
                    'questions' => $this->getTestQuestionnaire($_POST['code'], NULL, NULL),
                    'options' => $this->getTestQuestionnaireOptionsByCode($_POST['code']),
                    'smoking_answer' => ($_POST['code'] == 'smoking') ? $this->getSpecificTestQuestionnaireClientAnswer(3 /*smoking*/, NULL, NULL) : ''
                )
            ));
        
    }
    
    // SUBMIT LIFESTYLE ANSWER
    public function lifeStyleTestAnswerAction(){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $formula = new Model\TestFormulas();
        
        $error = array();
        
        if($_POST['answer_type'] == 'risk-profile'){
            $error_count = 0;
            foreach ($_POST['answer'] as $lifestyle_question_id => $answer) {
                if(trim($answer) != '' && ($answer > 7 || $answer < 1)){
                    $error_count +=1;
                }
            }
            if( $error_count > 0){
                $error[] = 'Make sure that your answer(s) must not exceed to 7 (highly likely) and not below 1  (highly unlikely).';
            }
        }
        
        if(count($error) == 0 ){
			
            if($_POST['answer_type'] == 'physical-activity' ||
                $_POST['answer_type'] == 'alcohol' || 
                $_POST['answer_type'] == 'mental-health' ||
                $_POST['answer_type'] == 'risk-profile' ||
                $_POST['answer_type'] == 'nutrition' ){
				$lq_ans = array();
                if(isset($_POST['answer'])){
                    foreach ($_POST['answer'] as $lifestyle_question_id => $answer) {
						$lq_ans[$lifestyle_question_id] = array(
								'answer'=> $answer
							);
						
                    }
					$session->set('litetest-'.$_POST['answer_type'], $lq_ans);
					
                }else{
				
					$session->set('litetest-'.$_POST['answer_type'], array() );
				}
				
            }elseif($_POST['answer_type'] == 'smoking'){
				$lq_ans = array();
                $lq_ans[3] = array(
								'answer'=> $_POST['answer']
							);
							
				$session->set('litetest-smoking', $lq_ans);

            }

			
            $s_complete = $this->sLiteLifestyleTestQuestionnaireAnswersDone($_POST['answer_type']);
            
            $response = array(
                'result' => 'success',
                'code' => $_POST['answer_type'],
                's_complete' => $s_complete
            );
        }else{
            $response = array(
                'result' => 'error',
                'message' => $error[0]
            );
        }
        
        
        return new Response(json_encode($response));
    }
	
    
	public function sLiteLifestyleTestQuestionnaireAnswersDone($code){
			$session = $this->getRequest()->getSession();
                        
            $codesearch = str_replace('-', '_', $code);
            $questionnaire = $this->getTestQuestionnaire($codesearch);
            
            $questions = count($questionnaire);
            $answers = 0;
            for($i=0; $i<count( $questionnaire ); $i++){
				//$item = $this->getSpecificTestQuestionnaireClientAnswer($questionnaire[$i]['lq_id'], $club_client_id, $client_test_id);
                $code_session = $session->get('litetest-'.$code);
                $answers += (isset($code_session[ $questionnaire[$i]['lq_id'] ]['answer']) && $code_session[ $questionnaire[$i]['lq_id'] ]['answer'] != '') ? 1 : 0;
                
                //$answers += ($code_session[ $questionnaire[$i]['lq_id'] ]['answer'] != '') ? 1 : 0;
            }
            return ($questions == $answers) ? true : false;
    }
    
    
    
    
//==============================================================================
// GET TEST RESULTS (AJAX)
//==============================================================================
    public function getBmiResultAction(){
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['weight'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['height'])){
            return new Response('Invalid parameter input!');
        }
        $_POST['weight'] = intval($_POST['weight']);
        $_POST['height'] = intval($_POST['height']);
        
        $response = $formula->bMI($_POST['height'], $_POST['weight']);
        return new Response($response);
        
    }
    
    public function getPhysicalActivityResultAction(){
        $session = $this->getRequest()->getSession();
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['moderate_vigorous_pa'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['muscle_strenthening_pa'])){
            return new Response('Invalid parameter input!');
        }
        
        
        $_POST['moderate_vigorous_pa'] = intval($_POST['moderate_vigorous_pa']);
        $_POST['muscle_strenthening_pa'] = intval($_POST['muscle_strenthening_pa']);
        
        $moderate_vigorous_pa = $formula->moderateOrVigorousIntensityPhysicalActivity($_POST['moderate_vigorous_pa']);
        $muscle_strenthening_pa = $formula->muscleStrentheningPhysicalActivity($_POST['muscle_strenthening_pa']);
        
        $response = $formula->physicalActivity($moderate_vigorous_pa, $muscle_strenthening_pa, TRUE);
        
        
        
        $session->set('lifestyle-physical-activity', $response);
        return new Response($response);
        
    }
    
    public function getSmokingResultAction(){
        $session = $this->getRequest()->getSession();
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['gender'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['answer'])){
            return new Response('Invalid parameter input!');
        }
        
        $_POST['answer'] = intval($_POST['answer']);
        
        $response = $formula->smokingHabits($_POST['gender'], $_POST['answer'], TRUE);
        
        $session->set('lifestyle-smoking', $response);
        
        return new Response($response);
    }
    
    public function getAlcoholConsumptionResultAction(){
        $session = $this->getRequest()->getSession();
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['gender'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['per_week_value'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['one_sitting_value'])){
            return new Response('Invalid parameter input!');
        }
        
        $_POST['per_week_value'] = intval($_POST['per_week_value']);
        $_POST['one_sitting_value'] = intval($_POST['one_sitting_value']);
        
        $alcohol_consumption_per_week = $formula->alcoholConsumptionPerWeekResult($_POST['gender'], $_POST['per_week_value']);
        $alcohol_consumption_in_one_sitting = $formula->alcoholConsumptionInOneSittingResult($_POST['one_sitting_value']);
        
        $response = $formula->alcoholConsumptionTotal($alcohol_consumption_per_week, $alcohol_consumption_in_one_sitting, TRUE);
        
        // UPDATE tbl_client_test
        $session->set('lifestyle-alcohol', $response);
        
        return new Response($response);
    }
    

    public function getNutritionResultAction(){
        $session = $this->getRequest()->getSession();
        $formula = new Model\TestFormulas();
        
        if(isset($_POST['answer'])){
            $number_of_yes = 0;
            foreach ($_POST['answer'] as $lifestyle_question_id => $answer) {
                $number_of_yes += intval($answer);
            }

            $response = $formula->nutrition($number_of_yes, TRUE);

        }else{
            $response = '';
        }
        $session->set('lifestyle-nutrition', $response);
        
        return new Response($response);
    }
    
    
    public function getMentalHealthResultAction(){
        $session = $this->getRequest()->getSession();
        $formula = new Model\TestFormulas();
        
        if(isset($_POST['answer'])){
            $mental_health_value = 0;
            foreach ($_POST['answer'] as $lifestyle_question_id => $answer) {
                if($answer != ''){
                    $mental_health_value += intval($answer);
                }
            }
            if($mental_health_value > 0){
                $response = $formula->mentalHealth($mental_health_value);

            }else{
                $response = '';
            }
        }else{
            $response = '';
        }
        
        // UPDATE tbl_client_test
        if($_POST['client_test_id'] != ''){
            $em = $this->getDoctrine()->getManager();
            $test = $em->getRepository('AcmeHeadOfficeBundle:ClientTest')
                        ->findOneBy(array(
                            'client_test_id' => $_POST['client_test_id'],
                        ));
            if($response != ''){
                $test->setLifestyleMentalHealth($response);
            }else{
                $test->setLifestyleMentalHealth(NULL);
            }
            $em->persist($test);
            $em->flush();
        }
        
        $session->set('lifestyle-mental-health', $response);
        
        return new Response($response);
    }
    
    
    public function getRiskProfileResultAction(){
        $session = $this->getRequest()->getSession();
        $formula = new Model\TestFormulas();
        
        if(isset($_POST['answer'])){
            $risk_profile_value = 0;
            foreach ($_POST['answer'] as $lifestyle_question_id => $answer) {
                if($answer != ''){
                    $risk_profile_value += intval($answer);
                }
            }
            if($risk_profile_value > 0){
                //$response = $formula->mentalHealth($mental_health_value);
                $response = $formula->riskProfile($risk_profile_value);

            }else{
                $response = '';
            }
        }else{
            $response = '';
        }
        
        // UPDATE tbl_client_test
        if($_POST['client_test_id'] != ''){
            $em = $this->getDoctrine()->getManager();
            $test = $em->getRepository('AcmeHeadOfficeBundle:ClientTest')
                        ->findOneBy(array(
                            'client_test_id' => $_POST['client_test_id'],
                        ));
            if($response != ''){
                $test->setLifestyleRiskProfile($response);
            }else{
                $test->setLifestyleRiskProfile(NULL);
            }
            $em->persist($test);
            $em->flush();
        }
        
        $session->set('lifestyle-risk-profile',$response);
        
        return new Response($response);
    }
    
    public function getVo2TestAction(){
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['gender'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['weight'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['age'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['test_type'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['value1'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['value2'])){
            return new Response('Invalid parameter input!');
        }
        
        $_POST['weight'] = intval($_POST['weight']);
        $_POST['age'] = intval($_POST['age']);
        $_POST['value1'] = intval($_POST['value1']);
        $_POST['value2'] = intval($_POST['value2']);
        
        if($_POST['test_type'] == 'bike'){
            $score = $formula->vo2BikeTest($_POST['gender'], $_POST['weight'], $_POST['age'], $_POST['value1']);
            
        }elseif($_POST['test_type'] == 'resting-hr'){
            
            $score = $formula->vo2RestingHrTest($_POST['age'], $_POST['value1']);
            
        }elseif($_POST['test_type'] == 'treadmill'){
            
            $score = $formula->vo2TreadmillTest($_POST['gender'], $_POST['weight'], $_POST['value1'], $_POST['value2']);
        
        }elseif($_POST['test_type'] == 'beep'){
            $score = $formula->vo2BeepTest($_POST['value1'], $_POST['value2']);
        }else{
            
            $score = $formula->vo2DirectEntry($_POST['value1']);
        }
        
        $result = $formula->vo2($_POST['gender'], $_POST['age'], $score);
        
        $response = array(
            'score'=>$score,
            'result'=>$result
            );
        
        return new Response(json_encode($response));
        
    }
    
    public function getBalanceTestAction(){
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['gender'])){
            return new Response('Invalid parameter input!');
        }
        if(!isset($_POST['value'])){
            return new Response('Invalid parameter input!');
        }
        
        $_POST['value'] = intval($_POST['value']);
        
        $response = $formula->balance($_POST['test_type'], $_POST['gender'], $_POST['value']);
        
        
        return new Response($response);
    }
    
    // removelater
    public function getWaistTestAction(){
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['gender'])){
            return new Response('Invalid parameter input!');
        }
        
        $_POST['value'] = intval($_POST['value']);
        
        $response = $formula->waist($_POST['gender'], $_POST['value']);
        
        
        return new Response($response);
    }
    
    // removelater
    public function getSquatTestAction(){
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['gender'])){
            return new Response('Invalid parameter input!');
        }
        
        $_POST['value'] = intval($_POST['value']);
        
        $response = $formula->waist($_POST['gender'], $_POST['value']);
        
        
        return new Response($response);
    }
    
    
    public function createShareImageAction($slug){
        
        $session = $this->getRequest()->getSession();
        
        header('Content-type: image/jpeg');
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        //$path = ($_SERVER['HTTP_HOST'] == 'localhost') ? $mod->siteURL()."/airport/web/dev/uploads/images" .$session->get("image_library_parent_dir") : $mod->siteURL()."/web/dev/uploads/images" . $session->get("image_library_parent_dir");
        // Create Image From Existing File
        $jpg_image = imagecreatefromjpeg($root_dir.'/img/fb_share_new_empty.jpg');
        $test_details = $this->getLiteTestDetailsById($slug);
        // Allocate A Color For The Text
        $white = imagecolorallocate($jpg_image, 108, 70, 149);

        // Set Path to Font File
        $font_path = $root_dir.'/fonts/Gotham-Light.otf';

        // Set Text to Be Printed On Image
        $text1 = round($test_details['healthy_life_score'] *100,1).'%';
        $text2 = round($test_details['estimated_current_healthy_life_age']);
        $text3 = round($test_details['you_could_add_up_to']);
        // Print Text On Image
        imagettftext($jpg_image, 55, 0, 160, 340, $white, $font_path, $text1);
        imagettftext($jpg_image, 55, 0, 560, 340, $white, $font_path, $text2);
        imagettftext($jpg_image, 55, 0, 900, 340, $white, $font_path, $text3);
        // Send Image to Browser
        imagejpeg($jpg_image);

        // Clear Memory
        imagedestroy($jpg_image);


    }
    
    public function shareResultAction($slug){
       
        return $this->render('AcmeLiteTestBundle:LiteTest:test_share.html.twig',array('litetest_id'=>$slug)); 

    }
    
    public function confirmationAction(){
       $session = $this->getRequest()->getSession(); 
       return $this->render('AcmeLiteTestBundle:LiteTest:test_confirmation.html.twig',array('litetest_id'=>$session->get('litetest_id'))); 

    }
}
