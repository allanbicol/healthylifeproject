<?php

namespace Acme\LiteTestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Acme\HeadOfficeBundle\Model;

use Acme\HeadOfficeBundle\Entity\ClientTest;
use Acme\HeadOfficeBundle\Entity\LifestyleQuestionnareClientAnswer;

class LiteTestController extends \Acme\HeadOfficeBundle\Controller\GlobalController
{
    
    
    // CREATE TEST
    public function createTestAction()
    {
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $formula = new Model\TestFormulas();
        $mod = new Model\GlobalModel();
        
        $root_dir = dirname($this->get('kernel')->getRootDir()) .'/web';
        require_once($root_dir.'/resources/pdfc/html2pdf.class.php');
        
        if(count($_POST) > 0){
            $error = array();
            $test_details = array();
            

            $s_lifestyle_physical_activity_answer_complete = $this->sLiteLifestyleTestQuestionnaireAnswersDone('physical-activity');
            $s_lifestyle_smoking_answer_complete = $this->sLiteLifestyleTestQuestionnaireAnswersDone('smoking');
            $s_lifestyle_alcohol_answer_complete = $this->sLiteLifestyleTestQuestionnaireAnswersDone('alcohol');
            $s_lifestyle_nutrition_answer_complete = $this->sLiteLifestyleTestQuestionnaireAnswersDone('nutrition');

// VALIDATE INPUTS
            if(trim($_POST['fname']) == ''){
                $error[] = array(
                                'message' => "Please enter your first name."
                        );
            }

            if(trim($_POST['lname']) == ''){
                $error[] = array(
                                'message' => "Please enter your last name."
                        );
            }

            if(trim($_POST['age']) == '' || trim($_POST['age']) == 0){
                $error[] = array(
                                'message' => "Please enter valid age (year)."
                        );
            }

            if(trim($_POST['height']) == '' || trim($_POST['height']) == 0){
                $error[] = array(
                                'message' => "Please enter valid height (cm)."
                        );
            }

            if(trim($_POST['weight']) == '' || trim($_POST['weight']) == 0){
                $error[] = array(
                                'message' => "Please enter valid weight (kg)."
                        );
            }

            if(!$s_lifestyle_physical_activity_answer_complete){
                $error[] = array(
                                'message' => "Please make sure to complete Physical Activity test."
                        );
            }

            if(!$s_lifestyle_smoking_answer_complete){
                $error[] = array(
                                'message' => "Please make sure to complete Smoking Habit test."
                        );
            }

            if(!$s_lifestyle_alcohol_answer_complete){
                $error[] = array(
                                'message' => "Please make sure to complete Alcohol Consumption test."
                        );
            }

            if(!$s_lifestyle_nutrition_answer_complete){
                $error[] = array(
                                'message' => "Please make sure to complete Nutrition test."
                        );
            }

            if(trim($_POST['biometric_waist_value']) == ''){
                $error[] = array(
                                'message' => "Waist must not be blank."
                        );
            }
            
            $_POST['weight'] = intval($_POST['weight']);
            $_POST['height'] = intval($_POST['height']);
            $_POST['age'] = intval($_POST['age']);
            $_POST['biometric_waist_value'] = intval($_POST['biometric_waist_value']);
            
	    $test_details['test_datetime_finalised'] = $datetime->format("Y-m-d H:i:s");
			
            $healthy_life_expectancy_male = $this->getHealthyLifeExpectancies('male');
            $healthy_life_expectancy_female = $this->getHealthyLifeExpectancies('female');
            $male_max_score = $this->getHealthyLifeMaxWorstScore('male', 'max');
            $female_max_score = $this->getHealthyLifeMaxWorstScore('female', 'max');
            $male_worst_score = $this->getHealthyLifeMaxWorstScore('male', 'worst');
            $female_worst_score = $this->getHealthyLifeMaxWorstScore('female', 'worst');
			
			
            $test_details['healthy_life_expectancy_dalys_female'] = $healthy_life_expectancy_female['dalys'];
            $test_details['healthy_life_expectancy_dalys_male'] = $healthy_life_expectancy_male['dalys'];
            $test_details['healthy_life_expectancy_female'] = $healthy_life_expectancy_female['expectancy'];
            $test_details['healthy_life_expectancy_male'] = $healthy_life_expectancy_male['expectancy'];
            $test_details['healthy_life_expectancy_total_female'] = $healthy_life_expectancy_female['total_expectancy'];
            $test_details['healthy_life_expectancy_total_male'] = $healthy_life_expectancy_male['total_expectancy'];
            $test_details['healthy_life_max_score_female'] = $female_max_score['score'];
            $test_details['healthy_life_max_score_male'] = $male_max_score['score'];
            $test_details['healthy_life_worst_score_female'] = $female_worst_score['score'];
            $test_details['healthy_life_worst_score_male'] = $male_worst_score['score'];
			
            $test_details['biometric_waist_value'] = $_POST['biometric_waist_value'];
            
                $bmi = $formula->bMI($_POST['height'], $_POST['weight']);
            $test_details['bmi'] = $bmi;
            $test_details['lifestyle_physical_activity'] = $_POST['lifestyle_physical_activity'];
            $test_details['lifestyle_smoking'] = $_POST['lifestyle_smoking'];
            $test_details['lifestyle_alcohol'] = $_POST['lifestyle_alcohol'];
            $test_details['lifestyle_nutrition'] = $_POST['lifestyle_nutrition'];
			
                $biometricWaist = $formula->waist($_POST['gender'], $_POST['biometric_waist_value'], TRUE);
            $test_details['biometric_waist'] = $biometricWaist;
			
            $totalCrf = $formula->totalCRF(
                    $formula->crf($_POST['lifestyle_physical_activity']), 
                    $formula->crf($_POST['lifestyle_smoking']), 
                    $formula->crf($_POST['lifestyle_alcohol']), 
                    $formula->crf($_POST['lifestyle_nutrition']), 
                    0, //$formula->crf($_POST['lifestyle_mental_health']), 
                    0, //$formula->crf($_POST['lifestyle_risk_profile']), 
                    0, //$formula->crf($biometricBodyFat), 
                    0, //$formula->crf($biometricBloodPressure), 
                    0, //$formula->crf($biometricBloodOxygen), 
                    0, //$formula->crf($physiologicalVo2_result), 
                    0, //$formula->crf($_POST['physiological_balance']), 
                    0, //$formula->crf($physiologicalSquat), 
                    0, //$formula->crf($physiologicalSitup), 
                    $formula->crf($biometricWaist)
                );
            
                $test_details['total_crf'] = $totalCrf;
            
            $healthyLifePoints = $formula->healthyLifeYears(
                    $_POST['gender'], 
                    $_POST['lifestyle_physical_activity'], 
                    $_POST['lifestyle_alcohol'], 
                    $_POST['lifestyle_nutrition'], 
                    $_POST['lifestyle_smoking'], 
                    0, //$_POST['lifestyle_mental_health'], 
                    0, //$_POST['lifestyle_risk_profile'], 
                    0, //$biometricBodyFat, 
                    0, //$biometricBloodPressure, 
                    0, //$biometricBloodOxygen, 
                    0, //$physiologicalVo2_result, 
                    0, //$_POST['physiological_balance'], 
                    0, //$physiologicalSquat, 
                    0, //$physiologicalSitup, 
                    $biometricWaist, 
                    $totalCrf
                );
            
                $test_details['healthy_life_points'] = $healthyLifePoints;
            
                $estimatedHealthyLifeExpectancy = $formula->estimatedHealthyLifeExpectancy(
                            $_POST['gender'], 
                            $healthy_life_expectancy_male['expectancy'], 
                            $healthy_life_expectancy_female['expectancy'],
                            $healthyLifePoints
                        );
            
                $test_details['estimated_healthy_life_expectancy'] = $estimatedHealthyLifeExpectancy;
            
                $max_hle = $formula->maxHLE(
                    $_POST['gender'], 
                    $healthy_life_expectancy_male['expectancy'], 
                    $healthy_life_expectancy_female['expectancy'], 
                    $male_max_score['score'], 
                    $female_max_score['score']);
					
                $test_details['max_hle'] = $max_hle;
            
                $healthyLifeScore = $formula->currentHealthyLifeScore(
                    $_POST['gender'], 
                    $estimatedHealthyLifeExpectancy, 
                    $max_hle
                );
                $test_details['healthy_life_score'] = $healthyLifeScore;
            
                $estimatedYearsOfHealthyLifeLeft = $formula->estimatedYearsOfHealthyLifeLeft($estimatedHealthyLifeExpectancy, $_POST['age']);
                $test_details['estimated_years_of_healthy_life_left'] = $estimatedYearsOfHealthyLifeLeft;
            
                        $estimatedYearsYouWillLiveWithDid = $formula->estimatedNumberOfYearsYouWillLive(
                                $_POST['gender'], 
                                $estimatedHealthyLifeExpectancy, 
                                $healthy_life_expectancy_male['total_expectancy'], 
                                $healthy_life_expectancy_female['total_expectancy']
                        );
                $test_details['estimated_years_you_will_live_with_did'] = $estimatedYearsYouWillLiveWithDid;

                        $estimatedYearsYouWillLiveWithDidAsOpposedTo = $formula->daly(
                                $_POST['gender'], 
                                $healthy_life_expectancy_male['dalys'],
                                $healthy_life_expectancy_female['dalys']
                        );
                $test_details['estimated_years_you_will_live_with_did_as_opposed_to'] = $estimatedYearsYouWillLiveWithDidAsOpposedTo;

                        $youCouldAddUpTo = $formula->couldAddUpTo(
                    $_POST['gender'], 
                    $_POST['age'], 
                    $healthy_life_expectancy_male['total_expectancy'], 
                    $healthy_life_expectancy_female['total_expectancy'], 
                    $max_hle, 
                    $estimatedHealthyLifeExpectancy, 
                    $male_max_score['score'], 
                    $female_max_score['score']
                );
            $test_details['you_could_add_up_to'] = $youCouldAddUpTo;

                    $estimatedCurrentHealthyLifeAge = $formula->estimatedCurrentHealthyLifeAge(
                            $_POST['age'], 
                            $healthyLifePoints, 
                            $max_hle
                    );
            $test_details['estimated_current_healthy_life_age'] = $estimatedCurrentHealthyLifeAge;
                    $yearsAdded = $formula->yearsAdded($_POST['age'], $estimatedCurrentHealthyLifeAge);
            $test_details['years_added'] = $yearsAdded;
            
            
            
            $test_details['lifestyle_physical_activity_result'] = $formula->physicalActivityResult($test_details['lifestyle_physical_activity']);
            $test_details['lifestyle_smoking_result'] = $formula->smokingHabitsResult($test_details['lifestyle_smoking']);
            $test_details['lifestyle_alcohol_result'] = $formula->alcoholConsumptionResult($test_details['lifestyle_alcohol']);
            $test_details['lifestyle_nutrition_result'] = $formula->nutritionResult($test_details['lifestyle_nutrition']);
            
            $test_details['biometric_waist_result'] = $formula->waistResult($test_details['biometric_waist']);
            
            $test_details['healthy_life_score_outcome'] = $formula->outcomes($test_details['healthy_life_score']);
            
            
            if(count($error) > 0){
                $this->get('session')->getFlashBag()->add(
                        'error',
                        $error
                    );
                
                $post = array(
                        'fname' => $_POST['fname'],
                        'lname' => $_POST['lname'],
                        'email' => $_POST['email'],
                        'weight' => $_POST['weight'],
                        'height' => $_POST['height'],
                        'age' => $_POST['age'],
                        'bmi' => $_POST['bmi'],
                        'lifestyle_physical_activity' => $_POST['lifestyle_physical_activity'],
                        'lifestyle_smoking' => $_POST['lifestyle_smoking'],
                        'lifestyle_alcohol' => $_POST['lifestyle_alcohol'],
                        'lifestyle_nutrition' => $_POST['lifestyle_nutrition'],
                        'biometric_waist_value' => $_POST['biometric_waist_value']

                    );
                
                if(isset($_POST['redirect'])){
                    
                    $session->set('post', $post);
                    $session->set('s_lifestyle_physical_activity_answer_complete', $s_lifestyle_physical_activity_answer_complete);
                    $session->set('s_lifestyle_smoking_answer_complete', $s_lifestyle_smoking_answer_complete);
                    $session->set('s_lifestyle_alcohol_answer_complete', $s_lifestyle_alcohol_answer_complete);
                    $session->set('s_lifestyle_nutrition_answer_complete', $s_lifestyle_nutrition_answer_complete);
                    
                    $_POST['redirect'] = trim($_POST['redirect'], '#');
                    return $this->redirect($_POST['redirect'] );
                    
                }else{
                    

                    return new Response($this->renderView('AcmeLiteTestBundle:LiteTest:create_test.html.twig',
                            array(
                                'post' => $post,
                                's_lifestyle_physical_activity_answer_complete' => $s_lifestyle_physical_activity_answer_complete,
                                's_lifestyle_smoking_answer_complete' => $s_lifestyle_smoking_answer_complete,
                                's_lifestyle_alcohol_answer_complete' => $s_lifestyle_alcohol_answer_complete,
                                's_lifestyle_nutrition_answer_complete' => $s_lifestyle_nutrition_answer_complete
                            )));
                }
                
                
                
                
            }else{
                    
				
                $content = $this->renderView('AcmeLiteTestBundle:LiteTest:email_test_pdf.html.twig',
                        array(
                            'site_url' => $root_dir,
                            'client_details' => array(
                                'fname' => $_POST['fname'],
                                'lname' => $_POST['lname'],
                                'weight' => $_POST['weight'],
                                'height' => $_POST['height'],
                                'age' => $_POST['age']),
                            'test_details' => $test_details,
                            'hl_expectancy_male' => $this->getHealthyLifeExpectancies('male'),
                            'hl_expectancy_female' => $this->getHealthyLifeExpectancies('female')
                        ));
                
                $html2pdf = new \HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(10, 10, 10, 10));
                //$html2pdf->setEncoding("ISO-8859-1");
                $stylesheet = file_get_contents($root_dir.'/css/email-test.css'); /// here call you external css file 
                $stylesheet = '<style>'.$stylesheet.'</style>';
                $html2pdf->WriteHTML($stylesheet . $content);

                $test_datetime = date_create($test_details['test_datetime_finalised']);
                $test_datetime = date_format($test_datetime,"d-m-Y");

                $file = $root_dir .'/temp/'. $_POST['fname'] . ' ' . $_POST['lname'] .' '.$test_datetime.'.pdf';
                $html2pdf->Output( $file ,"F"); //output to file


                $from = $this->container->getParameter('site_email_address');
                $subject = $_POST['fname'] . ' ' . $_POST['lname'] . ' -  Lite Test Results PDF';
                $body = $this->renderView('AcmeLiteTestBundle:LiteTest:email_test_content_to_person.html.twig',
                                array(
                                    'name' => $_POST['fname'] . ' ' . $_POST['lname'],
                                    'test_date' => $test_details['test_datetime_finalised']
                                ));
                
                $body_to_admin = $this->renderView('AcmeLiteTestBundle:LiteTest:email_test_content_to_admin.html.twig',
                                array(
                                    'name' => $_POST['fname'] . ' ' . $_POST['lname'],
                                    'fname' => $_POST['fname'],
                                    'lname' => $_POST['lname'],
                                    'weight' => $_POST['weight'],
                                    'height' => $_POST['height'],
                                    'age' => $_POST['age'],
                                    'biometric_waist_value' => $_POST['biometric_waist_value'],
                                    'gender' => $_POST['gender'],
                                    'test_date' => $test_details['test_datetime_finalised']
                                ));

                $attachments = array();


                $attachments[] = $file;

                $recipients = array();
                
                if($mod->isEmailValid(trim($_POST['email']))){
                    $recipients[ $_POST['email'] ] = $_POST['fname'] . ' ' . $_POST['lname'];
                }

                if(count($recipients) > 0){
                    //$this->sendEmail($recipients, '', $from, $subject, $body, $attachments);
                    
                    $admins = $this->getHeadOfficeAdminUsers('active');
                    $recipients_admin = array();
                    
                    for($i=0; $i<count($admins); $i++){
                        if($admins[$i]['receive_litetests'] == 1){
                            $recipients_admin[ $admins[$i]['email'] ] =  $admins[$i]['fname'] . ' ' . $admins[$i]['lname'];
                        }
                    }
                    if(count($recipients_admin) > 0){
                        //$this->sendEmail($recipients_admin, '', $from, $subject, $body_to_admin, $attachments);
                    }
                }else{
                    unlink($file);
                }

                
                
                $session->set('litetest-success', '1');
                $this->get('session')->getFlashBag()->add(
                            'success',
                            'Your Lite Test has been submitted. Thank you!'
                        );
                
                $session->set('litetest-physical-activity', '');
                $session->set('litetest-alcohol', '');
                $session->set('litetest-nutrition', '');
                $session->set('litetest-smoking', '');
                
                $session->set('post', '');
                $session->set('s_lifestyle_physical_activity_answer_complete', '');
                $session->set('s_lifestyle_smoking_answer_complete', '');
                $session->set('s_lifestyle_alcohol_answer_complete', '');
                $session->set('s_lifestyle_nutrition_answer_complete', '');
				
                if(isset($_POST['redirect'])){
                    $_POST['redirect'] = trim($_POST['redirect'], '#');
                    return $this->redirect($_POST['redirect'] );
                }else{
                    return $this->redirect($this->generateUrl('acme_lite_test_create_test') );
                }
                
            }
            
            
            
        }else{
            
            if($session->get('litetest-success') == 1){
             
                $session->set('litetest-success', 0);
                return new Response( '<center>
                        <h3>Your Lite Test has been submitted. Thank you!</h3>
                    </center>');
                
                    
                
            }else{
                $post = array();
            
                return new Response($this->renderView('AcmeLiteTestBundle:LiteTest:create_test.html.twig',
                    array(
                        'post' => $post,
                        's_lifestyle_physical_activity_answer_complete' => $this->sLiteLifestyleTestQuestionnaireAnswersDone('physical-activity'),
                        's_lifestyle_smoking_answer_complete' => $this->sLiteLifestyleTestQuestionnaireAnswersDone('smoking'),
                        's_lifestyle_alcohol_answer_complete' => $this->sLiteLifestyleTestQuestionnaireAnswersDone('alcohol'),
                        's_lifestyle_nutrition_answer_complete' => $this->sLiteLifestyleTestQuestionnaireAnswersDone('nutrition')
                    )));
            }
        }
    }
    
    
    
    // GET LIFESTYLE QUESTIONNAIRE TEMPLATE
    public function testQuestionnaireTemplateAction()
    {
        $session = $this->getRequest()->getSession();
        
        
        if(!isset($_POST['code'])){
            //return $this->redirect($this->generateUrl('acme_club_login'));
            return new Response("invalid parameter");
        }
        
        $_POST['code'] = trim($_POST['code']);
        
        $lifestyle_codes = array("physical_activity", "smoking", "alcohol", "nutrition", "mental_health", "risk_profile");

        if (!in_array($_POST['code'], $lifestyle_codes)){
          return new Response("invalid parameter");
        }
        
        //$smoking_answer = ($_POST['code'] == 'smoking') ? $this->getSpecificTestQuestionnaireClientAnswer(3 /*smoking*/, $_POST['club_client_id'], $_POST['client_test_id']) : '';
        
        return new Response($this->renderView(
            'AcmeLiteTestBundle:LiteTest:test_questionnaire_template_'. $_POST['code'] .'.html.twig',
                array(
                    'gender' => $_POST['gender'],
                    'questions' => $this->getTestQuestionnaire($_POST['code'], NULL, NULL),
                    'options' => $this->getTestQuestionnaireOptionsByCode($_POST['code']),
                    'smoking_answer' => ($_POST['code'] == 'smoking') ? $this->getSpecificTestQuestionnaireClientAnswer(3 /*smoking*/, NULL, NULL) : ''
                )
            ));
        
    }
    
    // SUBMIT LIFESTYLE ANSWER
    public function lifeStyleTestAnswerAction(){
        $session = $this->getRequest()->getSession();
        $datetime = new \DateTime(date("Y-m-d H:i:s"));
        $formula = new Model\TestFormulas();
        
        $error = array();
        
        if($_POST['answer_type'] == 'risk-profile'){
            $error_count = 0;
            foreach ($_POST['answer'] as $lifestyle_question_id => $answer) {
                if(trim($answer) != '' && ($answer > 7 || $answer < 1)){
                    $error_count +=1;
                }
            }
            if( $error_count > 0){
                $error[] = 'Make sure that your answer(s) must not exceed to 7 (highly likely) and not below 1  (highly unlikely).';
            }
        }
        
        if(count($error) == 0 ){
			
            if($_POST['answer_type'] == 'physical-activity' ||
                $_POST['answer_type'] == 'alcohol' || 
                $_POST['answer_type'] == 'mental-health' ||
                $_POST['answer_type'] == 'risk-profile' ||
                $_POST['answer_type'] == 'nutrition' ){
				$lq_ans = array();
                if(isset($_POST['answer'])){
                    foreach ($_POST['answer'] as $lifestyle_question_id => $answer) {
						$lq_ans[$lifestyle_question_id] = array(
								'answer'=> $answer
							);
						
                    }
					$session->set('litetest-'.$_POST['answer_type'], $lq_ans);
					
                }else{
				
					$session->set('litetest-'.$_POST['answer_type'], array() );
				}
				
            }elseif($_POST['answer_type'] == 'smoking'){
				$lq_ans = array();
                $lq_ans[3] = array(
								'answer'=> $_POST['answer']
							);
							
				$session->set('litetest-smoking', $lq_ans);

            }

			
            $s_complete = $this->sLiteLifestyleTestQuestionnaireAnswersDone($_POST['answer_type']);
            
            $response = array(
                'result' => 'success',
                'code' => $_POST['answer_type'],
                's_complete' => $s_complete
            );
        }else{
            $response = array(
                'result' => 'error',
                'message' => $error[0]
            );
        }
        
        
        return new Response(json_encode($response));
    }
	
    
	public function sLiteLifestyleTestQuestionnaireAnswersDone($code){
			$session = $this->getRequest()->getSession();
                        
            $codesearch = str_replace('-', '_', $code);
            $questionnaire = $this->getTestQuestionnaire($codesearch);
            
            $questions = count($questionnaire);
            $answers = 0;
            for($i=0; $i<count( $questionnaire ); $i++){
				//$item = $this->getSpecificTestQuestionnaireClientAnswer($questionnaire[$i]['lq_id'], $club_client_id, $client_test_id);
                $code_session = $session->get('litetest-'.$code);
                $answers += (isset($code_session[ $questionnaire[$i]['lq_id'] ]['answer']) && $code_session[ $questionnaire[$i]['lq_id'] ]['answer'] != '') ? 1 : 0;
                
                //$answers += ($code_session[ $questionnaire[$i]['lq_id'] ]['answer'] != '') ? 1 : 0;
            }
            return ($questions == $answers) ? true : false;
    }
    
    
    
    
//==============================================================================
// GET TEST RESULTS (AJAX)
//==============================================================================
    public function getBmiResultAction(){
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['weight'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['height'])){
            return new Response('Invalid parameter input!');
        }
        $_POST['weight'] = intval($_POST['weight']);
        $_POST['height'] = intval($_POST['height']);
        
        $response = $formula->bMI($_POST['height'], $_POST['weight']);
        return new Response($response);
        
    }
    
    public function getPhysicalActivityResultAction(){
        $session = $this->getRequest()->getSession();
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['moderate_vigorous_pa'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['muscle_strenthening_pa'])){
            return new Response('Invalid parameter input!');
        }
        
        
        $_POST['moderate_vigorous_pa'] = intval($_POST['moderate_vigorous_pa']);
        $_POST['muscle_strenthening_pa'] = intval($_POST['muscle_strenthening_pa']);
        
        $moderate_vigorous_pa = $formula->moderateOrVigorousIntensityPhysicalActivity($_POST['moderate_vigorous_pa']);
        $muscle_strenthening_pa = $formula->muscleStrentheningPhysicalActivity($_POST['muscle_strenthening_pa']);
        
        $response = $formula->physicalActivity($moderate_vigorous_pa, $muscle_strenthening_pa, TRUE);
        
        
        
        $session->set('lifestyle-physical-activity', $response);
        return new Response($response);
        
    }
    
    public function getSmokingResultAction(){
        $session = $this->getRequest()->getSession();
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['gender'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['answer'])){
            return new Response('Invalid parameter input!');
        }
        
        $_POST['answer'] = intval($_POST['answer']);
        
        $response = $formula->smokingHabits($_POST['gender'], $_POST['answer'], TRUE);
        
        $session->set('lifestyle-smoking', $response);
        
        return new Response($response);
    }
    
    public function getAlcoholConsumptionResultAction(){
        $session = $this->getRequest()->getSession();
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['gender'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['per_week_value'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['one_sitting_value'])){
            return new Response('Invalid parameter input!');
        }
        
        $_POST['per_week_value'] = intval($_POST['per_week_value']);
        $_POST['one_sitting_value'] = intval($_POST['one_sitting_value']);
        
        $alcohol_consumption_per_week = $formula->alcoholConsumptionPerWeekResult($_POST['gender'], $_POST['per_week_value']);
        $alcohol_consumption_in_one_sitting = $formula->alcoholConsumptionInOneSittingResult($_POST['one_sitting_value']);
        
        $response = $formula->alcoholConsumptionTotal($alcohol_consumption_per_week, $alcohol_consumption_in_one_sitting, TRUE);
        
        // UPDATE tbl_client_test
        $session->set('lifestyle-alcohol', $response);
        
        return new Response($response);
    }
    

    public function getNutritionResultAction(){
        $session = $this->getRequest()->getSession();
        $formula = new Model\TestFormulas();
        
        if(isset($_POST['answer'])){
            $number_of_yes = 0;
            foreach ($_POST['answer'] as $lifestyle_question_id => $answer) {
                $number_of_yes += intval($answer);
            }

            $response = $formula->nutrition($number_of_yes, TRUE);

        }else{
            $response = '';
        }
        $session->set('lifestyle-nutrition', $response);
        
        return new Response($response);
    }
    
    
    public function getMentalHealthResultAction(){
        $session = $this->getRequest()->getSession();
        $formula = new Model\TestFormulas();
        
        if(isset($_POST['answer'])){
            $mental_health_value = 0;
            foreach ($_POST['answer'] as $lifestyle_question_id => $answer) {
                if($answer != ''){
                    $mental_health_value += intval($answer);
                }
            }
            if($mental_health_value > 0){
                $response = $formula->mentalHealth($mental_health_value);

            }else{
                $response = '';
            }
        }else{
            $response = '';
        }
        
        // UPDATE tbl_client_test
        if($_POST['client_test_id'] != ''){
            $em = $this->getDoctrine()->getManager();
            $test = $em->getRepository('AcmeHeadOfficeBundle:ClientTest')
                        ->findOneBy(array(
                            'client_test_id' => $_POST['client_test_id'],
                        ));
            if($response != ''){
                $test->setLifestyleMentalHealth($response);
            }else{
                $test->setLifestyleMentalHealth(NULL);
            }
            $em->persist($test);
            $em->flush();
        }
        
        $session->set('lifestyle-mental-health', $response);
        
        return new Response($response);
    }
    
    
    public function getRiskProfileResultAction(){
        $session = $this->getRequest()->getSession();
        $formula = new Model\TestFormulas();
        
        if(isset($_POST['answer'])){
            $risk_profile_value = 0;
            foreach ($_POST['answer'] as $lifestyle_question_id => $answer) {
                if($answer != ''){
                    $risk_profile_value += intval($answer);
                }
            }
            if($risk_profile_value > 0){
                //$response = $formula->mentalHealth($mental_health_value);
                $response = $formula->riskProfile($risk_profile_value);

            }else{
                $response = '';
            }
        }else{
            $response = '';
        }
        
        // UPDATE tbl_client_test
        if($_POST['client_test_id'] != ''){
            $em = $this->getDoctrine()->getManager();
            $test = $em->getRepository('AcmeHeadOfficeBundle:ClientTest')
                        ->findOneBy(array(
                            'client_test_id' => $_POST['client_test_id'],
                        ));
            if($response != ''){
                $test->setLifestyleRiskProfile($response);
            }else{
                $test->setLifestyleRiskProfile(NULL);
            }
            $em->persist($test);
            $em->flush();
        }
        
        $session->set('lifestyle-risk-profile',$response);
        
        return new Response($response);
    }
    
    public function getVo2TestAction(){
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['gender'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['weight'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['age'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['test_type'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['value1'])){
            return new Response('Invalid parameter input!');
        }
        
        if(!isset($_POST['value2'])){
            return new Response('Invalid parameter input!');
        }
        
        $_POST['weight'] = intval($_POST['weight']);
        $_POST['age'] = intval($_POST['age']);
        $_POST['value1'] = intval($_POST['value1']);
        $_POST['value2'] = intval($_POST['value2']);
        
        if($_POST['test_type'] == 'bike'){
            $score = $formula->vo2BikeTest($_POST['gender'], $_POST['weight'], $_POST['age'], $_POST['value1']);
            
        }elseif($_POST['test_type'] == 'resting-hr'){
            
            $score = $formula->vo2RestingHrTest($_POST['age'], $_POST['value1']);
            
        }elseif($_POST['test_type'] == 'treadmill'){
            
            $score = $formula->vo2TreadmillTest($_POST['gender'], $_POST['weight'], $_POST['value1'], $_POST['value2']);
        
        }elseif($_POST['test_type'] == 'beep'){
            $score = $formula->vo2BeepTest($_POST['value1'], $_POST['value2']);
        }else{
            
            $score = $formula->vo2DirectEntry($_POST['value1']);
        }
        
        $result = $formula->vo2($_POST['gender'], $_POST['age'], $score);
        
        $response = array(
            'score'=>$score,
            'result'=>$result
            );
        
        return new Response(json_encode($response));
        
    }
    
    public function getBalanceTestAction(){
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['gender'])){
            return new Response('Invalid parameter input!');
        }
        if(!isset($_POST['value'])){
            return new Response('Invalid parameter input!');
        }
        
        $_POST['value'] = intval($_POST['value']);
        
        $response = $formula->balance($_POST['test_type'], $_POST['gender'], $_POST['value']);
        
        
        return new Response($response);
    }
    
    // removelater
    public function getWaistTestAction(){
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['gender'])){
            return new Response('Invalid parameter input!');
        }
        
        $_POST['value'] = intval($_POST['value']);
        
        $response = $formula->waist($_POST['gender'], $_POST['value']);
        
        
        return new Response($response);
    }
    
    // removelater
    public function getSquatTestAction(){
        $formula = new Model\TestFormulas();
        
        if(!isset($_POST['gender'])){
            return new Response('Invalid parameter input!');
        }
        
        $_POST['value'] = intval($_POST['value']);
        
        $response = $formula->waist($_POST['gender'], $_POST['value']);
        
        
        return new Response($response);
    }
}
