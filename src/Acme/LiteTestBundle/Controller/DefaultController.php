<?php

namespace Acme\LightTestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('AcmeLightTestBundle:Default:index.html.twig', array('name' => $name));
    }
}
