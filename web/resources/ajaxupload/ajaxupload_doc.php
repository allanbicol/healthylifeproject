<?php 
    $file = $_FILES['image']; // 'image' because that was the name we set in the javascript code
    
    $allowed =  array('doc','docx' ,'pdf');
    $filename = $_FILES['image']['name'];
    $ext = pathinfo($filename, PATHINFO_EXTENSION);
    if(!in_array($ext,$allowed) ) {
        echo 'error'; exit();
    }
    
    $path = '../../uploads/client-files/'; //whatever path on the server your images are being stored in
    $location = $path. uniqid() . '.' . $ext; //this is where the file will go
    
    $image = imagecreatefromstring(file_get_contents($_FILES['image']['tmp_name']));
    $exif = exif_read_data($_FILES['image']['tmp_name']);
    if(!empty($exif['Orientation'])) {
        switch($exif['Orientation']) {
            case 8:
                $image = imagerotate($image,90,0);
                break;
            case 3:
                $image = imagerotate($image,180,0);
                break;
            case 6:
                $image = imagerotate($image,-90,0);
                break;
        }
    }
    
//    if($ext== 'jpeg'){
//        imagejpeg($image, $location);
//    }else if($ext== 'JPEG'){
//        imagejpeg($image, $location);
//    }else if($ext== 'jpg'){
//        imagejpeg($image, $location);
//    }else if($ext== 'JPG'){
//        imagejpeg($image, $location);
//    }else if($ext== 'png'){
//        imagepng($image, $location);
//    }else if($ext== 'PNG'){
//        imagepng($image, $location);
//    }else if($ext== 'gif'){
//        imagegif($image, $location);
//    }else if($ext== 'GIF'){
//        imagegif($image, $location);
//    }
    
    
    
    //move_uploaded_file($_FILES["image"]["tmp_name"], $location); // move the file there
    
    if($_SERVER['SERVER_NAME'] != 'localhost'){
        $location = str_replace('../../', '../', $location);
    }
    
    echo $location; //send the file location back to the javascript
?>