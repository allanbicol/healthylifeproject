function savepic(){
    //can perform client side field required checking for "fileToUpload" field
    // http://www.scriptiny.com/2013/02/simple-file-upload-using-ajax/
    $('.progress.progress-striped').show();
    if($('.applications-option:checked').val() == 1){
        var fileno = attachbuttonclick.attr('tag');
    }else{
        var fileno = 0;
    }

    var final_fn = getFilename($('#fileToUpload').val());
    $('#fauxUploadValue').val(final_fn);


    $.ajaxFileUpload({
        url: resources_dir + '/ajaxupload/doajaxotherfileupload_new.php',
        secureuri:false,
        fileElementId:'fileToUpload',
        dataType: 'json',
        data:{'tm': admin_id + '-'+fileno+'_'},
        success: function (data, status)
        {
            setTimeout(function(){
                if(data.error=='undefined' || data.error==''){
                    $('.progress-bar').css('width','100%');
                    $(".image-navigator .files-list").find(".item").each(function(){
                       var $self = $(this);
                       if($self.attr("tag") == final_fn){
                           setTimeout(function(){
                            $self.click();
                           },1000);
                       }
                    });
                    
                    setTimeout(function(){
                        $('#uploadFileModal .modal-dialog .modal-content .modal-header').find('.close').click();
                        $('#uploadFileModal').find('#fauxUploadValue').val('/');

                        $('.progress-bar').css('width','45%');
                    },1000);
                    
                    var id= $('#uploadFileId').val();
                    document.getElementById(id).value=final_fn;

                }else{
                    $('.progress-bar').css('width','45%');
                    $('.progress.progress-striped').hide();
                }
            },2000);

            if(typeof(data.error) != 'undefined'){
                if(data.error != ''){
                    alert(data.error);
                }
//                else{
//                    alert(data.msg); // returns location of uploaded file
//                }
            }
        },
        error: function (data, status, e)
        {
            alert(e);
        }
    });

    return false;
}