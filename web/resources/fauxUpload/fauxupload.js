function getFilename(filename){
    var final_fn = '';
    filename =filename.split('\\');
    for(i=0; i<filename.length; i++){
        if(i == filename.length - 1){
            final_fn = filename[i];    
        }

    }   
    final_fn = final_fn.split("'").join('_');
    final_fn = final_fn.split(" ").join('_');
    return final_fn;
}

$("#fileToUpload").change(function(){
    var $this=$(this);
    $("#fauxUploadValue").val( getFilename($this.val()) );
});


$(".uploadpic").click(function(){
    $(".modal-footer button").attr('disabled','disabled');
    $('.progress').addClass("uploading");
    $(".progress-label").addClass("uploading");
    
    //can perform client side field required checking for "fileToUpload" field
    // http://www.scriptiny.com/2013/02/simple-file-upload-using-ajax/
    

    var final_fn = getFilename($('#fileToUpload').val());
    $('#fauxUploadValue').val(final_fn);

    var file = _("fileToUpload").files[0];
    // alert(file.name+" | "+file.size+" | "+file.type);
    var formdata = new FormData();
    formdata.append("fileToUpload", file);
    formdata.append("email", client_email);
    var ajax = new XMLHttpRequest();
    ajax.upload.addEventListener("progress", progressHandler, false);

    ajax.addEventListener('readystatechange', function(e) {
        if (ajax.readyState == ajax.DONE) {
            if(ajax.responseText.indexOf('Error') > -1){
                $('#uploadFileModal').modal('hide');
                bootbox.alert("Warning: Unknown errors.");
                $('.progress-bar').css('width','0%');
                $(".modal-footer button").removeAttr('disabled');
                $(".progress-label").find(".upload-percent").text(0);
                $(".progress-label").removeClass("uploading");
                $(".progress").removeClass("uploading");
            }else{
                if(ajax.responseText.indexOf('Warning') > -1){
                    $('#uploadFileModal').modal('hide');
                    bootbox.alert(ajax.responseText);
                    $('.progress-bar').css('width','0%');
                    $(".modal-footer button").removeAttr('disabled');
                    $(".progress-label").find(".upload-percent").text(0);
                    $(".progress-label").removeClass("uploading");
                    $(".progress").removeClass("uploading");
                }else{
                    $('#uploadFileModal').modal('hide');
    //                bootbox.alert(ajax.responseText);
                    $(".uploaded-files").find("table").append(ajax.responseText);
                    var client_files = $(".uploaded-files").find("table").find("tr").length;
                    $(".client-files-number").text(client_files);
                    completeHandler();
                }
            }
        }
      });
    ajax.addEventListener("error", errorHandler, false);
    ajax.addEventListener("abort", abortHandler, false);
    ajax.open("POST", fauxupload_url);
    ajax.send(formdata);

});

function _(el){
    return document.getElementById(el);
}
function progressHandler(event){ 
    var percent = (event.loaded / event.total) * 100;
    $('.progress-bar').css('width',percent + '%');
    var percent = Math.round( percent * 10 ) / 10;
    $(".upload-percent").text(percent);
}
function completeHandler(){
        $('#uploadFileModal').find('#fauxUploadValue').val('/');

        $('.progress-bar').css('width','0%');
        $('#uploadFileModal').modal('hide');
        $(".modal-footer button").removeAttr('disabled');
        $(".progress-label").find(".upload-percent").text(0);
        $(".progress-label").removeClass("uploading");
        $(".progress").removeClass("uploading");
        
        $(".uploaded-files").find("table").append();
}
function errorHandler(event){
        alert("Upload Failed");

        $('.progress-bar').css('width','0%');
        $(".modal-footer button").removeAttr('disabled');
        $(".progress-label").removeClass("uploading");
        $(".progress-label").find(".upload-percent").text(0);
        $(".progress").removeClass("uploading");
}
function abortHandler(event){
    $('.progress-bar').css('width','0%');
    $(".modal-footer button").removeAttr('disabled');
    $(".progress-label").removeClass("uploading");
    $(".progress-label").find(".upload-percent").text(0);
    $(".progress").removeClass("uploading");
}

$(".delete-file").click(function(){
    var $this = $(this);
    bootbox.confirm("Are you sure?", function(result) {
        if(result == true){
            $.ajax({
                url: fauxupload_delete_url,
                method: 'POST',
                data: {
                    'email': client_email,
                    'filename' : $this.attr("data-filename")
                },
                success: function(response) { 
                    if(response == 'success'){
                        $this.closest("tr").remove();
                        var client_files = $(".uploaded-files").find("table").find("tr").length;
                        $(".client-files-number").text(client_files);
                    }else{
                        bootbox.alert(response);
                    }
                }
            });
        }
    }); 
});