<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */
session_start();
function siteURL()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'];
    return $protocol.$domainName;
}
function ausDateFormat($stringdate){
    if(trim($stringdate) != '' && substr_count($stringdate, "-") > 1){
        list($y, $m , $d) = explode("-", $stringdate);
        return $d.'-'.$m.'-'.$y;
    }
}

if(isset($_SESSION['_sf2_attributes']['user_type']) && ($_SESSION['_sf2_attributes']['user_type'] == 'admin-user' || $_SESSION['_sf2_attributes']['user_type'] == 'public' || $_SESSION['_sf2_attributes']['user_type'] == 'corporate')){
    
    if($_SERVER['SERVER_NAME'] == 'localhost'){
        $con=mysqli_connect("localhost","root","","cls");
        $img_url = siteURL().'/capitallinkservices.com.au/web/img';
        $asset_url = siteURL().'/capitallinkservices.com.au/web';
    }else{
//        $con=mysqli_connect("localhost","clsvcdev","dreNca8gW4","clsvcdev");
        $con=mysqli_connect("245e87e26440aa9e47f3c987021b05cc73e5943b.rackspaceclouddb.com","clsvcdev","dreNca8gW4","capitallinkservices");
        $img_url = siteURL().'/web/img';
        $asset_url = siteURL().'/web';
    }

    $_GET['order'] = filter_var($_GET['order'], FILTER_SANITIZE_STRING);
    $_GET['order'] = intval($_GET['order']);
    
    if($_SESSION['_sf2_attributes']['user_type'] == 'admin-user'){ 
        $query = mysqli_query($con,"SELECT o.*, CONCAT(p.fname, ' ', p.lname) as sender_contact_name, p.phone, p.address, p.city, p.postcode, p.state, p.mba_organisation_name,
            o.doc_delivery_contact_no
            FROM tbl_orders o
            LEFT JOIN tbl_payment p ON p.order_no=o.order_no
            WHERE order_type=7 AND o.order_no=".$_GET['order']);
    }else{
        $query = mysqli_query($con,"SELECT o.*, CONCAT(p.fname, ' ', p.lname) as sender_contact_name, p.phone, p.address, p.city, p.postcode, p.state, p.mba_organisation_name,
            o.doc_delivery_contact_no
            FROM tbl_orders o
            LEFT JOIN tbl_payment p ON p.order_no=o.order_no
            WHERE order_type=7 AND o.order_no=".$_GET['order']." AND o.client_id=".$_SESSION['_sf2_attributes']['client_id']);
    }
    $order = mysqli_fetch_array($query);
    
    //Select * from tbl_orders where order_type=7 and order_no=10003908

    //$html = '<page footer="page">'; // for auto page number
    $html = '<page>';
    
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 100%; vertical-align: top; text-align: right;" >CLS COPY</td>';
    $html .= '</tr></table>';
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 50%; vertical-align: top;" >';
    $html .= '<img src="'.$asset_url.'/img/cls-logo.png" style="float: right;>';
    $html .= '</td>';
    $html .= '<td style="width: 50%; vertical-align: top; text-align: right;" ><br><h1>CLS ELITE DELIVERY</h1></td>';
    $html .= '</tr></table>';
    $html .= '<br/><br/>';
    
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 50%; vertical-align: top;" >CON NOTE: <b><span style="">'.$_GET['order'].'</span></b></td>';
    $html .= '<td style="width: 50%; vertical-align: top;" >SECURITY TAG NO.: <span style="">'.$order['doc_delivery_security_no'].'</span></td>';
    $html .= '</tr></table>';
    $html .= '<hr/>';
    $html .= '<br/><br/>';
    
    $html .= '<b>SENDER DETAILS:</b><br/><br/>';
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >COMPANY:</td>';
    $html .= '<td style="width: 70%; vertical-align: top; padding-bottom: 5px; " >'.$order['mba_organisation_name'].'</td>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >CONTACT AREA:</td>';
    $html .= '<td style="width: 70%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_pickup_contact_area'].'</td>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >CONTACT NAME:</td>';
    $html .= '<td style="width: 70%; vertical-align: top; padding-bottom: 5px; " >'.$order['sender_contact_name'].'</td>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >ADDRESS:</td>';
    $html .= '<td style="width: 70%; vertical-align: top; padding-bottom: 5px; " >';
    $html .= $order['address']. '<br>';
    $html .= '<span style="font-size: 18px;">'.$order['city'] .' '.$order['postcode'].'</span>';
    $html .= '</td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >PHONE NUMBER:</td>';
    $html .= '<td style="width: 70%; vertical-align: top; padding-bottom: 5px; " >';
    $html .= $order['phone'];
    $html .= '</td>';
    $html .= '</tr>';
    
    $html .= '</table>';
    $html .= '<br/>';
    $html .= '<hr/>';
    $html .= '<br/>';
    
    
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td><b>RECEIVER DETAILS:</b><br/><br/></td>';
    $html .= '<td><b>PRIMARY</b><br/><br/></td>';
    $html .= '<td><b>ALTERNATE 1</b><br/><br/></td>';
    $html .= '<td><b>ALTERNATE 2</b><br/><br/></td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >COMPANY:</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_delivery_company'].'</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_delivery_company_alt1'].'</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_delivery_company_alt2'].'</td>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >CONTACT AREA:</td>';
    //$html .= '<td style="width: 70%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_receiver_name'].'</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_pickup_contact_area'].'</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_pickup_contact_area_alt1'].'</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_pickup_contact_area_alt2'].'</td>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >CONTACT NAME:</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_delivery_recipient_name'].'</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_delivery_recipient_name_alt1'].'</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_delivery_recipient_name_alt2'].'</td>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >ADDRESS:</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >';
    $html .= $order['doc_delivery_address'] . '<br>';
    $html .= '<span style="font-size: 18px;">'.$order['doc_delivery_city'] .' '.$order['doc_delivery_postcode'].'</span>';
    $html .= '</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >';
    $html .= $order['doc_delivery_address_alt1'] . '<br>';
    $html .= '<span style="font-size: 18px;">'.$order['doc_delivery_city_alt1'] .' '.$order['doc_delivery_postcode_alt1'].'</span>';
    $html .= '</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >';
    $html .= $order['doc_delivery_address_alt2'] . '<br>';
    $html .= '<span style="font-size: 18px;">'.$order['doc_delivery_city_alt2'] .' '.$order['doc_delivery_postcode_alt2'].'</span>';
    $html .= '</td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >PHONE NUMBER:</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >';
    $html .= $order['doc_delivery_contact_no'];
    $html .= '</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >';
    //$html .= $order['doc_delivery_contact_no_alt1'];
    $html .= '</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >';
    //$html .= $order['doc_delivery_contact_no_alt2'];
    $html .= '</td>';
    $html .= '</tr>';
    
    $html .= '</table>';
    $html .= '<br/><br/>';
    $html .= '<hr/>';
    $html .= '<br/><br/>';
    
    
    
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 25%; vertical-align: top;" >';
    $html .= '<img src="'.$asset_url.'/api/barcodegen.1d-php5.v5.1.0/label.php?number='.$_GET["order"].'" style="height: 50px;">';
    $html .= '</td>';
    $html .= '<td style="width: 30%; vertical-align: top;" >';
    $html .= 'DATE: <span style="">'.ausDateFormat($order['doc_package_pickup_date']).'</span><br/>';
    $html .= 'NO. OF ITEMS: <span style="">'.$order['doc_package_total_pieces'].'</span><br/>';
    $html .= 'READY AT: <span style="">'.$order['doc_package_ready_hr'].':'.$order['doc_package_ready_min'].$order['doc_package_ready_ampm'].'</span><br/>';
    $html .= '</td>';
    $html .= '<td style="width: 45%; vertical-align: top; border: 1px solid black; padding: 5px;" >';
    $html .= 'PACKAGE CONDITION: ';
    $html .= '<br/>';
    $html .= '<br/>';
    $html .= '<br/>';
    $html .= '<br/>';
    $html .= '<br/>';
    $html .= '</td>';
    $html .= '</tr></table>';
    $html .= '<hr/>';
    $html .= '<br/><br/>';
    
   
    $html .= '<b>SENDER:</b><br/>';
    $html .= '<br/><br/>';
    $html .= 'NAME: _______________________________________________________________________________________';
    $html .= '<br/><br/>';
    $html .= 'SENDER SIGNATURE: ____________________________________________________ DATE: ________________';
    $html .= '<br/><br/><br/>';
    $html .= '<hr/>';
    $html .= '<br/><br/>';
    
    $html .= '<b>RECEIVED BY:</b><br/>';
    $html .= '<br/><br/>';
    $html .= 'NAME: _______________________________________________________________________________________';
    $html .= '<br/><br/>';
    $html .= 'RECIPIENT SIGNATURE: _________________________________________________ DATE: ________________';
    $html .= '<br/><br/><br/>';
    $html .= '<hr/>';
    
    
    
    
    
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 100%; vertical-align: top; text-align: right;" >RECEIVER COPY</td>';
    $html .= '</tr></table>';
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 50%; vertical-align: top;" >';
    $html .= '<img src="'.$asset_url.'/img/cls-logo.png" style="float: right;>';
    $html .= '</td>';
    $html .= '<td style="width: 50%; vertical-align: top; text-align: right;" ><br><h1>CLS ELITE DELIVERY</h1></td>';
    $html .= '</tr></table>';
    $html .= '<br/><br/>';
    
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 50%; vertical-align: top;" >CON NOTE: <b><span style="">'.$_GET['order'].'</span></b></td>';
    $html .= '<td style="width: 50%; vertical-align: top;" >SECURITY TAG NO.: <span style="">'.$order['doc_delivery_security_no'].'</span></td>';
    $html .= '</tr></table>';
    $html .= '<hr/>';
    $html .= '<br/><br/>';
    
    $html .= '<b>SENDER DETAILS:</b><br/><br/>';
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >COMPANY:</td>';
    $html .= '<td style="width: 70%; vertical-align: top; padding-bottom: 5px; " >'.$order['mba_organisation_name'].'</td>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >CONTACT AREA:</td>';
    $html .= '<td style="width: 70%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_pickup_contact_area'].'</td>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >CONTACT NAME:</td>';
    $html .= '<td style="width: 70%; vertical-align: top; padding-bottom: 5px; " >'.$order['sender_contact_name'].'</td>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >ADDRESS:</td>';
    $html .= '<td style="width: 70%; vertical-align: top; padding-bottom: 5px; " >';
    $html .= $order['address']. '<br>';
    $html .= '<span style="font-size: 18px;">'.$order['city'] .' '.$order['postcode'].'</span>';
    $html .= '</td>';
    $html .= '</tr>';
    $html .= '</table>';
    $html .= '<br/>';
    $html .= '<hr/>';
    $html .= '<br/>';;
    
    
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td><b>RECEIVER DETAILS:</b><br/><br/></td>';
    $html .= '<td><b>PRIMARY</b><br/><br/></td>';
    $html .= '<td><b>ALTERNATE 1</b><br/><br/></td>';
    $html .= '<td><b>ALTERNATE 2</b><br/><br/></td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >COMPANY:</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_delivery_company'].'</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_delivery_company_alt1'].'</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_delivery_company_alt2'].'</td>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >CONTACT AREA:</td>';
    //$html .= '<td style="width: 70%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_receiver_name'].'</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_pickup_contact_area'].'</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_pickup_contact_area_alt1'].'</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_pickup_contact_area_alt2'].'</td>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >CONTACT NAME:</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_delivery_recipient_name'].'</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_delivery_recipient_name_alt1'].'</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_delivery_recipient_name_alt2'].'</td>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >ADDRESS:</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >';
    $html .= $order['doc_delivery_address'] . '<br>';
    $html .= '<span style="font-size: 18px;">'.$order['doc_delivery_city'] .' '.$order['doc_delivery_postcode'].'</span>';
    $html .= '</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >';
    $html .= $order['doc_delivery_address_alt1'] . '<br>';
    $html .= '<span style="font-size: 18px;">'.$order['doc_delivery_city_alt1'] .' '.$order['doc_delivery_postcode_alt1'].'</span>';
    $html .= '</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >';
    $html .= $order['doc_delivery_address_alt2'] . '<br>';
    $html .= '<span style="font-size: 18px;">'.$order['doc_delivery_city_alt2'] .' '.$order['doc_delivery_postcode_alt2'].'</span>';
    $html .= '</td>';
    $html .= '</tr>';
    
    
    $html .= '</table>';
    $html .= '<br/><br/>';
    $html .= '<hr/>';
    $html .= '<br/><br/>';
    
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 25%; vertical-align: top;" >';
    $html .= '<img src="'.$asset_url.'/api/barcodegen.1d-php5.v5.1.0/label.php?number='.$_GET["order"].'" style="height: 50px;">';
    $html .= '</td>';
    $html .= '<td style="width: 30%; vertical-align: top;" >';
    $html .= 'DATE: <span style="">'.ausDateFormat($order['doc_package_pickup_date']).'</span><br/>';
    $html .= 'NO. OF ITEMS: <span style="">'.$order['doc_package_total_pieces'].'</span><br/>';
    $html .= 'READY AT: <span style="">'.$order['doc_package_ready_hr'].':'.$order['doc_package_ready_min'].$order['doc_package_ready_ampm'].'</span><br/>';
    $html .= '</td>';
    $html .= '<td style="width: 45%; vertical-align: top; border: 1px solid black; padding: 5px;" >';
    $html .= 'PACKAGE CONDITION: ';
    $html .= '<br/>';
    $html .= '<br/>';
    $html .= '<br/>';
    $html .= '<br/>';
    $html .= '<br/>';
    $html .= '</td>';
    $html .= '</tr></table>';
    $html .= '<hr/>';
    $html .= '<br/><br/>';
    
   
    $html .= '<b>SENDER:</b><br/>';
    $html .= '<br/><br/>';
    $html .= 'NAME: _______________________________________________________________________________________';
    $html .= '<br/><br/>';
    $html .= 'SENDER SIGNATURE: ____________________________________________________ DATE: ________________';
    $html .= '<br/><br/><br/>';
    $html .= '<hr/>';
    $html .= '<br/><br/>';
    
    $html .= '<b>DELEVERED BY:</b><br/>';
    $html .= '<br/><br/>';
    $html .= 'DRIVER NAME: _______________________________________________________________________________';
    $html .= '<br/><br/>';
    $html .= 'DRIVER SIGNATURE: ____________________________________________________ DATE: ________________';
    $html .= '<br/><br/><br/>';
    $html .= '<hr/>';
    
    
    
    $html .= '<br/><br/>';
    
    
    
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 100%; vertical-align: top; text-align: right;" >SENDER COPY</td>';
    $html .= '</tr></table>';
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 50%; vertical-align: top;" >';
    $html .= '<img src="'.$asset_url.'/img/cls-logo.png" style="float: right;>';
    $html .= '</td>';
    $html .= '<td style="width: 50%; vertical-align: top; text-align: right;" ><br><h1>CLS ELITE DELIVERY</h1></td>';
    $html .= '</tr></table>';
    $html .= '<br/><br/>';
    
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 50%; vertical-align: top;" >CON NOTE: <b><span style="">'.$_GET['order'].'</span></b></td>';
    $html .= '<td style="width: 50%; vertical-align: top;" >SECURITY TAG NO.: <span style="">'.$order['doc_delivery_security_no'].'</span></td>';
    $html .= '</tr></table>';
    $html .= '<hr/>';
    $html .= '<br/><br/>';
    
    $html .= '<b>SENDER DETAILS:</b><br/><br/>';
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >COMPANY:</td>';
    $html .= '<td style="width: 70%; vertical-align: top; padding-bottom: 5px; " >'.$order['mba_organisation_name'].'</td>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >CONTACT AREA:</td>';
    $html .= '<td style="width: 70%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_pickup_contact_area'].'</td>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >CONTACT NAME:</td>';
    $html .= '<td style="width: 70%; vertical-align: top; padding-bottom: 5px; " >'.$order['sender_contact_name'].'</td>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >ADDRESS:</td>';
    $html .= '<td style="width: 70%; vertical-align: top; padding-bottom: 5px; " >';
    $html .= $order['address']. '<br>';
    $html .= '<span style="font-size: 18px;">'.$order['city'] .' '.$order['postcode'].'</span>';
    $html .= '</td>';
    $html .= '</tr>';
    $html .= '</table>';
    $html .= '<br/>';
    $html .= '<hr/>';
    $html .= '<br/>';
    
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td><b>RECEIVER DETAILS:</b><br/><br/></td>';
    $html .= '<td><b>PRIMARY</b><br/><br/></td>';
    $html .= '<td><b>ALTERNATE 1</b><br/><br/></td>';
    $html .= '<td><b>ALTERNATE 2</b><br/><br/></td>';
    $html .= '</tr>';
    
    $html .= '<tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >COMPANY:</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_delivery_company'].'</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_delivery_company'].'</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_delivery_company'].'</td>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >CONTACT AREA:</td>';
    //$html .= '<td style="width: 70%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_receiver_name'].'</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_pickup_contact_area'].'</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_pickup_contact_area'].'</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_pickup_contact_area'].'</td>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >CONTACT NAME:</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_delivery_recipient_name'].'</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_delivery_recipient_name'].'</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >'.$order['doc_delivery_recipient_name'].'</td>';
    $html .= '</tr>';
    $html .= '<tr>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px;" >ADDRESS:</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >';
    $html .= $order['doc_delivery_address'] . '<br>';
    $html .= '<span style="font-size: 18px;">'.$order['doc_delivery_city'] .' '.$order['doc_delivery_postcode'].'</span>';
    $html .= '</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >';
    $html .= $order['doc_delivery_address'] . '<br>';
    $html .= '<span style="font-size: 18px;">'.$order['doc_delivery_city'] .' '.$order['doc_delivery_postcode'].'</span>';
    $html .= '</td>';
    $html .= '<td style="width: 25%; vertical-align: top; padding-bottom: 5px; " >';
    $html .= $order['doc_delivery_address'] . '<br>';
    $html .= '<span style="font-size: 18px;">'.$order['doc_delivery_city'] .' '.$order['doc_delivery_postcode'].'</span>';
    $html .= '</td>';
    $html .= '</tr>';
    
    
    $html .= '</table>';
    $html .= '<br/><br/>';
    $html .= '<hr/>';
    $html .= '<br/><br/>';
    
    $html .= '<table border="0" cellpading="0" cellspacing="0"><tr>';
    $html .= '<td style="width: 25%; vertical-align: top;" >';
    $html .= '<img src="'.$asset_url.'/api/barcodegen.1d-php5.v5.1.0/label.php?number='.$_GET["order"].'" style="height: 50px;">';
    $html .= '</td>';
    $html .= '<td style="width: 30%; vertical-align: top;" >';
    $html .= 'DATE: <span style="">'.ausDateFormat($order['doc_package_pickup_date']).'</span><br/>';
    $html .= 'NO. OF ITEMS: <span style="">'.$order['doc_package_total_pieces'].'</span><br/>';
    $html .= 'READY AT: <span style="">'.$order['doc_package_ready_hr'].':'.$order['doc_package_ready_min'].$order['doc_package_ready_ampm'].'</span><br/>';
    $html .= '</td>';
    $html .= '<td style="width: 45%; vertical-align: top; border: 1px solid black; padding: 5px;" >';
    $html .= 'PACKAGE CONDITION: ';
    $html .= '<br/>';
    $html .= '<br/>';
    $html .= '<br/>';
    $html .= '<br/>';
    $html .= '<br/>';
    $html .= '</td>';
    $html .= '</tr></table>';
    $html .= '<hr/>';
    $html .= '<br/><br/>';
    
   
    $html .= '<b>SENT BY:</b><br/>';
    $html .= '<br/><br/>';
    $html .= 'SENDER NAME: _______________________________________________________________________________';
    $html .= '<br/><br/>';
    $html .= 'SENDER SIGNATURE: ____________________________________________________ DATE: ________________';
    $html .= '<br/><br/><br/>';
    $html .= '<hr/>';
    $html .= '<br/><br/>';
    
    $html .= '<b>COLLECTED BY:</b><br/>';
    $html .= '<br/><br/>';
    $html .= 'DRIVER NAME: _______________________________________________________________________________';
    $html .= '<br/><br/>';
    $html .= 'DRIVER SIGNATURE: ____________________________________________________ DATE: ________________';
    $html .= '<br/><br/><br/>';
    $html .= '<hr/>';
    
    
    $html .='</page>';
    
    
    require_once(dirname(__FILE__).'/html2pdf.class.php');
    //$html2pdf = new HTML2PDF('L', array(127.35, 73.5), 'en', true, 'UTF-8', array(5, 3, 5, 3));
    $html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(13, 10, 13, 10));
    
    //$html2pdf->setEncoding("ISO-8859-1");
    $html2pdf->WriteHTML($html);
    $html2pdf->Output(); //output to file
}else{
    header('Location: '.siteURL().'/user-login');
}

