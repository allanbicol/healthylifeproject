<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */
    
//$html = file_get_contents("valid.html");
$html = '<div style="padding: 100px 125px 100px 125px;"><div style="height: 116px; width: 116px;  margin: auto"></div><p>Note No: FMB-00000001</p><p>The Department of Foreign Affairs and Trade presents its compliments to the Embassy of United States and has the honour to request the issue of a visa endorsement in respect to the following:</p><p><strong>Mr Leo karl Balibol LADENO </strong><br/><strong>Passport Number:</strong> 123456<br/><strong>Date of Birth:</strong> 1901-01-03<br/><strong>Agency:</strong> Aged Care Standards and Accreditation Agency Ltd<br/><strong>Position:</strong> a Contractor</p><p><strong>Mr Raymundo Esguerra GALLANO </strong><br/><strong>Passport Number:</strong> 123456<br/><strong>Date of Birth:</strong> 2014-03-05<br/><strong>Agency:</strong> Aged Care Standards and Accreditation Agency Ltd<br/><strong>Position:</strong> a Contractor</p><p>Mr Leo karl Balibol LADENO and Mr Raymundo Esguerra GALLANO will be travelling to the United States for the purpose of attending meetings. Mr Raymundo Esguerra GALLANO will arrive on 12 March 2014 and depart on 13 March 2014.</p><p>The Department of Foreign Affairs and Trade requests that the appropriate visa be inscribed in the enclosed passport.  For further information on this application the Embassy of United States may contact <strong>Leo karl Ladeno</strong> on <strong>(02) 6261 9754.</strong></p><p>The Department of Foreign Affairs and Trade avails itself of this opportunity to renew to the Embassy of United States the assurances of its highest consideration.</p><p><div style="height: 116px; width: 116px; background-color: black;"></div><br/>CANBERRA ACT<br/>[DateOfIssue]</p></div>';
require_once(dirname(__FILE__).'/../html2pdf.class.php');
$html2pdf = new HTML2PDF('P', 'A4', 'fr', true, 'UTF-8', array(0, 0, 0, 0));
//$html2pdf->setEncoding("ISO-8859-1");
$html2pdf->WriteHTML($html);
$html2pdf->Output("pdf/PDF.pdf"); //output to file
