<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 2mm }
    table.page_footer {width: 100%; border: none; background-color: #DDDDFF; border-top: solid 1mm #AAAADD; padding: 2mm}
    div.note {border: solid 1mm #DDDDDD;background-color: #EEEEEE; padding: 2mm; border-radius: 2mm; width: 100%; }
    ul.main { width: 95%; list-style-type: square; }
    ul.main li { padding-bottom: 2mm; }
    h1 { text-align: center; font-size: 20mm}
    h3 { text-align: center; font-size: 14mm}
-->
</style>


<page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
    <page_header>
        <table class="page_header">
            <tr>
                <td style="width: 50%; text-align: left">
                    A propos de ...
                </td>
                <td style="width: 50%; text-align: right">
                    
                </td>
            </tr>
        </table>
    </page_header>
    <page_footer>
        <table class="page_footer">
            <tr>
                <td style="width: 33%; text-align: left;">
                </td>
                <td style="width: 34%; text-align: center">
                    page [[page_cu]]/[[page_nb]]
                </td>
                <td style="width: 33%; text-align: right">
                </td>
            </tr>
        </table>
    </page_footer>
    <bookmark title="Présentation" level="0" ></bookmark>
    <br><br><br><br><br><br><br><br>
    <h1>HTML2PDF</h1>
    <h3>v<?php echo __CLASS_HTML2PDF__; ?></h3><br>
    <br><br><br><br><br>
    <div style="text-align: center; width: 100%;">
        <br>
        <img src="./res/logo.png" alt="Logo HTML2PDF" style="width: 150mm">
        <br>
    </div>
    <br><br><br><br><br>
    <div class="note">
        HTML2PDF est un convertisseur de code HTML vers PDF écrit en PHP5, utilisant la librairie <a href="http://tcpdf.org">TCPDF.</a><br>
    </div>
</page>

<page pageset="old">
    <bookmark title="Sommaire" level="0" ></bookmark>
    <!-- here will be the automatic index -->
</page>
