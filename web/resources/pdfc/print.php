<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */
session_start();


if(isset($_SESSION['_sf2_attributes']['user_type']) && 
        ($_SESSION['_sf2_attributes']['user_type'] == 'government' || $_SESSION['_sf2_attributes']['user_type'] == 'tpn-user')){
    
    if($_SERVER['SERVER_NAME'] == 'localhost'){
        $con=mysqli_connect("localhost","root","","cls");
    }else{
//        $con=mysqli_connect("localhost","clsvcdev","dreNca8gW4","clsvcdev");
        $con=mysqli_connect("245e87e26440aa9e47f3c987021b05cc73e5943b.rackspaceclouddb.com","clsvcdev","dreNca8gW4","capitallinkservices");
    }



    if($_SESSION['_sf2_attributes']['user_type'] == 'government'){
        $result = mysqli_query($con,"SELECT * FROM tbl_tpn WHERE tpn_no='". $_GET["tpn"] ."' AND client_id=".$_SESSION['_sf2_attributes']['client_id']);
    }else if($_SESSION['_sf2_attributes']['user_type'] == 'tpn-user'){
        $result = mysqli_query($con,"SELECT * FROM tbl_tpn WHERE tpn_no='". $_GET["tpn"] ."'");
    }
    $row = mysqli_fetch_array($result);

    //$html = '<page footer="page">'; // for auto page number
    $html = '<page style="font-size: 12pt"><page_header></page_header><page_footer style="text-align: right">Page [[page_cu]] of [[page_nb]]</page_footer>';
    $html .= $row["tpn_src"] . '';
    $html .='</page>';
    require_once(dirname(__FILE__).'/html2pdf.class.php');
    
    //$html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(31.75, 25.4, 31.75, 25.4));

    $html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(13, 18, 13, 18));
//    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//    $html2pdf->setAuthor('allan');
    //$html2pdf->setEncoding("ISO-8859-1");
    //$html2pdf->addFont('tahoma');
    $html2pdf->pdf->SetTitle('TPN');
    $html2pdf->pdf->SetAuthor('Voodoo Creative');

    // protect pdf
    $html2pdf->pdf->SetProtection(array('print'), '', 'v00d00cR3@t1v3', 0, null);
    $html2pdf->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    $html2pdf->WriteHTML($html);
    $html2pdf->Output($_GET["tpn"].".pdf"); //output to file
}

