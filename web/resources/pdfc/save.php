<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */
if($_SERVER['SERVER_NAME'] == 'localhost'){
    $con=mysqli_connect("localhost","root","root","cls");
}else{
//    $con=mysqli_connect("localhost","clsvcdev","dreNca8gW4","clsvcdev");
    $con=mysqli_connect("245e87e26440aa9e47f3c987021b05cc73e5943b.rackspaceclouddb.com","clsvcdev","dreNca8gW4","capitallinkservices");
}

$result = mysqli_query($con,"SELECT * FROM tbl_tpn WHERE tpn_no='". $_GET["tpn"] ."'");
$row = mysqli_fetch_array($result);



//$html = '<page footer="page">'; // for auto page number
$html = '<page><page_header></page_header><page_footer style="text-align: right">Page [[page_cu]] of [[page_nb]]</page_footer>';
$html .= $row["tpn_src"] . "";
$html .='</page>';
require_once(dirname(__FILE__).'/html2pdf.class.php');
$html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(20, 20, 20, 20));
//$html2pdf->setEncoding("ISO-8859-1");
$html2pdf->WriteHTML($html);
$html2pdf->Output("dev/".$_GET["tpn"].".pdf","F"); //output to file
