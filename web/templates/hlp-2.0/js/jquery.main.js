
if(typeof google !== 'undefined'){
	/*
	*
	*	Google Chart init
	*
	*/
	google.load('visualization', '1.1', {packages: ['corechart', 'bar', 'line']});
	google.setOnLoadCallback(drawChart);

}

function drawChart() {
	var lineOptions = {
		chartArea:{
			width:'85%',
			height: '80%'
		},colors: ['#6c4695'],
		lineWidth: 1,
		pointSize: 9,
		title: '',
		titlePosition: 'in', 
		titleTextStyle: {color:'#666'},
		enableInteractivity: false,
		legend: {position: 'none'},
		vAxis: {textStyle:{color: '#666'}},
		xAxis: {textStyle:{color: '#666'}},
		gridlineColor: '#fff'
	};
	var columnOptions = {
        title: '',
        titlePosition: 'in', 
        titleTextStyle: {color:'#666'},
        chartArea:{width:'85%',height: '80%'},
        colors: ['#6c4695'],
        enableInteractivity: false,
        vAxis: {
        	textStyle:{color: '#666'},
        	minValue: 0
        },
        xAxis: {textStyle:{color: '#666'}},
        bar: {groupWidth: '90%'},
        legend: { position: 'none' },
        gridlineColor: '#fff'
    };
        /* `lk: 150622*/
        var report_charts = new Array();
	jQuery('.line-chart, .bar-chart').each(function(){
		var holder = jQuery(this);
		var chartBox = holder.find('.chart');

		function initGraph(){
			//create data table object
			var dataTable = new google.visualization.DataTable();
			var values = holder.data('values');
			var options = {};
			if(holder.hasClass('line-chart')){
				jQuery.extend(options, lineOptions);
			}
			else{
				jQuery.extend(options, columnOptions);
			}

			if(values){
				if(values.title) options.title = values.title;
				if(values.viewWindow) options.vAxis.viewWindow = values.viewWindow;
				if(values.data) {
					//define columns
					dataTable.addColumn('string','Month');
					dataTable.addColumn('number', 'KG');
					//define rows of data
					dataTable.addRows(values.data);
				}
			}
			 
			var chart = new google.visualization[(holder.hasClass('line-chart') ? 'LineChart' : 'ColumnChart')](chartBox[0]);
			chart.draw(dataTable, options);
			if(values.title) jQuery('text:contains(' + values.title + ')',holder).attr('class','title').attr({'x':'1', 'y':'19.5'});
			var yAxisValues = jQuery('text[text-anchor="end"]',holder);
			yAxisValues.last().remove();
			yAxisValues.first().remove();

			/* `lk: 150622 */
                        report_charts.push( chart.getImageURI() );
		}

		initGraph();

		var timer;
		jQuery(window).on('resize orientationchange',function(){
			clearTimeout(timer);
			timer = setTimeout(initGraph,300);
		});
	});
        
        /* `lk: 150622 */
        if (typeof(chart_session_path) != "undefined"){
            $.ajax({
                url: chart_session_path,
                method: 'POST',
                data: {
                    'charts': report_charts
                },
                success: function(response) {
                },
                error:function(){
                }
            });
        }
}

