$(document).ready(function(){
	/*============================================
	Page Preloader
	==============================================*/
	
//	$(window).load(function(){
//		$('#page-loader').fadeOut(500);
//	});	
    
    

	
	/*============================================
	Navigation Functions
	==============================================*/
	if ($(window).scrollTop()< 15){
		$('body').removeClass('scrolled');
	}
	else{
		$('body').addClass('scrolled');  
	}

	$(window).scroll(function(){
		if ($(window).scrollTop()< 15){
			$('body').removeClass('scrolled');
		}
		else{
			$('body').addClass('scrolled'); 
		}
	});
	
	$('a.scrollto').click(function(e){
		e.preventDefault();
		var target =$(this).attr('href');
		$('html, body').stop().animate({scrollTop: $(target).offset().top}, 2000, 'easeInOutExpo',
			function(){window.location.hash =target;});
			
		if ($('.navbar-collapse').hasClass('in')){
			$('.navbar-collapse').removeClass('in').addClass('collapse');
		}
	});
    
//     /*============================================
//	   Pie-Chart
//           `lk: 261015 moved to /js/club/dashboard.js
//	==============================================*/
//    var WindowWidth = $(window).width();
//    if(WindowWidth > 991){
//    $('.percentage').each(function(){
//		$(this).easyPieChart({
//				size:160,
//				animate: 2000,
//				lineCap:'butt',
//				scaleColor: false,
//				trackColor: '#d3d7e2',
//				barColor: '#6c4695',
//				lineWidth: 8,
//				easing:'easeOutQuad'
//			});
//		});
//    }
//    else{
//        $('.percentage').each(function(){
//            $(this).easyPieChart({
//                    size:125,
//                    animate: 2000,
//                    lineCap:'butt',
//                    scaleColor: false,
//                    trackColor: '#d3d7e2',
//                    barColor: '#6c4695',
//                    lineWidth: 8,
//                    easing:'easeOutQuad'
//                });
//            });
//    }
    /*============================================
	Bootstrap-select
	==============================================*/
    
    $('.week-selectpicker').selectpicker();  
    
    
    /*============================================
	Week-day-slider
	==============================================*/
    
    $("#week-day").owlCarousel({
        items: 7,
        singleItem: true,
        pagination: false,
        slideSpeed:2000
    });
    
    var weekDayFilter = $("#week-day").data('owlCarousel');
    
    $('.week-day-filter-left').click(function(){
		weekDayFilter.prev();
	});
	
	$('.week-day-filter-right').click(function(){
		weekDayFilter.next();
	});
    
    /*============================================
	Mobile Week-day-slider
	==============================================*/
    
    $("#mob-program-slider").owlCarousel({
        items: 7,
        singleItem: true,
        pagination: false,
        slideSpeed:2000,
        dragBeforeAnimFinish : false,
        mouseDrag: false,
        touchDrag: false
    });
    
    var mobProgramFilter = $("#mob-program-slider").data('owlCarousel');
    
    $('.week-day-filter-left').click(function(){
            mobProgramFilter.prev();
    });

    $('.week-day-filter-right').click(function(){
            mobProgramFilter.next();
    });
    
    /*============================================
	Week-Program-slider
        `lk: 241015 moved to /js/dashboard.js
	==============================================*/
    
//    $("#program-slider").owlCarousel({
//        //items: 7,
//        singleItem: true,
//        pagination: false,        
//        dragBeforeAnimFinish : false,
//        mouseDrag: false,
//        touchDrag: false,
//        slideSpeed:2000
//    });
//    
//    var weekProgram = $("#program-slider").data('owlCarousel');
//    
//    $('.previous-prog').click(function(){
//		weekProgram.prev();
//	});
//	
//    $('.next-prog').click(function(){
//            weekProgram.next();
//    });
	
	/*============================================
	Waypoints Animations
	==============================================*/
	$(window).load(function(){
		
		$('.scrollimation').waypoint(function(){
			$(this).addClass('in');
		},{offset:'95%'});
		
	});
	
	/*============================================
	Refresh scrollSpy function
	==============================================*/
	function scrollSpyRefresh(){
		setTimeout(function(){
			$('body').scrollspy('refresh');
		},1000);
	}
    
   
	/*============================================
	Refresh waypoints function
	==============================================*/
	function waypointsRefresh(){
		setTimeout(function(){
			$.waypoints('refresh');
		},1000);
	}
    
    
    
//    $.validator.setDefaults({
//        /*OBSERVATION (1): note the options used for "ignore"*/
//        ignore: ':disabled,:hidden',
//        /*...other options omitted to focus on the OP...*/
//
//        errorPlacement: function (error, element) {
//            if (element.hasClass('selectpicker')) {
//                element.next().addClass('error');
//            }
//        }
//    });
    
    /*==============================================
    checkbox click for week items
    ==============================================*/
    
    $('.sortable-list li input[type="checkbox"]').click(function(){
        $(this).closest('li').toggleClass('completed');
    });
});