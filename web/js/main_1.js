/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    if($('.date-control').length > 0){
        $('.date-control').datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "1950:"+ (new Date).getFullYear(),
            dateFormat: "dd/mm/yy"
        });
    }
});

$("body").on("click", ".alert .close", function(){
    var $this = $(this);
    $this.closest("div.alert").hide();
});

$("body").on("click", "form .submit", function(){
   var $this = $(this);
   $this.closest("form").submit();
});


//==============================================================================
// EMAIL THIS TEST
//==============================================================================

$(".email-this-test").click(function(){
    var $this = $(this);
    $('form#emailTest').find('input[name=recipients]').val( $this.attr('data-email') );
    $('form#emailTest').find('input[name=client_test_id]').val( $this.attr('data-test-id') );
    //$("#emailTestModal").modal("show");
    $("#emailTestModal").find(".send").removeAttr("disabled");
});

$("#emailTestModal").find(".send").click(function(){ 
   var $this = $(this);
    var $formData =  $('form#emailTest').serialize() + "&sajax=1";
    var $caption = $this.html();
    $this.attr('disabled', 'disabled');
    $this.html('Sending ...');
    $.ajax({
        type: "post",
        url: $("form#emailTest").attr("action"),
        data: $formData,
//        data: {
//            'recipients': $('form#emailTest').find('input[name=recipients]').val(),
//            'client_test_id': $('form#emailTest').find('input[name=client_test_id]').val()
//        },
        success: function(result) {
            $this.removeAttr('disabled');
            $this.html($caption);
            if(result == 'success'){
                //$("#emailTestModal").modal("hide");
                $(".email-modal-close.close").click();
            }else{
                alert("The system can't send the email. Please try again!");
            }
            
            
        }
    }); 
});


//=========================================================================================
// LOCATION CHANGE
//=========================================================================================
$("select.location-option").change(function(){
	$this = $(this);
	$this.closest("form").submit();
});



$(function() {
  $('body').on('keydown','input[type=number]', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
});

//=========================================================================================
// AUTO LOGOUT
//=========================================================================================
$(document).ready(function(){ 
    if( $("body.home").length == 0 ){
        $(document).idleTimeout({ 
            inactivity: 3600000, 
            noconfirm: false, 
            sessionAlive: 300000,
            alive_url: dashboard_url,
            redirect_url: login_url,
            logout_url: logout_url,
            showDialog: false
        }); 
    }
});