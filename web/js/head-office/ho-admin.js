/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(".delete-admin").click(function(){ 
    var $this = $(this);
    $( "#dialog-confirm" ).dialog({
        resizable: false,
        height:240,
        modal: true,
        buttons: {
            "Delete item": function() {
                $("input[name=ho_admin_id]").val($this.attr('data-id'));
                $("form#deleteHoAdmin").submit();
            },
            Cancel: function() {
                $( this ).dialog( "close" );
            }
        }
    });
});
