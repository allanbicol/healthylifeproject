$(document).ready(function(){
    var $table = $('#example').DataTable({
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "order": [[ 1, "asc" ]],
        "columnDefs": [
            { 
                "orderable": false, 
                "targets": 0 
            },
            {
                "targets": [ 8, 9, 10, 11 ],
                "visible": false
            }
        ]
    });
    
//    tinymce.init({
//        selector: 'textarea.wysiwyg-editor',
//        menubar: false,
//        toolbar: 'undo redo | bold italic underline | link | bullist numlist',
//    });
//    
    // Apply the search
    $('select[name=club]').on( 'change', function () {
        if(this.value != ''){
            var query = "((  )|^)" + regex_escape(this.value) + "((  )|$)";
            $table
                .columns( 8 )
                .search( query, true, false  )
                .draw();
        }else{
            $table
                .columns( 8 )
                .search( this.value  )
                .draw();
        }
    } );
    
    $('select[name=s_aia_approved]').on( 'change', function () {
        if(this.value != ''){
            var query = "((  )|^)" + regex_escape(this.value) + "((  )|$)";
            $table
                .columns( 9 )
                .search( query, true, false  )
                .draw();
        }else{
            $table
                .columns( 9 )
                .search( this.value  )
                .draw();
        }
    } );

    $('select[name=status]').on( 'change', function () {
        if(this.value != ''){
            var query = "((  )|^)" + regex_escape(this.value) + "((  )|$)";
            $table
                .columns( 10 )
                .search( query, true, false  )
                .draw();
        }else{
            $table
                .columns( 10 )
                .search( this.value  )
                .draw();
        }
    } );
    
    $('select[name=role]').on( 'change', function () {
        if(this.value != ''){
            var query = "((  )|^)" + regex_escape(this.value) + "((  )|$)";
            $table
                .columns( 11 )
                .search( query, true, false  )
                .draw();
        }else{
            $table
                .columns( 11 )
                .search( this.value  )
                .draw();
        }
    } );

});

function regex_escape(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}

$(".btn-bulk-email").click(function(){
    var $this = $(this);
    if($this.hasClass("btn-default")){
        return false;
    }
    tinymce.remove();
    
    setTimeout(function(){
        tinymce.init({
            selector: 'textarea.wysiwyg-editor',
            menubar: false,
            toolbar: 'undo redo | bold italic underline | link | bullist numlist',
        });
    },300);
    
});

$(".checkall").change(function(){
    var $this = $(this);
    
    $this.closest("table").find("tbody td").find("input[type=checkbox]").each(function(){
       var $checkbox = $(this); 
       if($this.is(":checked")){
           $checkbox.closest(".jcf-checkbox").removeClass("jcf-unchecked");
           $checkbox.closest(".jcf-checkbox").addClass("jcf-checked");
           $checkbox.prop("checked", true);
       }else{
           $checkbox.closest(".jcf-checkbox").addClass("jcf-unchecked");
           $checkbox.closest(".jcf-checkbox").removeClass("jcf-checked");
           $checkbox.prop("checked", false);
       }
    });
    
    var $clients = getCheckedItems($this.closest("table"));
    if( $clients.length > 0 ){
        $(".btn-bulk-email").removeAttr("disabled");
        $(".btn-bulk-email").removeClass("btn-default");
        $(".btn-bulk-email").addClass("btn-primary");
    }else{
        $(".btn-bulk-email").attr("disabled","disabled");
        $(".btn-bulk-email").removeClass("btn-primary");
        $(".btn-bulk-email").addClass("btn-default");
    }
    
    $("input[name=clients]").val( JSON.stringify($clients) );
});

$("table#example").find("tbody td").find("input[type=checkbox]").change(function(){
    var $this = $(this);
    var $clients = getCheckedItems($this.closest("table"));
    if( $clients.length > 0 ){
        $(".btn-bulk-email").removeAttr("disabled");
        $(".btn-bulk-email").removeClass("btn-default");
        $(".btn-bulk-email").addClass("btn-primary");
    }else{
        $(".btn-bulk-email").attr("disabled","disabled");
        $(".btn-bulk-email").removeClass("btn-primary");
        $(".btn-bulk-email").addClass("btn-default");
    }
    
    $("input[name=clients]").val( JSON.stringify($clients) );
});

$(".btn-send-bulk-email").click(function(){
   var $this = $(this);
   $this.text("Sending...");
//   $this.parent().find("button").attr("disabled", "disabled");
});

$(".alert").find(".close").click(function(){
   var $this = $(this);
   $this.closest(".alert").hide();
});

function getCheckedItems($table){
    var $clients = [];
    $table.find("tbody td").find("input[type=checkbox]").each(function(){
       var $checkbox = $(this); 
       if($checkbox.is(":checked")){
           $clients.push($checkbox.val());
       }
    });
    
    return $clients;
}