/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(".delete-item").click(function(){
    var $this = $(this);
    $( "#dialog-confirm" ).dialog({
        resizable: false,
        height:240,
        modal: true,
        buttons: {
            "Delete item": function() {
                $("input[name=club_admin_id]").val($this.attr('data-id'));
                $("form#delete").submit();
            },
            Cancel: function() {
                $( this ).dialog( "close" );
            }
        }
    });
});

$(".delete-client-item").click(function(){
    var $this = $(this);
    $( "#dialog-confirm" ).dialog({
        resizable: false,
        height:240,
        modal: true,
        buttons: {
            "Delete item": function() {
                $("input[name=club_client_id]").val($this.attr('data-id'));
                $("input[name=club_client_email]").val($this.attr('data-email'));
                $("input[name=type]").val('club');
                $("form#delete").submit();
            },
            Cancel: function() {
                $( this ).dialog( "close" );
            }
        }
    });
});

$(".delete-12week-client-item").click(function(){
    var $this = $(this);
    $( "#dialog-confirm" ).dialog({
        resizable: false,
        height:240,
        modal: true,
        buttons: {
            "Delete item": function() {
                $("input[name=club_client_id]").val($this.attr('data-id'));
                $("input[name=club_client_email]").val($this.attr('data-email'));
                $("input[name=type]").val('12week');
                $("form#delete").submit();
            },
            Cancel: function() {
                $( this ).dialog( "close" );
            }
        }
    });
});

$(".assign_pass").click(function(){
    $('input[name="slug"]').val('assign');
    $('#pass_management').submit();
});

$(".reset_pass").click(function(){
    $('input[name="slug"]').val('reset');
    $('#pass_management').submit();
});