$(".btn-detailed-reports").click(function(){
   $("form.report-from").attr('action', detailed_report);
});

$(".btn-summary-reports").click(function(){
   $("form.report-from").attr('action', report);
});

function requestData(){
    $(".detailed-results").html('<div class="spinner"><img src="'+ asset_img +'/spinner.gif">loading...</div>');
    $.ajax({
        method: "POST",
        url: detailed_report_ajax,
        success: function(result) {
            //alert(jQuery.parseJSON(result));
            $(".detailed-results").html(result);
            //iniTialiseDataTable();
        }
    }); 
}

function iniTialiseDataTable(){
    
    $('#example').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": detailed_report_ajax
        },
        "columns": [
            { "data": "fname"},
            { "data": "lname"},
            { "data": "email" },
            { "data": "gender" },
            { "data": "age" },
            { "data": "weight" },
            { "data": "height" },
            { "data": "test_datetime_finalised" },
            { "data": "lite_test_id",
              "render":function ( data, type, full, meta ) {
                            return '<a style="color:#593680;text-decoration:underline;" href="'+view_test_path+'?test='+data+'">View</a>';
                        } 
            }
        ],
        "scrollX": true,
//        "columnDefs": [
//            { "width": "15%", "targets": 0 }
//        ],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            
            
        }
    } );
}
$(document).ready(function() {
//    requestData();
    //alert('asfasf');
    //requestData();
    iniTialiseDataTable();
    
});

