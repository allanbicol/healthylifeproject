$(".btn-detailed-reports").click(function(){
   $("form.report-from").attr('action', detailed_report);
});

$(".btn-summary-reports").click(function(){
   $("form.report-from").attr('action', report);
});

function requestData(){
    $(".detailed-results").html('<div class="spinner"><img src="'+ asset_img +'/spinner.gif">loading...</div>');
    $.ajax({
        method: "POST",
        url: detailed_report_ajax,
        success: function(result) {
            //alert(jQuery.parseJSON(result));
            $(".detailed-results").html(result);
            //iniTialiseDataTable();
        }
    }); 
}

function iniTialiseDataTable(){
    
    $('#example').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": detailed_report_ajax
        },
        "columns": [
            { "data": "club_client_id" ,
              "render":function ( data, type, full, meta ) {
                    return '<a class="link-in-table" href="'+client_view_path+'?ho=1&cid='+data+'" target="_blank">'+data+'</a>';
                }
            },
            { "data": "fname" },
            { "data": "lname" },
            { "data": "email" },
            { "data": "club_name" },
            { "data": "birthdate" },
            { "data": "gender" },
            { "data": "height" },
            { "data": "weight" },
            { "data": "status" },
            { "data": "program_type" },
            { "data": "registered_date" }
            
        ],
        "scrollX": true,
        "columnDefs": [
//            { "width": "3%", "targets": 0 }
        ],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            
            
        }
    } );
}
$(document).ready(function() {
//    requestData();
    //alert('asfasf');
    //requestData();
    iniTialiseDataTable();
    
});

