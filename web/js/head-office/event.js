/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(".delete-event").click(function(){
    var $this = $(this);
    $( "#dialog-confirm" ).dialog({
        resizable: false,
        height:240,
        modal: true,
        buttons: {
            "Delete item": function() {
                $("input[name=event_id]").val($this.attr('data-id'));
                $("form#deleteEvent").submit();
            },
            Cancel: function() {
                $( this ).dialog( "close" );
            }
        }
    });
});
