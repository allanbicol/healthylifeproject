var datatable = '';
$(".btn-detailed-reports").click(function(){
   $("form.report-from").attr('action', report);
});

$(".btn-summary-reports").click(function(){
   $("form.report-from").attr('action', detailed_report);
});

function requestData(){
    request_params = request_params.replace(/&amp;/g, '&');
    $(".detailed-results").html('<div class="spinner"><img src="'+ asset_img +'/spinner.gif">loading...</div>');
    $.ajax({
        type: "get",
        url: detailed_report_ajax+ "?"+request_params,
        success: function(result) {
            $(".detailed-results").html(result);
            iniTialiseDataTable();
        }
    }); 
}

function iniTialiseDataTable(){
    request_params = request_params.replace(/&amp;/g, '&');
    var hidden_columns = new Array();
    $("#frmColumnFilter input:checkbox:not(:checked)").each(function(){
        hidden_columns.push( parseInt($(this).attr("data-column")) );
    });
    
    datatable = $('#example').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": detailed_report_ajax+ "?"+request_params,
            "type": "post"
        },
        "columns": [
            { "data": "club_name" },
            { "data": "name" },
            { "data": "bmi_improvement" },
            { "data": "years_added" },
            { "data": "cm_lost" },
            { "data": "kg_lost" },
            { "data": "body_fat_reduction" },
            { "data": "vo2_improvement" },
            { "data": "squat_test_improvement" },
            { "data": "standing_functional_reach_improvement" },
            { "data": "stork_test_improvement" },
            { "data": "abdominal_test_improvement" },
            { "data": "vigorous_pa_per_week" },
            { "data": "strength_pa_per_week" },
            { "data": "smoking_improvement" },
            { "data": "standard_alcohol_per_week" },
            { "data": "maximum_alcohol_one_sitting" },
            { "data": "nutrition_score_improvement" },
            { "data": "mental_health_improvement" },
            { "data": "risk_profile_improvement" }
        ],
        "scrollX": true,
        "columnDefs": [
            {
                "render": function ( data, type, row ) {
                    return data +'%';
                },
                "targets": [17, 18, 19]
            },
            {
                "targets": hidden_columns,
                "visible": false
            }
        ],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            
            /*
             * YEARS ADDED
             */
            // Total over this page
            page_bmi_total = api
                .column( 2, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            page_bmi_total = page_bmi_total.toFixed(1);
            page_bmi_total = page_bmi_total.replace(".0","");
            
            // Update footer
            $( api.column( 2 ).footer() ).html(
//                ''+page_bmi_total +' / '+ bmi_total +''
                page_bmi_total
            );
    
 
            /*
             * YEARS ADDED
             */
 
            // Total over this page
            page_years_added_total = api
                .column( 3, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            page_years_added_total = page_years_added_total.toFixed(1);
            page_years_added_total = page_years_added_total.replace(".0","");
            
            // Update footer
            $( api.column( 3 ).footer() ).html(
//                ''+page_years_added_total +' / '+ years_added_total +''
                page_years_added_total
            );
    
    
            /*
             * CM LOST
             */
            // Total over this page
            page_cm_lost_total = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            page_cm_lost_total = page_cm_lost_total.toFixed(1);
            page_cm_lost_total = page_cm_lost_total.replace(".0","");
            // Update footer
            $( api.column( 4 ).footer() ).html(
//                ''+page_cm_lost_total +' / '+ cm_lost_total +''
                page_cm_lost_total
            );
    
    
            /*
             * KG LOST
             */
 
            // Total over this page
            page_kg_lost_total = api
                .column( 5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            page_kg_lost_total = page_kg_lost_total.toFixed(1);
            page_kg_lost_total = page_kg_lost_total.replace(".0","");
            // Update footer
            $( api.column( 5 ).footer() ).html(
//                ''+page_kg_lost_total +' / '+ kg_lost_total +''
                page_kg_lost_total
            );
    
    
            /*
             * BODY FAT % REDUCTION
             */
 
            // Total over this page
            page_bfpr_total = api
                .column( 6, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
                
            page_bfpr_total = page_bfpr_total.toFixed(1);
            page_bfpr_total = page_bfpr_total.replace(".0","");
 
            // Update footer
            $( api.column( 6 ).footer() ).html(
//                ''+page_bfpr_total +' / '+ bfpr_total +''
                page_bfpr_total
            );
    
            
            /*
             * VO2 IMPROVEMENT
             */
            // Total over this page
            page_vo2_improvement_total = api
                .column( 7, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            page_vo2_improvement_total = page_vo2_improvement_total.toFixed(1);
            page_vo2_improvement_total = page_vo2_improvement_total.replace(".0","");
            // Update footer
            $( api.column( 7 ).footer() ).html(
//                ''+page_vo2_improvement_total +' / '+ vo2_improvement_total +''
                page_vo2_improvement_total
            );
    
            
            /*
             * SQUAT TEST IMPROVEMENT
             */
            // Total over this page
            page_squat_test_improvement_total = api
                .column( 8, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            page_squat_test_improvement_total = page_squat_test_improvement_total.toFixed(1);
            page_squat_test_improvement_total = page_squat_test_improvement_total.replace(".0","");
            // Update footer
            $( api.column( 8 ).footer() ).html(
//                ''+page_squat_test_improvement_total +' / '+ squat_test_improvement_total +' '
                page_squat_test_improvement_total
            );
    
    
            /*
             * STANDING FUNCTIONAL REACH IMPROVEMENT
             */
            // Total over this page
            page_standing_functional_reach_improvement_total = api
                .column( 9, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            page_standing_functional_reach_improvement_total = page_standing_functional_reach_improvement_total.toFixed(1);
            page_standing_functional_reach_improvement_total = page_standing_functional_reach_improvement_total.replace(".0","");
            // Update footer
            $( api.column( 9 ).footer() ).html(
//                ''+page_standing_functional_reach_improvement_total +' / '+ standing_functional_reach_improvement_total +''
                page_standing_functional_reach_improvement_total
            );
    
            /*
             * STORK TEST IMPROVEMENT
             */
            // Total over this page
            page_storktest_improvement_total = api
                .column( 10, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            page_storktest_improvement_total = page_storktest_improvement_total.toFixed(1);
            page_storktest_improvement_total = page_storktest_improvement_total.replace(".0","");
            // Update footer
            $( api.column( 10 ).footer() ).html(
//                ''+page_storktest_improvement_total +' / '+ storktest_improvement_total +''
                page_storktest_improvement_total
            );
    
    
            /*
             * Abdominal test improvement
             */
            // Total over this page
            page_abdominal_test_improvement_total = api
                .column( 11 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            page_abdominal_test_improvement_total = page_abdominal_test_improvement_total.toFixed(1);
            page_abdominal_test_improvement_total = page_abdominal_test_improvement_total.replace(".0","");
            // Update footer
            $( api.column( 11 ).footer() ).html(
//                ''+page_abdominal_test_improvement_total +' / '+ abdominal_test_improvement_total +''
                page_abdominal_test_improvement_total
            );
    
    
            /*
             * Increase in days of physical activity per week
             */
            // Total over this page
            page_idpa_total = api
                .column( 12 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            page_idpa_total = page_idpa_total.toFixed(1);
            page_idpa_total = page_idpa_total.replace(".0","");
            // Update footer
            $( api.column( 12 ).footer() ).html(
//                ''+page_idpa_total +' / '+ idpa_total +''
                page_idpa_total
            );
    
            /*
             * Increase in days of strength activity per week
             */
            // Total over this page
            page_idsa_total = api
                .column( 13 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            page_idsa_total = page_idsa_total.toFixed(1);
            page_idsa_total = page_idsa_total.replace(".0","");
            // Update footer
            $( api.column( 13 ).footer() ).html(
//                ''+page_idsa_total +' / '+ idsa_total +''
                page_idsa_total
            );
    
            /*
             * SMOKING IMPROVEMENT
             */
            // Total over this page
            page_smoking_improvement_total = api
                .column( 14 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            page_smoking_improvement_total = page_smoking_improvement_total.toFixed(1);
            page_smoking_improvement_total = page_smoking_improvement_total.replace(".0","");
            // Update footer
            $( api.column( 14 ).footer() ).html(
//                ''+page_smoking_improvement_total +' / '+ smoking_improvement_total +''
                page_smoking_improvement_total
            );
    
            /*
             * Reduction in standard Alcoholic drinks per week
             */
            // Total over this page
            page_alcohol_reduction1_total = api
                .column( 15 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            page_alcohol_reduction1_total = page_alcohol_reduction1_total.toFixed(1);
            page_alcohol_reduction1_total = page_alcohol_reduction1_total.replace(".0","");
            // Update footer
            $( api.column( 15 ).footer() ).html(
//                ''+page_alcohol_reduction1_total +' / '+ alcohol_reduction1_total +''
                page_alcohol_reduction1_total
            );
    
            /*
             * Reduction in maximum number of standard drinks in one sitting
             */
            // Total over this page
            page_alcohol_reduction2_total = api
                .column( 16 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            page_alcohol_reduction2_total = page_alcohol_reduction2_total.toFixed(1);
            page_alcohol_reduction2_total = page_alcohol_reduction2_total.replace(".0","");
            // Update footer
            $( api.column( 16 ).footer() ).html(
//                ''+page_alcohol_reduction2_total +' / '+ alcohol_reduction2_total +''
                page_alcohol_reduction2_total
            );
    
            /*
             * Nutrition score improvement
             */
            // Total over this page
            page_nutrition_score_total = api
                .column( 17 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            page_nutrition_score_total = page_nutrition_score_total.toFixed(1);
            page_nutrition_score_total = page_nutrition_score_total.replace(".0","");
            // Update footer
            $( api.column( 17 ).footer() ).html(
//                ''+page_nutrition_score_total +' / '+ nutrition_score_total +''
                page_nutrition_score_total + '%'
            );
    
            /*
             * Mental Health improvement
             */
            // Total over this page
            page_mental_health_total = api
                .column( 18 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            page_mental_health_total = page_mental_health_total.toFixed(1);
            page_mental_health_total = page_mental_health_total.replace(".0","");
            // Update footer
            $( api.column( 18 ).footer() ).html(
//                ''+page_mental_health_total +' / '+ mental_health_total +''
                page_mental_health_total + '%'
            );
    
            /*
             * Risk profile improvement
             */
 
            // Total over this page
            page_risk_profile_total = api
                .column( 19 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            page_risk_profile_total = page_risk_profile_total.toFixed(1);
            page_risk_profile_total = page_risk_profile_total.replace(".0","");
            // Update footer
            $( api.column( 19 ).footer() ).html(
//                ''+page_risk_profile_total +' / '+ risk_profile_total +''
                page_risk_profile_total + '%'
            );
        }
    } );
    
    $("a.btn-column-filter").prependTo("#example_wrapper");
}
$(document).ready(function() {
//    requestData();
    iniTialiseDataTable();
});

$("button.btn-save-report-column-filter").click(function(){
    var $this = $(this);
    var $caption = $this.text();
    
    $this.text("Saving...");
    $.ajax({
        type: $('#frmColumnFilter').attr('method'),
        url: $('#frmColumnFilter').attr('action'),
        data: $('#frmColumnFilter').serialize(),
        success: function (response) {
            if( response == 'success' ){
                $this.text( $caption );
                $(".column-filter-modal-close").click();
                
                $('#frmColumnFilter input:checkbox').each(function () {
                    var $checkbox = $(this);
                    // Get the column API object
                    var column = datatable.column( $checkbox.attr("data-column") );
                    if( $checkbox.is(":checked") ){
                        column.visible( true );
                    }else{
                        column.visible( false );
                    }
               });
                
                

            }else{
                alert("The server cannot respond to request. Please try again!");
                $this.text( $caption );
            }
            
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("The server cannot respond to request. Please try again!");
            $this.text( $caption );
        }
    });
});

$(".btn-column-filter-modal-close").click(function(){
    $(".column-filter-modal-close").click();
});
