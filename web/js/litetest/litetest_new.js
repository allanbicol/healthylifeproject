//$(function() {
//    $( "#birthdate" ).datepicker();
//  });
//  $( "#birthdate" ).change(function() {
//    $( "#birthdate" ).datepicker( "option", "dateFormat", "dd/mm/yy" );
//  });
$(document).ready(function(){
    if($('.date-control').length > 0){
        $('.date-control').datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "1950:"+ (new Date).getFullYear(),
            dateFormat: "dd/mm/yy"
            //maxDate: "-16Y"
        });
    }
    postcode_change();
    check = $("#s_contact").is(":checked");
            if(check) {
                $('#div_contact').show();
            } else {
                $('#div_contact').hide();
            }
});
$(document).ready(function() {
    var form = $("#testForm");
    
    form.steps({
        headerTag: "h1",
        bodyTag: "fieldset",
        transitionEffect: "slideLeft",
        enableAllSteps: true,
        enableFinishButton: (form.hasClass('finalised')) ? false : true,
        onStepChanging: function (event, currentIndex, newIndex)
        { 
//            // Allways allow previous action even if the current form is not valid!
//            if (currentIndex > newIndex)
//            {
//                return true;
//            }
//
//            // Needed in some cases if the user went back (clean up)
//            if (currentIndex < newIndex)
//            { 
//                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
//            }
//
//            if(form.valid()){ 
//                $('.bootstrap-select').removeClass('error');
//            }
//
//            return form.valid();
                return true;
        },
        onStepChanged: function (event, currentIndex, priorIndex)
        { 
//            if( (form.hasClass('finalised')) ){
//                
//            }else{
//                $.ajax({
//                    type: $('#testForm').attr('method'),
//                    url: $('#testForm').attr('action'),
//                    data: $('#testForm').serialize(),
//                    success: function (data) {
//                    }
//                });
//            }
//            

        },
        onFinishing: function (event, currentIndex)
        {
//            $(".termsconditions").find("input[name=termsconditions]").addAttr("required","required");
            $("input[name=redirect]").val( $(location).attr('href') );
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        { 
            $("form#testForm").find("input[name=submit_type]").val("finalise");
            $("form#testForm").submit();
        }
    });
});

$(".nutrition-items > .item").each(function(){
    var $item = $(this);
    $item.find("input[type=checkbox]").change(function()
    { 
        if($(this).hasClass('opt1')){
            $item.find(".opt2").prop('checked',false);
            $item.find(".opt1").prop('checked',true);
        }else{ 
            $item.find(".opt1").prop('checked',false);
            $item.find(".opt2").prop('checked',true);
        }
        
    });
});

$(".help").click(function(){  
    var $this = $(this);
    var $field = $this.attr('data-field');
    var $window = $(window);
    
    $('#mdlHelp').modal({
        backdrop: 'static',
        keyboard: false
    });
    
    $(".help-container").html('<center><img src="'+ asset_img +'/spinner.gif"/></center>');
    
    $.ajax({
        url: get_help_path,
        method: 'POST',
        data: {
            'field': $field
        },
        success: function(response) {
            $(".help-container").html(response);
            $(window).resize();
        },
        error:function(){
            $(".help-container").html('error found');
        }
    });
});

// Set our timer global and give a timeout for stop typing.
var timer, timeout = 500;
$(document).ready(function(){
    // Check VO2
    $("select[name=physiological_vo2_test_type]").change();
    //removelater
    $("input[name=biometric_waist_value]").keyup();
});

// GET WEIGHT
$("input[name=weight], input[name=height]").on('keyup',function(){
    var $this = $(this);
    // Clear timer if it's set.
    if (typeof timer != undefined)
    clearTimeout(timer);

    // Set status to show we're typing.
    $("input[name=bmi]").val("calculating...");
   
    // Set status to show we're done typing on a delay.
    timer = setTimeout(function()
    {
        $.ajax({
            url: get_bmi_result_path,
            method: 'POST',
            data: {
                'weight': $("input[name=weight]").val(),
                'height': $("input[name=height]").val()
            },
            success: function(response) {
                $("input[name=bmi]").val(response);
                
                // update those fields that are weight dependent
                $(".weight-dependent").keyup();
            },
            error:function(){

            }
        });
    }, timeout);
});


$("#postcode").keyup(function(){
    postcode_change();
});

function postcode_change(){
    var $this = $("#postcode").val();
    if($this.length >=4 ){
        $.ajax({
            url: get_load_nearest_club_path,
            method: 'POST',
            data: {'postcode': $this
            },
            success: function(response) {
                var data = jQuery.parseJSON(response);
                var result= '';
                if(data.length !=0){
                    var chk = '';
                    for(i=0;i<data.length;i++){
                            if(i==0){
                                chk = 'checked="checked"';
                            }else{
                                chk='';
                            }
                            result += '<div class="form-group" id="result"> '+ 
                                        '<div style="text-decoration: underline;"><b><span id="club_name">'+data[i].club_name+'</span></b>'+
                                      '</div>'+
                                            '<small><span id="address" style="color:#0c9474;">'+data[i].address+', '+data[i].city+', '+data[i].state+' Australia</span></small><br/>'+
                                            '<small style="word-wrap:break-word;"><input style="margin-left: 0px;vertical-align:middle;" onclick="func(this);" class="chb" type="checkbox" '+chk+' name="allow_premium" value="'+data[i].club_id+'" /> I would like more information about the Premium Healthy Life Assessment from my nearest community partner.</small>'+
                                    '</div>';
    
                    }
                }else{
                    result +='No result';
                }
                $("#result_container").html(result);
            },
            error:function(){

            }

        });
    }else{
        $("#default").show();
        $("#result").hide();
    }
}

function func(checkBox) {
        var inputs = document.getElementsByTagName("input");
        
        if (checkBox.checked==true) {
            for(var j = 0; j < inputs.length; j++) {
              if(checkBox){
                if(inputs[j].type == "checkbox" && (inputs[j].className == "chb")){
                   inputs[j].checked = false;
                } 
              }    
            }
            checkBox.checked = true;
        }else{
            checkBox.checked = false;
        }
        
    }

var check;
$("#s_contact").on("click", function(){
    check = $("#s_contact").is(":checked");
    if(check) {
        $('#div_contact').show();
    } else {
        $('#div_contact').hide();
    }
}); 