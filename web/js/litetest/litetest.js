// DISPLAY LIFESTYLE QUESTIONNAIRE
$(".btn-questionnaire").click(function(){
    var $this = $(this);
    var $lifestyle_code = $this.attr('data-code');
    var $title = '';
    if($lifestyle_code == 'physical_activity'){
        $title = 'Physical Activity';
    }else if($lifestyle_code == 'smoking'){
        $title = 'Smoking Habits';
    }else if($lifestyle_code == 'alcohol'){
        $title = 'Alcohol Consumption';
    }else if($lifestyle_code == 'nutrition'){
        $title = 'Nutrition';
    }else if($lifestyle_code == 'mental_health'){
        $title = 'Mental Health';
    }else if($lifestyle_code == 'risk_profile'){
        $title = 'Risk Profile';
    }
    $("#lifeStyleQuestionnaireModal").find(".fancybox-overlay").css({
            width: 'auto', 
            height: 'auto',
            display: 'block',
            background: 'rgba(0, 0, 0, 0.65098)',
            'z-index': 99999
        });
    
    
    $("body").css("overflow","hidden");
    $("#lifeStyleQuestionnaireModal").find(".modal-title").html($title + ' Questionnaire');
    $("#lifeStyleQuestionnaireModal").find(".modal-body").html('<center><img src="'+ asset_img +'/spinner.gif"/></center>');
//    $("#lifeStyleQuestionnaireModal").css("display","block");
    $(window).resize();
	
    $.ajax({
        url: test_questionnaire_path,
        method: 'POST',
        data: {
            'code': $lifestyle_code, 
            'gender': $("select[name=gender]").val()
        },
        success: function(response) {
            $("#frmLifestyle").find(".modal-body").html(response);
            $(".btn-submit-lifestyle-answer").removeAttr('disabled');
            $(".btn-submit-lifestyle-answer").html('Save & Submit');
        },
        error:function(){
            $("#lifeStyleQuestionnaireModal").find(".modal-body").html('error found');
        }
    });
    
    //$("#lifeStyleQuestionnaireModal").modal("show");
});

// SUBMIT LIFESTYLE ANSWERS
$(".btn-submit-lifestyle-answer").click(function(){
    var $this = $(this);
    var $formData =  $('#frmLifestyle').serialize() + "&sajax=1";
    var $caption = $this.html();
    $this.attr('disabled', 'disabled');
    $this.html('Saving ...');
    $.ajax({
        type: "post",
        url: $("form#frmLifestyle").attr("action"),
        data: $formData,
        success: function(result) {
            var $obj = JSON.parse(result);
            $this.removeAttr('disabled');
            $this.html($caption);
            
            if($obj.result == 'success'){
                if($obj.s_complete){
                    $(".lifestyle-is-done.lifestyle-"+$obj.code).removeClass('glyphicon-unchecked');
                    $(".lifestyle-is-done.lifestyle-"+$obj.code).addClass('glyphicon-check');
                }
                
//                $(".questionnaire-modal-close.close").click();
                $("#lifeStyleQuestionnaireModal").find(".fancybox-overlay").css('display','none');
                $("body").css("overflow","auto");
            }else{
                
                alert( $obj.message );
            }
        }
    });
});



$("body").on("click",".questionnaire-modal-close.close", function(){
    
    $("#lifeStyleQuestionnaireModal").find(".fancybox-overlay").css('display','none');
    $("body").css("overflow","auto");
});



//==============================================================================
// FORMULAS
//==============================================================================
// Set our timer global and give a timeout for stop typing.
var timer, timeout = 500;

$(document).ready(function(){
    //removelater
    $("input[name=biometric_waist_value]").keyup();
});

// GET WEIGHT
$("input[name=weight], input[name=height]").on('keyup',function(){
    var $this = $(this);
	var $height = ( $.trim( $("input[name=height]").val() ) != '' ) ? $.trim( $("input[name=height]").val() ) : 0;
    // Clear timer if it's set.
    if (typeof timer != undefined)
    clearTimeout(timer);
	
	
	
    // Set status to show we're typing.
    $("input[name=bmi]").val("calculating...");
   
    // Set status to show we're done typing on a delay.
    timer = setTimeout(function()
    {
        $.ajax({
            url: get_bmi_result_path,
            method: 'POST',
            data: {
                'weight': $this.val(),
                'height': $("input[name=height]").val()
            },
            success: function(response) {
                $("input[name=bmi]").val(response);
                
                // update those fields that are weight dependent
                $(".weight-dependent").keyup();
            },
            error:function(){

            }
        });
    }, timeout);
});

// GET PHYSICAL ACTIVITY
$(".btn-submit-lifestyle-answer").click(function(){
    var $this = $(this);
    var $form = $("form#frmLifestyle");
    var answer_type = $form.find("input[name=answer_type]").val();
    
    // PHYSICAL ACTIVITY
    if(answer_type == 'physical-activity'){
        
        $("input[name=lifestyle_physical_activity]").val('calculating...');
        $.ajax({
            url: get_physical_activity_result_path,
            method: 'POST',
            data: {
				'default_gender' : $("select[name=gender]").val(),
                'moderate_vigorous_pa': $form.find("input#moderate_vigorous_pa").val(),
                'muscle_strenthening_pa': $form.find("input#muscle_strenthening_pa").val()
            },
            success: function(response) {
                $("input[name=lifestyle_physical_activity]").val(response);
                
            },
            error:function(){

            }
        });
        
        
    // SMOKING
    }else if(answer_type == 'smoking'){
        
        $("input[name=lifestyle_smoking]").val('calculating...');
        
        $.ajax({
            type: "post",
            url: get_smoking_path,
            data: $form.serialize() + "&sajax=1",
            success: function(response) {
                $("input[name=lifestyle_smoking]").val(response);
            }
        });
    
    
    // SMOKING
    }else if(answer_type == 'alcohol'){
        
        $("input[name=lifestyle_alcohol]").val('calculating...');
        $.ajax({
            url: get_alcohol_consumption_path,
            method: 'POST',
            data: {
                'gender': $("select[name=gender]").val(),
                'per_week_value': $form.find("input#per_week_value").val(),
                'one_sitting_value': $form.find("input#one_sitting_value").val()
            },
            success: function(response) {
                $("input[name=lifestyle_alcohol]").val(response);
            },
            error:function(){

            }
        });
    
    
    // NUTRITION
    }else if(answer_type == 'nutrition'){
        
        $("input[name=lifestyle_nutrition]").val('calculating...');
        
        $.ajax({
            type: "post",
            url: get_nutrition_path,
            data: $form.serialize() + "&sajax=1",
            success: function(response) {
                $("input[name=lifestyle_nutrition]").val(response);
            }
        });
    
    }
});


// GET VO2
$("select[name=physiological_vo2_test_type]").change(function(){
    
    
    if($(this).val() == 'treadmill' || $(this).val() == 'beep'){
        
        if($(this).val() == 'treadmill'){
            $("input[name=physiological_vo2_test_value1]").attr('placeholder','Time (Seconds)');
            $("input[name=physiological_vo2_test_value2]").attr('placeholder','HR upon completion (beats per minute)');
        }else{
            $("input[name=physiological_vo2_test_value1]").attr('placeholder','Level');
            $("input[name=physiological_vo2_test_value2]").attr('placeholder','Shuttle');
        }
        
        $("input[name=physiological_vo2_test_value2]").css('display', 'inline');
        
        if($("input[name=physiological_vo2_test_value1]").val() != '' &&
            $("input[name=physiological_vo2_test_value2]").val() != ''){
            // Set status to show we're typing.
            $("input[name=physiological_vo2]").val("calculating...");
            getVo2();
        }else{
            $("input[name=physiological_vo2]").val('');
        }
        
    }else{
        
        if($(this).val() == 'bike'){
            $("input[name=physiological_vo2_test_value1]").attr('placeholder','Watts');
            $("input[name=physiological_vo2_test_value2]").attr('placeholder','');
        }else if($(this).val() == 'resting-hr'){
            $("input[name=physiological_vo2_test_value1]").attr('placeholder','RHR');
            $("input[name=physiological_vo2_test_value2]").attr('placeholder','');
        }else if($(this).val() == 'resting-hr'){
            $("input[name=physiological_vo2_test_value1]").attr('placeholder','RHR');
            $("input[name=physiological_vo2_test_value2]").attr('placeholder','');
        }else{
            $("input[name=physiological_vo2_test_value1]").attr('placeholder','VO2');
            $("input[name=physiological_vo2_test_value2]").attr('placeholder','');
        }
        
        $("input[name=physiological_vo2_test_value2]").css('display', 'none');
        if($("input[name=physiological_vo2_test_value1]").val() != ''){
            // Set status to show we're typing.
            $("input[name=physiological_vo2]").val("calculating...");
            getVo2();
        }
    }
});

$('input[name=physiological_vo2_test_value1], input[name=physiological_vo2_test_value2]').on('keyup change',function(){
    var $this = $(this);
    // Clear timer if it's set.
    if (typeof timer != undefined)
    clearTimeout(timer);

    // Set status to show we're typing.
    $("input[name=physiological_vo2]").val("calculating...");
   
    // Set status to show we're done typing on a delay.
    timer = setTimeout(function()
    {
        getVo2();
        
    }, timeout);
});

function getVo2(){
    
    $.ajax({
        url: get_vo2_test_path,
        method: 'POST',
        data: {
            'gender': $("select[name=gender]").val(),
            'weight': $("input[name=weight]").val(),
            'age': default_age,
            'test_type': $("select[name=physiological_vo2_test_type]").val(),
            'value1': $("input[name=physiological_vo2_test_value1]").val(),
            'value2': $("input[name=physiological_vo2_test_value2]").val()
        },
        success: function(response) {
            var $obj = JSON.parse(response);
            
            $("input[name=physiological_vo2]").val($obj.result);
            //removelater
            $("#physiological_vo2_score").val($obj.score);
        },
        error:function(){

        }
    });
    
}

// GET BALANCE
$("select[name=physiological_balance_type]").change(function(){
    // Set status to show we're typing.
    $("input[name=physiological_balance]").val("calculating...");
    getBalance();
});
$("input[name=physiological_balance_value]").on('keyup change', function(){
    var $this = $(this);
    // Clear timer if it's set.
    if (typeof timer != undefined)
    clearTimeout(timer);

    // Set status to show we're typing.
    $("input[name=physiological_balance]").val("calculating...");

    // Set status to show we're done typing on a delay.
    timer = setTimeout(function()
    {
        getBalance();

    }, timeout);
});

function getBalance(){
    $.ajax({
        url: get_balance_test_path,
        method: 'POST',
        data: {
            'gender': $("select[name=gender]").val(),
            'test_type': $("select[name=physiological_balance_type]").val(),
            'value': $("input[name=physiological_balance_value]").val()
        },
        success: function(response) {
            $("input[name=physiological_balance]").val(response);
        },
        error:function(){

        }
    });
}

// removelater
$("input[name=biometric_waist_value]").on('keyup', function(){ 
    var $this = $(this);
    // Clear timer if it's set.
    if (typeof timer != undefined)
    clearTimeout(timer);

    // Set status to show we're typing.
    $("#biometric_waist_score").val("calculating...");

    // Set status to show we're done typing on a delay.
    timer = setTimeout(function()
    { 
        getWaist();

    }, timeout);
});
// removelater
function getWaist(){
    $.ajax({
        url: get_waist_test_path,
        method: 'POST',
        data: {
            'gender': $("select[name=gender]").val(),
            'value': $("input[name=biometric_waist_value]").val()
        },
        success: function(response) {
            $("#biometric_waist_score").val(response);
        },
        error:function(){

        }
    });
}


//==============================================================================
// AUTO SAVE FORM
//==============================================================================

function debounce(callback, timeout, _this) {
    var timer;
    return function(e) {
        var _that = this;
        if (timer)
            clearTimeout(timer);
        timer = setTimeout(function() { 
            callback.call(_this || _that, e);
        }, timeout);
    }
}

// we'll attach the function created by "debounce" to each of the target
// user input events; this function only fires once 2 seconds have passed
// with no additional input; it can be attached to any number of desired
// events
var userAction = debounce(function(e) {
    // set save type to draft
    $(".btn-draft").click();
}, 900000);

document.addEventListener("mousemove", userAction, false);
document.addEventListener("click", userAction, false);
document.addEventListener("scroll", userAction, false);
document.addEventListener("keydown", userAction, false);


//==============================================================================
// TERMS AND CONDITION
//==============================================================================

$(".btn-cancel-terms-condition").on("click", function(){
   $(".term-condition-modal-close.close").click(); 
});

$(".btn-finalise").on("click", function(){
    $("input[name=redirect]").val( $(location).attr('href') );
    
});


$("body").on("click", ".alert .close", function(){
    var $this = $(this);
    $this.closest("div.alert").hide();
});

$("body").on("click", "form .submit", function(){
   var $this = $(this);
   $this.closest("form").submit();
});

$("body").on("click",".the-fake-guide",function(){
   $("a.the-guide").click(); 
});	

//==============================================================================
// HELP
//==============================================================================
$("a.help").click(function(){
    var $this = $(this);
    getHelpText( $this.attr('data-field') );
});

function getHelpText($field){
    
    $(".help-container").html('<center><img src="'+ asset_img +'/spinner.gif"/></center>');
    
    $.ajax({
        url: get_help_path,
        method: 'POST',
        data: {
            'field': $field
        },
        success: function(response) {
            $(".help-container").html(response);
            
            if( $(".help-container").height() > 812 ){ //812 is the height of #sidebar
//                $(".help-container").html('<p>Click here to see <a class="pointer the-fake-guide">the guide</a></p>');
//                $("#popup3").find(".modal-body").html(response);
                $(".help-container").addClass("no-follow-screen");
                $(".help-container").removeClass("follow-screen");
            }else{
                $(".help-container").removeClass("no-follow-screen");
            }
            $( window ).scroll();
            
        },
        error:function(){
            $(".help-container").html('error found');
        }
    });
}

$( window ).resize(function() {
    $( window ).scroll();
});

jQuery(window).scroll(function(){
    if($('#sidebar-lite').length == 0 ){
            return false;
    }
    var distance = $('#sidebar-lite').offset().top;
    var $window = $(window);
    var $top = ($("#main-nav").position().top + $("#main-nav").height()) + 50;
//    var $top = ($("#main-header").offset().top + $("#main-header").height()) + 50;
    
    if($window.width() >= 1178){
        if($(".help-container").hasClass("no-follow-screen")){
            return false;
        }
        
        $(".help-container").css("width", $('#sidebar-lite').width() + 'px');
        if ( ($window.scrollTop() + $top) >= $('#sidebar-lite').offset().top + 50 ) {
            
            $(".help-container").addClass("follow-screen");
            //$(".help-container").css("top", $top  + 'px');
            
            
            $footer_top =  $('#footer').offset().top;
            if ( ($window.scrollTop() +  $('.help-container').height() + $top) >= $footer_top ) {
                $(".help-container").css("top", ($footer_top - ($window.scrollTop() +  $('.help-container').height())) +'px');

            }else{
                $(".help-container").css("top", $top  + 'px');
            }
            
        }else{
            $(".help-container").removeClass("follow-screen");
        }
        
        
    }else{
        $(".help-container").removeClass("follow-screen");
    }
    
    
});


//==============================================================================
// TERMS AND CONDITION
//==============================================================================

$(".btn-cancel-terms-condition").on("click", function(){
   $(".term-condition-modal-close.close").click(); 
});

$(".btn-open-disclaimer").on("click", function(){
    
    $("#termsConditionModal").find(".fancybox-overlay").css({
        width: 'auto', 
        height: 'auto',
        display: 'block',
        background: 'rgba(0, 0, 0, 0.65098)',
        'z-index': 99999
    });
    $("body").css("overflow","hidden");
});

$("body").on("click",".term-condition-modal-close.close", function(){
    
    $("#termsConditionModal").find(".fancybox-overlay").css('display','none');
    $("body").css("overflow","auto");
});

$(".btn-fake-finalise").on("click", function(){
    $(".btn-finalise").click();
    //$(".term-condition-modal-close.close").click(); 
});

$(".input-help").on('click focus',function(){
   var $self = $(this);
   $self.closest(".add-row").find(".help").click();
   getHelpText( $self.closest(".add-row").find(".help").attr("data-field") );
});

$(document).ready(function(){ 
    boxResize();
});

$(window).resize(function(){
    boxResize();
});
function boxResize(){
    var $box_width = $(window).width() - 40;
    if( $(window).width() > 1008 ){
        $(".fancybox-opened").width( 1008 );
        $(".fancybox-opened").css("left", "170px" );
    }else{
        $(".fancybox-opened").width( $box_width );
        $(".fancybox-opened").css("left", "20px" );
    }
    
}

$("form#testForm").submit(function(e){
    $(".submit-gif").show();
    $('html, body').animate({
        scrollTop: $(".submit-gif").find("img").offset().top - 100
    }, 500);
});


$(function() {
  $('body').on('keydown','input[type=number]', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
});