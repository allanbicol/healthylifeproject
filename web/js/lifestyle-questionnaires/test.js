$(document).ready(function() {
    var form = $("#testForm");
    form.steps({
        headerTag: "h1",
        bodyTag: "fieldset",
        transitionEffect: "slideLeft",
        enableAllSteps: true,
        enableFinishButton: (form.hasClass('finalised')) ? false : true,
        onStepChanging: function (event, currentIndex, newIndex)
        { 
//            // Allways allow previous action even if the current form is not valid!
//            if (currentIndex > newIndex)
//            {
//                return true;
//            }
//
//            // Needed in some cases if the user went back (clean up)
//            if (currentIndex < newIndex)
//            { 
//                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
//            }
//
//            if(form.valid()){ 
//                $('.bootstrap-select').removeClass('error');
//            }
//
//            return form.valid();
                return true;
        },
        onStepChanged: function (event, currentIndex, priorIndex)
        { 
            $.ajax({
                type: $('#testForm').attr('method'),
                url: $('#testForm').attr('action'),
                data: $('#testForm').serialize(),
                success: function (data) {
                }
            });
            
        },
        onFinishing: function (event, currentIndex)
        {
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        { 
            $("form#testForm").find("input[name=submit_type]").val("finalise");
            $("form#testForm").submit();
        }
    });
});

$(".nutrition-items > .item").each(function(){
    var $item = $(this);
    $item.find("input[type=checkbox]").change(function()
    { 
        if($(this).hasClass('opt1')){
            $item.find(".opt2").prop('checked',false);
            $item.find(".opt1").prop('checked',true);
        }else{ 
            $item.find(".opt1").prop('checked',false);
            $item.find(".opt2").prop('checked',true);
        }
        
    });
});

$(".help").click(function(){  
    var $this = $(this);
    var $field = $this.attr('data-field');
    var $window = $(window);
    
    $('#mdlHelp').modal({
        backdrop: 'static',
        keyboard: false
    });
    
    $(".help-container").html('<center><img src="'+ asset_img +'/spinner.gif"/></center>');
    
    $.ajax({
        url: get_help_path,
        method: 'POST',
        data: {
            'field': $field
        },
        success: function(response) {
            $(".help-container").html(response);
            $(window).resize();
        },
        error:function(){
            $(".help-container").html('error found');
        }
    });
});



//==============================================================================
// AUTO SAVE FORM
//==============================================================================



function debounce(callback, timeout, _this) {
    var timer;
    return function(e) {
        var _that = this;
        if (timer)
            clearTimeout(timer);
        timer = setTimeout(function() { 
            callback.call(_this || _that, e);
        }, timeout);
    }
}

// we'll attach the function created by "debounce" to each of the target
// user input events; this function only fires once 2 seconds have passed
// with no additional input; it can be attached to any number of desired
// events
var userAction = debounce(function(e) {
    // set save type to draft
    $(".btn-draft").click();
}, 600000);

document.addEventListener("mousemove", userAction, false);
document.addEventListener("click", userAction, false);
document.addEventListener("scroll", userAction, false);
document.addEventListener("keydown", userAction, false);

$(".btn-draft").click(function(){
    var $this = $(this);
    var $caption = $this.find(".text").text();
    $this.find(".text").text("Saving...");
    $.ajax({
        type: $('#testForm').attr('method'),
        url: $('#testForm').attr('action'),
        data: $('#testForm').serialize(),
        success: function (data) {
            $this.find(".text").text($caption);
        }
    });
});

