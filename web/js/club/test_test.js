// DISPLAY LIFESTYLE QUESTIONNAIRE
$(".btn-questionnaire").click(function(){
    var $this = $(this);
    var $lifestyle_code = $this.attr('data-code');
    var $title = '';
    if($lifestyle_code == 'physical_activity'){
        $title = 'Physical Activity';
    }else if($lifestyle_code == 'smoking'){
        $title = 'Smoking Habits';
    }else if($lifestyle_code == 'alcohol'){
        $title = 'Alcohol Consumption';
    }else if($lifestyle_code == 'nutrition'){
        $title = 'Nutrition';
    }else if($lifestyle_code == 'mental_health'){
        $title = 'Mental Health';
    }else if($lifestyle_code == 'risk_profile'){
        $title = 'Risk Profile';
    }
    
    $("#lifeStyleQuestionnaireModal").find(".modal-title").html($title + ' Questionnaire');
    $("#lifeStyleQuestionnaireModal").find(".modal-body").html('<center><img src="'+ asset_img +'/spinner.gif"/></center>');
    
    $.ajax({
        url: test_questionnaire_path,
        method: 'POST',
        data: {
            'code': $lifestyle_code, 
            'club_client_id': club_client_id,
            'client_test_id': client_test_id,
            'gender': default_gender
        },
        success: function(response) {
            $("#lifeStyleQuestionnaireModal").find(".modal-body").html(response);
            $(".btn-submit-lifestyle-answer").removeAttr('disabled');
            $(".btn-submit-lifestyle-answer").html('Save & Submit');
        },
        error:function(){
            $("#lifeStyleQuestionnaireModal").find(".modal-body").html('error found');
        }
    });
    
    $("#lifeStyleQuestionnaireModal").modal("show");
});

// SUBMIT LIFESTYLE ANSWERS
$(".btn-submit-lifestyle-answer").click(function(){
    var $this = $(this);
    var $formData =  $('#frmLifestyle').serialize() + "&sajax=1";
    var $caption = $this.html();
    $this.attr('disabled', 'disabled');
    $this.html('Saving ...');
    $.ajax({
        type: "post",
        url: $this.closest("form#frmLifestyle").attr("action"),
        data: $formData,
        success: function(result) {
            var $obj = JSON.parse(result);
            $this.removeAttr('disabled');
            $this.html($caption);
            if($obj.result == 'success'){
                if($obj.s_complete){
                    $(".lifestyle-is-done.lifestyle-"+$obj.code).removeClass('glyphicon-unchecked');
                    $(".lifestyle-is-done.lifestyle-"+$obj.code).addClass('glyphicon-check');
                }


                $("#lifeStyleQuestionnaireModal").modal("hide");
                
            }else{
                
                alert( $obj.message );
            }
        }
    });
});







//==============================================================================
// FORMULAS
//==============================================================================
// Set our timer global and give a timeout for stop typing.
var timer, timeout = 500;

$(document).ready(function(){
    // Check VO2
    $("select[name=physiological_vo2_test_type]").change();
    //removelater
    $("input[name=biometric_waist_value]").keyup();
});

// GET WEIGHT
$("input[name=weight]").on('keyup',function(){
    var $this = $(this);
    // Clear timer if it's set.
    if (typeof timer != undefined)
    clearTimeout(timer);

    // Set status to show we're typing.
    $("input[name=bmi]").val("calculating...");
   
    // Set status to show we're done typing on a delay.
    timer = setTimeout(function()
    {
        $.ajax({
            url: get_bmi_result_path,
            method: 'POST',
            data: {
                'weight': $this.val(),
                'height': default_height
            },
            success: function(response) {
                $("input[name=bmi]").val(response);
                
                // update those fields that are weight dependent
                $(".weight-dependent").keyup();
            },
            error:function(){

            }
        });
    }, timeout);
});

// GET PHYSICAL ACTIVITY
$(".btn-submit-lifestyle-answer").click(function(){
    var $this = $(this);
    var $form = $("form#frmLifestyle");
    var answer_type = $form.find("input[name=answer_type]").val();
    
    // PHYSICAL ACTIVITY
    if(answer_type == 'physical-activity'){
        
        $("input[name=lifestyle_physical_activity]").val('calculating...');
        $.ajax({
            url: get_physical_activity_result_path,
            method: 'POST',
            data: {
                'client_test_id' : client_test_id,
                'moderate_vigorous_pa': $form.find("input#moderate_vigorous_pa").val(),
                'muscle_strenthening_pa': $form.find("input#muscle_strenthening_pa").val()
            },
            success: function(response) {
                //if($(".lifestyle-is-done").hasClass(""))
                $("input[name=lifestyle_physical_activity]").val(response);
            },
            error:function(){

            }
        });
        
        
    // SMOKING
    }else if(answer_type == 'smoking'){
        
        $("input[name=lifestyle_smoking]").val('calculating...');
        
        $.ajax({
            type: "post",
            url: get_smoking_path,
            data: $form.serialize() + "&sajax=1",
            success: function(response) {
                $("input[name=lifestyle_smoking]").val(response);
            }
        });
    
    
    // SMOKING
    }else if(answer_type == 'alcohol'){
        
        $("input[name=lifestyle_alcohol]").val('calculating...');
        $.ajax({
            url: get_alcohol_consumption_path,
            method: 'POST',
            data: {
                'gender': default_gender,
                'client_test_id': client_test_id,
                'per_week_value': $form.find("input#per_week_value").val(),
                'one_sitting_value': $form.find("input#one_sitting_value").val()
            },
            success: function(response) {
                $("input[name=lifestyle_alcohol]").val(response);
            },
            error:function(){

            }
        });
    
    
    // NUTRITION
    }else if(answer_type == 'nutrition'){
        
        $("input[name=lifestyle_nutrition]").val('calculating...');
        
        $.ajax({
            type: "post",
            url: get_nutrition_path,
            data: $form.serialize() + "&sajax=1",
            success: function(response) {
                $("input[name=lifestyle_nutrition]").val(response);
            }
        });
    
    
    // MENTAL HEALTH
    }else if(answer_type == 'mental-health'){
        
        $("input[name=lifestyle_mental_health]").val('calculating...');
        $.ajax({
            type: "post",
            url: get_mental_health_path,
            data: $form.serialize() + "&sajax=1",
            success: function(response) {
                $("input[name=lifestyle_mental_health]").val(response);
            }
        });
    
    
    // RISK PROFILE
    }else if(answer_type == 'risk-profile'){
        
        $("input[name=lifestyle_risk_profile]").val('calculating...');
        $.ajax({
            type: "post",
            url: get_risk_profile_path,
            data: $form.serialize() + "&sajax=1",
            success: function(response) {
                $("input[name=lifestyle_risk_profile]").val(response);
            }
        });
    }
});


// GET VO2
$("select[name=physiological_vo2_test_type]").change(function(){
    
    
    if($(this).val() == 'treadmill' || $(this).val() == 'beep'){
        
        if($(this).val() == 'treadmill'){
            $("input[name=physiological_vo2_test_value1]").attr('placeholder','Time (Seconds)');
            $("input[name=physiological_vo2_test_value2]").attr('placeholder','HR upon completion (beats per minute)');
        }else{
            $("input[name=physiological_vo2_test_value1]").attr('placeholder','Level');
            $("input[name=physiological_vo2_test_value2]").attr('placeholder','Shuttle');
        }
        
        $("input[name=physiological_vo2_test_value2]").css('display', 'inline');
        
        if($("input[name=physiological_vo2_test_value1]").val() != '' &&
            $("input[name=physiological_vo2_test_value2]").val() != ''){
            // Set status to show we're typing.
            $("input[name=physiological_vo2]").val("calculating...");
            getVo2();
        }else{
            $("input[name=physiological_vo2]").val('');
        }
        
    }else{
        
        if($(this).val() == 'bike'){
            $("input[name=physiological_vo2_test_value1]").attr('placeholder','Watts');
            $("input[name=physiological_vo2_test_value2]").attr('placeholder','');
        }else if($(this).val() == 'resting-hr'){
            $("input[name=physiological_vo2_test_value1]").attr('placeholder','RHR');
            $("input[name=physiological_vo2_test_value2]").attr('placeholder','');
        }else if($(this).val() == 'resting-hr'){
            $("input[name=physiological_vo2_test_value1]").attr('placeholder','RHR');
            $("input[name=physiological_vo2_test_value2]").attr('placeholder','');
        }else{
            $("input[name=physiological_vo2_test_value1]").attr('placeholder','VO2');
            $("input[name=physiological_vo2_test_value2]").attr('placeholder','');
        }
        
        $("input[name=physiological_vo2_test_value2]").css('display', 'none');
        if($("input[name=physiological_vo2_test_value1]").val() != ''){
            // Set status to show we're typing.
            $("input[name=physiological_vo2]").val("calculating...");
            getVo2();
        }
    }
});

$('input[name=physiological_vo2_test_value1], input[name=physiological_vo2_test_value2]').on('keyup change',function(){
    var $this = $(this);
    // Clear timer if it's set.
    if (typeof timer != undefined)
    clearTimeout(timer);

    // Set status to show we're typing.
    $("input[name=physiological_vo2]").val("calculating...");
   
    // Set status to show we're done typing on a delay.
    timer = setTimeout(function()
    {
        getVo2();
        
    }, timeout);
});

function getVo2(){
    
    $.ajax({
        url: get_vo2_test_path,
        method: 'POST',
        data: {
            'gender': default_gender,
            'weight': $("input[name=weight]").val(),
            'age': default_age,
            'test_type': $("select[name=physiological_vo2_test_type]").val(),
            'value1': $("input[name=physiological_vo2_test_value1]").val(),
            'value2': $("input[name=physiological_vo2_test_value2]").val()
        },
        success: function(response) {
            var $obj = JSON.parse(response);
            
            $("input[name=physiological_vo2]").val($obj.result);
            //removelater
            $("#physiological_vo2_score").val($obj.score);
        },
        error:function(){

        }
    });
    
}

// GET BALANCE
$("select[name=physiological_balance_type]").change(function(){
    // Set status to show we're typing.
    $("input[name=physiological_balance]").val("calculating...");
    getBalance();
});
$("input[name=physiological_balance_value]").on('keyup change', function(){
    var $this = $(this);
    // Clear timer if it's set.
    if (typeof timer != undefined)
    clearTimeout(timer);

    // Set status to show we're typing.
    $("input[name=physiological_balance]").val("calculating...");

    // Set status to show we're done typing on a delay.
    timer = setTimeout(function()
    {
        getBalance();

    }, timeout);
});

function getBalance(){
    $.ajax({
        url: get_balance_test_path,
        method: 'POST',
        data: {
            'gender': default_gender,
            'test_type': $("select[name=physiological_balance_type]").val(),
            'value': $("input[name=physiological_balance_value]").val()
        },
        success: function(response) {
            $("input[name=physiological_balance]").val(response);
        },
        error:function(){

        }
    });
}

// removelater
$("input[name=biometric_waist_value]").on('keyup', function(){ 
    var $this = $(this);
    // Clear timer if it's set.
    if (typeof timer != undefined)
    clearTimeout(timer);

    // Set status to show we're typing.
    $("#biometric_waist_score").val("calculating...");

    // Set status to show we're done typing on a delay.
    timer = setTimeout(function()
    { 
        getWaist();

    }, timeout);
});
// removelater
function getWaist(){
    $.ajax({
        url: get_waist_test_path,
        method: 'POST',
        data: {
            'gender': default_gender,
            'value': $("input[name=biometric_waist_value]").val()
        },
        success: function(response) {
            $("#biometric_waist_score").val(response);
        },
        error:function(){

        }
    });
}


//==============================================================================
// AUTO SAVE FORM
//==============================================================================

function debounce(callback, timeout, _this) {
    var timer;
    return function(e) {
        var _that = this;
        if (timer)
            clearTimeout(timer);
        timer = setTimeout(function() { 
            callback.call(_this || _that, e);
        }, timeout);
    }
}

// we'll attach the function created by "debounce" to each of the target
// user input events; this function only fires once 2 seconds have passed
// with no additional input; it can be attached to any number of desired
// events
var userAction = debounce(function(e) {
    // set save type to draft
    $(".btn-draft").click();
}, 900000);

document.addEventListener("mousemove", userAction, false);
document.addEventListener("click", userAction, false);
document.addEventListener("scroll", userAction, false);
document.addEventListener("keydown", userAction, false);





//==============================================================================
// HELP
//==============================================================================

$("a.help").click(function(){
    var $this = $(this);
    var $field = $this.attr('data-field');
    
    $(".help-container").html('<center><img src="'+ asset_img +'/spinner.gif"/></center>');
    
    $.ajax({
        url: get_help_path,
        method: 'POST',
        data: {
            'field': $field
        },
        success: function(response) {
            $(".help-container").html(response);
            if( $(".help-container").height() > 812 ){ //812 is the height of #sidebar
                $(".help-container").html('<p>Click here to see <a class="pointer the-fake-guide">the guide</a></p>');
                $("#popup3").find(".modal-body").html(response);
            }
            
            
        },
        error:function(){
            $(".help-container").html('error found');
        }
    });
});
$( window ).resize(function() {
    $( window ).scroll();
});

jQuery(window).scroll(function(){
	if($('#sidebar').length == 0){
		return false;
	}
    var distance = $('#sidebar').offset().top;
    var $window = $(window);
    var $top = $window.scrollTop()-$window.height();
    
    if($window.width() >= 1178){
        if( ($window.scrollTop() + $window.height()) != $(document).height()) {
            
            $(".help-container").css("width", $('#sidebar').width() + 'px');
            if ( $window.scrollTop() >= $('#sidebar').offset().top + 50 ) {
                $(".help-container").addClass("follow-screen");
                
                // Normal
                $(".help-container").css("top", 20 + 'px');

            }else{
                $(".help-container").removeClass("follow-screen");
            }

        }
    
        $footer_top = ($('#footer').length > 0) ? $('#footer').offset().top : $(document).height()  - 100;
//    console.log ( 'top+help = ' + ($window.scrollTop() +  $('.help-container').height() + $add_for_litetest) );
//    console.log ( 'footer = ' + $footer_top );		
        if ( ($window.scrollTop() +  $('.help-container').height()) >= $footer_top ) {
            $(".help-container").css("top", ($footer_top - ($window.scrollTop() +  $('.help-container').height())) +'px');

        }
    }else{
        $(".help-container").removeClass("follow-screen");
    }
    
});
		
$("body").on("click",".the-fake-guide",function(){
   $("a.the-guide").click(); 
});	
