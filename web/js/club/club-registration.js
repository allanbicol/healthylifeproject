$(document).ready(function() {
    $('#lookgood').click(function(){
        debugger;
        alert("nbvv");
    });

    var form = $("#wizard-form").show();
    form.steps({
        headerTag: "h1",
        bodyTag: "section",
        transitionEffect: "slideLeft",
//        enableAllSteps: true,
        //enableFinishButton: true,
        onStepChanging: function (event, currentIndex, newIndex)
        {
            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex)
            {
                return true;
            }

            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex)
            {
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();

        },
        onStepChanged: function (event, currentIndex, priorIndex)
        {

        },
        onFinishing: function (event, currentIndex)
        {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            //alert("Submitted!");
            $('#wizard-form').submit();
        }
    }).validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules: {
            confirm_password: {
                equalTo: "#password"
            }
            }
    });
});

$("input[name=registration_option]").change(function(){
   var $this = $(this);
   if($this.val() == 'regular'){
       $(".registration-fee-text").text("Registration Fee");
       $(".registration-fee").text("169");
   }else{
       $(".registration-fee-text").text("Monthly Fee");
       $(".registration-fee").text("200");
   }
});