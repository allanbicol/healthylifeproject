
$(document).ready(function(){
    var $table = $('#example').DataTable({
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "order": [[ 1, "asc" ]],
        "columnDefs": [
            { 
                "orderable": false, 
                "targets": 0 
            },
            {
                "targets": [ 6, 7, 8 ],
                "visible": false
            }
        ]
    });
    
    // Apply the search
    $('select[name=event]').on( 'change', function () {
        if(this.value != ''){
            var query = "((  )|^)" + regex_escape(this.value) + "((  )|$)";
            $table
                .columns( 6 )
                .search( query, true, false  )
                .draw();
        }else{
            $table
                .columns( 6 )
                .search( this.value  )
                .draw();
        }
    } );
    
    $('select[name=program]').on( 'change', function () {
        if(this.value != ''){
            var query = "((  )|^)" + regex_escape(this.value) + "((  )|$)";
            $table
                .columns( 7 )
                .search( query, true, false  )
                .draw();
        }else{
            $table
                .columns( 7 )
                .search( this.value  )
                .draw();
        }
    } );

    $('select[name=coach]').on( 'change', function () {
        if(this.value != ''){
            var query = "((  )|^)" + regex_escape(this.value) + "((  )|$)";
            $table
                .columns( 8 )
                .search( query, true, false  )
                .draw();
        }else{
            $table
                .columns( 8 )
                .search( this.value  )
                .draw();
        }
    } );
    
    tinymce.init({
        selector: 'textarea.wysiwyg-editor',
        menubar: false,
        toolbar: 'undo redo | bold italic underline | link | bullist numlist',
    });
});

function regex_escape(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}


$(".checkall").change(function(){
    var $this = $(this);
    
    $this.closest("table").find("tbody td").find("input[type=checkbox]").each(function(){
       var $checkbox = $(this); 
       if($this.is(":checked")){
           $checkbox.prop("checked", true);
       }else{
           $checkbox.prop("checked", false);
       }
    });
    
    var $clients = getCheckedClients($this.closest("table"));
    if( $clients.length > 0 ){
        $(".btn-bulk-email").removeAttr("disabled");
    }else{
        $(".btn-bulk-email").attr("disabled","disabled");
    }
    
    $("input[name=clients]").val( JSON.stringify($clients) );
});

$("table#example").find("tbody td").find("input[type=checkbox]").change(function(){
    var $this = $(this);
    var $clients = getCheckedClients($this.closest("table"));
    if( $clients.length > 0 ){
        $(".btn-bulk-email").removeAttr("disabled");
    }else{
        $(".btn-bulk-email").attr("disabled","disabled");
    }
    
    $("input[name=clients]").val( JSON.stringify($clients) );
});

$(".btn-bulk-email").click(function(){
    $("#bulkEmailModal").modal("show");
});

$(".btn-send-bulk-email").click(function(){
   var $this = $(this);
   $this.text("Sending...");
//   $this.parent().find("button").attr("disabled", "disabled");
});

function getCheckedClients($table){
    var $clients = [];
    $table.find("tbody td").find("input[type=checkbox]").each(function(){
       var $checkbox = $(this); 
       if($checkbox.is(":checked")){
           $clients.push($checkbox.val());
       }
    });
    
    return $clients;
}
