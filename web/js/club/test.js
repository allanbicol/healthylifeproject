$(document).ready(function() {
    var form = $("#testForm");
    var active_tab = 0;
    if(s_signup_questions_done == 1){
        if(test_type == 'aia'){
            active_tab = (s_aia_health_questions_done == 1) ? 2 : 1;
        }else{
            active_tab = (s_signup_health_questions_done == 1) ? 2 : 1;
        }
    }else{
        active_tab = 0;
    }
    
    form.steps({
        headerTag: "h1",
        bodyTag: "fieldset",
        transitionEffect: "slideLeft",
        enableAllSteps: true,
        enableFinishButton: (form.hasClass('finalised')) ? false : true,
//        startIndex: (s_signup_questions_done == 1) ? 2 : 0,
        startIndex: active_tab,
        onStepChanging: function (event, currentIndex, newIndex)
        { 
//            // Allways allow previous action even if the current form is not valid!
//            if (currentIndex > newIndex)
//            {
//                return true;
//            }
//
//            // Needed in some cases if the user went back (clean up)
//            if (currentIndex < newIndex)
//            { 
//                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
//            }
//
//            if(form.valid()){ 
//                $('.bootstrap-select').removeClass('error');
//            }
//
//            return form.valid();
                return true;
        },
        onStepChanged: function (event, currentIndex, priorIndex)
        { 
            
//            $("#wizard .actions a[href='#finish']").hide();
            if( (form.hasClass('finalised')) ){
                
            }else{
                $.ajax({
                    type: $('#testForm').attr('method'),
                    url: $('#testForm').attr('action'),
                    data: $('#testForm').serialize() + '&ajax=1',
                    success: function (data) {
                    }
                });
            }
            

        },
        onFinishing: function (event, currentIndex)
        {


            
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        { 
            if(is_aia_test==1){
            $('#mdlDeclaration').modal({
                backdrop: 'static',
                keyboard: false,
                
            });
            
            
            }else{
                $("body").find(".actions a[href='#finish']").parent().addClass("disabled");
                $("body").find(".actions a[href='#finish']").text("Submitting...");
                $("body").find(".actions a[href='#finish']").attr("href","");
                $("body").find(".actions a[href='#previous']").parent().addClass("disabled");
                $("body").find(".actions a[href='#previous']").attr("href","");
                $("body").find(".btn-draft").attr("disabled","disabled");
                $("body").find(".btn-delete").attr("disabled","disabled");
                $("form#testForm").find("input[name=submit_type]").val("finalise");
                $("form#testForm").submit();
            }
            //$("form#testForm").submit();
        }
    });
    
    initialiseStrengthResultsOnLoad();
});

$('#chkDeclaration').click(function(){
     if($('#chkDeclaration').not(':checked').length){
         $("body").find("#btnDeclaration").addClass("disabled");       
    }else{
       $("body").find("#btnDeclaration").removeClass("disabled");
    }
});
$('#btnDeclaration').click(function(){
    if($('#chkDeclaration').not(':checked').length){
                
    }else{
        $('#mdlDeclaration').modal('toggle');
        $("body").find(".actions a[href='#finish']").parent().addClass("disabled");
        $("body").find(".actions a[href='#finish']").text("Submitting...");
        $("body").find(".actions a[href='#finish']").attr("href","");
        $("body").find(".actions a[href='#previous']").parent().addClass("disabled");
        $("body").find(".actions a[href='#previous']").attr("href","");
        $("body").find(".btn-draft").attr("disabled","disabled");
        $("body").find(".btn-delete").attr("disabled","disabled");
        $("form#testForm").find("input[name=submit_type]").val("finalise");
        $("form#testForm").submit();
    }
});

$(".nutrition-items > .item").each(function(){
    var $item = $(this);
    $item.find("input[type=checkbox]").change(function()
    { 
        if($(this).hasClass('opt1')){
            $item.find(".opt2").prop('checked',false);
            $item.find(".opt1").prop('checked',true);
        }else{ 
            $item.find(".opt1").prop('checked',false);
            $item.find(".opt2").prop('checked',true);
        }
        
    });
});

$(".help").click(function(){  
    var $this = $(this);
    var $field = $this.attr('data-field');
    var $window = $(window);
    
    $('#mdlHelp').modal({
        backdrop: 'static',
        keyboard: false
    });
    
    $(".help-container").html('<center><img src="'+ asset_img +'/spinner.gif"/></center>');
    
    $.ajax({
        url: get_help_path,
        method: 'POST',
        data: {
            'field': $field
        },
        success: function(response) {
            $(".help-container").html(response);
            $(window).resize();
        },
        error:function(){
            $(".help-container").html('error found');
        }
    });
});

//==============================================================================
// FORMULAS
//==============================================================================
// Set our timer global and give a timeout for stop typing.
var timer, timeout = 500;

$(document).ready(function(){
    // Check VO2
    $("select[name=physiological_vo2_test_type]").change();
    //removelater
    $("input[name=biometric_waist_value]").keyup();
});

// GET WEIGHT
$("input[name=weight],input[name=height]").on('keyup',function(){
    var $this = $(this);
    // Clear timer if it's set.
    if (typeof timer != undefined)
    clearTimeout(timer);

    // Set status to show we're typing.
    $("input[name=bmi]").val("calculating...");
   
    // Set status to show we're done typing on a delay.
    timer = setTimeout(function()
    {
        $.ajax({
            url: get_bmi_result_path,
            method: 'POST',
            data: {
                'weight': $("input[name=weight]").val(),
                'height': $("input[name=height]").val()
            },
            success: function(response) {
                $("input[name=bmi]").val(response);
                
                // update those fields that are weight dependent
                $(".weight-dependent").keyup();
            },
            error:function(){

            }
        });
    }, timeout);
});

// GET VO2
$("select[name=physiological_vo2_test_type]").change(function(){
    
    
    if($(this).val() == 'treadmill' || $(this).val() == 'beep'){
        
        if($(this).val() == 'treadmill'){
            $("input[name=physiological_vo2_test_value1]").attr('placeholder','Time (Seconds)');
            $("input[name=physiological_vo2_test_value2]").attr('placeholder','HR upon completion (beats per minute)');
        }else{
            $("input[name=physiological_vo2_test_value1]").attr('placeholder','Level');
            $("input[name=physiological_vo2_test_value2]").attr('placeholder','Shuttle');
        }
        
        $("input[name=physiological_vo2_test_value2]").css('display', 'inline');
        
        if($("input[name=physiological_vo2_test_value1]").val() != '' &&
            $("input[name=physiological_vo2_test_value2]").val() != ''){
            // Set status to show we're typing.
            $("input[name=physiological_vo2]").val("calculating...");
            getVo2();
        }else{
            $("input[name=physiological_vo2]").val('');
        }
        
    }else{
        
        if($(this).val() == 'bike'){
            $("input[name=physiological_vo2_test_value1]").attr('placeholder','Watts');
            $("input[name=physiological_vo2_test_value2]").attr('placeholder','');
        }else if($(this).val() == 'resting-hr'){
            $("input[name=physiological_vo2_test_value1]").attr('placeholder','RHR');
            $("input[name=physiological_vo2_test_value2]").attr('placeholder','');
        }else if($(this).val() == 'qc-step'){
            $("input[name=physiological_vo2_test_value1]").attr('placeholder','Heart Rate');
            $("input[name=physiological_vo2_test_value2]").attr('placeholder','');
        }else{
            $("input[name=physiological_vo2_test_value1]").attr('placeholder','VO2');
            $("input[name=physiological_vo2_test_value2]").attr('placeholder','');
        }
        
        $("input[name=physiological_vo2_test_value2]").css('display', 'none');
        if($("input[name=physiological_vo2_test_value1]").val() != ''){
            // Set status to show we're typing.
            $("input[name=physiological_vo2]").val("calculating...");
            getVo2();
        }
    }
});

$('input[name=physiological_vo2_test_value1], input[name=physiological_vo2_test_value2]').on('keyup change',function(){
    var $this = $(this);
    // Clear timer if it's set.
    if (typeof timer != undefined)
    clearTimeout(timer);

    // Set status to show we're typing.
    $("input[name=physiological_vo2]").val("calculating...");
   
    // Set status to show we're done typing on a delay.
    timer = setTimeout(function()
    {
        getVo2();
        
    }, timeout);
});

function getVo2(){
    
    $.ajax({
        url: get_vo2_test_path,
        method: 'POST',
        data: {
            'gender': default_gender,
            'weight': $("input[name=weight]").val(),
            'age': default_age,
            'test_type': $("select[name=physiological_vo2_test_type]").val(),
            'value1': $("input[name=physiological_vo2_test_value1]").val(),
            'value2': $("input[name=physiological_vo2_test_value2]").val()
        },
        success: function(response) {
            var $obj = JSON.parse(response);
            
            $("input[name=physiological_vo2]").val($obj.result);
            //removelater
            $("#physiological_vo2_score").val($obj.score);
        },
        error:function(){

        }
    });
    
}

// GET BALANCE
$("select[name=physiological_balance_type]").change(function(){
    // Set status to show we're typing.
    $("input[name=physiological_balance]").val("calculating...");
    getBalance();
});
$("input[name=physiological_balance_value]").on('keyup change', function(){
    var $this = $(this);
    // Clear timer if it's set.
    if (typeof timer != undefined)
    clearTimeout(timer);

    // Set status to show we're typing.
    $("input[name=physiological_balance]").val("calculating...");

    // Set status to show we're done typing on a delay.
    timer = setTimeout(function()
    {
        getBalance();

    }, timeout);
});

function getBalance(){
    $.ajax({
        url: get_balance_test_path,
        method: 'POST',
        data: {
            'gender': default_gender,
            'test_type': $("select[name=physiological_balance_type]").val(),
            'value': $("input[name=physiological_balance_value]").val()
        },
        success: function(response) {
            $("input[name=physiological_balance]").val(response);
        },
        error:function(){

        }
    });
}

// removelater
$("input[name=biometric_waist_value]").on('keyup', function(){ 
    var $this = $(this);
    // Clear timer if it's set.
    if (typeof timer != undefined)
    clearTimeout(timer);

    // Set status to show we're typing.
    $("#biometric_waist_score").val("calculating...");

    // Set status to show we're done typing on a delay.
    timer = setTimeout(function()
    { 
        getWaist();

    }, timeout);
});
// removelater
function getWaist(){
    $.ajax({
        url: get_waist_test_path,
        method: 'POST',
        data: {
            'gender': default_gender,
            'value': $("input[name=biometric_waist_value]").val()
        },
        success: function(response) {
            $("#biometric_waist_score").val(response);
        },
        error:function(){

        }
    });
}

//==============================================================================
// DELETE DRAFT
//==============================================================================

$(".btn-delete").click(function(){
    var $this = $(this);
    
    bootbox.confirm("Are you sure?", function(result) {
        if(result === true){
//            $("form#delete input[name=client_test_id]").val($this.closest('form').find("input[name=client_test_id]").val());
            $("form#delete").submit();
        }
    });  
    
    return false; 
});

//==============================================================================
// AUTO SAVE FORM
//==============================================================================



function debounce(callback, timeout, _this) {
    var timer;
    return function(e) {
        var _that = this;
        if (timer)
            clearTimeout(timer);
        timer = setTimeout(function() { 
            callback.call(_this || _that, e);
        }, timeout);
    }
}

// we'll attach the function created by "debounce" to each of the target
// user input events; this function only fires once 2 seconds have passed
// with no additional input; it can be attached to any number of desired
// events
var userAction = debounce(function(e) {
    // set save type to draft
    $(".btn-draft").click();
}, 600000);

document.addEventListener("mousemove", userAction, false);
document.addEventListener("click", userAction, false);
document.addEventListener("scroll", userAction, false);
document.addEventListener("keydown", userAction, false);

$(".btn-draft").click(function(){ 
    var $this = $(this);
    var $caption = $this.find(".text").text();
    $this.find(".text").text("Saving...");
    $.ajax({
        type: $('#testForm').attr('method'),
        url: $('#testForm').attr('action'),
        data: $('#testForm').serialize() + '&ajax=1',
        success: function (data) {
            $this.find(".text").text($caption);
        }
    });
});


//$(".btn-draft").click(function(){
//    $(".termsconditions").find("input[name=termsconditions]").removeAttr("required");
//});

$(".btn-finalise").click(function(){
    $(".termsconditions").find("input[name=termsconditions]").addAttr("required","required");
});

$(".na-blood-oxygen").change(function(){
   var $self = $(this) ;
   if( $self.is(':checked') ){
       $('input[name=biometric_blood_oxygen_value]').val('NA');
       $('input[name=biometric_blood_oxygen_value]').attr('readonly','readonly');
   }else{
       $('input[name=biometric_blood_oxygen_value]').val('');
       $('input[name=biometric_blood_oxygen_value]').removeAttr('readonly');
   }
});

$(".na-body-fat").change(function(){
   var $self = $(this) ;
   if( $self.is(':checked') ){
       $('input[name=biometric_body_fat_value]').val('NA');
       $('input[name=biometric_body_fat_value]').attr('readonly','readonly');
   }else{
       $('input[name=biometric_body_fat_value]').val('');
       $('input[name=biometric_body_fat_value]').removeAttr('readonly');
   }
});

$("input[name=biometric_waist_value], input[name=biometric_hip]").on('keyup', function(){ 
    var $this = $(this);
    // Clear timer if it's set.
    if (typeof timer != undefined)
    clearTimeout(timer);

    // Set status to show we're typing.
    $("#waist_hip_ratio").val("calculating...");

    // Set status to show we're done typing on a delay.
    timer = setTimeout(function()
    { 
        getWaistHipRatio();

    }, timeout);
});


$(".s-direct-entry input[type=checkbox]").change(function(){
    var $this = $(this);
    if($this.is(":checked")){
        $this.closest(".form-group").find("label.est").addClass("hidden");
        $this.closest(".form-group").find(".two-fields").addClass("hidden");
        $this.closest(".form-group").find(".direct-entry").removeClass("hidden");
    }else{
        $this.closest(".form-group").find("label.est").removeClass("hidden");
        $this.closest(".form-group").find(".two-fields").removeClass("hidden");
        $this.closest(".form-group").find(".direct-entry").addClass("hidden");
    }
});
$(".needs-result-display").on("keyup",function(){ 
    var $this = $(this);
    var $weight = $this.closest(".strength-fields").find(".needs-result-display.weight-input").val();
    var $reps = $this.closest(".strength-fields").find(".needs-result-display.reps-input").val();
    $weight = $weight ? $weight : 0;
    $reps = $reps ? $reps : 0;
    getStrength( $this, $weight, $reps);
});

function initialiseStrengthResultsOnLoad(){
    $(".strength-fields").each(function(){
        var $this = $(this);
        var $weight = $this.find(".needs-result-display.weight-input").val();
        var $reps = $this.find(".needs-result-display.reps-input").val();
        $weight = $weight ? $weight : 0;
        $reps = $reps ? $reps : 0;
        getStrength( $this, $weight, $reps);
        
        if( $this.find("input[type=checkbox]").is(":checked") ){
            $this.find("label.est").addClass("hidden");
        }else{
            $this.find("label.est").removeClass("hidden");
        }
    });
}

function getStrength( $elem,$weight, $reps){ 
    var $irm = formulaStrength($weight, $reps);
    $irm = $irm.toFixed(2);
    $elem.closest(".strength-fields").find("label.est").find("span").text( $irm );
}

// removelater
function getWaistHipRatio(){
    $.ajax({
        url: get_waist_hip_ratio_path,
        method: 'POST',
        data: {
            'waist': $("input[name=biometric_waist_value]").val(),
            'hip': $("input[name=biometric_hip]").val()
        },
        success: function(response) {
            $("#waist_hip_ratio").val(response);
        },
        error:function(response){
        }
    });
}

function formulaStrength($weight, $reps){
    var $weight = parseFloat($weight);
    var $reps = parseFloat($reps);

    var $result = $weight * ( 1 + ($reps/30) );
    return $result;
}