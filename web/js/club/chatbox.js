//=========================================================================================
// CHATBOX
//=========================================================================================
var currrent_msgs = 0;
$(document).ready(function(){
    setTimeout(function(){ 
        openChats(open_chats, 0); 
        newChatMessages(1);
    }, 10000);
    
   
    $(".ui-chatbox-log").scroll(function(){
        chatboxScroll($(this));
    });
    
});


//var broadcastMessageCallback = function(from, msg) { 
//    for(var i = 0; i < idList.length; i ++) { 
//        chatboxManager.addBox(idList[i]);
//        $("#" + idList[i]).chatbox("option", "boxManager").addMsg(from, msg);
//    }
//}


// chatboxManager is excerpt from the original project
// the code is not very clean, I just want to reuse it to manage multiple chatboxes
//chatboxManager.init({messageSent : broadcastMessageCallback});
chatboxManager.init({ 
    // ajax submission
    messageSent : function(id, user , msg){
        var userid = user.box_id;
        userid = userid.replace("box","");
        $.ajax({
            type: "post",
            url: chat_url,
            data: { 
                'email' : user.email,
                'id': userid,
                'message' : msg
            },
            success: function(result) {
                if(result == 'session_expired'){
                    window.location.href = logout_url;
                    return false;
                }
                if( result != 'failed' ){ 
                    var obj = JSON.parse(result);
                    if(typeof obj[0] === 'undefined'){
                        $("#" + user.box_id).chatbox("option", "boxManager").addMsg("You", user , 'Your message has not been sent. Please try again!', 'unsent');
                    }else{
                        $("#" + user.box_id).chatbox("option", "boxManager").addMsg("You", user , msg, obj[0]['chat_id']);
                    }
                }
            }
        }); 
        
    }
});


$("body").on("click",".chat-to-client",function(event){ 
    var $this = $(this);
    var id = "box" + $this.attr("data-id");
    
    
    chatboxManager.addBox(id, 
                            {box_id:"box" + $this.attr("data-id"), // not used in demo
                             title:"box" + $this.attr("data-id"),
                             first_name: $this.attr("data-fname"),
                             last_name: $this.attr("data-lname"),
                             email: $this.attr("data-email"),
                             club_client_id: $this.attr("data-id")
                             //you can add your own options too
                            }
                        );
                            
    $("#box"+ $this.attr("data-id") ).closest(".ui-chatbox-content").css("display","block");             
    $.ajax({
        type: "post",
        url: get_open_box_chat_msgs_url,
        data: { 
            'email' : $this.attr("data-email"),
            'box': $this.attr("data-id")
        },
        success: function(result) {
            var user = [];
            var obj = JSON.parse( result );
            
            for( i=0; i<obj.length; i++ ){
                // if message doesn't exist
                // then append
                if($("#box" + $this.attr("data-id")).find(".ui-chatbox-msg."+obj[i]['chat_id']).length <= 0 ){
                    if(obj[i]['sender_type'] == 'coach'){
                        if(obj[i]['club_admin_id'] == coach){
                            $("#box" + $this.attr("data-id")).chatbox("option", "boxManager").addMsg("You", user , obj[i]['message'], obj[i]['chat_id']);
                        }else{
                            $("#box" + $this.attr("data-id")).chatbox("option", "boxManager").addMsg(obj[i]['club_admin_name'], user , obj[i]['message'], obj[i]['chat_id']);
                        }
                    }else{
                        $("#box" + $this.attr("data-id")).chatbox("option", "boxManager").addMsg($this.attr("data-fname"), user , obj[i]['message'], obj[i]['chat_id']);
                    }
                }
            }
            
            $(".ui-chatbox-log").scroll(function(){
                chatboxScroll($(this));
            });
        }
    }); 
    
    
    event.preventDefault(); 
});
    

function openChats(jsonOpenChats, fromNew){  
    if(jsonOpenChats != ''){
        var objchat = JSON.parse(jsonOpenChats);
        for(row=0; row<(objchat.length); row++){ 
            (function(row)
            {
                var boxid = objchat[row]['id'];
                var fname = objchat[row]['fname'];
                var lname = objchat[row]['lname'];
                var email = objchat[row]['email'];
                chatboxManager.addBox("box" + boxid, 
                {
                    box_id:"box" + boxid, // not used in demo
                    title:"box" + boxid,
                    first_name: fname,
                    last_name: lname,
                    email: email,
                    club_client_id: boxid
                    //you can add your own options too
                });

                $.ajax({
                    type: "post",
                    url: get_open_box_chat_msgs_url,
                    data: { 
                        'email' : email,
                        'box': boxid
                    },
                    success: function(result) {
                        if(result == 'session_expired'){
                            window.location.href = logout_url;
                            return false;
                        }
                        var user = [];
                        var obj = JSON.parse( result );

                        for( i=0; i<obj.length; i++ ){ 
                            // if message doesn't exist
                            // then append
                            if($("#box" + boxid).find(".ui-chatbox-msg."+obj[i]['chat_id']).length <= 0 ){
                                if(obj[i]['sender_type'] == 'coach'){
                                    if(obj[i]['club_admin_id'] == coach){
                                        $("#box" + boxid).chatbox("option", "boxManager").addMsg("You", user , obj[i]['message'], obj[i]['chat_id']);
                                    }else{
                                        $("#box" + boxid).chatbox("option", "boxManager").addMsg(obj[i]['club_admin_name'], user , obj[i]['message'], obj[i]['chat_id']);
                                    }
                                }else{
                                    $("#box" + boxid).chatbox("option", "boxManager").addMsg(fname, user , obj[i]['message'], obj[i]['chat_id']);
                                }
                                
                            }
                        }
                        
                        if(fromNew == 1){
                            $(".ui-widget-content.ui-chatbox-content").css("display", "block");
                        }
                        $(".ui-chatbox-log").scroll(function(){
                            chatboxScroll($(this));
                        });
                    }
                }); 
            })(row);
        }
    }
}
    
function closeChatBox(id){
    var cid = id.replace("box","");
    $.ajax({
        type: "post",
        url: close_chat_url,
        data: { 
            'box' : cid
        },
        success: function(result) {
            if(result == 'session_expired'){
                window.location.href = logout_url;
                return false;
            }
        }
    }); 
}

function newChatMessages(onLoad){ 
    $.ajax({
        type: "post",
        url: get_open_box_chat_msgs_url,
        data: {
            'except_me': 1,
            'new_only' : 1
        },
        success: function(result) {
            if(result == 'session_expired'){
                window.location.href = logout_url;
                return false;
            }
            var obj = JSON.parse( result );
            if( obj.length > 0 ){
                var unique_client = new Array();
                // get unique client
                for(i=0; i<obj.length; i++){
                    (function(i)
                    {
                        if( isInMultiArray('id', obj[i]['club_client_id'], unique_client) == -1){
                            unique_client.push({
                                id: obj[i]['club_client_id'],
                                fname: obj[i]['client_fname'],
                                lname: obj[i]['client_lname'],
                                email: obj[i]['client_email']
                            });
                        }

                        $.ajax({
                            type: "post",
                            url: seen_message_url,
                            data: { 
                                'chat_id' : obj[i]['chat_id']
                            },
                            success: function(result) {
                                if(result == 'session_expired'){
                                    window.location.href = logout_url;
                                    return false;
                                }
                            }
                        });
                    })(i);
                }
                
                
//                if( onLoad != 1 ){
                jsonOpenChats = JSON.stringify(unique_client);
                
                openChats(jsonOpenChats, 1);
//                }
            }
            
            window.setTimeout(newChatMessages, 20000);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          window.setTimeout(newChatMessages, 20000);
        }
    }); 
}

function isInMultiArray(column, value, globalarray){
    var theIndex = -1;
    
    for (var e = 0; e < globalarray.length; e++) {
            if (globalarray[e][column] == value) {
                    theIndex = e;
                    break;
            }
    }
    
    return theIndex;
}
//function updateChatBoxMessages()

//$("body").on('scroll','.ui-chatbox-log',function(){


function chatboxScroll(elem){
    var $this = elem;
    if( $this.scrollTop() <= 0){
        var lastno = $this.find(".ui-chatbox-msg").length;
        if($this.find(".ui-chatbox-msg.spinner").length <= 0){
            $this.prepend('<div class="ui-chatbox-msg spinner">loading...</div>');
        


            $.ajax({
                type: "post",
                url: get_open_box_old_chat_msgs_url,
                data: { 
                    'lastno' : lastno,
                    'box_name': $this.attr("id")
                },
                success: function(result) {
                    if(result == 'session_expired'){
                        window.location.href = logout_url;
                        return false;
                    }
                    var user = [];
                    var obj = JSON.parse( result );
                    $this.find(".spinner").remove();
                    for( i=0; i<obj.length; i++ ){
                        // if message doesn't exist
                        // then append
                        if(obj[i]['sender_type'] == 'coach'){
                            if(obj[i]['club_admin_id'] == coach){

                                var e = document.createElement('div');
                                $this.prepend(e);
                                $(e).hide();

                                var msgElement = document.createElement("span");
                                $(msgElement).addClass('msg-container');
                                $(msgElement).text(obj[i]['message']);
                                e.appendChild(msgElement);
                                $(e).addClass("ui-chatbox-msg");
                                $(e).addClass(obj[i]['chat_id']);
                                $(e).addClass("my-msg");
                                $(e).attr('data-id',obj[i]['chat_id']);
                                $(e).css("maxWidth", $this.width());
                                $(e).fadeIn();

                                var peerName = document.createElement("span");
                                $(peerName).text("You"); 
                                $(peerName).addClass('msg-label');
                                e.appendChild(peerName);

                                //$this.prepend('<div style="display: block; max-width: 242px;" class="ui-chatbox-msg '+obj[i]['chat_id']+' my-msg" data-id="'+obj[i]['chat_id']+'"><span class="msg-container">'+obj[i]['message']+'</span><span class="msg-label">You</span></div>');
                                //$("#box" + $this.attr("data-id")).chatbox("option", "boxManager").addMsg("You", user , obj[i]['message'], obj[i]['chat_id']);
                            }else{

                                var e = document.createElement('div');
                                $this.prepend(e);
                                $(e).hide();

                                var msgElement = document.createElement("span");
                                $(msgElement).addClass('msg-container');
                                $(msgElement).text(obj[i]['message']);
                                e.appendChild(msgElement);
                                $(e).addClass("ui-chatbox-msg");
                                $(e).addClass(obj[i]['chat_id']);
                                $(e).attr('data-id',obj[i]['chat_id']);
                                $(e).css("maxWidth", $this.width());
                                $(e).fadeIn();

                                var peerName = document.createElement("span");
                                $(peerName).text(obj[i]['club_admin_name']); 
                                $(peerName).addClass('msg-label');
                                e.appendChild(peerName);

    //                                $this.prepend('<div style="display: block; max-width: 242px;" class="ui-chatbox-msg '+obj[i]['chat_id']+'" data-id="'+obj[i]['chat_id']+'"><span class="msg-container">'+obj[i]['message']+'</span><span class="msg-label">'+obj[i]['club_admin_name']+'</span></div>');
                            }
                        }else{
                            var e = document.createElement('div');
                            $this.prepend(e);
                            $(e).hide();

                            var msgElement = document.createElement("span");
                            $(msgElement).addClass('msg-container');
                            $(msgElement).text(obj[i]['message']);
                            e.appendChild(msgElement);
                            $(e).addClass("ui-chatbox-msg");
                            $(e).addClass(obj[i]['chat_id']);
                            $(e).attr('data-id',obj[i]['chat_id']);
                            $(e).css("maxWidth", $this.width());
                            $(e).fadeIn();

                            var peerName = document.createElement("span");
                            $(peerName).text(obj[i]['client_fname']); 
                            $(peerName).addClass('msg-label');
                            e.appendChild(peerName);
    //                            $this.prepend('<div style="display: block; max-width: 242px;" class="ui-chatbox-msg '+obj[i]['chat_id']+'" data-id="'+obj[i]['chat_id']+'"><span class="msg-container">'+obj[i]['message']+'</span><span class="msg-label">'+obj[i]['client_fname']+'</span></div>');
                        }

                    }
                }
            }); 


        }

    }else{
        $this.find(".spinner").remove();
    }
}