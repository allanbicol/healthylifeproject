var owl = '';
var week_loop_id = 0;
/*============================================
        Pie-Chart
        `lk: 261015 from /templates/hlp-2.0/js/scripts.js
     ==============================================*/
 var WindowWidth = $(window).width();
 if(WindowWidth > 991){
        $('.percentage').each(function(){
             $(this).easyPieChart({
                             size:160,
                             animate: 2000,
                             lineCap:'butt',
                             scaleColor: false,
                             trackColor: '#d3d7e2',
                             barColor: '#6c4695',
                             lineWidth: 8,
                             easing:'easeOutQuad'
                     });
            
        });
             
 }
 else{
     $('.percentage').each(function(){
         $(this).easyPieChart({
                 size:125,
                 animate: 2000,
                 lineCap:'butt',
                 scaleColor: false,
                 trackColor: '#d3d7e2',
                 barColor: '#6c4695',
                 lineWidth: 8,
                 easing:'easeOutQuad'
             });
         });
 }
 
 $('.toggle-tests').click(function(){
    var $this = $(this);
    $this.toggleClass('SeeMore2');
    if($(".all-tests").hasClass('in')){
        $this.find("span").text('more'); 
        $this.find(".fa").removeClass('fa-angle-up'); 
        $this.find(".fa").addClass('fa-angle-down'); 
    } else {
        $this.find("span").text('less'); 
        $this.find(".fa").removeClass('fa-angle-down'); 
        $this.find(".fa").addClass('fa-angle-up'); 
    }
});


///*============================================
//    Week-Program-slider
//    `lk: 241015 moved to /js/dashboard.js
//    ==============================================*/
//
//$("#program-slider").owlCarousel({
//    //items: 7,
//    singleItem: true,
//    pagination: false,        
//    dragBeforeAnimFinish : false,
//    mouseDrag: false,
//    touchDrag: false,
//    slideSpeed:2000
//});
//
//var weekProgram = $("#program-slider").data('owlCarousel');
//
//$('.previous-prog').click(function(){
//            weekProgram.prev();
//    });
//
//$('.next-prog').click(function(){
//        weekProgram.next();
//});

$(document).ready(function(){
    $(".aia-vitality-test").tooltip();
    
    if(s_paid_program == 1){
        getPaidProgramItems(1);
    }else{
        get12weekProgramItems(1); 
    }
});
var resizeid;
$(window).resize(function() {
    clearTimeout(resizeid);
    resizeid = setTimeout(fixWeekDaysHeight, 500);
    
});


$(".btn-take-test").click(function(){
    $("#testOptionsModal").modal("show");
});


function fixWeekDaysHeight(){
    if( $(window).width() < 975 ){
        return false;
    } 
//    console.log("fixWeekDaysHeight");
    var $maxHeight = 0;
    var $column = 0;
    $("#program-slider .active-week .week-day-item").css("height", "auto");
    
    for( $i=1; $i<=$("#program-slider .active-week .week-day-item").length; $i++ ){
        if($column < 3){
            $column += 1;
        } 
        if ( $("#program-slider .active-week .week-day-item."+$i).height() > $maxHeight ) { 
            $maxHeight = $("#program-slider .active-week .week-day-item."+$i).height(); 
        }
        
        if($column == 3){ 
            $("#program-slider .active-week .week-day-item."+$i).height( $maxHeight );
            $("#program-slider .active-week .week-day-item."+($i-1)).height( $maxHeight );
            $("#program-slider .active-week .week-day-item."+($i-2)).height( $maxHeight );
            $("#program-slider .active-week .week-tips.week-day-item."+($i-1)).height( $maxHeight + 36 );
            
            $maxHeight = 0;
            $column = 0;
        }
    }
}

function hideInactiveWeeks(){
    var $active_week = $(".current-week").html();
    $("#program-slider").find(".week:not(["+ $active_week+"])").removeClass("active-week");
    $("#program-slider").find(".week."+ $active_week).addClass("active-week");
}

function getPaidProgramItems(s_single){
    $.ajax({
        type: "post",
        url: get_paid_program_items_url,
        data: {
            'client_id': client_id,
            'prog_client_id': prog_client_id,
            'current_week': current_week,
            'display_week': workingweek,
            'program_type': program_type,
            'workout_base': workout_base,
            'workout_level': workout_level,
            's_lite_test_complete' : s_lite_test_complete,
            's_single' : s_single,
            'program': program,
        },
        success: function(result) {
            if(result == 'session_expired'){
                window.location.href = logout_url;
                return false;
            }
            
            $(".program-container").html( result );

            
            $(".alternative-workout-options").find("input[type=checkbox]").bootstrapToggle();            
            $('.selectpicker').selectpicker(); 
            
            
            
            /*============================================
                Week-Program-slider
                `lk: 241015 from /templates/hlp-2.0/js/scripts.js
                ==============================================*/
            owl = $("#program-slider");
            owl.owlCarousel({
                //items: 7,
                singleItem: true,
                pagination: false,        
                dragBeforeAnimFinish : false,
                mouseDrag: false,
                touchDrag: false,
                slideSpeed:2000
            });

            var weekProgram = owl.data('owlCarousel');
            // Now move the carousel to the third item.
            weekProgram.jumpTo( workingweek - 1 );
            
            $('.previous-prog').click(function(){
                var $nextwk = parseFloat($(".next-week").html()); 
                var $currentwk = parseFloat($(".current-week").html()); 
                var $prevwk = parseFloat($(".prev-week").html()); 

                $currentwk = $currentwk - 1;
                $(".prev-week").html( $prevwk - 1);
                $(".current-week").html( $currentwk );
                $(".next-week").html( $nextwk - 1 );
                $(".next-week").closest("div").removeClass('unluck-nextweek');
                $(".next-week").closest("div").removeClass('take-a-test');
                
                weekProgram.prev();
                
                hideInactiveWeeks();
                hideProgramButton();
                fixWeekDaysHeight();
                
            });

            $('.next-prog').click(function(){
                var $self = $(this); 
                if( $self.parent().hasClass('unluck-nextweek') ){
                    return false;
                }

                if( $self.parent().hasClass("take-a-test") ){
                     bootbox.alert("To unlock Week "+ parseFloat($self.parent().find(".next-week").html()) +", you must complete your Healthy Life Test.");     
                     return false;
                }

                var $nextwk = parseFloat($(".next-week").html()); 
                var $currentwk = parseFloat($(".current-week").html()); 
                var $prevwk = parseFloat($(".prev-week").html()); 

                $nextwk = $nextwk + 1;
                $currentwk = $currentwk + 1;
                $(".prev-week").html( $prevwk + 1 );
                $(".current-week").html( $currentwk);


                if( $nextwk == (parseFloat(current_week) + 2) ){
                    $(".next-week").closest("div").addClass('unluck-nextweek');
                }

                if( ($nextwk > current_week) && s_lite_test_complete == 0  ){
                    $(".next-week").closest("div").addClass('take-a-test');
                }
                
                
                
                $(".next-week").html( $nextwk );
                weekProgram.next();
                
                hideInactiveWeeks();
                hideProgramButton();
                fixWeekDaysHeight();
            });
            
            
            
            /*============================================
                Week-day-slider
                ==============================================*/

            $("#week-day").owlCarousel({
                items: 7,
                singleItem: true,
                pagination: false,
                slideSpeed:2000
            });

            $("#week-day .owl-item").on("touchstart mousedown", function(e) {
                // Prevent carousel swipe
                e.stopPropagation();
            });

            var weekDayFilter = $("#week-day").data('owlCarousel');

            weekDayFilter.jumpTo( currentday - 1 );

            $('.week-day-filter-left').click(function(){
                        weekDayFilter.prev();
                });

                $('.week-day-filter-right').click(function(){
                        weekDayFilter.next();
                });

            /*============================================
                Mobile Week-day-slider
                ==============================================*/

            $("#mob-program-slider").owlCarousel({
                items: 7,
                singleItem: true,
                pagination: false,
                slideSpeed:2000,
                dragBeforeAnimFinish : false,
                mouseDrag: false,
                touchDrag: false
            });

            var mobProgramFilter = $("#mob-program-slider").data('owlCarousel');

            mobProgramFilter.jumpTo( currentday - 1 );

            $('.week-day-filter-left').click(function(){
                    mobProgramFilter.prev();
            });

            $('.week-day-filter-right').click(function(){
                    mobProgramFilter.next();
            });
            
            
            hideInactiveWeeks();
            setTimeout(fixWeekDaysHeight, 500);
            
            week_loop_id = parseInt(current_week) + 1;
            getPaidProgramWeekItems(week_loop_id);
            
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
//              window.setTimeout(update12weekProgramProgress, 10000);
        }
    }); 
}

function getPaidProgramWeekItems(week_id){ 
    if( owl.find(".item."+ week_id).hasClass('no-data') ){
        $.ajax({
            type: "post",
            url: get_paid_program_items_url,
            data: {
                'client_id': client_id,
                'prog_client_id': prog_client_id,
                'current_week': current_week,
                'display_week': week_id,
                'workout_base': workout_base,
                'workout_level': workout_level,
                's_lite_test_complete' : s_lite_test_complete,
                's_single' : 1,
                's_week_item' : 1,
                'program': program,
            },
            success: function(result) {
                if(result == 'session_expired'){
                    window.location.href = logout_url;
                    return false;
                }
                
                owl.find(".item."+ week_id).html(result);
                owl.find(".item."+ week_id).removeClass('no-data');
                
                week_loop_id = week_loop_id - 1;
                
                if(week_loop_id > 0 ){ 
                    getPaidProgramWeekItems(week_loop_id);
                }
                
                fixWeekDaysHeight();
            }
        });
    }else{
        week_loop_id = week_loop_id - 1;
                
        if(week_loop_id > 0 ){ 
            getPaidProgramWeekItems(week_loop_id);
        }
    }
}

function get12weekProgramItems(s_single){
    if( $("#Program").length > 0 ){
        $.ajax({
            type: "post",
            url: get_12weekprogram_items_url,
            data: {
                'client_id': client_id,
                'prog_client_id': prog_client_id,
                'current_week': current_week,
                'display_week': workingweek,
                'workout_base': workout_base,
                'program_type': program_type,
                'client_email': client_email,
                's_lite_test_complete': s_lite_test_complete,
                's_single' : s_single
            },
            success: function(result) {
                if(result == 'session_expired'){
                    window.location.href = logout_url;
                    return false;
                }

                $(".program-container").html( result );
                $('.selectpicker').selectpicker();
    //            initializeSortableItems();
                


                /*============================================
                    Week-Program-slider
                    `lk: 241015 from /templates/hlp-2.0/js/scripts.js
                    ==============================================*/
                owl = $("#program-slider");
                owl.owlCarousel({
                    //items: 7,
                    singleItem: true,
                    pagination: false,        
                    dragBeforeAnimFinish : false,
                    mouseDrag: false,
                    touchDrag: false,
                    slideSpeed:2000
                });



                var weekProgram = owl.data('owlCarousel');
                // Now move the carousel to the third item.
                weekProgram.jumpTo( workingweek - 1 );

                $('.previous-prog').click(function(){
                    var $nextwk = parseFloat($(".next-week").html()); 
                    var $currentwk = parseFloat($(".current-week").html()); 
                    var $prevwk = parseFloat($(".prev-week").html()); 

                    $currentwk = $currentwk - 1;
                    $(".prev-week").html( $prevwk - 1);
                    $(".current-week").html( $currentwk );
                    $(".next-week").html( $nextwk - 1 );
                    $(".next-week").closest("div").removeClass('unluck-nextweek');
                    $(".next-week").closest("div").removeClass('take-a-test');

                    weekProgram.prev();

                    hideInactiveWeeks();
//                    changeWorkingWeek( $currentwk );
                    hideProgramButton();
                    fixWeekDaysHeight();

                });
                
                
                $('.next-prog').click(function(){
                    var $self = $(this); 
                    if( $self.parent().hasClass('unluck-nextweek') ){
                        return false;
                    }
                    
                    var $nextwk = parseFloat($(".next-week").html()); 
                    var $currentwk = parseFloat($(".current-week").html()); 
                    var $prevwk = parseFloat($(".prev-week").html()); 

                    $nextwk = $nextwk + 1;
                    $currentwk = $currentwk + 1;
                    $(".prev-week").html( $prevwk + 1 );
                    $(".current-week").html( $currentwk);


                    if( $nextwk == (parseFloat(current_week) + 2) ){
                        $(".next-week").closest("div").addClass('unluck-nextweek');
                    }

                    if( ($nextwk > current_week) && s_lite_test_complete == 0  ){
                        $(".next-week").closest("div").addClass('take-a-test');
                    }



                    $(".next-week").html( $nextwk );
                    weekProgram.next();

                    hideInactiveWeeks();
//                    changeWorkingWeek( $currentwk );
                    hideProgramButton();
                    fixWeekDaysHeight();
                });
                
                /*============================================
                    Week-day-slider
                    ==============================================*/

                $("#week-day").owlCarousel({
                    items: 7,
                    singleItem: true,
                    pagination: false,
                    slideSpeed:2000
                });

                $("#week-day .owl-item").on("touchstart mousedown", function(e) {
                    // Prevent carousel swipe
                    e.stopPropagation();
                });
                var weekDayFilter = $("#week-day").data('owlCarousel');

                weekDayFilter.jumpTo( currentday - 1 );

                $('.week-day-filter-left').click(function(){
                            weekDayFilter.prev();
                    });

                    $('.week-day-filter-right').click(function(){
                            weekDayFilter.next();
                    });

                /*============================================
                    Mobile Week-day-slider
                    ==============================================*/

                $("#mob-program-slider").owlCarousel({
                    items: 7,
                    singleItem: true,
                    pagination: false,
                    slideSpeed:2000,
                    dragBeforeAnimFinish : false,
                    mouseDrag: false,
                    touchDrag: false
                });

                var mobProgramFilter = $("#mob-program-slider").data('owlCarousel');

                mobProgramFilter.jumpTo( currentday - 1 );

                $('.week-day-filter-left').click(function(){
                        mobProgramFilter.prev();
                });

                $('.week-day-filter-right').click(function(){
                        mobProgramFilter.next();
                });


                hideInactiveWeeks();
                setTimeout(fixWeekDaysHeight, 500);
                
                week_loop_id = parseInt(current_week) + 1;
                get12weekProgramWeekItems(week_loop_id);

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
    //              window.setTimeout(update12weekProgramProgress, 10000);
            }
        }); 
    }
}

function hideProgramButton(){
    var $nextwk = parseFloat($(".next-week").html()); 
    var $prevwk = parseFloat($(".prev-week").html()); 
    
    if($prevwk < 1){
        $(".prev-week").closest("div").addClass("hidden");
    }else{
        $(".prev-week").closest("div").removeClass("hidden");
    }
    
    if($nextwk > ( parseFloat(weeklength) + 1 )  ){
        $(".next-week").closest("div").addClass("hidden");
    }else{
        $(".next-week").closest("div").removeClass("hidden");
    }
    
    if( $nextwk > weeklength ){ 
        $(".next-week").closest("div").addClass('hidden');
    }else{
        $(".next-week").closest("div").removeClass('hidden');
    }
    
}

function get12weekProgramWeekItems(week_id){ 
    if( owl.find(".item."+ week_id).hasClass('no-data') ){
        $.ajax({
            type: "post",
            url: get_12weekprogram_items_url,
            data: {
                'prog_client_id': prog_client_id,
                'current_week': current_week,
                'program_type': program_type,
                'workout_base': workout_base,
                'client_email': client_email,
                'client_id': client_id,
                's_lite_test_complete' : s_lite_test_complete,
                'display_week': week_id,
                's_single' : 1,
                's_week_item' : 1
            },
            success: function(result) {
                if(result == 'session_expired'){
                    window.location.href = logout_url;
                    return false;
                }
                
                owl.find(".item."+ week_id).html(result);
                owl.find(".item."+ week_id).removeClass('no-data');
                
                week_loop_id = week_loop_id - 1;
                
                if(week_loop_id > 0 ){ 
                    get12weekProgramWeekItems(week_loop_id);
                }
                
//                autosize($('.comment-holder .comment-entry textarea'));
                
                fixWeekDaysHeight();
//                initializeSortableItems();
            }
        });
    }else{
        week_loop_id = week_loop_id - 1;
                
        if(week_loop_id > 0 ){ 
            get12weekProgramWeekItems(week_loop_id);
        }
    }
}

$("body").on('change','select[name=week]',function(){
    if( $(this).find(':selected').attr('data-class') == 'take-a-test'){
        bootbox.alert("To unlock Week "+ parseFloat( $(this).val() ) +", the client must complete his/her Healthy Life Test.");     
        return false;
    }else{
        $(this).closest("form").submit(); 
    }
});