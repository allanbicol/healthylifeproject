var s_list_can_go = 0;
$(document).ready(function(){
//    getList();
//    sortClientSnapShots();
    updateClientSnapshotsSummaryResults();
//    update12weekProgramClientProgress();
    
});

/** load more at scroll to the bottom event*/

$(function(){
    $(window).scroll(function(){
        var document_height = $(document).height() - 200;
        var last_item_position = (jQuery('#clientSnapShots .item').length >0) ? jQuery('#clientSnapShots .item').last().offset().top : 0;
        var scroll_position = parseFloat($(window).scrollTop()+$(window).height());

        //if(document_height <= scroll_position){
        if( scroll_position > last_item_position ){ 
            getList();
        }
    });
});
 
var callajax= 0; // async alternative
var scroll_list_count = 3;
function getList(){
    if(s_list_can_go == 1){
        if($('#endOfRecord').css('display') != 'block'){
            if(callajax == 0){
                callajax = 1; // async alternative

                // show spinner
                $('#endOfRecord').css('display', 'none');
                $('#loadMore').css('display','inline');

                $.ajax({
                    url: club_client_list_path,
                    method: 'GET',
                    data: {
                        'limit_start_at': $('#clientSnapShots .item').length,
                        'sort' : sort_by,
                        'keyword' : keyword
                    },
                    success: function(list) { 
                        if(list.trim() != ''){
                            // load list
                            $('#clientSnapShots').append(list);
                            sortClientSnapShots();

                            // remove spinner
                            $('#loadMore').css('display', 'none');

                            if(list == ''){
                                $('#endOfRecord').css('display', 'block');
                            }else{
                                $('#endOfRecord').css('display', 'none');
                            }

                        }else{
                            $('#loadMore').css('display','none');
                            $('#endOfRecord').css('display', 'block');
    //                        if($('#clientSnapShots .item').length != 0){
    //                            $('#loadMore').css('display','none');
    //                        }
                        }


                        if($('#clientSnapShots .item').length == 0){
                            // no active quotepacket
                            $('#loadMore').css('display', 'none');
                        }


                        callajax=0; // async alternative
                        if(scroll_list_count > 0){ 
                            scroll_list_count = scroll_list_count - 1;
                            getList();
                        }else{
                            scroll_list_count = 3;
                            updateClientSnapshotsSummaryResults();
    //                        update12weekProgramClientProgress();
                        }
                    },
                    error:function(){
                        // error loading
                        $('#loadMore').css('display', 'none');
                        callajax=0; // async alternative
                    }
                });

            }
        }else{
            $('#endOfRecord').css('display', 'block');
            updateClientSnapshotsSummaryResults();
    //        update12weekProgramClientProgress();
        }
    }
}




$(".sort-options").change(function(){
    var $this = $(this);
    sortClientSnapShots();
});

function sortClientSnapShots(){
    var $sortby = $(".sort-options").val();
    if( $sortby == 'results' || $sortby == 'age' || $sortby == 'category'){
        tinysort('#clientSnapShots > div.item',{attr:'data-' + $sortby,order:'desc'});
    }else{
        tinysort('#clientSnapShots > div.item',{attr:'data-' + $sortby,order:'asc'});
    }
}

function updateClientSnapshotsSummaryResults(){
    var clients = [];
    $("#clientSnapShots").find(".item").each(function(){
        var $this = $(this);
        if( !$this.hasClass('summary-done') ){
            clients.push( parseFloat($this.attr("data-client-id")) );
            $this.addClass('summary-done');
        }
    });
    if( clients.length > 0){
        $.ajax({
            type: "get",
            url: update_client_snapshots_summary_results,
            data: {
                'clients': clients
            },
            success: function(result) {
                if(result == 'session_expired'){
                    window.location.href = logout_url;
                    return false;
                }
                var obj = JSON.parse( result );

                for($i=0, $count=obj.length; $i < $count; $i++){
                    var $this = $("#clientSnapShots").find("[data-client-id="+ obj[$i]['club_client_id'] +"]");
                    var $healthy_life_score = obj[$i]['healthy_life_score'] * 100;
                    $healthy_life_score = Math.round( $healthy_life_score * 10 ) / 10;
                    $this.find('.summ-healthy-life-potential').text( $healthy_life_score + '%' );

                    var $years_added = (obj[$i]['years_added'] != '' && obj[$i]['years_added'] > 0) ? obj[$i]['years_added'] : 0;
                    if($years_added > 0){
                        var $years_added = Math.round( $years_added * 10 ) / 10;
                        $this.find('.summ-years-added').text( '+' + $years_added  );
                    }else{
                        $this.find('.summ-years-added').text( '---'  );
                    }

                    $this.find('.client-number-of-tests').text( obj[$i]['total_tests'] );

                }
                
                
                
                
//                update12weekProgramClientProgress();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
//                  window.setTimeout(updateClientSnapshotsSummaryResults, 10000);
            }
        }); 
    }
}

function update12weekProgramClientProgress(){ 
    var emails = [];
    $("body").find(".weekly-and-overall-progress.active-client").each(function(){
        var $this = $(this);
        if( $this.find(".total-week-items").text() == '--' ){
            emails.push( $this.closest("div.item").attr("data-email") );
        }
        
        
    });
    
    if( emails.length > 0 ){
        $.ajax({
            type: "get",
            url: update_program_progress_url,
            data: {
                'emails': emails
            },
            success: function(result) {
                if(result == 'session_expired'){
                    window.location.href = logout_url;
                    return false;
                }
                var obj = JSON.parse( result );

                for($i=0, $count=obj.length; $i < $count; $i++){
                    var $this = $("#clientSnapShots").find('.item[data-email="'+ obj[$i]['email'] +'"]');
                    
                    $this.find(".done-week-items").text( obj[$i]['week_complete_actions'] );
                    $this.find(".total-week-items").text( obj[$i]['week_total_actions'] );
//                    $this.find(".done-overall-items").text( obj[$i]['overall_complete_actions'] );
//                    $this.find(".overall-items").text( obj[$i]['overall_actions'] );
                }




            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        }); 
    }
}

function updateGraphs(){
//    $("#yearsAdded").attr("data-values", '{"title":"yrs", "data":[["Oct",0],["Nov",0],["Dec",0],["Jan",10]]}');
    
}

if(typeof google !== 'undefined'){
	/*
	*
	*	Google Chart init
	*
	*/
	google.load('visualization', '1.1', {packages: ['corechart', 'bar', 'line']});
	google.setOnLoadCallback(drawChart);

}

function drawChart() {
	var lineOptions = {
		chartArea:{
			width:'85%',
			height: '80%'
		},colors: ['#6c4695'],
		lineWidth: 1,
		pointSize: 9,
		title: '',
		titlePosition: 'in', 
		titleTextStyle: {color:'#666'},
		enableInteractivity: false,
		legend: {position: 'none'},
		vAxis: {textStyle:{color: '#666'}},
		xAxis: {textStyle:{color: '#666'}},
		gridlineColor: '#fff'
	};
	var columnOptions = {
        title: '',
        titlePosition: 'in', 
        titleTextStyle: {color:'#666'},
        chartArea:{width:'85%',height: '80%'},
        colors: ['#6c4695'],
        enableInteractivity: false,
        vAxis: {
        	textStyle:{color: '#666'},
        	minValue: 0
        },
        xAxis: {textStyle:{color: '#666'}},
        bar: {groupWidth: '90%'},
        legend: { position: 'none' },
        gridlineColor: '#fff'
    };
    
        $.ajax({
            type: "get",
            url: generate_summary_url,
            data: {},
            success: function(result) { 
                if(result == 'session_expired'){
                    window.location.href = logout_url;
                    return false;
                }
                var obj = JSON.parse( result );
                if( obj[0]['total_years_added'] > 0 ){
                    var $tya = Math.round( obj[0]['total_years_added'] * 10 ) / 10;
                    $(".total-years-added").text( $tya );
                }else{
                    $(".total-years-added").text( '---' );
                }
                if( obj[0]['total_cm_lost'] > 0 ){
                    var $tcl = Math.round( obj[0]['total_cm_lost'] * 10 ) / 10;
                    $(".total-cm-lost").text( $tcl );
                }else{
                    $(".total-cm-lost").text( '---' );
                }
                if( obj[0]['total_kg_lost'] > 0 ){
                    var $tkl = Math.round( obj[0]['total_kg_lost'] * 10 ) / 10;
                    $(".total-kg-lost").text( $tkl );
                }else{
                    $(".total-kg-lost").text( '---' );
                }


//                jQuery('.line-chart, .bar-chart.cmlost').each(function(){
//                        var holder = jQuery(this);
//                        var chartBox = holder.find('.chart');
//
//                        function initGraph(){
//                                //create data table object
//                                var dataTable = new google.visualization.DataTable();
//                                var values = holder.data('values');
//                                var options = {};
//                                if(holder.hasClass('line-chart')){
//                                        jQuery.extend(options, lineOptions);
//                                }
//                                else{
//                                        jQuery.extend(options, columnOptions);
//                                }
//
//                                if(values){
//                                        if(values.title) options.title = values.title;
//                                        if(values.viewWindow) options.vAxis.viewWindow = values.viewWindow;
//                                        if(values.data) {
//                                                //define columns
//                                                dataTable.addColumn('string','Month');
//                                                dataTable.addColumn('number', 'KG');
//                                                //define rows of data
//                                                if( holder.hasClass('cmlost')){
//                                                    
//                                                    var chestval = (obj[0]['total_biometric_chest_lost'] > 0) ? obj[0]['total_biometric_chest_lost'] : 0;
//                                                    var thighsval = (obj[0]['total_biometric_thigh_lost'] > 0) ? obj[0]['total_biometric_thigh_lost'] : 0;
//                                                    var waistval = (obj[0]['total_biometric_waist_value_lost'] > 0) ? obj[0]['total_biometric_waist_value_lost'] : 0;
//                                                    var armsval = (obj[0]['total_biometric_arm_lost'] > 0) ? obj[0]['total_biometric_arm_lost'] : 0;
//                                                    dataTable.addRows( [["Chest", parseFloat(chestval) ],["Thighs", parseFloat(thighsval)],["Waist",parseFloat(waistval)],["Arms",parseFloat(armsval)]] );
//                                                    
//                                                }else{
//                                                    dataTable.addRows(values.data);
//                                                }
//                                        }
//                                }
//
//                                var chart = new google.visualization[(holder.hasClass('line-chart') ? 'LineChart' : 'ColumnChart')](chartBox[0]);
//                                chart.draw(dataTable, options);
//                                if(values.title) jQuery('text:contains(' + values.title + ')',holder).attr('class','title').attr({'x':'1', 'y':'19.5'});
//                                var yAxisValues = jQuery('text[text-anchor="end"]',holder);
//                                yAxisValues.last().remove();
//                                yAxisValues.first().remove();
//
//                        }
//
//                        initGraph();
//
//                        var timer;
//                        jQuery(window).on('resize orientationchange',function(){
//                                clearTimeout(timer);
//                                timer = setTimeout(initGraph,300);
//                        });
//                });
                
                
                
                
                
                
                
//                $.ajax({
//                    type: "get",
//                    url: generate_data_url,
//                    data: {},
//                    success: function(result) { 
//                        if(result == 'session_expired'){
//                            window.location.href = logout_url;
//                            return false;
//                        }
//                        var objresult = JSON.parse( result );
//                        var obj_yearsadded = objresult.yearsadded;
//                        var obj_kglost = objresult.kglost;
//
//
//                        jQuery('.bar-chart').each(function(){
//                            var holder = jQuery(this);
//
//                            if( holder.hasClass('cmlost') ){}else{
//                                var chartBox = holder.find('.chart');
//
//                                function initGraph(){
//                                        //create data table object
//                                        var dataTable = new google.visualization.DataTable();
//                                        var values = holder.data('values');
//                                        var options = {};
//                                        if(holder.hasClass('line-chart')){
//                                                jQuery.extend(options, lineOptions);
//                                        }
//                                        else{
//                                                jQuery.extend(options, columnOptions);
//                                        }
//
//                                        if(values){
//                                                if(values.title) options.title = values.title;
//                                                if(values.viewWindow) options.vAxis.viewWindow = values.viewWindow;
//                                                if(values.data) {
//                                                        //define columns
//                                                        dataTable.addColumn('string','Month');
//                                                        dataTable.addColumn('number', 'KG');
//                                                        //define rows of data
//                                                        if( holder.hasClass('yearsadded')){
//
//                                                            var yearsadded = [];
//
//                                                            for(var i in obj_yearsadded)
//                                                                yearsadded.push([i, parseFloat(obj_yearsadded[i])]);
//
//                                                            dataTable.addRows( yearsadded );
//
//                                                        }else if( holder.hasClass('kglost')){
//
//                                                            var kglost = [];
//
//                                                            for(var i in obj_kglost)
//                                                                kglost.push([i, parseFloat(obj_kglost[i])]);
//                                                            dataTable.addRows( kglost );
//                                                        }else{
//                                                            dataTable.addRows(values.data);
//                                                        }
//                                                }
//                                        }
//
//                                        var chart = new google.visualization[(holder.hasClass('line-chart') ? 'LineChart' : 'ColumnChart')](chartBox[0]);
//                                        chart.draw(dataTable, options);
//                                        if(values.title) jQuery('text:contains(' + values.title + ')',holder).attr('class','title').attr({'x':'1', 'y':'19.5'});
//                                        var yAxisValues = jQuery('text[text-anchor="end"]',holder);
//                                        yAxisValues.last().remove();
//                                        yAxisValues.first().remove();
//                                }
//
//                                initGraph();
//
//                                var timer;
//                                jQuery(window).on('resize orientationchange',function(){
//                                        clearTimeout(timer);
//                                        timer = setTimeout(initGraph,300);
//                                });
//                            }
//                        });
//                        
                        s_list_can_go = 1;
                        getList();
//                    },
//                    error: function (XMLHttpRequest, textStatus, errorThrown) {
//        //              window.setTimeout(update12weekProgramProgress, 10000);
//                    }
//                }); 
//                
                
                
                
                
                
                

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
//              window.setTimeout(update12weekProgramProgress, 10000);
                s_list_can_go = 1;
            }
        }); 
        
        
	
}