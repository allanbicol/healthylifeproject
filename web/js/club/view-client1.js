/*============================================
        Pie-Chart
        `lk: 261015 from /templates/hlp-2.0/js/scripts.js
     ==============================================*/
 var WindowWidth = $(window).width();
 if(WindowWidth > 991){
        $('.percentage').each(function(){
             $(this).easyPieChart({
                             size:160,
                             animate: 2000,
                             lineCap:'butt',
                             scaleColor: false,
                             trackColor: '#d3d7e2',
                             barColor: '#6c4695',
                             lineWidth: 8,
                             easing:'easeOutQuad'
                     });
            
        });
             
 }
 else{
     $('.percentage').each(function(){
         $(this).easyPieChart({
                 size:125,
                 animate: 2000,
                 lineCap:'butt',
                 scaleColor: false,
                 trackColor: '#d3d7e2',
                 barColor: '#6c4695',
                 lineWidth: 8,
                 easing:'easeOutQuad'
             });
         });
 }
 
 $('.toggle-tests').click(function(){
    var $this = $(this);
    $this.toggleClass('SeeMore2');
    if($(".all-tests").hasClass('in')){
        $this.find("span").text('more'); 
        $this.find(".fa").removeClass('fa-angle-up'); 
        $this.find(".fa").addClass('fa-angle-down'); 
    } else {
        $this.find("span").text('less'); 
        $this.find(".fa").removeClass('fa-angle-down'); 
        $this.find(".fa").addClass('fa-angle-up'); 
    }
});