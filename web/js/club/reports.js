$(".btn-detailed-reports").click(function(){
   $("form.report-from").attr('action', detailed_report);
});

$(".btn-summary-reports").click(function(){
   $("form.report-from").attr('action', report);
});

$(document).ready(function(){
    request_params = request_params.replace(/&amp;/g, '&');
    $.ajax({
            type: "get",
            url: update_report_summary + "?" + request_params,
            data: {},
            success: function(result) {
                if(result == 'session_expired'){
                    window.location.href = logout_url;
                    return false;
                }
                var objresult = JSON.parse( result );
                
                if($(".total-years-added").text() == '---'){
                    if( parseFloat(objresult[0]['total_years_added']) > 0 ){
                        var total_years_added =  Math.round(parseFloat(objresult[0]['total_years_added']) * 10 ) / 10;
                        $(".total-years-added").text( total_years_added );
                    }
                }

                if($(".total-years-added-avg").text() == '---'){
                    if( parseFloat(objresult[0]['avg_years_added']) > 0 ){
                        var avg_years_added =  Math.round(parseFloat(objresult[0]['avg_years_added']) * 10 ) / 10;
                        $(".total-years-added-avg").text( avg_years_added );
                    }
                }
                
                if($(".total-kg-lost").text() == '---'){
                    if( parseFloat(objresult[0]['total_kg_lost']) > 0 ){
                        var total_kg_lost =  Math.round(parseFloat(objresult[0]['total_kg_lost']) * 10 ) / 10;
                        $(".total-kg-lost").text( total_kg_lost );
                    }
                }

                if($(".total-kg-lost-avg").text() == '---'){
                    if( parseFloat(objresult[0]['avg_kg_lost']) > 0 ){
                        var avg_kg_lost =  Math.round(parseFloat(objresult[0]['avg_kg_lost']) * 10 ) / 10;
                        $(".total-kg-lost-avg").text( avg_kg_lost );
                    }
                }
                
                if($(".total-cm-lost").text() == '---'){
                    if( parseFloat(objresult[0]['total_cm_lost']) > 0 ){
                        var total_cm_lost =  Math.round(parseFloat(objresult[0]['total_cm_lost']) * 10 ) / 10;
                        $(".total-cm-lost").text( total_cm_lost );
                    }
                }
                
                if($(".total-cm-lost-avg").text() == '---'){
                    if( parseFloat(objresult[0]['avg_cm_lost']) > 0 ){
                        var avg_cm_lost =  Math.round(parseFloat(objresult[0]['avg_cm_lost']) * 10 ) / 10;
                        $(".total-cm-lost-avg").text( avg_cm_lost );
                    }
                }
                
                if($.trim($(".hlp-age-potential").text()) == '---'){
                    if( parseFloat(objresult[0]['avg_healthy_life_score']) > 0 ){
                        var avg_healthy_life_score = parseFloat(objresult[0]['avg_healthy_life_score']) * 100;
                        avg_healthy_life_score = Math.round( avg_healthy_life_score * 10 ) / 10;
                        $(".hlp-age-potential").text( avg_healthy_life_score + '%');
                    }
                }
                
                if($.trim($(".estimated-current-hl-age").text()) == '---'){
                    if( parseFloat(objresult[0]['avg_estimated_current_healthy_life_age']) > 0 ){
                        var avg_estimated_current_healthy_life_age = parseFloat(objresult[0]['avg_estimated_current_healthy_life_age']);
                        avg_estimated_current_healthy_life_age = Math.round( avg_estimated_current_healthy_life_age * 10 ) / 10;
                        $(".estimated-current-hl-age").text( avg_estimated_current_healthy_life_age );
                    }
                }
                
                if($.trim($(".avg-you-could-add-up-to").text()) == '---'){
                    if( parseFloat(objresult[0]['avg_you_could_add_up_to']) > 0 ){
                        var avg_you_could_add_up_to = parseFloat(objresult[0]['avg_you_could_add_up_to']);
                        avg_you_could_add_up_to = Math.round( avg_you_could_add_up_to * 10 ) / 10;
                        $(".avg-you-could-add-up-to").text( avg_you_could_add_up_to );
                    }
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        }); 
});
if(typeof google !== 'undefined'){
	/*
	*
	*	Google Chart init
	*
	*/
	google.load('visualization', '1.1', {packages: ['corechart', 'bar', 'line']});
	google.setOnLoadCallback(drawChart);

}

function drawChart() {
	var lineOptions = {
		chartArea:{
			width:'85%',
			height: '80%'
		},colors: ['#6c4695'],
		lineWidth: 1,
		pointSize: 9,
		title: '',
		titlePosition: 'in', 
		titleTextStyle: {color:'#666'},
		enableInteractivity: false,
		legend: {position: 'none'},
		vAxis: {textStyle:{color: '#666'}},
		xAxis: {textStyle:{color: '#666'}},
		gridlineColor: '#fff'
	};
	var columnOptions = {
        title: '',
        titlePosition: 'in', 
        titleTextStyle: {color:'#666'},
        chartArea:{width:'85%',height: '80%'},
        colors: ['#6c4695'],
        enableInteractivity: false,
        vAxis: {
        	textStyle:{color: '#666'},
        	minValue: 0
        },
        xAxis: {textStyle:{color: '#666'}},
        bar: {groupWidth: '90%'},
        legend: { position: 'none' },
        gridlineColor: '#fff'
    };
//        /* `lk: 150622*/
        var report_charts = new Array();
        request_params = request_params.replace(/&amp;/g, '&');

        $.ajax({
            type: "get",
            url: update_report_data + "?" + request_params,
            data: {},
            success: function(result) {
                if(result == 'session_expired'){
                    window.location.href = logout_url;
                    return false;
                }
                var objresult = JSON.parse( result );
                
                jQuery('.bar-chart').each(function(){
                    var holder = jQuery(this);
                    
//                    if( holder.hasClass('cmlost') ){}else{
                        var chartBox = holder.find('.chart');
                        
                        function initGraph(){
                                //create data table object
                                var dataTable = new google.visualization.DataTable();
                                var values = holder.data('values');
                                var options = {};
                                if(holder.hasClass('line-chart')){
                                        jQuery.extend(options, lineOptions);
                                }
                                else{
                                        jQuery.extend(options, columnOptions);
                                }

                                if(values){
                                        if(values.title) options.title = values.title;
                                        if(values.viewWindow) options.vAxis.viewWindow = values.viewWindow;
                                        if(values.data) {
//                                                //define columns
                                                dataTable.addColumn('string','Month');
                                                dataTable.addColumn('number', 'KG');
                                                //define rows of data
                                                if( holder.hasClass('yearsadded')){
                                                    
                                                    var yearsadded = [];
                                                    for(var i in objresult){
                                                        var total_years_added = 0;
                                                        if(typeof objresult[i][0] === 'undefined'){}else{
                                                            total_years_added = parseFloat(objresult[i][0]['total_years_added']);
                                                        }
                                                        yearsadded.push([ i, total_years_added]);
                                                    }
                                                    dataTable.addRows( yearsadded );


                                                }else if( holder.hasClass('kglost')){
                                                    
                                                    var kglost = [];
                                                    for(var i in objresult){
                                                        var total_kg_lost = 0;
                                                        if(typeof objresult[i][0] === 'undefined'){}else{
                                                            total_kg_lost = parseFloat(objresult[i][0]['total_kg_lost']);
                                                        }
                                                        kglost.push([ i, total_kg_lost]);
                                                    }
                                                    dataTable.addRows( kglost );
                                                    

                                                    
                                                }else if( holder.hasClass('cmlost')){
                                                    var total_biometric_arm_lost = 0;
                                                    var total_biometric_chest_lost = 0;
                                                    var total_biometric_thigh_lost = 0;
                                                    var total_biometric_waist_value_lost = 0;
                                                    for(var i in objresult){
                                                        
                                                        if(typeof objresult[i][0] === 'undefined'){}else{
                                                            total_biometric_arm_lost += parseFloat(objresult[i][0]['total_biometric_arm_lost']);
                                                            total_biometric_chest_lost += parseFloat(objresult[i][0]['total_biometric_chest_lost']);
                                                            total_biometric_thigh_lost += parseFloat(objresult[i][0]['total_biometric_thigh_lost']);
                                                            total_biometric_waist_value_lost += parseFloat(objresult[i][0]['total_biometric_waist_value_lost']);
                                                        }
                                                    }
                                                    
                                                    var chestval = (total_biometric_chest_lost > 0) ? total_biometric_chest_lost : 0;
                                                    var thighsval = (total_biometric_thigh_lost > 0) ? total_biometric_thigh_lost : 0;
                                                    var waistval = (total_biometric_waist_value_lost > 0) ? total_biometric_waist_value_lost : 0;
                                                    var armsval = (total_biometric_arm_lost > 0) ? total_biometric_arm_lost : 0;
                                                    
                                                    dataTable.addRows( [["Chest", parseFloat(chestval) ],["Thighs", parseFloat(thighsval)],["Waist",parseFloat(waistval)],["Arms",parseFloat(armsval)]] );
                                                    

                                                }else{
                                                    dataTable.addRows(values.data);
                                                }
                                        }
                                }

                                var chart = new google.visualization[(holder.hasClass('line-chart') ? 'LineChart' : 'ColumnChart')](chartBox[0]);
                                chart.draw(dataTable, options);
                                if(values.title) jQuery('text:contains(' + values.title + ')',holder).attr('class','title').attr({'x':'1', 'y':'19.5'});
                                var yAxisValues = jQuery('text[text-anchor="end"]',holder);
                                yAxisValues.last().remove();
                                yAxisValues.first().remove();

//                                /* `lk: 150622 */
                                report_charts.push( chart.getImageURI() );
                        }

                        initGraph();

                        var timer;
                        jQuery(window).on('resize orientationchange',function(){
                                clearTimeout(timer);
                                timer = setTimeout(initGraph,300);
                        });
//                    }
                });
                
                 /* `lk: 150622 */
                if (typeof(chart_session_path) != "undefined"){
                    $.ajax({
                        url: chart_session_path,
                        method: 'POST',
                        data: {
                            'charts': report_charts
                        },
                        success: function(response) {
                        },
                        error:function(){
                        }
                    });
                }

                
                $(".processing-data").css("display", "none");

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
//              window.setTimeout(update12weekProgramProgress, 10000);
            }
        }); 
	
}