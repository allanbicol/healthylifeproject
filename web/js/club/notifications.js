$(document).ready(function(){
    $(window).resize();
    
    
   newNotifications(); 
});

$(window).resize(function(){
    if( $(window).width() <= 991){
        $("li.chat-messages").appendTo("ul.mob-notices");
        $("li.notifications").appendTo("ul.mob-notices");
    }else{
        $("ul.mob-notices li.notifications").prependTo("#site-nav .right-content ul");
        $("ul.mob-notices li.chat-messages").prependTo("#site-nav .right-content ul");
        
    }
});

$("body").on("click",function(e){
    // if click outside notifications
    if($(e.target).closest("li.notifications").length == 0){ 
        $(".notification-dropdown").removeClass("show");
    }else{
        
        $(".notification-dropdown").toggleClass("show");
    
        if($(".notification-dropdown").hasClass("show")){
            if($(".notification-dropdown .lists ul li").length == 0 ||
                    $(e.target).closest("li.notifications").find(".badge").hasClass("show")){
                
                $(".notification-dropdown .lists").removeClass("show");
                $(".notification-dropdown .spinner").addClass("show");
                $(".notification-dropdown").find(".see-all").text("");
                $.ajax({
                    url: get_notifications_url,
                    method: 'GET',
                    data: {
                        'request' : 'notifications'
                    },
                    success: function(result) { 
                        if(result == 'session_expired'){
                            window.location.href = logout_url;
                            return false;
                        }
                        if(result == ''){
                            $(".notification-dropdown").find(".see-all").text("no new notification");
                        }
                        $(".notification-dropdown .spinner").removeClass("show");
                        $(".notification-dropdown .lists").addClass("show");
                        $(".notification-dropdown .lists ul").html(result);
                        
                        $(e.target).closest("li.notifications").find(".badge").removeClass("show");
                    },
                    error:function(){
                        // error loading
                        $(".notification-dropdown .spinner").removeClass("show");
                        $(".notification-dropdown .lists").addClass("show");
                    }
                });
            }
        }
    }
});
$("body").on("click",function(e){
    // if click outside notifications
    if($(e.target).closest("li.chat-messages").length == 0){
        $(".chat-messages-dropdown").removeClass("show");
    }else{
        
        $(".chat-messages-dropdown").toggleClass("show");
    
        if($(".chat-messages-dropdown").hasClass("show")){
            if($(".chat-messages-dropdown .lists ul li").length == 0 || 
                    $(e.target).closest("li.chat-messages").find(".badge").hasClass("show")){
                
                $(".chat-messages-dropdown .lists").removeClass("show");
                $(".chat-messages-dropdown .spinner").addClass("show");
                $(".chat-messages-dropdown").find(".see-all").text("");
                $.ajax({
                    url: get_notifications_url,
                    method: 'GET',
                    data: {
                        'request' : 'chat-messages'
                    },
                    success: function(result) { 
                        if(result == 'session_expired'){
                            window.location.href = logout_url;
                            return false;
                        }
                        if(result == ''){
                            $(".chat-messages-dropdown").find(".see-all").text("no new message");
                        }
                        $(".chat-messages-dropdown .spinner").removeClass("show");
                        $(".chat-messages-dropdown .lists").addClass("show");
                        $(".chat-messages-dropdown .lists ul").html(result);
                        $(e.target).closest("li.chat-messages").find(".badge").removeClass("show");
                    },
                    error:function(){
                        // error loading
                        $(".chat-messages-dropdown .spinner").removeClass("show");
                        $(".chat-messages-dropdown .lists").addClass("show");
                    }
                });
            }
        }
    }
});

var s_read_notice_done = 1;
function readNotifications($note_id, $club_client_id, $type){
    if(s_read_notice_done == 1){
        s_read_notice_done = 0;
        $.ajax({
            url: read_notifications_url,
            method: 'POST',
            data: {
                'note_id': $note_id,
                'club_client_id': $club_client_id,
                'type': $type
            },
            success: function(result) { 
                if(result == 'session_expired'){
                    window.location.href = logout_url;
                    return false;
                }

                if($type == 'chat-messages'){
                    $(".chat-messages-dropdown").find('li[data-club-client-id="'+$club_client_id+'"]').removeClass("unread");
                }
                
                s_read_notice_done = 1;
            },
            error:function(){
                // error loading
            }
        });
    }
}

$("body").on("click","li.chat-messages .notice-sread a", function(){
   var $this = $(this);
   var $note_id = $this.closest("li").attr("data-note-id");
   var $club_client_id = $this.closest("li").attr("data-club-client-id");
   $this.closest("li").removeClass("unread");
   
   readNotifications($note_id, $club_client_id, 'chat-messages');
   return false;
   
});

$("body").on("click","li.notifications .notice-sread a", function(){
   var $this = $(this);
   var $note_id = $this.closest("li").attr("data-note-id");
   var $club_client_id = $this.closest("li").attr("data-club-client-id");
   $this.closest("li").removeClass("unread");
   
   readNotifications($note_id, $club_client_id, 'notifications');
   return false;
   
});

$("body").on("click", ".chat-messages li .notice-photo, .chat-messages li .notice-details", function(){

    var $this = $(this);
    var openchatsarray = [{'id':$this.closest("li").attr("data-club-client-id"),
            'fname': $this.closest("li").attr("data-fname"),
            'lname': $this.closest("li").attr("data-lname"),
            'email': $this.closest("li").attr("data-email")
            }];
    var myJsonString = JSON.stringify(openchatsarray);
//    console.log(myJsonString);
    $("#box"+ $this.closest("li").attr("data-club-client-id") ).closest(".ui-chatbox-content").css("display","block");
    openChats(myJsonString, 0);
    return false;
});

$("body").on("focus",".ui-chatbox-input-box",function(){
   var $this = $(this);
   var $club_client_id = $this.closest(".ui-chatbox").find(".ui-widget-header span").attr("data-club-client-id");
   
   readNotifications(0, $club_client_id, 'chat-messages');
});

$("body").on("click", ".notifications li .notice-photo, .notifications li .notice-details", function(){

    var $this = $(this);
    var openchatsarray = [{'id':$this.closest("li").attr("data-club-client-id"),
            'fname': $this.closest("li").attr("data-fname"),
            'lname': $this.closest("li").attr("data-lname"),
            'email': $this.closest("li").attr("data-email")
            }];
        
        
    if( $this.closest("li").attr("data-note-type") == 'test'){
        $(location).attr('href', view_client_url + '?id=' + $this.closest("li").attr("data-club-client-id") + '&note=' + $this.closest("li").attr("data-note-id"));
    }else if( $this.closest("li").attr("data-note-type") == 'chat'){
        $(location).attr('href', view_client_url + '?id=' + $this.closest("li").attr("data-club-client-id") + '&note=' + $this.closest("li").attr("data-note-id"));
    }else if( $this.closest("li").attr("data-note-type") == 'payment-card-details'){
        $(location).attr('href', admin_url + '?note=' + $this.closest("li").attr("data-note-id"));
    }
    
    return false;
});


function newNotifications(){
    $.ajax({
        url: new_notifications_url,
        method: 'POST',
        data: {},
        success: function(result) { 
            if(result == 'session_expired'){
                window.location.href = logout_url;
                return false;
            }
            
            var obj = JSON.parse( result );
            if(obj['notice_count'] > 0){
                $("li.notifications").find(".badge").text( obj['notice_count'] );
                $("li.notifications").find(".badge").addClass("show");
            }else{
                $("li.notifications").find(".badge").text( 0 );
                $("li.notifications").find(".badge").removeClass("show");
            }
            
            if(obj['chat_count'] > 0){
                $("li.chat-messages").find(".badge").text( obj['chat_count'] );
                $("li.chat-messages").find(".badge").addClass("show");
            }else{
                $("li.chat-messages").find(".badge").text( 0 );
                $("li.chat-messages").find(".badge").removeClass("show");
            }
            
            window.setTimeout(newNotifications, 10000);
        },
        error:function(){
            // error loading
            window.setTimeout(newNotifications, 10000);
        }
    });
}
var s_request_done = 1;
$('.chat-messages-dropdown .lists').bind('scroll', function(){
  if($(this).scrollTop() + $(this).innerHeight()>=$(this)[0].scrollHeight)
  {
        
        if(s_request_done == 1 ){
            s_request_done = 0;
            var record_count = $(".chat-messages-dropdown ul li").length;
            $(".chat-messages-dropdown .spinner").addClass("show");
            $.ajax({
                url: get_notifications_url,
                method: 'GET',
                data: {
                    'request' : 'chat-messages',
                    'start_at':record_count
                },
                success: function(result) { 
                    if(result == 'session_expired'){
                        window.location.href = logout_url;
                        return false;
                    }
                    $(".chat-messages-dropdown .spinner").removeClass("show");
                    $(".chat-messages-dropdown .lists ul").append(result);
                    s_request_done = 1;
                },
                error:function(){
                    // error loading
                    $(".chat-messages-dropdown .spinner").removeClass("show");
                }
            });
        }
  }
});

var s_request_notifications_done = 1;
$('.notification-dropdown .lists').bind('scroll', function(){
  if($(this).scrollTop() + $(this).innerHeight()>=$(this)[0].scrollHeight)
  {
        
        if(s_request_notifications_done == 1 ){
            s_request_notifications_done = 0;
            var record_count = $(".notification-dropdown ul li").length;
            $(".notification-dropdown .spinner").addClass("show");
            $.ajax({
                url: get_notifications_url,
                method: 'GET',
                data: {
                    'request' : 'others',
                    'start_at':record_count
                },
                success: function(result) { 
                    if(result == 'session_expired'){
                        window.location.href = logout_url;
                        return false;
                    }
                    $(".notification-dropdown .spinner").removeClass("show");
                    $(".notification-dropdown .lists ul").append(result);
                    s_request_notifications_done = 1;
                },
                error:function(){
                    // error loading
                    $(".notification-dropdown .spinner").removeClass("show");
                }
            });
        }
  }
});

