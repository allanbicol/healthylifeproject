 $(document).ready(function(){
        
        /** 
         * This part does the "fixed navigation after scroll" functionality
         * We use the jQuery function scroll() to recalculate our variables as the 
         * page is scrolled/
         */
        var size =  detectmob();
        if (size==true){
             if ($('#side_bar').length > 0){
                $('#side_bar').removeClass('stick');
                $('.row').removeClass('row-new');
                $('.row').removeClass('vid');
             }
        }
                
        $(window).scroll(function(){
            var window_top = $(window).scrollTop() + 50; // the "12" should equal the margin-top value for nav.stick
            if ($('#side_bar').length > 0){
                var div_top = $('#side_bar').offset().top;
            }
                if (window_top > 270) {
                    $('#side_bar').addClass('stick');
                    
                }else {
                    $(".hash").removeClass("hash");
                    $('#side_bar').removeClass('stick');
                }
//                console.log(window_top +' - '+ div_top);
               var size =  detectmob();
                if (size==true){
                     $('#side_bar').removeClass('stick');
                     $('.row').removeClass('row-new');
                     $('.row').removeClass('vid');
                }
        });
        
        
        /**
         * This part causes smooth scrolling using scrollto.js
         * We target all a tags inside the nav, and apply the scrollto.js to it.
         */
        $("nav1 a").click(function(evn){
            evn.preventDefault();
            var str = this.hash;
            var id = str.replace('#','');
            $('html,body').scrollTo(this.hash, this.hash); 
            //alert(id);
            $("#" + id ).addClass("hash");
        });
        
        
        
        /**
         * This part handles the highlighting functionality.
         * We use the scroll functionality again, some array creation and 
         * manipulation, class adding and class removing, and conditional testing
         */
        var aChildren = $("nav1 li").children(); // find the a children of the list items
        var aArray = []; // create the empty aArray
        for (var i=0; i < aChildren.length; i++) {    
            var aChild = aChildren[i];
            var ahref = $(aChild).attr('href');
            aArray.push(ahref);
        } // this for loop fills the aArray with attribute href values
        
        $(window).scroll(function(){
            var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page
            var windowHeight = $(window).height(); // get the height of the window
            var docHeight = $(document).height();
            
            for (var i=0; i < aArray.length; i++) {
                var theID = aArray[i];
                if($(theID).length > 0){
                    var divPos = $(theID).offset().top; // get the offset of the div from the top of page
                    var divHeight = $(theID).height(); // get the height of the div in question
                    if (windowPos >= divPos && windowPos < (divPos + divHeight)) {
                        $("a[href='" + theID + "']").addClass("nav-active");
                    } else {
                        $("a[href='" + theID + "']").removeClass("nav-active");
                    }
                }
            }
            
            if(windowPos + windowHeight == docHeight) {
                if (!$("nav1 li:last-child a").hasClass("nav-active")) {
                    var navActiveCurrent = $(".nav-active").attr("href");
                    $("a[href='" + navActiveCurrent + "']").removeClass("nav-active");
                    $("nav1 li:last-child a").addClass("nav-active");
                }
            }
        });
    });
    function detectmob() {
        if(window.innerWidth <= 990 && window.innerHeight <= 600) {
          return true;
        } else {
          return false;
        }
    }

