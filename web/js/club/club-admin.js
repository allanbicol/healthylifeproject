/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


//$(".delete-admin").click(function(){
//    var $this = $(this);
//    $( "#dialog-confirm" ).dialog({
//        resizable: false,
//        height:240,
//        modal: true,
//        buttons: {
//            "Delete item": function() {
//                $("input[name=club_admin_id]").val($this.attr('data-id'));
//                $("form#deleteClubAdmin").submit();
//            },
//            Cancel: function() {
//                $( this ).dialog( "close" );
//            }
//        }
//    });
//});
$(document).ready(function(){
   if( $("#mdlBuyCredit").find(".alert.alert-danger").length > 0 ){
       $('#mdlBuyCredit').modal({
            backdrop: 'static',
            keyboard: false
        });
   } 
   
   if( $("#mdlNewAdmin").find(".alert.alert-danger").length > 0 ){
       $('#mdlNewAdmin').modal({
            backdrop: 'static',
            keyboard: false
        });
   } 
   
   if( $("#mdlUpdateAdmin").find(".alert.alert-danger").length > 0 ){
       $('#mdlUpdateAdmin').modal({
            backdrop: 'static',
            keyboard: false
        });
   } 
   
   $('.color_pic').each( function() {
        $(this).minicolors({
            control: $(this).attr('data-control') || 'hue',
            defaultValue: $(this).attr('data-defaultValue') || '',
            format: $(this).attr('data-format') || 'hex',
            keywords: $(this).attr('data-keywords') || '',
            inline: $(this).attr('data-inline') === 'true',
            letterCase: $(this).attr('data-letterCase') || 'lowercase',
            opacity: $(this).attr('data-opacity'),
            position: $(this).attr('data-position') || 'bottom left',
            
            theme: 'default'
        });

    });
            
});

$(".delete-admin").click(function(){
    var $this = $(this);
    bootbox.confirm("Are you sure?", function(result) {
        if(result === true){
            $("input[name=club_admin_id]").val($this.attr('data-id'));
            $("form#deleteClubAdmin").submit();
        }
    });  
});


$(".btn-purchase-credits").click(function(){
    $('#mdlBuyCredit').modal({
        backdrop: 'static',
        keyboard: false
    });
});


$(".btn-new-admin").click(function(){
    $("#mdlNewAdmin").find(".alert").hide();
    $("#mdlNewAdmin").find("input[name=fname]").val( "" );
    $("#mdlNewAdmin").find("input[name=lname]").val( "" );
    $("#mdlNewAdmin").find("input[name=email]").val( "" );
    $("#mdlNewAdmin").find("input[name=password]").val( "" );
    $("#mdlNewAdmin").find("input[name=confirm_password]").val( "" );
    
    $('#mdlNewAdmin').modal({
        backdrop: 'static',
        keyboard: false
    });
});

$(".edit-admin").click(function(){
    var $this = $(this);
    var json = $this.attr("data-info");
    var info = $.parseJSON( json );
    $("#mdlUpdateAdmin").find(".alert").hide();
    $("#mdlUpdateAdmin").find("input[name=club_admin_id]").val( info.club_admin_id );
    $("#mdlUpdateAdmin").find("input[name=fname]").val( info.fname );
    $("#mdlUpdateAdmin").find("input[name=lname]").val( info.lname );
    $("#mdlUpdateAdmin").find("input[name=email]").val( info.email );
    $("#mdlUpdateAdmin").find("select[name=role] option[value='"+ info.role +"']").prop('selected',true);
    $("#mdlUpdateAdmin").find("select[name=status] option[value='"+ info.status +"']").prop('selected',true);
    $("#mdlNewAdmin").find("input[name=password]").val( "" );
    $("#mdlNewAdmin").find("input[name=confirm_password]").val( "" );
    if(info.s_aia_approved == 1){
        $("#mdlUpdateAdmin").find("input[name=s_aia_approved]").prop('checked', true);
    }else{
        $("#mdlUpdateAdmin").find("input[name=s_aia_approved]").prop('checked', false);
    }
    
    $('#mdlUpdateAdmin').modal({
        backdrop: 'static',
        keyboard: false
    });
});

$(".btn-new-event").click(function(){
    $("#mdlNewEvent").find(".alert").hide();
    $("#mdlNewEvent").find("input[name=event_name]").val( "" );
    $("#mdlNewEvent").find("input[name=location]").val( "" );
    $("#mdlNewEvent").find("input[name=status]").val( "" );
    
    $('#mdlNewEvent').modal({
        backdrop: 'static',
        keyboard: false
    });
});

$(".edit-event").click(function(){
    var $this = $(this);
    var json = $this.attr("data-info");
    var info = $.parseJSON( json );
    $("#mdlEditEvent").find(".alert").hide();
    $("#mdlEditEvent").find("input[name=event_id]").val( info.event_id );
    $("#mdlEditEvent").find("input[name=event_name]").val( info.event_name );
    $("#mdlEditEvent").find("input[name=location]").val( info.location );
    $("#mdlUpdateAdmin").find("select[name=status] option[value='"+ info.status +"']").prop('selected',true);
    
    $('#mdlEditEvent').modal({
        backdrop: 'static',
        keyboard: false
    });
});


$(".btn-new-category").click(function(){
    $("#mdlNewCategory").find(".alert").hide();
    $("#mdlNewCategory").find("input[name=event_name]").val( "" );
    $("#mdlNewCategory").find("input[name=location]").val( "" );
    $("#mdlNewCategory").find("input[name=status]").val( "" );
    
    $('#mdlNewCategory').modal({
        backdrop: 'static',
        keyboard: false
    });
});

$(".edit-category").click(function(){
    var $this = $(this);
    var json = $this.attr("data-info");
    var info = $.parseJSON( json );
    $("#mdlEditCategory").find("input[name=id]").val( info.id );
    $("#mdlEditCategory").find("input[name=category_name]").val( info.category_name );
    $("#mdlEditCategory").find("input[name=color]").val( info.color );
    var sel_color = info.color;
    var color = sel_color.replace('#','');
    $("#"+color).attr('selected','selected');
    //alert($('#color2').prop('selectedIndex'));
    
    $('#color2').ddslick('selectid', {id: info.color });
    
    $('#mdlEditCategory').modal({
        backdrop: 'static',
        keyboard: false
    });
});

$(".delete-category").click(function(){
    var $this = $(this);
    bootbox.confirm("Are you sure?", function(result) {
        if(result === true){
                $("input[name=category_id]").val($this.attr('data-id'));
                $("form#deleteClientCategory").submit();
        }
    });  
});


$(function()
{	
	$('#color1').ddslick(
	{
		//callback function: do anything with selectedData
		onSelected: function(data)
		{
                    
			/*
				we are calling custom created function.
				that function will display selected option detail.
			*/
			displaySelectedData("Callback Function on Dropdown Selection" , data);
		}
	});
	
        $('#color2').ddslick(
	{
		//callback function: do anything with selectedData
		onSelected: function(data)
		{
                    
			/*
				we are calling custom created function.
				that function will display selected option detail.
			*/
			displaySelectedData("Callback Function on Dropdown Selection" , data);
		},
	});
	
	function displaySelectedData(demoIndex, ddData)
	{
		/*
			add heading to div
		*/
		$('#dd-display-data').html("<h3>Data from Demo " + demoIndex + "</h3>");
		/*
			append selected drop down index to result.
			also added code so you can check
			in browser console for selected object
		*/
		$('#dd-display-data').append('<strong>selectedIndex:</strong> ' + ddData.selectedIndex + '<br/><strong>selectedItem:</strong> Check your browser console for selected "li" html element');
		
		/*
			check if selection made.
		*/
		if (ddData.selectedData)
		{
			/*
				appeding more data to result div.
			*/
			$('#dd-display-data').append
			(
				'<br/><strong>Value:</strong>  ' + ddData.selectedData.value +
				'<br/><strong>Description:</strong>  ' + ddData.selectedData.description +
				'<br/><strong>ImageSrc:</strong>  ' + ddData.selectedData.imageSrc
			);
		}
		/*
			browser console code
		*/
		//console.log(ddData);
	}
});   