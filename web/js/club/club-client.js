/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(".delete-item").click(function(){
    var $this = $(this);
    bootbox.confirm("Are you sure?", function(result) {
        if(result == true){
            $("input[name=club_client_id]").val($this.attr('data-id'));
            $("form#delete").submit();
        }
    }); 
//    $( "#dialog-confirm" ).dialog({
//        resizable: false,
//        height:240,
//        modal: true,
//        buttons: {
//            "Delete item": function() {
//                $("input[name=club_client_id]").val($this.attr('data-id'));
//                $("form#delete").submit();
//            },
//            Cancel: function() {
//                $( this ).dialog( "close" );
//            }
//        }
//    });
});


if($('#imgPreview').length > 0){
    var spinner = $('#spinner'); //id of the preview image
    var imgpreview = $('#imgPreview');

    new AjaxUpload('imageUpload', {
        action: asset_resources + '/ajaxupload/ajaxupload.php', //the php script that receives and saves the image
        name: 'image', //The saveimagephp will find the image info in the variable $_FILES['image']
        onSubmit: function(file, extension) {
			$(".get-selected-image").attr("disabled","disabled");
            spinner.html('<img src="' + asset_img + '/spinner.gif">');
            //preview.attr('src', asset_img + '/spinner.gif'); //replace the image SRC with an animated GIF with a 'loading...' message 
        },
        onComplete: function(file, response) {
            if(response.search("Fatal error") > 0){
                $('#spinner').html("");
                $('#imgPreview').html("");
                $("#myModal").modal('hide');
                bootbox.alert("Sorry, your photo cannot be processed! Please choose another one.");
                
                
                
                return false;
            }
            spinner.html('');
			$(".get-selected-image").removeAttr("disabled");
            if(response != 'error'){
                //preview.attr('src', response); //make the preview image display the uploaded file
                imgpreview.html('<img class="picture crop" src="' + response + '">');
                
                $('#uploadedimg').val(response); //drop the path to the file into the hidden field
                
                $("body").find('#imgPreview > img.crop').cropper({
//                    aspectRatio: 1 / 1.08,
                    aspectRatio: 1 / 1,
                    autoCropArea: 0.65,
                    strict: false,
                    guides: false,
                    highlight: false,
                    dragCrop: false,
                    cropBoxMovable: false,
                    cropBoxResizable: false,
                    done: function(data) {
                        $("#crop-size").html('Size: ' +data.height + ' x ' + data.width);
                        $("#dataX").val(data.x);
                        $("#dataY").val(data.y);
                        $("#dataH").val(data.height);
                        $("#dataW").val(data.width);
                    }
                  });
                  
                  $( window ).resize();
            }else{

                alert('Invalid image file!');
                imgpreview.html('');
            }

        }
    });
}

$(".get-selected-image").click(function(){
    var filename = getFilename($('#uploadedimg').val());
	var $this = $(this);
    $this.attr("disabled","disabled");
   $.ajax({
        url: get_crop_photo_path,
        method: 'POST',
        data: {'x': $("#dataX").val(),
            'y': $("#dataY").val(),
            'height': $("#dataH").val(),
            'width': $("#dataW").val(),
            'image': filename
        },
        success: function(response) {
            $('#uploadedimg').val( 'client-pictures/' + response);
            $this.removeAttr("disabled");
            $datetimenow = new Date().getTime();
            $("#cropedPhotoPreview").html('<img class="img-responsive img-circle img-centered img-profile" src="' + asset_uploads + '/client-pictures/' + response + '?'+ $datetimenow + '">');
            $("#myModal").modal('hide');
        },
        error:function(){
            $this.removeAttr("disabled");
            $('#imgPreview').html("");
            $("#myModal").modal('hide');
            bootbox.alert("Sorry, your photo cannot be processed! Please choose another one.");
        }
    });
});

function getFilename(filename){
    var final_fn = '';
    filename =filename.split('/');
    for(i=0; i<filename.length; i++){
        if(i == filename.length - 1){
            final_fn = filename[i];    
        }

    }   
    return final_fn;
}
//==============================================================================
// HELP
//==============================================================================

$("a.help").click(function(){
    var $this = $(this);
    var $field = $this.attr('data-field');
    var $window = $(window);
    if($window.width() < 1178){
        $('html, body').animate({
            scrollTop: $("#sidebar").offset().top
        }, 1000);
    }
    $(".help-container").html('<center><img src="'+ asset_img +'/spinner.gif"/></center>');
    
    $.ajax({
        url: get_help_path,
        method: 'POST',
        data: {
            'field': $field
        },
        success: function(response) {
            $(".help-container").html(response);
//            if( $(".help-container").height() > 812 ){ //812 is the height of #sidebar
//                $(".help-container").html('<p>Click here to see <a class="pointer the-fake-guide">the guide</a></p>');
//                $("#popup3").find(".modal-body").html(response);
//            }
            
            
        },
        error:function(){
            $(".help-container").html('error found');
        }
    });
});

$('select[name="program_type"]').change(function(e) {
    if($('select[name="program_type"]').val()!=''){
        $('#div_program_start_date').show();
    }else{
        $('#div_program_start_date').hide();
    }
  
});

$(document).ready(function(){
     if($('select[name="program_type"]').val()!=''){
        $('#div_program_start_date').show();
    }else{
        $('#div_program_start_date').hide();
    }
    
    
    
});


$('.btnUpdate').click(function(){
    var chk = $('input[name="resetWeek"]').is(':checked'); 
    if(chk==true){
        bootbox.confirm("Are you sure? This will clear your clients program. It won't affect your tests however.", function(result) {
        if(result == true){
            $('#frmAddEditClient').submit();
        }
        
        });
    }else{
        $('#frmAddEditClient').submit();
    }
});

$("input[name=s_non_member_assessment]").change(function(){
    var $this = $(this);
    $this.closest(".non-member-assessment").toggleClass("non");
    if($this.is(":checked")){
        $this.closest(".non-member-assessment").find("input[name=opt_client_id]").removeClass("required");
    }else{
        $this.closest(".non-member-assessment").find("input[name=opt_client_id]").addClass("required");
    }
});

$("input[name=s_aia_vitality_member]").change(function(){
    var $this = $(this);
    $this.closest(".aia-vitality-member").toggleClass("non");
    if($this.is(":checked")){
        $this.closest(".aia-vitality-member").find("input[name=aia_vitality_member_number]").addClass("required");
    }else{
        $this.closest(".aia-vitality-member").find("input[name=aia_vitality_member_number]").removeClass("required");
    }
});