$(document).ready(function(){
    if($('.date-control').length > 0){
        $('.date-control').datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "-150:"+ (new Date).getFullYear(),
            dateFormat: "dd/mm/yy"
        });
    }
});

//=========================================================================================
// LOCATION CHANGE
//=========================================================================================
$("select.location-option").change(function(){
	$this = $(this);
	$this.closest("form").submit();
});



$(function() {
  $('body').on('keydown','input[type=number]', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
});

//=========================================================================================
// AUTO LOGOUT
//=========================================================================================
//$(document).ready(function(){ 
//    if( $("body.home").length == 0 ){ 
//        $(document).idleTimeout({ 
//            inactivity: 3600000, 
//            noconfirm: false, 
//            sessionAlive: 300000,
//            alive_url: dashboard_url,
//            redirect_url: login_url,
//            logout_url: logout_url,
//            showDialog: false
//        }); 
//    }
//    
//});

$(function() {
  $( "#birthdate" ).datepicker();
});
$( "#birthdate" ).change(function() {
  $( "#birthdate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
});
  
$('.btn-show-all').click(function(){
    $('.test-history').show();
    $('.btn-show-all').hide();
});


//==============================================================================
// EMAIL THIS TEST
//=========================================================================================
$(".email-this-test").click(function(){
    var $this = $(this);
    if($this.attr("data-test-type") != 'normal' && $this.attr("data-test-type") != 'aia'){
        $("form#emailTest").attr("action", email_focus_test_url);
    }else{
        $("form#emailTest").attr("action", email_full_test_url);
    }
    $('form#emailTest').find('input[name=recipients]').val( $this.attr('data-email') );
    $('form#emailTest').find('input[name=client_test_id]').val( $this.attr('data-test-id') );
    //$("#emailTestModal").modal("show");
    $("#emailTestModal").find(".send").removeAttr("disabled");
    
    $('#emailTestModal').modal({
        backdrop: 'static',
        keyboard: false
    });
});

$("#emailTestModal").find(".send").click(function(){ 
   var $this = $(this);
    var $formData =  $('form#emailTest').serialize() + "&sajax=1";
    var $caption = $this.html();
    $this.attr('disabled', 'disabled');
    $this.html('Sending ...');
    $.ajax({
        type: "post",
        url: $("form#emailTest").attr("action"),
        data: $formData,
        success: function(result) {
            $this.removeAttr('disabled');
            $this.html($caption);
            if(result == 'success'){
                $("#emailTestModal").modal("hide");
                
            }else{
                alert("The system can't send the email. Please try again!");
            }
            
            
        }
    }); 
});

$("body").on("change","#same_address",function(){
    if($(this).is(":checked")){
        $("input[name=bill_address]").val($("input[name=address]").val());
        $("input[name=bill_city]").val($("input[name=city]").val());
        $("input[name=bill_postcode]").val($("input[name=postcode]").val());
        $("select[name=bill_state]").val($("select[name=state]").val());
        $('.selectpicker').selectpicker('refresh');
    } else {
        $("input[name=bill_address]").val('');
        $("input[name=bill_city]").val('');
        $("input[name=bill_postcode]").val('');
        $("select[name=bill_state]").val('');
        $('.selectpicker').selectpicker('refresh');
    }       
});

$("body").on('click','.invite-12-week',function(){
    var $this = $(this);
    var client_id = $this.attr('data-id');
    var email = $this.attr('data-email');
    var fname = $this.attr('data-fname');
    var lname = $this.attr('data-lname');
    
    $("#modalProgramInvitation").find(".recp-name").text(fname);
    $("#modalProgramInvitation").find("input[name=club_client_id]").val(client_id);
    $("#modalProgramInvitation").find("input[name=email]").val(email);
    $("#modalProgramInvitation").find("input[name=name]").val(fname + ' ' + lname);
    $("#modalProgramInvitation").modal("show");
//    bootbox.confirm("Invite "+fname +" "+lname+" to join 12 week program?", function(result) {
//        if(result==true){
//            $.ajax({
//                type: "post",
//                url: invite_friend_url,
//                data: {'recipients':email,'friend_name':fname + ' '+lname},
//                success: function(result) {
//                    //alert(result);
//                }
//            }); 
//        }
//    });
});

$("body #modalProgramInvitation").on('click','.send-invitation',function(){
    var $self = $(this);
    var $caption = $self.text();
    $self.text('Sending...');
    
    $.ajax({
        type: "post",
        url: invite_friend_url,
        data: {
            'club_client_id' : $("#modalProgramInvitation").find("input[name=club_client_id]").val(),
            'program_type': $("#modalProgramInvitation").find("select[name=invitationOption]").val(),
            'recipients': $("#modalProgramInvitation").find("input[name=email]").val(),
            'friend_name': $("#modalProgramInvitation").find("input[name=name]").val()
        },
        success: function(result) {
            $self.text( $caption );
            if(result == 'success'){
                $("#modalProgramInvitation").modal("hide");
            }
        }
    }); 
});



$("body").on('click','.lifestyle-questionnaires-invitation',function(){
    var $this = $(this);
    var client_id = $this.attr('data-id');
    var email = $this.attr('data-email');
    var fname = $this.attr('data-fname');
    var lname = $this.attr('data-lname');
    
    bootbox.confirm("Invite "+fname +" "+lname+" to answer 6 lifestyle questionnaires in preperation for the healthy life assessment?", function(result) {
        if(result==true){
            $.ajax({
                type: "post",
                url: invite_to_take_lifestyle_questionnaires_url,
                data: {
                    'client_id': client_id,
                    'email':email,
                    'client_fname': fname,
                    'client_lname': lname
                },
                success: function(result) {
                    //alert(result);
                }
            }); 
        }
    });
});